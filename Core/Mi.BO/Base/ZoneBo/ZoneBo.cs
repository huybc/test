﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.Account;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Zone
{
    public class ZoneBo
    {
        public static ZoneEntity GetZoneById(int zoneId)
        {
            try
            {
                ZoneEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ZoneMainDal.GetZoneById(zoneId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ZoneEntity();
            }
        }
        public static ZoneEntity GetZoneByIdV2(int zoneId)
        {
            try
            {
                ZoneEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ZoneMainDal.GetZoneByIdV2(zoneId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ZoneEntity();
            }
        }
        public static ZoneEntity GetZoneByAlias(string alias)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneByAlias(alias);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ZoneEntity();
            }
        }
        public static List<ZoneEntity> GetListZoneByAlias(string alias)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetListZoneByAlias(alias);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneEntity> GetListPropertyByParentIdAndType(int parentId, int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetListPropertyByParentIdAndType(parentId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneEntity> GetListZoneActiveByParentId(int parentZoneId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneActiveByParentId(parentZoneId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        //public static List<ZoneEntity> GetListZoneByParentId(int parentZoneId)
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            return db.ZoneMainDal.GetZoneByParentId(parentZoneId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneEntity>();
        //    }
        //}
        public static List<ZoneEntity> GetListZoneByParentId(int parentZoneId, int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneByParentId(parentZoneId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneEntity> GetSiteMapByCateoryId(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetSiteMapByCategoryId(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListZoneActivedByParentId(int parentZoneId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneActivedByParentId(parentZoneId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        //public static List<ZoneWithSimpleFieldEntity> GetListParentZoneByParentId()
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            var zones = db.ZoneMainDal.GetZoneByParentId(0);
        //            var simpleZones = new List<ZoneWithSimpleFieldEntity>();
        //            simpleZones.AddRange(zones.Select(zone => new ZoneWithSimpleFieldEntity()
        //            {
        //                Id = zone.Id,
        //                Name = zone.Name,
        //                ParentId = zone.ParentId,
        //                ShortUrl = zone.ShortUrl,
        //                SortOrder = zone.SortOrder,
        //                Invisibled = zone.Invisibled,
        //                Status = zone.Status,
        //                Avatar = zone.Avatar
        //            }));
        //            return simpleZones;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneWithSimpleFieldEntity>();
        //    }
        //}

        public static List<ZoneWithSimpleFieldEntity> GetListParentZoneActivedByParentId()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var zones = db.ZoneMainDal.GetZoneActivedByParentId(0);
                    var simpleZones = new List<ZoneWithSimpleFieldEntity>();
                    simpleZones.AddRange(zones.Select(zone => new ZoneWithSimpleFieldEntity()
                    {
                        Id = zone.Id,
                        Name = zone.Name,
                        ParentId = zone.ParentId,
                        ShortUrl = zone.ShortUrl,
                        SortOrder = zone.SortOrder,
                        Invisibled = zone.Invisibled,
                        Status = zone.Status,
                        Avatar = zone.Avatar
                    }));
                    return simpleZones;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        //public static List<ZoneEntity> GetAllZone()
        //  {
        //      try
        //      {
        //          using (var db = new CmsMainDb())
        //          {
        //              return db.ZoneMainDal.GetZoneByParentId(-1);
        //          }
        //      }
        //      catch (Exception ex)
        //      {
        //          Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //          return new List<ZoneEntity>();
        //      }
        //  }  
        public static List<ZoneEntity> GetAllZone(int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneByParentId(-1, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneAndProductCountEntity> GetZoneByParentIdAndCount(int parentId,int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneByParentIdAndCount(parentId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneAndProductCountEntity>();
            }
        }
        public static IEnumerable<ZoneEntity> ZoneSearch(string keyword, int status, int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.ZoneSearch(keyword, status, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static IEnumerable<ZoneEntity> ZoneSearch(string keyword, string username, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate,
                                                       ZoneFilterFieldForUsername filterFieldForUsername,
                                                       ZoneSortExpression sortOrder,
                                                       int status,
                                                       int type, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.SearchNewsWithPaging(keyword, username, zoneIds, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, status, type, pageIndex, pageSize,
                                                      ref totalRow);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActived()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneActivedByParentId(-1);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        //public static List<ZoneEntity> GetListZoneByUserId(int userId)
        //{
        //    try
        //    {
        //        var user = UserBo.GetById(userId);
        //        if (null != user)
        //        {
        //            using (var db = new CmsMainDb())
        //            {
        //                return user.IsFullZone ? db.ZoneMainDal.GetZoneByParentId(-1) : db.ZoneMainDal.GetZoneByUserId(userId);
        //            }
        //        }
        //        return new List<ZoneEntity>();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneEntity>();
        //    }
        //}

        public static List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            try
            {
                List<ZoneEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ZoneMainDal.GetZoneByNewsId(newsId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        //public static List<ZoneEntity> GetListZoneByUsername(string username)
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            var user = db.UserMainDal.GetUserByUsername(username);
        //            if (null != user)
        //            {
        //                return user.IsFullZone ? db.ZoneMainDal.GetZoneByParentId(-1) : db.ZoneMainDal.GetZoneByUserId(user.Id);
        //            }
        //            return new List<ZoneEntity>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneEntity>();
        //    }
        //}
        //public static List<ZoneEntity> GetListZoneByUsername(string username, int type)
        //{
        //    try
        //    {
        //        var user = UserBo.GetUserByUsername(username);
        //        if (null != user)
        //        {
        //            return user.IsFullZone ? GetListZoneByParentId(-1, type) : GetListZoneByUserId(user.Id);
        //        }
        //        return new List<ZoneEntity>();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneEntity>();
        //    }
        //}
        public static List<ZoneEntity> GetZoneByUserIdAndPermissionId(int userId, int permissionId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUserIdAndPermissionId(userId, permissionId);
            }
            return returnValue;

        }
        public static List<ZoneEntity> GetListZoneByUsernameAndPermissionId(string username, int permissionId, int type)
        {
            try
            {
                var user = UserBo.GetUserByUsername(username);
                if (null != user)
                {
                    return user.IsFullZone && user.IsFullPermission
                               ? GetListZoneByParentId(-1, type)
                               : GetZoneByUserIdAndPermissionId(user.Id, permissionId);
                }
                return new List<ZoneEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }



        //public static List<ZoneEntity> GetListZoneByUsernameAndPermissionId(string username, int permissionId)
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            var user = db.UserMainDal.GetUserByUsername(username);
        //            if (null != user)
        //            {
        //                return user.IsFullZone && user.IsFullPermission
        //                           ? db.ZoneMainDal.GetZoneByParentId(-1)
        //                           : db.ZoneMainDal.GetZoneByUserIdAndPermissionId(user.Id, permissionId);
        //            }
        //            return new List<ZoneEntity>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<ZoneEntity>();
        //    }
        //}

        public static List<ZoneEntity> GetListZoneByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                    if (!String.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);
                    return db.ZoneMainDal.GetZoneByUsernameAndPermissionIds(username, listPermissionId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListZoneActivedByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                    if (!String.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);
                    return db.ZoneMainDal.GetZoneActivedByUsernameAndPermissionIds(username, listPermissionId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetZoneByKeyword(keyword);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static WcfActionResponse Insert(ZoneEntity zoneEntity, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Insert(zoneEntity))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse Update(ZoneEntity zoneEntity, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Update_v2(zoneEntity))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse InsertV2(ZoneEntity zoneEntity, string username, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Insert_V2(zoneEntity, ref id))
                        {
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup(zoneEntity.ZoneIdList + zoneEntity.Type);
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        } public static WcfActionResponse InsertV3(ZoneEntity zoneEntity, string username, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Insert_V3(zoneEntity, ref id))
                        {
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup(zoneEntity.ZoneIdList + zoneEntity.Type);
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse UpdateV2(ZoneEntity zoneEntity, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Update_v2(zoneEntity))
                        {
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup(zoneEntity.ZoneIdList + zoneEntity.Type);
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup("GetDisplayHomePage");
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse UpdateV3(ZoneEntity zoneEntity, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.Update_v3(zoneEntity))
                        {
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup(zoneEntity.ZoneIdList + zoneEntity.Type);
                            CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup("GetDisplayHomePage");
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse MoveUp(int zoneId, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {

                        if (db.ZoneMainDal.MoveUp(zoneId))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse MoveDown(int zoneId, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.MoveDown(zoneId))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse UpdateInvisibled(int zoneId, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.UpdateInvisibled(zoneId))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse UpdateAllowComment(int zoneId, string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        if (db.ZoneMainDal.UpdateAllowComment(zoneId))
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse UpdateBasicSetting(ZoneBasicSettingEntity obj)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ZoneMainDal.UpdateBasicSetting(obj))
                    {
                        CacheObjectBase.GetInstance<ZoneCached>().RemoveAllCachedByGroup("GetDisplayHomePage");
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse ChangeStatus(int id, string username, int status)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    if (null != user && user.IsFullPermission && user.IsFullZone)
                    {
                        db.ZoneMainDal.ChangeStatus(id, username, status);
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateZoneNotAllow, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateZoneNotAllow]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse ParentUpdate(int zoneId, int parentId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ZoneMainDal.ParentUpdate(zoneId, parentId))
                    {
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    return WcfActionResponse.CreateSuccessResponse();

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static List<ZoneWithSimpleFieldEntity> BindAllOfZoneToTreeviewFullDepth(List<ZoneWithSimpleFieldEntity> allZones, string prefix)
        {
            allZones.OrderBy(item => item.ParentId).ThenBy(item => item.SortOrder);
            var outputZones = new List<ZoneWithSimpleFieldEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZones.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        public static List<ZoneWithSimpleFieldEntity> BindAllOfZoneToTreeviewFullDepth(List<ZoneEntity> allZones, string prefix)
        {
            var allZonesWithSimpleField = new List<ZoneWithSimpleFieldEntity>();
            var count = allZones.Count;
            foreach (var zone in allZones)
            {
                allZonesWithSimpleField.Add(new ZoneWithSimpleFieldEntity
                {
                    Id = zone.Id,
                    ParentId = zone.ParentId,
                    Name = zone.Name,
                    ShortUrl = zone.ShortUrl,
                    SortOrder = zone.SortOrder,
                    AllowComment = zone.AllowComment,
                    Status = zone.Status,
                    Invisibled = zone.Invisibled,
                    Domain = zone.Domain,
                    Avatar = zone.Avatar
                });
            }

            allZonesWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.SortOrder);
            var outputZones = new List<ZoneWithSimpleFieldEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZonesWithSimpleField.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        private static void BindAllOfZoneToTreeviewFullDepth(List<ZoneWithSimpleFieldEntity> allZones, int currentParentId, string prefix, ref List<ZoneWithSimpleFieldEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentId == currentParentId)
                {
                    allZones[i].Name = currentPrefix + " " + allZones[i].Name;
                    outputZones.Add(allZones[i]);
                    BindAllOfZoneToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }

        //public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeView(string username)
        //{
        //    return BindAllOfZoneToTreeviewFullDepth(GetListZoneByUsername(username), "--");
        //}

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeViewAndPermission(string username, int permissionId)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByUsernameAndPermissionIds(username, permissionId), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeViewAndPermission(string username, params int[] permissionIds)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneByUsernameAndPermissionIds(username, permissionIds), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActivedByUserWithTreeViewAndPermission(string username, params int[] permissionIds)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByUsernameAndPermissionIds(username, permissionIds), "--");
        }

        //public static List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(bool getParentZoneOnly)
        //{
        //    if (getParentZoneOnly)
        //    {
        //        return BindAllOfZoneToTreeviewFullDepth(GetListZoneActiveByParentId(0), "--");
        //    }
        //    else
        //    {
        //        return BindAllOfZoneToTreeviewFullDepth(GetAllZone(), "--");
        //    }
        //}
        //public static List<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly)
        //{
        //    if (getParentZoneOnly)
        //    {
        //        return BindAllOfZoneToTreeviewFullDepth(GetListZoneByParentId(0), "--");
        //    }
        //    else
        //    {
        //        return BindAllOfZoneToTreeviewFullDepth(GetAllZone(), "--");
        //    }
        //}
        public static IEnumerable<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly, int type)
        {
            if (getParentZoneOnly)
            {
                return BindAllOfZoneToTreeviewFullDepth(GetListZoneByParentId(0, type), "--");
            }

            return BindAllOfZoneToTreeviewFullDepth(GetAllZone(type), "--");

        }
        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActivedWithTreeView(bool getParentZoneOnly)
        {
            if (getParentZoneOnly)
            {
                return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByParentId(0), "--");
            }
            else
            {
                return BindAllOfZoneToTreeviewFullDepth(GetAllZoneActived(), "--");
            }
        }

        public static List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {
            List<ZoneDefaultTagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDefaultTagMainDal.GetAllDefaultTagForZone();
            }
            return returnValue;
        }

        public static List<ZoneEntity> GetGroupNewsZoneByParentId(int zoneId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetGroupNewsZoneByParentId(zoneId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static IEnumerable<ZoneSimpleFieldEntity> GetDisplayHomePage(int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.GetDisplayHomePage(type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneSimpleFieldEntity>();
            }
        }
        

        public static IEnumerable<ZoneEntity> SelectHeader( string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ZoneMainDal.SearchHeader(shortUrl,pageNumber,pageSize,ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
    }
}

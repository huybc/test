﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Comment;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Comment
{
    public class CommentBo
    {
        #region Gets
        public static CommentEntity GetById(int id)
        {
            try
            {
                CommentEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.CommentMainDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new CommentEntity();
            }
        }

        public static List<CommentEntity> Search(long objectId, EnumCommentObjectType objectType, EnumCommentStatus status)
        {
            try
            {
                List<CommentEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.CommentMainDal.Search((int)status, (int)objectType, objectId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<CommentEntity>();
            }
        }
        public static List<CommentEntity> SearchCommentWithPaging(long objectId, int objectType, int parentId, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                List<CommentEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.CommentMainDal.SearchCommentWithPaging(objectId, objectType, parentId, status, sortOrder, pageIndex, pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<CommentEntity>();
            }
        }

        public static int Count(long objectId, EnumCommentObjectType objectType, EnumCommentStatus status)
        {
            try
            {
                int returnValue = 0;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.CommentMainDal.CountComment((int)status, (int)objectType, objectId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }
        #endregion
        #region Sets
        public static WcfActionResponse Insert(CommentEntity CommentEntity, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CommentMainDal.Insert(CommentEntity, ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
               
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse Update(CommentEntity CommentEntity, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CommentMainDal.Update(CommentEntity, ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse ChangeStatus(int id, EnumCommentStatus status)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CommentMainDal.ChangeStatus(id, (int)status))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
    }
    #endregion
}

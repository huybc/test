﻿using System;
using System.Collections.Generic;
using Mi.BO.Base.News;
using Mi.BO.Base.Point;
using Mi.BO.Common;
using Mi.Common;
using Mi.Entity.Base.Answer;
using Mi.Entity.Base.Point;
using Mi.Entity.Base.UserAction;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Answer
{
    public class AnswerBo
    {
        public static AnswerEntity GetById(int id)
        {
            try
            {
                AnswerEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.AnswerMainDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new AnswerEntity();
            }
        }

        public static List<AnswerEntity> Search(long objectId, EnumAnswerObjectType objectType, EnumAnswerStatus status)
        {
            try
            {
                List<AnswerEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.AnswerMainDal.Search((int)status, (int)objectType, objectId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<AnswerEntity>();
            }
        }

        public static int Count(long objectId, EnumAnswerObjectType objectType, EnumAnswerStatus status)
        {
            try
            {
                int returnValue = 0;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.AnswerMainDal.CountAnswer((int)status, (int)objectType, objectId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        //public static WcfActionResponse Insert(AnswerEntity answerEntity, ref int id)
        //{
        //    try
        //    {
        //        var point = 0;

        //        point = Utility.ConvertToInt(ConfigBo.GetByConfigName("NewAnswer").ConfigValue);

        //        var news = BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(answerEntity.ObjectId, answerEntity.CreatedBy);
        //        //Lưu hành động

        //        using (var db = new CmsMainDb())
        //        {
        //            db.AnswerMainDal.Insert(answerEntity, ref id);
        //            var ac = new UserActionEntity
        //            {
        //                ActionType = (int)EnumUserActionType.PostAnswer,
        //                ActionValue = point,
        //                ObjectId = id,
        //                ObjectType = (int)EnumUserActionObjectType.Answer,
        //                UserName = answerEntity.CreatedBy,
        //                CreatedBy = answerEntity.CreatedBy,
        //                Point = point
        //            };
        //            db.UserActionMainDal.AddUserAction(ac);

        //            //Tăng/giảm điểm user nhận hành động
        //            PointBo.AddPoint(new PointEntity
        //            {
        //                Point = ac.Point,
        //                LastModifiedDate = DateTime.Now,
        //                UserName = answerEntity.CreatedBy,
        //                ZoneId = news.NewsInfo.ZoneId
        //            });
        //        }
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}
        public static WcfActionResponse Update(AnswerEntity answerEntity)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.AnswerMainDal.Update(answerEntity))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public static WcfActionResponse ChangeStatus(int id, EnumAnswerStatus status)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.AnswerMainDal.ChangeStatus(id, (int)status))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }
        public static WcfActionResponse SetIsCorrect(int id, long objectId, bool isCorrect)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.AnswerMainDal.SetIsCorrect(id, objectId, isCorrect))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public static WcfActionResponse VoteAnswer(long id, EnumAnswerVoteDirection voteDirection)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.AnswerMainDal.VoteAnswer(id, voteDirection))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

    }
}

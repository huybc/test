﻿using System;
using Mi.BO.Base.Answer;
using Mi.BO.Base.News;
using Mi.BO.Base.Point;
using Mi.BO.Common;
using Mi.Common;
using Mi.Entity.Base.Answer;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Point;
using Mi.Entity.Base.UserAction;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.UserAction
{
    public class UserActionBo
    {
        //public static WcfActionResponse VoteSolution(string createdBy, bool isUp, long objectId, int objectType)
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            if (db.UserActionMainDal.GetUserActionDetail(createdBy, objectId, objectType, (int)EnumUserActionType.VoteDown) == null &&
        //            db.UserActionMainDal.GetUserActionDetail(createdBy, objectId, objectType, (int)EnumUserActionType.VoteUp) == null)
        //            {
        //                var news = BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(objectId, createdBy);

        //                //Lấy điểm
        //                var point = Utility.ConvertToInt(ConfigBo.GetByConfigName("ThumbUp").ConfigValue);

        //                //Lưu hành động
        //                var ac = new UserActionEntity
        //                {
        //                    ActionType = isUp ? (int)EnumUserActionType.VoteUp : (int)EnumUserActionType.VoteDown,
        //                    ActionValue = isUp ? 1 : -1,
        //                    ObjectId = objectId,
        //                    ObjectType = objectType,
        //                    UserName = news.NewsInfo.CreatedBy,
        //                    CreatedBy = createdBy,
        //                    Point = isUp ? point : point * -1
        //                };
        //                db.UserActionMainDal.AddUserAction(ac);

        //                //Cập nhật số vote cho tin
        //                NewsBo.VoteNews(objectId, isUp ? EnumNewsVoteDirection.Up : EnumNewsVoteDirection.Down);

        //                //Tăng/giảm điểm user nhận hành động
        //                return PointBo.AddPoint(new PointEntity
        //                {
        //                    Point = ac.Point,
        //                    LastModifiedDate = DateTime.Now,
        //                    UserName = news.NewsInfo.CreatedBy,
        //                    ZoneId = news.NewsInfo.ZoneId
        //                });
        //            }
        //            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

        //    }
        //}
        //public static WcfActionResponse VoteAnswer(string createdBy, bool isUp, long objectId, int objectType)
        //{
        //    try
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            if (db.UserActionMainDal.GetUserActionDetail(createdBy, objectId,objectType,(int)
        //            EnumUserActionType.VoteDown) == null &&
        //            db.UserActionMainDal.GetUserActionDetail(createdBy, objectId, objectType,(int)
        //                EnumUserActionType.VoteUp) == null)
        //            {
        //                var ans = AnswerBo.GetById(Utility.ConvertToInt(objectId));

        //                //Lấy điểm
        //                var point = Utility.ConvertToInt(ConfigBo.GetByConfigName("ThumbUp").ConfigValue);

        //                //Lưu hành động
        //                var ac = new UserActionEntity
        //                {
        //                    ActionType = isUp ? (int)EnumUserActionType.VoteUp : (int)EnumUserActionType.VoteDown,
        //                    ActionValue = isUp ? 1 : -1,
        //                    ObjectId = objectId,
        //                    ObjectType = objectType,
        //                    UserName = ans.CreatedBy,
        //                    CreatedBy = createdBy,
        //                    Point = isUp ? point : point * -1
        //                };
        //                db.UserActionMainDal.AddUserAction(ac);

        //                //Cập nhật số vote cho tin
        //                AnswerBo.VoteAnswer(objectId, isUp ? EnumAnswerVoteDirection.Up : EnumAnswerVoteDirection.Down);

        //                var news = BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(ans.ObjectId, createdBy);
        //                //Tăng/giảm điểm user nhận hành động
        //                return PointBo.AddPoint(new PointEntity
        //                {
        //                    Point = ac.Point,
        //                    LastModifiedDate = DateTime.Now,
        //                    UserName = ans.CreatedBy,
        //                    ZoneId = news.NewsInfo.ZoneId
        //                });
        //            }
        //            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

        //    }
        //}

        public static UserActionEntity GetUserAction(string createdBy, long objectId, int objectType, EnumUserActionType actionType)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserActionMainDal.GetUserActionDetail(createdBy, objectId, objectType,(int)actionType);
        }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.Account;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Security
{
    public class PermissionBo
    {
        #region Business

        public static WcfActionResponse GrantPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (permissionId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

            }
            if (zoneId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);

            }
            return UpdateUserPermission(username, permissionId, zoneId);

        }

        public static WcfActionResponse GrantPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

            }

            if (permissionId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

            }

            if (zoneId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);

            }
            var userPermission = new UserPermissionEntity { UserId = userId, PermissionId = permissionId, ZoneId = zoneId };
            var returnData = UpdateUserPermission(userPermission);
            if (returnData.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userId);
            }
            return returnData;
        }

        public static WcfActionResponse GrantListPermission(string username, List<UserPermissionEntity> userPermissions)
        {
            var updateSuccess = true;
            try
            {
                RemoveAllUserPermissionByUserName(username);

                var permissionCount = userPermissions.Count;
                for (var i = 0; i < permissionCount; i++)
                {
                    var userPermission = userPermissions[i];
                    updateSuccess = updateSuccess &&
                                    (UpdateUserPermission(username, userPermission.PermissionId, userPermission.ZoneId).Success);
                }
                if (updateSuccess)
                {
                       CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(username);
                    return WcfActionResponse.CreateSuccessResponse();
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return WcfActionResponse.CreateSuccessResponse();

        }

        public static WcfActionResponse GrantListPermission(int userId, List<UserPermissionEntity> userPermissions)
        {
            var updateSuccess = true;

            RemoveAllUserPermissionByUserId(userId);

            var permissionCount = userPermissions.Count;
            for (var i = 0; i < permissionCount; i++)
            {
                var userPermission = userPermissions[i];
                userPermission.UserId = userId;
                updateSuccess = updateSuccess &&
                                (UpdateUserPermission(userPermission).Success);
            }
            if (updateSuccess)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userId);
                return WcfActionResponse.CreateSuccessResponse();

            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

        }

        public static WcfActionResponse IsUserHasPermissionInZone(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);
            }

            var allZoneGranted = ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(username, permissionId);
            if (allZoneGranted.Exists(item => item.Id == zoneId))
            {
                return WcfActionResponse.CreateSuccessResponse();
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);

            }
            // full quyền trên tất cả các chuyên mục
            if (user.IsFullPermission && user.IsFullZone)
            {
                return WcfActionResponse.CreateSuccessResponse();

            }
            var permission = GetPermissionById(permissionId);
            // không full quyền trên tất cả các chuyên mục
            if (!user.IsFullPermission && !user.IsFullZone)
            {
                if (permission != null)
                {
                    return permission.IsGrantByCategory
                               ? CheckUserPermission(username, permissionId, zoneId)
                               : CheckUserPermission(username, permissionId);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);
            }

            if (!permission.IsGrantByCategory && user.IsFullPermission)  return WcfActionResponse.CreateSuccessResponse();

            // Lấy danh sách quyền theo user
            var userPermissions = GetListByUserId(user.Id, true);
            int count = userPermissions.Count;
            // Nếu full quyền thì check chuyên mục xem có nằm trong danh sách quyền của user hay không
            if (user.IsFullPermission)
            {
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].ZoneId == zoneId)
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                }
            }
            else // Nếu full chuyên mục thì check quyền xem có nằm trong danh sách quyền của user hay không
            {
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].PermissionId == permissionId)
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                }
            }
            // Không có quyền thỏa mãn
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);
        }

        public static WcfActionResponse IsUserInGroupPermission(string username, int groupPermissionId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);

            }

            if (groupPermissionId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);

            }

            return CheckUserInGroupPermission(username, groupPermissionId);
        }

        #endregion
        public static WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, UserPermissionEntity[] userPermission, bool isFullPermission, bool isFullZone, string accountName)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            var user = UserBo.GetById(userId);
            user.IsFullPermission = isFullPermission;
            user.IsFullZone = isFullZone;
            var response = UserBo.Update(user, accountName);
            if (response.Success)
            {
                if (!isFullPermission || !isFullZone)
                {
                    var errorCode = PermissionBo.GrantListPermission(userId,
                                                                     new List<UserPermissionEntity>(userPermission));
                    if (errorCode.Success)
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public static WcfActionResponse UpdateUserPermission(UserPermissionEntity userPermission)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userPermission.UserId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                if (null == db.PermissionMainDal.GetPermissionById(userPermission.PermissionId))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);
                }

                var existsZone = db.ZoneMainDal.GetZoneById(userPermission.ZoneId);
                if (null == existsZone)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);
                }


                if (db.UserPermissionMainDal.UpdateUserPermission(userPermission))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userPermission.UserId);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static WcfActionResponse UpdateUserPermission(string username, int permissionId, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }

                var existsPermission = db.PermissionMainDal.GetPermissionById(permissionId);
                if (null == existsPermission)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

                }

                var existsZone = db.ZoneMainDal.GetZoneById(zoneId);
                if (null == existsZone)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);

                }
                var userPermission = new UserPermissionEntity
                {
                    UserId = existsUser.Id,
                    PermissionId = permissionId,
                    ZoneId = zoneId
                };


                if (db.UserPermissionMainDal.UpdateUserPermission(userPermission))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }

        }
        public static WcfActionResponse RemoveUserPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (permissionId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);
            }

            if (zoneId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);

            }
            var userPermission = new UserPermissionEntity
            {
                UserId = userId,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userPermission.UserId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }
                if (db.UserPermissionMainDal.RemoveUserPermission(userPermission))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static WcfActionResponse RemoveUserPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            if (permissionId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

            }

            if (zoneId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);
            }
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                var userPermission = new UserPermissionEntity
                {
                    UserId = existsUser.Id,
                    PermissionId = permissionId,
                    ZoneId = zoneId
                };

                if (db.UserPermissionMainDal.RemoveUserPermission(userPermission))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }

        }
        public static WcfActionResponse RemoveAllUserPermissionByUserId(int userId)
        {

            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }

                if (db.UserPermissionMainDal.RemoveAllUserPermission(existsUser.Id))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static WcfActionResponse RemoveAllUserPermissionByUserName(string username)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }

                if (db.UserPermissionMainDal.RemoveAllUserPermission(existsUser.Id))
                {
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse CheckUserPermission(UserPermissionEntity userPermission)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userPermission.UserId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                if (existsUser.IsFullPermission)
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }

                if (db.UserPermissionMainDal.CheckUserPermission(userPermission))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }
        public static WcfActionResponse CheckUserPermission(string username, int permissionId, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                if (existsUser.IsFullPermission)
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                var userPermission = new UserPermissionEntity
                {
                    UserId = existsUser.Id,
                    PermissionId = permissionId,
                    ZoneId = zoneId
                };
                if (db.UserPermissionMainDal.CheckUserPermission(userPermission))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }


        }

        public static WcfActionResponse CheckUserPermission(string username, int permissionId)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse(
                        (int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound,
                        ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }
                if (existsUser.IsFullPermission)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Success,
                        ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);

                }
                if (GetListByUsernameAndPermissionId(username, permissionId).Any())
                {

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);


            }
        }

        public static WcfActionResponse CheckUserInGroupPermission(string username, int groupId)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(username);
                if (null == existsUser)
                {

                    return WcfActionResponse.CreateErrorResponse(
                     (int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound,
                     ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                if (existsUser.IsFullPermission)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                if (db.GroupPermissionMainDal.CheckUserInGroupPermission(existsUser.Id, groupId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }

        }

        public static PermissionEntity GetPermissionById(int permissionId)
        {
            using (var db = new CmsMainDb())
            {
                return db.PermissionMainDal.GetPermissionById(permissionId);
            }

        }
        public static List<PermissionEntity> GetListByGroupId(int groupPermissionId)
        {
            using (var db = new CmsMainDb())
            {
                return db.PermissionMainDal.GetListPermissionByGroupId(groupPermissionId);
            }
        }
        public static List<PermissionEntity> GetListByTemplateId(int permissionTemplateId)
        {
            using (var db = new CmsMainDb())
            {
                return db.PermissionMainDal.GetListPermissionByTemplateId(permissionTemplateId);
            }
        }

        public static List<PermissionEntity> GetPermissionByUsername(string username)
        {
            string filterInListPermissionId = Convert.ToInt32(EnumPermission.ArticleReporter) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleEditor) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleAdmin);
            using (var db = new CmsMainDb())
            {
                return db.PermissionMainDal.GetListPermissionByUsername(username, filterInListPermissionId);
            }
        }

        public static List<UserPermissionEntity> GetListByUserId(int userId, bool isGetChildZone)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUserId(userId, isGetChildZone);
            }
        }
        public static List<UserPermissionEntity> GetListByUserName(string username)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUserName(username);
            }
        }
        public static List<UserPermissionEntity> GetListByUserIdAndZoneId(int userId, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUserIdAndZoneId(userId, zoneId);
            }
        }
        public static List<UserPermissionEntity> GetListByUsernameAndZoneId(string username, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUsernameAndZoneId(username, zoneId);
            }
        }
        public static List<UserPermissionEntity> GetListByUserIdAndPermissionId(int userId, int permissionId)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUserIdAndPermissionId(userId, permissionId);
            }
        }
        public static List<UserPermissionEntity> GetListByUsernameAndPermissionId(string username, int permissionId)
        {
            using (var db = new CmsMainDb())
            {
                return db.UserPermissionMainDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);
            }
        }

        public static List<PermissionTemplateEntity> GetAllPermissionTemplate()
        {
            using (var db = new CmsMainDb())
            {
                return db.PermissionTemplateMainDal.GetAllPermissionTemplate();
            }
        }
        public static List<GroupPermissionEntity> GetAllPermissionGroup()
        {
            using (var db = new CmsMainDb())
            {
                return db.GroupPermissionMainDal.GetAllGroupPermission();
            }

        }

        public static List<GroupPermissionDetailEntity> GetAllPermissionGroupDetail()
        {
            var groupPermissionDetails = new List<GroupPermissionDetailEntity>();
            var groupPermissions = GetAllPermissionGroup();

            groupPermissionDetails.AddRange(
                groupPermissions.Select(groupPermission => new GroupPermissionDetailEntity
                {
                    Id = groupPermission.Id,
                    Name = groupPermission.Name,
                    PermissionList = GetListByGroupId(groupPermission.Id),
                }));

            return groupPermissionDetails;
        }



        #region PermissionTemplate
        public static List<PermissionTemplateEntity> PermissionTemplateGetAll()
        {
            return GetAllPermissionTemplate();
        }
        public static WcfActionResponse PermissionTemplateUpdate(PermissionTemplateEntity template)
        {
            using (var db = new CmsMainDb())
            {
                if (db.PermissionTemplateMainDal.UpdatePermissionTemplate(template))

                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }
        public static WcfActionResponse PermissionTemplateDelete(int templateId)
        {
            using (var db = new CmsMainDb())
            {
                if (db.PermissionTemplateMainDal.DeletePermissionTemplate(templateId))

                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static List<PermissionTemplateDetailEntity> GetListPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            using (var db = new CmsMainDb())
            {
                return db.PermissionTemplateMainDal.GetListPermissionTemplateDetailByTemplateId(templateId, isGetChildZone);
            }
        }
        public static WcfActionResponse PermissionTemplateDetailUpdate(int templateId, List<PermissionTemplateDetailEntity> permissionTemplates)
        {
            var updateSuccess = true;
            RemoveAllPermissionTemplateDetailByPermissionTemplateId(templateId);
            var permissionCount = permissionTemplates.Count;

            using (var db = new CmsMainDb())
            {

                for (var i = 0; i < permissionCount; i++)
                {

                    var permissionTemplate = permissionTemplates[i];
                    permissionTemplate.TemplateId = templateId;
                    updateSuccess = updateSuccess &&
                                    (db.PermissionTemplateMainDal.PermissionTemplateDetailUpdate(permissionTemplate));
                }
            }
            return WcfActionResponse.CreateSuccessResponse();
        }
        public static WcfActionResponse PermissionTemplateDetailUpdate(PermissionTemplateDetailEntity permissionTemplate)
        {

            var existsPermission = GetPermissionById(permissionTemplate.PermissionId);
            if (null == existsPermission)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

            }

            var existsZone = ZoneBo.GetZoneById(permissionTemplate.ZoneId);
            if (null == existsZone)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound]);

            }
            using (var db = new CmsMainDb())
            {
                if (db.PermissionTemplateMainDal.PermissionTemplateDetailUpdate(permissionTemplate))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }


        }
        public static WcfActionResponse RemoveAllPermissionTemplateDetailByPermissionTemplateId(int templateId)
        {
            using (var db = new CmsMainDb())
            {
                if (db.PermissionTemplateMainDal.DeleteAllPermissionTemplateDetail(templateId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }

        }

        public static PermissionTemplateWithPermissionDetailEntity GetPermissionTemplateWithPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {

            var templateWithPermissionDetailEntity = new PermissionTemplateWithPermissionDetailEntity
            {
                AllGroupPermission = PermissionBo.GetAllPermissionGroupDetail(),
                AllParentZone = ZoneBo.GetListParentZoneActivedByParentId(),
                PermissionTemplateDetailList = PermissionBo.GetListPermissionDetailByTemplateId(templateId, isGetChildZone)
            };
            return templateWithPermissionDetailEntity;
        }
        #endregion

    }
}

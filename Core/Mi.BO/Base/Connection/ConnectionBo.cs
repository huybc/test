﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Connection;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Connection
{
    public class ConnectionBo
    {
        public static ConnectionEntity GetConnectionByConnectionId(string connectionId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ConnectionMainDal.GetConnectionByConnectionId(connectionId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ConnectionEntity();
            }
        }
        public static List<ConnectionEntity> GetConnectionByUserName(string userName)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ConnectionMainDal.GetConnectionByUserName(userName);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ConnectionEntity>();
            }
        }
        public static WcfActionResponse Update(ConnectionEntity zoneEntity)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ConnectionMainDal.Update(zoneEntity))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
    }
}

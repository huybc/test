﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mi.Common;
using Mi.Entity.Base.FileUpload;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Base.FileUpload;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.FileUpload
{
    public class FileUploadBo
    {
        #region Update

        public static WcfActionResponse InsertFileUpload(FileUploadEntity fileUpload, ref int newFileUploadId)
        {
            try
            {
                if (null == fileUpload)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);
                }
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.InsertFileUpload(fileUpload, ref newFileUploadId) ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);

            }
        }
        public static WcfActionResponse UpdateFileUpload(FileUploadEntity fileUpload)
        {
            try
            {
                if (null == fileUpload)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest, ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]);

                }
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.UpdateFileUpload(fileUpload) ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);


                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        public static WcfActionResponse DeleteFileUpload(int fileUploadId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.DeleteFileUpload(fileUploadId) ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }

        #endregion

        #region Get

        public static FileUploadEntity GetFileUploadById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.GetFileUploadById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static IEnumerable<FileUploadEntity> SearchFileUpload(string keyword, int parentId, string uploadedBy, EnumFileUploadType type, EnumFileUploadStatus status, string ext, EnumFileSortOrder sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.SearchFileUpload(keyword, parentId, uploadedBy, type, status, ext, sortOrder, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FileUploadEntity>();
            }
        }
        public static IEnumerable<FolderEntity> FoldersSearch()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.FoldersSearch();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FolderEntity>();
            }
        }
        public static int FolderCheckIsExist(int id, string keyword, int parentId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.FileUploadMainDal.FolderCheckIsExist(id, keyword, parentId).Count();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }

        #endregion
    }
}

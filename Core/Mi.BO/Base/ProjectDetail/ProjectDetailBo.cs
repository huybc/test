﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.ProjectDetail;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.ProjectDetail
{
    public class ProjectDetailBo
    {
        public static List<ProjectDetailEntity> GetByArticleId(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.ProjectDetailDal.GetByArticleId(id);
            }
        }
        
        public static WcfActionResponse Create(ProjectDetailEntity obj,ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ProjectDetailDal.Create(obj,ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse Delete( long id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ProjectDetailDal.Delete(id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

      
    }
}

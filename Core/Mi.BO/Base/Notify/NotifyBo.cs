﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Notify;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Notify
{
    public class NotifyBo
    {
        public static List<NotifyEntity> NotifySearch(string username, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            using (var db = new CmsMainDb())
            {
                return db.NotifyMainDal.NotifySearch(username, status, pageIndex, pageSize, ref totalRows);
            }
        }
        public static List<NotifyEntity> GetNotifyByStatus(string userName, EnumNotifyStatus status)
        {
            using (var db = new CmsMainDb())
            {
                return db.NotifyMainDal.GetNotifyByStatus(userName, (int)status);
            }
        }

        public static int CountNotify(string userName, EnumNotifyStatus status)
        {
            using (var db = new CmsMainDb())
            {
                return db.NotifyMainDal.Count(userName, (int)status);
            }
        }

        public static WcfActionResponse AddNotify(NotifyEntity notify)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.NotifyMainDal.AddNotify(notify))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse NotifyUpdateStatus(NotifyEntity notify)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.NotifyMainDal.NotifyUpdateStatus(notify))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse NotifyUpdateStatus_SetAll(string userName, int statusInput, int statusOutput)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.NotifyMainDal.NotifyUpdateStatus_SetAll(userName, statusInput, statusOutput))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public static WcfActionResponse MarkAllRead(string userName)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.NotifyMainDal.MarkAllRead(userName))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
    }
}

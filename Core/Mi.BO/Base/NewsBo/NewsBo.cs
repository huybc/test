﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.Account;
using Mi.BO.Base.Point;
using Mi.BO.Base.Zone;
using Mi.BO.Common;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Point;
using Mi.Entity.Base.Security;
using Mi.Entity.Base.Tag;
using Mi.Entity.Base.UserAction;
using Mi.Entity.Base.ViewCount;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;
using Mi.WcfExtensions;

namespace Mi.BO.Base.News
{

    public class NewsBo
    {

        public static long SetPrimaryId()
        {
            return long.Parse(DateTime.Now.ToString(BoConstants.NEWS_FORMAT_ID));
        }

        #region NewsExtension

        public static NewsExtensionEntity GetNewsExtensionValue(long newsId, EnumNewsExtensionType type)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsExtensionMainDal.GetValue(newsId, (int)type);
            }
        }

        public static int GetNewsExtensionMaxValue(long newsId, int type)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsExtensionMainDal.GetMaxValue(newsId, (int)type);
            }
        }
        public static WcfActionResponse SetNewsExtensionValue(long newsId, EnumNewsExtensionType type, string value)
        {
            using (var db = new CmsMainDb())
            {
                if (db.NewsExtensionMainDal.SetValue(newsId, (int)type, value))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse DeleteNewsExtensionByNewsId(long newsId)
        {
            using (var db = new CmsMainDb())
            {
                if (db.NewsExtensionMainDal.DeleteByNewsId(newsId))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsExtensionMainDal.GetByNewsId(newsId);
            }
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType type, string value)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsExtensionMainDal.GetByTypeAndVaue((int)type, value);
            }
        }

        #endregion

        #region Tag news

        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsId(long newsId)
        {
            using (var db = new CmsMainDb())
            {
                var listTagNews = db.TagNewsMainDal.GetTagNewsByNewsId(newsId);
                return listTagNews.Select(tagNews => new TagNewsWithTagInfoEntity
                {
                    TagId = tagNews.TagId,
                    NewsId = tagNews.NewsId,
                    TagMode = tagNews.TagMode,
                    Priority = tagNews.Priority,
                    TagProperty = tagNews.TagProperty,
                    Name = tagNews.Name,
                    Url = tagNews.Url
                }).ToList();
            }

        }

        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByListOfTagId(string tagIds)
        {
            using (var db = new CmsMainDb())
            {
                var listTagNews = db.TagNewsMainDal.GetTagNewsWithTagInfoByListOfTagId(tagIds);
                return listTagNews.Select(tagNews => new TagNewsWithTagInfoEntity
                {
                    TagId = tagNews.TagId,
                    NewsId = tagNews.NewsId,
                    TagMode = tagNews.TagMode,
                    Priority = tagNews.Priority,
                    TagProperty = tagNews.TagProperty,
                    Name = "",
                    Url = ""
                }).ToList();
            }
        }

        #endregion

        private static readonly IDictionary<string, List<string>> DictsNewsTitleUpdating = new Dictionary<string, List<string>>();

        #region Check news title updating

        private static bool IsUpdatingThisNews(string newsTitle)
        {
            if (string.IsNullOrEmpty(newsTitle)) return false;

            var accountName = WcfMessageHeader.Current.ClientUsername;
            return (DictsNewsTitleUpdating.ContainsKey(accountName) && DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle));
        }
        private static void UpdatingThisNews(string newsTitle)
        {
            var accountName = WcfMessageHeader.Current.ClientUsername;
            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                if (!DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle))
                {
                    DictsNewsTitleUpdating[accountName].Add(newsTitle);
                }
            }
            else
            {
                DictsNewsTitleUpdating.Add(accountName, new List<string> { newsTitle });
            }
        }
        private static void NewsUpdated2(string newsTitle)
        {
            var accountName = WcfMessageHeader.Current.ClientUsername;
            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                var currentList = DictsNewsTitleUpdating[accountName];
                if (currentList != null)
                {
                    var index = currentList.FindIndex(item => item == newsTitle);
                    while (index >= 0)
                    {
                        currentList.RemoveAt(index);
                        index = currentList.FindIndex(item => item == newsTitle);
                    }
                }
                DictsNewsTitleUpdating[accountName] = currentList;
            }
        }

        #endregion

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:00 
        /// </summary>
        /// <param name="news">NewsEntity</param>
        /// <param name="zoneId">int</param>
        /// <param name="zoneIdList">string</param>
        /// <param name="tagIdList">string</param>
        /// <param name="tagIdListForPrimary">string</param>
        /// <param name="newsRelationIdList">string</param>
        /// <param name="usernameForUpdateAction">string</param>
        /// <param name="newNewsId">ref long</param>
        /// <param name="newEncryptNewsId">ref string</param>
        /// <param name="authorList">List of author info</param>
        /// <returns></returns>
        public static WcfActionResponse InsertNews(NewsEntity news, int zoneId, string tagIdList, string usernameForUpdateAction, ref long newsId, ref string urlReturn)
        {
            try
            {

                if (null == news || string.IsNullOrEmpty(news.Title))
                {

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle]);
                }

                if (zoneId <= 0)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }

                //var existsNews = NewsDal.GetNewsForValidateById(news.Id);
                var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsForValidateById(news.Id);
                if (null != existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsDuplicateId, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsDuplicateId]);

                }
                //var existsZone = ZoneDal.GetZoneById(zoneId);
                var existsZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                if (null == existsZone)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }

                /* Build link theo primary zone */
                //var primaryZone = ZoneDal.GetZoneById(zoneId);
                var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                if (null == primaryZone)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }
                // urlReturn = news.Url = BuildLinkUrl(news.Type, primaryZone.ShortUrl, string.IsNullOrEmpty(news.Url) ? news.Title : news.Url, zoneId);
                using (var db = new CmsMainDb())
                {

                    var inserted = db.NewsMainDal.InsertNews(news, zoneId, tagIdList, ref newsId);
                    if (inserted)
                    {

                        CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(newsId.ToString());

                        return WcfActionResponse.CreateSuccessResponse();

                    }

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                //NewsUpdated(news.Title);
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse InsertNewsV2(NewsEntity news, string zoneIds, string tagIdList, string usernameForUpdateAction, ref long newsId, ref string urlReturn)
        {
            try
            {

                if (null == news || string.IsNullOrEmpty(news.Title))
                {

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle]);
                }




                if (zoneIds.Length <= 0)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }




                //var existsNews = NewsDal.GetNewsForValidateById(news.Id);
                var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsForValidateById(news.Id);
                if (null != existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsDuplicateId, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsDuplicateId]);

                }
                //var existsZone = ZoneDal.GetZoneById(zoneId);
                //var existsZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                //if (null == existsZone)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                //}

                /* Build link theo primary zone */
                //var primaryZone = ZoneDal.GetZoneById(zoneId);
                //var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                //if (null == primaryZone)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                //}
                using (var db = new CmsMainDb())
                {

                    var inserted = db.NewsMainDal.InsertNewsV2(news, zoneIds, tagIdList, ref newsId);
                    if (inserted)
                    {

                        CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(newsId.ToString());

                        return WcfActionResponse.CreateSuccessResponse();

                    }

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                //NewsUpdated(news.Title);
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:06 
        /// </summary>
        /// <param name="news"></param>
        /// <param name="zoneId"></param>
        /// <param name="zoneIdList"></param>
        /// <param name="tagIdList"></param>
        /// <param name="tagIdListForPrimary"></param>
        /// <param name="newsRelationIdList"></param>
        /// <param name="usernameForUpdateAction"></param>
        /// <param name="isRebuildLink"></param>
        /// <returns></returns>
        public static WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string tagIdList, string usernameForUpdateAction, bool isRebuildLink, ref string urlReturn)
        {
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit]);
            }
            if (null == news || news.Id <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
            }

            if (zoneId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

            }

            //var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsById(news.Id);
            using (var db = new CmsMainDb())
            {
                var existsNews = db.NewsMainDal.GetNewsById(news.Id);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);

                }
                //var zones = GetNewsInZoneByNewsId(news.Id);
                //if (zones.Count > 0)
                //{
                //    existsNews.ZoneId = zones.FirstOrDefault().ZoneId;
                //}
                //var user = CacheObjectBase.GetInstance<UserCached>().GetUserByUsername(usernameForUpdateAction);
                //if (null == user) return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);


                //var userPermissionsForUpdater = CacheObjectBase.GetInstance<PermissionCached>().GetListByUserName(usernameForUpdateAction);

                //var isFullpermission = user.IsFullPermission || user.IsFullZone;
                //var isAdmin = IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                //    existsNews.ZoneId + "", user);
                //var isBtv = IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor,
                //                      existsNews.ZoneId + "", user) || IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter,
                //                      existsNews.ZoneId + "", user);

                //if (!(isFullpermission || isAdmin || isBtv))
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit]);
                //}

                //var existsZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                //if (null == existsZone)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                //}

                //var newsRelationList = "";

                //var tagList = TagBo.GetTagByListOfTagId(tagIdList);


                existsNews.Author = news.Author;

                existsNews.Avatar = news.Avatar;
                existsNews.Url = news.Url;
                existsNews.DistributionDate = news.DistributionDate;
                existsNews.Sapo = news.Sapo;
                existsNews.Title = news.Title;
                existsNews.Body = news.Body;
                existsNews.LastModifiedBy = news.LastModifiedBy;
                existsNews.LastModifiedDate = news.LastModifiedDate;
                existsNews.WordCount = news.WordCount;
                existsNews.Source = news.Source;
                existsNews.IsVideo = news.IsVideo;
                existsNews.Type = news.Type;
                existsNews.NewsType = news.NewsType;
                if (existsNews.Status != (int)NewsStatus.Published)
                    existsNews.Status = news.Status;
                existsNews.IsOnMobile = news.IsOnMobile;
                existsNews.SourceURL = news.SourceURL;
                existsNews.MetaDescription = news.MetaDescription;
                existsNews.MetaKeyword = news.MetaKeyword;
                existsNews.TitleSeo = news.TitleSeo;
                existsNews.PublishedBy = news.PublishedBy;
                existsNews.HoursOfWork = news.HoursOfWork;
                existsNews.Invest = news.Invest;
                existsNews.SurfaceArea = news.SurfaceArea;
                existsNews.Budget = news.Budget;
                existsNews.GroupId = news.GroupId;
                existsNews.Size = news.Size;
                existsNews.Avatar2 = news.Avatar2;
                /* Build link theo primary zone */
                //var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                //if (null == primaryZone)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                //}
                //if (news.Status == (int)NewsStatus.Published)
                //{
                //    existsNews.PublishedBy = news.PublishedBy;

                //    if (isRebuildLink)
                //        urlReturn = existsNews.Url = BuildLinkUrl(news.Type, primaryZone.ShortUrl, string.IsNullOrEmpty(news.Url) ? news.Title : news.Url, zoneId);
                //}
                //else
                //    urlReturn = existsNews.Url = BuildLinkUrl(news.Type, primaryZone.ShortUrl, string.IsNullOrEmpty(news.Url) ? news.Title : news.Url, zoneId);

                if (db.NewsMainDal.UpdateNews(existsNews, zoneId, tagIdList))
                {


                    if (existsNews.Status == (int)NewsStatus.Published)
                    {
                        db.NewsMainDal.ChangeStatusToPublished(existsNews.Id, usernameForUpdateAction, news.DistributionDate);

                    }

                    CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(news.Id.ToString());
                    // Kết thúc log
                    //   SearchEngine.RLucene.AddUpdateLuceneIndex(existsNews);
                    return WcfActionResponse.CreateSuccessResponse();

                }
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

        }
        public static WcfActionResponse UpdateNewsV2(NewsEntity news, string zoneIds, string tagIdList, string usernameForUpdateAction, bool isRebuildLink, ref string urlReturn)
        {
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit]);
            }
            if (null == news || news.Id <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
            }


            //var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsById(news.Id);
            using (var db = new CmsMainDb())
            {
                var existsNews = db.NewsMainDal.GetNewsById(news.Id);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);

                }
                var zones = GetNewsInZoneByNewsId(news.Id);
                if (zones.Count > 0)
                {
                    existsNews.ZoneId = zones.FirstOrDefault().ZoneId;
                }
                var user = CacheObjectBase.GetInstance<UserCached>().GetUserByUsername(usernameForUpdateAction);
                if (null == user) return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);


                var userPermissionsForUpdater = CacheObjectBase.GetInstance<PermissionCached>().GetListByUserName(usernameForUpdateAction);

                var isFullpermission = user.IsFullPermission || user.IsFullZone;
                var isAdmin = IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                    existsNews.ZoneId + "", user);
                var isBtv = IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor,
                                      existsNews.ZoneId + "", user) || IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter,
                                      existsNews.ZoneId + "", user);

                if (!(isFullpermission || isAdmin || isBtv))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit]);
                }



                //var newsRelationList = "";

                //var tagList = TagBo.GetTagByListOfTagId(tagIdList);


                existsNews.Author = news.Author;

                existsNews.Avatar = news.Avatar;
                existsNews.DistributionDate = news.DistributionDate;
                existsNews.Sapo = news.Sapo;
                existsNews.Title = news.Title;
                existsNews.Body = news.Body;
                existsNews.LastModifiedBy = news.LastModifiedBy;
                existsNews.LastModifiedDate = news.LastModifiedDate;
                existsNews.WordCount = news.WordCount;
                existsNews.Source = news.Source;
                existsNews.Type = news.Type;
                existsNews.NewsType = news.NewsType;
                if (existsNews.Status != (int)NewsStatus.Published)
                    existsNews.Status = news.Status;
                existsNews.IsOnMobile = news.IsOnMobile;
                existsNews.SourceURL = news.SourceURL;
                existsNews.MetaDescription = news.MetaDescription;
                existsNews.MetaKeyword = news.MetaKeyword;
                existsNews.TitleSeo = news.TitleSeo;

                if (db.NewsMainDal.UpdateNewsV2(existsNews, zoneIds, tagIdList))
                {


                    if (existsNews.Status == (int)NewsStatus.Published)
                    {
                        db.NewsMainDal.ChangeStatusToPublished(existsNews.Id, usernameForUpdateAction, news.DistributionDate);

                    }

                    CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(news.Id.ToString());
                    // Kết thúc log
                    //   SearchEngine.RLucene.AddUpdateLuceneIndex(existsNews);
                    return WcfActionResponse.CreateSuccessResponse();

                }
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

        }

        public static WcfActionResponse ChangeStatusToPublished(long newsId, string publishedBy, DateTime publishedDate)
        {
            using (var db = new CmsMainDb())
            {
                var newsInfo = db.NewsMainDal.GetNewsById(newsId);
                if (null == newsInfo || newsInfo.Id <= 0)
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                try
                {
                    var result = db.NewsMainDal.ChangeStatusToPublished(newsId, publishedBy, publishedDate);
                    return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

                }

            }
        }


        public static WcfActionResponse ChangeStatusToReject(long id, string publishedBy)
        {
            using (var db = new CmsMainDb())
            {

                var result = db.NewsMainDal.ChangeStatusToReject(id, publishedBy);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }

        }
        public static WcfActionResponse ChangeStatusComment(long id, string modifyBy, byte status)
        {

            using (var db = new CmsMainDb())
            {

                var result = db.NewsMainDal.ChangeStatusComment(id, modifyBy, status);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }


        }
        public static WcfActionResponse ChangeStatusToWaitPublish(long id, string modifyBy)
        {

            using (var db = new CmsMainDb())
            {
                var result = db.NewsMainDal.ChangeStatusToWaitPublish(id, modifyBy);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }


        }
        public static WcfActionResponse ChangeStatusToUnpublished(long id, string lastModifiedBy)
        {
            using (var db = new CmsMainDb())
            {
                var result = db.NewsMainDal.ChangeStatusToUnpublished(id, lastModifiedBy);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }

        }

        public static WcfActionResponse ChangeStatusToMovedToTrash(long id, string lastModifiedBy)
        {

            using (var db = new CmsMainDb())
            {
                var result = db.NewsMainDal.ChangeStatusToMovedToTrash(id, lastModifiedBy);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public WcfActionResponse ChangeStatusToRestoreFromTrash(long id, string lastModifiedBy)
        {

            using (var db = new CmsMainDb())
            {
                var result = db.NewsMainDal.ChangeStatusToRestoreFromTrash(id, lastModifiedBy);
                return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }

        }
        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:09 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="avatar"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        //public static WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position)
        //{
        //    var newsInfo = NewsDal.GetNewsById(newsId);
        //    if (null == newsInfo || newsInfo.Id <= 0)
        //        return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
        //    switch (position)
        //    {
        //        //case 0:
        //        //case 1:
        //        default:
        //            newsInfo.Avatar = avatar;
        //            break;
        //    }
        //    var result = false;
        //    try
        //    {
        //        result = NewsDal.UpdateNewsAvatar(newsInfo.Id, newsInfo.Avatar);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
        //    }
        //    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        //}




        public static WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position)
        {
            var responseData = new WcfActionResponse();
            using (var db = new CmsMainDb())
            {
                var newsInfo = db.NewsMainDal.GetNewsById(newsId);

                if (null == newsInfo || newsInfo.Id <= 0)
                {
                    responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                    return responseData;
                }
                var errorCode = db.NewsMainDal.UpdateNewsAvatar(newsInfo.Id, newsInfo.Avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                return errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }



        }
        //public static WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position)
        //{
        //    var responseData = new WcfActionResponse();
        //    var newsInfo = NewsDal.GetNewsById(newsId);
        //    if (null == newsInfo || newsInfo.Id <= 0)
        //    {
        //        responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
        //        return responseData;

        //    }

        //    switch (position)
        //    {

        //        default:
        //            newsInfo.Avatar = avatar;
        //            break;
        //    }

        //    try
        //    {
        //        var result = NewsDal.UpdateNewsAvatar(newsInfo.Id, newsInfo.Avatar);
        //        var errorCode = result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        //        responseData = errorCode == ErrorMapping.ErrorCodes.Success
        //                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "1")
        //                                   : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
        //    }
        //    return responseData;
        //}

        public static WcfActionResponse UpdateNewsIsAnswered(long newsId, string account, bool isAnswered)
        {

            using (var db = new CmsMainDb())
            {
                var newsInfo = db.NewsMainDal.GetNewsById(newsId);
                if (null == newsInfo || newsInfo.Id <= 0)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                }

                if (newsInfo.CreatedBy != account)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ValidAccountNotHavePermission, ErrorMapping.Current[ErrorMapping.ErrorCodes.ValidAccountNotHavePermission]);
                }

                try
                {
                    var result = db.NewsMainDal.UpdateNewsIsAnswered(newsInfo.Id, isAnswered);

                    var errorCode = result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                    return errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }

                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:08 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="usernameForUpdateAction"></param>
        /// <returns></returns>
        public static WcfActionResponse Delete(long newsId, string usernameForUpdateAction)
        {
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete]);

            }
            using (var db = new CmsMainDb())
            {
                if (newsId <= 0)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                }

                var existsNews = db.NewsMainDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);

                }

                /* Chỉ được quyền xóa khi tin ở trạng thái "Xóa tạm (thùng rác)" và người xóa là người cập nhật cuối (người xóa tạm) 
                 */
                if (
                    !((existsNews.Status == (int)NewsStatus.MovedToTrash &&
                       existsNews.LastModifiedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase))))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete]);

                }

                if (db.NewsMainDal.DeleteNews(newsId))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
        }

        public static WcfActionResponse VoteNews(long id, EnumNewsVoteDirection voteDirection)
        {
            using (var db = new CmsMainDb())
            {
                if (db.NewsMainDal.VoteNews(id, voteDirection))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }

        public static WcfActionResponse UpdateOnlyTitle(long newsId, string newsTitle, string accountName)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var newsInfo = db.NewsMainDal.GetNewsById(newsId);
                    if (newsInfo == null) return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                    else
                    {
                        // Get Zone Primary to build URL for news
                        var zonePrimary = db.ZoneMainDal.GetPrimaryZoneByNewsId(newsId);
                        var newsUrl = BuildLinkUrl(newsInfo.Type, zonePrimary.ShortUrl, newsTitle, newsInfo.ZoneId);
                        // Update Title for this news
                        if (db.NewsMainDal.UpdateOnlyNewsTitle(newsId, newsTitle, newsUrl, accountName))
                            return WcfActionResponse.CreateSuccessResponse();
                        else
                            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                }
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse UpdateDetail(long newsId, string newsTitle, string sapo, string body, string accountName)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var newsInfo = db.NewsMainDal.GetNewsById(newsId);
                    if (newsInfo == null) return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                    else
                    {
                        // Get Zone Primary to build URL for news
                        var zonePrimary = db.ZoneMainDal.GetPrimaryZoneByNewsId(newsId);
                        var newsUrl = BuildLinkUrl(newsInfo.Type, zonePrimary.ShortUrl, newsTitle, newsInfo.ZoneId);
                        // Update Title for this news
                        if (db.NewsMainDal.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName))
                            return WcfActionResponse.CreateSuccessResponse();
                        else
                            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                }
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var newsInfo = db.NewsMainDal.GetNewsById(newsId);
                    if (newsInfo == null) return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound]);
                    else
                    {
                        if (db.NewsMainDal.UpdateDisplayPosition(newsId, displayPosition, accountName))
                            return WcfActionResponse.CreateSuccessResponse();
                        else
                            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    }
                }
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        // #endregion

        #region Status flow

        public static WcfActionResponse Unpublish(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);

            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish]);

            }
            using (var db = new CmsMainDb())
            {
                // Kiểm tra tin cần chuyển trạng thái
                var existsNews = db.NewsMainDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);

                }
                // Trạng thái hiện tại không được phép gỡ xuất bản
                //if (existsNews.Status != (int)NewsStatus.Published)
                //{
                //    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish]);

                //}
                // Kiểm tra người xử lý
                var user = UserBo.GetUserByUsername(userForUpdateAction);
                // Không tìm thấy người xử lý
                if (null == user)
                {

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish]);

                }
                // Không phải là người xuất bản bài này => Không được gỡ bài
                //if (!existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                //{
                //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish;
                //}
                // Lấy quyền của người xử lý
                var userPermissionsForUpdater = db.UserPermissionMainDal.GetListUserPermissionByUserName(userForUpdateAction);
                // Người xử lý không có quyền thư ký trong chuyên mục của bài này => không được phép gỡ xuất bản
                if (!(user.IsFullPermission ||
                      (db.GroupPermissionMainDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin) &&
                       user.IsFullZone) ||
                      IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                      existsNews.ListZoneId, user)))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToPublish, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToPublish]);
                }
                try
                {
                    // Gỡ xuất bản
                    if (db.NewsMainDal.ChangeStatusToUnpublished(newsId, userForUpdateAction))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    else
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.WriteLog(Logger.LogType.Trace, exception.ToString());
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
                }
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static WcfActionResponse MoveToTrash(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);

            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash]);

            }
            // Kiểm tra tin cần chuyển trạng thái
            using (var db = new CmsMainDb())
            {
                var existsNews = db.NewsMainDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);

                }
                // Trạng thái hiện tại không được phép xóa tạm (xóa vào thùng rác)
                //if (existsNews.Status != (int)NewsStatus.Temporary &&
                //    existsNews.Status != (int)NewsStatus.Unpublished)
                //{
                //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
                //}
                // Kiểm tra người xử lý
                var user = UserBo.GetUserByUsername(userForUpdateAction);
                // Không tìm thấy người xử lý
                if (null == user)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash]);

                }
                if (existsNews.Status == (int)NewsStatus.Temporary)
                {
                    if (!existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash]);
                    }
                }
                if (
                    existsNews.Status == (int)NewsStatus.Unpublished)
                {
                    // Lấy quyền của người nhận
                    var userPermissionsForUpdater = db.UserPermissionMainDal.GetListUserPermissionByUserName(userForUpdateAction);
                    if (!(existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase) ||
                        IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user)))
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash]);

                    }
                }

                if (db.NewsMainDal.ChangeStatusToMovedToTrash(newsId, userForUpdateAction))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static WcfActionResponse ReceiveFromTrash(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash]);

            }
            using (var db = new CmsMainDb())
            {
                // Kiểm tra tin cần chuyển trạng thái
                var existsNews = db.NewsMainDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound]);
                }
                // Trạng thái hiện tại không được phép phục hồi tin đã xóa
                if (existsNews.Status != (int)NewsStatus.MovedToTrash)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash]);
                }
                // Kiểm tra người xử lý
                var user = UserBo.GetUserByUsername(userForUpdateAction);
                if (null == user)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash]);

                }
                if (!existsNews.LastModifiedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash, ErrorMapping.Current[ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash]);

                }

                if (db.NewsMainDal.ChangeStatusToRestoreFromTrash(newsId, existsNews.LastModifiedBy))
                {
                    if (existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                    {
                    }
                    else if (existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                    {
                    }

                    return WcfActionResponse.CreateSuccessResponse();
                }

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        #endregion

        #region Get news

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:23
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="currentUsername"></param>
        /// <returns></returns>
        public static NewsDetailForEditEntity GetDetail(long newsId, string currentUsername)
        {
            var newsVersionId = 0L;
            var newsDetail = new NewsDetailForEditEntity();

            newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(currentUsername, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin), "--");
            newsDetail.NewsInfo = (newsId < 0 ? null : GetNewsByNewsId(newsId, currentUsername, ref newsVersionId));

            using (var db = new CmsMainDb())
            {
                if (null != newsDetail.NewsInfo)
                {
                    var viewCountObj = db.ViewCountMainDal.GetByObjectIdAndObjectType(newsId, (int)EnumObjectType.TblNews);
                    newsDetail.NewsInfo.ViewCount = viewCountObj != null ? viewCountObj.ViewCount : 0;
                    // Zone
                    if (newsId > 0)
                    {
                        newsDetail.NewsInZone = GetNewsInZoneByNewsId(newsId);
                        newsDetail.TagInNews = GetTagNewsWithTagInfoByNewsId(newsId);

                        if (newsDetail.NewsInZone.Count > 0)
                        {
                            newsDetail.NewsInfo.ZoneId = newsDetail.NewsInZone.FirstOrDefault().ZoneId;
                        }
                    }
                }
            }

            newsDetail.NewsContentWithTemplate = newsDetail.NewsInfo != null ? newsDetail.NewsInfo.Body : "";

            return newsDetail;
        }
        public static NewsEntity GetNewsByNewsId(long id, string lastModifiedBy, ref long newsVesionId)
        {
            newsVesionId = 0;
            if (id > 0)
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetNewsById(id);
                }
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="zoneIdList"></param>
        /// <returns></returns>
        public static List<NewsInZoneEntity> GetNewsInZoneByZoneIdList(long newsId, string zoneIdList)
        {
            var newsInZones = new List<NewsInZoneEntity>();
            if (!string.IsNullOrEmpty(zoneIdList))
            {
                var zoneIds = zoneIdList.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                var isPrimary = true;
                foreach (var zoneId in zoneIds)
                {
                    newsInZones.Add(new NewsInZoneEntity()
                    {
                        NewsId = newsId,
                        ZoneId = Utility.ConvertToInt(zoneId),
                        IsPrimary = isPrimary
                    });
                    isPrimary = false;
                }
            }
            return newsInZones;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="tagIdListForPrimary"></param>
        /// <param name="tagIdList"></param>
        /// <returns></returns>
        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfo(long newsId, string tagIdListForPrimary,
                                                                            string tagIdList)
        {
            var tagNews = new List<TagNewsWithTagInfoEntity>();

            var tagIdsForPrimary = (!string.IsNullOrEmpty(tagIdListForPrimary)
                                        ? tagIdListForPrimary.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                                        : new string[] { });
            var index = 0;
            foreach (var tagIdForPrimary in tagIdsForPrimary)
            {
                tagNews.Add(new TagNewsWithTagInfoEntity()
                {
                    NewsId = newsId,
                    TagId = Utility.ConvertToInt(tagIdForPrimary),
                    Priority = index,
                    TagMode = (int)EnumTagModes.TagForNewsSubtitle,
                    TagProperty = ""
                });
                index++;
            }

            var tagIds = (!string.IsNullOrEmpty(tagIdList)
                              ? tagIdList.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                              : new string[] { });
            index = 0;
            foreach (var tagId in tagIds)
            {
                tagNews.Add(new TagNewsWithTagInfoEntity()
                {
                    NewsId = newsId,
                    TagId = Utility.ConvertToInt(tagId),
                    Priority = index,
                    TagMode = (int)EnumTagModes.TagForNews,
                    TagProperty = ""
                });
                index++;
            }
            return tagNews;
        }

        public virtual NewsEntity GetNewsByNewsId(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsById(id);
            }
        }

        public static List<NewsWithAnalyticEntity> GetNewsByListNewsId(NewsStatus status, string listNewsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsByListNewsId((int)status, listNewsId);
            }
        }

        public static List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsDistributionDateByListNewsId(listNewsId);
            }
        }


        public static List<NewsInListEntity> GetNewsByTagIdWithPaging(long tagId,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, EnumNewsIsAnswered isAnswered, int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetNewsByTagIdWithPaging(tagId, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, (int)status, (int)newsType, (int)isAnswered, pageIndex, pageSize,
                                                      ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsInListEntity> SearchNews(string keyword, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        int status,
                                                        int newsType, int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var news = new List<NewsInListEntity>();
            try
            {

                using (var db = new CmsMainDb())
                {
                    var objs = db.NewsMainDal.SearchNews(keyword, username, zoneIds, fromDate, toDate,
                                                    (int)filterFieldForUsername,
                                                    (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize,
                                                    ref totalRow).ToList();

                    foreach (var obj in objs)
                    {
                        var viewCountObj = db.ViewCountMainDal.GetByObjectIdAndObjectType(obj.Id, (int)EnumObjectType.TblNews);
                        obj.ViewCount = viewCountObj != null ? viewCountObj.ViewCount : 0;
                        news.Add(obj);
                    }
                }

                return news;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return news;
        }
        public static IEnumerable<NewsInListEntity> FESearchNews(string keyword, string username, string zoneIds,
                                                     DateTime fromDate,
                                                     DateTime toDate,
                                                     NewsFilterFieldForUsername filterFieldForUsername,
                                                     NewsSortExpression sortOrder,
                                                     NewsStatus status,
                                                     int newsType, EnumNewsIsAnswered isAnswered, int pageIndex, int pageSize,
                                                     ref int totalRow)
        {
            var news = new List<NewsInListEntity>();
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.FESearchNews(keyword, username, zoneIds, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, (int)status, newsType, (int)isAnswered, pageIndex, pageSize,
                                                      ref totalRow);


                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return news;
        }
        public static List<NewsInListEntity> SearchNewsByUsername(string keyword, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate, int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                List<NewsInListEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.NewsMainDal.SearchNewsByUsername(keyword, username, zoneIds, fromDate, toDate,
                        pageIndex, pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static List<NewsInListEntity> SearchNewsByStatus(string keyword, string usernameDoAction, string usernameUpdateNews, int filterFieldForUsernameUpdateNews, string zoneIds, DateTime fromDate, DateTime toDate, int type, int status, bool isGetTotalRow, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByStatus(keyword, usernameDoAction, usernameUpdateNews,
                    filterFieldForUsernameUpdateNews, zoneIds, fromDate, toDate, type, status, isGetTotalRow,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="displayPosition"></param>
        /// <param name="distributedDateFrom"></param>
        /// <param name="distributedDateTo"></param>
        /// <param name="excludeNewsIds"></param>
        /// <param name="newsType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                List<NewsInListEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.NewsMainDal.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                List<NewsInListEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.NewsMainDal.SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<NewsInListEntity> SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
            string excludePositionTypes,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                List<NewsInListEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.NewsMainDal.SearchNewsWhichPublishedForNewsPosition(zoneId, keyword, type,
                        displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType,
                        excludePositionTypes, pageIndex, pageSize, ref totalRow);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsInZoneMainDal.GetNewsInZoneByNewsId(newsId);
            }
        }


        public static List<NewsEntity> SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status,
                                                       int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByTag(tagIds, tagNames, searchBytag, status, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTag:{0}", ex.Message));
            }
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.SearchNewsPublishExcludeNewsInTag(zoneId, keyword, tagId, pageIndex, pageSize, ref totalRow);
            }
        }
        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.SearchNewsPublishExcludeNewsInThread(zoneId, keyword, threadId, pageIndex, pageSize, ref totalRow);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByFocusKeyword(keyword, top);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> GetMostedViewNews()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetMostedViewNews();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> GetTopNewestNews()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetTopNewestNews();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> GetTop4VideoNews()
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetTop4VideoNews();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> SearchNewsByTagId(long tagId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByTagId(tagId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTagId:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByTagIdWithPaging(tagId, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTagIdWithPaging:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByThreadIdWithPaging(threadId, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByThreadIdWithPaging:{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="role"></param>
        /// <param name="commonStatus"></param>
        /// <returns></returns>
        public static List<NewsCounterEntity> CounterNewsByStatus(string accountName, int role, string commonStatus)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var userInfo = db.UserMainDal.GetUserByUsername(accountName);
                    var isByZone = !userInfo.IsFullZone;

                    var zoneIds = string.Empty;
                    if (isByZone)
                    {
                        var zoneList = db.ZoneMainDal.GetZoneByUsername(accountName);
                        if (zoneList != null && zoneList.Count > 0)
                        {
                            zoneIds = zoneList.Aggregate(zoneIds, (current, zoneEntity) => current + ("," + zoneEntity.Id));
                            if (!string.IsNullOrEmpty(zoneIds)) zoneIds = zoneIds.Remove(0, 1);
                        }
                    }
                    return db.NewsMainDal.CounterNewsByStatus(accountName, zoneIds, role, commonStatus, isByZone);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: FOX
        /// LastModifiedDate: 2013-08-07 14:35
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public static List<NewsCounterEntity> GetNewsCounterByStatus(string accountName, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                var counters = db.NewsMainDal.GetCounter(accountName, zoneId);
                foreach (var status in Enum.GetValues(typeof(NewsStatus)))
                {
                    if (counters.FindIndex(item => item.Status == (int)status) < 0)
                    {
                        counters.Add(new NewsCounterEntity
                        {
                            Status = (int)status,
                            TotalRow = 0
                        });
                    }
                }
                return counters;
            }

        }

        /// <summary>
        /// Get dánh sách các bài viết để tính nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public static List<NewsForRoyaltiesEntity> GetRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetPRRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectPRRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate,
                                                               string creator, int author, int pageIndex, int pageSize, ref int totalRows,
                                                               int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRows, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyaltiesV2:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetRoyaltiesPRV2(string zoneId, DateTime startDate, DateTime endDate,
                                                              string creator, int author, int pageIndex, int pageSize, ref int totalRows,
                                                              int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectPRRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRows, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyaltiesV2:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Count tổng số bản ghi
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <returns></returns>
        public static int CountRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }

        public static int CountBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }

        public static int CountPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountPRRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountPRRoyalties:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Get tổng tien nhuan but
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <returns></returns>
        public static decimal GetTotalPrice(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                            string author, string newsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SelectTotalPrice(zoneId, startDate, endDate, creator, author, newsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectTotalPrice:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Set giá tiền cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Giá tiền</param>
        /// <returns></returns>
        public static WcfActionResponse SetPrice(long id, decimal price)
        {
            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdatePrice(id, price);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bonusPrice"></param>
        /// <returns></returns>
        public static WcfActionResponse SetBonusPrice(long id, decimal bonusPrice)
        {
            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdateBonusPrice(id, bonusPrice);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isOnHome"></param>
        /// <returns></returns>
        public static WcfActionResponse UpdateIsOnHome(long id, bool isOnHome)
        {

            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdateIsOnHome(id, isOnHome);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public static WcfActionResponse UpdateIsFocus(long id, bool isFocus)
        {

            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdateIsFocus(id, isFocus);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayInSlide"></param>
        /// <returns></returns>
        public static WcfActionResponse UpdateDisplayInSlide(long id, int displayInSlide)
        {

            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdateDisplayInSlide(id, displayInSlide);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }

        /// <summary>
        /// Set ghi chú khi chấm nhuận bút cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="note">Ghi chú</param>
        /// <returns></returns>
        public static WcfActionResponse SetNoteRoyalties(long id, string note)
        {

            using (var db = new CmsMainDb())
            {
                var isSuccess = db.NewsMainDal.UpdateNoteRoyalties(id, note);
                return isSuccess ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }

        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsForValidateById(id);
            }
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsInfoForCachedById(id);
            }
        }

        #endregion


        #region private members

        public static bool IsHasPermission(List<UserPermissionEntity> userPermissions,
                                            EnumPermission permissionToCheck, string listZoneId, UserEntity user)
        {
            if (user.IsFullPermission && user.IsFullZone) return true;

            if (user.IsFullPermission && userPermissions.Count != 0)
            {
                listZoneId = "," + listZoneId + ",";
                var count = userPermissions.Count;
                for (var i = 0; i < count; i++)
                {
                    if (listZoneId.IndexOf("," + userPermissions[i].ZoneId + ",") >= 0)
                    {
                        return true;
                    }
                }
            }

            if (user.IsFullZone && userPermissions.Count != 0)
            {
                listZoneId = "," + listZoneId + ",";
                var count = userPermissions.Count;
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].PermissionId == (int)permissionToCheck)
                    {
                        return true;
                    }
                }
            }

            if (userPermissions.Count != 0)
            {
                listZoneId = "," + listZoneId + ",";
                var count = userPermissions.Count;
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].PermissionId == (int)permissionToCheck &&
                        listZoneId.IndexOf("," + userPermissions[i].ZoneId + ",") >= 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        //public static string BuildLinkUrl(long newsId, string zoneUrl, string newsTitle)
        //{
        //    try
        //    {
        //        var formatUrl = AppSettings.GetString(BoConstants.NEWS_FORMAT_URL);
        //        var titleUnsignAndSlash = Utility.ConvertTextToLink(newsTitle, "-");
        //        return titleUnsignAndSlash != "" ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, newsId) : string.Empty;
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }
        //}

        public static string BuildLinkUrl(int newsType, string zoneUrl, string newsTitle, int zoneId)
        {
            try
            {
                string formatUrl;
                switch (newsType)
                {
                    default:
                        formatUrl = BoConstants.NewsUrlFormat;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL);
                        break;
                }
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                if (formatUrl.IndexOf("ca{3}") > 0)
                {
                    return titleUnsignAndSlash != ""
                               ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId)
                               : string.Empty;
                }
                else
                {
                    return titleUnsignAndSlash != ""
                               ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash)
                               : string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string BuildLinkUrlAdStore(long newsId, string zoneUrl, int zoneId, string newsTitle, string channelNamespace = "")
        {
            try
            {
                string formatUrl = string.IsNullOrEmpty(channelNamespace)
                                    ? BoConstants.NewsUrlFormatForAdStore :
                                      BoConstants.NewsUrlFormatForAdStoreByNamespace(channelNamespace);
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId, newsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string BuildLinkUrlAdStore(long newsId, string zoneUrl, int zoneId, string newsTitle)
        {
            try
            {
                string formatUrl = BoConstants.NewsUrlFormatForAdStore;
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId, newsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion

        #region NewsStats By OriginalId

        public static int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {

            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetViewCountByOriginalId(zoneIds, fromDate, toDate, originalId);
            }
        }
        public static List<NewsInListEntity> SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchNewsByOriginalId(zoneIds, fromDate, toDate, originalId, pageIndex, pageSize,
                                                          ref totalRow);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        #endregion

        #region Update like count
        public static WcfActionResponse UpdateLikeCount(long newsId, int likeCount)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.UpdateLikeCount(newsId, likeCount)
                          ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                //return data ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.SearchNewsByDateRange(startDate, endDate, status);
            }
        }
        #endregion

        public static int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountNewsBySource(sourceId, fromDate, toDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountNewsBySource:{0}", ex.Message));
            }
        }
        public static int CountNewsByZone(int zoneId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountNewsByZone(zoneId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountNewsByZone:{0}", ex.Message));
            }
        }
        public static int CountNewsByType(NewsType type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.CountNewsByType((int)type);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountNewsByType:{0}", ex.Message));
            }
        }
        public virtual NewsEntity GetNewsByTitle(string title)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsByTitle(title);
            }
        }

        public static NewsEntity GetNewsDetailById(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.GetNewsById(id);
            }
        }

  public static NewsEntity CMS_News_GetNewsDetailAndZoneById(long id)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsMainDal.CMS_News_GetNewsDetailAndZoneById(id);
            }
        }



        public static IEnumerable<NewsEntity> Search(string title, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.Search(title, sortOrder, pageIndex, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }
        public static IEnumerable<NewsEntity> SearchByShortUrl(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.SearchByShortUrl(shortUrl, pageNumber, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }  public static IEnumerable<NewsEntity> uspSectNewsInTreeWithZone(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.uspSectNewsInTreeWithZone(shortUrl, pageNumber, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }
        public static IEnumerable<NewsEntity> uspSectNewsByProductType(string shortUrl, int pageNumber, int pageSize, ref int totalRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.uspSectNewsByProductType(shortUrl, pageNumber, pageSize, ref totalRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }
        public static IEnumerable<NewsEntity> GetByParentId(int parentId, int type)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetByParentId(parentId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }
        public static IEnumerable<NewsEntity> GetByZoneId(int zoneId, int numOfRow)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.NewsMainDal.GetByZoneId(zoneId, numOfRow);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsEntity>();
            }
        }
    }
}

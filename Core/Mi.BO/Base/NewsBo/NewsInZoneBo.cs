﻿using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.News
{
    public class NewsInZoneBo
    {
        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.NewsInZoneMainDal.GetNewsInZoneByNewsId(newsId);
            }
        }
        public static WcfActionResponse InsertNewsInZone(NewsInZoneEntity newsInZone)
        {
            using (var db = new CmsMainDb())
            {
               return db.NewsInZoneMainDal.Insert(newsInZone)
                       ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }
    }
}

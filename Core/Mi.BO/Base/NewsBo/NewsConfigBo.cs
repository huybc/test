﻿using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.News
{
    public class ConfigBo
    {
        public static ConfigEntity GetByConfigName(string configName)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetByConfigName(configName);
            }
        }
        public static List<ConfigEntity> GetAll()
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetAll();
            }
        }
        public static IEnumerable<ConfigEntity> GetAll(string type, bool isEnable)
        {
          
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetAll(type, isEnable);

            }
        }
        public static List<ConfigEntity> GetListConfigForStaticHtmlTemplate(string groupConfigKey, EnumStaticHtmlTemplateType type)
        {

            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetListConfigByGroupKey(groupConfigKey, (int)type);
            }
        }
        public static List<ConfigEntity> GetListConfigByGroupKey(string groupConfigKey)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetListConfigByGroupKey(groupConfigKey, 0);
            }
        }
        public static List<ConfigEntity> GetListConfigByPage(string page, int type)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.GetListConfigByPage(page, type);
            }
        }
        public static WcfActionResponse Update(ConfigEntity obj)
        {

            using (var db = new CmsMainDb())
            {

                if (db.ConfigMainDal.Update(obj))
                {
                    CacheObjectBase.GetInstance<ConfigCached>().RemoveAllCachedByGroup(obj.ConfigName);
                  return  WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int) ErrorMapping.ErrorCodes.UnknowError,
                    ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static WcfActionResponse SetValue(string configName, string configValue, string configLabel)
        {
            using (var db = new CmsMainDb())
            {
                CacheObjectBase.GetInstance<ConfigCached>().RemoveAllCachedByGroup(configName);
                return db.ConfigMainDal.SetValue(configName, configValue, configLabel) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse SetValueForStaticHtmlTemplate(string configName, string configLabel, string configValue, EnumStaticHtmlTemplateType configType)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.SetValueAndLabel(configName, configLabel, configValue, (int)configType) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static AdvEntity AdvGetById(int Id)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.AdvGetById(Id);
            }
        }
        public static IEnumerable<AdvEntity> AdvGetByType(int type)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.AdvGetByType(type);
            }
        }
        public static IEnumerable<AdvEntity> AdvGetByType(int type,bool isEnable =true)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.AdvGetByType(type).Where(it=>it.IsEnable);
            }
        }
        public static WcfActionResponse AdvDelete(int obj)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.AdvDelete(obj) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }
        public static WcfActionResponse AdvUpdate(AdvEntity obj)
        {
            using (var db = new CmsMainDb())
            {
                return db.ConfigMainDal.AdvUpdate(obj) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse AdvInsert(AdvEntity obj)
        {
            using (var db = new CmsMainDb())
            {
              
                return db.ConfigMainDal.AdvInsert(obj) ? WcfActionResponse.CreateSuccessResponse()
                          : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
        }
    }
}

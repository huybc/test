﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base;
using Mi.Entity.Base.Province;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Cadastral
{
    public class CadastralBo
    {
        public static IEnumerable<ProvinceEntity> ProvinceGetAll()
        {
            using (var db = new CmsMainDb())
            {
                return db.CadastralMainDal.ProvinceGetAll();
            }
        }

        public static IEnumerable<DisctrictEntity> DistrictGetByProvinceCode(string provinceCode)
        {
            using (var db = new CmsMainDb())
            {
                return db.CadastralMainDal.DistrictGetByProvinceCode(provinceCode);
            }
        }
        public static ProvinceEntity GetProvinceByCode(string code)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.CadastralMainDal.GetProvinceByCode(code);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ProvinceEntity();
            }
        }
        public static DisctrictEntity GetDistrictByCode(string code)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.CadastralMainDal.GetDistrictByCode(code);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new DisctrictEntity();
            }
        }
    }
}

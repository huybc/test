﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Customer
{
    public class CustomerBo
    {
        public static IEnumerable<CustomerEntity> Search(string name, string phone,string type, int pageIndex, int pageSize,
            ref int totalRows)
        {
            try
            {
                IEnumerable<CustomerEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.CustomerDal.Search(name, phone,type, pageIndex, pageSize, ref totalRows);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<CustomerEntity>();
            }
        }
        public static CustomerEntity GetById(int id)
        {
            using (var db = new CmsMainDb())
            {
                return db.CustomerDal.GetById(id);
            }
        }
        
        public static WcfActionResponse Create(CustomerEntity obj,ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CustomerDal.Create(obj,ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse Update(CustomerEntity obj,ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CustomerDal.Update(obj,ref id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse Delete( int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.CustomerDal.Delete(id))
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

      
    }
}

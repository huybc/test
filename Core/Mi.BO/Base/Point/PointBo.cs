﻿using System.Collections.Generic;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.Point;

namespace Mi.BO.Base.Point
{
    public class PointBo
    {
        public static int GetPointByZone(string userName, int zoneId)
        {
            return CacheObjectBase.GetInstance<PointCached>().GetPointByZone(userName, zoneId);
        }

        public static List<PointEntity> GetTopUserByZone(int zoneId, int top,bool allowSelectExperience)
        {
            return CacheObjectBase.GetInstance<PointCached>().GetTopUserByZone(zoneId, top, allowSelectExperience);
        }

        public static int GetTotalPoint(string username)
        {
            return CacheObjectBase.GetInstance<PointCached>().GetTotalPoint(username);
        }

        public static WcfActionResponse AddPoint(PointEntity point)
        {
            return CacheObjectBase.GetInstance<PointCached>().AddPoint(point);
        }
    }
}

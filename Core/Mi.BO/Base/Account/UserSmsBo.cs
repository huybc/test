﻿using System;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Account
{
    public class UserSmsBo
    {
        public static void Insert(string userName, string smsCode, DateTime expiredDate)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(userName);
                    if (user != null)
                        db.UserSmsMainDal.Insert(user.Id, smsCode, expiredDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.Insert:{0}", ex.Message));
            }
        }

        public static int CheckValidateCode(string userName, string smsCode)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.UserSmsMainDal.CheckValidateCode(userName, smsCode);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.CheckValidateCode:{0}", ex.Message));
            }
        }

        public static void GetSmsCode(string userName, ref string smsCode, ref string phoneNumber)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    db.UserSmsMainDal.GetSmsCode(userName, ref smsCode, ref phoneNumber);
            }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.GetSmsCode:{0}", ex.Message));
            }
        }
    }
}

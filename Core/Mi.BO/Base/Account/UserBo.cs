﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.Security;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Account
{
    public class UserBo
    {
        #region SET
        /// <summary>
        /// Expired on 5 minutes
        /// </summary>
        private const int SmsExpired = 5;

        public static WcfActionResponse AddNew(UserEntity user, ref int newUserId, ref string newEncryptUserId)
        {

            if (null == user)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername]);
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername]);
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword]);
            }

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus]);
            }

            using (var db = new CmsMainDb())
            {
                if (null != db.UserMainDal.GetUserByUsername(user.UserName))
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUsernameExists, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUsernameExists]);
                }
                user.Password = Crypton.Encrypt(user.Password);
                try
                {
                    var errorCode = db.UserMainDal.AddnewUser(user, ref newUserId);
                    if (errorCode)
                    {
                        newEncryptUserId = CryptonForId.EncryptId(newUserId);
                        return WcfActionResponse.CreateSuccessResponse();
                    }

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }



        }

        public static WcfActionResponse Update(UserEntity user, string accountName)
        {
            if (null == user)
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }

            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidMobile;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus]);

            }

            try
            {
                user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);


                using (var db = new CmsMainDb())
                {
                    var existsUser = db.UserMainDal.GetUserById(user.Id);
                    if (null == existsUser)
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                    }
                    // Nếu không update password thì lấy password cũ
                    if (string.IsNullOrEmpty(user.Password))
                    {
                        user.Password = existsUser.Password;
                    }
                    var accountLogin = GetUserByUsername(accountName);
                    if (accountLogin == null)
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);


                    var existsAccount = GetById(user.Id);
                    if (existsAccount.IsSystem && !accountLogin.IsSystem)
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem]);

                    var success = db.UserMainDal.UpdateUserById(user);
                    var returnValue = success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                    if (success)
                    {
                        CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(user.Id);
                        CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(user.Id);
                    }
                    return returnValue;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static DateTime MinDateTime = new DateTime(1980, 1, 1);
        public static WcfActionResponse UpdateProfile(UserEntity user, string accountName)
        {
            if (null == user)
            {

                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

            }

            var accountLogin = GetUserByUsername(user.UserName);
            if (accountLogin == null)
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound]);

            user.Id = CryptonForId.DecryptIdToInt(accountLogin.EncryptId);

            // Nếu không update password thì lấy password cũ
            if (string.IsNullOrEmpty(user.Password))
            {
                user.Password = accountLogin.Password;
            }
            accountLogin.Id = user.Id;
            accountLogin.Address = user.Address;
            accountLogin.Avatar = user.Avatar;
            if (user.Birthday > MinDateTime) accountLogin.Birthday = user.Birthday;
            accountLogin.FullName = user.FullName;
            accountLogin.Email = user.Email;
            accountLogin.Mobile = user.Mobile;
            accountLogin.ModifiedDate = DateTime.Now;
            accountLogin.Password = user.Password;
            accountLogin.Status = user.Status;
            accountLogin.Description = user.Description;
            accountLogin.Skype = user.Skype;
            accountLogin.Website = user.Website;
            accountLogin.ZoneIdList = user.ZoneIdList;
            try
            {
                using (var db = new CmsMainDb())
                {
                    var returnValue = db.UserMainDal.UpdateUserById(accountLogin);
                    return returnValue ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse Delete(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
            }
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }



                if (db.UserMainDal.DeleteUserById(userId))
                {
                    CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }

        public static WcfActionResponse UpdateAvatar(string userName, string avatar)
        {
            using (var db = new CmsMainDb())
            {
                if (db.UserMainDal.UpdateUserAvatar(userName, avatar))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse UpdateStatus(string userName, int status)
        {


            using (var db = new CmsMainDb())
            {
                if (db.UserMainDal.UpdateUserStatus(userName, status))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse UpdateActiveCode(string userName, string activeCode)
        {
            using (var db = new CmsMainDb())
            {
                if (db.UserMainDal.UpdateActiveCode(userName, activeCode))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }

        public static WcfActionResponse ChangeStatus(int userId, UserStatus status)
        {

            using (var db = new CmsMainDb())
            {
                var returnValue = db.UserMainDal.GetUserById(userId);

                if (null == returnValue)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }
                if (db.UserMainDal.UpdateUserStatusByById(userId, (int)status))
                {
                    CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(returnValue.Id);
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(returnValue.Id);
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }

        public static WcfActionResponse ChangePassword(int userId, string oldPassword, string newPassword, string accountNameLogin)
        {
            try
            {
                //   var existsUser = UserDal.GetUserById(userId);
                // UserEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    UserEntity existsUser = db.UserMainDal.GetUserById(userId);

                    if (null == existsUser)
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                    }

                    oldPassword = Crypton.Encrypt(oldPassword);
                    if (existsUser.Password != oldPassword)
                    {
                        return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword]);

                    }
                    var accountLogin = GetUserByUsername(accountNameLogin);
                    // Nếu account đăng nhập ko phải account hệ thống
                    // và Account được reset pass là account hệ thống
                    // Thì ko được sửa pass
                    if (accountLogin == null || (!accountLogin.IsSystem && existsUser.IsSystem))
                        return
                            WcfActionResponse.CreateErrorResponse(
                                (int)ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem,
                                ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword]);


                    if (db.UserMainDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword)))
                    {
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse ResetPassword(int userId, string newPassword)
        {

            UserEntity existsUser;
            using (var db = new CmsMainDb())
            {
                existsUser = db.UserMainDal.GetUserById(userId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }

                if (db.UserMainDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword)))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static WcfActionResponse ResetMyPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            var errorCode = UserBo.ResetPassword(accountName, newPassword, cfNewPassword, oldPassword);
            if (errorCode.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public static WcfActionResponse ResetPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword]);


            if (string.IsNullOrEmpty(oldPassword))
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword]);

            if (string.IsNullOrEmpty(cfNewPassword))
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidRetypePassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidRetypePassword]);
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserByUsername(accountName);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }

                newPassword = Crypton.Encrypt(newPassword);
                oldPassword = Crypton.Encrypt(oldPassword);

                if (existsUser.Password != oldPassword)
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword]);

                if (db.UserMainDal.UpdateUserPasswordByById(existsUser.Id, newPassword))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }


        }
        #endregion SET
        #region GET
        public static UserEntity GetUserByUserNameAndActiveCode(string userName, string activeCode)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(activeCode))
            {
                return null;
            }
          
            using (var db = new CmsMainDb())
            {
                var user = db.UserMainDal.GetUserByUserNameAndActiveCode(userName, activeCode);
                return user;
            }
           
        }

        public static UserEntity GetById(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserById(userId);
                if (null != returnValue)
                {
                    returnValue.EncryptId = CryptonForId.EncryptId(returnValue.Id);
                    returnValue.Id = 0;
                }
            }
            return returnValue;
        }

        public static UserEntity GetById(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserById(userId);
                if (null != returnValue)
                {
                    returnValue.EncryptId = CryptonForId.EncryptId(returnValue.Id);
                    returnValue.Id = 0;
                }
            }
            return returnValue;

        }

        public static UserEntity GetBySocial(string socialId, byte loginType, string email)
        {

            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserBySocialId(socialId, loginType, email);
            }
            return returnValue;

        }

        public static UserEntity GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }
               
                using (var db = new CmsMainDb())
                {
                    var user = db.UserMainDal.GetUserByUsername(username);
                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }


        public static UserEntity GetUserByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    return null;
                }
                UserEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.UserMainDal.GetUserByEmail(email);
                    if (null != returnValue)
                    {
                        returnValue.EncryptId = CryptonForId.EncryptId(returnValue.Id);
                        returnValue.Id = 0;
                    }

                }
                return returnValue;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(int userId)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var obj = db.UserMainDal.GetUserById(userId);
                    if (null == obj)
                    {
                        return null;
                    }
                    else
                    {

                        var userWithPermission = new UserWithPermissionEntity
                        {
                            User = obj,
                            UserPermissions = db.UserPermissionMainDal.GetListUserPermissionByUserId(userId, true)
                        };
                        return userWithPermission;
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserName(string username)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var obj = db.UserMainDal.GetUserByUsername(username);
                    if (null == obj)
                    {
                        return null;
                    }
                    else
                    {

                        var userWithPermission = new UserWithPermissionEntity
                        {
                            User = obj,
                            UserPermissions = db.UserPermissionMainDal.GetListUserPermissionByUserId(obj.Id, true)
                        };
                        return userWithPermission;
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }


        public static List<MemberEntity> MemberSearch(string keyword, string userName, int status, int timeFilter, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {

            try
            {
                List<MemberEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.UserMainDal.MemberSearch(keyword, userName, status, timeFilter, sortOrder, pageIndex, pageSize,
                        ref totalRow);
                }
                return returnValue;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MemberEntity>();
            }
        }
        public static List<UserStandardEntity> SearchUser(string keyword,
                                                            UserStatus status,
                                                            UserSortExpression sortOrder,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            List<UserEntity> users;
            using (var db = new CmsMainDb())
            {
                users = db.UserMainDal.SearchUser(keyword, (int)status, (int)sortOrder, pageIndex, pageSize, ref totalRow);
                if (null != users && users.Count > 0)
                {
                    var count = users.Count;

                    for (var i = 0; i < count; i++)
                    {
                        var userInfo = new UserStandardEntity
                        {
                            Id = users[i].Id,
                            UserName = users[i].UserName,
                            FullName = users[i].FullName,
                            Avatar = users[i].Avatar,
                            Email = users[i].Email,
                            Mobile = users[i].Mobile,
                            IsFullPermission = users[i].IsFullPermission,
                            IsFullZone = users[i].IsFullZone,
                            Status = users[i].Status
                        };
                        userWithSimpleFields.Add(userInfo);
                    }
                }
            }

            return userWithSimpleFields;
        }

        /// <summary>
        /// Lấy danh sách user với quyền và zoneId theo userName
        /// check permission và zoneId của userName để lấy ra danh sách user tương ứng.
        /// Logic xử lý trong store
        /// </summary>
        /// <param name="permissionId"></param>
        /// <param name="zoneId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(EnumPermission permissionId, long newsId, string userName)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();


            List<UserEntity> users;
            using (var db = new CmsMainDb())
            {
                users = db.UserMainDal.GetUserWithFullPermissionAndZoneId((int)permissionId, newsId, userName);
                if (null != users && users.Count > 0)
                {
                    var count = users.Count;

                    for (var i = 0; i < count; i++)
                    {
                        var userInfo = new UserStandardEntity
                        {
                            Id = users[i].Id,
                            UserName = users[i].UserName,
                            FullName = users[i].FullName,
                            Avatar = users[i].Avatar,
                            Email = users[i].Email,
                            Mobile = users[i].Mobile,
                            IsFullPermission = users[i].IsFullPermission,
                            IsFullZone = users[i].IsFullZone,
                            Status = users[i].Status,
                            PermissionCount = users[i].PermissionCount
                        };
                        userWithSimpleFields.Add(userInfo);
                    }
                }
            }

            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionIdAndZoneList(EnumPermission permissionId, string zoneIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            using (var db = new CmsMainDb())
            {
                var users = db.UserMainDal.GetUserByPermissionIdAndZoneList((int)permissionId, zoneIds);
                if (null != users && users.Count > 0)
                {
                    var count = users.Count;

                    for (var i = 0; i < count; i++)
                    {
                        var userInfo = new UserStandardEntity
                        {
                            Id = users[i].Id,
                            UserName = users[i].UserName,
                            FullName = users[i].FullName,
                            Avatar = users[i].Avatar,
                            Email = users[i].Email,
                            Mobile = users[i].Mobile,
                            IsFullPermission = users[i].IsFullPermission,
                            IsFullZone = users[i].IsFullZone,
                            Status = users[i].Status,
                            PermissionCount = users[i].PermissionCount
                        };
                        userWithSimpleFields.Add(userInfo);
                    }
                }
            }

            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionListAndZoneList(string zoneIds, params EnumPermission[] permissionIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var listPermissionId = permissionIds.Aggregate("",
                                                           (current, permissionId) =>
                                                           current + (";" + (int)permissionId));

            if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

            using (var db = new CmsMainDb())
            {
                var users = db.UserMainDal.GetUserByPermissionListAndZoneList(listPermissionId, zoneIds);
                if (null != users && users.Count > 0)
                {
                    var count = users.Count;

                    for (var i = 0; i < count; i++)
                    {
                        var userInfo = new UserStandardEntity
                        {
                            Id = users[i].Id,
                            UserName = users[i].UserName,
                            FullName = users[i].FullName,
                            Avatar = users[i].Avatar,
                            Email = users[i].Email,
                            Mobile = users[i].Mobile,
                            IsFullPermission = users[i].IsFullPermission,
                            IsFullZone = users[i].IsFullZone,
                            Status = users[i].Status,
                            PermissionCount = users[i].PermissionCount
                        };
                        userWithSimpleFields.Add(userInfo);
                    }
                }
            }


            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(EnumPermission permissionId, int zoneId)
        {
            List<UserStandardEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetNormalUserByPermissionIdAndZoneId((int)permissionId, zoneId);
            }
            return returnValue;
        }

        public static WcfActionResponse AddnewSmsCodeForUser(int userId)
        {
            using (var db = new CmsMainDb())
            {
                var returnValue = db.UserMainDal.GetUserById(userId);
                if (null == returnValue)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);

                }

                if (returnValue.Status != (int)UserStatus.Actived)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked]);

                }

                var userSms = new UserSmsEntity
                {
                    UserId = userId,
                    SmsCode = Utility.GenerateRandomString(4),
                    ExpiredDate = DateTime.Now.AddMinutes(SmsExpired)
                };
                if (db.UserSmsMainDal.AddnewSmsCode(userSms))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                        ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }

        }

        public static WcfActionResponse RemoveInUsingState(int userId)
        {
            using (var db = new CmsMainDb())
            {
                var existsUser = db.UserMainDal.GetUserById(userId);
                if (null == existsUser)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserNotFound, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserNotFound]);
                }

                if (existsUser.Status != (int)UserStatus.Actived)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked]);
                }
                if (db.UserSmsMainDal.RemoveInUsing(userId))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }

            }


        }

        public static UserSmsEntity GetUserSmsCode(int userId)
        {
            UserSmsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.GetUserSmsByUserId(userId);
            }
            return returnValue;
        }

        public static string GetOtpSecretKeyByUsername(string username)
        {
            string returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetOtpSecretKeyByUsername(username);
            }
            return returnValue;
        }

        public static WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            using (var db = new CmsMainDb())
            {
                if (db.UserMainDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey))
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }

        public static int CountUser(UserStatus status)
        {
            try
            {
                int returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.UserMainDal.CountUser((int)status);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserDal.CountUser:{0}", ex.Message));
            }
        }

        #endregion GET
        #region Business

        public static UserStandardEntity GetUserStandardByUserId(string encryptUserId)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }

            var user = GetById(encryptUserId);

            if (null == user)
            {
                return null;
            }
            return new UserStandardEntity
            {
                Id = 0,
                EncryptId = encryptUserId,
                UserName = user.UserName,
                FullName = user.FullName,
                Avatar = user.Avatar,
                Email = user.Email,
                Mobile = user.Mobile,
                IsFullPermission = user.IsFullPermission,
                IsFullZone = user.IsFullZone,
                Status = user.Status,
                Website = user.Website,
                Skype = user.Skype
            };
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(string encryptUserId)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);

                if (userId <= 0)
                {
                    return null;
                }

                var userWithPermission = GetUserWithPermissionByUserId(userId);
                if (null != userWithPermission && null != userWithPermission.User)
                {
                    userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                    userWithPermission.User.Id = 0;
                }
                return userWithPermission;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            var userWithPermissionDetail = new UserWithPermissionDetailEntity
            {
                AllGroupPermission = PermissionBo.GetAllPermissionGroupDetail(),
                AllParentZone = ZoneBo.GetListParentZoneActivedByParentId(),
                User = GetById(userId),
                UserPermissionList = PermissionBo.GetListByUserId(userId, isGetChildZone)
            };
            return userWithPermissionDetail;
        }

        public static List<UserStandardEntity> Search(string keyword,
                                                           UserStatus status,
                                                           UserSortExpression sortOrder,
                                                           int pageIndex,
                                                           int pageSize,
                                                           ref int totalRow)
        {
            var userStandards = SearchUser(keyword, status, sortOrder, pageIndex, pageSize, ref totalRow);

            if (null != userStandards && userStandards.Count > 0)
            {
                int count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds, params EnumPermission[] permissionIds)
        {
            var userStandards = GetUserByPermissionListAndZoneList(zoneIds, permissionIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        public static List<UserStandardEntity> GetUserListByPermissionIdAndZoneList(EnumPermission permissionId, string zoneIds)
        {
            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> GetUserWhoCanManageThisNews(EnumPermission permissionId, long newsId)
        {
            var zones = ZoneBo.GetZoneByNewsId(newsId);
            var zoneIds = zones.Aggregate("", (current, zone) => current + ("," + zone.Id));
            if (!string.IsNullOrEmpty(zoneIds)) zoneIds = zoneIds.Substring(1);

            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Account
{
    public class UserExperienceBo
    {

        public static UserExperienceEntity GetById(int id)
        {
            try
            {
                UserExperienceEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.UserExperienceDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new UserExperienceEntity();
            }
        }

        public static List<UserExperienceEntity> GetAllExperienceByUserId(string userName, UserSortExperience sortExperience, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                List<UserExperienceEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.UserExperienceDal.GetAllExperienceByUserId(userName, (int)sortExperience, pageIndex, pageSize, ref totalRows);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<UserExperienceEntity>();
            }
        }


        public static WcfActionResponse ExprienceInsert(UserExperienceEntity obj, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    db.UserExperienceDal.Insert(obj, ref id);
                    return WcfActionResponse.CreateSuccessResponse();

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse ExprienceUpdate(UserExperienceEntity obj)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    db.UserExperienceDal.Update(obj);
                    return WcfActionResponse.CreateSuccessResponse();

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }



        public static WcfActionResponse ExprienceDelete(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    db.UserExperienceDal.Delete(id);
                    return WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
    }
}

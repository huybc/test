﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Debt;
using Mi.Entity.Base.Document;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Base.Debt;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Debt
{
    public class DebtBo
    {
        public static DebtEntity GetById(int id)
        {
            try
            {
              
                using (var db = new CmsMainDb())
                {
                    return  db.DebtDal.GetById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static WcfActionResponse Update(DebtEntity obj, ref int id)
        {
            using (var db = new CmsMainDb())
            {
                if (db.DebtDal.Update(obj, ref id))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse Insert(DebtEntity obj, ref int id)
        {
            using (var db = new CmsMainDb())
            {
                if (db.DebtDal.Insert(obj, ref id))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static TransEntity TransGetById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return  db.DebtDal.TransGetById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static WcfActionResponse TransUpdate(TransEntity obj, ref int id)
        {
            using (var db = new CmsMainDb())
            {
                if (db.DebtDal.TransUpdate(obj, ref id))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }
        public static WcfActionResponse TransInsert(TransEntity obj, ref int id)
        {
            using (var db = new CmsMainDb())
            {
                if (db.DebtDal.TransInsert(obj, ref id))
                {
                    return WcfActionResponse.CreateSuccessResponse();

                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }

        }
        public static IEnumerable<TransEntity> TransSearch(TransEntity obj)
        {
            try
            {
                IEnumerable<TransEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.DebtDal.TransSearch(obj);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<TransEntity>();
            }
        }

    }
}

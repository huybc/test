﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Product
{
    public class ShopBo
    {
        #region Gets
        public static ShopEntity GetById(int id)
        {
            try
            {
                ShopEntity returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.GetById(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ShopEntity();
            }
        }
        public static List<ShopSimpleEntity> GetByUserName(string userName)
        {
            try
            {
                List<ShopSimpleEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.GetByUserName(userName);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ShopSimpleEntity>();
            }
        }
        public static IEnumerable<ShopEntity> GetAllByProductId(int id)
        {
            try
            {
                IEnumerable<ShopEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.GetAllByProductId(id);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ShopEntity>();
            }
        }

        public static List<ShopEntity> Search(string keyword, string username, EnumShopStatus status, EnumShopOrder shopOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                List<ShopEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.Search(keyword, username, (int)status, (int)shopOrder, pageIndex, pageSize, ref totalRows);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ShopEntity>();
            }
        }

        public static IEnumerable<ShopEntity> FE_ShopSearch(string keyword, string username, string zoneIds, EnumShopStatus status, EnumShopOrder shopOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                IEnumerable<ShopEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.FE_Shop_Search(keyword, username, zoneIds, (int)status, (int)shopOrder, pageIndex, pageSize, ref totalRows);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ShopEntity>();
            }
        }
        public static IEnumerable<ShopEntity> FE_ShopSearchSimple(string keyword, string username, string zoneIds, EnumShopStatus status, EnumShopOrder shopOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                IEnumerable<ShopEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.FE_Shop_SearchSimple(keyword, username, zoneIds, (int)status, (int)shopOrder, pageIndex, pageSize, ref totalRows);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ShopEntity>();
            }
        }
        public static List<LocationEntity> SearchNearLocation(int getTop, double latitude, double longitude, int distance, int zoneId, int type, EnumShopLocationOrder locationOrder, ref int totalRows, bool isLogin)
        {
            try
            {
                List<LocationEntity> returnValue;
                using (var db = new CmsMainDb())
                {
                    returnValue = db.ShopMainDal.SearchNearLocation(getTop, latitude, longitude, distance, zoneId, type, locationOrder, ref totalRows, isLogin);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<LocationEntity>();
            }
        }
        #endregion

        #region Sets

        public static WcfActionResponse Insert(ShopEntity obj, string zoneIdList, ref int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ShopMainDal.Insert(obj, zoneIdList, ref id))
                    {

                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }


        public static WcfActionResponse Update(ShopEntity product, string zoneIdList)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ShopMainDal.Update(product, zoneIdList))
                    {

                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }


        public static WcfActionResponse ChangeStatus(int id, EnumShopStatus status)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ShopMainDal.ChangeStatus(id, (int)status))
                    {

                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static WcfActionResponse ChangeShopStatus(int id, int status)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    if (db.ShopMainDal.ChangeShopStatus(id, status))
                    {

                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public static bool IsExistNamePrimary(int id, string name)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ShopMainDal.IsExistNamePrimary(id, name);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return true;
            }


        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Common;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Base.Product;
using Mi.MainDal.Databases;

namespace Mi.BO.Base.Product
{
    public class ProductBo
    {
        public static ProductEntity GetById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.GetById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ProductEntity();
            }
        }
        public static ProductEntity GetByIdV2(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.GetByIdV2(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ProductEntity();
            }
        }
        public static List<ProductInNewsWithProductInfoEntity> GetProductInNewByNewsId(long newsId, byte type)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.GetProductInNewByNewsId(newsId, type);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductInNewsWithProductInfoEntity>();
            }
        }
        public static List<ProductEntity> Search(string keyword, string username, string zoneIds, DateTime from, DateTime to, EnumProductType type, EnumProductSortOrder sortOrder, EnumProductStatus status, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.Search(keyword, username, zoneIds, from, to, (int)type, (int)sortOrder, (int)status, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }
        public static List<ProductEntity> Search(string keyword, string username, string zoneIds, DateTime from, DateTime to, EnumProductType type, EnumProductSortOrder sortOrder, EnumProductStatus status)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.SearchSuggest(keyword, username, zoneIds, from, to, (int)type, (int)sortOrder, (int)status);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }

        public static IEnumerable<ProductEntity> RecentView(string keyword, string username, string zoneIds, string productIds, DateTime from, DateTime to, EnumProductType type, EnumProductSortOrder sortOrder, EnumProductStatus status, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.RecentView(keyword, username, zoneIds, productIds, from, to, (int)type, (int)sortOrder, (int)status, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }
        public static IEnumerable<ProductEntity> FE_Product_Search(string keyword, string username, string zoneIds, double priceFrom, double priceTo, DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.FE_Product_Search(keyword, username, zoneIds, priceFrom, priceTo, from, to, isDiscountNew, type, sortOrder, status, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }
        public static IEnumerable<ProductEntity> FE_Product_Search_V1(string keyword, string username, string zoneIds, double priceFrom, double priceTo,
            DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int group, int propertyId, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.FE_Product_Search_V1(keyword, username, zoneIds, priceFrom, priceTo, from, to, isDiscountNew, type, sortOrder, status, group, propertyId, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }
        public static IEnumerable<ProductEntity> FE_Product_Search_V1(string keyword, string username, string zoneIds, double priceFrom, double priceTo,
            DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int group, string propertyListId,int manufacturerId, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.FE_Product_Search_V1(keyword, username, zoneIds, priceFrom, priceTo, from, to, isDiscountNew, type, sortOrder, status, group, propertyListId, manufacturerId, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductEntity>();
            }
        }
        public static IEnumerable<ProductNearLocationEntity> FE_Product_SearchWithNearLocation(string keyword, string username,
            string zoneIds, double priceFrom, double priceTo, DateTime from, DateTime to, int distance, double longitude, double latitude, int isDiscountNew, int type,
            int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.FE_Product_SearchWithNearLocation(keyword, username, zoneIds, priceFrom, priceTo,
                    from, to, distance, longitude, latitude, isDiscountNew, type, sortOrder, status, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductNearLocationEntity>();
            }
        }


        public static WcfActionResponse Insert(ProductEntity product, string zoneIds, string tagIdList, string shopIdList, ref int id)
        {
            try
            {
                var urlReturn = string.Empty;
                var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(Utility.ConvertToInt(zoneIds));
                if (null == primaryZone)
                {

                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }

                if (string.IsNullOrEmpty(product.Url))
                {
                    urlReturn = product.Url = BuildLinkUrl(primaryZone.ShortUrl, string.IsNullOrEmpty(product.Url) ? product.Title : product.Url, Utility.ConvertToInt(zoneIds));

                }
                else
                {
                    urlReturn = primaryZone.ShortUrl + "/" + product.Url;
                }


                product.Url = urlReturn;
                using (var db = new CmsMainDb())
                {
                    if (db.ProductMainDal.Insert(product, zoneIds, tagIdList, shopIdList, ref id))
                    {
                        product.Id = id;
                        // AddUpdateLuceneIndex(product);
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse Update(ProductEntity product, string zoneIds, string tagIdList, string shopIdList)
        {
            try
            {
                var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(Utility.ConvertToInt(zoneIds));
                if (null == primaryZone)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone, ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone]);

                }

                if (string.IsNullOrEmpty(product.Url))
                {
                    product.Url = BuildLinkUrl(primaryZone.ShortUrl, product.Title, Utility.ConvertToInt(zoneIds));

                }
                else
                {
                    product.Url = primaryZone.ShortUrl + "/" + product.Url;
                }
                using (var db = new CmsMainDb())
                {
                    db.ProductMainDal.Update(product, zoneIds, tagIdList, shopIdList);
                    try
                    {
                        //index for search
                        // AddUpdateLuceneIndex(product);
                        return WcfActionResponse.CreateSuccessResponse();

                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "===>Search Product Index Error: " + ex.Message);
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse UpdateCatelog(ProductEntity product)
        {
            try
            {

                using (var db = new CmsMainDb())
                {
                    db.ProductMainDal.UpdateCatelog(product);
                    try
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "===>Search Product Index Error: " + ex.Message);
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }


        public static WcfActionResponse ChangeStatus(int id, EnumProductStatus status)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var result = db.ProductMainDal.ChangeStatus(id, (int)status);
                    return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }
        public static WcfActionResponse Update_SellOut(int id, bool sellOut)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    var result = db.ProductMainDal.Update_SellOut(id, sellOut);
                    return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        #region productBase

        public static ProductBaseEntity GetProductBaseById(int id)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.GetProductBaseById(id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ProductBaseEntity();
            }

        }

        public static List<ProductBaseEntity> SearchProductBase(string keyword, string username, string zoneIds, DateTime from, DateTime to, EnumProductBaseType type, EnumProductBaseSortOrder sortOrder, EnumProductBaseStatus status, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                using (var db = new CmsMainDb())
                {
                    return db.ProductMainDal.SearchProductBase(keyword, username, zoneIds, from, to, (int)type, (int)sortOrder, (int)status, pageIndex, pageSize, ref totalRows);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ProductBaseEntity>();
            }
        }


        public static WcfActionResponse InsertProductBase(ProductBaseEntity product, string zoneIds, string tagIdList, ref int id)
        {

            try
            {
                using (var db = new CmsMainDb())
                {
                    var result = db.ProductMainDal.InsertProductBase(product, zoneIds, tagIdList, ref id);
                    return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        public static WcfActionResponse UpdateProductBase(ProductBaseEntity product, string zoneIds, string tagIdList)
        {


            try
            {
                using (var db = new CmsMainDb())
                {
                    var result = db.ProductMainDal.UpdateProductBase(product, zoneIds, tagIdList);
                    return result ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);

            }
        }

        #endregion
        public static List<ProductInShopEntity> GetProductInShopByProductId(int productId)
        {
            using (var db = new CmsMainDb())
            {
                return db.ProductInShopMainDal.GetProductInShopByProductId(productId);
            }
        }

        //public virtual ProductDetailForEditEntity GetProductForEditByProductId(int productId, string currentUsername)
        //{
        //    var newsVersionId = 0L;
        //    var productDetail = new ProductDetailForEditEntity();

        //    productDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(currentUsername, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin), "--");
        //    productDetail.ProductInfo = (productId < 0 ? null : GetById(productId));
        //    if (null != productDetail.ProductInfo)
        //    {
        //        // Zone
        //        if (productId > 0)
        //        {
        //            productDetail.ProductInShop = GetProductInShopByProductId(productId);
        //            productDetail.TagInNews = TagP(newsId);
        //            if (productDetail.NewsInZone.Count > 0)
        //            {
        //                productDetail.ProductInfo.ZoneId = productDetail.NewsInZone.FirstOrDefault().ZoneId;
        //            }
        //        }
        //    }

        //    productDetail.NewsContentWithTemplate = productDetail.ProductInfo != null ? productDetail.ProductInfo.Detail : "";

        //    return productDetail;
        //}


        //public static IEnumerable<ProductEntity> SearchLucene(string keyword, string field, int pageIndex, int pageSize, ref int totalRows)
        //{
        //    return RLuceneProduct.Search(keyword, field, pageIndex, pageSize, ref totalRows).ToList();
        //}
        public static IEnumerable<ProductEmbedEntity> EF_BoxProductEmbedGetListByZone(int zone, int type, int pageSize)
        {
            using (var db = new CmsMainDb())
            {
                return db.ProductMainDal.EF_BoxProductEmbedGetListByZone(zone, type, pageSize);
            }

        }

        //public static void AddUpdateLuceneIndex(ProductEntity product)
        //{
        //    RLuceneProduct.AddUpdateLuceneIndex(product);
        //}

        //public static void AddUpdateLuceneIndex(List<ProductEntity> listProducts)
        //{
        //    RLuceneProduct.AddUpdateLuceneIndex(listProducts);
        //}
        public static string BuildLinkUrl(string zoneUrl, string newsTitle, int zoneId)
        {
            try
            {
                string formatUrl = BoConstants.ProductUrlFormat;
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                if (formatUrl.IndexOf("ca{3}") > 0)
                {
                    return titleUnsignAndSlash != ""
                               ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId)
                               : string.Empty;
                }
                else
                {
                    return titleUnsignAndSlash != ""
                               ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash)
                               : string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.WcfExtensions;

namespace Mi.BO.Common
{
    public class BoFactory
    {
        private static readonly Assembly CurrentAssembly = Assembly.GetExecutingAssembly();

        private static Dictionary<string, object> _instanceList;
        public static T GetInstance<T>()
        {
            try
            {
                if (null == _instanceList) _instanceList = new Dictionary<string, object>();

                var instanceType = typeof(T);
                var instanceName = instanceType.FullName;
                if (null != instanceName)
                {
                    if (!_instanceList.ContainsKey(instanceName) || _instanceList[instanceName] == null)
                    {
                        object newInstance = null;

                        var instanceAssembly = instanceName;
                        var providerName = "";
                        if (null != WcfMessageHeader.Current)
                        {
                            providerName = WcfMessageHeader.Current.Namespace;
                        }
                        if (!string.IsNullOrEmpty(providerName) && instanceAssembly.IndexOf(".Base.", System.StringComparison.Ordinal) > 0)
                        {
                            instanceAssembly = instanceAssembly.Replace(".Base.", ".External." + providerName + ".");
                            Logger.WriteLog(Logger.LogType.Debug, "1 => " + instanceAssembly);
                            newInstance = CurrentAssembly.CreateInstance(instanceAssembly, false,
                                                                         BindingFlags.CreateInstance,
                                                                         null,
                                                                         null,
                                                                         System.Globalization.CultureInfo.CurrentCulture,
                                                                         null);
                        }
                        if (newInstance == null)
                        {
                            Logger.WriteLog(Logger.LogType.Debug, "2 => " + instanceName);
                            newInstance = CurrentAssembly.CreateInstance(instanceName, false,
                                                                         BindingFlags.CreateInstance,
                                                                         null,
                                                                         null,
                                                                         System.Globalization.CultureInfo.CurrentCulture,
                                                                         null);
                        }

                        if (null != newInstance)
                        {
                            if (!_instanceList.ContainsKey(instanceName))
                            {
                                _instanceList.Add(instanceName, newInstance);
                            }
                            else
                            {
                                _instanceList[instanceName] = newInstance;
                            }
                            return (T)newInstance;
                        }
                        return default(T);
                    }
                    Logger.WriteLog(Logger.LogType.Debug, "3 => " + NewtonJson.Serialize(_instanceList));
                    return (T)_instanceList[instanceName];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return default(T);
        }
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Hoang_Long.Master" AutoEventWireup="true" CodeBehind="HoiDapDetail.aspx.cs" Inherits="Hoang_Long.Pages.HoiDapDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Mi.Action.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                    <li class="breadcrumb-item active">FAQs</li>
                </ol>
            </nav>
        </div>
    </div>
    <%//Lay chi tiet noi dung cau hoi %>
    <%
        var cauHoiId = int.Parse(Page.RouteData.Values["id"].ToString());
        var detail = NewsBo.GetNewsDetailById(cauHoiId);
    %>
    <div class="faqs-grid">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-7 col-12">
                    <div class="faqs-detail pr-md-5">
                        <h1 class="title">
                            <%=detail.Title %>

                        </h1>
                        <%=detail.Body %>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-5 col-12">
                    <div class="form-group form-search">
                        <input type="text" class="form-control" id="search_faq" placeholder="Tìm kiếm câu hỏi và ấn enter..." />
                        <i class="fas fa-search"></i>
                    </div>
                    <div class="faqs-right">
                        <div class="heading">
                            <%=UIHelper.GetConfigByName("FaqTitleLeft") %>
                        </div>
                        <div class="binding">
                            <%var listCauHoiRight = NewsBo.GetTopNewestNews(); %>
                            <%foreach (var item in listCauHoiRight)
                                { %>
                            <div class="item-question">
                                <a href="/hoi-dap/<%=item.Url %>.<%=item.Id %>.htm"><%=item.Title %></a>
                            </div>
                            <%} %>
                        </div>
                        <%//Lay danh sach 9 cau hoi moi nhat  %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Pages/UI_Script/HoiDap.js"></script>
</asp:Content>

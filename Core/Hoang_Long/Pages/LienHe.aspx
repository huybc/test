﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Hoang_Long.Master" AutoEventWireup="true" CodeBehind="LienHe.aspx.cs" Inherits="Hoang_Long.Pages.LienHe" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
        <section class="w-100 ">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d931.3829632560515!2d105.79311482921287!3d20.971307251396663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acd75109a67b%3A0x6cf9b956466669eb!2zU8OibiBCb8yBbmcgWm9uZTkgVsSDbiBRdWHMgW4!5e0!3m2!1svi!2s!4v1572685264252!5m2!1svi!2s"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </section>
    <section class="contact mb-5">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="box">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-12 left">
                            <div class="heading">
                                Liên hệ
                            </div>
                            <div class="form-group row ">
                                <label class="col-lg-3 col-md-3 col-sm-4 col-12 align-self-center">Họ tên:</label>
                                <div class=" col-lg-9 col-md-9 col-sm-8 col-12">
                                    <input class="form-control" type="text" id="txtName">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-lg-3 col-md-3 col-sm-4 col-12 align-self-center">Số điện
                                    thoại:</label>
                                <div class=" col-lg-9 col-md-9 col-sm-8 col-12">
                                    <input class="form-control" type="text" id="txtPhoneNumber">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-lg-3 col-md-3 col-sm-4 col-12 align-self-center">Email:</label>
                                <div class=" col-lg-9 col-md-9 col-sm-8 col-12">
                                    <input class="form-control" type="text" id="txtEmail">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-lg-3 col-md-3 col-sm-4 col-12 align-self-center">Tin nhắn:</label>
                                <div class=" col-lg-9 col-md-9 col-sm-8 col-12">
                                    <textarea class="form-control" type="text" id="txtNote"></textarea>
                                </div>
                            </div>


                            <div class="form-group row justify-content-center">

                                <div class="offset-3 col-md-9 col-sm-12 col-12 ">
                                    <button class="btn btn-book " type="button" id="btnSendContact">Gửi ngay</button>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5 col-sm-12 col-12 d-flex  right">
                            <div class="">
                                <div class="text-uppercase mb-0">
                                    Gọi ngay
                                </div>
                                <div class="h2 font-weight-bold mb-4">
                                    <%=UIHelper.GetConfigByName("HotLine") %>
                                </div>

                                <ul class="info-company pb-4">
                                    <li><%=UIHelper.GetConfigByName("TenCongTy") %>
                                    </li>
                                    <li>
                                        Địa chỉ: <%=UIHelper.GetConfigByName("Address") %>
                                    </li>
                                    <li>
                                        Email: <%=UIHelper.GetConfigByName("Email") %>
                                    </li>
                                </ul>
                                <div class="small py-4">
                                    <%=UIHelper.GetConfigByName("MoTaChung") %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Pages/UI_Script/DatHang.js"></script>
</asp:Content>

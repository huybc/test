﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Hoang_Long.Master" AutoEventWireup="true" CodeBehind="DoiTac.aspx.cs" Inherits="Hoang_Long.Pages.DoiTac" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="banner-our-customer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-12 align-self-center">
                    <div class="content">
                        <h1 class="title"><%=UIHelper.GetConfigByName("DoiTacTitle") %></h1>
                        <div class="mb-3">
                            <%=UIHelper.GetConfigByName("DoiTacSapo") %>
                        </div>
                        <div>
                            <a href="" class="btn btn-contact" role="btn">Liên hệ</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-12">
                    <div class="image-grid">
                        <marquee behavior="scroll" direction="up">
                            <img src="/uploads/<%=UIHelper.GetConfigByName("DoiTac_No1_Banner") %>" class="img-fluid"/>
                        </marquee>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="my-video video-2 bg-E8F8FF">
        <div class="container">
            <div class="heading-ss color-000">
                <%=UIHelper.GetConfigByName("DoiTac_No1_Title") %>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("DoiTac_No1_Sapo") %>
                    </div>
                    <div class="video">
                        <iframe width="100%" height="450" src="<%=UIHelper.GetConfigByName("DoiTac_No1_Video").Replace("watch?v=","embed/") %>"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%//Lay vung hien thi slide doi tac %>
    <%var slideDoiTac = ConfigBo.AdvGetByType(103).OrderBy(r => r.SortOrder).ToList(); %>
    <section class="slide-customers">
        <div class="container">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <%foreach (var item in slideDoiTac)
                        { %>
                        <div class="swiper-slide">
                        <div class="px-4">
                            <div class="text">
                                <%=item.Content %>
                            </div>
                            <div class="avatar">
                                <img src="/uploads/thumb/<%=item.Thumb %>" class="img-fluid" />
                            </div>
                            <div class="name">
                                <%=item.Name.Split('|')[0].TrimEnd() %>
                            </div>
                            <div class="position">
                                <%=item.Name.Split('|')[1].TrimStart() %>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>

    <%//Lay bai viet khach hang %>
    <%var listNewsCustomer = NewsBo.GetByZoneId(ZoneBo.GetZoneByAlias("khach-hang").Id, 4).ToList(); %>
    <section class="slide-customer-2">
        <div class="container">
            <div class="heading">
                <%=UIHelper.GetConfigByName("DoiTac_No2_Title") %>
            </div>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <%for (int i = 0; i < listNewsCustomer.Count(); i++)
                        { %>
                    <%if (i == 0)
                        { %>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<%=i %>" class="active">
                        <img src="/uploads/thumb/<%=listNewsCustomer[i].Avatar %>" class="img-fluid" />
                    </li>
                    <%} %>
                    <%if (i > 0)
                        { %>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<%=i %>">
                        <img src="/uploads/thumb/<%=listNewsCustomer[i].Avatar %>" class="img-fluid" />
                    </li>
                    <%} %>


                    <%} %>

                    <%--
                    <li data-target="#carouselExampleIndicators" data-slide-to="2">
                        <img src="/Themes/images/change/customer-thumb-03.jpg" class="img-fluid" />
                    </li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3">
                        <img src="/Themes/images/change/customer-thumb-05.jpg" class="img-fluid" />
                    </li>--%>
                </ol>
                <div class="carousel-inner">
                    <%for (int i = 0; i < listNewsCustomer.Count(); i++)
                        {  %>
                    <%if (i == 0)
                        { %>
                    <div class="carousel-item active">
                        <div class="content-carousel">
                            <div class="row justify-content-lg-end">
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-left">
                                        <div class="row">
                                            <div class=" col-lg-4 col-md-5 col-sm-4 col-4 avatar">
                                                <img src="/uploads/<%=listNewsCustomer[i].Avatar2 %>" class="img-fluid" alt="" />
                                            </div>
                                            <div class="text col-lg-8 col-md-7 col-sm-8 col-8">
                                                <div class="top">
                                                    <%=listNewsCustomer[i].Title %>
                                                </div>
                                                <div class="bot">
                                                    <%=listNewsCustomer[i].Sapo %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 comment">
                                                <%=listNewsCustomer[i].Body %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-right mb-5 mb-md-0">
                                        <img src="/uploads/<%=listNewsCustomer[i].Avatar %>" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                    <%if (i > 0)
                        { %>
                    <div class="carousel-item">
                        <div class="content-carousel">
                            <div class="row justify-content-lg-end">
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-left">
                                        <div class="row">
                                            <div class=" col-lg-4 col-md-5 col-sm-4 col-4 avatar">
                                                <img src="/uploads/<%=listNewsCustomer[i].Avatar2 %>" class="img-fluid" alt="" />
                                            </div>
                                            <div class="text col-lg-8 col-md-7 col-sm-8 col-8">
                                                <div class="top">
                                                    <%=listNewsCustomer[i].Title %>
                                                </div>
                                                <div class="bot">
                                                    <%=listNewsCustomer[i].Sapo %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 comment">
                                                <%=listNewsCustomer[i].Body %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-right mb-5 mb-md-0">
                                        <img src="/uploads/<%=listNewsCustomer[i].Avatar %>" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                    <%} %>

                    <%--<div class="carousel-item">
                        <div class="content-carousel">
                            <div class="row justify-content-end">
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-left">
                                        <div class="row">
                                            <div class=" col-lg-4 col-md-5 col-sm-4 col-4 avatar">
                                                <img src="/Themes/images/change/ava-customer.png" class="img-fluid" alt="" />
                                            </div>
                                            <div class="text col-lg-8 col-md-7 col-sm-8 col-8">
                                                <div class="top">
                                                    Sherri Langbur
                                                </div>
                                                <div class="bot">
                                                    <div>
                                                        CEO,
                                                    </div>
                                                    <div>
                                                        Babbleboxx
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 comment">
                                                <p>
                                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                                </p>
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                                         
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                    <div class="content-carousel-right mb-5 mb-md-0">
                                        <img src="/Themes/images/change/customer-01.jpg" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="content-carousel">
                            <div class="row justify-content-end">
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-left">
                                        <div class="row">
                                            <div class=" col-lg-4 col-md-5 col-sm-4 col-4 avatar">
                                                <img src="/Themes/images/change/ava-customer.png" class="img-fluid" alt="" />
                                            </div>
                                            <div class="text col-lg-8 col-md-7 col-sm-8 col-8">
                                                <div class="top">
                                                    Sherri Langbur
                                                </div>
                                                <div class="bot">
                                                    <div>
                                                        CEO,
                                                    </div>
                                                    <div>
                                                        Babbleboxx
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 comment">
                                                <p>
                                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                                </p>
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                                         
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                    <div class="content-carousel-right mb-5 mb-md-0">
                                        <img src="/Themes/images/change/customer-01.jpg" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="content-carousel">
                            <div class="row justify-content-end">
                                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                    <div class="content-carousel-left">
                                        <div class="row">
                                            <div class=" col-lg-4 col-md-5 col-sm-4 col-4 avatar">
                                                <img src="/Themes/images/change/ava-customer.png" class="img-fluid" alt="" />
                                            </div>
                                            <div class="text col-lg-8 col-md-7 col-sm-8 col-8">
                                                <div class="top">
                                                    Sherri Langbur
                                                </div>
                                                <div class="bot">
                                                    <div>
                                                        CEO,
                                                    </div>
                                                    <div>
                                                        Babbleboxx
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 comment">
                                                <p>
                                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                                </p>
                                                <p>
                                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                                         
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                    <div class="content-carousel-right mb-5 mb-md-0">
                                        <img src="/Themes/images/change/customer-01.jpg" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

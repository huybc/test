﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Hoang_Long.Master" AutoEventWireup="true" CodeBehind="SanPhamDetail.aspx.cs" Inherits="Hoang_Long.Pages.SanPhamDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        input[type="file"] {
            display:none;
        }
        .upload-div {
            text-align: center;
        }
        #label-input-file {
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%
        var sanPham_Id = Page.RouteData.Values["id"].ToInt32Return0();
        var sanPham_Detail = NewsBo.GetNewsDetailById(sanPham_Id);
        var sanPham_Part = ProjectDetailBo.GetByArticleId(sanPham_Id);

    %>
    <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item"><a href="#">Sản phẩm</a></li>
                    <li class="breadcrumb-item active"><%=sanPham_Detail.Title %></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs product-detail-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab-01" role="tab"
                            aria-selected="true">Chọn size sản phẩm</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab-02" role="tab"
                            aria-selected="false">Gửi mẫu thiết kế</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab-03" role="tab"
                            aria-selected="false">Tư vấn & sản xuất theo yêu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab-04" role="tab"
                            aria-selected="false">Đóng gói & Giao hàng tận nơi</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="tab-content tab-content-product">
                    <div class="tab-pane fade show active tab-order" id="tab-01" role="tabpanel">
                        <div class="order-product">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-12">
                                    <div class="image mb-3 mb-md-0">
                                        <img src="/uploads/thumb/<%=sanPham_Part[0].Image %>" class="img-fluid" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-12">
                                    <h1 class="title">
                                        <%=sanPham_Detail.Title %>
                                    </h1>
                                    <div class="description">
                                        <%=sanPham_Part[0].Content %>
                                    </div>
                                    <div class="form-row justify-content-between mb-4">
                                        <div class="col-md-4 col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>Kích thước:</label>
                                                <%// Lay cac size cua san pham %>
                                                <%var listSize = sanPham_Detail.Size.Split(';'); %>
                                                <select class="form-control" id="cbx_kich_thuoc">
                                                    <option selected hidden value="">Chọn kích thước</option>
                                                    <%foreach (var item in listSize)
                                                        { %>
                                                    <%if (item != "")
                                                        {  %>
                                                    <option value="<%=item %>"><%=item %></option>
                                                    <%} %>
                                                    <%} %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>Số lượng:</label>
                                                <input type="number" min="0" id="txt_so_luong" class="form-control" />
                                            </div>
                                        </div>
                                        <%//Lay danh sach mau sac %>
                                        <%
                                            var listColor = ZoneBo.GetAllZone((int)ZoneType.Color).Where(r => r.Status == 1).ToList();
                                            %>
                                        <div class="col-md-3 col-sm-4 col-12">
                                            <div class="form-group">
                                                <label for="disabledSelect">Màu sắc:</label>
                                                <select class="form-control" id="cbx_color">
                                                    <option selected hidden value="">Chọn màu sắc</option>
                                                    <%foreach (var item in listColor)
                                                        {  %>
                                                        <option value="<%=item.Id %>"><%=item.Name %></option>
                                                    <%} %>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="note">
                                        <div class="item">
                                            *Cấu hình vào đây hay là tùy sản phẩm?.
                                        </div>
                                        <div class="item">
                                            *Cấu hình vào đây hay là tùy sản phẩm?.
                                        </div>

                                    </div>
                                    <div class="">
                                        <a href="javascript:void(0)" class="btn" id="btn_datHang" data-id='<%=sanPham_Detail.Id %>' data-name="<%=sanPham_Detail.Title %>" role="button">Đặt hàng</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%//Tin 2 %>
                    <div class="tab-pane fade tab-send-design " id="tab-02" role="tabpanel">
                        <div class="send-design mb-5 upload-div">
                            
                            <%if (sanPham_Part.Count >= 2)
                                { %> 
                                <%=sanPham_Part[1].Content %>
                            <%} %>
                            <label id="label-input-file">
                                <input type="file" id="file_dinh_kem"/><i class="fas fa-upload"></i> Upload file: <span id="file-selected"></span>
                            </label>
                            
                        </div>
                    </div>
                    <%//Tin 3 %>
                    <div class="tab-pane fade tab-send-design " id="tab-03" role="tabpanel">
                        <div class="send-design mb-5">
                            <%if (sanPham_Part.Count >= 3)
                                { %> 
                                <%=sanPham_Part[2].Content %>
                            <%} %>
                            
                        </div>
                    </div>
                    <%//Tin 4 %>
                    <div class="tab-pane fade tab-send-design " id="tab-04" role="tabpanel">
                        <div class="send-design mb-5">
                            <%if (sanPham_Part.Count >= 4)
                                { %> 
                                <%=sanPham_Part[3].Content %>
                            <%} %>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%var zone_SP = ZoneBo.GetZoneByAlias("san-pham"); %>
    <%var list2 = NewsBo.GetByZoneId(zone_SP.Id, 100).Where(r => r.IsHot == false).OrderByDescending(r => r.Id).Skip(0).Take(6).ToList(); %>
    <%if (list2.Count > 6)
        { %>
    <section class="other-products">
        <div class="container">
            <div class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No4") %>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No4_Sapo") %>
                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-12">
                    <div class="grid-images">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <%//Lay danh sach san pham khon phai hot %>

                                <div class="row">
                                    <%var list2_part1 = list2.OrderBy(r => r.Id).Skip(0).Take(3).ToList(); %>

                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[0].Url %>.<%=list2_part1[0].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part1[0].Avatar %>"
                                                        class="img-fluid img-small" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[0].Url %>.<%=list2_part1[0].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[1].Url %>.<%=list2_part1[1].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part1[1].Avatar %>"
                                                        class="img-fluid img-small" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[1].Url %>.<%=list2_part1[1].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[2].Url %>.<%=list2_part1[2].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part1[2].Avatar %>"
                                                        class="img-fluid img-large" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[2].Url %>.<%=list2_part1[2].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%var list2_part2 = list2.OrderBy(r => r.Id).Skip(3).Take(3).ToList(); %>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row flex-wrap-reverse">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[0].Url %>.<%=list2_part2[0].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part2[0].Avatar %>"
                                                        class="img-fluid img-small" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[0].Url %>.<%=list2_part2[0].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[1].Url %>.<%=list2_part2[1].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part2[1].Avatar %>"
                                                        class="img-fluid img-small" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[1].Url %>.<%=list2_part2[1].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[2].Url %>.<%=list2_part2[2].Id %>.htm" title="">
                                                    <img src="/uploads/thumb/<%=list2_part2[2].Avatar %>"
                                                        class="img-fluid img-large" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[2].Url %>.<%=list2_part2[2].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%}
        else
        { %>
    <section class="other-products">
        <div class="container">
            <div class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No4") %>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No4_Sapo") %>
                    </div>

                </div>
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="grid-images">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-01.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-02.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-03.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row flex-wrap-reverse">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-01.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-02.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-03.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Pages/UI_Script/moment.js"></script>
    <script src="/Pages/UI_Script/DatHang.js"></script>
</asp:Content>

﻿$('#search_faq').off('keypress').on('keypress', function (e) {
    if (e.which == 13) {
        var filter = $('#search_faq').val();
        R.Post({
            params: {
                filter: filter
            },
            module: "ui-action",
            ashx: 'modulerequest.ashx',
            action: "faq-filter",
            success: function (res) {
                if (res.Success) {
                    console.log(res.Data);
                    var r = res.Data;
                    $('.binding').html('');
                    var htm = '';
                    r.forEach(e => {
                        htm += '<div class="item-question">';
                        htm += '<a href="/hoi-dap/' + e.Url + '.' + e.Id + '.htm">' + e.Title + '</a>'; 
                        htm += '</div>';
                    })
                    $('.binding').append(htm);
                }
                // $('.card-header').RLoadingModuleComplete();
            }
        });
    }
})
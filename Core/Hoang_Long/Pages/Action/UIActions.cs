﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.Customer;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Hoang_Long.CMS.Modules.Service.Action;
using Hoang_Long.Core.Helper;

namespace Hoang_Long.Pages.Action
{
    public class UIActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            switch (functionName)
            {

                case "save":
                    responseData = AddCustomer();
                    break;
                case "faq-detail":
                    responseData = FAQDetail();
                    break;
                case "faq-filter":
                    responseData = FAQFilter();
                    break;
            }

            return responseData;
        }

        private ResponseData FAQDetail()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData FAQFilter()
        {
            var responseData = new ResponseData();

            string filter = GetQueryString.GetPost("filter", string.Empty);
            var listZoneId = ZoneBo.GetListZoneByParentId(9, 203);

            var list = "";
            for (int i = 0; i < listZoneId.Count(); i++)
            {
                if (i < listZoneId.Count() - 1)
                    list += listZoneId[i].Id + ",";
                else
                    list += listZoneId[i].Id;
            }
            int TotalRows = 0;
            //NewsBo.SearchNews(string.Empty, "admin", zone_tin_tuc.Id.ToString(), DateTime.MinValue, DateTime.MaxValue, 0, 0, 3, 4, page, pageSize, ref totalRow).OrderByDescending(r => r.Id).ToList();
            var result = NewsBo.SearchNews(filter, "admin", list, DateTime.MinValue, DateTime.MaxValue, 0, 0,
                3, 4, 1, 1000, ref TotalRows).OrderByDescending(r => r.CreatedDate).Skip(0).Take(10);
            //var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            responseData.Data = result;
            return responseData;
        }

        private ResponseData AddCustomer()
        {
            var responseData = new ResponseData();
            string name = GetQueryString.GetPost("name", string.Empty);
            string email = GetQueryString.GetPost("email", string.Empty);
            string phoneNumber = GetQueryString.GetPost("phoneNumber", string.Empty);
            string note = GetQueryString.GetPost("note", string.Empty);


            if (!string.IsNullOrEmpty(name))
            {

                var obj = new CustomerEntity
                {
                    FullName = name,
                    Email = email,
                    Mobile = phoneNumber,
                    Note = note,
                    Type = "blog"
                };
                int outId = 0;

                responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                if (outId > 0)
                {
                    responseData.Success = true;
                }
                responseData.Message = "Thêm mới thành công !";

            }
            else
            {
                responseData.Success = false;
                responseData.Message = "Error";
            }
            return responseData;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RBLand.Pages.Controls.Sub
{
    public partial class UIPager : System.Web.UI.UserControl
    {

        [Description("")]
        [Browsable(true)]
        [DefaultSettingValue("")]
        public int PageSize { get; set; }

        [Description("")]
        [Browsable(true)]
        [DefaultSettingValue("")]
        public string QueryKey { get; set; }

        [Description("")]
        [Browsable(true)]
        [DefaultSettingValue("")]
        public int TotalItems { get; set; }

        [Description("")]
        [Browsable(true)]
        [DefaultSettingValue("")]
        public int PageShow { get; set; }


        public int CurrentPage;
        public int TotalPages;
        public int StartPage;
        public int EndPage;
        public string path;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                var page = Request.QueryString["page"];
                if (TotalItems > 0)
                {
                    PageSize = PageSize == 0 ? 10 : PageSize;
                    Pager(TotalItems, page.ToInt32Return0(), PageSize);

                }
            }
        }

        public void Pager(int totalItems, int? pageIndex, int pageSize)
        {
            PageShow = PageShow > 0 ? PageShow : 5;
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = pageIndex > 0 ? (int)pageIndex : 1;
            var startPage = currentPage - (PageShow - 3);
            var endPage = currentPage + (PageShow - 3);
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > pageSize)
                {
                    startPage = endPage - PageShow;
                }
            }

            if (totalPages > (PageShow * 2))
            {

            }

            CurrentPage = currentPage;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;

            // check url has param


            var pr = HttpContext.Current.Request.QueryString.AllKeys;
            List<string> _params = new List<string>();
            foreach (var s in pr)
            {
                if (!string.IsNullOrEmpty(s))
                {
                    _params.Add(s);
                }
            }


            var tempParam = string.Empty;
            QueryKey = string.IsNullOrEmpty(QueryKey) ? "page" : QueryKey;
            path = HttpContext.Current.Request.Url.PathAndQuery;

            if (HttpContext.Current.Request.QueryString.HasKeys())
            {
                if (_params != null)
                {
                    int pos = Array.IndexOf(_params.ToArray(), QueryKey);

                    tempParam = QueryKey;

                    if (pos > -1)
                    {

                        //Tồn tại QueryKey. Xóa dấu hỏi, chuyển thành dấu và
                        if (_params.Count() > 1)
                        {
                            // tồn tại QueryKey vs param khác

                            QueryKey = "&" + QueryKey + "=";
                        }
                        else
                        {
                            // tồn tại duy nhất QueryKey   
                            QueryKey = QueryKey + "=";
                        }


                    }
                    else
                    {
                        // không tồn tại QueryKey
                        QueryKey = "&" + QueryKey + "=";
                    }
                }
            }
            else
            {
                QueryKey = "?" + QueryKey + "=";

            }


            path = Regex.Replace(path.ToLower(), @"(&" + tempParam + "=[^&\\s]+|" + tempParam + "=[^&\\s]+&?)", "");
        }




    }

    public static class Utils
    {
        public static Int32 ToInt32Return0<T>(this T input)
        {
            int output = 0;
            if (null != input)
            {
                try
                {
                    output = Convert.ToInt32(input);
                }
                catch (Exception)
                {
                    return output;
                }
            }

            return output;
        }

    }
}
﻿using System.Net;
using System.Web;
using Mi.Common;


namespace Hoang_Long.Core.Helper
{
    /// <summary>
    /// Summary description for ImageProxy
    /// </summary>
    public class ImageProxy : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var imageUrl = GetQueryString.GetQuery("url", "");
            const int BUFFER_SIZE = 1024 * 1024;
            var req = WebRequest.Create(imageUrl);
            using (var resp = req.GetResponse())
            {
                using (var stream = resp.GetResponseStream())
                {
                    var bytes = new byte[BUFFER_SIZE];
                    while (true)
                    {
                        var n = stream.Read(bytes, 0, BUFFER_SIZE);
                        if (n == 0)
                        {
                            break;
                        }
                        context.Response.ContentType = "image/png";
                        context.Response.OutputStream.Write(bytes, 0, n);
                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
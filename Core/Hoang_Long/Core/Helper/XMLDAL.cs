﻿using System;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Mi.Common.ChannelConfig;

namespace Hoang_Long.Core.Helper
{
    public static class XMLDAL
    {
        public static void AddNode(string url, int id)
        {
            try
            {
                string pathXml = HttpContext.Current.Server.MapPath("~/sitemap.xml");
                var doc = XDocument.Load(pathXml);

                XElement newProduct = new XElement("url", new XAttribute("id", id),
                    new XElement("loc", CmsChannelConfiguration.GetAppSetting("Domain") + url));

                // Add node as a very first node
                doc.Root.Add(newProduct);

                // To add bottom use doc.Root.Add
                doc.Save(pathXml);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static void UpdateNode(string url, int id)
        {
            try
            {
                string pathXml = HttpContext.Current.Server.MapPath("~/sitemap.xml");
                var doc = XDocument.Load(pathXml);

                var obj = (from xml in doc.Descendants("url")
                           where Convert.ToInt32(xml.Attribute("id").Value) == id
                           select xml).FirstOrDefault();
                if (obj != null)
                {
                    obj.Element("loc").Value = CmsChannelConfiguration.GetAppSetting("Domain") + url;
                    doc.Save(pathXml);

                }
                else
                {
                    AddNode(url, id);
                }
                UpdateNodeLastMod();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static void UpdateNodeLastMod()
        {
            try
            {
                string pathXml = HttpContext.Current.Server.MapPath("~/sitemap.xml");
                var doc = XDocument.Load(pathXml);

                var obj = (from xml in doc.Descendants("url")
                           where Convert.ToInt32(xml.Attribute("id").Value) == 1
                           select xml).FirstOrDefault();
                if (obj != null)
                {
                    obj.Element("lastmod").Value = DateTime.Now.ToString();
                    doc.Save(pathXml);

                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        public static void DeleteXmlNode(string id)
        {
            XmlDocument doc = new XmlDocument();

            string pathXml = HttpContext.Current.Server.MapPath("~/sitemap.xml");
            doc.Load(pathXml);
            XmlNodeList nodes = doc.GetElementsByTagName("url");

            try
            {
                foreach (XmlNode node in nodes)
                {
                    foreach (XmlAttribute attribute in node.Attributes)
                    {
                        if ((attribute.Name == "id") && (attribute.Value == id))

                        {

                            node.ParentNode.RemoveAll();
                            doc.Save(pathXml);
                        }
                    }
                }


            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
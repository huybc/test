﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Mi.Action.Core;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.Account;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Common.ChannelConfig;
using Mi.Entity.Base.News;
using Hoang_Long.Core.Helper;
using Mi.Hoang_Long.Core.Helper;
using Detector = Mi.Action.MobileUtils.Detector;

namespace Hoang_Long.Core.Helper
{
    public class UIHelper
    {
        public static readonly Assembly CurrentActionAssembly = Assembly.GetExecutingAssembly();
        public static string EncriptMd5(string str)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(str);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
        public static string FormatCurrency(string currencyCode, object amount, bool unit, bool hideNull = false)
        {
            string amountReturn = string.Empty;

            if (amount != null)
            {
                decimal _amount = Utility.ConvertToDecimal(amount);
                if (hideNull && _amount <= 0) { return amountReturn; }
                CultureInfo culture = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                                       let r = new RegionInfo(c.LCID)
                                       where r != null
                                       && r.ISOCurrencySymbol.ToUpper() == currencyCode.ToUpper()
                                       select c).FirstOrDefault();
                if (culture == null)
                {
                    // fall back to current culture if none is found
                    // you could throw an exception here if that's not supposed to happen
                    culture = CultureInfo.CurrentCulture;

                }
                culture = (CultureInfo)culture.Clone();
                culture.NumberFormat.CurrencySymbol = currencyCode;
                culture.NumberFormat.CurrencyPositivePattern = culture.NumberFormat.CurrencyPositivePattern == 0 ? 2 : 3;
                var cnp = culture.NumberFormat.CurrencyNegativePattern;
                switch (cnp)
                {
                    case 0: cnp = 14; break;
                    case 1: cnp = 9; break;
                    case 2: cnp = 12; break;
                    case 3: cnp = 11; break;
                    case 4: cnp = 15; break;
                    case 5: cnp = 8; break;
                    case 6: cnp = 13; break;
                    case 7: cnp = 10; break;
                }
                culture.NumberFormat.CurrencyNegativePattern = cnp;
                if (unit)
                {
                    amountReturn = _amount.ToString("C" + ((_amount % 1) == 0 ? "0" : "2"), culture).Replace("VND", "đ");
                }
                else
                {
                    amountReturn = _amount.ToString("C" + ((_amount % 1) == 0 ? "0" : "2"), culture).Replace("VND", "");

                }

            }
            return amountReturn;
        }
        public static string GetDateName(DateTime date)
        {
            var result = "";
            var dateNumber = (int)date.DayOfWeek;
            switch (dateNumber)
            {
                case 0:
                    result = "Chủ nhật";
                    break;
                case 1:
                    result = "Thứ hai";
                    break;
                case 2:
                    result = "Thứ ba";
                    break;
                case 3:
                    result = "Thứ tư";
                    break;
                case 4:
                    result = "Thứ năm";
                    break;
                case 5:
                    result = "Thứ sáu";
                    break;
                case 6:
                    result = "Thứ bảy";
                    break;
            }
            return result;
        }
        public static bool IsValiFolder(string folder)
        {
            const string str = "CON,PRN,AUX,CLOCK$,NUL,COM1,COM2,COM3,COM4,COM5,COM6,COM7,COM8,COM9,LPT1,LPT2,LPT3,LPT4,LPT5,LPT6,LPT7,LPT8,LPT9";
            var folders = str.Split(',');
            int pos = Array.IndexOf(folders, folder);
            if (pos > -1)
            {
                return false;
            }
            return true;
        }
        public string ToText(string input)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(@"<html><body>" + input + "</body></html>");
            return doc.DocumentNode.SelectSingleNode("//body").InnerText;
        }
        public static string Md5HasCode(string plainText)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(plainText));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            return returnValue.ToString();
        }
        public static string TrimByWord(object title, int numberOfWords = 20, string afterFix = "...")
        {
            if (numberOfWords < 0) numberOfWords = 10;
            var result = title.ToString();
            var resultArray = result.Split(new Char[] { ' ' });

            if (resultArray.Length > numberOfWords)
            {
                List<string> Slice = new List<string>(resultArray).GetRange(0, numberOfWords);
                result = string.Join(" ", Slice.ToArray()) + afterFix;
            }
            return result;
        }
        public static string TrimByWord(object title, int numberOfWords = 20, string splitChar = "", string afterFix = "...")
        {
            if (numberOfWords < 0) numberOfWords = 10;
            var result = title.ToString();
            var resultArray = result.Split(splitChar.ToCharArray());

            if (resultArray.Length > numberOfWords)
            {
                List<string> Slice = new List<string>(resultArray).GetRange(0, numberOfWords);
                result = string.Join(splitChar, Slice.ToArray()) + afterFix;
            }
            return result;
        }
        public static string GetLongDate(object dt)
        {
            if (dt == null) return "";

            var dateTime = Convert.ToDateTime(dt);
            if (dateTime == DateTime.MinValue)
            {
                return "-";
            }
            var dw = string.Empty;
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    dw = "Thứ Hai";
                    break;
                case DayOfWeek.Tuesday:
                    dw = "Thứ Ba";
                    break;
                case DayOfWeek.Wednesday:
                    dw = "Thứ Tư";
                    break;
                case DayOfWeek.Thursday:
                    dw = "Thứ Năm";
                    break;
                case DayOfWeek.Friday:
                    dw = "Thứ Sáu";
                    break;
                case DayOfWeek.Saturday:
                    dw = "Thứ Bảy";
                    break;
                case DayOfWeek.Sunday:
                    dw = "Chủ Nhật";
                    break;
            }

            return String.Format("{0}, {1}", dw, GetFullDate(dt));
        }
        public static string EventProcess(DateTime start, DateTime stop)
        {
            string htm = "";
            try
            {
                if (start > DateTime.Now)
                {
                    htm = "<a href=\"/su-kien\" class=\"btn btn-green btn-block\">Sắp diễn ra</a>";
                }
                if (start <= DateTime.Now && stop >= DateTime.Now)
                {
                    htm = "<a href=\"/su-kien\" class=\"btn btn-primary btn-block\">Đang diễn ra</a>";
                }
                if (stop <= DateTime.Now)
                {
                    htm = "<a href=\"/su-kien\" class=\"btn btn-default btn-block\">Đã diễn ra</a>";
                }

            }
            catch (Exception)
            {

                return "n/a";
            }
            return htm;
        }
        public static string GetTetxStatusShop(object status)
        {
            var dw = string.Empty;
            var data = status.ToString();
            if (!string.IsNullOrEmpty(data))
            {

                switch (data)
                {
                    case "1":
                        dw = "<i data=\"publish\" class=\"fa fa-check-circle publish\"></i><em>Đang hoạt động</em>";

                        break;
                    case "2":
                        dw = "<i data=\"unpublish\" class=\"fa fa-check-circle unpublish\"></i><em>Tạm ngừng</em>";
                        break;
                    case "3":
                        dw = "<i class=\"fa fa-check-circle close\"></i><em>Đóng cửa</em>";
                        break;

                }
            }

            return dw;
        }
        public static string GetTetxStatusProduct(object status)
        {
            var dw = string.Empty;
            var data = status.ToString();
            if (!string.IsNullOrEmpty(data))
            {

                switch (data)
                {
                    case "1":
                        dw = "<i data=\"publish\" class=\"fa fa-check-circle publish\"></i><em>Hiển thị</em>";

                        break;
                    case "2":
                        dw = "<i data=\"unpublish\" class=\"fa fa-check-circle unpublish\"></i><em>Ẩn</em>";
                        break;


                }
            }

            return dw;
        }
        public static string FomatMoney(object obj)
        {
            string data = string.Empty;

            if (!string.IsNullOrEmpty(obj.ToString()))
            {
                CultureInfo cultureInfo = new CultureInfo("vi-VN");
                data = String.Format(cultureInfo, "{0:C}", obj);
            }
            else
            {
                data = "0.0 đ";
            }
            return data;
        }
        public static bool IsExpiredTime(object obj, int timeOut, object createdBy)
        {
            var flag = false;
            try
            {
                double returMinute = (DateTime.Now - Convert.ToDateTime(obj)).TotalMinutes;
                if (returMinute >= timeOut && createdBy.Equals(GetCurrentUserName()))
                {
                    flag = true;
                }
            }
            catch (Exception)
            {

                return flag;
            }
            return flag;
        }

        public static bool IsMobile()
        {
            try
            {
                Detector detect = new Detector(HttpContext.Current);
                if (detect.DetectSmartPhone() || detect.DetectTierTablet())
                {
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
            return false;
        }

        public static string GetFullDate(object dt)
        {
            if (dt == null) return "";
            var dateTime = Convert.ToDateTime(dt);
            return string.Format("{0}/{1}/{2} {3}:{4}", dateTime.Day, dateTime.Month, dateTime.Year, dateTime.Hour,
                dateTime.Minute > 10 ? dateTime.Minute + "" : ("0" + dateTime.Minute));
        }
        public static string GetDateTime(object dt)
        {
            if (dt == null) return "";
            var dateTime = Convert.ToDateTime(dt);
            return string.Format("{0}/{1}/{2} {3}:{4}", dateTime.Day, dateTime.Month, dateTime.Year, dateTime.Hour,
                dateTime.Minute > 10 ? dateTime.Minute + "" : ("0" + dateTime.Minute));
        }
        public static string GetOnlyDate(object dt, string symbol = null)
        {
            var dateTime = Convert.ToDateTime(dt);
            if (symbol == null)
            {
                return string.Format("{0}/{1}/{2}", dateTime.Day, dateTime.Month, dateTime.Year);
            }
            else
            {
                var frameFormat = "{0}" + symbol + "{1}" + symbol + "{2}";
                return string.Format(frameFormat, dateTime.Day, dateTime.Month, dateTime.Year);
            }
        }
        public static string GetOnlyDateFormarMonthYear(object dt, string symbol = null)
        {
            string str = string.Empty;
            var dateTime = Convert.ToDateTime(dt);
            if (symbol == null && (DateTime.Compare(DateTime.MinValue, dateTime) != 0))

            {
                str = string.Format("{0}/{1}", dateTime.Month, dateTime.Year);
            }
            else
            {


                str = "Đến nay";
            }
            return str;
        }
        public static string GetOnlyDateWithAddZero(object dt, string symbol = null)
        {
            var dateTime = Convert.ToDateTime(dt);
            if (symbol == null)
            {
                return string.Format("{0}/{1}/{2}", dateTime.Day, dateTime.Month, dateTime.Year);
            }
            else
            {
                var frameFormat = "{0}" + symbol + "{1}" + symbol + "{2}";
                return string.Format(frameFormat, dateTime.Day < 10 ? "0" + dateTime.Day.ToString() : dateTime.Day.ToString(), dateTime.Month < 10 ? "0" + dateTime.Month.ToString() : dateTime.Month.ToString(), dateTime.Year);
            }
        }
        public static string GetOnlyTime(object dt, bool addZeroToFist = false)
        {
            var dateTime = Convert.ToDateTime(dt);
            if (!addZeroToFist)
            {
                return string.Format("{0}:{1}", dateTime.Hour, dateTime.Minute);
            }
            else
            {
                return string.Format("{0}:{1}", dateTime.Hour < 10 ? "0" + dateTime.Hour.ToString() : dateTime.Hour.ToString(), dateTime.Minute < 10 ? "0" + dateTime.Minute.ToString() : dateTime.Minute.ToString());
            }
        }
        public static string GetTicks(object dt, bool isReturnNow = false)
        {
            if (dt == null)
            {
                if (isReturnNow)
                {
                    dt = DateTime.Now;
                }
                else
                {
                    return "0";
                }
            }
            var dateTime = Convert.ToDateTime(dt);
            if (dateTime == DateTime.MinValue)
            {
                if (isReturnNow)
                {
                    dateTime = DateTime.Now;
                }
                else
                {
                    return "0";
                }
            }
            return dateTime.Ticks.ToString();
        }

        public static bool CompareDateNow(DateTime strDate)
        {
            if (DateDiff("mi", DateTime.Now, strDate) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static double DateDiff(string datepart, DateTime startDate, DateTime endDate)
        {
            var diff = endDate.Subtract(startDate);
            double result = 0;
            switch (datepart)
            {
                case "mi":
                    result = diff.TotalMinutes / 1000 * 60;
                    break;
                case "h":
                    result = diff.TotalHours / 1000 * 60 * 60;
                    break;
                case "d":
                    result = diff.TotalDays / 1000 * 60 * 60 * 24;
                    break;
                case "m":
                    result = diff.TotalDays / 1000 * 60 * 60 * 24 * 30;
                    break;
            }
            return result;
        }

        public static string NumberFormat(double num, double decimalNum, bool bolLeadingZero, bool bolParens, bool bolCommas)
        {

            if (num == 0) return "0";

            var tmpNum = num;
            var iSign = num < 0 ? -1 : 1;

            tmpNum *= Math.Pow(10, decimalNum);
            tmpNum = Math.Round(Math.Abs(tmpNum));
            tmpNum /= Math.Pow(10, decimalNum);
            tmpNum *= iSign;

            var tmpNumStr = tmpNum.ToString();

            if (!bolLeadingZero && num < 1 && num > -1 && num != 0)
                if (num > 0)
                    tmpNumStr = tmpNumStr.Substring(1, tmpNumStr.Length);
                else
                    tmpNumStr = "-" + tmpNumStr.Substring(2, tmpNumStr.Length);

            if (bolCommas && (num >= 1000 || num <= -1000))
            {
                var iStart = tmpNumStr.IndexOf(".");
                if (iStart < 0)
                    iStart = tmpNumStr.Length;
                else
                {
                    tmpNumStr = tmpNumStr.Replace(".", ",");
                }

                iStart -= 3;
                while (iStart >= 1)
                {
                    tmpNumStr = tmpNumStr.Substring(0, iStart) + "." + tmpNumStr.Substring(iStart, tmpNumStr.Length);
                    iStart -= 3;
                }
            }
            if (bolParens && num < 0)
                tmpNumStr = "(" + tmpNumStr.Substring(1, tmpNumStr.Length) + ")";
            return tmpNumStr;
        }

        public static string ToTipsyTitle(object title)
        {
            return title == null ? "" : title.ToString().Replace("\"", "\'");
        }
        public static void RedirectError()
        {
            HttpContext.Current.Response.Redirect("/");
        }

        #region Host Thumb, Static...

        public static string Thumb_W(int width, string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                if (fileName.StartsWith("http://") || fileName.StartsWith("https://"))
                {
                    return fileName;
                }
                else
                {

                    fileName = ("/" + CmsChannelConfiguration.GetAppSetting("StorageUrl") + "/" + fileName).Replace("//", "/").Replace("http:/", "http://") + "?w=" + width;

                }
            }
            else
            {
                //fileName = CmsChannelConfiguration.GetAppSetting("Domain") + "/Statics/Images/logo.png";
                fileName = "/CMS/Themes/Images/no-thumbnai.png";
            }
            return fileName;
        }
        public static string ThumbFileStore_W(int width, string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                if (fileName.StartsWith("http://") || fileName.StartsWith("https://"))
                {
                    return fileName;
                }
                else
                {

                    fileName = (CmsChannelConfiguration.GetAppSetting("DFileStore") + fileName).Replace("//", "/").Replace("http:/", "http://") + "?w=" + width;

                }
            }
            else
            {
                //fileName = CmsChannelConfiguration.GetAppSetting("Domain") + "/Statics/Images/logo.png";
                fileName = "/Statics/images/reapair-logo-black.png";
            }
            return fileName;
        }

        public static string GetPhotoUrlForSave(string url, bool isCheckHostImage = true)
        {
            url = url.ToLower();
            var hostImage =
                CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD").ToLower();
            if (isCheckHostImage)
            {
                if (!url.StartsWith(hostImage))
                {
                    return "";
                }
            }
            url = url.Replace(hostImage, "");
            List<string> regs = new List<string>();
            regs.Add(@"zoom/\w+/");
            regs.Add(@"thumb/\w+/");
            regs.Add(@"thumb_w/\w+/");
            regs.Add(@"thumb_h/\w+/");
            foreach (var s in regs)
            {
                Regex rgx = new Regex(s, RegexOptions.IgnoreCase);
                url = rgx.Replace(url, "");
            }
            return url;
        }

        #endregion

        #region util for compare eval data
        public static bool IsTrue(object evalData)
        {
            try
            {
                return bool.Parse(evalData.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool IsEvalString(object evalData, string compareTo = "")
        {
            try
            {
                if (evalData == null)
                {
                    return false;
                }
                else
                {
                    if (evalData.ToString().ToLower().Trim() == compareTo)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        //public static string BuildRealImageUrl(object url, string noImageUrl = "")
        //{
        //    if (url != null && (url + "") != "")
        //        return CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + url;
        //    else
        //    {
        //        if (noImageUrl == "")
        //            return CmsChannelConfiguration.GetAppSetting("Domain") + "/images/avatar1.gif";
        //        else
        //            return CmsChannelConfiguration.GetAppSetting(Constants.HOST_BOOKMARK) + noImageUrl;
        //    }
        //}
        //public static string FormatThumb(int width, int height, object url, string noImageUrl = "")
        //{
        //    var hostThumbFormat = CmsChannelConfiguration.GetAppSetting(Constants.HOST_CMS_THUMB_FORMAT);

        //    if (url == null || url.ToString() == "" || url.ToString().Length < 5)
        //    {
        //        if (noImageUrl == "")
        //            url = CmsChannelConfiguration.GetAppSetting("Domain") + "Statics/images/reapair-logo-black.png";
        //        else
        //            url = CmsChannelConfiguration.GetAppSetting("Domain") + noImageUrl;
        //    }
        //    if (url.ToString().StartsWith("http://") || url.ToString().StartsWith("https://"))
        //    {
        //        return url.ToString();
        //    }
        //    else
        //    {
        //        return string.Format(hostThumbFormat, width, height, url);
        //    }
        //}
        public static string GetTime(object dt)
        {
            var dateTime = Convert.ToDateTime(dt);
            var dw = string.Empty;

            var hour = dateTime.Hour < 10 ? "0" + dateTime.Hour.ToString() : dateTime.Hour.ToString();
            var min = dateTime.Minute < 10 ? "0" + dateTime.Minute.ToString() : dateTime.Minute.ToString();

            return String.Format("{0}:{1}", hour, min);
        }
        public static string ConvertToTimeAgo(object dt)
        {
            var dateTime = Convert.ToDateTime(dt);
            if (DateTime.Now.AddDays(-1) > dateTime)
            {
                return GetFullDate(dt) + ":nottimeago";
            }
            else
            {
                return dateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz");
            }
        }

        public static string GetTime(DateTime dt)
        {
            string strTime = string.Empty;

            var curentDate = DateTime.Now;
            var dif = curentDate.Subtract(dt);

            if (dif.TotalSeconds < 60)
                strTime = "Vài giây trước";
            else if (dif.TotalSeconds < 3600)
                strTime = Math.Floor(dif.TotalMinutes) + " phút trước";
            else if (dif.TotalSeconds < 3600 * 24)
                strTime = Math.Floor(dif.TotalHours) + " giờ trước";
            else
            {
                var yy = dt.Year;
                var mm = dt.Month;
                var dd = dt.Day;
                var hr = dt.Hour;
                var mi = dt.Minute;
                var ss = dt.Second;

                strTime = "";
                strTime += dd < 10 ? ('0' + dd.ToString()) : dd.ToString();
                strTime += "/" + (mm < 10 ? ('0' + mm.ToString()) : mm.ToString());
                strTime += "/" + (yy < 10 ? ('0' + yy.ToString()) : yy.ToString());
                strTime += " " + (hr < 10 ? ('0' + hr.ToString()) : hr.ToString());
                strTime += ":" + (mi < 10 ? ('0' + mi.ToString()) : mi.ToString());
            }

            return strTime;
        }

        public static string ConvertToDateTimePicker(object datetime, bool isReturnNow = false)
        {
            if (datetime == null)
            {
                if (isReturnNow)
                {
                    datetime = DateTime.Now;
                }
                else
                {
                    datetime = DateTime.MinValue;
                }
            }
            DateTime dt = DateTime.Parse(datetime.ToString());
            if (dt == DateTime.MinValue)
            {
                if (isReturnNow)
                {
                    dt = DateTime.Now;
                }
                else
                {
                    return "";
                }
            }

            return dt.ToString("dd/MM/yyyy HH:mm");
        }
        public static string ConvertToDatePicker(object datetime, bool isReturnNow = false)
        {
            if (datetime == null)
            {
                if (isReturnNow)
                {
                    datetime = DateTime.Now;
                }
                else
                {
                    datetime = DateTime.MinValue;
                }
            }
            DateTime dt = DateTime.Parse(datetime.ToString());
            if (dt == DateTime.MinValue)
            {
                if (isReturnNow)
                {
                    dt = DateTime.Now;
                }
                else
                {
                    return "";
                }
            }

            return dt.ToString("dd/MM/yyyy");
        }
        public static string ConvertToDateMonthYear(object datetime, bool isReturnNow = false)
        {
            if (datetime == null)
            {
                if (isReturnNow)
                {
                    datetime = DateTime.Now;
                }
                else
                {
                    datetime = DateTime.MinValue;
                }
            }
            DateTime dt = DateTime.Parse(datetime.ToString());
            if (dt == DateTime.MinValue)
            {
                if (isReturnNow)
                {
                    dt = DateTime.Now;
                }
                else
                {
                    return "";
                }
            }

            return dt.ToString("MM/yyyy");
        }

        public static string GetCurrentUserName()
        {
            return PolicyProviderManager.Provider.GetAccountName();
        }

        public static int CheckAllowTimeAgo(object dt)
        {
            var dateTime = Convert.ToDateTime(dt);
            return (DateTime.Now.AddDays(-1) - dateTime) > TimeSpan.Zero ? 1 : 0;
        }

        public static string EscapeTitle(object title)
        {
            return title.ToString().Replace("'", "&apos;").Replace("\"", "&quot;");
        }

        public static string FormatNumber(object number)
        {
            return string.Format("{0:n0}", number);
        }
        public static string FormatMobile(object mobile)
        {
            if (mobile == null)
            {
                return "---";
            }
            else
            {
                if (string.IsNullOrEmpty(mobile.ToString()))
                {
                    return "---";
                }
                else
                {
                    return mobile.ToString();
                }
            }
        }
        public static string FormatSex(object sex)
        {
            if (sex == null)
            {
                return "---";
            }
            else
            {
                if (string.IsNullOrEmpty(sex.ToString()))
                {
                    return "---";
                }
                else
                {
                    if (int.Parse(sex.ToString()) == 0)
                    {
                        return "Nữ";
                    }
                    else
                    {
                        return "Nam";
                    }
                }
            }
        }

        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public static string FormatFileSize(Int64 value)
        {
            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));
            if (mag > 0)
                return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
            return "n/a";
        }
        public static string FormatFileSize(Double value)
        {
            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));
            if (mag > 0)
                return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
            return "n/a";
        }

        public static bool isSmartPhone()
        {
            return isTablet();
        }
        public static bool isTablet()
        {
            Detector detect = new Detector(HttpContext.Current);
            return (detect.DetectAndroidTablet() || detect.DetectWebOSTablet() || detect.DetectIos() ||
                    detect.DetectSmartPhone());
        }


        #region converData

        public static int BoolToInt(object val)
        {
            if (val == null) return 0;
            if (Utility.ConvertToBoolean(val)) return 1;
            return 0;
        }

        #endregion

        public static string ReplaceQuote(object content)
        {
            if (content != null)
            {
                return content.ToString().Replace("'", "&apos;");
            }
            else
            {
                return "";
            }
        }
        public static string ReFormatPhoneNumber(object phoneNumber)
        {
            if (phoneNumber == null)
            {
                return "";
            }
            else
            {
                return (phoneNumber + "").Replace(" ", "").Replace("+84", "0").Replace(".", "");
            }
        }

        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex).Replace("&amp;", ";").Replace("&nbsp;", " ");
        }

        public static string GeneratorColorAvatar(string chr)
        {
            if (string.IsNullOrEmpty(chr.ToString()))
            {
                return "btn-default";
            }
            chr = (chr + "").ToCharArray()[0].ToString().ToUpper();
            if (chr == "A" || chr == "C")
            {
                return "btn-primary";
            }
            if (chr == "B")
            {
                return "btn-angry";
            }
            if (chr == "G" || chr == "H" || chr == "K" || chr == "Y")
            {
                return "btn-success";
            }
            if (chr == "M" || chr == "N" || chr == "L" || chr == "P")
            {
                return "btn-info";
            }
            if (chr == "Q" || chr == "S" || chr == "U")
            {
                return "btn-warning";
            }
            if (chr == "T")
            {
                return "btn-infidelity";
            }
            if (chr == "V")
            {
                return "btn-happy";
            }
            if (chr == "X" || chr == "O" || chr == "D")
            {
                return "btn-danger";
            }
            return "btn-default";
        }
        public static string GeneratorColorAvatar(object _chr)
        {
            if (string.IsNullOrEmpty(_chr.ToString()))
            {
                return "btn-default";
            }

            var chr = _chr.ToString().ToCharArray()[0].ToString().ToUpper();

            if (chr == "A" || chr == "C")
            {
                return "btn-primary";
            }
            if (chr == "B")
            {
                return "btn-angry";
            }
            if (chr == "G" || chr == "H" || chr == "K" || chr == "Y")
            {
                return "btn-success";
            }
            if (chr == "M" || chr == "N" || chr == "L" || chr == "P")
            {
                return "btn-info";
            }
            if (chr == "Q" || chr == "S" || chr == "U")
            {
                return "btn-warning";
            }
            if (chr == "T")
            {
                return "btn-infidelity";
            }
            if (chr == "V")
            {
                return "btn-happy";
            }
            if (chr == "X" || chr == "O" || chr == "D")
            {
                return "btn-danger";
            }
            return "btn-default";
        }


        public static void SetFormValue(string key, string value)
        {
            var collection = HttpContext.Current.Request.Form;

            // Get the "IsReadOnly" protected instance property.
            var propInfo = collection.GetType().GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);

            // Mark the collection as NOT "IsReadOnly"
            propInfo.SetValue(collection, false, new object[] { });

            // Change the value of the key.
            collection[key] = value;

            // Mark the collection back as "IsReadOnly"     
            propInfo.SetValue(collection, true, new object[] { });
        }

        public static string RemoveScriptTagInHtml(object inputHtml)
        {
            return Regex.Replace(inputHtml == null ? "" : inputHtml.ToString(), "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        }

        public static string Version
        {
            get { return CmsChannelConfiguration.GetAppSetting("Version"); }
        }

        //public static string StripHtml(string source)
        //{
        //    try
        //    {
        //        string result;

        //        // Remove HTML Development formatting
        //        // Replace line breaks with space
        //        // because browsers inserts space
        //        result = source.Replace("\r", " ");
        //        // Replace line breaks with space
        //        // because browsers inserts space
        //        result = result.Replace("\n", " ");
        //        // Remove step-formatting
        //        result = result.Replace("\t", string.Empty);
        //        // Remove repeating spaces because browsers ignore them
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                                                              @"( )+", " ");

        //        // Remove the header (prepare first by clearing attributes)
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*head([^>])*>", "<head>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"(<( )*(/)( )*head( )*>)", "</head>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(<head>).*(</head>)", string.Empty,
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // remove all scripts (prepare first by clearing attributes)
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*script([^>])*>", "<script>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"(<( )*(/)( )*script( )*>)", "</script>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        //result = System.Text.RegularExpressions.Regex.Replace(result,
        //        //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
        //        //         string.Empty,
        //        //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"(<script>).*(</script>)", string.Empty,
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // remove all styles (prepare first by clearing attributes)
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*style([^>])*>", "<style>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"(<( )*(/)( )*style( )*>)", "</style>",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(<style>).*(</style>)", string.Empty,
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // insert tabs in spaces of <td> tags
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*td([^>])*>", "\t",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // insert line breaks in places of <BR> and <LI> tags
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*br( )*>", "\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*li( )*>", "\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // insert line paragraphs (double line breaks) in place
        //        // if <P>, <DIV> and <TR> tags
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*div([^>])*>", "\r\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*tr([^>])*>", "\r\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<( )*p([^>])*>", "\r\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // Remove remaining tags like <a>, links, images,
        //        // comments etc - anything that's enclosed inside < >
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"<[^>]*>", string.Empty,
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // replace special characters:
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @" ", " ",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&bull;", " * ",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&lsaquo;", "<",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&rsaquo;", ">",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&trade;", "(tm)",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&frasl;", "/",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&lt;", "<",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&gt;", ">",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&copy;", "(c)",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 @"&reg;", "(r)",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        // Remove all others. More can be added, see
        //        // http://hotwired.lycos.com/webmonkey/reference/special_characters/
        //        //result = System.Text.RegularExpressions.Regex.Replace(result,
        //        //         @"&(.{2,6});", string.Empty,
        //        //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // for testing
        //        //System.Text.RegularExpressions.Regex.Replace(result,
        //        //       this.txtRegex.Text,string.Empty,
        //        //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //        // make line breaking consistent
        //        result = result.Replace("\n", "\r");

        //        // Remove extra line breaks and tabs:
        //        // replace over 2 breaks with 2 and over 4 tabs with 4.
        //        // Prepare first to remove any whitespaces in between
        //        // the escaped characters and remove redundant tabs in between line breaks
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\r)( )+(\r)", "\r\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\t)( )+(\t)", "\t\t",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\t)( )+(\r)", "\t\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\r)( )+(\t)", "\r\t",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        // Remove redundant tabs
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\r)(\t)+(\r)", "\r\r",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        // Remove multiple tabs following a line break with just one tab
        //        result = System.Text.RegularExpressions.Regex.Replace(result,
        //                 "(\r)(\t)+", "\r\t",
        //                 System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        //        // Initial replacement target string for line breaks
        //        string breaks = "\r\r\r";
        //        // Initial replacement target string for tabs
        //        string tabs = "\t\t\t\t\t";
        //        for (int index = 0; index < result.Length; index++)
        //        {
        //            result = result.Replace(breaks, "\r\r");
        //            result = result.Replace(tabs, "\t\t\t\t");
        //            breaks = breaks + "\r";
        //            tabs = tabs + "\t";
        //        }

        //        // That's it.
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return source;
        //    }
        //}


        public static string UserAvatar(object fileName, object fullName)
        {
            string imagePath = fileName.ToString();
            if (!string.IsNullOrEmpty(imagePath))
            {
                if (imagePath.StartsWith("http://") || imagePath.StartsWith("https://"))
                {

                    return string.Format("<img src=\"" + imagePath + "\" alt=\"" + fullName + "\"  data-name='{0}'/>", fullName);
                }
                else
                {

                    return string.Format("<img alt=\"" + fullName + "\" src=\"" + (CmsChannelConfiguration.GetAppSetting(Constants.HOST_CMS_THUMB) + fileName).Replace(
                        "//", "/")
                        .Replace("http:/", "http://") + "\"  data-name='{0}'/>", fullName);


                }
            }
            else
            {
                try
                {
                    var avatar = fileName + "";

                    if (string.IsNullOrEmpty(avatar))
                    {
                        var nameA = (fullName + "").Split(new[] { ' ' });
                        var name = "";
                        var first = "";
                        int i = 0;
                        foreach (var n in nameA)
                        {
                            name += (n.ToCharArray()[0] + "").ToUpper();
                            if (i < nameA.Length - 1)
                            {
                                name += ".";
                            }
                            i++;
                        }
                        avatar = string.Format("<img alt=\"" + fullName + "\" class='RCharAvatar' data-name='{0}'/>", name);
                    }
                    else
                    {
                        avatar = string.Format("<img class=\"_avatar\" src='{0}'/>", CmsChannelConfiguration.GetAppSetting("StorageUrl") + avatar);
                    }
                    return avatar;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }

        }
        public static string GetUserAvatar(object fullName, object _avatar)
        {
            try
            {
                var avatar = _avatar + "";

                if (string.IsNullOrEmpty(avatar))
                {
                    var nameA = (fullName + "").Split(new[] { ' ' });
                    var name = "";
                    var first = "";
                    int i = 0;
                    foreach (var n in nameA)
                    {
                        name += (n.ToCharArray()[0] + "").ToUpper();
                        if (i < nameA.Length - 1)
                        {
                            name += ".";
                        }
                        i++;
                    }
                    avatar = string.Format("<img alt=\"" + fullName + "\" class='RCharAvatar' data-name='{0}'/>", name);
                }
                else
                {
                    avatar = string.Format("<img class=\"_avatar\" src='{0}'/>", CmsChannelConfiguration.GetAppSetting("StorageUrl") + avatar);
                }
                return avatar;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string GetUserFullName(object userName)
        {
            var u = UserBo.GetUserByUsername(userName + "");
            if (u != null)
            {
                return u.FullName;
            }
            return "";
        }

        public static string EncryptQueryString(string url)
        {
            var fakeUrl = "http://repair.vn/";
            if (!url.StartsWith("http"))
            {
                url = fakeUrl + url;
            }
            Uri myUri = new Uri(url);
            var values = HttpUtility.ParseQueryString(myUri.Query);
            QueryStringHelper args = new QueryStringHelper();
            foreach (var val in values.AllKeys)
            {
                args[val] = values[val];
            }
            return args.ToString().ToLower();
        }
        public static string DecryptQueryString(string query, string key)
        {
            QueryStringHelper args = new QueryStringHelper(query);
            return args[key];
        }





        public static string GetUserProfileLink(object _username)
        {
            return string.Format("/tai-khoan/{0}.htm", EncryptQueryString("?email=" + _username));
        }

        public static string GetPageNumber(int pageSize, int totalRows)
        {
            return Math.Ceiling((double)totalRows / (double)pageSize) + "";
        }

        public static void CheckLoginWithRedirect()
        {
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                HttpContext.Current.Response.Redirect("/cpanel/login.htm?url=" + HttpContext.Current.Request.RawUrl);
            }
        }
        public static string ZoneType(object type)
        {
            string url = string.Empty;
            switch ((NewsType)type)
            {
                case NewsType.Tutorial:
                    url = "giai-phap";
                    break;
                case NewsType.Question:
                    url = "cau-hoi";
                    break;
                default:
                    break;

            }
            return url;
        }
        public static string GetMenuPost(object createdBy)
        {
            string html = string.Empty;
            var currentUser = UIHelper.GetCurrentUserName();

            if (currentUser.Equals(createdBy))
            {
                html = "<span class=\"btn-edit\">Sửa</span><span class=\"btn-del\" >Xóa</span>";
            }
            return html;
        }
        public static string GetDomain()
        {
            return HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
        }
        public static string QuestionCount(object obj, object Count)
        {
            var htm = string.Empty;
            if (Utility.ConvertToInt(obj) == (int)NewsType.Question)
            {
                htm = " <span title=\"" + Count + " giải pháp cho câu hỏi\" class=\"solution-count\"><i class=\"fa  fa-lightbulb-o\"></i><label>" + Count +
                      "</label></span>";
            }
            return htm;
        }
        public static string GetItemMenu(object userName, object id)
        {
            string el = string.Empty;
            if (GetCurrentUserName().Equals(userName))
            {
                el = "<a href=\"javascript:void(0)\" data-id=\"" + id + "\" class=\"edit-item about-save-btn profile-btn canel-btn\" title=\"Sửa\" ><i class=\"fa fa-pencil\"></i></a><a href=\"javascript:void(0)\" data-id=\"" + id + "\" class=\"del-item about-remove-btn profile-btn \" title=\"Xóa\" ><i class=\"fa fa-trash-o\"></i></a>";
            }
            return el;
        }
        public static string ItemMenuStick(object userName, bool isCorrect, object objectId)
        {
            string el = string.Empty;
            var obj = NewsBo.GetNewsDetailById((long)objectId);
            if (obj != null)
            {
                if (GetCurrentUserName().Equals(obj.CreatedBy))
                {
                    el += "u-current ";
                    if (isCorrect)
                    {
                        el += "IsCorrect";
                    }

                }
                else
                {
                    if (isCorrect)
                    {
                        el += "IsCorrect";
                    }
                }
            }


            return el;
        }

        public static string GetDoamin()
        {

            return CmsChannelConfiguration.GetAppSetting("Domain");
        }
        public static string GetConfigByName(string name)
        {
            string str = string.Empty;
            var config = CacheObjectBase.GetInstance<ConfigCached>().GetByConfigName(name);
            if (config != null)
            {
                str = config.ConfigValue;
            }
            return str;


        }
        public static string Canonical(string name)
        {
            if (HttpContext.Current.Request.Url.AbsolutePath == "/default.aspx")
            {
                return name;
            }
            else
            {

                return name + HttpContext.Current.Request.Url.AbsolutePath;
            }



        }
        public static ResponseData SendHttpReqest(object param)
        {
            var responseData = new ResponseData();
            try
            {
                var apiUrl = CmsChannelConfiguration.GetAppSetting("XAPIHttpRequest") + "?" + param;
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.Method = "GET";
                request.MaximumAutomaticRedirections = 1;
                request.AllowAutoRedirect = true;
                request.Referer = apiUrl;
                request.UserAgent = @"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5";
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = "";
                using (var r = new StreamReader(response.GetResponseStream()))
                {
                    responseString = r.ReadToEnd();
                }
                responseData.Success = true;
                responseData.Data = responseString;


            }
            catch (Exception ex)
            {
                responseData.Success = false;
                responseData.Message = ex.Message;
            }

            return responseData;

        }

    }
    public static class Utils
    {
        public static Int32 ToInt32Return0<T>(this T input)
        {
            int output = 0;
            if (null != input)
            {
                try
                {
                    output = Convert.ToInt32(input);
                }
                catch (Exception)
                {
                    return output;
                }
            }

            return output;
        }

    }
}
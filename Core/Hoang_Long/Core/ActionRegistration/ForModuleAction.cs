﻿using Hoang_Long.CMS.Modules.Authenticate.Actions;
using Hoang_Long.CMS.Modules.Config.Action;
using Hoang_Long.CMS.Modules.Customer.Action;
using Hoang_Long.CMS.Modules.FileManager.Action;
using Hoang_Long.CMS.Modules.News.Action;
using Hoang_Long.CMS.Modules.Project.Action;
using Hoang_Long.CMS.Modules.Service.Action;
using Hoang_Long.CMS.Modules.Slider.Action;
using Hoang_Long.CMS.Modules.Tag.Action;
using Hoang_Long.Core.RequestBase;
using Hoang_Long.CMS.Modules.Zone.Action;
using Hoang_Long.Pages.Action;
using Hoang_Long.CMS.Modules.DoiTac.Action;
using Hoang_Long.CMS.Modules.TinTuc.Action;
using Hoang_Long.CMS.Modules.GioiThieu.Action;
using Hoang_Long.CMS.Modules.CauHoi.Action;
//using Hoang_Long.Pages.Action;

namespace Hoang_Long.Core.ActionRegistration
{
    public class ForModuleAction : CmsAsyncRequestBase
    {
        static ForModuleAction()
        {
            var className = typeof(ForModuleAction).Name;
            RegisterAction(className, "fmanager", typeof(FileManagerActions), true);
            RegisterAction(className, "ui-action", typeof(UIActions), true);
            RegisterAction(className, "service", typeof(ServiceActions), true);
            RegisterAction(className, "authenticate", typeof(AuthenticateAction), true);
            RegisterAction(className, "customer", typeof(CustomerActions), true);
            RegisterAction(className, "zone", typeof(ZoneActions), true);
            //RegisterAction(className, "charge", typeof(ChargeActions), true);
            RegisterAction(className, "config", typeof(ConfigActions), true);
            RegisterAction(className, "adv", typeof(AdvActions), true);
            RegisterAction(className, "news", typeof(NewsActions), true);
            RegisterAction(className, "project", typeof(ProjectActions), true);
            RegisterAction(className, "coach", typeof(CoachAction), true);
            RegisterAction(className, "doitac", typeof(DoiTacActions), true);
            RegisterAction(className, "tintuc", typeof(TinTucActions), true);
            RegisterAction(className, "gioithieu", typeof(GioiThieuActions), true);
            RegisterAction(className, "cauhoi", typeof(CauHoiActions), true);
            //RegisterAction(className, "comment", typeof(CommentActions), true);
            //RegisterAction(className, "answer", typeof(AnswerActions), true);
            //RegisterAction(className, "profile", typeof(UserActions), true);
            //RegisterAction(className, "user", typeof(UserActions), true);
            RegisterAction(className, "tag", typeof(TagActions), true);
            //RegisterAction(className, "livecms", typeof(LiveCmsActions), true);
            //RegisterAction(className, "media", typeof(MediaActions), true);
            //RegisterAction(className, "photo", typeof(PhotoActions), true);
            //RegisterAction(className, "hanwha", typeof(HanwhaActions), true);
            //RegisterAction(className, "shop", typeof(ShopActions), true);
            //RegisterAction(className, "experience", typeof(ExperienceActions), true);
            //   RegisterAction(className, "contact", typeof(ContactActions), true);

        }
    }
}
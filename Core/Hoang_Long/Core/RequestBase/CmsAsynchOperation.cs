﻿using System;
using System.Threading;
using System.Web;

namespace Hoang_Long.Core.RequestBase
{
    public class CmsAsynchOperation : IAsyncResult
    {
        private readonly Object _state;
        private readonly AsyncCallback _callback;
        private readonly HttpContext _context;

        public bool IsCompleted { get; private set; }
        WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
        Object IAsyncResult.AsyncState { get { return _state; } }
        bool IAsyncResult.CompletedSynchronously { get { return false; } }

        public CmsAsynchOperation(AsyncCallback callback, HttpContext context, Object state)
        {
            _callback = callback;
            _context = context;
            _state = state;
            IsCompleted = false;
        }

        public void StartAsyncWork()
        {
            ThreadPool.QueueUserWorkItem(StartAsyncTask, null);
        }

        private void StartAsyncTask(Object workItemState)
        {
            //_context.Response.Write("<p>Completion IsThreadPoolThread is " + Thread.CurrentThread.IsThreadPoolThread + "</p>\r\n");
            //_context.Response.Write("Hello World from Async Handler!");
            IsCompleted = true;
            _callback(this);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Hoang_Long.CMS.Login" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đăng nhập hệ thống</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/CMS/Themes/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/CMS/Themes/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/CMS/Themes/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="/CMS/Themes/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="/CMS/Themes/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <!-- Core JS files -->

    <script type="text/javascript" src="/CMS/Themes/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->
    <!-- Theme JS files -->
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/core/app.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/pages/login.js"></script>
    <!-- /theme JS files -->
    <script>
        var R = {};
    </script>

    <%
        List<string> urls = new List<string>();
        List<string> cssUrls = new List<string>();
        urls.Add("/Core/InitClientConstants/Constants.ashx");
        //urls.Add("/Core/InitClientConstants/ClientSessionConstants.ashx");
    %>

    <%= IncludeResources(urls, ResourceType.Javascript) %>
    <style>
     /*body { background: url('/Statics/Images/background-poker-6.jpg') no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; }*/
    </style>
</head>

<body class="bg-slate-8001" style="background: url(/CMS/Themes/images/backgrounds/boxed_bg.png) ">

    <!-- Page container -->
    <div class="page-container login-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Advanced login -->

                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                            <h5 class="content-group-lg">Đăng nhập<small class="display-block">Thông tin đăng nhập</small></h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" id="EmailTxt" placeholder="Tài khoản">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" id="PwdTxt" class="form-control" placeholder="Mật khẩu">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked="checked">
                                        Ghi nhớ
									
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="javascript:void(0)">Tìm lại mật khẩu?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="button" id="LoginBtn" class="btn bg-blue btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
                        </div>

                        <div class="content-divider text-muted form-group"><span>or sign in with</span></div>
                        <ul class="list-inline form-group list-inline-condensed text-center">
                            <li><a href="javascript:void(0)" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                            <li><a href="javascript:void(0)" class="btn border-pink-300 text-pink-300 btn-flat btn-icon btn-rounded"><i class="icon-dribbble3"></i></a></li>
                            <li><a href="javascript:void(0)" class="btn border-slate-600 text-slate-600 btn-flat btn-icon btn-rounded"><i class="icon-github"></i></a></li>
                            <li><a href="javascript:void(0)" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>
                        </ul>


                    </div>

                    <!-- /advanced login -->


                    <!-- Footer -->
                    <div class="footer text-white">
                        &copy; 2016. <a href="#" class="text-white">Dev</a> by <a href="http://MiGroup.asia" class="text-white" target="_blank">MiGroup</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <script>
        var R = {};
    </script>
    <!-- /page container -->
    <script src="/Libs/core.js"></script>

    <script>
        $(function () {
            $('#LoginBtn').off('click').on('click', function () {
                DoLogin();
            });

            $('#EmailTxt,#PwdTxt').keypress(function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode === 13) {
                    DoLogin();
                }
            });
        });
        function DoLogin() {

            if (ValidFormForLogin()) {
                var email = $('#EmailTxt').val();
                var pass = $('#PwdTxt').val();
                $('.login-form').RModuleBlock();
                R.Post({
                    module: "authenticate",
                    params: {
                        email: email,
                        pass: pass
                    },
                    ashx: 'modulerequest.ashx',
                    action: "login",
                    success: function (res1) {
                        if (res1.Success) {
                            //console.log(1);
                            setTimeout(function () {
                                var p = R.GetURLParameter('url');
                                if (p == '') {
                                    location.href = '/cpanel/dashbroad.htm';
                                } else {

                                    window.location = p;
                                }
                            }, 1000);
                        } else {
                            var $email = $('#EmailTxt');
                            var $pass = $('#PwdTxt');
                            $email.addClass('error');
                            $pass.addClass('error');
                            alert("Đăng nhập không thành công !");
                        }
                        $('.login-form').RModuleUnBlock();
                    },
                    error: function () {
                        console.log('error');
                    }
                });
            }
        }
        function ValidFormForLogin() {
            var ok = true;
            $('input').removeClass('error');

            var $email = $('#EmailTxt');
            if ($.trim($email.val()) == '') {
                $email.addClass('error');
                ok = false;
            }
            var $pass = $('#PwdTxt');
            if ($.trim($pass.val()) == '') {
                $pass.addClass('error');
                ok = false;
            }
            return ok;
        };
    </script>
    <style>
        .error { border-color: red; }
    </style>
</body>
</html>

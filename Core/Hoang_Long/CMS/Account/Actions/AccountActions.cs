﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using Mi.BO.Base.Account;
using Mi.BO.Base.Security;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Hoang_Long.CMS.Core.SystemData;
using Mi.Action;
using Mi.Action.Core;
using Hoang_Long.Core.Helper;

namespace Hoang_Long.CMS.Account.Actions
{
    public class AccountActions : ActionBase
    {
        #region Implementation of IAction

        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }
        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }

            else
            {
                switch (functionName)
                {
                    case "init_main":
                        responseData = InitMain();
                        break;
                    case "init_update":
                        responseData = InitFormUpdateUser();
                        break;
                    case "edit":
                        responseData = EditPermissionUser();
                        break;
                    case "editmyprofile":
                        responseData = EditProfile();
                        break;
                    case "editmyprofilev2":
                        responseData = EditProfileV2();
                        break;
                    case "userlist":
                        responseData = GetUserList();
                        break;
                    case "update":
                        responseData = UpdatePermission();
                        break;

                    case "addaccount":
                        responseData = AddAccount();
                        break;
                    case "add_update_account":
                        responseData = UpdateUserInfo();
                        break;
                    case "updateprofile":
                        responseData = UpdateProfile();
                        break;
                    case "update_avatar":
                        responseData = UpdateAvatar();
                        break;
                    case "resetpwd":
                        responseData = ResetPassword();
                        break;
                    case "resetmypwd":
                        responseData = ResetMyPassword();
                        break;

                    case "switchrole":
                        responseData = SwitchCurrentRole();
                        break;
                    case "listrole":
                        responseData = GetPermissionForCurrentUsername();
                        break;

                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                }
            }
            return responseData;
        }


        #endregion


        #region main

        private ResponseData InitMain()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Modules\\Account\\Templates\\Main.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData EditPermissionUser()
        {
            ResponseData responseData;
            responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Modules\\Account\\Templates\\UpdatePermission.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData EditProfile()
        {
            ResponseData responseData;

            try
            {
                var user = UserBo.GetUserByUsername(PolicyProviderManager.Provider.GetAccountName());
                responseData = ConvertResponseData.CreateResponseData(user, null == user ? 0 : 1);
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            responseData.Content = TemplateUtils.LoadPage("\\Modules\\Account\\Templates\\UpdateMyProfile.aspx", HttpContext.Current.Request.Form);
            return responseData;
        }
        private ResponseData EditProfileV2()
        {
            ResponseData responseData;

            try
            {
                var user = UserBo.GetUserByUsername(PolicyProviderManager.Provider.GetAccountName());
                responseData = ConvertResponseData.CreateResponseData(user, null == user ? 0 : 1);
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            responseData.Content = TemplateUtils.LoadPage("\\Modules\\Account\\Templates\\UpdateMyProfileV2.aspx", HttpContext.Current.Request.Form);
            return responseData;
        }
        private ResponseData InitFormUpdateUser()
        {
            ResponseData responseData;
            var encryptUserId = GetQueryString.GetPost("userid", string.Empty);
            try
            {
                //var user = UserBo.GetUserWithPermissionDetailByUserId(encryptUserId);
                responseData = ConvertResponseData.CreateResponseData("", 0);
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            responseData.Content = TemplateUtils.LoadPage("\\Modules\\Account\\Templates\\AccountTemplate.aspx",
                                                            HttpContext.Current.Request.Form);
            return responseData;
        }

        private ResponseData GetUserList()
        {
            var responseData = new ResponseData();
            var pageIndex = GetQueryString.GetPost("page", 1);
            var pageSize = GetQueryString.GetPost("num", 10);
            var keyword = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var userStatus = UserStatus.Unknow;
            if (status == 1)
            {
                userStatus = UserStatus.Actived;
            }
            if (status == 0)
            {
                userStatus = UserStatus.Locked;
            }
            var templateId = GetQueryString.GetPost("templateId", 1);
            try
            {
                var totalRow = 0;
                //if (user != null)
                //{                
                // }
                if (templateId == 1)
                {
                    responseData = ConvertResponseData.CreateResponseData("", totalRow);
                    responseData.Content = TemplateUtils.LoadPage("\\Modules\\Account\\Templates\\ListAccount.aspx", HttpContext.Current.Request.Form);
                }
                else
                {
                    var user = UserBo.SearchUser(keyword, userStatus,
                                                           UserSortExpression.CreateDateDesc, pageIndex,
                                                           pageSize, ref totalRow);
                    responseData = ConvertResponseData.CreateResponseData(user, totalRow);
                    responseData.Content = "";
                }
            }
            catch (Exception ex)
            {
                responseData.Success = false;
                responseData.Message = "";
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }


        // vuongpm thêm mới. tupa edited 02/05/2013
        private ResponseData AddAccount()
        {
            ResponseData responseData;
            var encryptUserId = GetQueryString.GetPost("userid", string.Empty);
            if (string.IsNullOrEmpty(encryptUserId))
            {
                responseData = ConvertResponseData.CreateResponseData(null, 1);
                responseData.Success = true;
            }
            else
            {
                try
                {
                    var data = UserBo.GetById(encryptUserId);
                    responseData = ConvertResponseData.CreateResponseData(data, 1);
                }
                catch (Exception ex)
                {
                    responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            responseData.Content = TemplateUtils.LoadPage("\\Modules\\Account\\Templates\\UpdateProfile.aspx",
                                                          HttpContext.Current.Request.Form);

            return responseData;
        }

        // Vương thêm mới 
        public ResponseData UpdateUserInfo()
        {
            ResponseData responseData;

            var encryptUserId = GetQueryString.GetPost("userid", string.Empty);
            var fullName = GetQueryString.GetPost("fullname", string.Empty);
            var username = GetQueryString.GetPost("username", string.Empty);
            var password = GetQueryString.GetPost("password", string.Empty);
            var address = GetQueryString.GetPost("address", string.Empty);
            var description = GetQueryString.GetPost("description", string.Empty);
            var avatar = GetQueryString.GetPost("avatar", string.Empty);
            var mobile = GetQueryString.GetPost("mobile", string.Empty);
            var email = GetQueryString.GetPost("email", string.Empty);
            var birthday = GetQueryString.GetPost("birthday", DateTime.MinValue);
            int status = GetQueryString.GetPost("status", 1);
            try
            {
                var user = new UserEntity
                {
                    Id = 0,
                    EncryptId = encryptUserId,
                    Password = password,
                    UserName = username,
                    FullName = fullName,
                    Avatar = avatar,
                    Email = email,
                    Mobile = UIHelper.ReFormatPhoneNumber(mobile),
                    Status = status,
                    Address = address,
                    Birthday = birthday,
                    Description = description
                };
                string idr = string.Empty;
                if (string.IsNullOrEmpty(encryptUserId))
                {
                    var userId = 0;
                    responseData =
                        ConvertResponseData.CreateResponseData(UserBo.AddNew(user, ref userId, ref idr));
                    responseData.Data = userId;
                }
                else
                {
                    responseData =
                        ConvertResponseData.CreateResponseData(UserBo.UpdateProfile(user, PolicyProviderManager.Provider.GetAccountName()));
                }

                // if (responseData.Success) Users.RefreshListAllUser();

            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return responseData;
        }

        private ResponseData UpdatePermission()
        {
            ResponseData responseData;
            var encryptUserId = GetQueryString.GetPost("userid", string.Empty);
            var permissionWithZoneList = GetQueryString.GetPost("zonelist", string.Empty);
            var isFullPermission = GetQueryString.GetPost("IsFullPermission", false, false);
            var isFullZone = GetQueryString.GetPost("IsFullZone", false, false);
            try
            {

                var userPermissionEntityList = new List<UserPermissionEntity>();
                if (!isFullZone || !isFullPermission)
                {
                    var groupList = permissionWithZoneList.Split(';');
                    foreach (var group in groupList)
                    {
                        var match = Regex.Match(group, @"G(?<PermissionId>[0-9]+)\((?<ZoneIdList>[0-9,]+)\)");

                        if (!match.Success) continue;

                        var permissionId = int.Parse(match.Groups["PermissionId"].ToString());
                        var zoneIdList = match.Groups["ZoneIdList"].ToString();

                        var zoneIds = zoneIdList.Split(',');
                        foreach (var zoneId in zoneIds)
                        {
                            if (!string.IsNullOrEmpty(zoneId) && zoneId != "0")
                                userPermissionEntityList.Add(new UserPermissionEntity
                                {
                                    UserId = 0,
                                    PermissionId = permissionId,
                                    ZoneId = Utility.ConvertToInt(zoneId)
                                });
                        }
                    }
                }

                responseData =
                    ConvertResponseData.CreateResponseData(PermissionBo.UpdateUserPermissionByUserId(encryptUserId,
                                                                                                         userPermissionEntityList.ToArray(),
                                                                                                         isFullPermission,
                                                                                                         isFullZone,
                                                                                                         PolicyProviderManager.Provider.
                                                                                                             GetAccountName
                                                                                                             ()));

            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return responseData;
        }

        private ResponseData UpdateProfile()
        {
            ResponseData responseData;

            var encryptUserId = GetQueryString.GetPost("userid", string.Empty);
            var fullName = GetQueryString.GetPost("fullname", string.Empty);
            var username = GetQueryString.GetPost("username", string.Empty);
            var password = GetQueryString.GetPost("password", string.Empty);
            var address = GetQueryString.GetPost("address", string.Empty);
            var description = GetQueryString.GetPost("description", string.Empty);
            var avatar = GetQueryString.GetPost("avatar", string.Empty);
            var mobile = GetQueryString.GetPost("mobile", string.Empty);
            var email = GetQueryString.GetPost("email", string.Empty);
            var birthday = GetQueryString.GetPost("birthday", DateTime.MinValue);

            try
            {

                var user = new UserEntity
                {
                    Id = 0,
                    EncryptId = encryptUserId,
                    Password = password,
                    UserName = username,
                    FullName = fullName,
                    Avatar = avatar,
                    Email = email,
                    Mobile = mobile.Replace(" ", ""),
                    Address = address,
                    Birthday = birthday,
                    Description = description
                };

                responseData =
                    ConvertResponseData.CreateResponseData(UserBo.UpdateProfile(user,
                                                                                              PolicyProviderManager.Provider.
                                                                                                  GetAccountName
                                                                                                  ()));
                if (responseData.Success) Users.RefreshListAllUser();
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return responseData;
        }

        private ResponseData UpdateAvatar()
        {
            ResponseData responseData;
            var accountName = PolicyProviderManager.Provider.GetAccountName();
            var avatar = GetQueryString.GetPost("avatar", string.Empty);
            try
            {
                responseData =
                    ConvertResponseData.CreateResponseData(UserBo.UpdateAvatar(accountName,
                                                                                              avatar));

                if (responseData.Success) Users.RefreshListAllUser();
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }

        private ResponseData ResetPassword()
        {
            ResponseData responseData;
            var userId = GetQueryString.GetPost("userid", 0);
            var password = GetQueryString.GetPost("pwd", string.Empty);
            try
            {
                responseData =
                    ConvertResponseData.CreateResponseData(UserBo.ResetPassword(userId, password));
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }

        private ResponseData ResetMyPassword()
        {
            ResponseData responseData;
            var accountName = PolicyProviderManager.Provider.GetAccountName();
            var oldPassword = GetQueryString.GetPost("oldpass", string.Empty);
            var newPassword = GetQueryString.GetPost("newpass", string.Empty);
            var cfNewPassword = GetQueryString.GetPost("cfnewpass", string.Empty);
            try
            {
                responseData =
                    ConvertResponseData.CreateResponseData(UserBo.ResetMyPassword(accountName,
                                                                                              newPassword, cfNewPassword, oldPassword));
            }
            catch (Exception ex)
            {
                responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateErrorResponse(ex));
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }


        private ResponseData SwitchCurrentRole()
        {
            var currentUsername = PolicyProviderManager.Provider.GetAccountName();
            var permissionId = GetQueryString.GetQuery("role", 0);

            if (string.IsNullOrEmpty(currentUsername))
            {
                return
                    ConvertResponseData.CreateResponseData(
                        WcfResponseData.CreateErrorResponse((int)ErrorMapping.ErrorCodes.SwitchCurrentRoleUserNotFound,
                                                            ErrorMapping.Current[
                                                                ErrorMapping.ErrorCodes.SwitchCurrentRoleUserNotFound]));
            }
            if (permissionId != (int)EnumPermission.ArticleAdmin &&
                permissionId != (int)EnumPermission.ArticleEditor &&
                permissionId != (int)EnumPermission.ArticleReporter)
            {
                return
                    ConvertResponseData.CreateResponseData(
                        WcfResponseData.CreateErrorResponse((int)ErrorMapping.ErrorCodes.SwitchCurrentRoleInvalidRole,
                                                            ErrorMapping.Current[ErrorMapping.ErrorCodes.SwitchCurrentRoleInvalidRole
                                                                ]));
            }

            var permission = (EnumPermission)permissionId;

            if (!Role.HasRoleOnPermission(permission, currentUsername))
            {
                return
                    ConvertResponseData.CreateResponseData(
                        WcfResponseData.CreateErrorResponse((int)ErrorMapping.ErrorCodes.SwitchCurrentRoleUserNotHaveRole,
                                                            ErrorMapping.Current[ErrorMapping.ErrorCodes.SwitchCurrentRoleUserNotHaveRole
                                                                ]));
            }

            ResponseData responseData = ConvertResponseData.CreateResponseData(WcfResponseData.CreateSuccessResponse());
            //responseData.Data = PolicyProviderManager.Provider.GenCookieAuthenticate(currentUsername, permission);

            return responseData;
        }

        private ResponseData GetPermissionForCurrentUsername()
        {
            var currentUsername = PolicyProviderManager.Provider.GetAccountName();
            var data = PermissionBo.GetPermissionByUsername(currentUsername);
            return ConvertResponseData.CreateResponseData(data, data.Count);
        }

        #endregion main


    }
}
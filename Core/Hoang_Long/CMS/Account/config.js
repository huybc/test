﻿
define("Modules/Account/Config", [], function () {
    var config = {
        "css": [
            "/Modules/Account/Templates/css/styles.css"
        ],
        "js": [
        
        ],
        "templates": [
        ],
        templateMode: R.ModuleSetting.TemplateMode.ThreeColumns
    };
    return config;
});
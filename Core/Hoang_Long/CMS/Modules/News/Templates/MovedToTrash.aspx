﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MovedToTrash.aspx.cs" Inherits="Hoang_Long.CMS.Modules.News.Templates.MovedToTrash" %>

<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows %>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
            <td class="w100">
                <div>
                    <label>
                        <input type="checkbox" id="<%# Eval("Id") %>" />
                    </label>
                    <label class="_hot">
                       <i class="icon-bin position-static"></i>
                    </label>
                </div>
            </td>
            <td class="w80 ">
                <a class="avatar">
                    <img class="thumb" src="<%#  UIHelper.Thumb_W(60,Eval("Avatar").ToString()) %>" width="60" />
                    <%# (bool)Eval("IsVideo")?"<i class=\"icon-video\"></i>":"" %>
                </a>
            </td>
            <td>
                <p>
                <%# Eval("Title") %> - <b class="wysiwyg-color-green _zone" data-id="<%# Eval("ZoneId") %>"></b>
                </p>
                <p>
                    <label>Tạo bởi:</label><span class="bold"><%# Eval("CreatedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>,
                                                        <label> Xuất bản bởi:</label><span class="bold"><%# Eval("PublishedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("DistributionDate")) %></time>
                </p>
            </td>
            <td>
                <p>
                    <label>xem: <%# Eval("ViewCount") %></label>
                </p>
            </td>
            <%--<td class="text-center">
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-file-pdf"></i>Export to .pdf</a></li>
                            <li><a href="#"><i class="icon-file-excel"></i>Export to .csv</a></li>
                            <li><a href="#"><i class="icon-file-word"></i>Export to .doc</a></li>
                        </ul>
                    </li>
                </ul>
            </td>--%>
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

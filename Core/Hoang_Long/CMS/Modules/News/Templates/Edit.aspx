﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Hoang_Long.CMS.Modules.News.Templates.Edit" %>

<%@ Import Namespace="Mi.Entity.Base.Tag" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<div class="tabbable" id="news-id">

    <div id="nwrapper">
        <div class=" _body" id="_body">
            <div class="col-lg-9">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div id="icroll">
                            <fieldset class="content-group">
                                <legend class="text-semibold">
                                    <i class="icon-file-text2 position-left"></i>
                                    Đăng bài viết
                                </legend>
                            </fieldset>
                            <fieldset class="content-group">
                                <div class="form-group">
                                    <label class="col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="basic" id="titletxt" class="form-control" value="<%=_obj.NewsInfo.Title %>" tabindex="1" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2">Url<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="basic" id="Urltxt" class="form-control" tabindex="1" required="required" placeholder="link bài viết.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Mô tả<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <textarea id="Sapo" class="form-control" rows="3" placeholder="Nội dung ngắn gọn" tabindex="2"><%=_obj.NewsInfo.Sapo %></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Chi tiết<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <%--<button type="button" id="add-media"><i class="fa fa-camera"></i>Thêm media</button>--%>
                                        <textarea id="detailCkeditor" class="editor" rows="8" style="height: 500px" tabindex="3"><%=_obj.NewsInfo.Body %></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Tags<span class="text-danger"></span></label>
                                    <div class="col-lg-10 tags">
                                        <textarea id="Tagstext" data-type="<%=(int) EnumTagType.ForNews %>" preset="<%=GetTags((int)EnumTagType.ForNews) %>" placeholder="Tag: sửa chữa, thay thế, phụ tùng..." class="tag-editor-hidden-src"></textarea>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="content-group">
                                <legend class="text-bold">SEO <i class="icon-stats-growth"></i></legend>
                                <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Url<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" name="basic" id="url" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Đường dẫn tối ưu cho bài viết">
                            </div>
                        </div>--%>
                                <div class="form-group">
                                    <label class="col-lg-2">Tiêu đề SEO<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="basic" id="titleSeotxt" class="form-control" required="required" value="<%=_obj.NewsInfo.TitleSeo %>" placeholder="Tối ưu ở [60-100] ký tự.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2">Meta Keyword<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="maximum_characters" id="metakeywordtxt" class="form-control" value="<%=_obj.NewsInfo.MetaKeyword %>" placeholder="Khai báo các từ khóa với bộ máy tìm kiếm.">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2">Meta Description<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <textarea name="maximum_characters" class="form-control" id="metadescriptiontxt" placeholder="Mô tả khi tìm kiếm google tối ưu độ dài khoảng [140-160] ký tự"><%=_obj.NewsInfo.MetaDescription %></textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3">
                <div id="sidebar">
                    <div id="sidebar-content">
                        <div class="panel panel-flat">
                            <div class="panel-body">

                                <%-- <div class="col-lg-12 col-md-12 col-sm-12">--%>
                                <div class="form-group">
                                    <label>Chuyên mục</label>
                                    <select data-placeholder="Select your state" class="select" id="zoneddl" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.ZoneId %>">
                                        <asp:repeater runat="server" id="ZoneRpt">
                                            <ItemTemplate>
                                                <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                            </ItemTemplate>
                                        </asp:repeater>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Bút danh</label>
                                    <input type="text" class="form-control" placeholder="Ex:Author" value="<%=_obj.NewsInfo.Author %>" tabindex="6" id="authortxt">
                                </div>
                                <div class="form-group">
                                    <label>Nguồn</label>
                                    <input type="text" class="form-control" placeholder="Ex:Example.com" tabindex="7" value="<%=_obj.NewsInfo.Source %>" id="sourcetxt">
                                </div>
                                <%--<div class="form-group">
                                    <label>Ngày viết</label>
                                    <input type="text" class="form-control" id="datetxt" placeholder="" tabindex="10" value="<%=_obj.NewsInfo.CreatedDate %>">
                                </div>--%>
                                <div class="form-group text-left">
                                    <%--<label class="checkbox-inline checkbox-left">
                                        <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                        Nổi bật
								
                                    </label>--%>
                                    <label class="checkbox-inline checkbox-left">
                                        <input <%=_obj.NewsInfo.IsVideo?"checked":"" %> type="checkbox" tabindex="8" id="videocb">
                                        Tin video
								
                                    </label>
                                    <label class="checkbox-inline checkbox-left">
                                        <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                        Tin hot
                                    </label>
                                  
                                </div>
                                <div class="form-group text-left">
                                    <label class="checkbox-inline checkbox-left">
                                        <input type="checkbox" tabindex="9" id="commentcb" <%=_obj.NewsInfo.IsAllowComment?"checked":"" %>>Đóng bình luận
								
                                    </label>
                                </div>
                                 <div class="form-group" >
                                            <label>Tài liệu đính kèm:</label>
                                            <a href="javascript:void(0)" id="document-attack">[Đính kèm]</a>
                                        </div>
                                <div class="form-group" id="attack-news-thumb">

                                    <div class="avatar2">
                                        <input accept="image/*" id="fileSingleupload" type="file" name="files[]" style="display: none" />
                                        <label for="fileSingleupload">
                                            <%if (_obj.NewsInfo.Id > 0)
                                                {%>
                                            <img width="250" data-img="<%=_obj.NewsInfo.Avatar %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                            <%}
                                                else
                                                {%>
                                            <img src="/CMS/Themes/Images/no-thumbnai.png" width="250" />
                                            <% } %>
                                        </label>
                                    </div>

                                </div>

                                <div class="text-right">

                                    <button type="button" status="<% =_obj.NewsInfo.Status%>" class="btn btn-default ibtn-xs btn-primary" id="news-save-temp"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;
                                             <button type="button" class="btn btn-default ibtn-xs btn-primary" id="news-save-request"><i class="icon-paperplane position-left"></i>Xuất bản</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>

                                </div>
                                <%--</div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="references" style="display: none"></div>
    </div>
</div>


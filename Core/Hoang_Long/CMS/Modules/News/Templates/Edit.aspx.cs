﻿using System;
using System.Collections.Generic;
using System.Linq;

using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;

namespace Hoang_Long.CMS.Modules.News.Templates
{
    public partial class Edit : PageBase
    {
        public NewsDetailForEditEntity _obj = new NewsDetailForEditEntity();
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0L);
            _obj = NewsBo.GetDetail(id, PolicyProviderManager.Provider.GetAccountName());
            if (id <= 0)
            {
                _obj = new NewsDetailForEditEntity
                {
                    NewsInfo = new NewsEntity()
                };
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.All).ToList().Where(it => it.Status == 1);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
            //ZoneRpt2.DataSource = zones;
            //ZoneRpt2.DataBind();


        }

        protected string GetTags(int type)
        {
            List<string> tags = new List<string>();
            if (_obj.TagInNews != null)
            {
                foreach (var tag in _obj.TagInNews)
                {
                    //TagNameA|1|TagUrl_A
                    if (tag.TagMode == type)
                    {
                        tags.Add(string.Format("{0}|{1}|{2}", tag.Name, tag.TagId, tag.Url));
                    }
                }
            }
            return String.Join(",", tags);
        }
    }
}
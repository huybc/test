﻿using Hoang_Long.Core.Helper;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.BO.Base.ProjectDetail;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.ProjectDetail;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hoang_Long.CMS.Modules.GioiThieu.Action
{
    public class GioiThieuActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                    case "search":
                        responseData = GetListZone();
                        break;
                    case "xsearch":
                        responseData = XGetListZone();
                        break;
                    case "xsave":
                        responseData = ProjectSave();
                        break;
                    case "news_init_template":
                        responseData = NewsInitTemplate();
                        break;

                    case "update_status":
                        var status = GetQueryString.GetPost("status", 0);
                        responseData = ChangeStatus(status);
                        break;

                    case "xedit":
                        responseData = XEdit();
                        break;
                    case "move":
                        responseData = ParentUpdate();
                        break;
                }
            }

            return responseData;
        }

        private ResponseData NewsInitTemplate()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }


        private ResponseData XEdit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\GioiThieu\\Template\\XData\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListZone()
        {
            ResponseData responseData;
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name.Replace("+ ", "")
                });
            }
            responseData = ConvertResponseData.CreateResponseData(rs, rs.Count(), "");




            return responseData;
        }
        private ResponseData XGetListZone()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\GioiThieu\\Template\\XData\\List.aspx");
            responseData.Success = true;
            return responseData;
        }


        public ResponseData ProjectSave()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                var id = GetQueryString.GetPost("Id", 0);
                var title = GetQueryString.GetPost("title", string.Empty);
                var sapo = GetQueryString.GetPost("sapo", string.Empty);
                var avatar = GetQueryString.GetPost("avatar", "");
                var zone = GetQueryString.GetPost("zone", 0);
                var source = GetQueryString.GetPost("source", "");
                var hot = GetQueryString.GetPost("hot", false);
                var comment = GetQueryString.GetPost("comment", false);
                var video = GetQueryString.GetPost("video", false);
                var metakeyword = GetQueryString.GetPost("metakeyword", "");
                var titleseo = GetQueryString.GetPost("titleseo", "");
                var metadescription = GetQueryString.GetPost("metadescription", "");
                var date = GetQueryString.GetPost("date", DateTime.MinValue);
                var status = GetQueryString.GetPost("status", 0);
                var url = GetQueryString.GetPost("url", string.Empty);
                var body = GetQueryString.GetPost("content", string.Empty);
                var tagIdList = GetQueryString.GetPost("tagIdList", string.Empty);
                var size = GetQueryString.GetPost("size", string.Empty);

                //var budget = GetQueryString.GetPost("budge", "");
                //var invest = GetQueryString.GetPost("invest", "");
                //var hoursOfWork = GetQueryString.GetPost("hours_of_work", 0);
                //var surfaceArea = GetQueryString.GetPost("surface_area", 0);
                ////var groupId = GetQueryString.GetPost("groupId", 0);

                body = HttpContext.Current.Server.UrlDecode(body);
                var contentsJson = GetQueryString.GetPost("contents", string.Empty);
                var listContent = JsonConvert.DeserializeObject<List<Content>>(contentsJson);
                var userName = Policy.GetAccountName();
                if (!string.IsNullOrEmpty(title))
                {
                    var shortUrl = Utility.UnicodeToKoDauAndGach(title);
                    var news = new NewsEntity
                    {
                        Title = title,
                        Sapo = sapo,
                        Body = body,
                        Avatar = avatar,
                        Author = userName,
                        Status = status,
                        Source = source,
                        Type = (int)ZoneType.Project,
                        CreatedBy = userName,
                        EditedBy = userName,
                        LastModifiedBy = userName,
                        ZoneId = zone,
                        Url = url,
                        WordCount = Utility.CountWords(body),
                        CreatedDate = DateTime.Now,
                        //  DistributionDate = date,
                        Id = id,
                        IsOnMobile = false,
                        SourceURL = "",
                        MetaKeyword = metakeyword,
                        MetaDescription = metadescription,
                        TitleSeo = titleseo,
                        IsAllowComment = comment,
                        IsVideo = video,
                        IsHot = hot,
                        //Budget = budget,
                        //Invest = invest,
                        //HoursOfWork = hoursOfWork,
                        //SurfaceArea = surfaceArea,
                        GroupId = (int)ZoneType.Service,
                        Size = size
                    };
                    if (status == (int)NewsStatus.Published)
                        news.PublishedBy = userName;

                    if (id > 0)
                    {
                        news.Id = id;

                        responseData = ConvertResponseData.CreateResponseData(NewsBo.UpdateNews(news, zone, tagIdList, userName, true, ref shortUrl));
                        responseData.Data = id;
                        ProjectDetailBo.Delete(id);
                        foreach (var ct in listContent)
                        {
                            int idProject = 0;
                            ProjectDetailBo.Create(new ProjectDetailEntity
                            {
                                ArticleId = id,
                                Title = ct.title,
                                Content = ct.content,
                                Image = ct.image

                            }, ref idProject);
                        }

                    }
                    else
                    {
                        long retunrId = 0;
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.InsertNews(news, zone, tagIdList, userName, ref retunrId, ref shortUrl));
                        responseData.Data = retunrId;

                        if (retunrId > 0)
                        {

                            foreach (var ct in listContent)
                            {
                                int idProject = 0;
                                ProjectDetailBo.Create(new ProjectDetailEntity
                                {
                                    ArticleId = retunrId,
                                    Title = ct.title,
                                    Content = ct.content,
                                    Image = ct.image

                                }, ref idProject);
                            }

                            //if ((int)ZoneStatus.Publish == status)
                            //{
                            //    XMLDAL.AddNode(url, id);
                            //}
                            responseData.Success = true;
                            responseData.Message = "Thêm mới thành công !";
                        }
                        else
                        {
                            responseData.Success = false;

                        }
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid news's name";
                }
            }
            return responseData;
        }

        public ResponseData ParentUpdate()
        {
            var id = GetQueryString.GetPost("id", 0);
            var parent_id = GetQueryString.GetPost("parent_id", 0);

            var data = ZoneBo.ParentUpdate(id, parent_id);
            var responseData = ConvertResponseData.CreateResponseData(data);
            responseData.Success = true;
            return responseData;
        }
        public ResponseData Reject()
        {
            int id = 0;
            var listNewsId = GetQueryString.GetPost("id", 0L);
            var content = GetQueryString.GetPost("content", string.Empty);
            var username = Policy.GetAccountName();

            var data = NewsBo.ChangeStatusToReject(listNewsId, username);
            var responseData = ConvertResponseData.CreateResponseData(data);

            if (responseData.Success)
            {
                //responseData.Data = CryptonForId.EncryptId(listNewsId);
                //ContentLogMapping.Insert(new ContentLogEntity
                //{
                //    ObjbectId = listNewsId,
                //    Content = content,
                //    CreatedBy = Policy.GetAccountName(),
                //    CreatedDate = DateTime.Now,
                //    ObjectType = (byte)EnumContentLogType.News

                //}, ref id);
            }
            return responseData;
        }
        //public ResponseData Reject_Log()
        //{
        //    var listNewsId = GetQueryString.GetPost("id", 0L);
        //    var data = ContentLogMapping.Search(listNewsId, (byte)EnumContentLogType.News);
        //    var list = new List<ContentLogEntity>();
        //    foreach (var i in data)
        //    {
        //        list.Add(new ContentLogEntity
        //        {
        //            ObjbectId = i.ObjbectId,
        //            Content = i.Content,
        //            CreatedBy = i.CreatedBy,
        //            Date = UIHelper.GetLongDate(i.CreatedDate),
        //            ObjectType = i.ObjectType
        //        });
        //    }


        //    var responseData = ConvertResponseData.CreateResponseData(list, list.Count(), "");
        //    return responseData;
        //}


        public ResponseData ChangeStatus(int status)
        {
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                switch (status)
                {
                    case 3:
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.ChangeStatusToPublished(id, username, DateTime.Now));
                        break;
                    case 5:
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.ChangeStatusToUnpublished(id, username));
                        break;
                    case 4:
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.MoveToTrash(id, username));
                        break;
                }
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
                var objsNews = ZoneBo.GetZoneById(id.ToInt32Return0());
                if (objsNews != null)
                {
                    //if ((int)ZoneStatus.Publish == status)
                    //{
                    //    XMLDAL.UpdateNode(objsNews.ShortUrl, id.ToInt32Return0());

                    //}

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
        public class Content
        {

            public string title { get; set; }
            public string image { get; set; }
            public string content { get; set; }
        }
    }
}
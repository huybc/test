﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="hoptac.aspx.cs" Inherits="Hoang_Long.CMS.Modules.Customer.Template.hoptac" %>

<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<table class="table-hoptac datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>" data-table="tblHoptac" style="width: 100%">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
                    <td class="w50">
                        <div>
                           <label class="_hot">
                                <i class="icon-pencil7 edit position-static" data="<%# Eval("Id") %>"></i> #<%# Eval("Id") %>
                            </label>
                        </div>
                    </td>
            
                <td>
                <p>
                   <b> <%# Eval("FullName") %></b>
                </p>
                <p>
                    <label>Điện thoại:</label><%# Eval("Mobile") %>
                  
                </p> 
              <%--  <p>
                    <label>Mail:</label><%# Eval("Email") %>
                  
                </p>--%>
                <%--<p>
                    <label>Ngày tạo:</label><time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>
                </p>--%>
            </td>
<%--             <td>
                <p>
                    <label>Cty:</label><%# Eval("Firm") %>
                 
                </p>
                 <p>
                    <label>Dịch vụ:</label><%# Eval("Service") %>
                 
                </p>
                <p>
                    <%# Eval("Address") %>
                </p>
            </td> --%>
                    <td>
                <p>
                    <label>Chức vụ:</label><%# Eval("Contactperson") %>
                 
                </p> <p>
                    <label>Nội dung tin nhắn:</label><%# Eval("Note") %>
                 
                </p>
               
            </td>
            
        
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>
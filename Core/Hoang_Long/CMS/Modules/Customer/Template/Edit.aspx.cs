﻿using System;
using Mi.BO.Base.Customer;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Action.Core;

namespace Hoang_Long.CMS.Modules.Customer.Template
{
    public partial class Edit : PageBase
    {
        public CustomerEntity _obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0);
            if (id > 0)
            {
                _obj = CustomerBo.GetById(id);

            }
            else
            {
                _obj = new CustomerEntity();
            }

        }
    }
}
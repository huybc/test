﻿
define("Modules/FileManager/Config", [], function () {
    var config = {
        "css": [
            "/Modules/FileManager/Templates/css/styles.css"
        ],
        "js": [
        ],
        "templates": [
            'Main.aspx'
        ],
        templateMode: R.ModuleSetting.TemplateMode.ThreeColumns
    };
    return config;
});

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.V1.aspx.cs" Inherits="Hoang_Long.CMS.Modules.FileManager.Main_V1" %>

<%@ Import Namespace="Mi.Common" %>
<link href="/CMS/Modules/FileManager/Styles/filemanager.v1.css" rel="stylesheet" />

<div id="fm-container">
    <div id="fm-toolbar">
        <label class="fl-left" id="progressall">Đang tải lên <b></b> %</label>
        <div class="tool-items">
            <div class="" style=" float: left;">
                <span id="frowInTotals">23/23/2</span>
                <div class="ipagination iweb">
                    <a href="#" class="first" data-action="first">&laquo;</a>
                    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                    <input type="text" readonly="readonly" />
                    <a href="#" class="next" data-action="next">&rsaquo;</a>
                    <a href="#" class="last" data-action="last">&raquo;</a>
                </div>
            </div>
            <ul class="tools">
                <li title="Crop"><i class="crop"></i></li>
            </ul>
            <ul class="tools">
                <li title="Create folder"><i class="create-folder"></i></li>
                <li title="Upload" id="btn-fm-upload"><label for="fileSingleupload"><i class="upload"></i></label><input accept="image/*" id="fileSingleupload" multiple type="file" name="files[]" style="display: none" /></li>
            </ul>
            <ul class="tools">
                <li title="List view"><i class="list"></i></li>
                <li title="Grid View"><i class="grid"></i></li>
            </ul>

            <%--<ul class="tools">
                <li class="btn-close"><i class="iclose"></i></li>
            </ul>--%>
        </div>
    </div>
    <div id="fm-main">
        <div id="fm-sidebar">
            <%--  <div class="fm-header">
                Folder
            </div>--%>
            <div id="fm-folder">
            </div>
        </div>
        <div id="fm-content">
            <div id="fm-data-wrapper">
                <div class="fm-list-wrapper">
                    <div class="fm-list">
                    </div>
                </div>

                <%-- <div id="fm-upload-content">
                    <div id="plupload" style="height: 300px"></div>
                </div>--%>
            </div>

            <div id="fm-file-view">
                <div class="header">Chi tiết tệp</div>
                <div class="attachment-info">
                </div>

            </div>
            <div class="clear"></div>
        </div>

    </div>
    <div id="fm-footer">

        <div class="btn-attack">
            <button type="button" id="btn-attack">Đính kèm</button>
            <button type="button" id="dl-close" class="iclose">Đóng</button>
        </div>
    </div>
</div>
<script src="/CMS/Modules/FileManager/Libs/plupload/plupload.full.js"></script>
<script>
    var folders = <%= NewtonJson.Serialize(FolderResult())%>;
</script>

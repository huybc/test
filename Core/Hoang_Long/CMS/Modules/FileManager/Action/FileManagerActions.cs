﻿using System;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.FileUpload;
using Mi.Common;
using Mi.Entity.Base.FileUpload;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Hoang_Long.Core.Helper;

namespace Hoang_Long.CMS.Modules.FileManager.Action
{
    public class FileManagerActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }

        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "folder_save":
                        responseData = FolderSave();

                        break;
                    case "rename":
                        //  responseData = ApprovalRedeem();
                        break;
                    case "folder_remove":
                        responseData = FolderRemove();
                        break;
                    case "file_remove":
                        responseData = FolderRemove();
                        break;

                    case "file_manager_init":
                        responseData = Init_FileManager();
                        break;
                    case "search":
                        responseData = Search();
                        break;
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                }
            }
            return responseData;
        }



        private static ResponseData Init_FileManager()
        {
            return ConvertResponseData.CreateResponseData("[]", 1, "\\CMS\\Modules\\FileManager\\Main.V1.aspx");
        }
        private static ResponseData FolderSave()
        {
            var responseData = new ResponseData();
            var name = GetQueryString.GetPost("name", string.Empty);
            var action = GetQueryString.GetPost("action", string.Empty);
            var nodeId = GetQueryString.GetPost("node_id", 0);
            var parentId = GetQueryString.GetPost("parent_id", 0);


            if (!UIHelper.IsValiFolder(name))
            {
                responseData.Success = false;
                responseData.Message = "Tên không hợp lệ !";
                return responseData;
            }

            int id = 0;



            switch (action.ToLower())
            {
                case "create":
                    if (FileUploadBo.FolderCheckIsExist(0, name.ToLower(), nodeId) > 0)
                    {
                        responseData.Success = false;
                        responseData.Message = "Đã tồn tại !";
                        return responseData;
                    }

                    FileUploadBo.InsertFileUpload(new FileUploadEntity
                    {
                        Name = name,
                        Description = "Folder",
                        ParentId = nodeId,
                        UploadedBy = "hiephv",
                        UploadedDate = DateTime.Now

                    }, ref id);
                    if (id > 0)
                    {
                        responseData.Success = true;
                        responseData.Data = id;
                        responseData.Message = "Thành công!";
                        return responseData;
                    }

                    else
                    {
                        responseData.Success = false;
                        responseData.Content = "db do not insert";
                        responseData.Message = "Đã có xự cố xảy ra !";
                        return responseData;
                    }
                    break;
                case "rename":

                    if (FileUploadBo.FolderCheckIsExist(nodeId, name.ToLower(), parentId) > 0)
                    {
                        responseData.Success = false;
                        responseData.Message = "Đã tồn tại !";
                        return responseData;
                    }
                    FileUploadBo.UpdateFileUpload(new FileUploadEntity
                    {
                        Id = nodeId,
                        Name = name,
                        Description = "Folder",
                        ParentId = parentId,
                        LastModifiedBy = "hiephv",
                        LastModifiedDate = DateTime.Now

                    });
                    responseData.Success = true;
                    responseData.Data = id;
                    responseData.Message = "Thành công!";
                    break;
                case "move":
                    var node_name = GetQueryString.GetPost("node_name", string.Empty);

                    if (FileUploadBo.FolderCheckIsExist(nodeId, node_name.ToLower(), parentId) > 0)
                    {
                        responseData.Success = false;
                        responseData.Message = "Đã tồn tại. Bạn cần đổi tên trước !";
                        return responseData;
                    }
                    FileUploadBo.UpdateFileUpload(new FileUploadEntity
                    {
                        Id = nodeId,
                        Name = node_name,
                        Description = "Folder",
                        ParentId = parentId,
                        LastModifiedBy = "hiephv",
                        LastModifiedDate = DateTime.Now

                    });
                    responseData.Success = true;
                    responseData.Data = id;
                    responseData.Message = "Thành công!";

                    break;

            }

            return responseData;
        }

        private static ResponseData FolderRemove()
        {
            var responseData = new ResponseData();
            var nodeId = GetQueryString.GetPost("node_id", 0);
            FileUploadBo.DeleteFileUpload(nodeId);
            responseData.Success = true;
            responseData.Message = "Xóa thành công!";
            return responseData;
        }
        private static ResponseData Search()
        {
            var pageindex = GetQueryString.GetPost("pageindex", 0);
            var pagesize = GetQueryString.GetPost("pagesize", 0);
            var parentid = GetQueryString.GetPost("parentid", 0);
            var keyword = GetQueryString.GetPost("keyword", "");
            int totalRows = 0;

            var username = PolicyProviderManager.Provider.GetAccountName();
            parentid = parentid == 0 ? -1 : parentid;

            if (PolicyProviderManager.Provider.GetCurrentRole() == EnumPermission.ArticleAdmin &&
                         Role.HasRoleOnPermission(EnumPermission.ArticleAdmin, username))
            {

                username = "";
            }



            pagesize = pagesize > 0 ? pagesize : 90;
            var objs = FileUploadBo.SearchFileUpload(keyword, parentid, username,
                EnumFileUploadType.File, EnumFileUploadStatus.AllStatus, string.Empty, EnumFileSortOrder.UploadedDateDesc,
                pageindex, pagesize, ref totalRows);
            var extant = totalRows - (pagesize * pageindex);
            var pageInfo = Math.Ceiling((decimal)((double)totalRows / pagesize)) + "#" + (extant < pagesize ? totalRows : (pagesize * pageindex)) + "#" + totalRows;

            var responseData = ConvertResponseData.CreateResponseData(objs, totalRows, "");
            responseData.Content = pageInfo;
            return responseData;
        }


    }
}
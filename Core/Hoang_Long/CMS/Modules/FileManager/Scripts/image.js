﻿R.Image = {
    Dialog: function (callback) {
        var dl = $.dialog({
            title: '',
            columnClass: 'upload-content',
            //bootstrapClasses: 'upload-wrapper',
            bootstrapClasses: {
                container: 'upload-wrapper',
                containerFluid: 'upload-wrapper',
                row: 'row'
            },
            content: '<div class="modal-main"><div class="modal-dropzone"><input accept="image/*" id="fileupload" type="file" name="files[]" ><div class="modal-dropzone-default" style="display: block;"><div class="modal-dropzone-img"></div><p><b>Kéo và thả hoặc click vào đây</b> để tải ảnh lên (max 2 MiB)</p></div></div><div class="files" id="files"></div><div class="modal-option"><div class="modal-options"><div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div class="modal-options-url tab-page" style="display: block;">hoặc link ảnh<input class="modal-input-url user-input" type="text" id="imageUrl" name="upload-url" placeholder="http://example.com/image.png" value=""></div></div></div><div class="modal-footer "><div class="modal-cta"><span></span><button type="button" class="re-btn re-btn-primary" id="attack-btn">Đính kèm</button></div></div></div>',
            onOpen: function () {
                R.Image.Upload(callback, function () {
                    dl.close();
                    $('.files').hide();
                });
                $('#imageUrl').change(function () {
                    $('.modal-cta span').text('');
                });
                $('#attack-btn').off('click').on('click', function () {
                    var $this = $(this);
                    if ($this.hasClass('re-btn-default')) {
                        return;
                    }
                    if ($('#imageUrl').val().length > 0 && $('#files .temp-upload').length == 0) {
                        $('#progress .progress-bar').css({
                            'width': 95 + '%',
                        });
                        $this.removeClass('re-btn-primary').addClass('re-btn-default').html('đang xử lý...');
                        $.ajax({
                            method: "POST",
                            data: {
                                url: $('#imageUrl').val()
                            },
                            url: StorageUploadUrl + '?fn=link',
                            success: function (res) {
                                var jsonResult = $.parseJSON(res);
                                if (jsonResult.success) {
                                    dl.close();
                                    if (callback) {
                                        callback({
                                            multi: false,
                                            upload: true,
                                            data: jsonResult.path
                                        });
                                    }
                                    return;
                                } else {
                                    $this.removeClass('re-btn-default').addClass('re-btn-primary').html('Đính kèm');
                                    $('.modal-cta span').text(jsonResult.messages);
                                    $('#imageUrl').val('');
                                    $('#progress .progress-bar').css({
                                        'width': 0 + '%',
                                    });

                                    return;
                                }
                            },
                            error: function (a) {
                                $this.removeClass('re-btn-default').addClass('re-btn-primary').html('Đính kèm');
                                $.confirm({
                                    title: 'Thông báo !',
                                    type: 'red',
                                    content: 'Có lỗi xảy ra.',
                                    animation: 'scaleY',
                                    closeAnimation: 'scaleY',
                                    buttons: {
                                        cancel: {
                                            text: 'Đóng',
                                            btnClass: 're-btn re-btn-default'
                                        },
                                        yes: {
                                            isHidden: true, // hide the button
                                            keys: ['ESC'],
                                            action: function () {

                                            }
                                        }

                                    }

                                });
                            }
                        });
                    }


                });
            },
            onOpenBefore: function () {
                $('.modal-cta span').text('');
            }
        });
    },
    Upload: function (callback, callbackSuccess) {
        $('.modal-dropzone').fileupload({
            url: StorageUploadUrl + '?fn=upload',
            dataType: 'json',
            limitMultiFileUploads: 1,
            limitMultiFileUploadSize: 2048000, //2Mb
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            autoUpload: false,
            disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewCrop: true,
            done: function (e, data) {
            },
            add: function (e, data) {
                $('.files').show();
                $('.modal-cta span').text('');
                if (data.files && data.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#files').append('<div class="temp-upload"><i class="fa fa-trash-o"></i><img src="' + e.target.result + '"/></div>');
                    }
                    reader.readAsDataURL(data.files[0]);
                    setTimeout(function () {
                        $('#files .temp-upload i').off('click').on('click', function () {
                            $(this).closest('.temp-upload').remove();
                            $('.files').hide();
                        });
                    }, 200);
                }

                $('#attack-btn').off('click').on('click', function () {
                    if ($('#files .temp-upload').length == 0) {
                        return;
                    }
                    data.submit();
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css({
                    'width': progress + '%',
                });
                $('#progress').css({
                    'box-shadow': '0 -1px 0 rgba(0, 0, 0, 0.15) inset'
                });
            },
            success: function (response, status) {

                if (!response.success) {
                    $('.modal-cta span').text(response.messages);
                    $('#progress .progress-bar').css({
                        'width': 0 + '%',
                    });
                    return;
                }

                if (response.success && response.error == 200) {
                    $('#files').html('');
                    if (callbackSuccess) {
                        callbackSuccess();
                    }
                    var img = response.path;
                    if (callback) {
                        callback({
                            multi: false,
                            upload: true, //upload:true, link:false
                            data: img // string, array string
                        });
                    }
                }
            },
            error: function (error) {
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Tải ảnh lên không thành công.',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        },
                        yes: {
                            isHidden: true, // hide the button
                            keys: ['ESC'],
                            action: function () {

                            }
                        }

                    }

                });
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        $('.modal-dropzone-default').off('click').on('click', function () {
            $('#fileupload').trigger('click');
        });
    },
    SingleUpload: function (el, progressall, callbackSuccess) {
        el.fileupload({
            url: StorageUploadUrl + '?fn=upload',
            dataType: 'json',
            limitMultiFileUploads: 1,
            limitMultiFileUploadSize: 2048000, //2Mb
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            autoUpload: true,
            disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewCrop: true,
            done: function (e, data) {
            },
            send: function (e, data) {
              //  $(el).RModuleBlock();
            },
            progressall: function (e, data) {
                if (progressall) {
                    progressall(e, data);
                }
            },
            success: function (response, status) {
                
              //  $(el).RModuleUnBlock();
                if (callbackSuccess) {
                    callbackSuccess(response, status);
                }
            },
            error: function (error) {
                $(el).RModuleUnBlock();
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Tải ảnh lên không thành công.',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        },
                        yes: {
                            isHidden: true, // hide the button
                            keys: ['ESC'],
                            action: function () {

                            }
                        }

                    }

                });
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    }
}

function run() {
    tim = setInterval(function () {
        if (i >= 100) { clearInterval(tim); return; }
        $('#count').val(++i);
    }, 100);
}
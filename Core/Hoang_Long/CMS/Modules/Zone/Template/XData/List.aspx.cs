﻿using System;
using System.Linq;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Action.Core;

namespace Hoang_Long.CMS.Modules.Zone.Template.XData
{
    public partial class List : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            int status = GetQueryString.GetPost("status", 0);
            string title = GetQueryString.GetPost("title", string.Empty);
            var fromDate = GetQueryString.GetPost("from", DateTime.MinValue);
            var todate = GetQueryString.GetPost("to", DateTime.MaxValue);
            var pageIndex = GetQueryString.GetPost("pageindex", 1);
            var pagesize = GetQueryString.GetPost("pagesize", 10);
            var keyWord = GetQueryString.GetPost("keyword", String.Empty);
            var zoneId = GetQueryString.GetPost("zone", 0);
            var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
            var username = GetQueryString.GetPost("account", PolicyProviderManager.Provider.GetAccountName());
            var order = GetQueryString.GetPost("order", (int)ZoneSortExpression.CreatedDateDesc);
            var type = GetQueryString.GetPost("type", (int)ZoneType.Project);



            var sortBy = ZoneSortExpression.CreatedDateDesc;
            //   var status = ZoneStatus.Publish;
            DataRpt.DataSource = ZoneBo.ZoneSearch(keyWord, username, zoneIds, fromDate, todate, ZoneFilterFieldForUsername.All, sortBy, status, type, pageIndex, pagesize, ref TotalRows).OrderBy(it => it.SortOrder);
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }

        public string ZoneStatus(int status)
        {
            string htm = string.Empty;
            switch (status)
            {
                case 1:
                    htm = "Hiển thị";
                    break;
                case 2:
                    htm = "Ẩn";
                    break;
                case 3:
                    htm = "Lưu tạm";
                    break;
                case 4:
                    htm = "Xóa";
                    break;
                default:
                    htm = "n/a"; break;
            }

            return htm;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.BO.Base.ProjectDetail;
using Mi.Entity.Base.ProjectDetail;
using Mi.Action;
using Mi.Action.Core;
using Newtonsoft.Json;
using Hoang_Long.Core.Helper;

namespace Hoang_Long.CMS.Modules.Zone.Action
{
    public class ZoneActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                    case "search":
                        responseData = GetListZone();
                        break;
                    case "xsearch":
                        responseData = XGetListZone();
                        break;
                    case "main":
                        responseData = InitMain();
                        break;
                    case "xmain":
                        responseData = XInitMain();
                        break;
                    case "xsave":
                        responseData = SaveZoneNews();
                        break;
                    case "save_project":
                        responseData = SaveZoneNews();
                        break;
                    case "news_init_template":
                        responseData = NewsInitTemplate();
                        break;

                    case "update_status":

                        responseData = ChangeStatus();
                        break;

                    case "xedit":
                        responseData = XEdit();
                        break;
                    case "edit":
                        responseData = Edit();
                        break;
                    case "save":
                        responseData = Save();
                        break;
                    case "move":
                        responseData = ParentUpdate();
                        break;
                }
            }

            return responseData;
        }

        private ResponseData NewsInitTemplate()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData InitMain()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Main.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData XInitMain()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\XMain.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Edit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData XEdit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\XData\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListZone()
        {
            ResponseData responseData;
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name.Replace("+ ", "")
                });
            }
            responseData = ConvertResponseData.CreateResponseData(rs, rs.Count(), "");




            return responseData;
        }
        private ResponseData XGetListZone()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Zone\\Template\\XData\\List.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData SaveZoneNews()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("title", string.Empty);
                var desc = GetQueryString.GetPost("sapo", string.Empty);
                var content = GetQueryString.GetPost("content", string.Empty);
                var zone = GetQueryString.GetPost("zone", 0);
                var status = GetQueryString.GetPost("status", 0);
                var type = GetQueryString.GetPost("type", 0);
                var date = GetQueryString.GetPost("date", DateTime.MinValue);
                var url = GetQueryString.GetPost("url", string.Empty);
                var avatar = GetQueryString.GetPost("avatar", string.Empty);
                var metakey = GetQueryString.GetPost("metakeyword", string.Empty);
                var metades = GetQueryString.GetPost("metadescription", string.Empty);
                var metatitle = GetQueryString.GetPost("metatitle", string.Empty);

                var hours_of_work = GetQueryString.GetPost("hours_of_work", 0);
                var construction_date = GetQueryString.GetPost("construction_date", DateTime.MinValue);
                var surface_area = GetQueryString.GetPost("surface_area", 0);
                var budge = GetQueryString.GetPost("budge", 0L);
                var contentsJson = GetQueryString.GetPost("contents", string.Empty);

                var listContent = JsonConvert.DeserializeObject<List<Content>>(contentsJson);
                var userName = Policy.GetAccountName();
                if (zone == 0)
                {
                    responseData.Success = false;
                    responseData.Message = "Chọn chuyên mục !";
                    return responseData;
                }

                var currentZone = ZoneBo.GetZoneById(zone);

                if (!string.IsNullOrEmpty(name))
                {
                    var shortUrl = Utility.UnicodeToKoDauAndGach(name);
                    var obj = new ZoneEntity
                    {
                        Name = name,
                        ParentId = zone,
                        Description = desc,
                        Content = content,
                        CreatedDate = date,
                        ShortUrl = string.IsNullOrEmpty(url) ? shortUrl : url,
                        Invisibled = false,
                        Status = status,
                        CreatedBy = userName,
                        // SortOrder = sortOrder,
                        // ModifiedDate = DateTime.Now,
                        //  AllowComment = allowComment,
                        //Domain = domain,
                        Avatar = avatar,
                        MetaDescription = metades,
                        MetaKeyword = metakey,
                        MetaTitle = metatitle,
                        DistributionDate = date,
                        Type = (byte)type,
                        HoursOfWork = hours_of_work,
                        ConstructionDate = construction_date,
                        Budget = budge,
                        SurfaceArea = surface_area
                    };
                    if (id > 0)
                    {
                        obj.Id = id;
                        obj.ModifiedDate = DateTime.Now;
                        obj.ModifiedBy = userName;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.UpdateV3(obj, userName));

                        ProjectDetailBo.Delete(id);
                        foreach (var ct in listContent)
                        {
                            int idProject = 0;
                            ProjectDetailBo.Create(new ProjectDetailEntity
                            {
                                ArticleId = id,
                                Title = ct.title,
                                Content = ct.content,
                                Image = ct.image

                            }, ref idProject);
                        }
                        if ((int)ZoneStatus.Publish == status)
                        {
                            XMLDAL.UpdateNode(url, id);

                        }


                        responseData.Message = "Cập nhật thành công !";
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.InsertV3(obj, userName, ref id));

                        if (id > 0)
                        {

                            foreach (var ct in listContent)
                            {
                                int idProject = 0;
                                ProjectDetailBo.Create(new ProjectDetailEntity
                                {
                                    ArticleId = id,
                                    Title = ct.title,
                                    Content = ct.content,
                                    Image = ct.image

                                }, ref idProject);
                            }

                            if ((int)ZoneStatus.Publish == status)
                            {
                                XMLDAL.AddNode(url, id);
                            }
                            responseData.Success = true;
                            responseData.Message = "Thêm mới thành công !";
                        }
                        else
                        {
                            responseData.Success = false;

                        }


                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }
        private ResponseData Save()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty);
                var desc = GetQueryString.GetPost("sapo", string.Empty);
                var parentId = GetQueryString.GetPost("parent_id", 0);
                var status = GetQueryString.GetPost("status", 1);
                var isDisplayHome = GetQueryString.GetPost("isDisplayHome", false);
                var sortOrder = GetQueryString.GetPost("sort", 0);
                var type = GetQueryString.GetPost("type", 0);
                var domain = GetQueryString.GetPost("domain", string.Empty);
                var avatar = GetQueryString.GetPost("avatar", string.Empty);
                var icon = GetQueryString.GetPost("icon", string.Empty);
                var metakey = GetQueryString.GetPost("metakey", string.Empty);
                var metades = GetQueryString.GetPost("metades", string.Empty);
                var metaTitle = GetQueryString.GetPost("metatitle", string.Empty);
                // avatar = avatar.Replace(ConfigurationSettings.AppSettings["FILE_MANAGER_HTTPDOWNLOAD"], "");
                var userName = Policy.GetAccountName();
                if (id == parentId && parentId > 0)
                {
                    responseData.Success = false;
                    responseData.Message = "Chuyên mục cha không hợp lệ !";
                    return responseData;
                }
                if (!string.IsNullOrEmpty(name))
                {
                    var shortUrl = Utility.UnicodeToKoDauAndGach(name);
                    var zone = new ZoneEntity
                    {
                        Name = name,
                        ParentId = parentId,
                        Content = desc,
                        CreatedDate = DateTime.Now,
                        ShortUrl = shortUrl,
                        Invisibled = false,
                        Status = status,
                        SortOrder = sortOrder,
                        ModifiedDate = DateTime.Now,
                        Banner = icon,
                        //  AllowComment = allowComment,
                        Domain = domain,
                        Avatar = avatar,
                        MetaDescription = metades,
                        MetaKeyword = metakey,
                        Type = (byte)type,
                        MetaTitle = metaTitle,
                        ZoneIdList = "",
                        IsShowHomePage = isDisplayHome
                    };
                    if (id > 0)
                    {
                        zone.Id = id;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.Update(zone, userName));
                    }
                    else
                    {
                        int idoutput = 0;
                        responseData = ConvertResponseData.CreateResponseData(ZoneBo.InsertV2(zone, userName, ref idoutput));
                        if (idoutput > 0) { }
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }

        public ResponseData ParentUpdate()
        {
            var id = GetQueryString.GetPost("id", 0);
            var parent_id = GetQueryString.GetPost("parent_id", 0);

            var data = ZoneBo.ParentUpdate(id, parent_id);
            var responseData = ConvertResponseData.CreateResponseData(data);
            responseData.Success = true;
            return responseData;
        }
        public ResponseData Reject()
        {
            int id = 0;
            var listNewsId = GetQueryString.GetPost("id", 0L);
            var content = GetQueryString.GetPost("content", string.Empty);
            var username = Policy.GetAccountName();

            var data = NewsBo.ChangeStatusToReject(listNewsId, username);
            var responseData = ConvertResponseData.CreateResponseData(data);

            if (responseData.Success)
            {
                //responseData.Data = CryptonForId.EncryptId(listNewsId);
                //ContentLogMapping.Insert(new ContentLogEntity
                //{
                //    ObjbectId = listNewsId,
                //    Content = content,
                //    CreatedBy = Policy.GetAccountName(),
                //    CreatedDate = DateTime.Now,
                //    ObjectType = (byte)EnumContentLogType.News

                //}, ref id);
            }
            return responseData;
        }
        //public ResponseData Reject_Log()
        //{
        //    var listNewsId = GetQueryString.GetPost("id", 0L);
        //    var data = ContentLogMapping.Search(listNewsId, (byte)EnumContentLogType.News);
        //    var list = new List<ContentLogEntity>();
        //    foreach (var i in data)
        //    {
        //        list.Add(new ContentLogEntity
        //        {
        //            ObjbectId = i.ObjbectId,
        //            Content = i.Content,
        //            CreatedBy = i.CreatedBy,
        //            Date = UIHelper.GetLongDate(i.CreatedDate),
        //            ObjectType = i.ObjectType
        //        });
        //    }


        //    var responseData = ConvertResponseData.CreateResponseData(list, list.Count(), "");
        //    return responseData;
        //}


        public ResponseData ChangeStatus()
        {
            var status = GetQueryString.GetPost("status", 0);
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(ZoneBo.ChangeStatus((int)id, username, status));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
                var objsNews = ZoneBo.GetZoneById(id.ToInt32Return0());
                if (objsNews != null)
                {
                    //if ((int)ZoneStatus.Publish == status)
                    //{
                    //    XMLDAL.UpdateNode(objsNews.ShortUrl, id.ToInt32Return0());

                    //}

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
        public class Content
        {

            public string title { get; set; }
            public string image { get; set; }
            public string content { get; set; }
        }
    }
}
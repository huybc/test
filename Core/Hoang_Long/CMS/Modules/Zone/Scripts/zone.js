﻿R.Zone = {
    Init: function () {
        R.Zone.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 1;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');
        R.Zone.XNews();
    },
    RegisterEvents: function () {
        $('#zoneStatusdll,#zoneTypedll').fselect();
        $('#zoneStatusdll').off('change').on('change',
            function (e) {
                R.Zone.XNews();
            });
        $('.zone-action ._edit').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            R.Zone.Edit(id);
        });

        $('.zone-action .status').off('click').on('click', function (e) {
            var id = $(this).parent().attr('data-id');
            var status = $(this).attr('data');
            R.Zone.XUpdateStatus({
                status: status,
                id: id
            });
        });




        var delay = 0;
        $('#cms-sidebar li').off('click').on('click',
            function () {
                $('.navigation-main li').removeClass('active');
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                        R.Zone.XNews();
                },
                    200);

            });

        $('#zoneAddBtn').off('click').on('click',
            function () {
                R.Zone.Edit(0);

            });

        $('#zoneSaveBtn').off('click').on('click',
            function () {
                var zoneType = $('#cms-sidebar li.active').attr('data');

                var data = {
                    id: R.Zone.ZoneId,
                    name: $('#zoneNameTxt').val(),
                    parent_id: $('#_zoneddl').val(),
                    sort: $('#sortZoneTxt').val(),
                    sapo: CKEDITOR.instances['sapoZoneTxt'].getData(),
                    metakey: $('#metaKeywordZoneTxt').val(),
                    metades: $('#metaDescriptionZoneTxt').val(),
                    metatitle: $('#metaTitletxt').val(),
                    avatar: $('#attack-thumb img').attr('data-img'),
                    icon: zoneType == 13 ? $('#clasIconTxt').val(): $('#attack-icon img').attr('data-img'),
                    type: $('#cms-sidebar li.active').attr('data'),
                    isDisplayHome: $('#isShowHomaPage').is(":checked")

                }
                R.Zone.Save(data);
            });
        $('#items .item i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });


        $('#items .item .avatar').off('click').on('click',
            function () {
                var $this = $(this);

                R.Image.SingleUpload($this, function () {

                    //process
                }, function (response, status) {

                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.closest('.avatar').find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }


                });

            });

    },


   
    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Zone.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = $('#zone-wrapper');
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: 0,
            pageindex: R.Zone.PageIndex,
            pagesize: R.Zone.PageSize,
            status: $('#zoneStatusdll').val(),
            from: R.Zone.startDate,
            to: R.Zone.endDate,
            type: $('#cms-sidebar li.active').attr('data')
        }
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {

                    $(el).find('.datatable-scroll._list').html(res.Content);

                    R.CMSMapZone(el);
                    $('.btn-group.btn-action').hide();


                    //pager
                    if ($('#zone-wrapper .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#zone-wrapper .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Zone.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Zone.PageIndex) * parseInt(R.Zone.PageSize) - parseInt(R.Zone.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Zone.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Zone.PageIndex = page;
                                R.Zone.XNews();
                            }
                        });
                    }

                    setTimeout(function () {
                        R.Zone.RegisterEvents();
                    },
                        300);
                    R.ScrollAutoSize('#zone-wrapper .datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    Save: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {
                if (res.Success) {
                    R.Zone.XNews();
                    R.ScrollAutoSize('#data-zones', function () {
                        return $(window).height() - 10;
                    }, function () {
                        return 'auto';
                    }, {});
                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
            }
        });
    },
    Edit: function (id) {
        $('#zone-form-edit').RLoading();
        var zoneType= $('#cms-sidebar li.active').attr('data');
        R.Post({
            params: {
                id: id,
                zoneType: zoneType
            },
            module: "zone",
            ashx: 'modulerequest.ashx',
            action: "edit",
            success: function (res) {
                if (res.Success) {

                    R.ShowOverlay(res.Content, function () {

                        $("#ZoneSaveButton").off('mouseover').off('mouseout')
                            .mouseover(function () {
                                $(this).removeClass('SaveButtonNormalState');
                                $(this).addClass('SaveButtonOverState');
                            })
                            .mouseout(function () {
                                $(this).removeClass('SaveButtonOverState');
                                $(this).addClass('SaveButtonNormalState');
                            });

                        $('#ZoneSaveButton').off('click').on('click', function () {
                            return false;
                        });

                        if (zoneType == 13) {
                            $('#attack-icon').hide();
                            $('#class-icon').show();
                        }

                    }, function () {

                    });

                    if ($('#cms-sidebar li.active').attr('data') == 1) {
                        $('#isShowHomaPageEl').show();
                    }
                    R.Zone.ZoneId = id;
                    if (R.Zone.ZoneType === 3) {
                        $('#zone-panel').hide();
                    }
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');
                        }
                        if (CKEDITOR.instances['sapoZoneTxt']) {
                            CKEDITOR.instances['sapoZoneTxt'].destroy();
                        }
                        CKEDITOR.replace('sapoZoneTxt', { toolbar: 'iweb' });
                        R.ScrollAutoSize('#zone-edit .panel-body', function () {
                            return $(window).height() - 10;
                        }, function () {
                            return 'auto';
                        }, {});

                        $('#attack-files .avatar').off('click').on('click', function () {
                            var $this = $(this);
                            R.Image.SingleUpload($this,
                                function () {
                                    //process
                                }, function (response, status) {
                                    $($this).RModuleUnBlock();
                                    //success
                                    if (!response.success) {
                                        $.confirm({
                                            title: 'Thông báo !',
                                            type: 'red',
                                            content: response.messages,
                                            animation: 'scaleY',
                                            closeAnimation: 'scaleY',
                                            buttons: {
                                                cancel: {
                                                    text: 'Đóng',
                                                    btnClass: 're-btn re-btn-default'
                                                },
                                                yes: {
                                                    isHidden: true, // hide the button
                                                    keys: ['ESC'],
                                                    action: function () {

                                                    }
                                                }
                                            }

                                        });
                                        return;
                                    }

                                    if (response.success && response.error == 200) {
                                        var img = response.path;
                                        $this.find('img').attr({
                                            'src': img + '?w=150&h=100&mode=crop',
                                            'data-img': response.name
                                        });
                                    }

                                });

                        });

                    }, 400);

                    $('#zone-form-edit').RLoadingComplete();
                    R.Zone.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    }



}
$(function () {
    R.Zone.Init();
});
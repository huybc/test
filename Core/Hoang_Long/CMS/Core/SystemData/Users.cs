﻿using System;
using System.Collections.Generic;
using Mi.BO.Base.Account;
using Mi.Common;
using Mi.Entity.Base.Security;

namespace Hoang_Long.CMS.Core.SystemData
{
    public class Users
    {
        public enum OnlineStatus : int
        {
            Online = 0,
            Offline = 1,
            Idle = 2
        }
        public class UserInfo
        {
            //public string id { get; set; }
            public string username { get; set; }
            public string name { get; set; }
            public string avatar { get; set; }
            public string email { get; set; }
            public string type { get; set; }
            public int onlinestatus { get; set; }
            public string connectionid { get; set; }
        }

        private static List<UserInfo> _listAllUser;
        public static List<UserInfo> ListAllUser
        {
            get
            {
                if (_listAllUser == null || _listAllUser.Count <= 0)
                {
                    RefreshListAllUser();
                }
                return _listAllUser;
            }
        }

        public static void RefreshListAllUser()
        {
            try
            {
                var newListAllUser = new List<UserInfo>();
                var totalRow = 0;
                var users = UserBo.SearchUser("", UserStatus.Actived, UserSortExpression.UserNameAsc, 1,
                                                        10000, ref totalRow);
                foreach (var user in users)
                {
                    var userInfo = new UserInfo
                    {
                        //id = user.EncryptId,
                        name = user.FullName,
                        // avatar = UIHelper.GetAvatarThumb(user.UserName),
                        email = user.Email,
                        username = user.UserName.ToLower(),
                        type = "account",
                        onlinestatus = (int)OnlineStatus.Offline

                    };

                    newListAllUser.Add(userInfo);
                }

                _listAllUser = newListAllUser;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
        }

        public static bool SetOnlineStatus(string username, OnlineStatus onlineStatus, string connectionId)
        {
            if (_listAllUser == null || _listAllUser.Count <= 0) RefreshListAllUser();

            int count = _listAllUser.Count;
            for (var i = 0; i < count; i++)
            {
                if (username.Equals(_listAllUser[i].username, StringComparison.CurrentCultureIgnoreCase))
                {
                    _listAllUser[i].onlinestatus = (int)onlineStatus;
                    if (connectionId != null)
                    {
                        _listAllUser[i].connectionid = connectionId;
                    }
                    return true;
                }
            }
            return false;
        }

        public static bool SetOfflineStatus(string connectionId)
        {
            if (_listAllUser == null || _listAllUser.Count <= 0) RefreshListAllUser();

            int count = _listAllUser.Count;
            for (var i = 0; i < count; i++)
            {
                if (connectionId.Equals(_listAllUser[i].connectionid, StringComparison.CurrentCultureIgnoreCase))
                {
                    _listAllUser[i].onlinestatus = (int)OnlineStatus.Offline;
                    _listAllUser[i].connectionid = null;
                    return true;
                }
            }
            return false;
        }

        public static UserInfo GetUserInfo(string username)
        {
            if (_listAllUser == null || _listAllUser.Count <= 0) RefreshListAllUser();

            int count = _listAllUser.Count;
            for (var i = 0; i < count; i++)
            {
                if (username.Equals(_listAllUser[i].username, StringComparison.CurrentCultureIgnoreCase))
                {
                    return _listAllUser[i];
                }
            }
            return null;
        }

        public static bool UpdateUserConnection(string username, string newConnectionId)
        {
            if (_listAllUser == null || _listAllUser.Count <= 0) RefreshListAllUser();

            int count = _listAllUser.Count;
            for (var i = 0; i < count; i++)
            {
                if (username.Equals(_listAllUser[i].username, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (newConnectionId != null)
                    {
                        _listAllUser[i].connectionid = newConnectionId;
                    }
                    return true;
                }
            }
            return false;
        }

        public static UserInfo GetUserInfoByMainConnection(string connection)
        {
            if (_listAllUser == null || _listAllUser.Count <= 0) RefreshListAllUser();

            int count = _listAllUser.Count;
            for (var i = 0; i < count; i++)
            {
                if (connection.Equals(_listAllUser[i].connectionid, StringComparison.CurrentCultureIgnoreCase))
                {
                    return _listAllUser[i];
                }
            }
            return null;
        }

    }
}
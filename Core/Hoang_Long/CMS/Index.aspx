﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Hoang_Long.CMS.Index" %>

<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<%@ Import Namespace="Hoang_Long.Core.Helper" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeaderCph">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainCph">
    <div class="content" style="margin-top: 30px">
        <div class="row">
            <div id="embed-api-auth-container"></div>
            <div id="chart-container"></div>
            <div id="view-selector-container"></div>
        </div>
        <div class="row">
            <div class="col-lg-7">

                <!-- Traffic sources -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">timeline</h6>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <section id="timeline"></section>

                        </div>
                    </div>


                </div>
                <!-- /traffic sources -->

            </div>

            <%--<div class="col-lg-5">

                <!-- Sales stats -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Sales statistics</h6>
                        <div class="heading-elements">
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a>
                    </div>

                    <div class="container-fluid">
                        <div class="row text-center">
                            <section id="view-selector"></section>

                        </div>
                    </div>

                </div>
                <!-- /sales stats -->

            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="FooterCph">
    <!-- Theme JS files -->
    <script type="text/javascript" src="/CMS/Themes/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="/CMS/Themes/js/core/app.js"></script>
    <%=UIHelper.GetConfigByName("DashbroadAnalytics") %>

    <script>
        gapi.analytics.ready(function () {

            // Step 3: Authorize the user.


            gapi.analytics.auth.authorize({
                container: 'auth-button',
                clientid: '436774269610-duljhdnm0r6jcoiacvam3ee7sd2hbnr7.apps.googleusercontent.com'
            });

            // Step 4: Create the view selector.

            var viewSelector = new gapi.analytics.ViewSelector({
                container: 'view-selector'
            });

            // Step 5: Create the timeline chart.

            var timeline = new gapi.analytics.googleCharts.DataChart({
                reportType: 'ga',
                query: {
                    'dimensions': 'ga:date',
                    'metrics': 'ga:sessions',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                },
                chart: {
                    type: 'LINE',
                    container: 'timeline'
                }
            });

            // Step 6: Hook up the components to work together.

            gapi.analytics.auth.on('success', function (response) {
                viewSelector.execute();
            });

            viewSelector.on('change', function (ids) {
                var newIds = {
                    query: {
                        ids: ids
                    }
                }
                timeline.set(newIds).execute();
            });
        });
    </script>
    <%--<script type="text/javascript" src="/CMS/Themes/js/pages/dashboard.js"></script>--%>
</asp:Content>



﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="Hoang_Long.CMS.Pages.Products" %>

<%@ Import Namespace="Mi.Action" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Modules/Product/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/ftag/ftag.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Sản phẩm</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="active"><a href="#"><i class="icon-magazine position-left"></i><span>Đang hiển thị</span></a></li>
                            <li><a href="#"><i class="icon-file-presentation"></i>Hạ xuống</a></li>
                            <li><a href="#"><i class="icon-floppy-disk"></i>Bị xóa</a></li>
                            <li><a href="#"><i class="icon-file-download"></i>Nổi bật </a></li>
                            <!-- /main -->
                            <!-- Main -->
                            <li class="navigation-header"><span>Landing Page</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="#"><i class="icon-newspaper"></i><span>Danh sách</span></a></li>
                            <li><a href="#"><i class="icon-design"></i>Tạo landing page</a></li>
                            <li class="navigation-header"><span>Danh mục</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="#"><i class="icon-menu7"></i><span>Danh sách</span></a></li>
                            <li class="navigation-header"><span>Thông tin mở rộng</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="#"><i class="icon-menu7"></i><span>Nhóm sản phẩm</span></a></li>
                            <li><a href="#"><i class="icon-menu7"></i><span>Hãng sản xuất</span></a></li>
                            <li><a href="#"><i class="icon-menu7"></i><span>Hãng sản xuất</span></a></li>
                            <!-- /main -->
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">
                <!-- Content area -->
                <div class="page-header" id="product-edit">
                    <div class="page-header-content">
                        <div class="panel-heading" id="header-page">
                            <h6 class="panel-title">

                                <button type="button" data-toggle="modal" id="btn-new-product" class="btn btn-default ibtn-xs btn-primary heading-btn _newspost"><i class="icon-file-plus position-left"></i>Thêm sản phẩm</button>
                            </h6>
                            <div class="heading-elements">
                                <div class="input-group">
                                    <input type="text" class="form-control _search-data" placeholder="Tìm kiếm sản phẩm">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
                                    </span>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="icon-menu"></i></a>

                        </div>
                        <div class="navbar navbar-default navbar-xs navbar-component" style="margin-right: 0; margin-left: 0">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-filter">
                                <p class="navbar-text">Lọc:</p>
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <div id="reportrange" class="pull-right date-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;<span></span><b class="caret"></b>
                                        </div>
                                    </li>

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort position-left"></i>Sắp xếp<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><i class="icon-sort-time-desc position-left"></i>Ngày xuất bản</a></li>
                                            <li><a href="#"><i class="icon-sort-alpha-asc position-left"></i>Tên A-Z</a></li>
                                            <li><a href="#"><i class="icon-sort-alpha-desc position-left"></i>Tên Z-A</a></li>
                                            <li><a href="#"><i class="icon-sort-numeric-asc position-left"></i>Lượt xem tăng</a></li>
                                            <li><a href="#"><i class="icon-sort-numberic-desc position-left"></i>Lượt xem giảm</a></li>
                                        </ul>
                                    </li>

                                    <%--<li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-numeric-asc position-left"></i>By priority <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Show all</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Highest</a></li>
                                            <li><a href="#">High</a></li>
                                            <li><a href="#">Normal</a></li>
                                            <li><a href="#">Low</a></li>
                                        </ul>
                                    </li>

                                    <li class="active"><a href="#"><i class="icon-sort-alpha-asc position-left"></i>Asc</a></li>
                                    <li><a href="#"><i class="icon-sort-alpha-desc position-left"></i>Desc</a></li>--%>
                                </ul>

                                <div class="navbar-right">
                                    <ul class="nav navbar-nav" style="padding-right: 20px">
                                        <li>
                                            <div class="IMSPagerStats">151 -> 200 <span>/</span> 2013</div>
                                        </li>
                                        <li>
                                            <input class="pager-input" value="1" />
                                        </li>
                                        <li><a class="pager-back" id="btn-next" href="javascript:void(0)"><i class="icon-arrow-left32"></i></a></li>
                                        <li><a class="pager-next" id="btn-pev" href="javascript:void(0)"><i class="icon-arrow-right32"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="module-content">
                            <div class="btn-group btn-action" style="display: none">
                                <button type="button" class="btn btn-default btn-sm _view" data-popup="tooltip" title="Xem" data-original-title="Top tooltip"><i class="icon-eye"></i></button>
                                <button type="button" class="btn btn-default btn-sm _edit" data-popup="tooltip" data-original-title="Top tooltip" title="Sửa bài"><i class="icon-pencil7"></i></button>
                                <button type="button" class="btn btn-default btn-sm _delete" data-popup="tooltip" data-original-title="Top tooltip" title="Xóa bài"><i class="icon-bin"></i></button>
                            </div>
                            <div id="data-list">
                                <div class="datatable-scroll">
                                    <%= TemplateUtils.LoadPage("\\CMS\\Modules\\Product\\Templates\\List.aspx",Request.Form) %>
                                </div>

                            </div>


                        </div>

                    </div>
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <!-- Modal -->

    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <%--<script type="text/javascript" src="/CMS/Themes/js/plugins/forms/validation/validate.min.js"></script>--%>
    <%--<script type="text/javascript" src="/CMS/Themes/js/plugins/forms/selects/bootstrap_multiselect.js"></script>--%>
    <%--<script type="text/javascript" src="/CMS/Themes/js/plugins/forms/inputs/touchspin.min.js"></script>--%>
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js"></script>
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <script src="/CMS/Themes/js/ftag/ftag.js"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js"></script>
    <script src="/CMS/Themes/js/autosize.min.js"></script>

    <script type="text/javascript" src="/CMS/Themes/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/pickers/anytime.min.js"></script>
    <%--<script type="text/javascript" src="/CMS/Themes/js/pages/components_popups.js"></script>--%>
    <script src="/CMS/Themes/js/maskmoney/jquery.maskMoney.js"></script>
    <%--<script type="text/javascript" src="/CMS/Themes/js/pages/form_validation.js"></script>--%>
    <!--file manager -->
    <script src="/CMS/Themes/js/jsTree/dist/jstree.min.js"></script>
    <script src="/CMS/Modules/FileManager/fmanagers.v1.js"></script>
    <script src="/CMS/Modules/Product/Scripts/main.js"></script>


</asp:Content>

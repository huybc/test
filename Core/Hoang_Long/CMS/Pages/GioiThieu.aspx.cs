﻿using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hoang_Long.CMS.Pages
{
    public partial class GioiThieu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public List<NewsV2.ZoneWithSimpleField> GetAllZoneWithTreeViewSimpleFields()
        {
            List<NewsV2.ZoneWithSimpleField> simplaFields = new List<NewsV2.ZoneWithSimpleField>();

            var zones = CacheObjectBase.GetInstance<ZoneCached>().GetAllZone((int)ZoneType.About).Where(it => it.Status == 1);
            foreach (var zone in zones)
            {
                simplaFields.Add(new NewsV2.ZoneWithSimpleField
                {
                    Id = zone.Id,
                    Name = zone.Name.Trim(),
                    ShortURL = zone.ShortUrl,
                    //   RealName = zone.Name.Trim(),
                    ParentId = zone.ParentId
                });
            }
            return simplaFields;
        }
        public class ZoneWithSimpleField
        {
            public string Name { get; set; }
            public string ShortURL { get; set; }
            // public string RealName { get; set; }
            public int Id { get; set; }
            //  public int ZoneId { get { return this.Id; } }
            public int ParentId { get; set; }
        }
    }
}
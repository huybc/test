﻿using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.Common;
using Mi.Entity.Base.Tag;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
   public class TagCached:CacheObjectBase
    {
        public List<TagNewsWithTagInfoEntity> GetTagNewsByNewsId(long newsId)
        {
            var cachedKey = string.Format("GetTagNewsByNewsId[{0}]", newsId);
            var data = Get<List<TagNewsWithTagInfoEntity>>(newsId+"", cachedKey);
            if (data == null|| data.Count==0)
            {
                using (var db = new CmsMainDb())
                {
                    var listTagNews = db.TagNewsMainDal.GetTagNewsByNewsId(newsId);
                    var newTags =listTagNews.Select(tagNews => new TagNewsWithTagInfoEntity
                    {
                        TagId = tagNews.TagId,
                        NewsId = tagNews.NewsId,
                        TagMode = tagNews.TagMode,
                        Priority = tagNews.Priority,
                        TagProperty = tagNews.TagProperty,
                        Name = tagNews.Name,
                        Url = tagNews.Url
                    }).ToList();
                    data = newTags;
                    Add(newsId+"", cachedKey, data);
                }
            }
            return data;
        }

        //public static List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        //{
        //    try
        //    {
        //        List<TagEntity> returnValue;
        //        using (var db = new CmsMainDb())
        //        {
        //            returnValue = db.TagMainDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
        //        }
        //        return returnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
        //    }
        //    return new List<TagEntity>();
        //}

        public List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            var cachedKey = string.Format("SearchTag[{0}]", zoneId);
            var data = Get<List<TagEntity>>(zoneId + type + "", cachedKey);
            if (data == null|| data.Count==0)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.TagMainDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
                    Add(zoneId + type + "", cachedKey, data);
                }
            }
            return data;
        }
      
    }
}

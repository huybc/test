﻿using System.Collections.Generic;
using Mi.BoCached.Common;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
    public class NewsCached : CacheObjectBase
    {
        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            var cachedKey = string.Format("GetNewsForValidateById[{0}]", id);
            var data = Get<NewsForValidateEntity>(id.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.NewsMainDal.GetNewsForValidateById(id);
                    Add(id.ToString(), cachedKey, data);
                }
            }
            return data;
        }

        public NewsEntity GetNewsById(long id)
        {
            var cachedKey = string.Format("GetNewsById[{0}]", id);
            var data = Get<NewsEntity>("GetNewsById"+id, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.NewsMainDal.GetNewsById(id);
                    Add(id.ToString(), cachedKey, data);
                }
            }
            return data;
        }

        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            var cachedKey = string.Format("GetNewsInZoneByNewsId[{0}]", newsId);
            var data = Get<List<NewsInZoneEntity>>("GetNewsInZoneByNewsId"+newsId, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.NewsInZoneMainDal.GetNewsInZoneByNewsId(newsId);
                Add(newsId.ToString(), cachedKey, data);
            }
            }
            return data;
        }
    }
}

﻿using System.Collections.Generic;
using Mi.BoCached.Common;
using Mi.Entity.Base.Security;
using Mi.Entity.ErrorCode;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
    public class PermissionCached : CacheObjectBase
    {
        public ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId)
        {
            var cachedKey = string.Format("CheckUserPermission[{0},{1}]", username, permissionId);
            var data = Get<string>(username, cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                using (var db = new CmsMainDb())
                {
                    var existsUser = db.UserMainDal.GetUserByUsername(username);
                    if (null == existsUser)
                    {
                        data = bool.FalseString;
                    }
                    else
                    {
                        if (existsUser.IsFullPermission)
                        {
                            data = bool.TrueString;
                        }
                        else
                        {
                            var userPermission = db.UserPermissionMainDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);

                            data = userPermission.Count > 0 ? bool.TrueString : bool.FalseString;
                        }
                    }
                }
                Add(username, cachedKey, data);
            }
            return data == bool.TrueString ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId, int zoneId)
        {
            var cachedKey = string.Format("CheckUserPermission[{0},{1}]", username, permissionId);
            var data = Get<string>(username, cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                using (var db = new CmsMainDb())
                {
                    var existsUser = db.UserMainDal.GetUserByUsername(username);
                    if (null == existsUser)
                    {
                        data = bool.FalseString;
                    }
                    else
                    {
                        if (existsUser.IsFullPermission)
                        {
                            data = bool.TrueString;
                        }
                        else
                        {
                            var userPermission = new UserPermissionEntity
                            {
                                UserId = existsUser.Id,
                                PermissionId = permissionId,
                                ZoneId = zoneId
                            };

                            data = db.UserPermissionMainDal.CheckUserPermission(userPermission) ? bool.TrueString : bool.FalseString;
                        }
                    }
                }
                Add(username, cachedKey, data);
            }
            return data == bool.TrueString ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public List<UserPermissionEntity> GetListByUserName(string username)
        {
            var cachedKey = string.Format("GetListByUserName[{0}]", username);
            var data = Get<List<UserPermissionEntity>>(username, cachedKey);
            if (data == null || data.Count == 0)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.UserPermissionMainDal.GetListUserPermissionByUserName(username);
                    Add(username, cachedKey, data);
                }
            }
            return data;
        }

        public bool CheckUserInGroupPermission(int userId, int groupId)
        {
            var cachedKey = string.Format("CheckUserInGroupPermission[{0},{1}]", userId, groupId);
            var data = Get<string>(userId.ToString(), cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                using (var db = new CmsMainDb())
                {
                    data = db.GroupPermissionMainDal.CheckUserInGroupPermission(userId, groupId)
                           ? bool.TrueString
                           : bool.FalseString;
                    Add(userId.ToString(), cachedKey, data);
                }
            }
            return data == bool.TrueString;
        }

        public bool RemoveAllCachedByGroup(int userId)
        {
            RemoveAllCachedByGroup(userId.ToString());
            using (var db = new CmsMainDb())
            {
                var user = db.UserMainDal.GetUserById(userId);
                if (user != null)
                {
                    RemoveAllCachedByGroup(user.UserName);
                }
            }
            return true;
        }

        public override void RemoveAllCachedByGroup(string group, bool removeGroup = true)
        {
            using (var db = new CmsMainDb())
            {
                var user = db.UserMainDal.GetUserByUsername(group);
                if (user != null)
                {
                    RemoveAllCachedByGroup(user.Id.ToString());
                }
            }
            base.RemoveAllCachedByGroup(group, removeGroup);
        }
    }
}

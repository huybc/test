﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mi.BoCached.Common;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.MainDal.Databases;

namespace Mi.BoCached.CacheObjects
{
    public class ZoneCached : CacheObjectBase
    {
        public ZoneEntity GetZoneById(int id)
        {

            var cachedKey = string.Format("GetZoneById[{0}]", id);
            var data = Get<ZoneEntity>(id.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetZoneById(id);
                    Add(id.ToString(), cachedKey, data);
                }
            }
            return data;
        }
        public ZoneEntity GetZoneByAlias(string alias)
        {

            var cachedKey = string.Format("GetZoneByAlias[{0}]", alias);
            var data = Get<ZoneEntity>(alias, cachedKey);
            if (data == null)
            {
                try
                {
                    using (var db = new CmsMainDb())
                    {
                        data = db.ZoneMainDal.GetZoneByAlias(alias);
                        Add(alias, cachedKey, data);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    return new ZoneEntity();
                }
            }
            return data;
        }



        //public List<ZoneEntity> GetAllZone()
        //{
        //    var cachedKey = string.Format("GetAllZone[{0}]", "all");
        //    var data = Get<List<ZoneEntity>>("all", cachedKey);
        //    if (data == null)
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            data = db.ZoneMainDal.GetZoneByParentId(-1);
        //            Add("all", cachedKey, data);
        //        }

        //    }
        //    return data;
        //}
        public List<ZoneEntity> GetAllZone(int type)
        {
            var cachedKey = string.Format("GetAllZone[{0}]", "all");
            var data = Get<List<ZoneEntity>>("all"+ type, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetZoneByParentId(-1,type).Where(it=>it.Status==(int)ZoneStatus.Publish).ToList();
                    Add("all"+ type, cachedKey, data);
                }

            }
            return data;
        }
        public List<ZoneEntity> GetListZoneByParentId(int parentZoneId,int type)
        {
            var cachedKey = string.Format("GetListZoneByParentId[{0}]", parentZoneId+type);
            var data = Get<List<ZoneEntity>>(parentZoneId+ type.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetZoneByParentId(parentZoneId, type);
                    Add(parentZoneId.ToString(), cachedKey, data);
                }

            }
            return data;
        }
        public List<ZoneSimpleFieldEntity> GetDisplayHomePage(int type)
        {
            var cachedKey = string.Format("GetDisplayHomePage[{0}]", type);
            var data = Get<List<ZoneSimpleFieldEntity>>("GetDisplayHomePage", cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetDisplayHomePage(type).ToList();
                    Add("GetDisplayHomePage", cachedKey, data);
                }

            }
            return data;
        }
        public List<ZoneEntity> GetListPropertyByParentIdAndType(int parentId, int type)
        {
            var cachedKey = string.Format("GetListPropertyByParentIdAndType[{0}]", parentId+""+ type);
            var data = Get<List<ZoneEntity>>(parentId +""+ type, cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetListPropertyByParentIdAndType( parentId,  type).ToList();
                    Add(parentId + "" + type, cachedKey, data);
                }

            }
            return data;
        }
        public List<ZoneEntity> GetBreadCrumbByCateoryId(int id)
        {
            var cachedKey = string.Format("GetBreadCrumbByCateoryId[{0}]", id);
            var data = Get<List<ZoneEntity>>(id.ToString(), cachedKey);
            if (data == null)
            {
                using (var db = new CmsMainDb())
                {
                    data = db.ZoneMainDal.GetBreadCrumbByZoneId(id);
                    Add(id.ToString(), cachedKey, data);
                }

            }
            return data;
        }

        //public ZoneEntity GetRootZoneByChildId(string alias)
        //{
        //    var cachedKey = string.Format("GetRootZoneByChildId[{0}]", alias);
        //    var data = Get<ZoneEntity>(alias, cachedKey);
        //    if (data == null)
        //    {
        //        using (var db = new CmsMainDb())
        //        {
        //            data = db.ZoneMainDal.GetRootZoneByChildId(alias);
        //            Add(alias, cachedKey, data);
        //        }
        //    }
        //    return data;
        //}
    }
}


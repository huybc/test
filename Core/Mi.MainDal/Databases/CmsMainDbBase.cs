﻿//using Mi.MainDal.External.CafeF;

using System.Data;
using Mi.Common.DalCommon;
using Mi.MainDal.Base.Account;
using Mi.MainDal.Base.Answer;
using Mi.MainDal.Base.AuthenticateLog;
using Mi.MainDal.Base.Cadastral;
using Mi.MainDal.Base.Comment;
using Mi.MainDal.Base.Connection;
using Mi.MainDal.Base.ContentLog;
using Mi.MainDal.Base.Document;
using Mi.MainDal.Base.FileUpload;
using Mi.MainDal.Base.News;
using Mi.MainDal.Base.Notify;
using Mi.MainDal.Base.Point;
using Mi.MainDal.Base.Product;
using Mi.MainDal.Base.Tag;
using Mi.MainDal.Base.ViewCount;
using Mi.MainDal.Base.Zone;
using Mi.MainDal.Base.Company;
using Mi.MainDal.Base.Customer;
using Mi.MainDal.Base.Debt;
using Mi.MainDal.Base.Media;
using Mi.MainDal.Base.ProjectDetail;
using Mi.MainDal.Base.UserAction;

namespace Mi.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Account

        private UserDal _userMainDal;
        public UserDal UserMainDal
        {
            get { return _userMainDal ?? (_userMainDal = new UserDal((CmsMainDb)this)); }
        }
        private UserExperienceDal _userExperienceDal;
        public UserExperienceDal UserExperienceDal
        {
            get { return _userExperienceDal ?? (_userExperienceDal = new UserExperienceDal((CmsMainDb)this)); }
        }

        private GroupPermissionDal _groupPermissionMainDal;
        public GroupPermissionDal GroupPermissionMainDal
        {
            get { return _groupPermissionMainDal ?? (_groupPermissionMainDal = new GroupPermissionDal((CmsMainDb)this)); }
        }

        private PermissionDal _permissionMainDal;
        public PermissionDal PermissionMainDal
        {
            get { return _permissionMainDal ?? (_permissionMainDal = new PermissionDal((CmsMainDb)this)); }
        }

        private PermissionTemplateDal _permissionTemplateMainDal;
        public PermissionTemplateDal PermissionTemplateMainDal
        {
            get { return _permissionTemplateMainDal ?? (_permissionTemplateMainDal = new PermissionTemplateDal((CmsMainDb)this)); }
        }

        private UserPenNameDal _userPenNameMainDal;
        public UserPenNameDal UserPenNameMainDal
        {
            get { return _userPenNameMainDal ?? (_userPenNameMainDal = new UserPenNameDal((CmsMainDb)this)); }
        }

        private UserPermissionDal _userPermissionMainDal;
        public UserPermissionDal UserPermissionMainDal
        {
            get { return _userPermissionMainDal ?? (_userPermissionMainDal = new UserPermissionDal((CmsMainDb)this)); }
        }

        private UserSmsDal _userSmsMainDal;
        public UserSmsDal UserSmsMainDal
        {
            get { return _userSmsMainDal ?? (_userSmsMainDal = new UserSmsDal((CmsMainDb)this)); }
        }

        private AuthenticateLogDal _authenticateLogMainDal;
        public AuthenticateLogDal AuthenticateLogMainDal
        {
            get { return _authenticateLogMainDal ?? (_authenticateLogMainDal = new AuthenticateLogDal((CmsMainDb)this)); }
        }

        #endregion

        #region News
        private NewsDal _newsMainDal;
        public NewsDal NewsMainDal
        {
            get { return _newsMainDal ?? (_newsMainDal = new NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region File Upload
        private FileUploadDal _fileUploadMainDal;
        public FileUploadDal FileUploadMainDal
        {
            get { return _fileUploadMainDal ?? (_fileUploadMainDal = new FileUploadDal((CmsMainDb)this)); }
        }
        #endregion
        #region File Folder
        private FolderDal _folderMainDal;
        public FolderDal FolderMainDal
        {
            get { return _folderMainDal ?? (_folderMainDal = new FolderDal((CmsMainDb)this)); }
        }
        #endregion
        #region File Folder
        private ContentLogDal _contentLogMainDal;
        public ContentLogDal ContentLogMainDal
        {
            get { return _contentLogMainDal ?? (_contentLogMainDal = new ContentLogDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Extension
        private NewsExtensionDal _newsExtensionMainDal;
        public NewsExtensionDal NewsExtensionMainDal
        {
            get { return _newsExtensionMainDal ?? (_newsExtensionMainDal = new NewsExtensionDal((CmsMainDb)this)); }
        }
        #endregion


        #region News In Zone
        private NewsInZoneDal _newsInZoneMainDal;
        public NewsInZoneDal NewsInZoneMainDal
        {
            get { return _newsInZoneMainDal ?? (_newsInZoneMainDal = new NewsInZoneDal((CmsMainDb)this)); }
        }
        #endregion


        #region Zone
        private ZoneDal _zoneMainDal;
        public ZoneDal ZoneMainDal
        {
            get { return _zoneMainDal ?? (_zoneMainDal = new ZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region Media
        private MediaDal _mediaDal;
        public MediaDal MediaDal
        {
            get { return _mediaDal ?? (_mediaDal = new MediaDal((CmsMainDb)this)); }
        }
        #endregion

        #region ZoneDefaultTag
        private ZoneDefaultTagDal _zoneDefaultTagDal;
        public ZoneDefaultTagDal ZoneDefaultTagMainDal
        {
            get { return _zoneDefaultTagDal ?? (_zoneDefaultTagDal = new ZoneDefaultTagDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagZone
        private TagZoneDal _tagZoneMainDal;
        public TagZoneDal TagZoneMainDal
        {
            get { return _tagZoneMainDal ?? (_tagZoneMainDal = new TagZoneDal((CmsMainDb)this)); }
        }
        #endregion


        #region TagNewsDal
        private TagNewsDal _tagNewsMainDal;
        public TagNewsDal TagNewsMainDal
        {
            get { return _tagNewsMainDal ?? (_tagNewsMainDal = new TagNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagDal
        private TagDal _tagMainDal;
        public TagDal TagMainDal
        {
            get { return _tagMainDal ?? (_tagMainDal = new TagDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Config
        private ConfigDal _newsConfigMainDal;
        public ConfigDal ConfigMainDal
        {
            get { return _newsConfigMainDal ?? (_newsConfigMainDal = new ConfigDal((CmsMainDb)this)); }
        }
        #endregion


        #region Comment
        private CommentDal _commentMainDal;
        public CommentDal CommentMainDal
        {
            get { return _commentMainDal ?? (_commentMainDal = new CommentDal((CmsMainDb)this)); }
        }
        #endregion

        #region Product
        private ProductDal _productMainDal;
        public ProductDal ProductMainDal
        {
            get { return _productMainDal ?? (_productMainDal = new ProductDal((CmsMainDb)this)); }
        }

        #endregion
        private ProductInShopDal _productInShopDal;
        public ProductInShopDal ProductInShopMainDal
        {
            get { return _productInShopDal ?? (_productInShopDal = new ProductInShopDal((CmsMainDb)this)); }
        }
        #region Shop
        private ShopDal _shopMainDal;
        public ShopDal ShopMainDal
        {
            get { return _shopMainDal ?? (_shopMainDal = new ShopDal((CmsMainDb)this)); }
        }
        #endregion

        #region Answer
        private Answeredal _answerMainDal;
        public Answeredal AnswerMainDal
        {
            get { return _answerMainDal ?? (_answerMainDal = new Answeredal((CmsMainDb)this)); }
        }
        #endregion
        #region Point
        private PointDal _pointMainDal;
        public PointDal PointMainDal
        {
            get { return _pointMainDal ?? (_pointMainDal = new PointDal((CmsMainDb)this)); }
        }
        #endregion

        #region Notify
        private NotifyDal _notifyMainDal;
        public NotifyDal NotifyMainDal
        {
            get { return _notifyMainDal ?? (_notifyMainDal = new NotifyDal((CmsMainDb)this)); }
        }
        #endregion

        #region UserAction
        private UserActionDal _userActionMainDal;
        public UserActionDal UserActionMainDal
        {
            get { return _userActionMainDal ?? (_userActionMainDal = new UserActionDal((CmsMainDb)this)); }
        }
        #endregion 
        #region UserAction
        private ViewCountDal _viewCountMainDal;
        public ViewCountDal ViewCountMainDal
        {
            get { return _viewCountMainDal ?? (_viewCountMainDal = new ViewCountDal((CmsMainDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion

        #region Connection
        private ConnectionDal _connectionMainDal;
        public ConnectionDal ConnectionMainDal
        {
            get { return _connectionMainDal ?? (_connectionMainDal = new ConnectionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News EmbedBox OnPage
        private NewsEmbedBoxOnPageDal _newsEmbedBoxOnPageMainDal;
        public NewsEmbedBoxOnPageDal NewsEmbedBoxOnPageMainDal
        {
            get { return _newsEmbedBoxOnPageMainDal ?? (_newsEmbedBoxOnPageMainDal = new NewsEmbedBoxOnPageDal((CmsMainDb)this)); }
        }
        #endregion
        #region Cadastral
        private CadastralDal _cadastralDal;
        public CadastralDal CadastralMainDal
        {
            get { return _cadastralDal ?? (_cadastralDal = new CadastralDal((CmsMainDb)this)); }
        }
        #endregion
        #region GovDocument
        private GovDocumentDal _documentDal;
        public GovDocumentDal GovDocumentlDal
        {
            get { return _documentDal ?? (_documentDal = new GovDocumentDal((CmsMainDb)this)); }
        }
        #endregion

        #region CompanyDal
        private CompanyDal _companyDal;
        public CompanyDal CompanyDal
        {
            get { return _companyDal ?? (_companyDal = new CompanyDal((CmsMainDb)this)); }
        }
        #endregion
        #region Debt
        private DebtDal _debtDal;
        public DebtDal DebtDal
        {
            get { return _debtDal ?? (_debtDal = new DebtDal((CmsMainDb)this)); }
        }
        #endregion

        #region ProjectDetail
        private ProjectDetailDal _projectDetailDal;
        public ProjectDetailDal ProjectDetailDal
        {
            get { return _projectDetailDal ?? (_projectDetailDal = new ProjectDetailDal((CmsMainDb)this)); }
        }
        #endregion

        #region Customer
        private CustomerDal _customerDal;
        public CustomerDal CustomerDal
        {
            get { return _customerDal ?? (_customerDal = new CustomerDal((CmsMainDb)this)); }
        }
        #endregion
        #endregion
    }
}
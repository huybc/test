﻿using System.Data;
using System.Data.SqlClient;
using Mi.Common.ChannelConfig;
using Mi.MainDal.Common;

namespace Mi.MainDal.Databases
{
    public class CmsMainDb : CmsMainDbBase
    {
        private const string ConnectionStringName = "CmsMainDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString("Full",
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
} 
﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Connection;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Connection
{
    public abstract class ConnectionDalBase
    {
        #region Gets
        public ConnectionEntity GetConnectionByConnectionId(string connectionId)
        {
            const string commandText = "CMS_Connection_GetByConnectionId";
            try
            {
                ConnectionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConnectionId", connectionId);
                data = _db.Get<ConnectionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<ConnectionEntity> GetConnectionByUserName(string userName)
        {
            const string commandText = "CMS_Connection_GetByUsername";
            try
            {
                List<ConnectionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<ConnectionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Sets

        public bool Update(ConnectionEntity connectionEntity)
        {

            const string commandText = "CMS_Connection_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConnectionId", connectionEntity.ConnectionId);
                _db.AddParameter(cmd, "UserAgent", connectionEntity.UserAgent);
                _db.AddParameter(cmd, "UserName", connectionEntity.UserName);
                _db.AddParameter(cmd, "Connected", connectionEntity.Connected);
                var totalRow = cmd.ExecuteNonQuery();
                return totalRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ConnectionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

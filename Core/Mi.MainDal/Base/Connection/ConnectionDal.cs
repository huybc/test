﻿using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Connection
{
    public class ConnectionDal : ConnectionDalBase
    {
        internal ConnectionDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}

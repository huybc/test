﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public abstract class NewsInZoneDalBase
    {
        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsInZone_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsInZoneEntity> data = _db.GetList<NewsInZoneEntity>(cmd);
                Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(data));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInZoneEntity> GetNewsInZoneByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_NewsInZone_GetByListNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                List<NewsInZoneEntity> data = _db.GetList<NewsInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(NewsInZoneEntity newsInZone)
        {
            const string commandText = "CMS_NewsInZone_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", newsInZone.ZoneId);
                _db.AddParameter(cmd, "NewsId", newsInZone.NewsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsInZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

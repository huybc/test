﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public abstract class NewsEmbedBoxOnPageDalBase
    {

        public List<NewsEmbedBoxOnPageListEntity> GetListNewsEmbedBoxOnPage(long zoneId, int type)
        {
            const string commandText = "CMS_BoxNewsEmbed_GetListByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", Convert.ToInt32(zoneId));
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                List<NewsEmbedBoxOnPageListEntity> data = _db.GetList<NewsEmbedBoxOnPageListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEmbedBoxOnPageListEntity> GetListProductEmbedBoxOnPage(dynamic zoneId, int type)
        {
            const string commandText = "CMS_BoxProductEmbed_GetListByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                IEnumerable<NewsEmbedBoxOnPageListEntity> data = _db.GetList<NewsEmbedBoxOnPageListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEmbedBoxEntity> GetListNewsEmbedByTypeAndZone(int zoneId, int type)
        {
            const string commandText = "FE_BoxNewsEmbed_GetByTypeAndZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", Convert.ToInt32(zoneId));
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                IEnumerable<NewsEmbedBoxEntity> data = _db.GetList<NewsEmbedBoxEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<NewsEmbedBoxOnPageListEntity> EF_BoxProductEmbed_GetListByZone(int zoneId, int type,int pageSize)
        {
            const string commandText = "EF_BoxProductEmbed_GetListByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var data = _db.GetList<NewsEmbedBoxOnPageListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(NewsEmbedBoxOnPageEntity newsEmbebBox)
        {
            const string commandText = "CMS_BoxNewsEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", newsEmbebBox.ZoneId);
                _db.AddParameter(cmd, "NewsId", newsEmbebBox.NewsId);
                _db.AddParameter(cmd, "SortOrder", newsEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", newsEmbebBox.Type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(string listNewsId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxNewsEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long newsId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxNewsEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAll(dynamic zoneId, int type)
        {
            const string commandText = "CMS_BoxNewsEmbed_DeleteAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsEmbedBoxOnPageDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

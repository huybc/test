﻿using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public class NewsEmbedBoxOnPageDal : NewsEmbedBoxOnPageDalBase
    {
        internal NewsEmbedBoxOnPageDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}

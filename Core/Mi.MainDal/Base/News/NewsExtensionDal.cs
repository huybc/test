﻿using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public class NewsExtensionDal : NewsExtensionDalBase
    {
        internal NewsExtensionDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}

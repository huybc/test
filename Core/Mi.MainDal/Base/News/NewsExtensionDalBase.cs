﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.News
{
    public abstract class NewsExtensionDalBase
    {
        public NewsExtensionEntity GetValue(long newsId, int type)
        {
            const string commandText = "CMS_NewsExtension_GetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                NewsExtensionEntity data = _db.Get<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetMaxValue(long newsId, int type)
        {
            const string commandText = "CMS_NewsExtension_GetMaxValue";
            try
            {
                var maxValue = 1;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MaxValue", maxValue, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.ExecuteReader(cmd);
                maxValue = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return maxValue;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(long newsId, int type, string value)
        {
            const string commandText = "CMS_NewsExtension_SetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsExtension_DeleteByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsExtension_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsExtensionEntity> data = _db.GetList<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetByTypeAndVaue(int type, string value)
        {
            const string commandText = "CMS_NewsExtension_GetByTypeAndValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                List<NewsExtensionEntity> data = _db.GetList<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsExtensionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

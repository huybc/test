﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Product
{
    public abstract class ProductInShopDalBase
    {
        public List<ProductInShopEntity> GetProductInShopByProductId(int productId)
        {
            const string commandText = "CMS_ProductInShop_GetByProductId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProductId", productId);
                List<ProductInShopEntity> data = _db.GetList<ProductInShopEntity>(cmd);
                Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(data));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
      
        #region Core members

        private readonly CmsMainDb _db;

        protected ProductInShopDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Product
{
    public abstract class ProductDalBase
    {
        #region Gets

        public ProductEntity GetById(int id)
        {
            const string commandText = "CMS_Product_GetById";
            try
            {
                ProductEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ProductEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ProductEntity GetByIdV2(int id)
        {
            const string commandText = "CMS_Product_GetById_V2";
            try
            {
                ProductEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ProductEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ProductInNewsWithProductInfoEntity> GetProductInNewByNewsId(long newsId, byte type)
        {

            const string commandText = "CMS_ProductNews_GetByNewsId";
            try
            {
                List<ProductInNewsWithProductInfoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<ProductInNewsWithProductInfoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ProductEntity> Search(string keyword, string username, string zoneIds, DateTime from, DateTime to, int type, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_Product_Search";
            try
            {
                List<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProductEntity> FE_Product_Search(string keyword, string username, string zoneIds, double priceFrom, double priceTo,
            DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Product_Search";
            try
            {
                List<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "PriceFrom", priceFrom);
                _db.AddParameter(cmd, "PriceTo", priceTo);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "IsDiscountNew", isDiscountNew);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ProductEntity> FE_Product_Search_V1(string keyword, string username, string zoneIds, double priceFrom, double priceTo,
                   DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int group, int propertyId, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Product_Search_V1";
            try
            {
                IEnumerable<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "PriceFrom", priceFrom);
                _db.AddParameter(cmd, "PriceTo", priceTo);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "IsDiscountNew", isDiscountNew);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "GroupId", group);
                _db.AddParameter(cmd, "PropertyId", propertyId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ProductEntity> FE_Product_Search_V1(string keyword, string username, string zoneIds, double priceFrom, double priceTo,
                   DateTime from, DateTime to, int isDiscountNew, int type, int sortOrder, int status, int group, string propertyListId, int manufacturerId, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Product_Search_XeNang";
            try
            {
                IEnumerable<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "PriceFrom", priceFrom);
                _db.AddParameter(cmd, "PriceTo", priceTo);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "IsDiscountNew", isDiscountNew);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "GroupId", group);
                _db.AddParameter(cmd, "PropertyListId", propertyListId);
                _db.AddParameter(cmd, "ManufacturerId", manufacturerId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public IEnumerable<ProductEntity> RecentView(string keyword, string username, string zoneIds, string productIds, DateTime from, DateTime to, int type, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Product_RecentView";
            try
            {
                List<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "ProductIds", productIds);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public IEnumerable<ProductNearLocationEntity> FE_Product_SearchWithNearLocation(string keyword, string username,
            string zoneIds, double priceFrom, double priceTo, DateTime from, DateTime to, int distance, double longitude, double latitude, int isDiscountNew, int type,
            int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "FE_Product_SearchWithNearLocation";
            try
            {
                List<ProductNearLocationEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "PriceFrom", priceFrom);
                _db.AddParameter(cmd, "PriceTo", priceTo);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "Distance", distance);
                _db.AddParameter(cmd, "Longitude", longitude);
                _db.AddParameter(cmd, "Latitude", latitude);
                _db.AddParameter(cmd, "IsDiscountNew", isDiscountNew);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductNearLocationEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProductEntity> SearchSuggest(string keyword, string username, string zoneIds, DateTime from, DateTime to, int type, int sortOrder, int status)
        {
            const string commandText = "CMS_Product_Search_Suggest";
            try
            {
                List<ProductEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                //_db.AddParameter(cmd, "PageIndex", pageIndex);
                //_db.AddParameter(cmd, "PageSize", pageSize);
                //_db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductEntity>(cmd);
                // totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ProductBaseEntity GetProductBaseById(int id)
        {
            const string commandText = "CMS_ProductBase_GetById";
            try
            {
                ProductBaseEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ProductBaseEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ProductBaseEntity> SearchProductBase(string keyword, string username, string zoneIds, DateTime from, DateTime to, int type, int sortOrder, int status, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_ProductBase_Search";
            try
            {
                List<ProductBaseEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (from <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", from);
                if (to <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", to);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<ProductBaseEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<ProductEmbedEntity> EF_BoxProductEmbedGetListByZone(int zone, int type, int pageSize)
        {

            const string commandText = "EF_BoxProductEmbed_GetListByZone";
            try
            {
                List<ProductEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zone);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ProductEmbedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Sets

        public bool Insert(ProductEntity product, string zoneIds, string tagIdList, string shopIdList, ref int id)
        {

            const string commandText = "CMS_Product_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", product.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", product.Title);
                _db.AddParameter(cmd, "Sapo", product.Sapo);
                _db.AddParameter(cmd, "Detail", product.Detail);
                _db.AddParameter(cmd, "Status", product.Status);
                _db.AddParameter(cmd, "CreatedBy", product.CreatedBy);
                _db.AddParameter(cmd, "Type", product.Type);
                _db.AddParameter(cmd, "Avatar", product.Avatar);
                _db.AddParameter(cmd, "AvatarArray", product.AvatarArray);
                _db.AddParameter(cmd, "ZoneIdList", zoneIds);
                //_db.AddParameter(cmd, "tagIdList", tagIdList);
                //_db.AddParameter(cmd, "ShopIdList", shopIdList);
                _db.AddParameter(cmd, "Url", product.Url);

                _db.AddParameter(cmd, "Price", product.Price);
                _db.AddParameter(cmd, "SalesOff", product.SalesOff);
                _db.AddParameter(cmd, "DiscountPrice", product.DiscountPrice);
                _db.AddParameter(cmd, "Percent", product.Percent);
                _db.AddParameter(cmd, "OriginType", product.OriginType);
                _db.AddParameter(cmd, "Usage", product.Usage);
                _db.AddParameter(cmd, "PromotionInfo", product.PromotionInfo);
                _db.AddParameter(cmd, "BackLink", product.BackLink);
                _db.AddParameter(cmd, "Tips", product.Tips);
                _db.AddParameter(cmd, "VAT", product.VAT);
                _db.AddParameter(cmd, "Unit", product.Unit);
                _db.AddParameter(cmd, "MetaKeyword", product.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", product.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", product.TitleSeo);
                _db.AddParameter(cmd, "ManufacturerId", product.ManufacturerId);
                _db.AddParameter(cmd, "GroupId", product.GroupId);
                _db.AddParameter(cmd, "Warranty", product.Warranty);
                _db.AddParameter(cmd, "Pcode", product.PCode);
                _db.AddParameter(cmd, "PropertyListId", product.PropertyIdList);

                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0 &&
                id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(ProductEntity product, string zoneIds, string tagIdList, string shopIdList)
        {

            const string commandText = "CMS_Product_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", product.Id);
                _db.AddParameter(cmd, "Title", product.Title);
                _db.AddParameter(cmd, "Sapo", product.Sapo);
                _db.AddParameter(cmd, "Detail", product.Detail);
                _db.AddParameter(cmd, "Status", product.Status);
                _db.AddParameter(cmd, "CreatedBy", product.CreatedBy);
                _db.AddParameter(cmd, "Type", product.Type);
                _db.AddParameter(cmd, "Avatar", product.Avatar);
                _db.AddParameter(cmd, "AvatarArray", product.AvatarArray);
                _db.AddParameter(cmd, "ZoneIdList", zoneIds);
                _db.AddParameter(cmd, "Url", product.Url);
                _db.AddParameter(cmd, "Price", product.Price);
                _db.AddParameter(cmd, "SalesOff", product.SalesOff);
                _db.AddParameter(cmd, "DiscountPrice", product.DiscountPrice);
                _db.AddParameter(cmd, "Percent", product.Percent);
                _db.AddParameter(cmd, "OriginType", product.OriginType);
                _db.AddParameter(cmd, "Usage", product.Usage);
                _db.AddParameter(cmd, "PromotionInfo", product.PromotionInfo);
                _db.AddParameter(cmd, "BackLink", product.BackLink);
                _db.AddParameter(cmd, "Tips", product.Tips);
                _db.AddParameter(cmd, "VAT", product.VAT);
                _db.AddParameter(cmd, "Unit", product.Unit);
                _db.AddParameter(cmd, "MetaKeyword", product.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", product.MetaDescription);
                _db.AddParameter(cmd, "TitleSeo", product.TitleSeo);
                _db.AddParameter(cmd, "ManufacturerId", product.ManufacturerId);
                _db.AddParameter(cmd, "GroupId", product.GroupId);
                _db.AddParameter(cmd, "Warranty", product.Warranty);

                _db.AddParameter(cmd, "PCode", product.PCode);
                //  _db.AddParameter(cmd, "ShopId", product.ShopId);
                _db.AddParameter(cmd, "PropertyListId", product.PropertyIdList);

                return cmd.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateCatelog(ProductEntity product)
        {

            const string commandText = "CMS_Product_UpdateCatelog";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", product.Id);
                _db.AddParameter(cmd, "Catalog", product.Catalog);
                return cmd.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatus(int id, int status)
        {
            const string commandText = "CMS_Product_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool InsertProductBase(ProductBaseEntity product, string zoneIds, string tagIdList, ref int id)
        {

            const string commandText = "CMS_Product_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", product.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", product.Title);
                _db.AddParameter(cmd, "Detail", product.Detail);
                _db.AddParameter(cmd, "Status", product.Status);
                _db.AddParameter(cmd, "CreatedBy", product.CreatedBy);
                _db.AddParameter(cmd, "Type", product.Type);
                _db.AddParameter(cmd, "ZoneId", zoneIds);
                _db.AddParameter(cmd, "tagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", product.Url);
                _db.AddParameter(cmd, "Avatar", product.Avatar);
                _db.AddParameter(cmd, "Avatar2", product.Avatar2);
                _db.AddParameter(cmd, "Avatar3", product.Avatar3);
                _db.AddParameter(cmd, "Avatar4", product.Avatar4);
                _db.AddParameter(cmd, "Avatar5", product.Avatar5);
                _db.AddParameter(cmd, "Price", product.Price);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateProductBase(ProductBaseEntity product, string zoneIds, string tagIdList)
        {

            const string commandText = "CMS_ProductBase_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", product.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", product.Title);
                _db.AddParameter(cmd, "Detail", product.Detail);
                _db.AddParameter(cmd, "Status", product.Status);
                _db.AddParameter(cmd, "CreatedBy", product.CreatedBy);
                _db.AddParameter(cmd, "Type", product.Type);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "tagIdList", tagIdList);
                _db.AddParameter(cmd, "Url", product.Url);
                _db.AddParameter(cmd, "Avatar", product.Avatar);
                _db.AddParameter(cmd, "Avatar2", product.Avatar2);
                _db.AddParameter(cmd, "Avatar3", product.Avatar3);
                _db.AddParameter(cmd, "Avatar4", product.Avatar4);
                _db.AddParameter(cmd, "Avatar5", product.Avatar5);
                _db.AddParameter(cmd, "Price", product.Price);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_SellOut(int id, bool sellOut)
        {

            const string commandText = "CMS_Product_Update_SellOut";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "SellOut", sellOut);

                return cmd.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ProductDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Entity.Base.ProjectDetail;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.ProjectDetail
{
    public abstract class ProjectDetailDalBase
    {
        #region Gets

        public List<ProjectDetailEntity> GetByArticleId(long id)
        {
            const string commandText = "CMS_ProjectDetail_GetByArticleId";
            try
            {
                List<ProjectDetailEntity> data = new List<ProjectDetailEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ArticleId", id);
                
                data = _db.GetList<ProjectDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
     
        #endregion

        #region Sets


        public bool Create(ProjectDetailEntity ProjectDetail,ref int id)
        {

            const string commandText = "CMS_ProjectDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ArticleId", ProjectDetail.ArticleId);
                _db.AddParameter(cmd, "Title", ProjectDetail.Title);
                _db.AddParameter(cmd, "Content", ProjectDetail.Content);
                _db.AddParameter(cmd, "Image", ProjectDetail.Image);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(long id)
        {

            const string commandText = "CMS_ProjectDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ProjectDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

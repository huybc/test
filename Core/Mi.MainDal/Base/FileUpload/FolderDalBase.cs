﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Folder;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.FileUpload
{
    public abstract class FolderDalBase
    {
        #region Update

        public bool InsertFolder(FolderEntity obj, ref int newFolderId)
        {
            const string commandText = "CMS_Folder_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Icon", obj.Icon);
                _db.AddParameter(cmd, "CreatedDate", obj.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "FolderPath", obj.FolderPath);

                var numberOfRow = cmd.ExecuteNonQuery();
                newFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFolder(FolderEntity obj, ref int newFolderId)
        {
            const string commandText = "CMS_Folders_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "ModifyBy", obj.ModifyBy);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "FolderPath", obj.FolderPath);

                var numberOfRow = cmd.ExecuteNonQuery();
             //   newFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFolder(int FolderId)
        {
            const string commandText = "CMS_Folder_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FolderId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
       

        #endregion

        #region Get

        public FolderEntity GetFolderById(int FolderId)
        {
            const string commandText = "CMS_Folder_GetById";
            try
            {
                FolderEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FolderId);
                data = _db.Get<FolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public IEnumerable<FolderEntity> Search()
        {
            const string commandText = "CMS_Folders_Search";
            try
            {
                IEnumerable<FolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                //  _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);

                data = _db.GetList<FolderEntity>(cmd);

                //  totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
       public IEnumerable<FolderEntity> FolderPathById(int id)
        {
            const string commandText = "CMS_Folder_GetAllParentSubId";
            try
            {
                List<FolderEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChildId", id);

                data = _db.GetList<FolderEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
       
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected FolderDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

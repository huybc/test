﻿using System;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.ViewCount;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.ViewCount
{
    public class ViewCountDalBase
    {
        private readonly CmsMainDb _db;

        protected ViewCountDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        public bool Insert(ViewCountEntity obj, ref int id)
        {
            const string commandText = "CMS_ViewCount_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectId", obj.ObjectId);
                _db.AddParameter(cmd, "ObjectType", obj.ObjectType);
                _db.AddParameter(cmd, "ViewCount", obj.ViewCount);
                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public bool Update(ViewCountEntity obj)
        {

            const string commandText = "CMS_ViewCount_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectId", obj.ObjectId);
                _db.AddParameter(cmd, "ObjectType", obj.ObjectType);
             Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ViewCountEntity GetByObjectIdAndObjectType(long objectId,int objectType)
        {
            const string commandText = "CMS_ViewCount_GetByObjectTypeAndObjectId";
            try
            {
                ViewCountEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                data = _db.Get<ViewCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
    }
}

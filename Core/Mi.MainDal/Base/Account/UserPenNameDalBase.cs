﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserPenNameDalBase
    {
        public bool EditPenName(UserPenNameEntity penName)
        {
            const string commandText = "CMS_UserPenName_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", penName.Id);
                _db.AddParameter(cmd, "UserName", penName.UserName);
                _db.AddParameter(cmd, "PenName", penName.PenName);
                _db.AddParameter(cmd, "VietId", penName.VietId);
                _db.AddParameter(cmd, "Email", penName.Email);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePenNameById(int id)
        {
            const string commandText = "CMS_UserPenName_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_UserPenName_Search";
            try
            {
                List<UserPenNameEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Email", email);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<UserPenNameEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        public UserPenNameEntity GetPenNameByVietId(long vietId)
        {
            const string commandText = "CMS_UserPenName_GetByVietID";
            try
            {
                UserPenNameEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VietId", vietId);
                data = _db.Get<UserPenNameEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly CmsMainDb _db;

        protected UserPenNameDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

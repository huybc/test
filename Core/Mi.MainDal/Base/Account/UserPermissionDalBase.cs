﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserPermissionDalBase
    {
        public bool UpdateUserPermission(UserPermissionEntity userPermission)
        {
            const string commandText = "CMS_UserPermision_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userPermission.UserId);
                _db.AddParameter(cmd, "PermissionId", userPermission.PermissionId);
                _db.AddParameter(cmd, "ZoneId", userPermission.ZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemoveUserPermission(UserPermissionEntity userPermission)
        {
            const string commandText = "CMS_UserPermision_Remove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userPermission.UserId);
                _db.AddParameter(cmd, "PermissionId", userPermission.PermissionId);
                _db.AddParameter(cmd, "ZoneId", userPermission.ZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemoveAllUserPermission(int userId)
        {
            const string commandText = "CMS_UserPermision_RemoveAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool CheckUserPermission(UserPermissionEntity userPermission)
        {
            const string commandText = "CMS_UserPermission_CheckUserPermissionZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userPermission.UserId);
                _db.AddParameter(cmd, "PermissionId", userPermission.PermissionId);
                _db.AddParameter(cmd, "ZoneId", userPermission.ZoneId);
                var reader = cmd.ExecuteReader();

                return reader.Read() && reader["ExistsPermission"].ToString() == "1";
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUserId(int userId, bool isGetChildZone)
        {
            const string commandText = "CMS_UserPermission_GetListByUserId";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "IsGetChildZone", isGetChildZone);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUserName(string username)
        {
            const string commandText = "CMS_UserPermission_GetListByUserName";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUserIdAndZoneId(int userId, int zoneId)
        {
            const string commandText = "CMS_UserPermission_GetListByUserIdAndZoneId";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUsernameAndZoneId(string username, int zoneId)
        {
            const string commandText = "CMS_UserPermission_GetListByUsernameAndZoneId";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "username", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUserIdAndPermissionId(int userId, int permissionId)
        {
            const string commandText = "CMS_UserPermission_GetListByUserIdAndPermissionId";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserPermissionEntity> GetListUserPermissionByUsernameAndPermissionId(string username, int permissionId)
        {
            const string commandText = "CMS_UserPermission_GetListByUsernameAndPermissionId";
            try
            {
                List<UserPermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "username", username);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<UserPermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected UserPermissionDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

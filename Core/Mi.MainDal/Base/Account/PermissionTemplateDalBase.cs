﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class PermissionTemplateDalBase
    {
        public List<PermissionTemplateEntity> GetAllPermissionTemplate()
        {
            const string commandText = "CMS_PermissionTemplate_GetAll";
            try
            {
                List<PermissionTemplateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PermissionTemplateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePermissionTemplate(PermissionTemplateEntity pm)
        {
            const string commandText = "CMS_PermissionTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", pm.Id);
                _db.AddParameter(cmd, "TemplateName", pm.TemplateName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePermissionTemplate(int templateId)
        {
            const string commandText = "CMS_PermissionTemplate_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAllPermissionTemplateDetail(int templateId)
        {
            const string commandText = "CMS_PermissionTemplateDetail_DeleteByTemplateId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PermissionTemplateDetailEntity> GetListPermissionTemplateDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            const string commandText = "CMS_PermissionTemplateDetail_GetListByPermissionTemplate";
            try
            {
                List<PermissionTemplateDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                _db.AddParameter(cmd, "IsGetChildZone", isGetChildZone);
                data = _db.GetList<PermissionTemplateDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PermissionTemplateDetailUpdate(PermissionTemplateDetailEntity permissionTemplateDetail)
        {
            const string commandText = "CMS_PermissionTemplateDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", permissionTemplateDetail.TemplateId);
                _db.AddParameter(cmd, "PermissionId", permissionTemplateDetail.PermissionId);
                _db.AddParameter(cmd, "ZoneId", permissionTemplateDetail.ZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly CmsMainDb _db;

        protected PermissionTemplateDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

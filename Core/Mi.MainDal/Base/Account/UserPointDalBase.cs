﻿using System;
using Mi.Common;
using Mi.Entity.Base.Point;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserPointDalBase
    {
        public bool EditPenName(PointEntity up)
        {
            const string commandText = "FE_UserPoint_Add";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", up.UserName);
                _db.AddParameter(cmd, "ZoneId", up.ZoneId);
                _db.AddParameter(cmd, "Point", up.Point);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetTotalPoint(string userName)
        {
            const string commandText = "FE_UserPoint_GetTotal";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                var numberOfRow = cmd.ExecuteNonQuery();
                int data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PointEntity FE_UserPoint_Get(string userName, int zoneId)
        {
            const string commandText = "FE_UserPoint_Get";
            try
            {
                PointEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.Get<PointEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected UserPointDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

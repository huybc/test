﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserDalBase
    {
        public bool AddnewUser(UserEntity user, ref int newUserId)
        {
            const string commandText = "CMS_User_Addnew";
            try
            {
                newUserId = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", user.UserName);
                _db.AddParameter(cmd, "Password", user.Password);
                _db.AddParameter(cmd, "FullName", user.FullName);
                _db.AddParameter(cmd, "Avatar", user.Avatar);
                _db.AddParameter(cmd, "Email", user.Email);
                _db.AddParameter(cmd, "Mobile", user.Mobile);
                _db.AddParameter(cmd, "IsFullPermission", user.IsFullPermission);
                _db.AddParameter(cmd, "IsFullZone", user.IsFullZone);
                _db.AddParameter(cmd, "Status", user.Status);
                _db.AddParameter(cmd, "Address", user.Address);
                _db.AddParameter(cmd, "BirthDay", user.Birthday);
                _db.AddParameter(cmd, "Description", user.Description);
                _db.AddParameter(cmd, "Skype", user.Skype);
                _db.AddParameter(cmd, "Website", user.Website);
                _db.AddParameter(cmd, "ActiveCode", user.ActiveCode);
                _db.AddParameter(cmd, "LoginType", user.LoginType);
                _db.AddParameter(cmd, "SocialId", user.SocialId);
                var numberOfRow = cmd.ExecuteNonQuery();

                newUserId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && newUserId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserById(UserEntity user)
        {
            const string commandText = "CMS_User_UpdateById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", user.Id);
                _db.AddParameter(cmd, "ZoneIdList", user.ZoneIdList);
                _db.AddParameter(cmd, "FullName", user.FullName);
                _db.AddParameter(cmd, "Avatar", user.Avatar);
                _db.AddParameter(cmd, "Email", user.Email);
                _db.AddParameter(cmd, "Mobile", user.Mobile);
                _db.AddParameter(cmd, "IsFullPermission", user.IsFullPermission);
                _db.AddParameter(cmd, "IsFullZone", user.IsFullZone);
                _db.AddParameter(cmd, "Status", user.Status);
                _db.AddParameter(cmd, "Address", user.Address);
                _db.AddParameter(cmd, "BirthDay", user.Birthday);
                _db.AddParameter(cmd, "Description", user.Description);
                _db.AddParameter(cmd, "Skype", user.Skype);
                _db.AddParameter(cmd, "Website", user.Website);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteUserById(int id)
        {
            const string commandText = "CMS_User_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserAvatar(string accountName, string avatar)
        {
            const string commandText = "CMS_User_UpdateAvatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "Avatar", avatar);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateUserStatus(string accountName, int stauts)
        {
            const string commandText = "CMS_User_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "Status", stauts);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateActiveCode(string accountName, string activeCode)
        {
            const string commandText = "CMS_User_UpdateActiveCode";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "ActiveCode", activeCode);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserStatusByById(int id, int status)
        {
            const string commandText = "CMS_User_UpdateStatusByUserId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateUserPasswordByById(int id, string password)
        {
            const string commandText = "CMS_User_UpdatePasswordByUserId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Password", password);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserByUserNameAndActiveCode(string userName, string activeCode)
        {
            const string commandText = "CMS_User_GetUserByUserNameAndActiveCode";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "ActiveCode", activeCode);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserById(int id)
        {
            const string commandText = "CMS_User_GetUserById";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserBySocialId(string socialId, byte type,string email)
        {
            const string commandText = "CMS_User_GetUserBySocialId";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SocialId", socialId);
                _db.AddParameter(cmd, "LoginType", type);
                _db.AddParameter(cmd, "Email", email);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserByUsername(string username)
        {
            const string commandText = "CMS_User_GetUserByUsername";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserByEmail(string email)
        {
            const string commandText = "CMS_User_GetUserByEmail";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Email", email);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserEntity> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_User_SeachUser";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                data = _db.GetList<UserEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<MemberEntity> MemberSearch(string keyword, string userName, int status, int timeFilter, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_User_MemberSearch";
            try
            {
                List<MemberEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "TimeFilter", timeFilter);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                data = _db.GetList<MemberEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            const string commandText = "CMS_User_GetUserByPermissionIdAndZoneList";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserEntity> GetUserByPermissionListAndZoneList(string permissionIds, string zoneIds)
        {
            const string commandText = "CMS_User_GetUserByPermissionListAndZoneList";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(int permissionId, int zoneId)
        {
            const string commandText = "CMS_User_GetNormalUserByPermissionIdAndZoneId";
            try
            {
                List<UserStandardEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<UserStandardEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region lấy danh sách user theo permissionId và ZoneId
        public List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            const string commandText = "CMS_User_GetListUserByPermissionAndZoneId";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public string GetOtpSecretKeyByUsername(string username)
        {
            const string commandText = "CMS_User_GetOtpSecretKeyByUsername";
            try
            {
                var data = "";
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                data = Utility.ConvertToString(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            const string commandText = "CMS_User_UpdateOtpSecretKeyForUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "OtpSecretKey", otpSecretKey);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountUser(int status)
        {
            //CMS_User_Count
            const string commandText = "CMS_User_Count";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected UserDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

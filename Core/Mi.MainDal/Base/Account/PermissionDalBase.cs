﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Security;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class PermissionDalBase
    {
        public List<PermissionEntity> GetListPermissionByGroupId(int groupPermissionId)
        {
            const string commandText = "CMS_Permission_GetListByGroupId";
            try
            {
                List<PermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupId", groupPermissionId);
                data = _db.GetList<PermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PermissionEntity> GetListPermissionByTemplateId(int permissionTemplateId)
        {
            const string commandText = "CMS_Permission_GetListByTemplateId";
            try
            {
                List<PermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", permissionTemplateId);
                data = _db.GetList<PermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PermissionEntity> GetListPermissionByUsername(string username, string filterInListPermissionId)
        {
            const string commandText = "CMS_Permission_GetListByUsername";
            try
            {
                List<PermissionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "FilterInListPemissionId", filterInListPermissionId);
                data = _db.GetList<PermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PermissionEntity GetPermissionById(int id)
        {
            const string commandText = "CMS_Permission_GetById";
            try
            {
                PermissionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<PermissionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;
        
        protected PermissionDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

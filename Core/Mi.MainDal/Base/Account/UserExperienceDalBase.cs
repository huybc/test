﻿using System;
using System.Collections.Generic;
using System.Data;
using Mi.Common;
using Mi.Entity.Base.Security;
using Mi.MainDal.Common;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Account
{
    public abstract class UserExperienceDalBase
    {

        #region Gets

        public UserExperienceEntity GetById(int id)
        {
            const string commandText = "CMS_UserExperiences_GetById";
            try
            {
                UserExperienceEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<UserExperienceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserExperienceEntity> GetAllExperienceByUserId(string userName, int sortOrder, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_UserExperiences_GetAllByUserId";
            try
            {
                List<UserExperienceEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                data = _db.GetList<UserExperienceEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion


        public void Insert(UserExperienceEntity obj, ref int id)
        {
            const string commandText = "CMS_UserExperiences_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", obj.UserName);
                _db.AddParameter(cmd, "ZoneId", obj.ZoneId);
                _db.AddParameter(cmd, "DateFrom", obj.DateFrom);
                _db.AddParameter(cmd, "DateTo", obj.DateTo);
                _db.AddParameter(cmd, "Description", obj.Description);
                cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Update(UserExperienceEntity obj)
        {
            const string commandText = "CMS_UserExperiences_Update";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "UserName", obj.UserName);
                _db.AddParameter(cmd, "ZoneId", obj.ZoneId);
                _db.AddParameter(cmd, "DateFrom", obj.DateFrom);
                if (obj.DateTo <= Constants.MinDateTime)
                {
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                }

                else
                {
                    _db.AddParameter(cmd, "DateTo", obj.DateTo);
                }

                _db.AddParameter(cmd, "Description", obj.Description);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_UserExperiences_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected UserExperienceDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using Mi.Common;
using Mi.Entity.Base.Point;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Point
{
    public abstract class PointDalBase
    {
        #region Gets

        public int GetPointByZone(string username, int zoneId)
        {
            const string commandText = "FE_UserPoint_Get";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PointEntity> GetTopUserByZone(int zoneId, int top,bool allowSelectExperience)
        {
            const string commandText = "FE_UserPoint_GetTopByZone";
            try
            {
                List<PointEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "AllowSelectExperience", allowSelectExperience);
                data = _db.GetList<PointEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetTotalPoint(string userName)
        {
            const string commandText = "FE_UserPoint_GetTotal";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets


        public void AddPoint(PointEntity point)
        {

            const string commandText = "FE_UserPoint_Add";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", point.UserName);
                _db.AddParameter(cmd, "ZoneId", point.ZoneId);
                _db.AddParameter(cmd, "Point", point.Point);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected PointDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

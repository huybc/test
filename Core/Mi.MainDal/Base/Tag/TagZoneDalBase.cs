﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Tag;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Tag
{
    public abstract class TagZoneDalBase
    {
        public List<TagZoneEntity> GetTagNewsByTagId(long tagId)
        {
            const string commandText = "CMS_TagZone_GetByTagId";
            try
            {
                List<TagZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                data = _db.GetList<TagZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TagZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

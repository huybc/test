﻿using System;
using System.Collections.Generic;
using Mi.Entity.Base.Tag;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.Tag
{
    public abstract class TagNewsDalBase
    {
        public List<TagNewsWithTagInfoEntity> GetTagNewsByNewsId(long newsId)
        {
            const string commandText = "CMS_TagNews_GetByNewsId";
            try
            {
                List<TagNewsWithTagInfoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsID", newsId);
                data = _db.GetList<TagNewsWithTagInfoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagNewsEntity> GetTagNewsWithTagInfoByListOfTagId(string tagIds)
        {
            const string commandText = "CMS_TagNews_GetByListOfTagId";
            try
            {
                List<TagNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagIds", tagIds);
                data = _db.GetList<TagNewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagNewsEntity> GetTagNewsByNewsIdAndMode(long newsId, Int16 tagMode)
        {
            const string commandText = "CMS_TagNews_GetByNewsIdAndTagMode";
            try
            {
                List<TagNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsID", newsId);
                _db.AddParameter(cmd, "TagMode", tagMode);
                data = _db.GetList<TagNewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        //public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsId(long newsId)
        //{
        //    
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId)
        //                             };
        //        reader = SqlHelper.ExecuteReader(Constants.GetConnectionString(Constants.Connection.CmsMainDb),
        //                                         CommandType.StoredProcedure,
        //                                         Constants.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsId",
        //                                         parameters);

        //        var tagNewss = new List<TagNewsWithTagInfoEntity>();
        //        while (reader.Read())
        //        {
        //            var tagNews = new TagNewsWithTagInfoEntity();
        //            EntityBase.SetObjectValue(reader, ref tagNews);
        //            tagNewss.Add(tagNews);
        //        }
        //        return tagNewss;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format(Constants.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsId:{0}", ex.Message));
        //    }
        //    finally
        //    {
        //        if (null != reader && !reader.IsClosed) reader.Close();
        //    }
        //}
        //public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsIdAndMode(long newsId, Int16 tagMode)
        //{
        //    
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId),
        //                                 new SqlParameter("@TagMode", tagMode)
        //                             };
        //        reader = SqlHelper.ExecuteReader(Constants.GetConnectionString(Constants.Connection.CmsMainDb),
        //                                         CommandType.StoredProcedure,
        //                                         Constants.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsIdAndTagMode",
        //                                         parameters);

        //        var tagNewss = new List<TagNewsWithTagInfoEntity>();
        //        while (reader.Read())
        //        {
        //            var tagNews = new TagNewsWithTagInfoEntity();
        //            EntityBase.SetObjectValue(reader, ref tagNews);
        //            tagNewss.Add(tagNews);
        //        }
        //        return tagNewss;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format(Constants.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsIdAndTagMode:{0}", ex.Message));
        //    }
        //    finally
        //    {
        //        if (null != reader && !reader.IsClosed) reader.Close();
        //    }
        //}
        public bool UpdateNews(long tagId, string deleteNewsId, string addNewsId, int tagMode = 1)
        {
            const string commandText = "CMS_Tag_UpdateNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "DeleteNewsIds", deleteNewsId);
                _db.AddParameter(cmd, "AddNewsIds", addNewsId);
                _db.AddParameter(cmd, "TagMode", tagMode);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TagNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}

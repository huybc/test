﻿using System;
using Mi.Entity.Base.UserAction;
using Mi.MainDal.Databases;

namespace Mi.MainDal.Base.UserAction
{
    public abstract class UserActionDalBase
    {
        #region Gets

        public UserActionEntity GetUserActionDetail(string username, long objectId, int objectType, int actionType)
        {
            const string commandText = "FE_UserAction_Get";
            try
            {
                UserActionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CreatedBy", username);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ActionType", actionType);
                data = _db.Get<UserActionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets


        public void AddUserAction(UserActionEntity ac)
        {

            const string commandText = "FE_UserAction_Add";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", ac.UserName);
                _db.AddParameter(cmd, "ObjectId", ac.ObjectId);
                _db.AddParameter(cmd, "ObjectType", ac.ObjectType);
                _db.AddParameter(cmd, "ActionType", ac.ActionType);
                _db.AddParameter(cmd, "ActionValue", ac.ActionValue);
                _db.AddParameter(cmd, "CreatedBy", ac.CreatedBy);
                _db.AddParameter(cmd, "Point", ac.Point);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected UserActionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}

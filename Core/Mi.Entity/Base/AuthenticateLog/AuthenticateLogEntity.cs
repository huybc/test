﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Mi.Common;

namespace Mi.Entity.Base.AuthenticateLog
{
    [DataContract]
    public class AuthenticateLogEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string MachineName { get; set; }
        [DataMember]
        public string IpAddress { get; set; }
        [DataMember]
        public string UserAgent { get; set; }
        [DataMember]
        public string UrlReferrer { get; set; }
        [DataMember]
        public DateTime LogDate { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Os { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool Status { set; get; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace Mi.Entity.Base.Debt
{
    public class DebtEntity
    {
        [DataMember]
        public int DebtID { get; set; }
        [DataMember]
        public string CompanyId { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string KyTinhThue { get; set; }
        [DataMember]
        public string KhoanThue { get; set; }
        [DataMember]
        public int TieuMuc { get; set; }
        [DataMember]
        //public long SoTienNopGoc { get; set; }
        //[DataMember]
        //public DateTime HanNopNoGoc { get; set; }
        //[DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime CreatedAt { get; set; }
        [DataMember]
        public DateTime LastModifiedAt { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
    }
    public class TransEntity
    {

        [DataMember]
        public int DebtTransactionId { get; set; }
        [DataMember]
        public int DebtId { get; set; }
        [DataMember]
        public DateTime HanNopNoGoc { get; set; }
        [DataMember]
        public long TienNoGoc { get; set; }
        [DataMember]
        public DateTime NgayChamNop1 { get; set; }
        [DataMember]
        public DateTime NgayChamNop2 { get; set; }
        [DataMember]
        public double TyLe { get; set; }
        [DataMember]
        public long SoTienChamNop { get; set; }  [DataMember]
        public long SoTienNop { get; set; }
        [DataMember]
        public DateTime NgayNop { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }
        [DataMember]
        public DateTime LastModifiedAt { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
    }
}

﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Security
{
    [DataContract]
    public class PermissionTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TemplateName { get; set; }
    }
}

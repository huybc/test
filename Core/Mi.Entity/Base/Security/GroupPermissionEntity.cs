﻿using System.Runtime.Serialization;
using Mi.Common;
using System.Collections.Generic;

namespace Mi.Entity.Base.Security
{
    [DataContract]
    public enum GroupPermission
    {
        /// <summary>
        /// Quản lý bài viết
        /// </summary>
        [EnumMember]
        Article = 1,
        /// <summary>
        /// Quản lý bình luân
        /// </summary>
        [EnumMember]
        Comment = 2,
        /// <summary>
        /// Quản lý chung
        /// </summary>
        [EnumMember]
        Admin = 4,
        /// <summary>
        /// Cài bài nổi bật
        /// </summary>
        [EnumMember]
        NewsPositionManager =8,
        /// <summary>
        /// Quản lý media
        /// </summary>
        [EnumMember]
        MediaManager = 10,
        /// <summary>
        /// Quản lý box nhúng
        /// </summary>
        [EnumMember]
        EmbedBoxManager = 11,
        /// <summary>
        /// Quản lý Tag
        /// </summary>
        [EnumMember]
        TagManager = 12,
        /// <summary>
        /// Chấm nhuận bút
        /// </summary>
        [EnumMember]
        RoyaltiesManager = 13,
        /// <summary>
        /// Thảo luận
        /// </summary>
        [EnumMember]
        DiscussionManager = 14,
        /// <summary>
        /// Quản lý dạng bài đặc biệt
        /// </summary>
        [EnumMember]
        SpecialArticleManager = 15
    }

    [DataContract]
    public class GroupPermissionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class GroupPermissionDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<PermissionEntity> PermissionList { get; set; }
    }
}

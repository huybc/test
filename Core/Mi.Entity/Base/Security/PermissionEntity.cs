﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Security
{
    [DataContract]
    public enum EnumPermission
    {
        [EnumMember]
        ArticleReporter = 1, // Phóng viên
        [EnumMember]
        ArticleEditor = 2, // Biên tập viên
        [EnumMember]
        ArticleAdmin = 3, // Thư ký
        /*####################################*/
        [EnumMember]
        AccountManager = 11, // Quản lý User
        [EnumMember]
        AccountPermissionManager = 12, // Quản lý phân quyền User
        [EnumMember]
        DeleteAccountManager = 13, // Xóa User
        [EnumMember]
        Statistic = 15, // Thống kê
        /*####################################*/       
        [EnumMember]
        PhotoManager = 20, // Quản lý ảnh
        [EnumMember]
        VideoManager = 21, // Quản lý video
        [EnumMember]
        ApproveVideo = 32, // Duyệt video
        /*####################################*/
        [EnumMember]
        EmbedView = 22, // Xem box nhúng
        [EnumMember]
        EmbedSetup = 23, // Cài bài box nhúng
        [EnumMember]
        EmbedApprover = 30, // Duyệt box nhúng ngoài trang
        /*####################################*/
        [EnumMember]
        SetupMetaTag = 24, // Quản lý MetaTag
        [EnumMember]
        TagManager = 25, // Quản lý Tag
        /*####################################*/  
        [EnumMember]
        ApproveComment = 31, // Duyệt comment
        [EnumMember]
        ReceiveComment = 39, // Duyệt comment
        /*####################################*/
        [EnumMember]
        CreateTopicDiscussion = 33, // Create topic discussion
        [EnumMember]
        CommentOnlyOnDiscussion = 34, // Comment only on discussion        
        /*####################################*/
        [EnumMember]
        AdvertismentManager = 40, // Advertisment manager              
        [EnumMember]
        CommentVideoEditor = 46, // Nhận comment video
        [EnumMember]
        CommentVideoManager = 47, // Duyệt comment video
    }

    [DataContract]
    public class PermissionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsGrantByCategory { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace Mi.Entity.Base.Security
{
  public  class UserExperienceEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime DateFrom { get; set; }
        [DataMember]
        public DateTime DateTo { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
    [DataContract]
    public enum UserSortExperience
    {
        [EnumMember]
        DateFormDesc = 0,
        [EnumMember]
        DateToAsc = 1
        
    }
}

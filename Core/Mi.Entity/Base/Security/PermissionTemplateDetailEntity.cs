﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;
using Mi.Entity.Base.Zone;

namespace Mi.Entity.Base.Security
{
    [DataContract]
    public class PermissionTemplateDetailEntity : EntityBase
    {
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int PermissionId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
    [DataContract]
    public class PermissionTemplateWithPermissionDetailEntity : EntityBase
    {      
        [DataMember]
        public List<GroupPermissionDetailEntity> AllGroupPermission { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllParentZone { get; set; }
        [DataMember]
        public List<PermissionTemplateDetailEntity> PermissionTemplateDetailList { get; set; }
    }
}

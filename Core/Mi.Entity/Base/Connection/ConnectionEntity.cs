﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Connection
{
    [DataContract]
    public class ConnectionEntity : EntityBase
    {
        [DataMember]
        public string ConnectionId { get; set; }
        [DataMember]
        public string UserAgent { get; set; }      
        [DataMember]
        public bool Connected { get; set; }
        [DataMember]
        public string UserName { get; set; }
    }
}

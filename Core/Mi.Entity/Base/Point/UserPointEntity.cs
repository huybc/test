﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Point
{
    [DataContract]
    public class PointEntity : EntityBase
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Point { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int SCount { get; set; }
        [DataMember]
        public string ListExperienceId { get; set; }
    }
}

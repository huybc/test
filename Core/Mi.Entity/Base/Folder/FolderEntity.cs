﻿using System;
using System.Runtime.Serialization;

namespace Mi.Entity.Base.Folder
{
    [DataContract]
    public class FolderEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public string FolderPath { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ModifyBy{ get; set; }
    }
}

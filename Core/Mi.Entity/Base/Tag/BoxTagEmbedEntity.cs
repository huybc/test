﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Mi.Common;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public class BoxTagEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public long TagId { get; set; }//tagid

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }
    }
}

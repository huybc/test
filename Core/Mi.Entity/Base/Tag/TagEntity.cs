﻿using System;
using System.Runtime.Serialization;
using Mi.Common;
using System.Collections.Generic;

namespace Mi.Entity.Base.Tag
{
    [DataContract]
    public enum EnumTagType : int
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        ForNews = 0,
        [EnumMember]
        ForImage = 1,
        [EnumMember]
        ForVideo = 2,
        [EnumMember]
        TagAuto = 3,
        [EnumMember]
        Term = 4,
        [EnumMember]
        ForProduct = 5
    }
    [DataContract]
    public enum EnumSearchTagOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        NewsCountDesc = 1,
        [EnumMember]
        NewsCountAsc = 2,
        [EnumMember]
        TagHot = 3
    }
    [DataContract]
    public class TagEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public bool IsThread { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public string TagContent { get; set; }
        [DataMember]
        public string TagTitle { get; set; }
        [DataMember]
        public string TagInit { get; set; }
        [DataMember]
        public string TagMetaKeyword { get; set; }
        [DataMember]
        public string TagMetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public int NewsCountDay { get; set; }
        [DataMember]
        public int NewsCountWeek { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }
    [DataContract]
    public class TagEntityDetail : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public bool IsThread { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public string TagContent { get; set; }
        [DataMember]
        public string TagTitle { get; set; }
        [DataMember]
        public string TagInit { get; set; }
        [DataMember]
        public string TagMetaKeyword { get; set; }
        [DataMember]
        public string TagMetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public List<TagZoneEntity> TagZone { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
    }

    [DataContract]
    public class TagWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
    }
}

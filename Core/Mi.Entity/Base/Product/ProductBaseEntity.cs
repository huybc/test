﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Product
{

    [DataContract]
    public class ProductBaseEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
    }


    [DataContract]
    public enum EnumProductBaseStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Published = 1,
        [EnumMember]
        UnPublished = 2
    }

    [DataContract]
    public enum EnumProductBaseType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Tool = 1,
        [EnumMember]
        Service = 2
    }
    [DataContract]
    public enum EnumProductBaseSortOrder
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        TitleDesc = 4,
        [EnumMember]
        TitleAsc = 5,
        [EnumMember]
        ViewCountDesc = 6,
        [EnumMember]
        ViewCountAsc = 7
    }
}

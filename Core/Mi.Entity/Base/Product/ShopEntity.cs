﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Product
{

    [DataContract]
    public class ShopEntity : EntityBase
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public byte Status { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public long RowNum { get; set; }
        [DataMember]
        public string PrimaryName { get; set; }
        [DataMember]
        public string DistrictCode { get; set; }
    }
    [DataContract]
    public class ShopSimpleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public byte Type { get; set; }

    }
    public class LocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public double distance { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }


    [DataContract]
    public enum EnumShopStatus
    {
        [EnumMember]
        AllWithMember = -2,
        [EnumMember]
        All = -1,
        [EnumMember]
        Published = 1,
        [EnumMember]
        UnPublished = 2,
        [EnumMember]
        MoveToTrash = 3
    }
    [DataContract]
    public enum EnumShopOrder
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        NameDesc = 2,
        [EnumMember]
        NameAsc = 3,


    }
    public enum EnumShopLocationOrder
    {
        [EnumMember]
        LocationDesc = 0,
        [EnumMember]
        LocationAsc = 1,



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Mi.Common;

namespace Mi.Entity.Base.Product
{
    public class ProductInShopEntity
    {
        [DataMember]
        public long ProductId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(ProductId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
}

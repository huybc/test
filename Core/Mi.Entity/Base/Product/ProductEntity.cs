﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Tag;
using Mi.Entity.Base.Zone;

namespace Mi.Entity.Base.Product
{

    [DataContract]
    public class ProductEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TitleSeo { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarArray { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public double DiscountPrice { get; set; }
        //[DataMember]
        //public int ProductBaseId { get; set; }
        [DataMember]
        public string PropertyIdList { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public int Percent { get; set; }
        [DataMember]
        public byte OriginType { get; set; }
        [DataMember]
        public byte Usage { get; set; }
        [DataMember]
        public string PromotionInfo { get; set; }
        [DataMember]
        public string BackLink { get; set; }
        [DataMember]
        public string Tips { get; set; }
        [DataMember]
        public string Unit { get; set; }
        [DataMember]
        public bool VAT { get; set; }
        [DataMember]
        public bool SalesOff { get; set; }

        [DataMember]
        public int ManufacturerId { get; set; }
        [DataMember]
        public string Manufacturer { get; set; }
        [DataMember]
        public int PropertyId { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string Warranty { get; set; }
        [DataMember]
        public string PCode { get; set; }
        [DataMember]
        public bool SellOut { get; set; }
        [DataMember]
        public long? RowNum { get; set; }
        [DataMember]
        public string Catalog { get; set; }
    }
    [DataContract]
    public class ProductNearLocationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TitleSeo { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public double DiscountPrice { get; set; }
        [DataMember]
        public int ProductBaseId { get; set; }
        [DataMember]
        public string ShopIdList { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public string Catalog { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Distance { get; set; }
    }

    [DataContract]
    public class ProductDetailForEditEntity : EntityBase
    {
        [DataMember]
        public ProductEntity ProductInfo { get; set; }
        public List<ZoneWithSimpleFieldEntity> AllZone { get; set; }
        [DataMember]
        public List<ProductInZoneEntity> ProductInZone { get; set; }
        [DataMember]
        public List<ShopInProductEntity> ShopInProduct { get; set; }
        [DataMember]
        public List<TagProductWithTagInfoEntity> TagInProduct { get; set; }
    }
    [DataContract]
    public class ShopInProductEntity : EntityBase
    {
        [DataMember]
        public long ProductId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(ProductId); } set { } }
        [DataMember]
        public int ShopId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
    [DataContract]
    public class ProductInNewsWithProductInfoEntity : EntityBase
    {
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        //public long NewsId { get; set; }
        //[DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public double DiscountPrice { get; set; }
    }
    [DataContract]
    public class ProductEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public bool SalesOff { get; set; }
        [DataMember]
        public int Percent { get; set; }
        [DataMember]
        public double DiscountPrice { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string Url { get; set; }

    }
    [DataContract]
    public class CatalogEntity : EntityBase
    {
        [DataMember]
        public string ext { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string name { get; set; }
       

    }
    [DataContract]
    public enum EnumProductStatus
    {
        [EnumMember]
        AllWithMember = -2,
        [EnumMember]
        All = -1,
        [EnumMember]
        Published = 1,
        [EnumMember]
        UnPublished = 2,
        [EnumMember]
        MoveToTrash = 3,
        [EnumMember]
        Temp = 4
    }

    [DataContract]
    public enum EnumProductType
    {

        [EnumMember]
        AllWithAdmin = -2,
        [EnumMember]
        All = -1,
        [EnumMember]
        Product = 1,
        [EnumMember]
        Service = 2,
        [EnumMember]
        Temp = 10
    }
    [DataContract]
    public enum EnumProductInNewsType
    {
        [EnumMember]
        Tool = 1,
        [EnumMember]
        Replacement = 2,
        [EnumMember]
        Service = 3
    }
    [DataContract]
    public enum EnumProductSortOrder
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        TitleDesc = 4,
        [EnumMember]
        TitleAsc = 5,
        [EnumMember]
        ViewCountDesc = 6,
        [EnumMember]
        ViewCountAsc = 7,
        [EnumMember]
        ModifyDateDesc = 8,
        [EnumMember]
        DiscountNewDesc = 9,
        [EnumMember]
        DiscountNewAsc = 10,
        [EnumMember]
        Random = 11

    }

    [DataContract]
    public enum EnumObjectEmbedInZoneType
    {
        [EnumMember]
        Product = 1,
        [EnumMember]
        News = 2,
        [EnumMember]
        ProductInProduct = 5,
        [EnumMember]
        ProductInNews = 6,
        [EnumMember]
        NewsEmbed = 7


    }
}

﻿using Mi.Common;

namespace Mi.Entity.Base.Province
{
    public class ProvinceEntity : EntityBase

    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}

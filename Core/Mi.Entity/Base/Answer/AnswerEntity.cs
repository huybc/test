﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Answer
{
    [DataContract]
    public class AnswerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int VoteCount { get; set; }
        [DataMember]
        public bool IsCorrect { get; set; }
    }


    [DataContract]
    public enum EnumAnswerStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Published = 1,
        [EnumMember]
        UnPublished = 2
    }

    [DataContract]
    public enum EnumAnswerObjectType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Solution = 1,
        [EnumMember]
        Question = 2,
        [EnumMember]
        Answer = 3
    }

    [DataContract]
    public enum EnumAnswerVoteDirection
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Up = 1,
        [EnumMember]
        Down = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsSourceEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]       
        public string Name { get; set; }
        [DataMember]
        public int SourceType { get; set; }        
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
    }

    [DataContract]
    public enum SourceType
    {
        [EnumMember]
        Default = 1
    }
}

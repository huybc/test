﻿using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class ConfigGroupEntity : EntityBase
    {
        [DataMember]
        public string ConfigGroupKey { get; set; }
        [DataMember]
        public string ConfigGroupName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    /// <summary>
    /// No nhu 1 truong trong DB
    /// </summary>
    [DataContract]
    public enum EnumNewsExtensionType
    {
        [EnumMember]
        HardLevel = 1            
    }
    [DataContract]
    public class NewsExtensionEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public int NumericValue { get; set; }
    }
}

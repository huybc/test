﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class ConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ConfigName { get; set; }
        [DataMember]
        public string ConfigGroupKey { get; set; }
        [DataMember]
        public string ConfigLabel { get; set; }
        [DataMember]
        public string ConfigValue { get; set; }
        [DataMember]
        public int ConfigValueType { get; set; }
        [DataMember]
        public string ConfigInitValue { get; set; }
        [DataMember]
        public bool IsEnable { get; set; }
    }
    [DataContract]
    public class AdvEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Thumb { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public int Position { get; set; }

        [DataMember]
        public bool IsEnable { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
    }
    [DataContract]
    public enum EnumStaticHtmlTemplateType
    {
        [EnumMember]
        AllType = 0,
        [EnumMember]
        Normal = 1,
        [EnumMember]
        HotNews = 2,
        [EnumMember]
        GroupNewsBox = 3,
        [EnumMember]
        Images = 4,
        [EnumMember]
        Code = 5,
        [EnumMember]
        Text = 6,
        [EnumMember]
        Content = 7
    }

    [DataContract]
    public enum EnumSlide
    {

        [EnumMember]
        SliderHomePage = 1,
        [EnumMember]
        Partner = 2,
        [EnumMember]
        ChooseUs = 3,
        [EnumMember]
        Support = 4,
        [EnumMember]
        Menu = 5,
        [EnumMember]
        SideBarTop = 6,
        [EnumMember]
        SideBottom = 7

    }

}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Mi.Common;
using Mi.Entity.Base.Product;
using Mi.Entity.Base.Tag;
using Mi.Entity.Base.Zone;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public enum EnumNewsIsAnswered
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        True = 1,
        [EnumMember]
        False = 2
    }
    [DataContract]
    public enum EnumNewsVoteDirection
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Up = 1,
        [EnumMember]
        Down = 2
    }
    [DataContract]
    public enum NewsActionType
    {
        [EnumMember]
        AllAction = -1,
        [EnumMember]
        Addnew = 0,
        [EnumMember]
        Update = 1,
        [EnumMember]
        Delete = 2,
        [EnumMember]
        SendToEdit = 3,
        [EnumMember]
        SendToPublish = 4,
        [EnumMember]
        ReturnEditor = 5,
        [EnumMember]
        ReturnReporter = 6,
        [EnumMember]
        ReceiveToEdit = 7,
        [EnumMember]
        ReceiveToPublish = 8,
        [EnumMember]
        Publish = 9,
        [EnumMember]
        UnPublish = 10
    }

    [DataContract]
    public enum NewsStatus
    {
        /// <summary>
        /// Không xác định
        /// </summary>
        [EnumMember]
        Unknow = -1,
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = 0,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 1,
        /// <summary>
        /// Đợi xuất bản
        /// </summary>
        [EnumMember]
        WaitForPublish = 2,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Published = 3,
        /// <summary>
        /// Xóa tạm
        /// </summary>
        [EnumMember]
        MovedToTrash = 4,
        /// <summary>
        /// Gỡ xuất bản
        /// </summary>
        [EnumMember]
        Unpublished = 5,
        [EnumMember]
        Reject = 6
    }

    [DataContract]
    public enum NewsType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Tutorial = 1,
        [EnumMember]
        Question = 2,
        [EnumMember]
        News = 3,
        [EnumMember]
        Event = 4
    }

    [DataContract]
    public enum NewsSortExpression
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        DistributionDateDesc = 2,
        [EnumMember]
        DistributionDateAsc = 3,
        [EnumMember]
        TitleDesc = 4,
        [EnumMember]
        TitleAsc = 5,
        [EnumMember]
        ViewCountDesc = 6,
        [EnumMember]
        ViewCountAsc = 7,
        [EnumMember]
        LastModifiedDateDesc = 8,
        [EnumMember]
        LastModifiedDateAsc = 9
    }

    [DataContract]
    public enum NewsFilterFieldForUsername
    {
        [EnumMember]
        CreatedBy = 0,
        [EnumMember]
        LastModifiedBy = 1,
        [EnumMember]
        PublishedBy = 2,
        [EnumMember]
        EditedBy = 3,
        [EnumMember]
        LastReceiver = 4
    }

    [DataContract]
    public class NewsEntity : EntityBase
    {
        public NewsEntity()
        {
            CreatedDate = DateTime.Now;
            LastModifiedDate = DateTime.Now;
            Status = 0;
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string StrId
        {
            get
            {
                return Id.ToString();
            }
            set { }
        }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TitleSeo { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int LikeCount { get; set; }
        [DataMember]
        public int VoteCount { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
        [DataMember]
        public int AnswerCount { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string SourceURL { get; set; }

        [DataMember]
        public bool IsAllowComment { get; set; }
        [DataMember]
        public bool IsVideo { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool IsLandingPage { get; set; }
        [DataMember]
        public string Css { get; set; }
        [DataMember]
        public string Script { get; set; }
        [DataMember]
        public DateTime EventStart { get; set; }
        [DataMember]
        public DateTime EventStop { get; set; }
        [DataMember]
        public int HoursOfWork { get; set; }
        [DataMember]
        public string Invest { get; set; }
        [DataMember]
        public int SurfaceArea { get; set; }
        [DataMember]
        public string Budget { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string ShortURL { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public string Avatar2 { get; set; }
    }

    [DataContract]
    public class NewsDetailForEditEntity : EntityBase
    {
        [DataMember]
        public NewsEntity NewsInfo { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllZone { get; set; }
        [DataMember]
        public List<NewsInZoneEntity> NewsInZone { get; set; }
        [DataMember]
        public List<TagNewsWithTagInfoEntity> TagInNews { get; set; }

        //[DataMember]
        //public List<ProductInNewsWithProductInfoEntity> ProductToolInNews { get; set; }

        //[DataMember]
        //public List<ProductInNewsWithProductInfoEntity> ProductRepInNews { get; set; }

        //[DataMember]
        //public List<ProductInNewsWithProductInfoEntity> ProductServiceInNews { get; set; }

        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelation { get; set; }
        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelationSpecial { get; set; }
        [DataMember]
        public List<NewsVersionWithSimpleFieldsEntity> ListVersion { get; set; }
        [DataMember]
        public List<NewsExtensionEntity> NewsExtensions { get; set; }
        [DataMember]
        public string NewsContentWithTemplate { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public string ExpertQuote { get; set; }
    }

    [DataContract]
    public class NewsInListEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TitleSeo { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public long ActiveUsers { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int LikeCount { get; set; }
        [DataMember]
        public int VoteCount { get; set; }

        [DataMember]

        public int CommentCount { get; set; }

        // Temp for is Validate Auto, GameK, GenK
        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public decimal BonusPrice { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int IsAnswered { get; set; }
        [DataMember]
        public int AnswerCount { get; set; }
        [DataMember]
        public bool IsAllowComment { get; set; }
        [DataMember]
        public bool IsVideo { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public int GroupId { get; set; }
    }

    [DataContract]
    public class NewsForExportEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Comments { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public decimal BonusPrice { get; set; }
    }

    [DataContract]
    public class NewsForOtherAPIEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string ZoneShortUrl { get; set; }
        [DataMember]
        public string Url { get; set; }
    }

    [DataContract]
    public class NewsInListWithNotifyEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public bool IsNotify { get; set; }
        [DataMember]
        public bool IsWarning { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public string LabelName { get; set; }
        [DataMember]
        public string LabelColor { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
    }

    [DataContract]
    public class NewsForValidateEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
    }

    public class NewsInfoForCachedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int PrimaryZoneId { get; set; }
        [DataMember]
        public int PrimaryZoneParentId { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public string PrimaryZoneUrl { get; set; }
        [DataMember]
        public string PrimaryZoneParentUrl { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class NewsCounterEntity : EntityBase
    {
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class NewsForRoyaltiesEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string StrId
        {
            get
            {
                return Id.ToString();
            }
            set { }
        }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int LikeCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string NewsRoyaltiesType { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public decimal BonusPrice { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public int PictureType { get; set; }
        [DataMember]
        public string QualityFactor { get; set; }
        [DataMember]
        public string ContributionFactor { get; set; }
        [DataMember]
        public string RiskFactor { get; set; }
        [DataMember]
        public int NumberOfPicture { get; set; }
        [DataMember]
        public string PenName { get; set; }
        [DataMember]
        public string Rate { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string PictureTypeName { get; set; }
        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public string PrPrice { get; set; }
        [DataMember]
        public string TotalAmount { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
        [DataMember]
        public string UserPicture { get; set; }
    }

    public class NewsForSuggestionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
    }

    public class NewsWithAnalyticEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int ViewCountHourly { get; set; }
        [DataMember]
        public int ViewCountDaily { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int ActiveUser { get; set; }
    }

    public class NewsWithDistributionDateEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        public DateTime DistributionDate { get; set; }
    }
}

﻿using System;
using Mi.Common;
using System.Runtime.Serialization;


namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsEmbedBoxOnPageEntity : EntityBase
    {
        [DataMember]
        public long ZoneId { get; set; }

        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }
    }

    [DataContract]
    public class NewsEmbedBoxOnPageListEntity : EntityBase
    {

        [DataMember]
        public long NewsId { get; set; } [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public double DiscountPrice { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Percent { get; set; }
        [DataMember]
        public int PropertyId { get; set; }
        [DataMember]
        public int GroupId { get; set; } [DataMember]
        public bool SalesOff { get; set; }

    }

    public class NewsEmbedBoxEntity : EntityBase
    {

        [DataMember]
        public long Id { get; set; }

        //[DataMember]
        //public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }

        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
        [DataMember]
        public int LikeCount { get; set; }
        [DataMember]
        public int VoteCount { get; set; }
        [DataMember]
        public int AnswerCount { get; set; }
        [DataMember]
        public int Type { get; set; }


    }
}


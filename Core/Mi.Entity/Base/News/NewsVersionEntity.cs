﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.News
{
    [DataContract]
    public class NewsVersionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public DateTime CreatedDateVersion { get; set; }
        [DataMember]
        public bool IsEditing { get; set; }

        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string TagItem { get; set; }

        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
    }
    [DataContract]
    public class NewsVersionWithSimpleFieldsEntity :EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public DateTime CreatedDateVersion { get; set; }
        [DataMember]
        public bool IsEditing { get; set; }

        [DataMember]
        public long NewsId { get; set; }
    }
    [DataContract]
    public class NewsVersionWithNewsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public DateTime CreatedDateVersion { get; set; }
        [DataMember]
        public bool IsEditing { get; set; }

        [DataMember]
        public NewsEntity News { get; set; }
    }
}

﻿namespace Mi.Entity.Base.ProjectDetail
{
   public class ProjectDetailEntity
    {
        public int Id { get; set; }
        public long ArticleId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
    }
}

﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Customer
{
    [DataContract]
    public class CustomerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]

        public string FullName { get; set; }
        [DataMember]

        public string Email { get; set; }
        [DataMember]

        public string Mobile { get; set; }
        [DataMember]

        public string Type { get; set; }
        [DataMember]

        public string Firm { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Contactperson { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string Address { get; set; } [DataMember]
        public DateTime CreatedDate { get; set; }
      
    }
}

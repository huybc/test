﻿using System;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.Base.Zone
{
    [DataContract]
    public class ZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Background { get; set; }
        [DataMember]
        public string Banner { get; set; }
        [DataMember]
        public string BannerLink { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public bool IsDisplayHomePage { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public int HoursOfWork { get; set; }
        [DataMember]
        public DateTime ConstructionDate { get; set; }
        [DataMember]
        public int SurfaceArea { get; set; }
        [DataMember]
        public long Budget { get; set; }
        [DataMember]
        public bool IsShowHomePage { get; set; }
    }

    [DataContract]
    public class ZoneAndProductCountEntity : ZoneBasicEntity
    {
        [DataMember]
        public int ProductCount { get; set; }
    }
    [DataContract]
    public class ZoneWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
    }
    [DataContract]
    public class ZoneSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public string Banner { get; set; }
        [DataMember]
        public string BannerLink { get; set; }
        [DataMember]
        public int PageSize { get; set; }
    }

    public class ZoneBasicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }

    }
    public class ZoneBasicSettingEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public bool IsDisplayHomePage { get; set; }

    }
    [DataContract]
    public enum ZoneType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        News = 1,
        [EnumMember]
        Recruitment = 2, // Tuyển dụng
        [EnumMember]
        RBLand = 3,
        [EnumMember]
        Project = 4,
        [EnumMember]
        Properties = 5,
        [EnumMember]
        Manufacturer = 6,
        [EnumMember]
        Shareholders = 7,// Cổ đông
        [EnumMember]
        Advisory = 9, // Tư vấn
        
        [EnumMember]
        Cooperative = 11, // Hợp tác
        [EnumMember]
        Invest = 12, // Đầu tư
        [EnumMember]
        Product = 13, // Sản phẩm
        [EnumMember]
        Type = 14,// Đầu tư
        [EnumMember]
        Group = 15,// Nhóm,
        //[EnumMember]
        //Color = 16, // Đầu tư
        [EnumMember]
        Service = 101,//sẻvice

        [EnumMember]
        Coach = 102,
        [EnumMember]
        Blog = 103,
        [EnumMember]
        ServiceNews = 105,
        [EnumMember]
        Schedule = 106, // lịch học

        //Hoang Long
        [EnumMember]
        GioiThieu = 200,
        [EnumMember]
        Color = 201,
        [EnumMember]
        N_News = 202,
        [EnumMember]
        Faq = 203,
        [EnumMember]
        Customer = 204,
        [EnumMember]
        About = 205,
        [EnumMember]
        BacSi = 206,
        [EnumMember]
        Media = 207,







    }
    [DataContract]
    public enum ZoneStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Publish = 1,
        [EnumMember]
        UnPublish = 2,
        [EnumMember]
        Temp = 3,
        [EnumMember]
        Delete = 4


    }
    [DataContract]
    public enum ZoneSortExpression
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        DistributionDateDesc = 2,
        [EnumMember]
        DistributionDateAsc = 3,

        [EnumMember]
        TitleDesc = 4,
        [EnumMember]
        TitleAsc = 5,

    }

    [DataContract]
    public enum ZoneFilterFieldForUsername
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        CreatedBy = 0,
        [EnumMember]
        ModifiedBy = 1

    }

}

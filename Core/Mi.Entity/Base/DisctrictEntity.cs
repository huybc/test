﻿using Mi.Common;

namespace Mi.Entity.Base
{
    public class DisctrictEntity : EntityBase

    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string ProvinceCode { get; set; }
    }
}

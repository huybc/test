﻿using System.Collections;
using System.Runtime.Serialization;
using Mi.Common;

namespace Mi.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            #region User error
            InnerHashtable[ErrorCodes.UpdateAccountInvalidUsername] = "Tên đăng nhập không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidPassword] = "Mật khẩu không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidRetypePassword] = "Mật khẩu nhập lại không chính xác";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidEmail] = "Địa chỉ Email không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidMobile] = "Số điện thoại di động không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountUsernameExists] = "Tên đăng nhập đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateAccountEmailExists] = "Địa chỉ email đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateAccountUserNotFound] = "Không tìm thấy thông tin tài khoản";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidOldPassword] = "Mật khẩu cũ không chính xác";
            InnerHashtable[ErrorCodes.UpdateAccountPermissionNotFound] = "Không tìm thấy thông tin quyền";
            InnerHashtable[ErrorCodes.UpdateAccountZoneNotFound] = "Khôn tìm thấy thông tin chuyên mục";
            InnerHashtable[ErrorCodes.UpdateAccountUserHasBeenLocked] = "Tài khoản đang bị khóa";
            InnerHashtable[ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch] = "Mật khẩu cũ và mật khẩu mới không giống nhau";
            InnerHashtable[ErrorCodes.UpdateAccountCantNotEditSystem] = "Bạn không có quyền sửa thông tin account này";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidStatus] = "Trạng thái không hợp lệ";

            InnerHashtable[ErrorCodes.ValidAccountInvalidUsername] = "Không tìm thấy tài khoản";
            InnerHashtable[ErrorCodes.ValidAccountInvalidPassword] = "Mật khẩu không hợp lệ";
            InnerHashtable[ErrorCodes.ValidAccountUserLocked] = "Tài khoản đang bị khóa";
            InnerHashtable[ErrorCodes.ValidAccountInvalidSmsCode] = "Mã sms không hợp lệ";
            InnerHashtable[ErrorCodes.ValidAccountNotHavePermission] = "Không có quyền xử lý";

            InnerHashtable[ErrorCodes.SwitchCurrentRoleUserNotFound] = "Tài khoản không hợp lệ";
            InnerHashtable[ErrorCodes.SwitchCurrentRoleInvalidRole] = "Vai trò không hợp lệ";
            InnerHashtable[ErrorCodes.SwitchCurrentRoleUserNotHaveRole] = "Tài khoản không có vai trò cần chuyển";
            #endregion

            #region News error

            InnerHashtable[ErrorCodes.UpdateNewsNewsNotFound] = "Không tìm thấy bài";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidPrimaryZone] = "Bài chưa có chuyên mục chính";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidTitle] = "Bài chưa có tiêu đề";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidAvatar] = "Bài chưa có avatar";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidTag] = "Bài chưa có tag. Bạn phải nhập tag mới được xuất bản";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidSapo] = "Bài chưa có mô tả";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidBody] = "Bài chưa có nội dung";
            InnerHashtable[ErrorCodes.UpdateNewsNotAllowEdit] = "Trạng thái hiện tại không cho phép sửa";
            InnerHashtable[ErrorCodes.UpdateNewsNotAllowDelete] = "Trạng thái hiện tại không cho phép xóa";
            InnerHashtable[ErrorCodes.UpdateNewsDuplicateId] = "ID của bài đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateNewsLockedByOtherUserForEdit] = "Bài này cũng đang được cập nhật bởi {0}. Bạn có muốn tiếp tục không?";

            InnerHashtable[ErrorCodes.ChangeNewsStatusNewsNotFound] = "Không tìm thấy bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToSend] = "Không được phép gửi bài lên";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceive] = "Không được phép nhận bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToPublish] = "Không được phép xuất bản bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToUnpublish] = "Không được phép gỡ bài đã xuất bản";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReturn] = "Không được phép trả bài về";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToGetBack] = "Không được phép rút lại bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore] = "Không được phép nhận bài trả về";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash] = "Không được phép xóa bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash] = "Không được phép phục hồi bài bị xóa";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToForward] = "Không được phép chuyển bài cho người khác";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReturnYourself] = "Bạn viết bài này nên không được phép trả lại chính bạn";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator] = "Không được phép nhận bài Cộng tác viên";

            InnerHashtable[ErrorCodes.UpdateNewsPositionNotAllowToUpdate] = "Không có quyền cập nhật vị trí";
            InnerHashtable[ErrorCodes.UpdateNewsPositionHasBeenLocked] = "Vị trí tin đang bị khóa";
            InnerHashtable[ErrorCodes.UpdateNewsPositionInvalidUpdateGroup] = "Không tìm thấy nhóm vị trí tin";

            InnerHashtable[ErrorCodes.UpdateNewsNotifictionInvalidUser] = "Không tìm thấy tài khoản cần gửi thông báo";
            InnerHashtable[ErrorCodes.UpdateNewsNotifictionInvalidLabel] = "Không tìm thấy nhãn cần gán cho tin";

            #endregion

            #region Tag error
            InnerHashtable[ErrorCodes.UpdateTagTagNotFound] = "Không tìm thấy tag";
            InnerHashtable[ErrorCodes.UpdateTagInvalidTagName] = "Chưa nhập tag";
            InnerHashtable[ErrorCodes.UpdateTagConflictTagName] = "Trùng tên tag";
            #endregion

            #region Zone error

            InnerHashtable[ErrorCodes.UpdateZoneNotAllow] = "Bạn không có quyền cập nhật chuyên mục.";

            InnerHashtable[ErrorCodes.UpdateZoneInvalidName] = "Tên của chuyên mục không hợp lệ.";

            #endregion

            #region Thread error
            #endregion

            #region Comment error
            #endregion

            #region Video errors

            InnerHashtable[ErrorCodes.VideoNotAvailable] = "Video này không tồn tại";
            InnerHashtable[ErrorCodes.VideoTitleCouldNotBeEmpty] = "Tiêu đề của video không được trống";
            InnerHashtable[ErrorCodes.VideoMustHaveFileToSend] = "Bạn phải upload file video trước khi gửi lên chờ xuất bản";
            InnerHashtable[ErrorCodes.VideoMustHaveFileToPublish] = "Bạn phải upload file video trước khi xuất bản";
            InnerHashtable[ErrorCodes.VideoNotAllowToSend] = "Bạn không có quyền gửi video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToReturn] = "Bạn không có quyền trả lại video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToUpdate] = "Bạn không có quyền sửa video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToPublish] = "Bạn không có quyền xuất bản video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToUnpublish] = "Bạn không có quyền gỡ xuất bản video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToDelete] = "Bạn không có quyền xóa video này";

            InnerHashtable[ErrorCodes.YoutubeVideoUrlExists] = "Địa chỉ youtube đã tồn tại, vui lòng kiểm tra lại trong danh sách";

            InnerHashtable[ErrorCodes.ZoneVideoMustHaveName] = "Bạn chưa nhập tên chuyên mục của video";

            InnerHashtable[ErrorCodes.VideoTagMustHaveName] = "Bạn chưa nhập tên tag video";

            InnerHashtable[ErrorCodes.VideoPlaylistMustHaveName] = "Bạn chưa nhập tên playlist cho video";

            #endregion

            #region Photo errors

            InnerHashtable[ErrorCodes.PhotoAlbumNotFound] = "Album ảnh này không tồn tại";
            InnerHashtable[ErrorCodes.PhotoAlbumNotAllowEdit] = "Không có quyền cập nhật album";

            #endregion         

        }

        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            #region User error

            [EnumMember]
            UpdateAccountUserNotFound = 01000,
            [EnumMember]
            UpdateAccountInvalidUsername = 01001,
            [EnumMember]
            UpdateAccountInvalidPassword = 01002,
            [EnumMember]
            UpdateAccountInvalidRetypePassword = 01003,
            [EnumMember]
            UpdateAccountInvalidEmail = 01004,
            [EnumMember]
            UpdateAccountInvalidMobile = 01005,
            [EnumMember]
            UpdateAccountUsernameExists = 01006,
            [EnumMember]
            UpdateAccountEmailExists = 01007,
            [EnumMember]
            UpdateAccountInvalidOldPassword = 01008,
            [EnumMember]
            UpdateAccountPermissionNotFound = 01009,
            [EnumMember]
            UpdateAccountZoneNotFound = 01010,
            [EnumMember]
            UpdateAccountUserHasBeenLocked = 01011,
            [EnumMember]
            UpdateAccountOldAndNewPasswordNotMatch = 01012,
            [EnumMember]
            UpdateAccountInvalidStatus = 01013,
            [EnumMember]
            UpdateAccountCantNotEditSystem = 01014,

            [EnumMember]
            ValidAccountInvalidUsername = 02001,
            [EnumMember]
            ValidAccountInvalidPassword = 02002,
            [EnumMember]
            ValidAccountUserLocked = 02003,
            [EnumMember]
            ValidAccountInvalidSmsCode = 02004,
            [EnumMember]
            ValidAccountNotHavePermission = 02005,

            [EnumMember]
            SwitchCurrentRoleUserNotFound = 02006,
            [EnumMember]
            SwitchCurrentRoleInvalidRole = 02007,
            [EnumMember]
            SwitchCurrentRoleUserNotHaveRole = 02008,

            #endregion

            #region News error

            [EnumMember]
            UpdateNewsNewsNotFound = 03000,
            [EnumMember]
            UpdateNewsInvalidPrimaryZone = 03001,
            [EnumMember]
            UpdateNewsInvalidTitle = 03002,
            [EnumMember]
            UpdateNewsInvalidAvatar = 03003,
            [EnumMember]
            UpdateNewsInvalidSapo = 03004,
            [EnumMember]
            UpdateNewsInvalidBody = 03005,
            [EnumMember]
            UpdateNewsNotAllowEdit = 03006,
            [EnumMember]
            UpdateNewsNotAllowDelete = 03007,
            [EnumMember]
            UpdateNewsDuplicateId = 03008,
            [EnumMember]
            UpdateNewsLockedByOtherUserForEdit = 03009,
            [EnumMember]
            UpdateNewsInvalidTag = 030010,

            [EnumMember]
            UpdateNewsPositionNotAllowToUpdate = 04051,
            [EnumMember]
            UpdateNewsPositionHasBeenLocked = 04052,
            [EnumMember]
            UpdateNewsPositionInvalidUpdateGroup = 04053,

            [EnumMember]
            UpdateNewsNotifictionInvalidUser = 04031,
            [EnumMember]
            UpdateNewsNotifictionInvalidLabel = 04032,

            [EnumMember]
            ChangeNewsStatusNewsNotFound = 04001,
            [EnumMember]
            ChangeNewsStatusNotAllowToSend = 04002,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceive = 04003,
            [EnumMember]
            ChangeNewsStatusNotAllowToPublish = 04004,
            [EnumMember]
            ChangeNewsStatusNotAllowToUnpublish = 04005,
            [EnumMember]
            ChangeNewsStatusNotAllowToReturn = 04006,
            [EnumMember]
            ChangeNewsStatusNotAllowToGetBack = 04007,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromReturnStore = 04008,
            [EnumMember]
            ChangeNewsStatusNotAllowToMoveToTrash = 04009,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromTrash = 04010,
            [EnumMember]
            ChangeNewsStatusNotAllowToForward = 04011,
            [EnumMember]
            ChangeNewsStatusNotAllowToReturnYourself = 04012,
            [EnumMember]
            ChangeNewsStatusNotAllowToRelease = 04013,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromCooperator = 04014,

            #endregion

            #region Tag error

            [EnumMember]
            UpdateTagTagNotFound = 05000,
            [EnumMember]
            UpdateTagInvalidTagName = 05001,
            [EnumMember]
            UpdateTagConflictTagName = 05002,

            #endregion

            #region Zone error
            [EnumMember]
            UpdateZoneNotAllow = 06000,
            [EnumMember]
            UpdateZoneInvalidName = 06001,
            #endregion

            #region Comment error
            #endregion

            #region Video
            [EnumMember]
            VideoNotAvailable = 07001,
            [EnumMember]
            VideoTitleCouldNotBeEmpty = 07002,
            [EnumMember]
            VideoMustHaveFileToSend = 07003,
            [EnumMember]
            VideoMustHaveFileToPublish = 07004,
            [EnumMember]
            VideoNotAllowToSend = 07005,
            [EnumMember]
            VideoNotAllowToReturn = 07006,
            [EnumMember]
            VideoNotAllowToUpdate = 07007,
            [EnumMember]
            VideoNotAllowToPublish = 07008,
            [EnumMember]
            VideoNotAllowToUnpublish = 07009,
            [EnumMember]
            VideoNotAllowToDelete = 07010,

            [EnumMember]
            YoutubeVideoUrlExists = 07011,

            [EnumMember]
            ZoneVideoMustHaveName = 07012,

            [EnumMember]
            VideoTagMustHaveName = 07013,

            [EnumMember]
            VideoPlaylistMustHaveName = 07014,
            #endregion

            #region Photo

            [EnumMember]
            PhotoAlbumNotFound = 08001,
            [EnumMember]
            PhotoAlbumNotAllowEdit = 08002,

            #endregion                      
            
        }
    }
}

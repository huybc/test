﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Xml;

namespace Mi.Common.ChannelConfig
{
    public class CmsChannelConfiguration
    {
        #region Private members

        private static string ConfigFile
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ChannelConfigFile"]);
                }
                else
                {
                    return ConfigurationManager.AppSettings["ChannelConfigFile"];
                }
            }
        }

        private const string _CONFIG_CACHED_NAME_FORMAT_APPSETTING = "CMS_CHANNEL_CONFIGURATION_CACHED_FORMAT_APPSETTING_{0}";

        private static CmsChannelConfiguration InitConfig(string channelNamespace)
        {
            var outputConfigs = new CmsChannelConfiguration()
            {
                ModuleExtensions = new List<ModuleExtension>(),
                AppSettings = new Dictionary<string, string>()
            };

            try
            {
                var baseConfigFile = ConfigFile;

                if (File.Exists(baseConfigFile))
                {
                    GetConfigInFile(ref outputConfigs, baseConfigFile);
                }

                var fileName = baseConfigFile.Substring(baseConfigFile.LastIndexOf(@"\") + 1);
                var baseFolder = baseConfigFile.Substring(0, baseConfigFile.LastIndexOf(@"\") + 1);
                var extenConfigFile = baseFolder + (string.IsNullOrEmpty(channelNamespace) ? CurrentChannelNamespace : channelNamespace) + @"\" + fileName;

                if (File.Exists(extenConfigFile))
                {
                    GetConfigInFile(ref outputConfigs, extenConfigFile);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }

            return outputConfigs;
        }

        private static void GetConfigInFile(ref CmsChannelConfiguration channelConfig, string configFilePath)
        {
            if (channelConfig == null) channelConfig = new CmsChannelConfiguration();

            var xmlConfigFile = new XmlDocument();
            xmlConfigFile.Load(configFilePath);

            var channelNodes = xmlConfigFile.SelectNodes("//Channel");
            if (channelNodes != null && channelNodes.Count > 0)
            {
                var channelNode = channelNodes[0];
                var channelNamespace = channelNode.Attributes["namespace"].Value;
                if (!string.IsNullOrEmpty(channelNamespace))
                {
                    #region Module extension & Hubs

                    var moduleExtensions = channelConfig.ModuleExtensions;
                    if (moduleExtensions == null)
                    {
                        moduleExtensions = new List<ModuleExtension>();
                    }

                    var moduleExtensionNodes = channelNode.SelectNodes("ModuleExtensions/module");
                    if (moduleExtensionNodes != null)
                    {
                        foreach (XmlNode moduleExtensionNode in moduleExtensionNodes)
                        {
                            if (moduleExtensionNode != null && moduleExtensionNode.Attributes != null)
                            {
                                var moduleExt = new ModuleExtension
                                {
                                    Source = moduleExtensionNode.Attributes["source"] != null ? moduleExtensionNode.Attributes["source"].Value : "",
                                    Target = moduleExtensionNode.Attributes["target"] != null ? moduleExtensionNode.Attributes["target"].Value : "",
                                    Hub = moduleExtensionNode.Attributes["hub"] != null ? moduleExtensionNode.Attributes["hub"].Value : "",
                                    TargetHub = moduleExtensionNode.Attributes["targetHub"] != null ? moduleExtensionNode.Attributes["targetHub"].Value : ""
                                };
                                var existsItemIndex = moduleExtensions.FindIndex(item => item.Source == moduleExtensionNode.Attributes["source"].Value);
                                if (existsItemIndex >= 0)
                                {
                                    moduleExtensions[existsItemIndex] = moduleExt;
                                }
                                else
                                {
                                    moduleExtensions.Add(moduleExt);
                                }
                            }
                        }
                    }
                    channelConfig.ModuleExtensions = moduleExtensions;

                    #endregion

                    #region AppSettings

                    var appSettings = channelConfig.AppSettings;
                    if (appSettings == null)
                    {
                        appSettings = new Dictionary<string, string>();
                    }

                    var appSettingNodes = channelNode.SelectNodes("appSettings/add");
                    if (appSettingNodes != null)
                    {
                        foreach (XmlNode appSettingNode in appSettingNodes)
                        {
                            if (appSettingNode != null && appSettingNode.Attributes != null)
                            {
                                if (appSettings.ContainsKey(appSettingNode.Attributes["key"].Value))
                                {
                                    appSettings[appSettingNode.Attributes["key"].Value] = appSettingNode.Attributes["value"].Value;
                                }
                                else
                                {
                                    appSettings.Add(appSettingNode.Attributes["key"].Value,
                                                appSettingNode.Attributes["value"].Value);
                                }
                            }
                        }
                    }
                    channelConfig.AppSettings = appSettings;

                    #endregion
                }
            }
        }

        #endregion

        #region CmsSettings define

        //public string Namespace { get; set; }
        public List<ModuleExtension> ModuleExtensions { get; set; }
        public Dictionary<string, string> AppSettings { get; set; }
        public class ModuleExtension
        {
            public string Source { get; set; }
            public string Target { get; set; }
            public string Hub { get; set; }
            public string TargetHub { get; set; }
        }

        #endregion

        #region public methods

        public static string CurrentChannelNamespace
        {
            get
            {
                return ConfigurationManager.AppSettings["CmsApi.Namespace"];
            }
        }

        private static List<ModuleExtension> AllModuleExtensionCached { get; set; }
        public static List<ModuleExtension> GetAllModuleExtension(string channelNamespace)
        {
            var currentValue = new List<ModuleExtension>();
            if (HttpContext.Current != null)
            {
                var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_APPSETTING,
                    channelNamespace);
                currentValue = HttpContext.Current.Cache[cachedKey] as List<ModuleExtension>;
                if (currentValue == null || currentValue.Count <= 0)
                {
                    try
                    {
                        currentValue = InitConfig(channelNamespace).ModuleExtensions;

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                            new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                            TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal,
                            "Read CmsChannelConfig.GetAllModuleExtension() error => " + ex);
                    }
                }
            }
            else
            {
                if (AllModuleExtensionCached == null || AllModuleExtensionCached.Count <= 0)
                {
                    AllModuleExtensionCached = InitConfig(channelNamespace).ModuleExtensions;
                }
                currentValue = AllModuleExtensionCached;
            }
            return currentValue ?? (currentValue = new List<ModuleExtension>());
        }
        public static List<ModuleExtension> GetAllModuleExtension()
        {
            var channelNamespace = CurrentChannelNamespace;
            var currentValue = new List<ModuleExtension>();
            if (HttpContext.Current != null)
            {
                var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_APPSETTING,
                                              channelNamespace);
                currentValue = HttpContext.Current.Cache[cachedKey] as List<ModuleExtension>;
                if (currentValue == null || currentValue.Count <= 0)
                {
                    try
                    {
                        currentValue = InitConfig(channelNamespace).ModuleExtensions;

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                                                         new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                                                         TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "Read CmsChannelConfig.GetAllModuleExtension() error => " + ex);
                    }
                }
            }
            else
            {
                if (AllModuleExtensionCached == null || AllModuleExtensionCached.Count <= 0)
                {
                    AllModuleExtensionCached = InitConfig(channelNamespace).ModuleExtensions;
                }
                currentValue = AllModuleExtensionCached;
            }
            return currentValue ?? (currentValue = new List<ModuleExtension>());
        }

        private static Dictionary<string, string> AppSettingCached { get; set; }
        public static string GetAppSetting(string channelNamespace, string key)
        {
            var currentValue = "";
            if (HttpContext.Current != null)
            {
                var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_APPSETTING,
                                              channelNamespace + "_" + key);
                currentValue = HttpContext.Current.Cache[cachedKey] as string;
                if (string.IsNullOrEmpty(currentValue))
                {
                    try
                    {
                        currentValue = InitConfig(channelNamespace).AppSettings[key];

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                                                         new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                                                         TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "Read CmsChannelConfig.GetAppSetting(" + key + ") error => " + ex);
                    }
                }
            }
            else
            {
                if (AppSettingCached == null || AppSettingCached.Count <= 0)
                {
                    AppSettingCached = InitConfig(channelNamespace).AppSettings;
                }
                currentValue = AppSettingCached[key];
            }
            return currentValue ?? (currentValue = "");
        }
        public static string GetAppSetting(string key)
        {
            var channelNamespace = CurrentChannelNamespace;
            var currentValue = "";
            if (HttpContext.Current != null)
            {
                var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_APPSETTING,
                                              channelNamespace + "_" + key);
                currentValue = HttpContext.Current.Cache[cachedKey] as string;
                if (string.IsNullOrEmpty(currentValue))
                {
                    try
                    {
                        currentValue = InitConfig(channelNamespace).AppSettings[key];

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                                                         new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                                                         TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "Read CmsChannelConfig.GetAppSetting(" + key + ") error => " + ex);
                    }
                }
            }
            else
            {
                if (AppSettingCached == null || AppSettingCached.Count <= 0)
                {
                    AppSettingCached = InitConfig(channelNamespace).AppSettings;
                }
                currentValue = AppSettingCached[key];
            }
            return currentValue ?? (currentValue = "");
        }

        public static bool GetAppSettingInBoolean(string channelNamespace, string key)
        {
            return Utility.ConvertToBoolean(GetAppSetting(channelNamespace, key));
        }
        public static bool GetAppSettingInBoolean(string key)
        {
            return Utility.ConvertToBoolean(GetAppSetting(key));
        }

        public static int GetAppSettingInInt32(string channelNamespace, string key)
        {
            return Utility.ConvertToInt(GetAppSetting(channelNamespace, key));
        }
        public static int GetAppSettingInInt32(string key)
        {
            return Utility.ConvertToInt(GetAppSetting(key));
        }

        public static long GetAppSettingInInt64(string channelNamespace, string key)
        {
            return Utility.ConvertToLong(GetAppSetting(channelNamespace, key));
        }
        public static long GetAppSettingInInt64(string key)
        {
            return Utility.ConvertToLong(GetAppSetting(key));
        }

        #endregion
    }
}

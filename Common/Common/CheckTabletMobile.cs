﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Mi.Common
{

    public class CheckIsMobile  
    {
        public static bool isMobileBrowser(HttpContext context)
        {
            //GETS THE CURRENT USER CONTEXT
            // HttpContext context = HttpContext.Current;

            //FIRST TRY BUILT IN ASP.NT CHECK
            if (context.Request.Browser.IsMobileDevice)
            {
                return true;
            }
            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }
            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }
            //AND FINALLY CHECK THE HTTP_USER_AGENT 
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                #region
                ////Create a list of all mobile types
                ////string[] mobiles = { "midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "erics", "ericsson", "SonyEricsson", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi", "moto", "iphone" };

                ////Loop through each item in the list created above 
                ////and check if the header contains that text
                //foreach (string s in mobiles)
                //{
                //    if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                //                                        ToLower().Contains(s.ToLower()))
                //    {
                //        return true;
                //    }
                //}
                #endregion
                string u = context.Request.ServerVariables["HTTP_USER_AGENT"];
                Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
                {
                    return true;
                }
            }

            return false;
        }
    } 

    public class CheckIsTablet 
    {

        private string useragent = "";
        private string httpaccept = "";

        #region Fields - Detection Argument Values

        //standardized values for detection arguments.
        private const string dargsIphone = "iphone";
        private const string dargsIpod = "ipod";
        private const string dargsIpad = "ipad";
        private const string dargsIphoneOrIpod = "iphoneoripod";
        private const string dargsIos = "ios";
        private const string dargsAndroid = "android";
        private const string dargsAndroidPhone = "androidphone";
        private const string dargsAndroidTablet = "androidtablet";
        private const string dargsGoogleTV = "googletv";
        private const string dargsWebKit = "webkit";
        private const string dargsSymbianOS = "symbianos";
        private const string dargsS60 = "series60";
        private const string dargsWindowsPhone7 = "windowsphone7";
        private const string dargsWindowsMobile = "windowsmobile";
        private const string dargsBlackBerry = "blackberry";
        private const string dargsBlackBerryWebkit = "blackberrywebkit";
        private const string dargsPalmOS = "palmos";
        private const string dargsPalmWebOS = "webos";
        private const string dargsWebOSTablet = "webostablet";
        private const string dargsSmartphone = "smartphone";
        private const string dargsBrewDevice = "brew";
        private const string dargsDangerHiptop = "dangerhiptop";
        private const string dargsOperaMobile = "operamobile";
        private const string dargsWapWml = "wapwml";
        private const string dargsKindle = "kindle";
        private const string dargsMobileQuick = "mobilequick";
        private const string dargsTierTablet = "tiertablet";
        private const string dargsTierIphone = "tieriphone";
        private const string dargsTierRichCss = "tierrichcss";
        private const string dargsTierOtherPhones = "tierotherphones";

        #endregion Fields - Detection Argument Values

        #region Fields - User Agent Keyword Values

        private const string engineWebKit = "WEBKIT";
        private const string deviceIphone = "IPHONE";
        private const string deviceIpod = "IPOD";
        private const string deviceIpad = "IPAD";
        private const string deviceMacPpc = "MACINTOSH"; //Used for disambiguation

        private const string deviceAndroid = "ANDROID";
        private const string deviceGoogleTV = "GOOGLETV";
        private const string deviceHtcFlyer = "HTC_FLYER"; //HTC Flyer

        private const string deviceNuvifone = "NUVIFONE";  //Garmin Nuvifone

        private const string deviceSymbian = "SYMBIAN";
        private const string deviceS60 = "SERIES60";
        private const string deviceS70 = "SERIES70";
        private const string deviceS80 = "SERIES80";
        private const string deviceS90 = "SERIES90";

        private const string deviceWinPhone7 = "WINDOWS PHONE OS 7";
        private const string deviceWinMob = "WINDOWS CE";
        private const string deviceWindows = "WINDOWS";
        private const string deviceIeMob = "IEMOBILE";
        private const string devicePpc = "PPC"; //Stands for PocketPC
        private const string enginePie = "WM5 PIE"; //An old Windows Mobile browser

        private const string deviceBB = "BLACKBERRY";
        private const string vndRIM = "VND.RIM"; //Detectable when BB devices emulate IE or Firefox
        private const string deviceBBStorm = "BLACKBERRY95"; //Storm 1 and 2
        private const string deviceBBBold = "BLACKBERRY97"; //Bold 97x0 (non-touch)
        private const string deviceBBBoldTouch = "BLACKBERRY 99"; //Bold 99x0 (touchscreen)
        private const string deviceBBTour = "BLACKBERRY96"; //Tour
        private const string deviceBBCurve = "BLACKBERRY89"; //Curve2
        private const string deviceBBCurveTouch = "BLACKBERRY 938"; //Curve Touch 9380
        private const string deviceBBTorch = "BLACKBERRY 98"; //Torch
        private const string deviceBBPlaybook = "PLAYBOOK"; //PlayBook tablet

        private const string devicePalm = "PALM";
        private const string deviceWebOS = "WEBOS"; //For Palm's line of WebOS devices
        private const string deviceWebOShp = "HPWOS"; //For HP's line of WebOS devices

        private const string engineBlazer = "BLAZER"; //Old Palm
        private const string engineXiino = "XIINO"; //Another old Palm

        private const string deviceKindle = "KINDLE";  //Amazon Kindle, eInk one
        private const string engineSilk = "SILK";  //Amazon's accelerated Silk browser for Kindle Fire

        //Initialize private strings for mobile-specific content.
        private const string vndwap = "VND.WAP";
        private const string wml = "WML";

        //Initialize private strings for other random devices and mobile browsers.
        private const string deviceTablet = "TABLET"; //Generic term for slate and tablet devices
        private const string deviceBrew = "BREW";
        private const string deviceDanger = "DANGER";
        private const string deviceHiptop = "HIPTOP";
        private const string devicePlaystation = "PLAYSTATION";
        private const string deviceNintendoDs = "NITRO";
        private const string deviceNintendo = "NINTENDO";
        private const string deviceWii = "WII";
        private const string deviceXbox = "XBOX";
        private const string deviceArchos = "ARCHOS";

        private const string engineOpera = "OPERA"; //Popular browser
        private const string engineNetfront = "NETFRONT"; //Common embedded OS browser
        private const string engineUpBrowser = "UP.BROWSER"; //common on some phones
        private const string engineOpenWeb = "OPENWEB"; //Transcoding by OpenWave server
        private const string deviceMidp = "MIDP"; //a mobile Java technology
        private const string uplink = "UP.LINK";
        private const string engineTelecaQ = "TELECA Q"; //a modern feature phone browser

        private const string devicePda = "PDA"; //some devices report themselves as PDAs
        private const string mini = "MINI";  //Some mobile browsers put "mini" in their names.
        private const string mobile = "MOBILE"; //Some mobile browsers put "mobile" in their user agent private strings.
        private const string mobi = "MOBI"; //Some mobile browsers put "mobi" in their user agent private strings.

        //Use Maemo, Tablet, and Linux to test for Nokia"s Internet Tablets.
        private const string maemo = "MAEMO";
        private const string linux = "LINUX";
        private const string qtembedded = "QT EMBEDDED"; //for Sony Mylo
        private const string mylocom2 = "COM2"; //for Sony Mylo also

        //In some UserAgents, the only clue is the manufacturer.
        private const string manuSonyEricsson = "SONYERICSSON";
        private const string manuericsson = "ERICSSON";
        private const string manuSamsung1 = "SEC-SGH";
        private const string manuSony = "SONY";
        private const string manuHtc = "HTC"; //Popular Android and WinMo manufacturer

        //In some UserAgents, the only clue is the operator.
        private const string svcDocomo = "DOCOMO";
        private const string svcKddi = "KDDI";
        private const string svcVodafone = "VODAFONE";

        //Disambiguation strings.
        private const string disUpdate = "UPDATE"; //pda vs. update

        #endregion Fields - User Agent Keyword Values

        /// <summary>
        /// To instantiate a WebPage sub-class with built-in
        /// mobile device detection delegates and events.
        /// </summary>
        public CheckIsTablet(HttpContext context)
        {
            useragent = (context.Request.ServerVariables["HTTP_USER_AGENT"] ?? "").ToUpper();
            httpaccept = (context.Request.ServerVariables["HTTP_ACCEPT"] ?? "").ToUpper();
        }

        #region Mobile Device Detection Methods

        //**************************
        // Detects if the current device is an iPod Touch.
        public bool DetectIpod()
        {
            if (useragent.IndexOf(deviceIpod) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an iPad tablet.
        public bool DetectIpad()
        {
            if (useragent.IndexOf(deviceIpad) != -1 && DetectWebkit())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an iPhone.
        public bool DetectIphone()
        {
            if (useragent.IndexOf(deviceIphone) != -1)
            {
                //The iPad and iPod touch say they're an iPhone! So let's disambiguate.
                if (DetectIpad() || DetectIpod())
                {
                    return false;
                }
                else
                    return true;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current device is an iPhone or iPod Touch.
        public bool DetectIphoneOrIpod()
        {
            //We repeat the searches here because some iPods may report themselves as an iPhone, which would be okay.
            if (useragent.IndexOf(deviceIphone) != -1 ||
                useragent.IndexOf(deviceIpod) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects *any* iOS device: iPhone, iPod Touch, iPad.
        public bool DetectIos()
        {
            if (DetectIphoneOrIpod() || DetectIpad())
                return true;
            else
                return false;
        }

        //**************************
        // Detects *any* Android OS-based device: phone, tablet, and multi-media player.
        // Also detects Google TV.
        public bool DetectAndroid()
        {
            if ((useragent.IndexOf(deviceAndroid) != -1) ||
                DetectGoogleTV())
                return true;
            //Special check for the HTC Flyer 7" tablet. It should report here.
            if (useragent.IndexOf(deviceHtcFlyer) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is a (small-ish) Android OS-based device
        // used for calling and/or multi-media (like a Samsung Galaxy Player).
        // Google says these devices will have 'Android' AND 'mobile' in user agent.
        // Ignores tablets (Honeycomb and later).
        public bool DetectAndroidPhone()
        {
            if (DetectAndroid() &&
                (useragent.IndexOf(mobile) != -1))
                return true;
            //Special check for Android phones with Opera Mobile. They should report here.
            if (DetectOperaAndroidPhone())
                return true;
            //Special check for the HTC Flyer 7" tablet. It should report here.
            if (useragent.IndexOf(deviceHtcFlyer) != -1)
                return true;
            else
                return false;
        }


        //**************************
        // Detects if the current device is a (self-reported) Android tablet.
        // Google says these devices will have 'Android' and NOT 'mobile' in their user agent.
        public bool DetectAndroidTablet()
        {
            //First, let's make sure we're on an Android device.
            if (!DetectAndroid())
                return false;

            //Special check for Opera Android Phones. They should NOT report here.
            if (DetectOperaMobile())
                return false;
            //Special check for the HTC Flyer 7" tablet. It should NOT report here.
            if (useragent.IndexOf(deviceHtcFlyer) != -1)
                return false;

            //Otherwise, if it's Android and does NOT have 'mobile' in it, Google says it's a tablet.
            if (useragent.IndexOf(mobile) > -1)
                return false;
            else
                return true;
        }

        //**************************
        // Detects if the current device is a GoogleTV device.
        public bool DetectGoogleTV()
        {
            if (useragent.IndexOf(deviceGoogleTV) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an Android OS-based device and
        //   the browser is based on WebKit.
        public bool DetectAndroidWebKit()
        {
            if (DetectAndroid() && DetectWebkit())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is based on WebKit.
        public bool DetectWebkit()
        {
            if (useragent.IndexOf(engineWebKit) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is the Nokia S60 Open Source Browser.
        public bool DetectS60OssBrowser()
        {
            //First, test for WebKit, then make sure it's either Symbian or S60.
            if (DetectWebkit())
            {
                if (useragent.IndexOf(deviceSymbian) != -1 ||
                    useragent.IndexOf(deviceS60) != -1)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current device is any Symbian OS-based device,
        //   including older S60, Series 70, Series 80, Series 90, and UIQ, 
        //   or other browsers running on these devices.
        public bool DetectSymbianOS()
        {
            if (useragent.IndexOf(deviceSymbian) != -1 ||
                useragent.IndexOf(deviceS60) != -1 ||
                useragent.IndexOf(deviceS70) != -1 ||
                useragent.IndexOf(deviceS80) != -1 ||
                useragent.IndexOf(deviceS90) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a 
        // Windows Phone 7 device.
        public bool DetectWindowsPhone7()
        {
            if (useragent.IndexOf(deviceWinPhone7) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a Windows Mobile device.
        // Excludes Windows Phone 7 devices. 
        // Focuses on Windows Mobile 6.xx and earlier.
        public bool DetectWindowsMobile()
        {
            //Exclude new Windows Phone 7.
            if (DetectWindowsPhone7())
                return false;
            //Most devices use 'Windows CE', but some report 'iemobile' 
            //  and some older ones report as 'PIE' for Pocket IE. 
            if (useragent.IndexOf(deviceWinMob) != -1 ||
                useragent.IndexOf(deviceIeMob) != -1 ||
                useragent.IndexOf(enginePie) != -1)
                return true;
            //Test for Windows Mobile PPC but not old Macintosh PowerPC.
            if (useragent.IndexOf(devicePpc) != -1 &&
                !(useragent.IndexOf(deviceMacPpc) != -1))
                return true;
            //Test for certain Windwos Mobile-based HTC devices.
            if (useragent.IndexOf(manuHtc) != -1 &&
                useragent.IndexOf(deviceWindows) != -1)
                return true;
            if (DetectWapWml() == true &&
                useragent.IndexOf(deviceWindows) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is any BlackBerry device.
        // Includes the PlayBook.
        public bool DetectBlackBerry()
        {
            if ((useragent.IndexOf(deviceBB) != -1) ||
                (httpaccept.IndexOf(vndRIM) != -1))
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is on a BlackBerry tablet device.
        //    Example: PlayBook
        public bool DetectBlackBerryTablet()
        {
            if (useragent.IndexOf(deviceBBPlaybook) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a BlackBerry device AND uses a
        //    WebKit-based browser. These are signatures for the new BlackBerry OS 6.
        //    Examples: Torch. Includes the Playbook.
        public bool DetectBlackBerryWebKit()
        {
            if (DetectBlackBerry() && DetectWebkit())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a BlackBerry Touch
        //    device, such as the Storm, Torch, and Bold Touch. Excludes the Playbook.
        public bool DetectBlackBerryTouch()
        {
            if (DetectBlackBerry() &&
                (useragent.IndexOf(deviceBBStorm) != -1 ||
                useragent.IndexOf(deviceBBTorch) != -1 ||
                useragent.IndexOf(deviceBBBoldTouch) != -1 ||
                useragent.IndexOf(deviceBBCurveTouch) != -1))
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a BlackBerry device AND
        //    has a more capable recent browser. Excludes the Playbook.
        //    Examples, Storm, Bold, Tour, Curve2
        //    Excludes the new BlackBerry OS 6 and 7 browser!!
        public bool DetectBlackBerryHigh()
        {
            //Disambiguate for BlackBerry OS 6 or 7 (WebKit) browser
            if (DetectBlackBerryWebKit())
                return false;
            if (DetectBlackBerry())
            {
                if (DetectBlackBerryTouch() ||
                    useragent.IndexOf(deviceBBBold) != -1 ||
                    useragent.IndexOf(deviceBBTour) != -1 ||
                    useragent.IndexOf(deviceBBCurve) != -1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a BlackBerry device AND
        //    has an older, less capable browser. 
        //    Examples: Pearl, 8800, Curve1.
        public bool DetectBlackBerryLow()
        {
            if (DetectBlackBerry())
            {
                //Assume that if it's not in the High tier, then it's Low.
                if (DetectBlackBerryHigh() || DetectBlackBerryWebKit())
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current browser is on a PalmOS device.
        public bool DetectPalmOS()
        {
            //Most devices nowadays report as 'Palm', but some older ones reported as Blazer or Xiino.
            if (useragent.IndexOf(devicePalm) != -1 ||
                useragent.IndexOf(engineBlazer) != -1 ||
                useragent.IndexOf(engineXiino) != -1)
            {
                //Make sure it's not WebOS first
                if (DetectPalmWebOS() == true)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current browser is on a Palm device
        //    running the new WebOS.
        public bool DetectPalmWebOS()
        {
            if (useragent.IndexOf(deviceWebOS) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is on an HP tablet running WebOS.
        public bool DetectWebOSTablet()
        {
            if (useragent.IndexOf(deviceWebOShp) != -1 &&
                useragent.IndexOf(deviceTablet) != -1)
            {
                return true;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a
        //    Garmin Nuvifone.
        public bool DetectGarminNuvifone()
        {
            if (useragent.IndexOf(deviceNuvifone) != -1)
                return true;
            else
                return false;
        }


        //**************************
        // Check to see whether the device is any device
        //   in the 'smartphone' category.
        public bool DetectSmartphone()
        {
            if (DetectIphoneOrIpod() ||
                DetectAndroidPhone() ||
                DetectS60OssBrowser() ||
                DetectSymbianOS() ||
                DetectWindowsMobile() ||
                DetectWindowsPhone7() ||
                DetectBlackBerry() ||
                DetectPalmWebOS() ||
                DetectPalmOS() ||
                DetectGarminNuvifone())
                return true;
            else
                return false;
        }

        //**************************
        // Detects whether the device is a Brew-powered device.
        public bool DetectBrewDevice()
        {
            if (useragent.IndexOf(deviceBrew) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects the Danger Hiptop device.
        public bool DetectDangerHiptop()
        {
            if (useragent.IndexOf(deviceDanger) != -1 ||
                useragent.IndexOf(deviceHiptop) != -1)
                return true;
            else
                return false;
        }


        //**************************
        // Detects if the current browser is Opera Mobile or Mini.
        public bool DetectOperaMobile()
        {
            if (useragent.IndexOf(engineOpera) != -1)
            {
                if ((useragent.IndexOf(mini) != -1) ||
                 (useragent.IndexOf(mobi) != -1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        //**************************
        // Detects if the current browser is Opera Mobile
        // running on an Android phone.
        public bool DetectOperaAndroidPhone()
        {
            if ((useragent.IndexOf(engineOpera) != -1) &&
                (useragent.IndexOf(deviceAndroid) != -1) &&
                (useragent.IndexOf(mobi) != -1))
                return true;
            else
                return false;
        }

        // Detects if the current browser is Opera Mobile
        // running on an Android tablet.
        public bool DetectOperaAndroidTablet()
        {
            if ((useragent.IndexOf(engineOpera) != -1) &&
                (useragent.IndexOf(deviceAndroid) != -1) &&
                (useragent.IndexOf(deviceTablet) != -1))
                return true;
            else
                return false;
        }

        //**************************
        // Detects whether the device supports WAP or WML.
        public bool DetectWapWml()
        {
            if (httpaccept.IndexOf(vndwap) != -1 ||
                httpaccept.IndexOf(wml) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an Amazon Kindle (eInk devices only).
        // Note: For the Kindle Fire, use the normal Android methods. 
        public bool DetectKindle()
        {
            if (useragent.IndexOf(deviceKindle) != -1 &&
                !DetectAndroid())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current Amazon device is using the Silk Browser.
        // Note: Typically used by the the Kindle Fire.
        public bool DetectAmazonSilk()
        {
            if (useragent.IndexOf(engineSilk) != -1)
                return true;
            else
                return false;
        }

        //**************************
        //   Detects if the current device is a mobile device.
        //   This method catches most of the popular modern devices.
        //   Excludes Apple iPads and other modern tablets.
        public bool DetectMobileQuick()
        {
            //Let's exclude tablets
            if (DetectTierTablet())
                return false;

            //Most mobile browsing is done on smartphones
            if (DetectSmartphone())
                return true;

            if (DetectWapWml() ||
                DetectBrewDevice() ||
                DetectOperaMobile())
                return true;

            if ((useragent.IndexOf(engineNetfront) != -1) ||
                (useragent.IndexOf(engineUpBrowser) != -1) ||
                (useragent.IndexOf(engineOpenWeb) != -1))
                return true;

            if (DetectDangerHiptop() ||
                DetectMidpCapable() ||
                DetectMaemoTablet() ||
                DetectArchos())
                return true;

            if ((useragent.IndexOf(devicePda) != -1) &&
                (useragent.IndexOf(disUpdate) < 0)) //no index found
                return true;
            if (useragent.IndexOf(mobile) != -1)
                return true;

            //We also look for Kindle devices
            if (DetectKindle() ||
                DetectAmazonSilk())
                return true;

            else
                return false;
        }

        //**************************
        // Detects if the current device is a Sony Playstation.
        public bool DetectSonyPlaystation()
        {
            if (useragent.IndexOf(devicePlaystation) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is a Nintendo game device.
        public bool DetectNintendo()
        {
            if (useragent.IndexOf(deviceNintendo) != -1 ||
                 useragent.IndexOf(deviceWii) != -1 ||
                 useragent.IndexOf(deviceNintendoDs) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is a Microsoft Xbox.
        public bool DetectXbox()
        {
            if (useragent.IndexOf(deviceXbox) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an Internet-capable game console.
        public bool DetectGameConsole()
        {
            if (DetectSonyPlaystation())
                return true;
            else if (DetectNintendo())
                return true;
            else if (DetectXbox())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device supports MIDP, a mobile Java technology.
        public bool DetectMidpCapable()
        {
            if (useragent.IndexOf(deviceMidp) != -1 ||
                httpaccept.IndexOf(deviceMidp) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is on one of the Maemo-based Nokia Internet Tablets.
        public bool DetectMaemoTablet()
        {
            if (useragent.IndexOf(maemo) != -1)
                return true;
            //For Nokia N810, must be Linux + Tablet, or else it could be something else. 
            else if (useragent.IndexOf(linux) != -1 &&
                useragent.IndexOf(deviceTablet) != -1 &&
                !DetectWebOSTablet() &&
                !DetectAndroid())
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an Archos media player/Internet tablet.
        public bool DetectArchos()
        {
            if (useragent.IndexOf(deviceArchos) != -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current browser is a Sony Mylo device.
        public bool DetectSonyMylo()
        {
            if (useragent.IndexOf(manuSony) != -1)
            {
                if ((useragent.IndexOf(qtembedded) != -1) ||
                 (useragent.IndexOf(mylocom2) != -1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        //**************************
        // The longer and more thorough way to detect for a mobile device.
        //   Will probably detect most feature phones,
        //   smartphone-class devices, Internet Tablets, 
        //   Internet-enabled game consoles, etc.
        //   This ought to catch a lot of the more obscure and older devices, also --
        //   but no promises on thoroughness!
        public bool DetectMobileLong()
        {
            if (DetectMobileQuick())
                return true;
            if (DetectGameConsole() ||
                DetectSonyMylo())
                return true;

            //Detect older phones from certain manufacturers and operators. 
            if (useragent.IndexOf(uplink) != -1)
                return true;
            if (useragent.IndexOf(manuSonyEricsson) != -1)
                return true;
            if (useragent.IndexOf(manuericsson) != -1)
                return true;
            if (useragent.IndexOf(manuSamsung1) != -1)
                return true;

            if (useragent.IndexOf(svcDocomo) != -1)
                return true;
            if (useragent.IndexOf(svcKddi) != -1)
                return true;
            if (useragent.IndexOf(svcVodafone) != -1)
                return true;

            else
                return false;
        }
        //*****************************
        // For Mobile Web Site Design
        //*****************************

        //**************************
        // The quick way to detect for a tier of devices.
        //   This method detects for the new generation of
        //   HTML 5 capable, larger screen tablets.
        //   Includes iPad, Android (e.g., Xoom), BB Playbook, WebOS, etc.
        public bool DetectTierTablet()
        {
            if (DetectIpad()
                || DetectAndroidTablet()
                || DetectBlackBerryTablet()
                || DetectWebOSTablet())
                return true;
            else
                return false;
        }
        //**************************
        // The quick way to detect for a tier of devices.
        //   This method detects for devices which can 
        //   display iPhone-optimized web content.
        //   Includes iPhone, iPod Touch, Android, Windows Phone 7, WebOS, etc.
        public bool DetectTierIphone()
        {
            if (DetectIphoneOrIpod() ||
                DetectAndroidPhone() ||
                (DetectBlackBerryWebKit() &&
                    DetectBlackBerryTouch()) ||
                DetectWindowsPhone7() ||
                DetectPalmWebOS() ||
                DetectGarminNuvifone())
                return true;
            else
                return false;
        }
        //**************************
        // The quick way to detect for a tier of devices.
        //   This method detects for devices which are likely to be capable 
        //   of viewing CSS content optimized for the iPhone, 
        //   but may not necessarily support JavaScript.
        //   Excludes all iPhone Tier devices.
        public bool DetectTierRichCss()
        {
            if (DetectMobileQuick())
            {
                //Exclude iPhone Tier and e-Ink Kindle devices
                if (DetectTierIphone() || DetectKindle())
                    return false;

                if (DetectWebkit() ||
                    DetectS60OssBrowser())
                    return true;

                //Note: 'High' BlackBerry devices ONLY
                if (DetectBlackBerryHigh() == true)
                    return true;

                //Older Windows 'Mobile' isn't good enough for iPhone Tier.
                if (DetectWindowsMobile() == true)
                    return true;
                if (useragent.IndexOf(engineTelecaQ) != -1)
                    return true;

                else
                    return false;
            }
            else
                return false;
        }
        //**************************
        // The quick way to detect for a tier of devices.
        //   This method detects for all other types of phones,
        //   but excludes the iPhone and Smartphone Tier devices.
        public bool DetectTierOtherPhones()
        {
            if (DetectMobileLong() == true)
            {
                //Exclude devices in the other 2 categories
                if (DetectTierIphone() ||
                    DetectTierRichCss())
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        //***************************************************************
        #endregion
    }
}

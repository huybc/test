﻿namespace Mi.Common
{
    public class HtmlControls
    {
        public static void ShowPannel(System.Web.UI.WebControls.Panel panelId)
        {
            panelId.Visible = true;
        }

        public static void HidePannel(System.Web.UI.WebControls.Panel panelId)
        {
            panelId.Visible = false;
        }
    }
}

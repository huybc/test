﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Mi.Common.DalCommon
{
    public class EntityMapper
    {
        private static object CreateObjectFromReader(Type type, IDataReader reader, bool closeReader)
        {
            object returnObject = null;
            var isSuccess = Null.NullBoolean;
            var canRead = true;
            if (closeReader)
            {
                canRead = reader.Read();
            }
            try
            {
                if (canRead)
                {
                    returnObject = CreateObject(type, false);
                    FillObjectFromReader(returnObject, reader);
                }
                isSuccess = true;
            }
            finally
            {
                if ((!isSuccess))
                    closeReader = true;
                CloseDataReader(reader, closeReader);
            }
            return returnObject;
        }

        public static void InitializeObject(object objObject)
        {
            foreach (var objPropertyInfo in GetObjectMapping(objObject.GetType()).Properties.Values.Where(objPropertyInfo => objPropertyInfo.CanWrite))
            {
                objPropertyInfo.SetValue(objObject, Null.SetNull(objPropertyInfo), null);
            }
        }

        public static void CloseDataReader(IDataReader reader, bool closeReader)
        {
            if (reader != null && closeReader)
            {
                reader.Close();
            }
        }
        public static TObject CreateObject<TObject>()
        {
            return (TObject)CreateObject(typeof(TObject), false);
        }
        public static TObject CreateObject<TObject>(bool initialise)
        {
            return (TObject)CreateObject(typeof(TObject), initialise);
        }
        public static object CreateObject(Type type, bool initialise)
        {
            object objectValue = Activator.CreateInstance(type);
            if (initialise)
            {
                InitializeObject(objectValue);
            }
            return objectValue;
        }
        //
        private static IDictionary<TKey, TValue> FillDictionaryFromReader<TKey, TValue>(string keyField, IDataReader reader, IDictionary<TKey, TValue> dictionary)
        {
            var keyValue = default(TKey);
            try
            {
                while (reader.Read())
                {
                    var objectValue = (TValue)CreateObjectFromReader(typeof(TValue), reader, false);

                    if (typeof(TKey).Name == "Int32" && reader[keyField].GetType().Name == "Decimal")
                    {
                        keyValue = (TKey)Null.SetNull(reader[keyField], keyValue);
                    }
                    else if (typeof(TKey).Name.ToLower() == "string" && reader[keyField].GetType().Name.ToLower() == "dbnull")
                    {
                        keyValue = (TKey)Null.SetNull(reader[keyField], "");
                    }
                    else
                    {
                        keyValue = (TKey)Null.SetNull(reader[keyField], keyValue);
                    }
                    if (null != objectValue)
                    {
                        dictionary[keyValue] = objectValue;
                    }
                }
            }
            finally
            {
                CloseDataReader(reader, true);
            }
            return dictionary;
        }

        public static Dictionary<TKey, TValue> FillDictionary<TKey, TValue>(string keyField, IDataReader reader)
        {
            return (Dictionary<TKey, TValue>)FillDictionaryFromReader(keyField, reader, new Dictionary<TKey, TValue>());
        }
        public static Dictionary<TKey, TValue> FillDictionary<TKey, TValue>(string keyField, IDataReader reader, IDictionary<TKey, TValue> dictionary)
        {
            return (Dictionary<TKey, TValue>)FillDictionaryFromReader(keyField, reader, dictionary);
        }
        //
        private static IList FillListFromReader(Type type, IDataReader reader, IList list, bool closeReader)
        {
            var isSuccess = Null.NullBoolean;
            try
            {
                while (reader.Read())
                {
                    object objectValue = CreateObjectFromReader(type, reader, false);
                    list.Add(objectValue);
                }
                isSuccess = true;
            }
            finally
            {
                if ((!isSuccess))
                    closeReader = true;
                CloseDataReader(reader, closeReader);
            }
            return list;
        }

        public static ArrayList FillCollection(IDataReader reader, Type type)
        {
            return (ArrayList)FillListFromReader(type, reader, new ArrayList(), true);
        }
        public static ArrayList FillCollection(IDataReader reader, Type type, bool closeReader)
        {
            return (ArrayList)FillListFromReader(type, reader, new ArrayList(), closeReader);
        }

        public static IList FillCollection(IDataReader reader, Type type, ref IList objectToFill)
        {
            return FillListFromReader(type, reader, objectToFill, true);
        }
        //
        private static IList<TItem> FillListFromReader<TItem>(IDataReader reader, IList<TItem> list, bool closeReader)
        {
            var isSuccess = Null.NullBoolean;
            try
            {
                while (reader.Read())
                {
                    var objectValue = (TItem)CreateObjectFromReader(typeof(TItem), reader, false);
                    list.Add(objectValue);
                }
                isSuccess = true;
            }
            finally
            {
                if ((!isSuccess))
                    closeReader = true;
                CloseDataReader(reader, closeReader);
            }
            return list;
        }

        public static List<TItem> FillCollection<TItem>(IDataReader reader)
        {
            return (List<TItem>)FillListFromReader(reader, new List<TItem>(), true);
        }
        public static IList<TItem> FillCollection<TItem>(IDataReader reader, ref IList<TItem> objectToFill)
        {
            return FillListFromReader(reader, objectToFill, true);
        }
        public static IList<TItem> FillCollection<TItem>(IDataReader reader, IList<TItem> objectToFill, bool closeReader)
        {
            return FillListFromReader(reader, objectToFill, closeReader);
        }
        //
        private static void SetObjectValue(IDataRecord reader, ref EntityBase entity)
        {
            var type = entity.GetType();


            for (var i = 0; i < reader.FieldCount; i++)
            {
                var fieldName = reader.GetName(i);
                try
                {

                    var propertyInfo =
                        type.GetProperties().FirstOrDefault(
                            info => info.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
                    if (propertyInfo != null)
                    {
                        if ((reader[i] != null) && (reader[i] != DBNull.Value))
                        {
                            propertyInfo.SetValue(entity, reader[i], null);
                        }
                        else
                        {
                            propertyInfo.SetValue(entity, Null.SetNull(propertyInfo), null);
                        }
                    }
                    else
                    {
                        var entityBase = entity;
                        if (entityBase != null) entityBase[fieldName] = reader[i];
                    }
                }
                catch (Exception ex)
                {

                    Logger.WriteLog(Logger.LogType.Error, "Cannot conver type of filed: [" + fieldName + "],Msg:" + ex.Message);

                    throw new Exception("Cannot conver type of filed: [" + fieldName + "],Msg:" + ex.Message);
                }
            }


        }
        private static void FillObjectFromReader(object _object, IDataReader reader)
        {
            if (_object == null) throw new ArgumentNullException("_object");
            if (_object is EntityBase)
            {
                var entity = _object as EntityBase;
                SetObjectValue(reader, ref entity);
            }
            else
            {
                HydrateObject(_object, reader);
            }
        }

        private static void HydrateObject(object _object, IDataReader reader)
        {
            int index;
            var mappingInfo = GetObjectMapping(_object.GetType());
            for (index = 0; index <= reader.FieldCount - 1; index++)
            {
                PropertyInfo propertyInfo;
                if (mappingInfo.Properties.TryGetValue(reader.GetName(index).ToUpperInvariant(), out propertyInfo))
                {
                    var propertyType = propertyInfo.PropertyType;
                    if (propertyInfo.CanWrite)
                    {
                        var dataValue = reader.GetValue(index);
                        var dataType = dataValue.GetType();
                        if (dataValue == DBNull.Value)
                        {
                            propertyInfo.SetValue(_object, Null.SetNull(propertyInfo), null);
                        }
                        else if (propertyType == dataType)
                        {
                            propertyInfo.SetValue(_object, dataValue, null);
                        }
                        else
                        {

                            if (propertyType.BaseType != null && propertyType.BaseType == typeof(Enum))
                            {
                                propertyInfo.SetValue(_object,
                                                      Regex.IsMatch(dataValue.ToString(), "^\\d+$")
                                                          ? Enum.ToObject(propertyType,
                                                                                 Convert.ToInt32(dataValue))
                                                          : Enum.ToObject(propertyType, dataValue), null);
                            }
                            else if (propertyType == typeof(Guid))
                            {
                                propertyInfo.SetValue(_object, Convert.ChangeType(new Guid(dataValue.ToString()), propertyType), null);
                            }
                            else if (propertyType == typeof(Version))
                            {
                                propertyInfo.SetValue(_object, new Version(dataValue.ToString()), null);
                            }
                            else if (propertyType == dataType)
                            {
                                propertyInfo.SetValue(_object, dataValue, null);
                            }
                            else
                            {
                                propertyInfo.SetValue(_object, Convert.ChangeType(dataValue, propertyType), null);
                            }
                        }
                    }
                }
            }
        }
        public static EntityMappingInfo GetObjectMapping(Type type)
        {
            var map = new EntityMappingInfo { ObjectType = type.FullName, PrimaryKey = "", TableName = GetTableName(type) };
            foreach (var objProperty in type.GetProperties())
            {
                map.Properties.Add(objProperty.Name.ToUpperInvariant(), objProperty);
                map.ColumnNames.Add(objProperty.Name.ToUpperInvariant(), GetColumnName(objProperty));
            }
            return map;
        }
        private static string GetColumnName(PropertyInfo property)
        {
            return property.Name;
        }
        private static string GetTableName(Type type)
        {
            var tableName = string.Empty;
            if (string.IsNullOrEmpty(tableName))
            {
                tableName = type.Name;
                if (tableName.EndsWith("Info"))
                {
                    return tableName.Replace("Info", string.Empty);
                }
            }
            return tableName;
        }

        public static TObject FillObject<TObject>(IDataReader reader)
        {
            return (TObject)CreateObjectFromReader(typeof(TObject), reader, true);
        }
        public static TObject FillObject<TObject>(IDataReader reader, bool closeReader)
        {
            return (TObject)CreateObjectFromReader(typeof(TObject), reader, closeReader);
        }
        public static object FillObject(IDataReader reader, Type type)
        {
            return CreateObjectFromReader(type, reader, true);
        }
        public static object FillObject(IDataReader reader, Type type, bool closeReader)
        {
            return CreateObjectFromReader(type, reader, closeReader);
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace Mi.Common
{
    [DataContract]
    public class WcfResponseData : EntityBase
    {
        public WcfResponseData()
        {
            Success = false;
            Message = string.Empty;
            Data = string.Empty;
            TotalRow = 0;
            ErrorCode = 0;
        }

        #region create response methods
        public static WcfResponseData CreateErrorResponse(int errorCode, string message)
        {
            return new WcfResponseData
            {
                Success = false,
                Message = (string.IsNullOrEmpty(message) ? "Error!" : message),
                Data = "",
                TotalRow = 0,
                ErrorCode = errorCode
            };
        }
        public static WcfResponseData CreateErrorResponse(Exception ex)
        {
            return CreateErrorResponse(9998, ex.Message);
        }
        public static WcfResponseData CreateErrorResponse(string message)
        {
            return CreateErrorResponse(9997, message);
        }
        public static WcfResponseData CreateErrorResponseForInvalidRequest()
        {
            return CreateErrorResponse(9996, "Invalid request!");
        }

        public static WcfResponseData CreateSuccessResponse(string data, int totalRow)
        {
            return new WcfResponseData
            {
                Success = true,
                Message = "Success!",
                Data = data,
                TotalRow = totalRow,
                ErrorCode = 0
            };
        }
        public static WcfResponseData CreateSuccessResponse(string message)
        {
            return new WcfResponseData
            {
                Success = true,
                Message = message,
                Data = "",
                TotalRow = 0,
                ErrorCode = 0
            };
        }
        public static WcfResponseData CreateSuccessResponse()
        {
            return CreateSuccessResponse("Success!");
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="WcfResponseData"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember]
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets total row from return data
        /// </summary>
        /// <value>
        /// The total row from return data.
        /// </value>
        [DataMember]
        public int TotalRow { get; set; }

        /// <summary>
        /// Gets or sets Error code
        /// </summary>
        /// <value>
        /// The Error code.
        /// </value>
        [DataMember]
        public int ErrorCode { get; set; }

        #endregion
    }
}

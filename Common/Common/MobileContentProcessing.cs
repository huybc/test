﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using APICertificate;
using System.Threading;

namespace Mi.Common
{
    public class MobileContentProcessing
    {
        public static void ProcessContentForMobile(string content, out string shortContent, out string contentMobile, out string imageList, out int imageCount)
        {
            imageCount = 0;
            imageList = string.Empty;
            shortContent = string.Empty;
            contentMobile = string.Empty;

            if (string.IsNullOrEmpty(content)) return;
            try
            {
                string patternImage = @"(?<G>(?<=<img.*?src=[""'])[^""']*)+";
                string patternBr = @"(<br.*?>)+";
                string patternAllHtmlTags = @"(?:\s|\t|\n|\r|&nbsp;)*<\/?(?:(?:p\s+|div|table|tbody|tr|th|td|font|em|br|h1|h2|a|img|\??[a-z0-9\-]+\:[a-z0-9\-]+)[^>]*|p)>(?:\s|\t|\n|\r|&nbsp;)*|<\!--[\S\s]*-->";

                contentMobile = GetFileMP4(content, 320, 275);

                shortContent = Regex.Replace(content, patternAllHtmlTags, "<br />");
                shortContent = Regex.Replace(shortContent, patternBr, "<br />");

                Regex regImage = new Regex(patternImage, RegexOptions.Singleline);
                foreach (System.Text.RegularExpressions.Match matchImage in regImage.Matches(content))
                {
                    if (matchImage.Success)
                    {
                        imageList += "|" + matchImage.Groups["G"].Value;
                        imageCount++;
                    }
                }
                if (!string.IsNullOrEmpty(imageList)) imageList = imageList.Remove(0, 1);
            }
            catch (Exception ex)
            {
            }
        }
        private static string GetFileMP4(string text, int width, int height)
        {

            try
            {
                string tagVideo = "<video width=\"" + width + "\" height=\"" + height + "\" controls=\"controls\"><source src=\"{0}\" type=\"video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;\" /></video>";
                string reGexObject = "(<object([^\\<>]|\\s)*>((?!</object>).)*((?!</object>).)*\\<\\/object>)";
                string reGexEmbed = "(<embed[^\\<>]*)src\\s*=\\s*[\"|']?[^\"']+key=(((?!&amp;).)+)(&amp;[^\"']+)*[\"|']?[^\\<>]*>";
                Regex rGexBg = new Regex(reGexObject, RegexOptions.IgnoreCase | RegexOptions.Singleline);

                MatchCollection matchObject = rGexBg.Matches(text);

                string tagObject = string.Empty;
                string key = string.Empty;
                string token = string.Empty;
                string videoFile = string.Empty;
                foreach (System.Text.RegularExpressions.Match objectmatch in matchObject)
                {
                    tagObject = objectmatch.Groups[1].Value;
                    Regex rGexKey = new Regex(reGexEmbed, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    MatchCollection matchkey = rGexKey.Matches(tagObject);
                    foreach (System.Text.RegularExpressions.Match keymatch in matchkey)
                    {
                        if (keymatch.Groups[2].Value.Split('&').Length > 0)
                        {
                            key = keymatch.Groups[2].Value.Split('&')[0];
                        }
                        else
                            key = keymatch.Groups[2].Value;
                        token = Certificate.CreateSignature("", "");
                        videoFile = RequestURL(key, token);
                        text = text.Replace(tagObject, string.Format(tagVideo, videoFile));
                    }
                }
                return text;
            }
            catch
            {
                return text;
            }
        }
        private static string RequestURL(string key, string token)
        {
            string result = string.Empty;
            string strNewValue = "{\"key\":\"" + key + "\",\"token\":\"" + token + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://media.hosting.vcmedia.vn/sinfo.svc/videourl");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    StreamReader stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result.Replace("\\", "").Replace("\"", "");
        }
    }
}

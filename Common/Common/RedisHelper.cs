﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using ServiceStack.Redis;

namespace Mi.Common
{
    public class RedisHelper
    {
        private const int DefaultExpired = 24*60*60;
        private static readonly string RedisHost = AppSettings.GetString("RedisHost");
        private static readonly int RedisPort = AppSettings.GetInt32("RedisPort");
        private static readonly string RedisPassword = AppSettings.GetString("RedisPassword");

        private static RedisClient CreateRedisClient(long dbNumber)
        {
            return new RedisClient(RedisHost, RedisPort, string.IsNullOrEmpty(RedisPassword) ? null : RedisPassword, dbNumber);
        }

        #region Normal

        public static bool Add(long dbNumber, string key, object value)
        {
            bool result;
            using (var redisClient = CreateRedisClient(dbNumber))
            {
                result = redisClient.Add(key, value, DateTime.Now.AddSeconds(DefaultExpired));
            }
            return result;
        }
        public static bool Add(long dbNumber, string key, object value, DateTime expiresAt)
        {
            bool result;
            using (var redisClient = CreateRedisClient(dbNumber))
            {
                result = redisClient.Add(key, value, expiresAt);
            }
            return result;
        }

        public static T Get<T>(long dbNumber, string key)
        {
            T result;
            using (var redisClient = CreateRedisClient(dbNumber))
            {
                result = redisClient.Get<T>(key);
            }
            return result;
        }

        public static object Get(long dbNumber, string key)
        {
            object result;
            using (var redisClient = CreateRedisClient(dbNumber))
            {
                result = redisClient.Get(key);
            }
            return result;
        }

        public static bool Remove(long dbNumber, string key)
        {
            bool result;
            using (var redisClient = CreateRedisClient(dbNumber))
            {
                result = redisClient.Remove(key);
            }
            return result;
        }

        #endregion
    }
}

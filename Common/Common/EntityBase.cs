﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;

namespace Mi.Common
{
    [DataContract]
    public abstract class EntityBase
    {
        private readonly Dictionary<string, object> _extendedProperties = new Dictionary<string, object>();

        public object this[string propertyName]
        {
            get
            {
                return (_extendedProperties.ContainsKey(propertyName) ? _extendedProperties[propertyName] : null);
            }
            set
            {
                if (_extendedProperties.ContainsKey(propertyName))
                {
                    _extendedProperties[propertyName] = value;
                }
                else
                {
                    _extendedProperties.Add(propertyName, value);
                }
            }
        }

        public static bool SetObjectValue<T>(IDataRecord reader, ref T entity) where T : class
        {
            Type type = typeof(T);

            if (type.BaseType == typeof(EntityBase) && entity != null)
            {
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var fieldName = reader.GetName(i);
                    try
                    {

                        var propertyInfo =
                            type.GetProperties().FirstOrDefault(
                                info => info.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
                        if (propertyInfo != null)
                        {
                            if ((reader[i] != null) && (reader[i] != DBNull.Value))
                            {
                                propertyInfo.SetValue(entity, reader[i], null);
                            }
                            else
                            {
                                if (propertyInfo.PropertyType == typeof(DateTime) ||
                                    propertyInfo.PropertyType == typeof(DateTime?))
                                {
                                    propertyInfo.SetValue(entity, DateTime.MinValue, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(string))
                                {
                                    propertyInfo.SetValue(entity, string.Empty, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(bool) ||
                                    propertyInfo.PropertyType == typeof(bool?))
                                {
                                    propertyInfo.SetValue(entity, false, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(decimal) ||
                                    propertyInfo.PropertyType == typeof(decimal?))
                                {
                                    propertyInfo.SetValue(entity, decimal.Zero, null);
                                }
                                else if (propertyInfo.PropertyType == typeof(double) ||
                                propertyInfo.PropertyType == typeof(double?))
                                {
                                    propertyInfo.SetValue(entity, double.Parse("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(float) ||
                                     propertyInfo.PropertyType == typeof(float?))
                                {
                                    propertyInfo.SetValue(entity, 0, null);
                                }



                                //hiephv---------------------------------------------------------------------------
                                else if (propertyInfo.PropertyType == typeof(ushort) ||
                                                         propertyInfo.PropertyType == typeof(ushort?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToUInt16("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(uint) ||
                                                     propertyInfo.PropertyType == typeof(uint?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToInt32("0"), null);
                                }

                                else if (propertyInfo.PropertyType == typeof(ulong) ||
                                                 propertyInfo.PropertyType == typeof(ulong?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToInt64("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(long) ||
                                propertyInfo.PropertyType == typeof(long?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToInt64("0"), null);
                                }

                                else if (propertyInfo.PropertyType == typeof(char) ||
                                    propertyInfo.PropertyType == typeof(char?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToChar(""), null);
                                }

                                else if (propertyInfo.PropertyType == typeof(short) || propertyInfo.PropertyType == typeof(short?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToInt16("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(byte) || propertyInfo.PropertyType == typeof(byte?))
                                {
                                    propertyInfo.SetValue(entity, Convert.ToByte("0"), null);
                                }
                                else if (propertyInfo.PropertyType == typeof(Guid) || propertyInfo.PropertyType == typeof(Guid?))
                                {
                                    propertyInfo.SetValue(entity, Guid.Parse(""), null);
                                }
                                else
                                {
                                    propertyInfo.SetValue(entity, 0, null);
                                }

                            }
                        }
                        else
                        {
                            (entity as EntityBase)[fieldName] = reader[i];
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Cannot conver type of filed: ["+ fieldName+"],Msg:"+ ex.Message);
                        throw new Exception("Cannot conver type of filed " + fieldName,ex);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

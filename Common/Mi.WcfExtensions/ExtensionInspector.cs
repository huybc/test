﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace Mi.WcfExtensions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ExtensionInspector : Attribute, IDispatchMessageInspector, IClientMessageInspector, IEndpointBehavior, IServiceBehavior
    {
        #region Implementation of IDispatchMessageInspector

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            var header = request.Headers.GetHeader<WcfMessageHeader>(WcfMessageHeader.MessageName, WcfMessageHeader.MessageNamespace);

            //if (header != null && header.SecretKey == AppConfigs.CmsApiSecretKey)
            if (header != null && AppConfigs.IsValidClientSecretKey(header.Namespace, header.SecretKey))
            {
                OperationContext.Current.IncomingMessageProperties.Add(WcfMessageHeader.MessageName, header);
            }
            else
            {
                throw new Exception("Access denied!");
            }

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
        }

        #endregion

        #region Implementation of IClientMessageInspector

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            var username = "";
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.User != null)
                {
                    username = HttpContext.Current.User.Identity.Name;
                }
            }
            catch
            {
            }
            var headerData = new WcfMessageHeader
            {
                SecretKey = AppConfigs.CmsApiSecretKey,
                Namespace = AppConfigs.CmsApiNamespace,
                ClientUsername = username
            };

            var typedHeader = new MessageHeader<WcfMessageHeader>(headerData);
            var untypedHeader = typedHeader.GetUntypedHeader(WcfMessageHeader.MessageName, WcfMessageHeader.MessageNamespace);

            request.Headers.Add(untypedHeader);
            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        #endregion

        #region Implementation of IEndpointBehavior

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            var channelDispatcher = endpointDispatcher.ChannelDispatcher;
            if (channelDispatcher == null) return;
            foreach (var endPoint in channelDispatcher.Endpoints)
            {
                endPoint.DispatchRuntime.MessageInspectors.Add(new ExtensionInspector());
            }

            ((BasicHttpBinding)endpoint.Binding).MaxBufferPoolSize = Int64.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).MaxBufferSize = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).MaxReceivedMessageSize = Int32.MaxValue;

            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxArrayLength = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxDepth = Int32.MaxValue;
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            var inspector = new ExtensionInspector();
            clientRuntime.MessageInspectors.Add(inspector);

            ((BasicHttpBinding)endpoint.Binding).MaxBufferPoolSize = Int64.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).MaxBufferSize = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).MaxReceivedMessageSize = Int32.MaxValue;

            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxArrayLength = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
            ((BasicHttpBinding)endpoint.Binding).ReaderQuotas.MaxDepth = Int32.MaxValue;
        }

        #endregion

        #region Implementation of IServiceBehavior

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
            foreach (var serviceEndpoint in serviceDescription.Endpoints)
            {
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferPoolSize = Int64.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferSize = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxReceivedMessageSize = Int32.MaxValue;

                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxArrayLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxDepth = Int32.MaxValue;
            }
            foreach (var serviceEndpoint in endpoints)
            {
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferPoolSize = Int64.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferSize = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxReceivedMessageSize = Int32.MaxValue;

                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxArrayLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxDepth = Int32.MaxValue;
            }
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var endPoint in serviceHostBase.ChannelDispatchers.Cast<ChannelDispatcher>().SelectMany(channelDispatcher => channelDispatcher.Endpoints))
            {
                endPoint.DispatchRuntime.MessageInspectors.Add(new ExtensionInspector());
            }
            foreach (var serviceEndpoint in serviceDescription.Endpoints)
            {
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferPoolSize = Int64.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxBufferSize = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).MaxReceivedMessageSize = Int32.MaxValue;

                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxArrayLength = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
                ((BasicHttpBinding)serviceEndpoint.Binding).ReaderQuotas.MaxDepth = Int32.MaxValue;
            }
        }

        #endregion
    }
}

﻿using Mi.Common;
using Mi.Common.ChannelConfig;

namespace Mi.WcfExtensions
{
    public class AppConfigs
    {
        public static string CmsApiNamespace
        {
            get { return AppSettings.GetString("CmsApi.Namespace"); }
        }
        public static string CmsApiSecretKey
        {
            get { return Utility.CreateMD5Checksum(AppSettings.GetString("CmsApi.SecretKey")); }
        }
        public static bool IsValidClientSecretKey(string channelNamespance, string clientSecretKey)
        {
            var sec = Utility.CreateMD5Checksum(ServiceChannelConfiguration.GetSecretKey(channelNamespance));
            return sec == clientSecretKey;
        }
    }
}

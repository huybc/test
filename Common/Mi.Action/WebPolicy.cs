﻿using System;
using System.Web;
using Mi.Action.Core;
using Mi.Common;
using Mi.Entity.Base.Security;


namespace Mi.Action
{
    public class WebPolicy : PolicyProvider
    {
        public const string DateTimeFormat = "MM/dd/yyyy HH:mm:ss";

        public override string GetAccountName()
        {
            try
            {
                return HttpContext.Current.User.Identity.Name;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        public override EnumPermission GetCurrentRole()
        {
            try
            {
                //Todo
                var currentRole = HttpContext.Current.Session["CurrentRole"];
                if (null == currentRole)
                {
                    var maxRole = EnumPermission.ArticleReporter;
                    if (Role.HasRoleOnPermission(EnumPermission.ArticleAdmin, GetAccountName()))
                    {
                        maxRole = EnumPermission.ArticleAdmin;
                    }
                    else if (Role.HasRoleOnPermission(EnumPermission.ArticleEditor, GetAccountName()))
                    {
                        maxRole = EnumPermission.ArticleEditor;
                    }
                    HttpContext.Current.Session["CurrentRole"] = maxRole;
                    return maxRole;
                }
                return (EnumPermission)currentRole;
            }
            catch (Exception ex)
            {
                return EnumPermission.ArticleReporter;
            }
        }
        public override string GetEncryptAccountName()
        {
            try
            {
                return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        public override string GenCookieAuthenticate(string username)
        {
            return string.Empty;
        }
        public override string GenCookieAuthenticate(string username, EnumPermission permission)
        {
            return string.Empty;
        }
        public override bool IsLogin()
        {
            return HttpContext.Current.Request.IsAuthenticated &&
                   !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name);
        }
        public override AuthCookieData GetAuthenticate()
        {
            return null;
        }
        public override string GetCookieAuthenticate()
        {
            return string.Empty;
        }

        public override bool IsFullPermission()
        {
            return HttpContext.Current.Request.IsAuthenticated &&
                   !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) &&
                   Role.IsAdministrator(HttpContext.Current.User.Identity.Name);
        }

        public override string TwoFactorSecret
        {
            get
            {
                return (string)HttpContext.Current.Session["TwoFactorSecret"];
            }
            set
            {
                if (string.IsNullOrEmpty(Utility.ConvertToString(HttpContext.Current.Session["TwoFactorSecret"])))
                {
                    HttpContext.Current.Session.Add("TwoFactorSecret", value);
                }
                else
                {
                    HttpContext.Current.Session["TwoFactorSecret"] = value;
                }
            }
        }

        public override DateTime? LastLoginAttemptUtc
        {
            get
            {
                try
                {
                    return (DateTime?)HttpContext.Current.Session["LastLoginAttemptUtc"];
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                if (string.IsNullOrEmpty(Utility.ConvertToString(HttpContext.Current.Session["LastLoginAttemptUtc"])))
                {
                    HttpContext.Current.Session.Add("LastLoginAttemptUtc", value);
                }
                else
                {
                    HttpContext.Current.Session["LastLoginAttemptUtc"] = value;
                }
            }
        }
    }
}

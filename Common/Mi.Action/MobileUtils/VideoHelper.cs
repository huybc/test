﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using APICertificate;
using ICore.Common;
using ICore.Common.ChannelConfig;

using Match = System.Text.RegularExpressions.Match;

namespace IWeb.Action.MobileUtils
{
    public class VideoHelper
    {
        private static readonly string FormatApiGetEmbedByKey = CmsChannelConfiguration.GetAppSetting(Constants.API_VIDEO_GET_EMBED_URL_BYKEY) + "?token={0}&key={1}";

        #region Lấy File MP4
        public static string ReplaceFlashObject(string sourceText, int videoWidth, int videoHeight)
        {
            if (!string.IsNullOrEmpty(sourceText))
            {
                try
                {
                    string tagVideo = "<video width=\"" + videoWidth + "\" height=\"" + videoHeight + "\" controls=\"controls\"><source src=\"{0}\" type=\"video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;\" data=\"{1}\" /></video>";
                    string reGexObject = "<object[^>]*>((?!</object>).)*</object>";
                    string reGexEmbed = "<embed[^>]*src=\"[^\"]*key=([a-z0-9]+)&[^\"]*\"";
                    Regex rGexBg = new Regex(reGexObject, RegexOptions.IgnoreCase | RegexOptions.Singleline);

                    MatchCollection matchObject = rGexBg.Matches(sourceText);
                    if (matchObject.Count > 0)
                    {
                        string tagObject = string.Empty;
                        string key = string.Empty;
                        string token = string.Empty;
                        string videoFile = string.Empty;
                        foreach (Match objectmatch in matchObject)
                        {
                            tagObject = objectmatch.Groups[0].Value;
                            //Logger.WriteLog(Logger.LogType.Info, "\n match object: " + tagObject);
                            Regex rGexKey = new Regex(reGexEmbed, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            var matched = rGexKey.Match(tagObject);
                            if (matched.Success)
                            {
                                key = matched.Groups[1].Value;
                                //Logger.WriteLog(Logger.LogType.Info, "\n match key: " + key);

                                token = Certificate.CreateSignature("", "");
                                videoFile = RequestURL(key, token).Replace("\"", "");
                                if (!string.IsNullOrEmpty(videoFile) && videoFile.Length > 0)
                                {
                                    string encrypt = Crypto.EncryptForHTML(tagObject);
                                    sourceText = sourceText.Replace(tagObject, string.Format(tagVideo, videoFile, encrypt));
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, "[ ReplaceFlashObject error.] " + ex.Message + "\n" + ex.StackTrace);
                }

            }
            return sourceText;
        }

        public static string ReplaceHtml5VideoObject(string sourceText)
        {
            try
            {

                string reGexObject = "<video[^>]*>((?!</video>).)*</video>";
                string reGexEmbed = "<source[^>]*data\\s*=\\s*\"([a-zA-Z0-9_/]+)\"";
                Regex rGexBg = new Regex(reGexObject, RegexOptions.IgnoreCase | RegexOptions.Singleline);

                MatchCollection matchObject = rGexBg.Matches(sourceText);

                string tagObject = string.Empty;
                string data = string.Empty;
                foreach (Match objectmatch in matchObject)
                {
                    tagObject = objectmatch.Groups[0].Value;
                    Regex rGexKey = new Regex(reGexEmbed, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    var matched = rGexKey.Match(tagObject);
                    if (matched.Success)
                    {
                        data = matched.Groups[1].Value;

                        if (!string.IsNullOrEmpty(data))
                        {
                            string decrypt = Crypto.DecryptFromHTML(data);
                            sourceText = sourceText.Replace(tagObject, decrypt);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "[ ReplaceHtml5VideoObject error.] " + ex.Message + "\n" + ex.StackTrace);
            }
            return sourceText;
        }

        private static string RequestURL(string key, string token)
        {
            string invokeParameters = string.Format("{{\"key\":\"{0}\",\"token\":\"{1}\"}}", key, token);
            string responseText = "";
            try
            {
                var parameterInBytes = Encoding.UTF8.GetBytes(invokeParameters);

                var httpRequest = (HttpWebRequest)WebRequest.Create(CmsChannelConfiguration.GetAppSetting(Constants.API_VIDEO_GET_EMBED_URL_BYKEY));
                httpRequest.ContentType = "application/json; charset=utf-8";
                httpRequest.ContentLength = parameterInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(parameterInBytes, 0, parameterInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    responseText = streamReader.ReadToEnd();
                    streamReader.Close();
                }

                //Logger.WriteLog(Logger.LogType.Info, "[ Get mp4 video for key:" + key + " "+responseText);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "[ Get mp4 video for key:" + key + " error.] " + ex.Message + "\n" + ex.StackTrace);
            }

            return responseText.Replace("\\", ""); ;
        }


        #endregion
    }
}

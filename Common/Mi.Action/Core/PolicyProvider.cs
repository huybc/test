﻿using System;
using System.Configuration.Provider;
using Mi.Entity.Base.Security;

namespace Mi.Action.Core
{
    public abstract class PolicyProvider : ProviderBase
    {
        public abstract string GetAccountName();
        public abstract EnumPermission GetCurrentRole();
        public abstract string GetEncryptAccountName();
        public abstract string GenCookieAuthenticate(string username);
        public abstract string GenCookieAuthenticate(string username, EnumPermission permission);
        public abstract bool IsLogin();
        public abstract bool IsFullPermission();
        public abstract AuthCookieData GetAuthenticate();
        public abstract string GetCookieAuthenticate();
        public abstract string TwoFactorSecret { get; set; }
        public abstract DateTime? LastLoginAttemptUtc { get; set; }
    }
}

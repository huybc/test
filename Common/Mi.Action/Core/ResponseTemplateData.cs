﻿namespace Mi.Action.Core
{
    public class ResponseTemplateData
    {
        private bool _success = false;
        private string _message = string.Empty;
        private object _data = null;
        private string _contentTemplate = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ResponseData"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public int CurrentRole { get; set; }

        public string ContentTemplate
        {
            get { return _contentTemplate; }
            set { _contentTemplate = value; }
        }
    }
}

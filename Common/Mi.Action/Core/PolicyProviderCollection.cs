﻿using System.Configuration.Provider;

namespace Mi.Action.Core
{
    public class PolicyProviderCollection : ProviderCollection
    {
        new public PolicyProvider this[string name]
        {
            get { return (PolicyProvider) base[name]; }
        }
    }
}

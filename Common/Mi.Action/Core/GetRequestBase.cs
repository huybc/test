﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Mi.Action.Core
{
    public abstract class GetRequestBase : IHttpHandler, IRequiresSessionState
    {
        #region process

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.AddHeader("Accept-Encoding", "gzip, deflate");
            var actionName = !string.IsNullOrEmpty(context.Request.QueryString["m"])
                                 ? context.Request.QueryString["m"].Trim().ToLower()
                                 : string.Empty;
            if (string.IsNullOrEmpty(actionName))
            {
                context.Response.Status = "404 not found";
                context.Server.Transfer("/404.aspx");
                context.Response.End();
                return;
            }
            var action = GetAction(actionName);
            if (action != null)
            {
                action.Do(context);
            }
        }

        #endregion

        #region private

        // Fields
        private static readonly IDictionary<string, ActionBase> Dicts = new Dictionary<string, ActionBase>();

        protected static void RegisterAction(string actionName, ActionBase action)
        {
            Dicts[actionName] = action;
        }

        protected static ActionBase GetAction(string actionName)
        {
            ActionBase action;
            if (Dicts.TryGetValue(actionName, out action))
            {
                return action;
            }
            return null;
        }


        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

﻿using System.Web;
using System.Web.SessionState;
using Mi.Common;

namespace Mi.Action.Core
{
    public abstract class ActionBase : IRequiresSessionState
    {
        protected abstract string ResponseContentType { get; }
        protected abstract bool IsResponseDataDirectly { get; }


        public string ModuleBasePath
        {
            get; set;
        }

        public static PolicyProvider Policy
        {
            get { return PolicyProviderManager.Provider; }
        }

        public void Do(HttpContext context)
        {
            var functionName = "";

            if (IsResponseDataDirectly)
            {
                functionName = !string.IsNullOrEmpty(context.Request.QueryString["f"])
                                   ? context.Request.QueryString["f"].ToLower()
                                   : "";
                // For post form
                if (string.IsNullOrEmpty(functionName))
                {
                    functionName = !string.IsNullOrEmpty(context.Request.Form["f"])
                                       ? context.Request.Form["f"].Trim().ToLower()
                                       : "";
                }
                // For post file
                if (string.IsNullOrEmpty(functionName))
                {
                    functionName = !string.IsNullOrEmpty(context.Request["f"])
                                       ? context.Request["f"].Trim().ToLower()
                                       : "";
                }
            }
            else
            {
                functionName = !string.IsNullOrEmpty(context.Request.QueryString["fn"])
                                      ? context.Request.QueryString["fn"].ToLower()
                                      : "";
                // For post form
                if (string.IsNullOrEmpty(functionName))
                {
                    functionName = !string.IsNullOrEmpty(context.Request.Form["fn"])
                                       ? context.Request.Form["fn"].Trim().ToLower()
                                       : "";
                }
                // For post file
                if (string.IsNullOrEmpty(functionName))
                {
                    functionName = !string.IsNullOrEmpty(context.Request["f"])
                                       ? context.Request["f"].Trim().ToLower()
                                       : "";
                }
            }
            
            var responseData = ProcessAction(functionName, context);

            context.Response.ContentType = string.IsNullOrEmpty(ResponseContentType) ? "text/plain; charset=utf-8" : ResponseContentType;
            if (IsResponseDataDirectly)
            {
                context.Response.Write(null != responseData ? responseData.ToString() : string.Empty);
            }
            else
            {
                var jSonString = NewtonJson.Serialize(responseData, "MM/dd/yyyy HH:mm:ss");
                if (!string.IsNullOrEmpty(context.Request.Params["callback"]))
                    jSonString = string.Format("{0}({1})", context.Request.Params["callback"], jSonString);
                context.Response.Write(jSonString);
            }
            context.Response.Flush();
            context.Response.End();
        }

        protected abstract object ProcessAction(string functionName, HttpContext context);
    }
}

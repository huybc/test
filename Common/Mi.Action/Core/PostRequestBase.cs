﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Mi.Action.Entity;
using Mi.Common;
using Mi.Common.ChannelConfig;

namespace Mi.Action.Core
{
    public abstract class PostRequestBase : IHttpHandler, IRequiresSessionState
    {
        public delegate void ProcessRequestDelegate(HttpContext ctx);

        #region process

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.AddHeader("Accept-Encoding", "gzip, deflate");
            var actionName = !string.IsNullOrEmpty(context.Request.Form["m"])
                                 ? context.Request.Form["m"].Trim().ToLower()
                                 : "file";
            if (string.IsNullOrEmpty(actionName))
            {
                context.Response.Status = "404 not found";
                context.Server.Transfer("/404.aspx");
                context.Response.End();
                return;
            }
            var action = GetAction(actionName);
            if (action != null)
            {
                action.Do(context);
            }			
            else
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32(Constants.CMS_SERVER_DEBUG_MODE) == 1)
                {
                    Logger.WriteLog(Logger.LogType.Error, "Action: " + actionName + " chưa được đăng ký!");
                    var msg = TemplateUtils.BuildServerErrorWithFormat("Action: " + actionName, "Chưa được đăng ký!");
                    context.Response.Write(NewtonJson.Serialize(new ResponseData { Content = msg, Success = true }));
                    context.Response.End();
                }
            }
        }

        #endregion

        #region ###

        // Fields
        private static readonly IDictionary<string, ActionBase> Dicts = new Dictionary<string, ActionBase>();

        protected static void RegisterAction(string actionName, ActionBase action)
        {
            Dicts[actionName] = action;
        }

        protected static ActionBase GetAction(string actionName)
        {
            ActionBase action;
            return Dicts.TryGetValue(actionName, out action) ? action : null;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        #endregion

        //public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        //{
        //    ProcessRequestDelegate prg = ProcessRequest;
        //    return prg.BeginInvoke(context, cb, extraData);
        //}

        //public void EndProcessRequest(IAsyncResult result)
        //{
        //}
    }
}

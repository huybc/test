﻿using System.Web;

namespace Mi.Action.Core
{
    public class ResponseData
    {
        //public static ResponseData CreateResponseData(WcfResponseData wcfResponseDate, string templatePath)
        //{
        //    return new ResponseData
        //    {
        //        Success = wcfResponseDate.Success,
        //        Message = wcfResponseDate.Message,
        //        Data = wcfResponseDate.Data,
        //        TotalRow = wcfResponseDate.TotalRow,
        //        ErrorCode = wcfResponseDate.ErrorCode,
        //        Content = TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
        //    };
        //}
        //public static ResponseData CreateResponseData(WcfResponseData wcfResponseDate)
        //{
        //    return new ResponseData
        //    {
        //        Success = wcfResponseDate.Success,
        //        Message = wcfResponseDate.Message,
        //        Data = wcfResponseDate.Data,
        //        TotalRow = wcfResponseDate.TotalRow,
        //        ErrorCode = wcfResponseDate.ErrorCode,
        //        Content = ""
        //    };
        //}
        public static ResponseData CreateResponseData()
        {
            return new ResponseData
            {
                Success = false,
                Message = "",
                Data = null,
                TotalRow = 0,
                ErrorCode = 0,
                Content = ""
            };
        }
        public static ResponseData CreateSuccessResponseData(object data, int totalRow, string templatePath)
        {
            return new ResponseData
            {
                Success = true,
                Message = "Success!",
                Data = data,
                TotalRow = totalRow,
                ErrorCode = 0,
                Content = string.IsNullOrEmpty(templatePath) ? "" : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
            };
        }
        public static ResponseData CreateSuccessResponseData(string message, string templatePath)
        {
            return new ResponseData
            {
                Success = true,
                Message = message,
                Data = null,
                TotalRow = 0,
                ErrorCode = 0,
                Content = string.IsNullOrEmpty(templatePath) ? "" : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
            };
        }
        public static ResponseData CreateErrorResponseData(int errorCode, string message, string templatePath)
        {
            return new ResponseData
            {
                Success = false,
                Message = "Error!",
                Data = null,
                TotalRow = 0,
                ErrorCode = errorCode,
                Content = string.IsNullOrEmpty(templatePath) ? "" : TemplateUtils.LoadPage(templatePath, HttpContext.Current.Request.Form)
            };
        }

        private bool _success = false;
        private string _message = string.Empty;
        private object _data = null;
        private string _content = string.Empty;
        private int _totalRow = 0;
        private int _errorCode = 0;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ResponseData"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public int TotalRow
        {
            get { return _totalRow; }
            set { _totalRow = value; }
        }

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

    }
}

﻿namespace Mi.Action.Entity
{
    public class Constants
    {
        public enum ErrorCode
        {
            Error = 1,
            Fatal = 2,
            Invalid = 3,
            Ok = 0
        }

        // ConnectionString
        public const string BOOKMARK_CONNECTION_STRING = "BookmarkConnectionStringName";
        public const string BOOKMARK_CONNECTION_DECRYPT_KEY = "DecryptKeyBookmarkConnectionString";

        #region SESSION

        public const string ASP_NET_SESSION_ID = "ASP.NET_SessionId";
        public const string SESSION_USERNAME = "SESSION_USERNAME_LOGIN";
        public const string AUTH_COOKIE_NAME = "_aspx_auth_";
        public const string AUTH_COOKIE_EXPIRE = "AUTH_COOKIE_EXPIRE";
        public const string AUTH_DECRYPT_ACCOUNT = "AUTH_DECRYPT_ACCOUNT";

        #endregion


        /*##############################################*/
        /*# Hostings */
        /*##############################################*/
        public const string HOST_BOOKMARK = "HOST_BOOKMARK";
        public const string HOST_STATIC = "HOST_STATIC";
        public const string HOST_BOOKMARK_AVATAR = "HOST_BOOKMARK_AVATAR";
        public const string HOST_BOOKMARK_THUMB = "HOST_BOOKMARK_THUMB";


        /*##############################################*/
        /*# Settings */
        /*##############################################*/
        public const string EnabledSms = "EnabledSms";
        public const string AUTH_RIJNDEALKEY = "4022753ee9daa45e8ad5dec533b3e3d70";//32bit
        public const string CONNECTION_RIJNDEALKEY = "ab353651c4d14e5ba9e80180508948be";//32bit

        public const string ALLOW_CACHE_JS_CSS = "ALLOW_CACHE_JS_CSS";
        public const string CACHE_JS_CSS_EXPIRED = "CACHE_JS_CSS_EXPIRED";
        public const string CSS_WIDGET = "CSS_WIDGET";
        public const string JS_WIDGET = "JS_WIDGET";
        public const string JS_BOOKMARK = "JS_BOOKMARK";

        public const string CMS_BOOKMARK_VERSION = "CMS_BOOKMARK_VERSION";
        public const string CMS_BM_ELEMENT_WIDGET = "CMS_BM_ELEMENT_WIDGET";
        public const string CMS_BM_ELEMENT_AUTH_COOKIE = "_aspx_auth_";
        
        // Actions params

        public const string SECRET_KEY = "SECRET_KEY";

        public const string SURFIX_LOGIN = "SURFIX_LOGIN";

        public const string EXTERNAL_REQUEST_SECRET_KEY = "EXTERNAL_REQUEST_SECRET_KEY";

        #region Message

        public const string MESG_CAN_NOT_FOUND_ACTION = "Hành động của bạn không hợp lệ.";
        public const string MESG_LOAD_TEMPLATE_FAIL = "Không thể tải được template này.";
        public const string MESG_LOGIN_FAIL = "Đăng nhập không thành công";
        public const string MESG_TIMEOUT_SESSION = "Bạn chưa đăng nhập hoặc đã hết phiên làm việc.";
        public const string MESG_UPDATE_SUCCESS = "Cập nhật thành công.";
        public const string MESG_UPDATE_ERROR = "Có lỗi xảy ra trong quá trình xử lý. Hãy thử lại.";
        public const string MESG_RETPWD_SUCCESS = "Mật khẩu đã được thay đổi.";
        public const string MESG_SUCCESS = "Success.";
        public const string MESG_ERROR = "Error.";

        #endregion

        #region FileManager

        public const string FILE_MANAGER_HTTPSERVER = "FILE_MANAGER_HTTPSERVER";
        public const string FILE_MANAGER_HTTPDOWNLOAD = "FILE_MANAGER_HTTPDOWNLOAD";
        public const string FILE_MANAGER_HTTPUPLOAD = "FILE_MANAGER_HTTPUPLOAD";
        public const string FILE_MANAGER_SECRET_KEY = "FILE_MANAGER_SECRET_KEY";
        public const string FILE_MANAGER_PREFIX = "FILE_MANAGER_PREFIX";
        public const string FILE_MANAGER_INCLUDING_PREFIX = "FILE_MANAGER_INCLUDING_PREFIX";

        public const string FILE_MANAGER_SPLITCHAR = "FILE_MANAGER_SPLITCHAR";
        public const string FILE_MANAGER_UPLOAD_NAMESPACE = "FILE_MANAGER_UPLOAD_NAMESPACE";
        public const string FILE_MANAGER_UPLOAD_EXPIRED_TIME = "FILE_MANAGER_UPLOAD_EXPIRED_TIME";
        public const string FILE_MANAGER_UPLOAD_CALLBACK = "FILE_MANAGER_UPLOAD_CALLBACK";
        public const string FILE_MANAGER_UPLOAD_MAXLENGTH = "FILE_MANAGER_UPLOAD_MAXLENGTH";

        #endregion


        #region VideoManager

        public const string VIDEO_MANAGER_HTTPSERVER = "VIDEO_MANAGER_HTTPSERVER";
        public const string VIDEO_MANAGER_HTTPDOWNLOAD = "VIDEO_MANAGER_HTTPDOWNLOAD";
        public const string VIDEO_MANAGER_HTTPUPLOAD = "VIDEO_MANAGER_HTTPUPLOAD";
        public const string VIDEO_MANAGER_SECRET_KEY = "VIDEO_MANAGER_SECRET_KEY";
        public const string VIDEO_MANAGER_CLIENT_HASH = "VIDEO_MANAGER_CLIENT_HASH";
        public const string VIDEO_MANAGER_PREFIX = "VIDEO_MANAGER_PREFIX";
        public const string VIDEO_MANAGER_INCLUDING_PREFIX = "VIDEO_MANAGER_INCLUDING_PREFIX";

        public const string VIDEO_MANAGER_SPLITCHAR = "VIDEO_MANAGER_SPLITCHAR";
        public const string VIDEO_MANAGER_UPLOAD_NAMESPACE = "VIDEO_MANAGER_UPLOAD_NAMESPACE";
        public const string VIDEO_MANAGER_UPLOAD_EXPIRED_TIME = "VIDEO_MANAGER_UPLOAD_EXPIRED_TIME";
        public const string VIDEO_MANAGER_UPLOAD_CALLBACK = "VIDEO_MANAGER_UPLOAD_CALLBACK";
        public const string VIDEO_MANAGER_UPLOAD_MAXLENGTH = "VIDEO_MANAGER_UPLOAD_MAXLENGTH";

        public const string VIDEO_MANAGER_TOKEN_GET_PATH = "APiGetVideoPath.TokenKey";

        /* API VideoManager */
        public const string API_VIDEO_CONVERT_MP4 = "API_ConvertVideo";
        public const string API_VIDEO_RESIZE = "API_ResizeVideo";
        public const string API_VIDEO_CROP = "API_CropVideo";
        public const string API_VIDEO_GET_EMBED_CROP = "API_GetEmbedCropVideo";
        public const string API_VIDEO_CHECK_CONVERT = "API_GetUrl";
        public const string API_VIDEO_GET_EMBED_URL = "API_VideoEmbedUrl";


        #endregion
    }
}

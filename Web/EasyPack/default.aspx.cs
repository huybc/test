﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Golden.Core.Helper;

namespace Golden
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                Page.Title = UIHelper.GetConfigByName("MetaTitle");
                Page.MetaDescription = UIHelper.GetConfigByName("MetaDescription");
                Page.MetaKeywords = UIHelper.GetConfigByName("MetaKeyword");


            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPackWBanner.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Golden._default" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<%=UIHelper.GetConfigByName("MetaTitle") %>" />
    <meta property="og:description" content="<%=UIHelper.GetConfigByName("MetaDescription") %>" />
    <meta property="og:image" content="https://EasyPack.vn/Uploads/<%=UIHelper.GetConfigByName("PageImage") %>" />
    
    <%--<script src="//widget.manychat.com/724769421221321.js" async="async"></script>--%>
    <style>
        .coach_text {
            height: 350px;
        }

            .coach_text .bot {
                word-spacing: 2px;
                line-height: 16px;
            }

        img-small {
            height: auto;
            width: 100%;
        }

        img-large {
            width: 100%;
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
<div style="position: relative">
    <h1 style="position: absolute; top: -999px; display:none"><%=UIHelper.GetConfigByName("H1HomePage") %></h1>
</div>
    <%// LAy danh sach san pham %>
    <%  var zone_SP = ZoneBo.GetZoneByAlias("san-pham");
        var listSanPham = NewsBo.GetByZoneId(zone_SP.Id, 6);
        var listSanPham2 = NewsBo.GetByZoneId(zone_SP.Id, 6);%>
    <section class="home-main-products py-5">
        <div class="container">
            <h2 class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No1") %>
            </h2>
            <div id="MainSection" class="row">
                <%foreach (var item in listSanPham)
                    {  %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12 align-self-sm-end home-item">
                    <div class="item">
                        <div class="image text-center">
                            <a href="/san-pham/<%=item.Url %>.<%=item.Id %>.htm">
                                <img data-alt-src="/uploads/thumb/<%=item.Avatar2 == null ? item.Avatar : item.Avatar2 %>" src="/uploads/thumb/<%=item.Avatar %>?v=1.0" alt="<%=item.Title %>" class="img-fluid swap" /></a>
                        </div>
                        <div class="text-center">
                            <div class="cate-name">
                                <a href="/san-pham/<%=item.Url %>.<%=item.Id %>.htm"><h3><%=item.Title %></h3></a>
                            </div>
                            <div class="view-more">
                                <a href="/san-pham/<%=item.Url %>.<%=item.Id %>.htm" class="">Xem thêm<i class="fas fa-chevron-right ml-2"></i>
                                </a>
                            </div>
                            <div class="des">
                                <%=item.Sapo %>
                            </div>
                            <div>
                                <a href="/san-pham/<%=item.Url %>.<%=item.Id %>.htm" class="btn btn-order">Đặt hàng ngay</a>
                            </div>
                        </div>
                    </div>
                </div>

                <%} %>
            </div>
        </div>
    </section>
    <section class="home-features">
        <div class="container">
            <h2 class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No2") %>
            </h2>
            <div class="row justify-content-center mb-5">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text-center mb-5">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No2_Sapo") %>
                    </div>
                    <div class="video">
                        <iframe width="100%" height="450" src="<%=UIHelper.GetConfigByName("HomePageVideo1").Replace("watch?v=","embed/") %>"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <%//Lay danh sach stamp trang chu %>
            <%var listStamp = ConfigBo.AdvGetByType(102).ToList().OrderBy(r => r.SortOrder); %>
            <div class="row">
                <%if (listStamp.Any())
                    { %>
                <%foreach (var item in listStamp)
                    { %>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="item">
                        <div class="image text-center">
                            <a href="">
                                <img src="/uploads/thumb/<%=item.Thumb %>?v=1.0" alt="" class="img-fluid" /></a>
                        </div>
                        <div class="text">
                            <%=item.Content %>
                        </div>
                    </div>
                </div>
                <%} %>
                <%} %>
            </div>
        </div>
    </section>
    <section class="my-video">
        <div class="container">
            <h2 class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No3") %>
            </h2>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No3_Sapo") %>
                    </div>
                    <div class="video">
                        <iframe width="100%" height="450" src="<%=UIHelper.GetConfigByName("HomePageVideo2").Replace("watch?v=","embed/") %>"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <%var list2 = NewsBo.GetByZoneId(zone_SP.Id, 100).Where(r => r.IsHot == false).OrderByDescending(r => r.Id).Skip(0).Take(6).ToList(); %>
    <%if (list2.Count > 5)
        { %>
    <section class="other-products">
        <div class="container">
            <h2 class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No4") %>
            </h2>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No4_Sapo") %>
                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-12">
                    <div class="grid-images">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <%//Lay danh sach san pham khon phai hot %>

                                <div class="row">
                                    <%var list2_part1 = list2.OrderBy(r => r.Id).Skip(0).Take(3).ToList(); %>

                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[0].Url %>.<%=list2_part1[0].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part1[0].Avatar %>?v=1.0"
                                                        class="img-fluid img-small" alt="<%=list2_part1[0].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[0].Url %>.<%=list2_part1[0].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[1].Url %>.<%=list2_part1[1].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part1[1].Avatar %>?v=1.0"
                                                        class="img-fluid img-small" alt="<%=list2_part1[1].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[1].Url %>.<%=list2_part1[1].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part1[2].Url %>.<%=list2_part1[2].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part1[2].Avatar %>?v=1.0"
                                                        class="img-fluid img-large" alt="<%=list2_part1[2].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part1[2].Url %>.<%=list2_part1[2].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%var list2_part2 = list2.OrderBy(r => r.Id).Skip(3).Take(3).ToList(); %>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row flex-wrap-reverse">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[0].Url %>.<%=list2_part2[0].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part2[0].Avatar %>?v=1.0"
                                                        class="img-fluid img-small" alt="<%=list2_part2[0].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[0].Url %>.<%=list2_part2[0].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[1].Url %>.<%=list2_part2[1].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part2[1].Avatar %>?v=1.0"
                                                        class="img-fluid img-small" alt="<%=list2_part2[1].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[1].Url %>.<%=list2_part2[1].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="/san-pham/<%=list2_part2[2].Url %>.<%=list2_part2[2].Id %>.htm" title="">
                                                    <img src="/uploads/<%=list2_part2[2].Avatar %>?v=1.0"
                                                        class="img-fluid img-large" alt="<%=list2_part2[2].Title %>" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="/san-pham/<%=list2_part2[2].Url %>.<%=list2_part2[2].Id %>.htm" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%}
        else
        { %>
    <section class="other-products">
        <div class="container">
            <div class="heading-ss">
                <%=UIHelper.GetConfigByName("HomePageTitle_No4") %>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-10 col-sm-12 col-12">
                    <div class="text">
                        <%=UIHelper.GetConfigByName("HomePageTitle_No4_Sapo") %>
                    </div>

                </div>
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="grid-images">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-01.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-02.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-03.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="row flex-wrap-reverse">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-01.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-02.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="item">
                                            <div class="image">
                                                <a href="javascript:void(0)" title="">
                                                    <img src="/Themes/images/change/orther-pr-03.jpg"
                                                        class="img-fluid" alt="" /></a>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="btn">Đặt mua</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%} %>
    <section class="get-price">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-ss">
                        Bạn muốn đặt số lượng lớn?
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-9 col-sm-10 col-12">
                            <div class="text">
                                Liên hệ ngay với chúng tôi để nhận được giá tốt nhất
                            </div>
                            <div class="text-center">
                                <a href="/lien-he" role="button" class="btn mb-3">Yêu cầu báo giá</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script type="text/javascript">
        var sourceSwap = function () {
            var $this = $(this).find("img.swap");
            var newSource = $this.data('alt-src');
            $this.data('alt-src', $this.attr('src'));
            $this.attr('src', newSource);
        }
        $(function () {
            $('.home-item').hover(sourceSwap, sourceSwap);
        });

        
    </script>
</asp:Content>

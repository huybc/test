﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPack.Master" AutoEventWireup="true" CodeBehind="TinTucDetail.aspx.cs" Inherits="Golden.Pages.TinTucDetail" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Mi.Action.Core" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta property="fb:app_id" content="<%=UIHelper.GetConfigByName("FBAppID") %>" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (detail != null)
      { %>
        <meta property="og:title" content="<%= detail.Title %>" />
        <meta property="og:description" content="<%= detail.Sapo%>" />
        <meta property="og:image" content="https://EasyPack.vn/Uploads/<%= detail.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(detail.TitleSeo)?detail.TitleSeo:detail.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+detail.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=detail.DistributionDate %>",
                                    "dateModified":"<%=detail.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"repair"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"Repair.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=detail.Sapo %>"
                            }
    </script>
    <%//Lay thong tin bai viet %>
    <% var zone_tin_tuc = ZoneBo.GetZoneByAlias("tin-tuc"); %>
    <% var tinTucId = int.Parse(Page.RouteData.Values["id"].ToString()); %>
  
    <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                    <li class="breadcrumb-item"><a href="/tin-tuc">Tin tức</a></li>
                    <li class="breadcrumb-item active"><%=detail.Title %></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-12">
                <div class="news-detail">
                    <div class="d-flex mb-4">
                        <div class="time align-self-center">
                            <%=UIHelper.GetDateName(detail.CreatedDate) %> - <%=detail.CreatedDate.ToString("dd/MM/yyyy") %>
                        </div>
                        <div class="share d-flex ml-auto">
                            <span class="mr-2 align-self-center">Chia sẻ</span>
                            <a class="mr-2" href="">
                                <div class="fb-ic"></div>
                            </a>
                            <a class="mr-2" href="">
                                <div class="gg-ic"></div>
                            </a>
                        </div>
                    </div>
                    <section class="content-blog">
                        <h1 class="title"><%=detail.Title %></h1>
                        <div class="description">
                            <%=detail.Sapo %>
                        </div>

                        <div class="newsbody">
                            <%=detail.Body %>
                        </div>


                    </section>
                </div>

                <div class="list-news mb-4">
                    <div class="heading-ss-right heading-ss-right-2 mb-4">
                        <a title="" href="">Cùng chuyên mục</a>
                    </div>
                    <%var top5 = NewsBo.GetByZoneId(zone_tin_tuc.Id, 5).ToList(); %>
                    <%foreach (var item in top5)
                        { %> 
                        <div class="item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-12">
                                <div class="image">
                                    <a href="/tin-tuc/<%=item.Url %>.<%=item.Id %>.htm" title=""><img src="/uploads/thumb/<%=item.Avatar %>?v=1.0" class="img-fluid"
                                            alt="<%=item.Title %>" /></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-12">
                                <h2 class="title">
                                    <a href="/tin-tuc/<%=item.Url %>.<%=item.Id %>.htm" title=""><%=item.Title %></a>
                                </h2>
                                <div class="time">
                                    <div class="date">
                                        <i class="far fa-clock mr-2"></i><%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                    </div>
                                    <div class="cmt ml-4">
                                        <i class="fas fa-comment-alt mr-2"></i>1254
                                    </div>
                                </div>
                                <div class="text">
                                    <%=item.Sapo %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>

            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="row">
                    <div class="col-md-12 col-sm-8 col-12">
                        <div class="blog-ss-right mb-3">
                            <div class="heading-ss-right mb-4">
                                <a title="" href="">Tin đọc nhiều</a>
                            </div>
                            <%var top7 = NewsBo.GetByZoneId(zone_tin_tuc.Id, 7).ToList(); %>
                            <%for (int i = 0; i < top7.Count(); i++)
                                { %> 
                                <%if (i == 0)
                                    { %> 
                                    <div class="item-large ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="image">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><img src="/uploads/thumb/<%=top7[i].Avatar %>?v=1.0" alt="<%=top7[i].Title %>"
                                                    class="w-100"></a>
                                        </div>
                                        <h3 class="title">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><%=top7[i].Title %>
                                        </h3>
                                        <div class="detail">
                                            <%=top7[i].Sapo %>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <%} %>
                            <%if (i > 0)
                                { %> 
                                <div class="item-small">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 col-sm-4 col-4">
                                        <div class="image">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title="">
                                                <img src="/uploads/thumb/<%=top7[i].Avatar %>?v=1.0" alt="<%=top7[i].Title %>" class="img-fluid"></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-12 col-sm-8 col-8 pl-0 pl-md-3 pl-lg-0">
                                        <div class="text">
                                            <h3 class="title">
                                                <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><%=top7[i].Title %></a>
                                            </h3>
                                            <div class="time">
                                                <div><%=top7[i].CreatedDate.ToString("dd/MM/yyyy") %></div>
                                                <div class="ml-4">1242 bình luận</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <%} %>
                            <%} %>
                        </div>
                        <%--<div class="qc mb-5">
                            <a href="" title="">
                                <img src="/Themes//images/change/qc-01.jpg" class="img-fluid" />
                            </a>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

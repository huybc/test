﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;

namespace Golden.Pages
{
    public partial class SanPhamDetail : System.Web.UI.Page
    {
        public NewsEntity sanPham_Detail;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                sanPham_Detail = NewsBo.GetNewsDetailById(newsId);
                 
                 if (newsId > 0)
                 {
                     sanPham_Detail = NewsBo.GetNewsDetailById(newsId);
                     Page.Title = !string.IsNullOrEmpty(sanPham_Detail.TitleSeo) ? sanPham_Detail.TitleSeo : sanPham_Detail.Title;
                     Page.MetaKeywords = sanPham_Detail.MetaKeyword;
                     Page.MetaDescription = sanPham_Detail.MetaDescription;
                 }
                 else
                 {
                     sanPham_Detail = new NewsEntity();

                 }
            }

            
        }
    }
}
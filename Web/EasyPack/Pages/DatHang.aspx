﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPack.Master" AutoEventWireup="true" CodeBehind="DatHang.aspx.cs" Inherits="Golden.Pages.DatHang" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item active">Đặt hàng</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="request-code">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <form class="form-request">
                        <div class="heading">
                            <%=UIHelper.GetConfigByName("RequestLeftTitle") %>
                        </div>
                        <div class="text">
                            <%=UIHelper.GetConfigByName("RequestLeftBody") %>
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtHoTen" class="form-control" placeholder="Họ và tên" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtSoDienThoai" class="form-control" placeholder="Số điện thoại" />
                        </div>
                        <div class="form-group">
                            <input type="text" id="txtEmail" class="form-control" placeholder="Email" />
                        </div>
                        <%//Kiem tra querystring %>
                        <%var soLuong = Request.QueryString["soLuong"]; %>
                        <div class="form-group">
                            <input type="text" id="txtSoLuong" class="form-control" value="<%=soLuong %>" placeholder="Số lượng" />
                        </div>
                        <%//Xay  dung noi dung loi nhan %>
                        <%var sp_Id = Request.QueryString["id"];
                            var color_Id = Request.QueryString["mauSac"];
                            var kichThuoc = Request.QueryString["kichThuoc"];
                            var file = Request.QueryString["fileMau"];
                            var loiNhan = "";
                            var sp_detail = new NewsEntity();
                            var color_detail = new ZoneEntity();
                            var newLine = "\r\n";
                            if (sp_Id != null)
                            {
                                sp_detail = NewsBo.GetNewsDetailById(int.Parse(sp_Id));
                                
                            }
                            if(color_Id != null)
                                color_detail = ZoneBo.GetZoneById(int.Parse(color_Id));
                            loiNhan = String.Format("Tên sản phẩm: {0};{1}Kích thước: {2};{3}Màu sắc: {4};{5}Lời nhắn: ;{6}File mẫu: {7}", sp_detail.Title, newLine, kichThuoc, newLine, color_detail.Name, newLine,newLine,file);
                        %>

                        <div class="form-group">
                            <textarea type="text" id="txtLoiNhan" rows="4" class="form-control" placeholder="Lời nhắn"><%=loiNhan %></textarea>
                        </div>
                        <div class="form-group text-center">
                            <a href="javascript:void(0)" id="btn_request_customer" class="btn">Đặt hàng</a>
                        </div>

                    </form>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="text-request">
                        <div class="heading">
                            <%=UIHelper.GetConfigByName("RequestRightTitle") %>
                        </div>
                        <%=UIHelper.GetConfigByName("RequestRightBody") %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Pages/UI_Script/DatHang.js"></script>
</asp:Content>

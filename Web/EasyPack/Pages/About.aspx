﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPack.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Golden.Pages.About" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Mi.Action.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
        <div class="banner-about">
        <img src="/uploads/<%=UIHelper.GetConfigByName("AboutBanner1") %>" class="img-fluid" />
    </div>
    <%//Lay bai tin gioi thieu %>
    <%var zone_gioi_thieu = ZoneBo.GetZoneByAlias("gioi-thieu").Id;  %>
    <%var tinGioiThieu = NewsBo.GetByZoneId(zone_gioi_thieu, 1).FirstOrDefault(); %>
    <div class="about-text">        
        <div class="container">
            <div class="heading"><%=tinGioiThieu.Title %></div>
            <div class="border-bottom pb-4">
                <%=tinGioiThieu.Body %>
            </div>
            <div class="connect py-4">
                <div class="text-center mb-4">
                    Connect With Us
                </div>
                <div class="row justify-content-center social">
                    <div class=" col-md-2 col-sm-4 col-6">
                        <a href="" title=""><i class="fab fa-twitter mr-2"></i>Twitter</a>
                    </div>
                    <div class="col-md-2 col-sm-4 col-6">
                        <a href="" title=""><i class="fab fa-facebook-f mr-2"></i>Facebook</a>
                    </div>
                    <div class="col-md-2 col-sm-4 col-6">
                        <a href="" title=""><i class="fab fa-pinterest mr-2"></i>Pinterest</a>
                    </div>
                    <div class="col-md-2 col-sm-4 col-6">
                        <a href="" title=""><i class="fab fa-instagram mr-2"></i>Instagram</a>
                    </div>
                    <div class="col-md-2 col-sm-4 col-6">
                        <a href="" title=""><i class="far fa-envelope mr-2"></i>Email</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="banner-about">
        <a href=""><img src="/uploads/<%=UIHelper.GetConfigByName("AboutBanner2") %>" class="img-fluid" /></a>
    </div>
    <%//Lay danh sach bai detail cua tin gioi thieu %>
    <%var listDetails = ProjectDetailBo.GetByArticleId(tinGioiThieu.Id).ToList(); %>
    <div class="questions">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-lg-10 col-md-11 col-sm-12 col-12">
                    <%foreach (var item in listDetails)
                        { %> 
                        <div class="item">
                        <div class="heading">
                            <%=item.Title %>
                        </div>
                        <%=item.Content %>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </div>

    <div class="swiper-container about">
        <!-- Additional required wrapper -->
        <%//Danh sach anh slide trang about %>
        <div class="swiper-wrapper">
            <!-- Slides -->
            <%var listSlide = ConfigBo.AdvGetByType(104); %>
            <%foreach(var item in listSlide){ %> 
                <div class="swiper-slide">
                <img src="/uploads/thumb/<%=item.Thumb %>" class="w-100"/>
            </div>
            <%} %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

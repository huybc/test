﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPack.Master" AutoEventWireup="true" CodeBehind="TinTuc.aspx.cs" Inherits="Golden.Pages.TinTuc" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Mi.Action.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
        <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                    <li class="breadcrumb-item active">Tin tức</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-12">
                <div class="list-news mb-4">
                    <%//Lay danh sach bai viet %>
                    <%var zone_tin_tuc = ZoneBo.GetZoneByAlias("tin-tuc");
                        var page = Request.QueryString["page"] == null ? 1 : int.Parse(Request.QueryString["page"]);
                        var pageSize = 10;
                        var totalRow = 0;
                        var listTinTuc = NewsBo.SearchNews(string.Empty, "admin", zone_tin_tuc.Id.ToString(), DateTime.MinValue, DateTime.MaxValue, 0, 0, 3, 4, page, pageSize, ref totalRow).OrderByDescending(r => r.Id).ToList(); %>
                    <%foreach (var item in listTinTuc)
                        {  %> 
                        <div class="item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-12">
                                <div class="image">
                                    <a href="/tin-tuc/<%=item.Url %>.<%=item.Id %>.htm" title=""><img src="/uploads/thumb/<%=item.Avatar %>?v=1.0" class="img-fluid"
                                            alt="<%=item.Title %>" /></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-12">
                                <h2 class="title">
                                    <a href="/tin-tuc/<%=item.Url %>.<%=item.Id %>.htm" title=""><%=item.Title %></a>
                                </h2>
                                <div class="time">
                                    <div class="date">
                                        <i class="far fa-clock mr-2"></i><%=item.CreatedDate.ToString("dd/MM/yyyy") %>
                                    </div>
                                    <div class="cmt ml-4">
                                        <i class="fas fa-comment-alt mr-2"></i>1254
                                    </div>
                                </div>
                                <div class="text">
                                    <%=item.Sapo %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                   
                </div>
                <nav class="easypack-pagination mb-5">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#"><i class="fas fa-chevron-left"></i></a>
                        </li>
                        <%for (int i = 0; i < totalRow/10+1; i++)
                            { %> 
                        <%if (i == 0)
                            { %> 
                            <li class="page-item active"><a class="page-link" href="/tin-tuc">1</a></li>
                        <%} %>
                            <%if (i > 0)
                                { %> 
                            <li class="page-item"><a class="page-link" href="/tin-tuc?page=<%=i+1 %>"><%=i+1 %></a></li>
                        <%} %>
                        <%} %>
                        
                        <li class="page-item">
                            <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                        </li>
                    </ul>
                </nav>

            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="row">
                    <div class="col-md-12 col-sm-8 col-12">
                        <div class="blog-ss-right mb-3">
                            <div class="heading-ss-right mb-4">
                                <a title="" href="">Tin đọc nhiều</a>
                            </div>
                            <%//Lay 7 bai moi nhat %>
                            <%var top7 = NewsBo.GetByZoneId(zone_tin_tuc.Id, 7).ToList(); %>
                            <%for (int i = 0; i < top7.Count(); i++)
                                { %> 
                                <%if (i == 0)
                                    { %> 
                                    <div class="item-large ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="image">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><img src="/uploads/thumb/<%=top7[i].Avatar %>?v=1.0" alt="<%=top7[i].Title %>"
                                                    class="w-100"></a>
                                        </div>
                                        <h3 class="title">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><%=top7[i].Title %>
                                        </h3>
                                        <div class="detail">
                                            <%=top7[i].Sapo %>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <%} %>
                            <%if (i > 0)
                                { %> 
                                <div class="item-small">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 col-sm-4 col-4">
                                        <div class="image">
                                            <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title="">
                                                <img src="/uploads/thumb/<%=top7[i].Avatar %>?v=1.0" alt="<%=top7[i].Title %>" class="img-fluid"></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-12 col-sm-8 col-8 pl-0 pl-md-3 pl-lg-0">
                                        <div class="text">
                                            <h3 class="title">
                                                <a href="/tin-tuc/<%=top7[i].Url %>.<%=top7[i].Id %>.htm" title=""><%=top7[i].Title %></a>
                                            </h3>
                                            <div class="time">
                                                <div><%=top7[i].CreatedDate.ToString("dd/MM/yyyy") %></div>
                                                <div class="ml-4">1242 bình luận</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <%} %>
                            <%} %>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

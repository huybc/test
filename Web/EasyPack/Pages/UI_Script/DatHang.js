﻿$('#btn_datHang').off('click').on('click', function () {
    var id = $(this).data('id');
    var name = $(this).data('name');
    var kichThuoc = $("#cbx_kich_thuoc").val() == "" ? "" : $("#cbx_kich_thuoc").val();
    var mauSac = $("#cbx_color").val() == "" ? "" : $("#cbx_color").val();
    var soLuong = $('#txt_so_luong').val() == "" ? 0 : $('#txt_so_luong').val();
    var date = new Date();
    var dateString = moment(date).format("YYYY-MM-DD HH:mm:ss");
    var fileMau = $('#file_dinh_kem').val().split('\\').pop();
    if (fileMau != "")
        uploadFile(function () {
            var link = '/dat-hang?id=' + id + '&kichThuoc=' + kichThuoc + '&mauSac=' + mauSac + '&soLuong=' + soLuong + '&fileMau=' + fileMau;
            window.open(link);
        });
    //console.log(id, name, kichThuoc, mauSac, soLuong);
    var link = '/dat-hang?id=' + id + '&kichThuoc=' + kichThuoc + '&mauSac=' + mauSac + '&soLuong=' + soLuong + '&fileMau=' + fileMau;
    window.open(link);
})
$('#btn_request_customer').off('click').on('click', function () {
    var hoTen = $('#txtHoTen').val();
    var sdt = $('#txtSoDienThoai').val();
    var email = $('#txtEmail').val();
    var soLuong = $('#txtSoLuong').val();
    var loiNhan = $('#txtLoiNhan').val() + ";";
    var newLine = '\r\n';
    var obj = {
        name: hoTen,
        email: email,
        phoneNumber: sdt,
        note: loiNhan + newLine + 'Số lượng: ' + soLuong
    }
    R.Post({
        params: obj,
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save",
        success: function (res) {
            if (res.Success) {
                console.log(1);
                alert("Cám ơn bạn đã liên hệ!");
            }
            // $('.card-header').RLoadingModuleComplete();
        }
    });
})
$('#btnSendContact').off('click').on('click', function () {
    var hoTen = $('#txtName').val();
    var sdt = $('#txtPhoneNumber').val();
    var email = $('#txtEmail').val();
    //var soLuong = $('#txtSoLuong').val();
    var loiNhan = $('#txtNote').val();
    //var newLine = '\r\n';
    var obj = {
        name: hoTen,
        email: email,
        phoneNumber: sdt,
        note: loiNhan
    }
    R.Post({
        params: obj,
        module: "ui-action",
        ashx: 'modulerequest.ashx',
        action: "save",
        success: function (res) {
            if (res.Success) {
                console.log(1);
                alert("Cám ơn bạn đã liên hệ!");
            }
            // $('.card-header').RLoadingModuleComplete();
        }
    });
})
$('#file_dinh_kem').on('change', function () {
    var rep = "C:\\fakepath\\";
    var fileName = '';
    fileName = $(this).val().replace(rep, "");
    $('#file-selected').html(fileName);
})

function uploadFile(cb) {
    alert("Vào hàm upload");
    //var formData = new FormData();
    //var file = document.getElementById("file-dinh-kem").files[0];
    //for (var i = 0; i < file.length; i++)
    //    formData.append(file[i].name, file[i]);

    var fileUpload = $("#file_dinh_kem").get(0);
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        //files[i].name = name;
        data.append(files[i].name, files[i]);
    }

    $.ajax({
        url: "/FileUploadHandler.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            alert(result);
        },
        error: function (err) {
            alert(err.statusText)
        }
    })

}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/EasyPack.Master" AutoEventWireup="true" CodeBehind="HoiDap.aspx.cs" Inherits="Golden.Pages.HoiDap" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Mi.Action.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="easypack-breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item active">FAQs</li>
                </ol>
            </nav>
        </div>
    </div>
    <%//Lay danh sach zone cau hoi %>

    <%var listZone = ZoneBo.GetListZoneActiveByParentId(9); %>

    <div class="faqs-grid">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-7 col-12">
                    <div class="row">
                        <%foreach (var item in listZone)
                            { %>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="item-list-faqs">
                                <div class="topic">
                                    <a href="" title=""><%=item.Name %></a>
                                </div>
                                <%//Lay danh sach cau hoi %>
                                <%var listCauHoi = NewsBo.GetByZoneId(item.Id, 100); %>
                                <%if (listCauHoi != null)
                                    { %>
                                <ul class="list">
                                    <%foreach (var i in listCauHoi)
                                        { %>
                                    <li>
                                        <a href="/hoi-dap/<%=i.Url %>.<%=i.Id %>.htm" title=""><%=i.Title %></a>
                                    </li>
                                    <%} %>
                                </ul>
                                <%} %>
                            </div>
                        </div>
                        <%} %>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-5 col-12">
                    <div class="form-group form-search">
                        <input type="text" class="form-control" id="search_faq" placeholder="Tìm kiếm câu hỏi và ấn enter..." />
                        <i class="fas fa-search"></i>
                    </div>
                    <div class="faqs-right">
                        <div class="heading">
                            <%=UIHelper.GetConfigByName("FaqTitleLeft") %>
                        </div>
                        <div class="binding">
                            <%//Lay danh sach 9 cau hoi moi nhat  %>
                            <%var listCauHoiRight = NewsBo.GetTopNewestNews(); %>
                            <%foreach (var item in listCauHoiRight)
                                { %>
                            <div class="item-question">
                                <a href="/hoi-dap/<%=item.Url %>.<%=item.Id %>.htm"><%=item.Title %></a>
                            </div>
                            <%} %>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Pages/UI_Script/HoiDap.js"></script>
</asp:Content>

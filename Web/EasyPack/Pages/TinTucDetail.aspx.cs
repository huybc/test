﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Golden.Core.Helper;
using Mi.BO.Base.News;
using Mi.BO.Base.Product;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Product;

namespace Golden.Pages
{
    public partial class TinTucDetail : System.Web.UI.Page
    {
        public NewsEntity detail;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    detail = NewsBo.GetNewsDetailById(newsId);
                    Page.Title = !string.IsNullOrEmpty(detail.TitleSeo) ? detail.TitleSeo : detail.Title;
                    Page.MetaKeywords = detail.MetaKeyword;
                    Page.MetaDescription = detail.MetaDescription;
                }
                else
                {
                    detail = new NewsEntity();

                }
            }
        }
    }
}
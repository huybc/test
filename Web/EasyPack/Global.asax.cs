﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Golden
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            //// Code that runs on application startup
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterRoutes(RouteTable.Routes);
        }
        public Global()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            //  this.PostReleaseRequestState += new EventHandler(Global_PostReleaseRequestState);
        }
public static void RegisterRoutes(RouteCollection routes)
        {
            //Client
            //EasyPack
            routes.MapPageRoute("home", "home", "~/default.aspx");
            

            //routes.MapPageRoute("services", "services", "~/Pages/Service.aspx");
            //routes.MapPageRoute("serviceDetail", "services/{zoneName}/{title}.{id}", "~/Pages/ServiceDetail.aspx");
            //routes.MapPageRoute("list-news", "blog/{ZoneName}", "~/Pages/ListNews.aspx");
            //routes.MapPageRoute("coach", "coach", "~/Pages/coach.aspx");
            //routes.MapPageRoute("blog", "blog", "~/Pages/Blog.aspx");
            //routes.MapPageRoute("blog-detail", "blog/{ZoneName}/{title}.{id}", "~/Pages/BlogDetail.aspx");
            //routes.MapPageRoute("contact", "contact", "~/Pages/Contact.aspx");
            //routes.MapPageRoute("schedule", "schedule", "~/Pages/Schedule.aspx");
            routes.MapPageRoute("about", "gioi-thieu", "~/Pages/About.aspx");
            routes.MapPageRoute("san-pham-detail", "san-pham/{title}.{id}.htm", "~/Pages/SanPhamDetail.aspx");
            routes.MapPageRoute("dat-hang", "dat-hang", "~/Pages/DatHang.aspx");
            routes.MapPageRoute("doi-tac", "doi-tac", "~/Pages/DoiTac.aspx");
            routes.MapPageRoute("tin-tuc", "tin-tuc", "~/Pages/TinTuc.aspx");
            routes.MapPageRoute("hoi-dap", "hoi-dap", "~/Pages/HoiDap.aspx");
            routes.MapPageRoute("lien-he", "lien-he", "~/Pages/LienHe.aspx");
            routes.MapPageRoute("hoi-dap-detail", "hoi-dap/{title}.{id}.htm", "~/Pages/HoiDapDetail.aspx");
            routes.MapPageRoute("tin-tuc-detail", "tin-tuc/{title}.{id}.htm", "~/Pages/TinTucDetail.aspx");
            


            //vintravel
            //cms


            //client
            //routes.MapPageRoute("tours", "tour", "~/Pages/Tours.aspx");
            //routes.MapPageRoute("blog", "blog", "~/Pages/Blog.aspx");
            //routes.MapPageRoute("about", "about", "~/Pages/About.aspx");
            ////routes.MapPageRoute("blog-categories", "blog-categories.htm", "~/Pages/BlogCategories.aspx");
            //routes.MapPageRoute("blog-categories", "blog/{zoneName}", "~/Pages/BlogCategories.aspx");
            //routes.MapPageRoute("blog-detail", "blog/{zoneName}/{title}.{id}.htm", "~/Pages/BlogDetail.aspx");
            //routes.MapPageRoute("contact", "contact", "~/Pages/Contact.aspx");
            //routes.MapPageRoute("blog-detail", "blog.htm", "~/Pages/Blog.aspx");
            //routes.MapPageRoute("blog", "blog.htm", "~/Pages/Blog.aspx");




            //cms
            routes.MapPageRoute("Configs", "cpanel/configs.htm", "~/CMS/Pages/configs.aspx");
            routes.MapPageRoute("projects", "cpanel/projects.htm", "~/CMS/Pages/Projects.aspx");
            routes.MapPageRoute("servicev2", "cpanel/servicev2.htm", "~/CMS/Pages/Servicev2.aspx");
            routes.MapPageRoute("CMScoach", "cpanel/coach.htm", "~/CMS/Pages/Coach.aspx");
            routes.MapPageRoute("CMSschedule", "cpanel/schedule.htm", "~/CMS/Pages/Schedule.aspx");
            routes.MapPageRoute("customers", "cpanel/customers.htm", "~/CMS/Pages/Customers.aspx");
            routes.MapPageRoute("Dashbroad", "cpanel/dashbroad.htm", "~/CMS/Index.aspx");
            routes.MapPageRoute("Products", "cpanel/products", "~/CMS/Pages/Products.aspx");
            routes.MapPageRoute("ProductsAdd", "cpanel/product/add", "~/CMS/Pages/ProductEdit.aspx");
            routes.MapPageRoute("ProductsEdit", "cpanel/product/edit", "~/CMS/Pages/ProductEdit.aspx");
            routes.MapPageRoute("NewsAdd", "cpanel/news/add", "~/CMS/Pages/NewsEdit.aspx");
            routes.MapPageRoute("NewsEdit", "cpanel/news/edit", "~/CMS/Pages/NewsEdit.aspx");
            //routes.MapPageRoute("CMSNews", "cpanel/projects.htm", "~/CMS/Pages/Game.aspx");
            routes.MapPageRoute("CMSNewsV2", "cpanel/newsv2.htm", "~/CMS/Pages/NewsV2.aspx");

            //CMS EasyPack
            routes.MapPageRoute("SanPham", "cpanel/san-pham.htm", "~/CMS/Pages/SanPham.aspx");
            routes.MapPageRoute("KhachHang", "cpanel/khach-hang.htm", "~/CMS/Pages/KhachHang.aspx");
            routes.MapPageRoute("TinTuc", "cpanel/tin-tuc.htm", "~/CMS/Pages/TinTuc.aspx");
            routes.MapPageRoute("GioiThieu", "cpanel/gioi-thieu.htm", "~/CMS/Pages/GioiThieu.aspx");
            routes.MapPageRoute("HoiDap", "cpanel/hoi-dap.htm", "~/CMS/Pages/CauHoi.aspx");


            routes.MapPageRoute("Game", "cpanel/services.htm", "~/CMS/Pages/Services.aspx");
            routes.MapPageRoute("Media", "cpanel/media", "~/CMS/Pages/Media.aspx");
            routes.MapPageRoute("CMSPhoto", "cpanel/photo", "~/CMS/Pages/Photo.aspx");
            routes.MapPageRoute("Sliders", "cpanel/sliders-{type}.htm", "~/CMS/Pages/Sliders.aspx");
            routes.MapPageRoute("Sliders2", "cpanel/sliders.htm", "~/CMS/Pages/Sliders.aspx");
            routes.MapPageRoute("login", "cpanel/login.htm", "~/CMS/Login.aspx");
            routes.MapPageRoute("login1", "cpanel.htm", "~/CMS/Login.aspx");
            routes.MapPageRoute("zone", "cpanel/zone.htm", "~/CMS/Pages/Zone.aspx");
            routes.MapPageRoute("CMSservices", "cpanel/services.htm", "~/CMS/Pages/Servicev2.aspx");
            //routes.MapPageRoute("StystemUser", "system-user", "~/Pages/StystemUser.aspx");
            // Client
            //routes.MapPageRoute("ListNews2", "sp/{productType}.htm", "~/Pages/ProductType.aspx");
            //routes.MapPageRoute("NewsByTag", "tag/{tagName}.htm", "~/Pages/NewsByTag.aspx");

//routes.MapPageRoute("Contact", "lien-he.htm", "~/Pages/Contact.aspx");
            //routes.MapPageRoute("hoi-dap", "hoi-dap.htm", "~/Pages/FAQ.aspx");
            //routes.MapPageRoute("Recruitment", "tuyen-dung.htm", "~/Pages/Recruitment.aspx");
            //routes.MapPageRoute("doi-tac", "doi-tac.htm", "~/Pages/CoOperater.aspx");
            //routes.MapPageRoute("ShareHolders", "co-dong.htm", "~/Pages/ShareHolders.aspx");
            //routes.MapPageRoute("About", "gioi-thieu.htm", "~/Pages/About.aspx");
            //routes.MapPageRoute("Terms", "dieu-khoan-su-dung.htm", "~/Pages/Terms.aspx");
            //routes.MapPageRoute("Privacy", "bao-mat.htm", "~/Pages/Privacy.aspx");

            //routes.MapPageRoute("ListNews", "tin-tuc.htm", "~/Pages/ListNews.aspx");

            //routes.MapPageRoute("TuVan", "tin-tuc.htm", "~/Pages/ListNews.aspx");
            //routes.MapPageRoute("Project", "du-an.htm", "~/Pages/ListProjects.aspx");

            //routes.MapPageRoute("ProjectDetail", "du-an/{zoneName}/{title}.{id}.htm", "~/Pages/ProjectDetail.aspx");

            //routes.MapPageRoute("Articles4", "phan-tich-nhan-dinh/{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("ListProjects", "du-an.htm", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("NotFound", "not-found.htm", "~/Pages/NotFound.aspx");

            //routes.MapPageRoute("NewsDetail", "{title}-{id}.htm", "~/Pages/NewsDetail.aspx");
            //routes.MapPageRoute("NewsDetail2", "{zoneName}/{title}.{id}.htm", "~/Pages/NewsDetail.aspx");
            //routes.MapPageRoute("Articles1", "{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("Articles2", "du-an/{zoneName}", "~/Pages/Articles.aspx");
            //routes.MapPageRoute("Articles3", "tu-van/{zoneName}", "~/Pages/Articles.aspx");

            //routes.MapPageRoute("_log.gif", "_log.gif", "~/log.aspx");
        }
        private void Global_PostReleaseRequestState(object sender, EventArgs e)
        {
            string contentType = Response.ContentType; // Get the content type.

            // Compress only html and stylesheet documents.
            if (contentType == "text/html" || contentType == "text/css")
            {
                // Get the Accept-Encoding header value to know whether zipping is supported by the browser or not.
                string acceptEncoding = Request.Headers["Accept-Encoding"];

                if (!string.IsNullOrEmpty(acceptEncoding))
                {
                    // If gzip is supported then gzip it else if deflate compression is supported then compress in that technique.
                    if (acceptEncoding.Contains("gzip"))
                    {
                        // Compress and set Content-Encoding header for the browser to indicate that the document is zipped.
                        Response.Filter = new GZipStream(Response.Filter, CompressionMode.Compress);
                        Response.AppendHeader("Content-Encoding", "gzip");
                    }
                    else if (acceptEncoding.Contains("deflate"))
                    {
                        // Compress and set Content-Encoding header for the browser to indicate that the document is zipped.
                        Response.Filter = new DeflateStream(Response.Filter, CompressionMode.Compress);
                        Response.AppendHeader("Content-Encoding", "deflate");
                    }
                }
            }
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //var path = HttpContext.Current.Request.Url.AbsolutePath;
            //HttpContext context = ((HttpApplication)sender).Context;
            //if (path.ToLower().Contains("/templates/") || path.ToLower().Contains("/pages/"))
            //{
            //    context.Response.Write("Access denied");
            //    context.Response.End();
            //}
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();
            //  Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
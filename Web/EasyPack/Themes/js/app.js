
var swiper = new Swiper('.slide-customers .swiper-container', {
    direction: 'vertical',
    height:600,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  });

sliderAbout();
function sliderAbout(){
    var sliderAbout = new Swiper('.swiper-container.about', {
        slidesPerView: 4.5,
        spaceBetween: 0,
        loop:true,
          breakpoints: {
            320: {
             
            },
            567: {
                slidesPerView: 1.5,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 2.5,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }

    });
}



$(window).scroll(function () {
    if ($(this).scrollTop() >= 60) {        // If page is scrolled more than 50px
        $('.header').addClass("fixed");    // Fade in the arrow
    } else {
        $('.header').removeClass("fixed");   // Else fade out the arrow
    }
});
$('.box-bottom .header-b').click(function () {
    $('.box-bottom').toggleClass('hidden');
})
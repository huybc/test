﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Configs.aspx.cs" Inherits="Golden.CMS.Pages.Configs" %>

<%@ Import Namespace="Mi.Action" %>
<%@ Import Namespace="Golden.Core.Helper" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Modules/Config/Styles/style.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <style>
        .config-wrapper {
            height: 600px !important;
            overflow: auto !important;
            margin-bottom: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="config-sidebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Cấu hình website</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li data="all" class="item active"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Toàn hệ thống</span></a></li>
                            <li data="home" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Trang chủ</span></a></li>
                            <li data="dat-hang" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Đặt hàng</span></a></li>
                            <li data="doi-tac" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Đối tác</span></a></li>
                            <li data="gioi-thieu" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Giới thiệu</span></a></li>
                            <li data="cau-hoi" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Câu hỏi</span></a></li>
                            <li data="contact" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Liên hệ</span></a></li>
                            <%--<li data="partner" class="item"><a href="javascript:void(0)"><i class="icon-code position-left"></i><span>Customer partner</span></a></li>--%>

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">
                <!-- Content area -->
                <div class="page-header" id="config-main">

                    <%--                    <div class="panel-heading" id="header-page">
                        <div class="panel-title">
                            Cấu hình
                        </div>

                    </div>--%>
                    <div class="panel-body " id="rlist">
                        <div class="row">
                            <div id="config-wrapper">
                                <table class="table datatable-show-all dataTable no-footer">
                                    <%= TemplateUtils.LoadPage("\\CMS\\Modules\\Config\\Templates\\List.aspx",Request.Form) %>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <div id="IMSOverlayWrapper">
        <div id="IMSOverlayWrapperIn">
            <div id="IMSOverlayContent">
            </div>
            <span id="IMSOverlayClose"></span>
        </div>
    </div>
    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js"></script>

    <script src="/CMS/Themes/js/jquery.jqpagination.js"></script>
    <script src="/CMS/Themes/js/autosize.min.js"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js"></script>
     <script src="/CMS/Themes/js/ckeditor4.2/config.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/Config/Scripts/main.js?v=<%=UIHelper.Version %>"></script>
   
</asp:Content>

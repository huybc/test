﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.Zone;

namespace Golden.CMS.Pages
{
    public partial class Services : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Service).ToList().Where(it => it.Invisibled == false);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
        }
        public List<ZoneWithSimpleField> GetAllZoneWithTreeViewSimpleFields(bool getParentsOnly = false)
        {
            List<ZoneWithSimpleField> simplaFields = new List<ZoneWithSimpleField>();
            var zones = ZoneBo.GetAllZoneWithTreeView(getParentsOnly, (int)ZoneType.Service).Where(it => it.Invisibled == false);
            foreach (var zone in zones)
            {
                simplaFields.Add(new ZoneWithSimpleField
                {
                    Id = zone.Id,
                    Name = zone.Name.Trim().Replace("-- ", ""),
                    ShortURL = zone.ShortUrl,
                    RealName = zone.Name.Trim(),
                    ParentId = zone.ParentId
                });
            }
            return simplaFields;
        }
        public class ZoneWithSimpleField
        {
            public string Name { get; set; }
            public string ShortURL { get; set; }
            public string RealName { get; set; }
            public int Id { get; set; }
            public int ZoneId { get { return this.Id; } }
            public int ParentId { get; set; }
        }
    }
}
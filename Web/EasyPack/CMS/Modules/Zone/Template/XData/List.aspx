﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Golden.CMS.Modules.Zone.Template.XData.List" %>

<%@ Import Namespace="Golden.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
            <td class="w80 ">
                <a class="avatar">
                    <img class="thumb" src="<%#  UIHelper.Thumb_W(60,Eval("Avatar").ToString()) %>" width="60" />
                </a>
            </td>
            <td>
                <p>
                  <a target="_blank" style="color: black" href="/project/<%# Eval("ShortURL") %>"><%# Eval("Name") %></a>  - <b class="wysiwyg-color-green _zone" data-id="<%# Eval("ParentId") %>"></b>
                </p>
                <p>
                    <label>Tạo bởi:</label><span class="bold"><%# Eval("CreatedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>,
                                                        <label> Thay đổi bởi:</label><span class="bold"><%# Eval("ModifiedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("ModifiedDate")) %></time>
                </p>
            </td>
            <td>Thứ tự: <%# Eval("SortOrder") %></td>
            <td><%# ZoneStatus((int)Eval("Status")) %></td>
            <td class="text-center">
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right zone-action" data-id="<%# Eval("Id") %>">
                            <li class="_edit"><a href="javascript:void(0)"><i class="icon-pencil7"></i>Sửa</a></li>
                            <li data="1"  class="_active status"><a href="javascript:void(0)"><i class="icon-check"></i>Hiên thị</a></li>
                            <li   data="2" class="_un-active status"><a href="javascript:void(0)"><i class="icon-minus-circle2"></i>Ẩn</a></li>
                            <li  data="4" class="_delete status"><a href="javascript:void(0)"><i class="icon-bin"></i>Xóa</a></li>
                       </ul>
                    </li>
                </ul>
            </td>
        </tr>
           </ItemTemplate>
       </asp:repeater>
    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Golden.CMS.Modules.Customer.Template.Edit" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<style>
    .customer label.col-lg-4 { text-align: right }
</style>

<div class="tabbable customer" id="news-id">

    <div class=" _body" id="_body">
        <div class="col-lg-10" style="margin: 0 auto; float: none">
            <div class="form-group row" style="text-align: center"><b>Thông tin khách hàng</b></div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Họ tên<span class="text-danger">*</span></label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="namext" class="form-control" value="<%=_obj.FullName %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Email</label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="mailtxt" class="form-control" value="<%=_obj.Email %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>

            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Điện thoại<span class="text-danger">*</span></label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="phonetxt" class="form-control" value="<%=_obj.Mobile %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>

          
           <%-- <div class="form-group row">
                <label class="col-lg-2" style="text-align: right">Địa chỉ</label>
                <div class="col-lg-10">
                    <input type="text" name="basic" id="addresstxt" class="form-control" value="<%=_obj.Address %>" tabindex="1" required="required" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Công ty</label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="companytxt" class="form-control" value="<%=_obj.Firm %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Loại hình</label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="typetxt" class="form-control" value="<%=_obj.Type %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>

            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Dịch vụ</label>
                        <div class="col-lg-8">
                            <select class="form-control" id="servicedll" data-selected="<%=_obj.Service %>">
                                <option value="">- Chọn -</option>
                                <option value="Engineering">Engineering</option>
                                <option value="Drafting">Drafting</option>
                                <option value="Shopdrawing">Shopdrawing</option>
                                <option value="Others">Others</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Trạng thái</label>
                        <div class="col-lg-8">
                            <select class="form-control" id="statusddl" data-selected="<%=_obj.Status %>">
                                <option value="">- Chọn -</option>
                                <option value="Potential">Potential</option>
                                <option value="Contacted">Contacted</option>
                                <option value="Pending">Pending</option>
                                <option value="Ongoing">Ongoing</option>
                                <option value="Denied">Denied</option>
                                <option value="Others">Others</option>

                            </select>
                        </div>
                    </div>

                </div>

            </div>--%>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-4">Vị trí ứng tuyển</label>
                        <div class="col-lg-8">
                            <input type="text" name="basic" id="contacttxt" class="form-control" value="<%=_obj.Contactperson %>" tabindex="1" required="required" placeholder="">
                        </div>
                    </div>

                </div>

            </div>
            <div class="form-group row">
                <div class="form-group">
                    <label class="col-lg-2" style="text-align: right">Trình độ chuyên môn</label>
                    <div class="col-lg-10">
                        <textarea name="basic" id="notetxt" class="form-control"><%=_obj.Note %></textarea>
                    </div>
                </div>
            </div>
            <div class="text-center row" id="news-save">
                <button type="button" status="<%=(int)ZoneStatus.Temp %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>


            </div>
        </div>

    </div>
</div>

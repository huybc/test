﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Golden.CMS.Modules.Customer.Template.List" %>

<%@ Import Namespace="Golden.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
                    <td class="w50">
                        <div>
                           <label class="_hot">
                                <i class="icon-pencil7 edit position-static" data="<%# Eval("Id") %>"></i> #<%# Eval("Id") %>
                            </label>
                        </div>
                    </td>
            
                <td>
                <p>
                   <b> <%# Eval("FullName") %></b>
                </p>
                <p>
                    <label>Điện thoại:</label><%# Eval("Mobile") %>
                  
                </p> 
                <p>
                    <label>Mail:</label><%# Eval("Email") %>
                  
                </p>
                <p>
                    <label>Ngày tạo:</label><time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>
                </p>
            </td>
<%--             <td>
                <p>
                    <label>Cty:</label><%# Eval("Firm") %>
                 
                </p>
                 <p>
                    <label>Dịch vụ:</label><%# Eval("Service") %>
                 
                </p>
                <p>
                    <%# Eval("Address") %>
                </p>
            </td> --%>
                    <td>
                 <p>
                    <label>Ghi chú:</label><%# Eval("Note").ToString().Replace(";",";<br>") %>
                 
                </p>
               
            </td>
                    <td>
                        <label>File mẫu: </label><a href="/UploadMau/<%# Eval("Note").ToString().Split(';').Where(r => r.Contains("File mẫu: ")).FirstOrDefault() == null ? "" : Eval("Note").ToString().Split(';').Where(r => r.Contains("File mẫu: ")).FirstOrDefault().Replace("File mẫu: ","") %>" target="_blank"><%# Eval("Note").ToString().Split(';').Where(r => r.Contains("File mẫu: ")).FirstOrDefault() == null ? "" : Eval("Note").ToString().Split(';').Where(r => r.Contains("File mẫu: ")).FirstOrDefault().Replace("File mẫu: ","") %></a>
                    </td>
                    <%--<%if (Eval("Note").ToString().Split(';').Where(r => r.Contains("File mẫu: ")).FirstOrDefault() != null)
                        { %> 
                        <td></td>
                    <%} %>--%>
                    
            
        
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

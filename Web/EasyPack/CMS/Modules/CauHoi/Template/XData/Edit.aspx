﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Golden.CMS.Modules.CauHoi.Template.XData.Edit" %>


<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Golden.Core.Helper" %>


<div class="tabbable" id="news-id">
    <div class=" _body" id="_body">
        <div class="col-lg-9" style="padding-left: 0;">
            <div id="zone-content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Thông tin dịch vụ
                            </legend>
                        </fieldset>
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="titletxt" class="form-control" value="<%=_obj.NewsInfo.Title %>" tabindex="1" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Url<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="urltxt" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Tùy chỉnh đường dẫn tin bài">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Mô tả<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea id="Sapo" class="form-control" rows="3" placeholder="Nội dung ngắn gọn" tabindex="2"><%=_obj.NewsInfo.Sapo %></textarea>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="col-lg-2">Thông số<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Diện tích xây dựng</label>
                                        <input type="text" name="basic" id="hoursOfWorkTxt" class="form-control" value="<%=_obj.NewsInfo.HoursOfWork %>" tabindex="1" required="required" >
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Chủ đầu tư</label>
                                        <input type="text" name="basic" id="InvestTxt" class="form-control" value="<%= _obj.NewsInfo.Invest %>" tabindex="1" required="required" >
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Lượng sản phẩm</label>
                                        <input type="text" name="basic" id="surfaceAreaTxt" class="form-control" value="<%=_obj.NewsInfo.SurfaceArea %>" tabindex="1" required="required" >
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">T.gian hoàn thành</label>
                                        <input type="text" name="basic" id="budgetTxt" class="form-control" value="<%=_obj.NewsInfo.Budget %>" tabindex="1" required="required" >
                                    </div>

                                </div>
                            </div>--%>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Chi tiết<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <%--<button type="button" id="add-media"><i class="fa fa-camera"></i>Thêm media</button>--%>
                                    <textarea id="contentCkeditor" class="editor" rows="8" style="height: 500px" tabindex="3"><%=_obj.NewsInfo.Body %></textarea>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">SEO <i class="icon-stats-growth"></i></legend>
                            <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Url<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" name="basic" id="url" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Đường dẫn tối ưu cho bài viết">
                            </div>
                        </div>--%>

                            <div class="form-group">
                                <label class="col-lg-2">Meta title<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=_obj.NewsInfo.TitleSeo %>" tabindex="1" required="required" placeholder="Khai báo từ khóa với công cụ tìm kiếm">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Meta Keyword<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="maximum_characters" id="metakeywordtxt" class="form-control" value="<%=_obj.NewsInfo.MetaKeyword %>" placeholder="Khai báo các từ khóa với bộ máy tìm kiếm.">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2">Meta Description<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea name="maximum_characters" class="form-control" id="metadescriptiontxt" placeholder="Mô tả bài viết hiển thị trên công cụ tìm kiếm. Độ dài tối ưu độ dài khoảng [140-160] ký tự"><%=_obj.NewsInfo.MetaDescription %></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" style="padding-right: 0">
            <div id="sidebar">
                <div id="sidebar-content">
                    <div class="panel panel-flat">
                        <div class="panel-body">

                            <%-- <div class="col-lg-12 col-md-12 col-sm-12">--%>
                            <div class="form-group">
                                <label>Dịch vụ</label>
                                <select data-placeholder="Select your state" class="select" id="_zoneddl" style="width: 100%" tabindex="4" data="<%=_obj.NewsInfo.ZoneId %>">
                                    <option value="">-- Chọn --</option>
                                    <asp:repeater runat="server" id="ZoneRpt">
                                            <ItemTemplate>
                                                <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                            </ItemTemplate>
                                        </asp:repeater>
                                    <%--<option value='4532' data-parent="0">Has#Tag</option>--%>
                                </select>
                            </div>
                            <%-- <div class="form-group">
                                <label>Ngày đăng</label>
                                <input type="text" class="form-control" id="datetxt" placeholder="" tabindex="10" value="<%=String.Format("{0:dd/MM/yyyy HH:mm:ss tt}", _obj.DistributionDate) %>" data="<%=String.Format("{0:yyyy/MM/dd HH:mm:ss tt}", _obj.DistributionDate) %>">
                            </div>--%>
                            <div class="form-group text-left">
                                <label class="checkbox-inline checkbox-left">
                                    <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                    Nổi bật
								
                                </label>
                                <label class="checkbox-inline checkbox-left">
                                    <input <%=_obj.NewsInfo.IsVideo?"checked":"" %> type="checkbox" tabindex="8" id="videocb">
                                    Tin video
								
                                </label>
                                <label class="checkbox-inline checkbox-left">
                                    <input type="checkbox" tabindex="9" id="commentcb" <%=_obj.NewsInfo.IsAllowComment?"checked":"" %>>Đóng bình luận
								
                                </label>
                            </div>
                            <%-- <div class="form-group">
                                            <label>Tài liệu đính kèm</label>
                                            <a href="#">[Đính kèm]</a>

                                        </div>--%>
                            <div class="form-group" id="attack-thumb">
                                <div>
                                    <label>Ảnh đại diện</label>
                                </div>
                                <%--<a id="attack-thumb" href="#">
                                    <img data="<%=_obj.Avatar %>" src="<%= UIHelper.Thumb_W(70,_obj.Avatar) %>" width="70" /></a>--%>
                                <div class="avatar">
                                    <input accept="image/*" id="fileSingleupload" type="file" name="files[]" style="display: none" />
                                    <label for="fileSingleupload">
                                        <%if (_obj.NewsInfo.Id > 0)
                                            {%>
                                        <img data-img="<%=_obj.NewsInfo.Avatar %>" src="/<%=!string.IsNullOrEmpty(_obj.NewsInfo.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.NewsInfo.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                        <%}
                                            else
                                            {%>
                                        <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                                        <% } %>
                                    </label>
                                </div>
                            </div>
                            <div class="text-right" id="news-save">
                                <button type="button" status="<%=(int)NewsStatus.Temporary %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="button" status="<%=(int)NewsStatus.Published %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-paperplane position-left"></i>Đăng bài</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>
                            </div>

                            <%--</div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


﻿using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Golden.CMS.Modules.CauHoi.Template.XData
{
    public partial class Edit : PageBase
    {
        public NewsDetailForEditEntity _obj = new NewsDetailForEditEntity();
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0L);
            _obj = NewsBo.GetDetail(id, PolicyProviderManager.Provider.GetAccountName());
            if (id <= 0)
            {
                _obj = new NewsDetailForEditEntity
                {
                    NewsInfo = new NewsEntity()
                };
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Faq).ToList().Where(it => it.Status == 1).Where(it => it.Invisibled == false);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
        }
    }
}
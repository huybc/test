﻿using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Golden.CMS.Modules.CauHoi.Template.XData
{
    public partial class List : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            var listZoneId = ZoneBo.GetListZoneByParentId(9, 203);

            var list = "";
            for (int i = 0; i < listZoneId.Count(); i++) {
                if (i < listZoneId.Count() - 1)
                    list += listZoneId[i].Id + ",";
                else
                    list += listZoneId[i].Id;
            }


            string title = GetQueryString.GetPost("title", string.Empty);
            var fromDate = GetQueryString.GetPost("from", DateTime.MinValue);
            var todate = GetQueryString.GetPost("to", DateTime.MaxValue);
            var pageIndex = GetQueryString.GetPost("pageindex", 1);
            var pagesize = GetQueryString.GetPost("pagesize", 10);
            var keyWord = GetQueryString.GetPost("keyword", String.Empty);
            var zoneId = GetQueryString.GetPost("zone", list);
            var status = GetQueryString.GetPost("status", (int)NewsStatus.All);
            var username = GetQueryString.GetPost("account", PolicyProviderManager.Provider.GetAccountName());
            var order = GetQueryString.GetPost("order", (int)NewsSortExpression.DistributionDateDesc);
            var newsType = GetQueryString.GetPost("type", (int)NewsType.All);



            var result = NewsBo.SearchNews(keyWord, username, zoneId, fromDate, DateTime.MaxValue, NewsFilterFieldForUsername.CreatedBy, NewsSortExpression.CreatedDateDesc,
                status, 4, pageIndex, pagesize, ref TotalRows);



            DataRpt.DataSource = result;
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }
    }
}
﻿using System;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Action.Core;

namespace Golden.CMS.Modules.Config.Templates
{
    public partial class Edit : PageBase
    {
        public ConfigEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var key = GetQueryString.GetPost("key", string.Empty);
            obj = ConfigBo.GetByConfigName(key);
        }
    }
}
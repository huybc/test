﻿R.Slider = {
    Init: function () {
        R.Slider.RegisterEvents();
        this.html = '<tr class="row-item"><td><input type="text" class="name form-control" placeholder="Tiêu đề" /><textarea style="margin-top: 10px" class="description form-control" placeholder="Mô tả" ></textarea>' +
            '</td><td><label for="fileSingleupload' + randomNumber + '"><input accept="image/*" id="fileSingleupload' + randomNumber +'" multiple type="file" name="files[]" style="display: none" /><img src="/CMS/Themes/images/cover.jpg?w=100" /></label>' +
          '</td><td><input type="text" style="width: 100%" class="url form-control" value="#"/></td>' +
          '<td style="width: 100px"><input type="text" class="sort form-control" /></td><td style="width: 40px"><input type="checkbox" class="isaenable" /></td><td><i class="icon-x delete position-left"></i></td></tr>';

        
       
        R.ScrollAutoSize('#adv-wrapper', function () {
            return $(window).height() - 92;
        }, function () {
            return 'auto';
        }, {}, {}, {}, true);
    },
    RegisterEvents: function () {
        $('table tr.row-item input, table tr.row-item textarea').off('keypress keyup').on('keypress keyup', function () {
            $(this).parents('tr').addClass('edit');
        });
        $('table tr.row-item input[type=checkbox]').off('click').on('click', function () {
            $(this).parents('tr').addClass('edit');
        });

        $('#row-add i').off('click').on('click', function () {
            var randomNumber = 101 + Math.floor(Math.random() * 100) + Math.floor(Math.random() * 100);
            var html = '<tr class="row-item"><td><input type="text" class="name form-control" placeholder="Tiêu đề" /><textarea style="margin-top: 10px" class="description form-control" placeholder="Mô tả" ></textarea>' +
                '</td><td><label for="fileSingleupload' + randomNumber + '"><input accept="image/*" id="fileSingleupload' + randomNumber + '" multiple type="file" name="files[]" style="display: none" /><img src="/CMS/Themes/images/cover.jpg?w=100" /></label>' +
                '</td><td><input type="text" style="width: 100%" class="url form-control" value="#"/></td>' +
                '<td style="width: 100px"><input type="text" class="sort form-control" /></td><td style="width: 40px"><input type="checkbox" class="isaenable" /></td><td><i class="icon-x delete position-left"></i></td></tr>';
            $('#row-add').before(html);

            R.Slider.RegisterEvents();
        });

        $('table tr.row-item img').off('click').on('click', function () {
            $(this).parents('tr').addClass('edit');
            var fieldUpload = $(this).closest('td').find('input[type="file"]').attr('id');
          
            R.Image.SingleUpload($('#' + fieldUpload),
             function () {
                 //process
             }, function (response, status) {
                 $('#' + fieldUpload).RModuleUnBlock();
                 //success
                 if (!response.success) {
                     $.confirm({
                         title: 'Thông báo !',
                         type: 'red',
                         content: response.messages,
                         animation: 'scaleY',
                         closeAnimation: 'scaleY',
                         buttons: {
                             cancel: {
                                 text: 'Đóng',
                                 btnClass: 're-btn re-btn-default'
                             },
                             yes: {
                                 isHidden: true, // hide the button
                                 keys: ['ESC'],
                                 action: function () {

                                 }
                             }
                         }
                     });
                     return;
                 }
                 if (response.success && response.error == 200) {
                     var img =  response.path;
                     $('#' + fieldUpload).closest('td').find('img').attr({
                         'data': response.name,
                         'src': img + '?w=100'
                     });

                 }
             });
            R.Slider.RegisterEvents();
        });

        $('table tr.row-item i.delete').off('click').on('click', function (e) {
            var id = $(this).parents('tr').attr('data');
            var el = $(this).parents('tr');
            if (parseInt(id) > 0) {
                $.confirm({
                    title: 'Xác nhận',
                    content: 'Xóa ?',
                    buttons: {
                        confirm: {
                            text: 'Xóa',
                            action: function () {

                                R.Slider.Delete(id, el);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            action: function () {

                            }
                        },
                    }
                });
            } else {
                $(this).parents('tr').remove();
            }


        });

        $('#save-temp').off('click').on('click', function (e) {
            var data = [];

            $('tr.row-item.edit').each(function (i, v) {
                var obj = {
                    type: $('#config-sidebar li.active').attr('data-type'),
                    id: $(v).attr('data'),
                    order: $(v).find('.sort').val(),
                    label: $(v).find('.name').val(),
                    des: $(v).find('.description').val(),
                    url: $(v).find('.url').val(),
                    img: $(v).find('img').attr('data'),
                    isenable: $(v).find('.isaenable').is(':checked')
                };
                data.push(obj);
            });

            R.Slider.Save(data);

        });


        R.ScrollAutoSize('#adv-wrapper', function () {
            return $(window).height() - 80;
        }, function () {

        }, {});
    },

    Delete: function (id, el) {

        $('#adv-wrapper').RModuleBlock();
        R.Post({
            params: {
                id: id

            },
            module: "adv",
            ashx: 'modulerequest.ashx',
            action: "delete",
            success: function (res) {
                if (res.Success) {
                    el.remove();
                    $.notify("Xóa thành công !", {
                        autoHideDelay: 2000, className: "success",
                        globalPosition: 'right top'
                    });

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Slider.RegisterEvents();
                $('#adv-wrapper').RModuleUnBlock();
            }

        });
    },


    Save: function (data) {
        $('#adv-wrapper').RModuleBlock();
        R.Post({
            params: { json: JSON.stringify(data) },
            module: "adv",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 2000, className: "success",
                        globalPosition: 'right top'
                    });

                    location.reload();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Slider.RegisterEvents();
                $('#adv-wrapper').RModuleUnBlock();
            }

        });
    }



}

$(function () {
    R.Slider.Init();
})
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="Golden.CMS.Modules.News.Templates.Preview" %>


<div style="position: relative; width: 800px; margin: 0 auto">
    <div class="text-right right" style="float: right; clear: both; margin: 3px 0; position: absolute; z-index: 99; right: -30px">
        <i class="icon-x position-left" data-dismiss="modal" style="cursor: pointer"></i>
    </div>
</div>
<div id="news-preview">
    <div class="container-news">
        <nav class="navbar-fixed-top body">
            <div class="btn-action" style="padding: 8px 0 4px 0;">
                <button type="button" class="btn btn-default btn-sm _unpublish" style="display: none" data-popup="tooltip" data-original-title="Hạ bài" data-placement="bottom"><i class="icon-file-download"></i></button>
                <button type="button" class="btn btn-default btn-sm _publish" style="display: none" data-popup="tooltip" data-original-title="Xuất bản" data-placement="bottom"><i class="icon-file-upload"></i></button>
                <button type="button" class="btn btn-default btn-sm _request_publish" style="display: none" data-popup="tooltip" data-original-title="Gửi y/c duyệt" data-placement="bottom"><i class="icon-paperplane"></i></button>
                <button type="button" class="btn btn-default btn-sm _reject" style="display: none" data-popup="tooltip" data-original-title="Từ chối" data-placement="bottom"><i class="icon-file-minus2"></i></button>
            </div>
        </nav>
        <div id="body" class="body">
            <header>
                <h1 style="font-size: 14px; font-weight: bold; text-transform: uppercase; padding: 45px 0 10px 0">
                    <%=obj.NewsInfo.Title %> [<span class="_zone" data-id="<%=obj.NewsInfo.ZoneId %>"></span>]</h1>
            </header>
            <div class="thumbnail text-center" style="padding: 6px 0">
                <img src="/uploads/<%= obj.NewsInfo.Avatar %>" width="80%" />
            </div>
            <%--<div class=""><a href="<%=obj.NewsInfo.Url %>"><%=obj.NewsInfo.Url %></a></div>--%>
            <p class="">
                <b><%=obj.NewsInfo.Sapo %></b>
            </p>
            <div class="detail">
                <%=obj.NewsInfo.Body %>
            </div>
            <div class="tags">Tags:<%=GetTags(0) %> </div>
     <%--       <div class="Metakeyword">
                <label>MetaKeyword:</label><%=obj.NewsInfo.MetaKeyword %>
            </div>
            <div class="Metakeyword">
                <label>MetaDescription:</label><%=obj.NewsInfo.MetaDescription %>
            </div>--%>
            <div id="reject-text" style="display: none;padding: 10px 0"></div>
            <div id="reject-content" style="padding-bottom: 20px;display: none">
                <p class="text-center"><strong>Phản hồi</strong></p>
                <p id="lbMessage"><em style="font-size: 10px; color: red;display: none">*Gửi thông tin phản hồi cho tác giả.</em></p>
                <textarea id="replyReject"></textarea>
            </div>
        </div>


    </div>
</div>

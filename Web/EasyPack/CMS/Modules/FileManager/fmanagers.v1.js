﻿R.FileManager = {
    Init: function () {
        this.dialogFileManager = null;
        this.multi = null;
        this.plUploadFile = null;
        this.eljstree = '#fm-folder';
        this.folderSelected = 0;
        this.ItemSelected = 0;
        this.View = 'grid';
        this.IndexFileStart = 0;
        this.PageIndex = 1;
        this.PageSize = 90;
        R.FileManager.RegisterEvents();
    },
    RegisterEvents: function () {
        $('#btn-fm-delete').off('click').on('click', function () {
            var id = $(this).attr('data-id');
            if (confirm('Xác nhận xóa !')) {
                R.Post({
                    params: { node_id: id },
                    module: "fmanager",
                    ashx: 'modulerequest.api',
                    action: "file_remove",
                    success: function (res) {
                        if (res.Success) {
                            $('#fm-grid').find('li#' + id + '').remove();
                            $('#fm-file-view .attachment-info').html('');
                        } else {
                            alert(res.Message);
                        }
                    }
                });
            }

        });
        //list
        $('#fm-content .fm-list .item').off('click').on('click', function (e) {

            var indexFileEnd = 0;
            if (R.FileManager.multi === 'multi') {
                if (e.ctrlKey) {

                    if ($(this).hasClass('_active')) {
                        $(this).removeClass('_active');
                    } else {
                        $(this).addClass('_active');
                    }
                    //  $(this).addClass('_active');

                } else

                    if (e.shiftKey) {
                        indexFileEnd = parseInt($(this).index());// get index item end selected
                        //   alert('start:' + indexFileEnd)
                        if (R.FileManager.IndexFileStart < indexFileEnd) {
                            for (var i = R.FileManager.IndexFileStart; i <= indexFileEnd; i++) {
                                $('#fm-content .fm-list .item:eq(' + i + ')').addClass('_active');
                            }
                        } else {
                            for (var i = R.FileManager.IndexFileStart; i >= indexFileEnd; i--) {
                                $('#fm-content .fm-list .item:eq(' + i + ')').addClass('_active');
                            }
                        }
                    }
                    else {

                        $('#fm-content .fm-list .item').removeClass('_active');
                        $(this).addClass('_active');

                    }

                // alert('start:' +(parseInt($('#fm-content .fm-list .item._active').index())))
                // get index item selected
                R.FileManager.IndexFileStart = parseInt($('#fm-content .fm-list .item._active').index());// chua chuẩn
            } else {

                if ($(this).hasClass('_active')) {
                    $(this).removeClass('_active');
                } else {

                    $('#fm-content .fm-list .item').removeClass('_active');
                    $(this).addClass('_active');
                }
            }

        });
        // grid
        $('#fm-grid li.item').off('click').on('click', function (e) {

            var data = {
                id: $(this).attr('id'),
                name: $(this).attr('name'),
                size: $(this).attr('size'),
                time: $(this).attr('time'),
                ext: $(this).attr('ext'),
                path: $(this).attr('path'),
                dimensions: $(this).attr('dimensions')
            }

            R.FileManager.FileDetail(data);
            var indexFileEnd = 0;
            if (R.FileManager.multi === 'multi') {
                if (e.ctrlKey) {

                    if ($(this).hasClass('_active')) {
                        $(this).removeClass('_active');
                    } else {
                        $(this).addClass('_active');
                    }
                    //  $(this).addClass('_active');

                } else

                    if (e.shiftKey) {
                        indexFileEnd = parseInt($(this).index());// get index item end selected
                        //   alert('start:' + indexFileEnd)
                        if (R.FileManager.IndexFileStart < indexFileEnd) {
                            for (var i = R.FileManager.IndexFileStart; i <= indexFileEnd; i++) {
                                $('#fm-grid li:eq(' + i + ')').addClass('_active');
                            }
                        } else {
                            for (var i = R.FileManager.IndexFileStart; i >= indexFileEnd; i--) {
                                $('#fm-grid li:eq(' + i + ')').addClass('_active');
                            }
                        }
                    }
                    else {

                        $('#fm-grid li').removeClass('_active');
                        $(this).addClass('_active');
                    }
                // alert('start:' +(parseInt($('#fm-content .fm-list .item._active').index())))
                // get index item selected
                R.FileManager.IndexFileStart = parseInt($('#fm-grid li._active').index());// chua chuẩn
            } else {

                if ($(this).hasClass('_active')) {
                    $(this).removeClass('_active');
                } else {

                    $('#fm-content .fm-list .item').removeClass('_active');
                    $(this).addClass('_active');
                }
            }

        });
    },

    Open: function (callback, multi) {

        if ($('#fm-container').length === 0) {
            $('body').append('<div id="fm-container-dialog"></div>');
        }

        R.Post({
            params: {},
            module: "fmanager",
            ashx: 'modulerequest.api',
            action: "file_manager_init",
            success: function (res) {
                if (res.Success) {
                    var $elDialog = $('#fm-container-dialog');
                    R.FileManager.dialogFileManager = $elDialog.dialog({
                        autoOpen: false,
                        height: $(window).height() - 40,
                        width: $(window).width() - 40,
                        resizable: false,
                        show: "fadein",
                        hide: "fadeout",
                        modal: true,
                        zIndex: 500,
                        buttons: {

                        },
                        create: function () {
                            $elDialog.parent().find('.ui-dialog-titlebar').remove();
                            $elDialog.html(res.Content);
                            $("#fm-main").css({ 'height': ($(window).height() - 40) - 50 - 32 });
                            $("#fm-data-wrapper").css({ 'width': ($(window).width()) - 220 - 250 - 42 }); // 220 sldebar . 250 view , 62 space and border
                            $("body").css({ overflow: 'hidden' }).append('<div class="ui-overlay"></div>');
                            R.FileManager.multi = multi;
                            R.FileManager.JSTree();
                            R.FileManager.Search(R.FileManager.View);
                            $(window).resize(function () {
                                var wWidth = $(window).width();
                                var dWidth = wWidth - 40;
                                var wHeight = $(window).height();
                                var dHeight = wHeight - 40;
                                var w = ($(window).width()) - 220 - 250 - 42;
                                $("#fm-data-wrapper").css({ 'width': w }); // 220 sldebar . 250 view , 62 space and border
                                $elDialog.dialog({
                                    "width": dWidth,
                                    "height": dHeight,
                                    autoOpen: false

                                });
                                $("#fm-main").css({ 'height': dHeight - 50 - 32 });
                                var hs = ($(window).height() - 40) - 50 - 32;// 26 footer
                                $("#fm-content .fm-list").slimScroll({
                                    height: hs,
                                    width: w
                                });
                            });

                        },
                        close: function () {
                            $("body").css({ overflow: 'inherit' }).find('.ui-overlay').remove();
                            $elDialog.dialog("destroy");
                        },
                        open: function (event, ui) {
                            $elDialog.parent().find('.ui-dialog-titlebar').remove();
                            $('#dl-close').click(function () {
                                $elDialog.dialog("close");
                            });

                            $('#btn-attack').off('click').on('click', function () {
                                var el = [];

                                if ($('#fm-grid').length > 0) {
                                    $('#fm-data-wrapper ul li._active').each(function (i, v) {
                                        el.push($(v).attr('name'));
                                    });
                                    $('#fm-data-wrapper ul li._active').removeClass('_active');
                                } else {
                                    $('#fm-data-wrapper table tbody tr._active').each(function (i, v) {
                                        el.push($(v).attr('data'));
                                    });

                                }

                                if (callback) {
                                    callback(el);
                                    $elDialog.dialog("close");
                                }
                            });
                            R.FileManager.InitUpload();
                        }
                    });
                    $elDialog.dialog("open");
                    // upload


                    // $(el).RModuleUnBlock();
                }
            }
        });
    },
    CreateFolder: function ($node, action) {
        if ($('#create-folder').length === 0) {
            $('#fm-container').append('<div id="create-folder" style="" ></div>');
        }
        var $el = $('#create-folder');
        $el.dialog({
            autoOpen: true,
            height: 170,
            width: 300,
            show: "fadein",
            hide: "fadeout",
            modal: true,
            buttons: {

            },
            close: function () {
                $('#create-folder').html('');
            },
            open: function (event, ui) {
                $el.closest('.ui-dialog').find(".ui-dialog-titlebar").remove();
                var htm = '';
                if (action === 'rename') {
                    htm = '<div class="content"><div data-alertify-msg=""><label>Nhập tên mới</label><input data-alertify-input="" id="folderName" value="' + $node.text + '" id="_newFolder" type="text"></div><nav data-alertify-btn-holder=""><button data-alertify-btn="ok" tabindex="1" class="_save">Cập nhật</button><button class="_cancel" data-alertify-btn="cancel" tabindex="2">Cancel</button></nav></div>';
                } else {
                    htm = '<div class="content"><div data-alertify-msg=""><label>Nhập tên folder</label><input data-alertify-input="" id="folderName" value="New Folder" id="_newFolder" type="text"></div><nav data-alertify-btn-holder=""><button data-alertify-btn="ok" tabindex="1" class="_save">Create folder</button><button class="_cancel" data-alertify-btn="cancel" tabindex="2">Cancel</button></nav></div>';

                }

                $el.html(htm);

                setTimeout(function () {
                    $('button._cancel').click(function () {
                        $el.dialog("close");

                    });
                    $('button._save').click(function () {
                        var name = $('#folderName').val();
                        var rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
                        if (rx.test(name)) {
                            $('#folderName').val('').addClass('ms_error');
                            alert('Tên không chưa ký tự: <>:"\/\\|?*');
                            return;

                        }
                        R.FileManager.FolderSave($node, name, action);
                    });


                });

            }
        });
    },
    JSTree: function () {

        //console.log('js tree')
        var tree = $('#fm-folder').jstree({

            "core": {
                callback: {
                    onselect: function (NODE, TREE_OBJ) {
                        alert(NODE.id);
                    }
                },
                "animation": 0,
                "check_callback": function (op, node, par, pos, more) {
                    alert(op)
                    if ((op === "move_node" || op === "copy_node") && node.type && node.type == "root") {
                        return false;
                    }
                    if ((op === "move_node" || op === "copy_node") && more && more.core && !confirm('Xác nhận !')) {

                        return false;
                    }

                    if ((op === "delete_node" || op === "delete_node") && more && more.core && !confirm('Xác nhận 1 !')) {
                        return false;
                    }

                    // return true;
                },
                "themes": { "stripes": true },
                'data': folders
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 4,
                    "valid_children": ["root"]
                },
                "root": {
                    //  "icon": "/static/3.3.3/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "contextmenu": {
                "items": function ($node) {
                    //    var tree = $("#tree").jstree(true);
                    return {
                        "Create": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Create",
                            icon: 'fa fa-edit',
                            "action": function (obj) {
                                R.FileManager.CreateFolder($node, 'create');
                            }
                        },
                        "Rename": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Rename",
                            icon: 'fa tag',

                            "action": function (obj) {
                                R.FileManager.CreateFolder($node, 'rename');

                            }
                        },
                        "Remove": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Remove",
                            "action": function (obj) {
                                if (confirm('Xác nhận xóa !')) {
                                    R.Post({
                                        params: { node_id: $node.id },
                                        module: "fmanager",
                                        ashx: 'modulerequest.api',
                                        action: "folder_remove",
                                        success: function (res) {
                                            if (res.Success) {
                                                var tree = $(R.FileManager.eljstree).jstree(true);
                                                tree.delete_node([$node]);
                                            } else {
                                                alert(res.Message);
                                            }
                                        }
                                    });
                                }


                            },
                            icon: 'fa fa-trash'
                        },
                        //"Edit": {
                        //    "separator_before": false,
                        //    "separator_after": true,
                        //    "label": "Edit",
                        //    "action": false,

                        //    "submenu": {
                        //        "Cut": {
                        //            "seperator_before": false,
                        //            "seperator_after": false,
                        //            "label": "Cut",
                        //            "icon": "fa cut",
                        //            action: function (obj) {
                        //                alert("Cut");
                        //                // this.create(obj, "last", { "attr": { "rel": "Cut" } });
                        //            }
                        //        },
                        //        "Copy": {
                        //            "seperator_before": false,
                        //            "seperator_after": false,
                        //            "label": "Copy",
                        //            "icon": "fa copy",
                        //            action: function (obj) {
                        //                alert("Copy");
                        //                //   this.create(obj, "last", { "attr": { "rel": "Copy" } });
                        //            }
                        //        },
                        //        "Pate": {
                        //            "seperator_before": false,
                        //            "seperator_after": false,
                        //            "label": "Pate",
                        //            "icon": "fa paste",
                        //            action: function (obj) {
                        //                alert("Pate");
                        //                //  this.create(obj, "last", { "attr": { "rel": "service" } });
                        //            }
                        //        }
                        //    }
                        //}
                    };
                }
            },
            "plugins": [
                "contextmenu", "dnd", "search", "types", "wholerow"
            ]
        }).bind("move_node.jstree", function (e, data, node, par) {

            //var parent = data.node.parent;// new parent
            //var old_parent = data.node.old_parent;// old_parent
            //var id = data.node.id;
            //var name = data.node.text;


            var _data = {
                id: data.node.id,
                parent: data.node.parent,// new parent,
                text: data.node.text
            }
            R.FileManager.FolderSave(_data, '', 'move');

        }).on('delete_node.jstree', function (e, data) {

        }).bind("select_node.jstree", function (evt, data) {
            R.FileManager.folderSelected = data.node.id;
            $('#fm-wrapper').attr('node-select', data.node.id);
            R.FileManager.PageIndex = 1;
            R.FileManager.Search(R.FileManager.View);
        });;
    },
    FolderSave: function (node, name, action) {

        var data = {
            'node_id': node.id,
            'node_name': node.text,
            'parent_id': node.parent,
            'name': name,
            'action': action

        }
        R.Post({
            params: data,
            module: "fmanager",
            ashx: 'modulerequest.api',
            action: "folder_save",
            success: function (res) {
                if (res.Success) {
                    if (action === 'create') {
                        $(R.FileManager.eljstree).jstree().create_node(node.id, {
                            "id": res.Data,
                            "text": name
                        }, "last", function () {
                            console.log('add node success !');
                        });
                    }
                    if (action === 'rename') {
                        //rename node
                        $(R.FileManager.eljstree).jstree('rename_node', node.id, name);

                    }
                    if (action === 'move') {

                    }

                    $('#create-folder').dialog('close');
                } else {
                    alert(res.Message);
                }
            }
        });
    },
    Search: function (view) {
        $('#fm-data-wrapper').RModuleBlock();
        var data = {
            'keyword': '',
            'parentid': R.FileManager.folderSelected,
            'action': '',
            'pageindex': R.FileManager.PageIndex,
            'pagesize': R.FileManager.PageSize
        }
        R.Post({
            params: data,
            module: "fmanager",
            ashx: 'modulerequest.api',
            action: "search",
            success: function (res) {

                if (res.Success) {

                    var data = JSON.parse(res.Data);
                    var row = '';
                    $.each(data, function (i, v) {
                        var dateString = v.UploadedDate.substr(6);
                        var currentTime = new Date(parseInt(dateString));
                        var month = currentTime.getMonth() + 1;
                        var day = currentTime.getDate();
                        var year = currentTime.getFullYear();
                        var h = currentTime.getHours();
                        var m = currentTime.getMinutes();
                        var date = day + "/" + month + "/" + year + " " + h + ":" + m;
                        var ext = v.FileExt.replace('.', '').toLowerCase();

                        if (view === 'line') {
                            row += '<tr ext="' + ext + '" dimensions="' + v.Dimensions + '" name="' + v.Name + '" time="' + date + '" size="' + v.FileSize + '"  class="item"><td  class="name"><span class="fname"><i class="list-icon ' + ext + '" ></i>' + v.Name + '</span></td><td class="type">' + v.FileExt + '</td><td class="size">' + v.FileSize + '' +
                                ' Kb</td><td class="dimensions">' + v.Dimensions + '</td><td class="date">' + date + '</td></tr>';
                        } else {

                            var icon = '';

                            if (ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif' && ext !== 'bmp') {
                                icon = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_' + ext + '.png"  alt="">';
                            } else
                                if (ext == '') {
                                    icon = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_file.png"  alt="">';
                                }
                                else {
                                    icon = '<img src="' + StorageUrl + v.FilePath + '?w=150"  alt="">';
                                }

                            row += '<li id="' + v.Id + '" path="' + v.FilePath + '" class="item" ext="' + ext + '" dimensions="' + v.Dimensions + '" name="' + v.Name + '" time="' + date + '" size="' + v.FileSize + '">' + icon + '<i class="fa fa-check"></i><p class="Cdiv">' + v.Name + '</p></li>';
                        }

                    });
                    var htmTable = '';
                    if (view === 'line') {
                        htmTable += '<table class="_list">';
                        htmTable += '<thead>';
                        htmTable += '            <tr class="rowHeader" >';
                        htmTable += '            <th><span >Name</span></th>';
                        htmTable += '            <th ><span>Type</span></th>';
                        htmTable += '            <th ><span >Size</span></th>';
                        htmTable += '            <th ><span>Dimensions</span></th>';
                        htmTable += '            <th class="sorted sorted-desc"><span >Date</span></th>';
                        htmTable += '        </tr>';
                        htmTable += '</thead>';
                        htmTable += '      <tbody>' + row + '';
                        htmTable += '      </tbody>';
                        htmTable += '</table>';
                    } else {
                        htmTable += '<ul id="fm-grid">';
                        htmTable += row;
                        htmTable += '</ul>';

                    }
                    $('#fm-content .fm-list').html(htmTable);

                    var hs = ($(window).height() - 40) - 50 - 32; // 26 footer
                    var w = ($(window).width()) - 220 - 250 - 42;

                    $("#fm-content .fm-list").slimScroll({
                        height: hs,
                        width: w
                    });

                    $('.item-file').on('click', function (e) {

                        // console.log($(this).attr('data'))
                    });

                    var pageInfo = res.Content.split('#');
                    var page = pageInfo[0];
                    var extant = pageInfo[1];
                    var totals = pageInfo[2];

                    var rowFrom = '';
                    if (R.FileManager.PageIndex === 1) {
                        rowFrom = 'Từ 1 đến ' + extant;
                    } else {
                        rowFrom = 'Từ ' + (parseInt(R.FileManager.PageIndex) * parseInt(R.FileManager.PageSize) - parseInt(R.FileManager.PageSize)) + ' đến ' + extant;
                    }

                    $('#frowInTotals').text(rowFrom + '/' + totals);
                    $('#fm-toolbar .ipagination').jqPagination({
                        current_page: R.FileManager.PageIndex,
                        max_page: page,
                        paged: function (page) {
                            R.FileManager.PageIndex = page;
                            R.FileManager.Search();
                        }
                    });
                    R.FileManager.RegisterEvents();
                }
                else {
                    alert(res.Message);
                }
                $('#fm-data-wrapper').RModuleUnBlock();
            }
        });
    },
    FileDetail: function (data) {
        var icon = '';
        if (data.ext !== 'png' && data.ext !== 'jpg' && data.ext !== 'jpeg' && data.ext !== 'gif' && data.ext !== 'bmp') {
            icon = '<img src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_' + data.ext + '.png"  alt="">';
        } else
            if (data.ext == '') {
                icon = '<img src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_file.png"  alt="">';
            }
            else {
                icon = '<img src="' + StorageUrl + data.path + '"  alt="">';
            }
        var htm = '<div class="file-thumb">' + icon + '</div><div class="details"><div class="filename">' + data.name + '</div><div class="uploaded">Thời gian: ' + data.time + '</div><div class="file-size">Dung lượng: ' + data.size + 'kb</div><div class="dimensions">Kích thước: ' + data.dimensions + '</div></div><div class="items-action"><button class="file-action" data-id="' + data.id + '" id="btn-fm-delete">Xóa</button></div>';
        $('#fm-file-view .attachment-info').html(htm);
        R.FileManager.RegisterEvents();

    },
    InitUploadV2: function () {
        $('#btn-fm-upload').fileupload({
            url: StorageUploadUrl + '?fn=upload&fid=' + R.FileManager.folderSelected,
            dataType: 'json',
            limitMultiFileUploads: 1,
            limitMultiFileUploadSize: 2048000, //2Mb
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            autoUpload: true,
            disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewCrop: true,
            done: function (e, data) {
            },
            send: function (e, data) {

            },

            progress: function (e, data) {

            },
            progressall: function (e, data) {
                $('#progressall').show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progressall b').text(progress);
            },
            success: function (response, status) {
                var htm = '';
                var thumb = '';

                //$('#fm-grid').find('#' + file.id + '').html(htm).attr({
                //    ext: ext,
                //    dimensions: v.Dimensions,
                //    name: v.Name,
                //    time: v.strDate,
                //    size: v.FileSize
                //}).addClass('item');

                if (response.success && response.error == 200) {
                    var ext = response.ext.toLowerCase();
                    ext = ext.replace(/\./g, '');
                    var img = StorageUrl + response.path;
                    if (ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif' && ext !== 'bmp') {

                        thumb = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_' + ext + '.png"  alt=""><i class="fa fa-check"></i>';
                    } else
                        if (ext == '') {

                            thumb = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_file.png"  alt=""><i class="fa fa-check"></i>';
                        }
                        else {

                            thumb = '<img src="' + img + '"  alt=""><i class="fa fa-check"></i>';

                        }
                    htm += '<li  name="' + response.name + '" time="" size="">' + thumb + '</li>';


                }

                $('#fm-grid li:first-child').before(htm);
                $('#progressall').hide();
            },
            error: function (error) {
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Tải ảnh lên không thành công.',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        },
                        yes: {
                            isHidden: true, // hide the button
                            keys: ['ESC'],
                            action: function () {

                            }
                        }

                    }

                });
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    },
    InitUpload: function (data) {
        var uploader = new plupload.Uploader({
            runtimes: 'html5,silverlight,flash,html4',
            url: '/CMS/Modules/FileManager/Action/ImageUploadHandler.ashx',
            max_file_size: '10mb',
            chunk_size: '100000kb',
            unique_names: false,
            autostart: true,
            browse_button: 'btn-fm-upload', // you can pass in id...
            container: document.getElementById('container'), // ... or DOM Element itself
            filters: [
                { title: "Image files", extensions: "jpg,jpeg,gif,png,bmp" },
                { title: "Zip files", extensions: "zip,rar" },
                { title: "Doc files", extensions: "doc, xls, pps, docx, xlsx, ppsx,vsd" },
                { title: "PDF files", extensions: "pdf" }

            ],
            multiple_queues: true,
            multipart_params: {
                'folder_id': R.FileManager.folderSelected
            },
            // Flash settings
            flash_swf_url: '/plupload/js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url: '/plupload/js/Moxie.xap',


            init: {
                //PostInit: function () {
                //    document.getElementById('filelist').innerHTML = '';

                //    document.getElementById('uploadfiles').onclick = function () {
                //        uploader.start();
                //        return false;
                //    };
                //},

                FilesAdded: function (up, files) {
                    var htm = '';
                    if (R.FileManager.View === 'grid') {
                        plupload.each(files, function (file) {
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();

                            if (dd < 10) {
                                dd = '0' + dd;
                            }

                            if (mm < 10) {
                                mm = '0' + mm;
                            }

                            today = mm + '/' + dd + '/' + yyyy;

                            htm += '<li id="' + file.id + '" name="' + file.name + '" time="' + today + '" size="' + plupload.formatSize(file.size) + '"><img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_file.png"  alt=""><b></b></li>';

                            // $('#fm-grid').append ('<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                        });
                    } else {
                        //list
                    }
                    $('#fm-grid li:first-child').before(htm);
                    uploader.start();
                },

                UploadProgress: function (up, file) {

                    document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                },
                FileUploaded: function (up, file, res) {
                    var v = $.parseJSON(res.response);


                    var ext = v.FileExt.replace('.', '').toLowerCase();
                    var htm = '';
                    if (ext !== 'png' && ext !== 'jpg' && ext !== 'jpeg' && ext !== 'gif' && ext !== 'bmp') {
                        //     path = '/CMS/Modules/FileManager/Libs/Images/fileicons/icon_' + ext + '.png';
                        htm = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_' + ext + '.png"  alt=""><i class="fa fa-check"></i>';
                    } else
                        if (ext == '') {

                            htm = '<img class="thumb" src="/CMS/Modules/FileManager/Libs/Images/fileicons/icon_file.png"  alt=""><i class="fa fa-check"></i>';
                        }
                        else {
                            htm = '<img src="/' + v.FilePath + '"  alt=""><i class="fa fa-check"></i>';

                        }

                    //  htm = '<img class="thumb" src="' + path + '"  alt=""><i class="fa fa-check"></i>';
                    $('#fm-grid').find('#' + file.id + '').html(htm).attr({
                        ext: ext,
                        dimensions: v.Dimensions,
                        name: v.Name,
                        time: v.strDate,
                        size: v.FileSize
                    }).addClass('item');
                },
                UploadComplete: function (up, args) {
                    R.FileManager.RegisterEvents();

                },
                Error: function (up, err) {
                    document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
                }
            }
        });

        uploader.init();
    }
}


$(function () {
    R.FileManager.Init();
})
﻿R.Project = {
    Init: function () {
        R.Project.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 0;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 3;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');

        if ($('#mini-game').length > 0) {
            R.Project.XNews();
            $('[data-popup="tooltip"]').tooltip();
        };


    },
    RegisterEvents: function () {
        //Nut them kich thuoc san pham
        $('#addProductSizeBtn').off('click').on('click', function () {
            //alert(1);
            $('.sizeProductDiv').last().clone().appendTo('#sizeProductContainer').find("input[type='text']").val("");
        })
        $(".product_size_delete").off('click').on('click', function () {
            $(this).parent().remove();
        })

        //
        $('#titletxt').bind("keypress keyup", function (event) {
            $('#urltxt').val(R.UnicodeToSeo($('#titletxt').val()));
        });
        if ($('#mini-game').length > 0) {

            $('#news-save .btn-primary').off('click').on('click',
                function (e) {
                    var status = $(this).attr('status');
                    var oid = R.Project.XNewsId;
                    var title = $('#titletxt').val();
                    var sapo = $('#Sapo').val();
                    var zoneddl = $('#_zoneddl').val();
                    var url = $('#urltxt').val();
                    var titleSeo = $('#metaTitletxt').val();
                    var metakeyword = $('#metakeywordtxt').val();
                    var metadescription = $('#metadescriptiontxt').val();
                    var date = $('#datetxt').attr('data');
                    var content = CKEDITOR.instances['contentCkeditor'].getData();
                    var cssddl = $('#_cssddl').val();
                    var contents = [];
                    $('#items .item').each(function (i, v) {
                        var ckid = $(v).find('.editor').attr('id');
                        var image = $(v).find('img').attr('data-img');
                        contents.push({
                            title: $(v).find('.title').val(),
                            image: typeof (image) != 'undefined' ? image : '',
                            content: CKEDITOR.instances['' + ckid + ''].getData()
                        });

                    });
                    var size = '';
                    $('.product_size').each(function (i, obj) {
                        size += $(this).val() + ';';
                    })

                    var data = {
                        id: oid,
                        title: title,
                        sapo: sapo,
                        //  author: author,
                        // source: source,
                        zone: zoneddl,
                        date: date,
                        content: content,
                        url: url,
                        //status:status,
                        metakeyword: metakeyword,
                        status: status,
                        metadescription: metadescription,
                        avatar: $('#attack-thumb img').attr('data-img'),
                        avatar2: $('#attack-thumb-2 img').attr('data-img'),
                        metatitle: titleSeo,
                        hours_of_work: $('#hoursOfWorkTxt').val(),
                        invest: $('#InvestTxt').val(),
                        surface_area: $('#surfaceAreaTxt').val(),
                        //budge: R.FormatDate($('#budgetTxt').val()),
                        //groupId: $('#groupDdl').val(),
                        contents: JSON.stringify(contents),
                        size: size,
                        hot: $('#hotcb').is(':checked') == true ? true : false

                    }

                    R.Project.XSave(data);
                });


            $('#module-content .btn-action button._edit').off('click').on('click',
                function (e) {
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');

                    R.Project.XEdit(id);
                });

            $('#btn-new-post').off('click').on('click',
                function () {
                    R.Project.XEdit(0);
                });
            var delay = null;
            $('#news-sldebar .navigation li.sub-news-game').off('click').on('click',
                function () {

                    R.Project.ItemSelected = [];
                    $('#main-pnl').show('slow');
                    $('#sub-pnl').hide();
                    $('#news-sldebar .navigation li').removeClass('active');
                    $(this).addClass('active');
                    R.Project.startDate = R.Project.minDate;
                    R.Project.endDate = R.Project.maxDate;
                    R.Project.PageIndex = 1;
                    $('#date-filter span').html("Tất cả: " + R.Project.minDate + ' - ' + R.Project.maxDate);
                    R.Project.NewsStatus = $(this).attr('status');
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Project.XNews();
                    },
                        200);

                });
            $('#date-filter').off('apply.daterangepicker').on('apply.daterangepicker',
                function (ev, picker) {
                    R.Project.startDate = picker.startDate.format('YYYY-MM-DD');
                    R.Project.endDate = picker.endDate.format('YYYY-MM-DD');
                    R.Project.XNews();
                });
            $('#zone-filer-ddl').off('change').on('change',
                function (e) {
                    R.Project.XNews();
                });
            $('#zone-type-ddl').off('change').on('change',
                function (e) {
                    R.Project.XNews();
                });
            $('#btn-new-reload').off('click').on('click',
                function (e) {
                    R.Project.startDate = R.Project.minDate;
                    R.Project.endDate = R.Project.maxDate;
                    R.Project.PageIndex = 1;
                    $('#_search-data').val('');
                    $('#date-filter span').html("Tất cả: " + R.Project.minDate + ' - ' + R.Project.maxDate);
                    if (delay != null) clearTimeout(delay);
                    delay = setTimeout(function () {
                        R.Project.XNews();
                    },
                        200);
                });
            $('#module-content .btn-action button._view').off('click').on('click',
                function (e) {
                    $('#news-preview').RLoading();
                    var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked')
                        .attr('id');
                    $('#news-preview').modal('show').off('shown.bs.modal').on('shown.bs.modal',
                        function () {
                            R.Post({
                                params: { id: id },
                                module: "news",
                                ashx: 'modulerequest.ashx',
                                action: "preview",
                                success: function (res) {
                                    if (res.Success) {
                                        $('#news-preview-content').html(res.Content).find('#body .detail img')
                                            .css({ 'max-width': '80%' }).parent().css({
                                                'text-align': 'center',
                                                'width': '100%',
                                                'display': 'inline-block'
                                            });
                                        $('#news-preview-content p img,#news-preview-content p iframe').parent()
                                            .css('text-align', 'center');
                                        $('#news-preview .modal-content').css('overflow-y', 'hidden');
                                        $('[data-popup="tooltip"]').tooltip();
                                        // check menu action in view

                                        if (R.Project.NewsStatus == 3) {
                                            $('.btn-group.btn-action ._unpublish').show("slow");
                                        } else {
                                            $('.btn-group.btn-action ._unpublish').hide("slow");
                                        }
                                        // bài lưu tạm
                                        if (R.Project.NewsStatus == 5) {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').show("slow");
                                        } else {
                                            // từ chối
                                            $('.btn-group.btn-action ._reject').hide("slow");
                                        }

                                        if (R.Project.NewsStatus == '6' || R.Project.NewsStatus == 6) {
                                            $('.btn-group.btn-action ._request_publish').hide("slow");
                                        } else {

                                            $('.btn-group.btn-action ._request_publish').show("slow");
                                        }

                                        R.ScrollAutoSize('#news-preview .container-news',
                                            function () {
                                                return $(window).height() - 10;
                                            },
                                            function () {
                                                return 'auto';
                                            },
                                            {});

                                        var $zoneItem = $('#news-preview').find('._zone');
                                        if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
                                            var strHtml = '';
                                            var arrayId = $zoneItem.attr('data-id').split(',');
                                            for (var k = 0; k < arrayId.length; k++) {
                                                $(RAllZones).each(function (i, item) {
                                                    if (arrayId[k] == item.Id) {
                                                        if (strHtml.length == 0) {
                                                            strHtml = item.Name;
                                                        } else {
                                                            strHtml += ';' + item.Name;
                                                        }
                                                    }
                                                    return;
                                                });

                                            }
                                            $zoneItem.text(strHtml);
                                        }
                                        R.Post({
                                            params: { id: id },
                                            module: "news",
                                            ashx: 'modulerequest.ashx',
                                            action: "reject_log",
                                            success: function (res) {
                                                var str = '';
                                                if (res.Success) {
                                                    if (parseInt(res.TotalRow) > 0) {
                                                        $.each($.parseJSON(res.Data),
                                                            function (i, item) {
                                                                str += '<li><p><b>' +
                                                                    item.CreatedBy +
                                                                    '</b> - <time>' +
                                                                    item.Date +
                                                                    '</time></p><div id="log_content">' +
                                                                    item.Content +
                                                                    '</div></li>';
                                                            });
                                                        $('#reject-text')
                                                            .html('<div id="_log"><ul>' + str + '</ul></div>').show();

                                                    }
                                                }
                                            }
                                        });

                                        R.Project.RegisterEvents();
                                        $('#news-preview').RLoadingComplete();

                                    } else {
                                        alert(res.Message);
                                    }
                                }
                            });
                        });
                });
            $('#module-content .btn-action button.status').off('click').on('click',
                function (e) {
                    var status = $(this).attr('data-status');
                    var data = {
                        id: R.Project.ItemSelected.join(','),
                        status: status
                    }
                    $.confirm({
                        title: 'Thông báo',
                        content: 'Xác nhận yêu cầu ?',
                        buttons: {
                            confirm: {
                                text: 'Thực hiện',
                                action: function () {
                                    R.Project.XUpdateStatus(data);
                                }
                            },
                            cancel: {
                                text: 'Hủy',
                                action: function () {

                                }
                            },
                        }
                    });


                });

            $('#data-list .datatable-scroll tr td input[type=checkbox]').off('click').on('click',
                function () {

                    var id = $(this).attr('id');
                    if ($(this).is(':checked')) {
                        $(this).closest('tr').addClass('row-selected');
                        R.Project.ItemSelected.push(id);
                    } else {

                        var index = R.Project.ItemSelected.indexOf(id);
                        if (index > -1) {
                            R.Project.ItemSelected.splice(index, 1);
                        }
                        $(this).closest('tr').removeClass('row-selected');
                    }


                    if (R.Project.ItemSelected.length === 0) {
                        $('.btn-group.btn-action').slideUp();
                    }
                    if (R.Project.ItemSelected.length === 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        // $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').show("slow");
                        $('.btn-group.btn-action ._edit').show("slow");


                    } else if (R.Project.ItemSelected.length > 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        //  $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').hide();
                        $('.btn-group.btn-action ._edit').hide();
                        // bài đã duyệt
                        //if (R.Project.NewsStatus === '1' || R.Project.NewsStatus === 1) {
                        //    $('.btn-group.btn-action ._unpublish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._unpublish').hide("slow");
                        //}
                        // bài lưu tạm
                        //if (R.Project.NewsStatus === '4' || R.Project.NewsStatus === 4) {
                        //    $('.btn-group.btn-action ._publish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._publish').hide("slow");
                        //}


                    }

                    if (R.Project.NewsStatus == 5) {
                        $('.btn-group.btn-action ._delete').hide("slow");

                    } else {
                        $('.btn-group.btn-action ._delete').show("slow");
                    }
                    if (R.Project.NewsStatus == 5) {
                        $('.btn-group.btn-action ._publish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._publish').hide("slow");
                    }
                    if (R.Project.NewsStatus == 3) {
                        $('.btn-group.btn-action ._unpublish').show("slow");
                    } else {
                        $('.btn-group.btn-action ._unpublish').hide("slow");
                    }
                    $('#rows').text(R.Project.ItemSelected.length);
                });
        }


        var delay = 0;
        $('#news-sldebar .navigation li.sub-item').off('click').on('click',
            function () {

                R.Project.ZoneType = parseInt($(this).attr('data-type'));
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Project.MainInit();
                },
                    200);

            });

        $('#btn-add-zone').off('click').on('click',
            function () {
                R.Project.Edit(0);

            });


        $('#items .item i.icon-bin').off('click').on('click',
            function () {
                var $this = $(this);
                $.confirm({
                    title: 'Thông báo !',
                    type: 'red',
                    content: 'Xác nhận xóa',
                    animation: 'scaleY',
                    closeAnimation: 'scaleY',
                    buttons: {
                        yes: {
                            text: 'Xóa',
                            keys: ['ESC'],
                            action: function () {
                                $this.closest('.item').remove();
                            }
                        },
                        cancel: {
                            text: 'Đóng',
                            btnClass: 're-btn re-btn-default'
                        }
                    }

                });

            });


        $('#items .item i.icon-bin').hover(function () {
            $(this).closest('.item').find('._field').css('opacity', .3)

        },
            function () {
                $(this).closest('.item').find('._field').css('opacity', 1)
            });

        $('#addItemBtn').off('click').on('click',
            function () {
                var stepid = 'step-content' + Date.now();
                var stepCount = parseInt($('#items .item').length) + 1;

                $('#items').append(R.Project.StepHtml({
                    id: stepid,
                    stepName: 'Bước ' + stepCount + ': ',
                    content: ''
                }));
                var editor = CKEDITOR.replace(stepid,
                    {
                        allowedContent: true,
                        toolbar: 'basic'
                    });
                editor.focusManager.focus();
                R.Project.RegisterEvents();

            });



        $('#items .item .avatar').off('click').on('click',
            function () {
                var $this = $(this);

                R.Image.SingleUpload($this, function () {

                    //process
                }, function (response, status) {

                    //success
                    if (!response.success) {
                        $.confirm({
                            title: 'Thông báo !',
                            type: 'red',
                            content: response.messages,
                            animation: 'scaleY',
                            closeAnimation: 'scaleY',
                            buttons: {
                                cancel: {
                                    text: 'Đóng',
                                    btnClass: 're-btn re-btn-default'
                                },
                                yes: {
                                    isHidden: true, // hide the button
                                    keys: ['ESC'],
                                    action: function () {

                                    }
                                }
                            }

                        });
                        return;
                    }

                    if (response.success && response.error == 200) {
                        var img = response.path;
                        $this.closest('.avatar').find('img').attr({
                            'src': img + '?w=150&h=100&mode=crop',
                            'data-img': response.name
                        });
                    }


                });

            });

    },
    StepHtml: function (data) {

        return ' <div class="item _add">' +
            '<i class="icon-bin"></i>' +
            '<div class="control-label col-lg-2 text-center _field">' +
            '<div class="avatar"  style="width: auto;height: auto">' +
            '(1500px*630px)<input accept="image/*" id="img_attack_' + data.id + '" type="file" name="files[]" style="display: none" />' +
            '  <label for="img_attack_' + data.id + '"><img  src="/CMS/Themes/Images/nothumb.JPG"/></label> </div></div><div class="col-lg-10 _field">' +
            '<input type="text" name="basic" class="form-control title" value="" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">' +
            '<div class="content-tiem" style="padding-top: 10px">' +
            '<textarea class="editor" id=' + data.id + '></textarea>' +
            '</div></div></div>';




    },

    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "project",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Project.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: $('#zone-filer-ddl').val(),
            pageindex: R.Project.PageIndex,
            pagesize: R.Project.PageSize,
            status: R.Project.NewsStatus,
            from: R.Project.startDate,
            to: R.Project.endDate,
            type: $('#zone-type-ddl').val()
        }
        R.Post({
            params: data,
            module: "project",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {
                    R.Project.ItemSelected = [];
                    $(el).find('.datatable-scroll._list').html(res.Content);
                    $('.btn-group.btn-action').hide();
                    R.CMSMapZone('#data-list');

                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                    //pager
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Project.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Project.PageIndex) * parseInt(R.Project.PageSize) - parseInt(R.Project.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Project.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Project.PageIndex = page;
                                R.Project.XNews();
                            }
                        });
                    }
                    //end pager



                    R.Project.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    XEdit: function (id) {

        $('body').RModuleBlock();
        R.Post({
            params: {
                id: id
                // zoneType: R.Project.ZoneType
            },
            module: "project",
            ashx: 'modulerequest.ashx',
            action: "xedit",
            success: function (res) {

                if (res.Success) {
                    //   $('#zone-form-edit').html(res.Content);

                    R.ShowOverlayFull({ content: res.Content }, function () {


                    }, function () {

                    });

                    R.Project.XNewsId = id;
                    //Bind kich thuoc san pham
                    if (id > 0) {
                        var size = $('#sizeProductContainer').data('size').split(";");
                        //alert(size);
                        $('#sizeProductContainer').html('');
                        var htm = '';
                        size.forEach(element => {
                            if (element != "") {
                                htm += '<div class="col-lg-3 sizeProductDiv">';
                                htm += '<input type="text" class="form-control product_size" value="' + element + '" required="required" placeholder="Nhập kích thước SP" /> <span class="product_size_delete">x</span>'
                                htm += '</div>'
                            }
                            if (element == "") {
                                htm += '<div class="col-lg-3 sizeProductDiv">';
                                htm += '<input type="text" class="form-control product_size" value="" required="required" placeholder="Nhập kích thước SP" /> <span class="product_size_delete">x</span>'
                                htm += '</div>'
                            }

                        })
                        $('#sizeProductContainer').append(htm);
                    }
                    $(".product_size_delete").off('click').on('click', function () {
                        $(this).parent().remove();
                    })
                    $('#attack-thumb .avatar').off('click').on('click', function () {
                        R.Image.SingleUpload($('#attack-thumb .avatar'),
                            function () {
                                //process
                            }, function (response, status) {
                                $('#attack-thumb .avatar').RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {

                                    var img = response.path;
                                    $('#attack-thumb .avatar img').attr({
                                        'src': img + '?w=150&h=100&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    $('#attack-thumb-2 .avatar').off('click').on('click', function () {
                        R.Image.SingleUpload($('#attack-thumb-2 .avatar'),
                            function () {
                                //process
                            }, function (response, status) {
                                $('#attack-thumb-2 .avatar').RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {

                                    var img = response.path;
                                    $('#attack-thumb-2 .avatar img').attr({
                                        'src': img + '?w=150&h=100&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        }); $('#groupDdl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');

                            $('#groupDdl').val($('#groupDdl').attr('data'));
                            $('#groupDdl').trigger('fselect:updated');
                            $('#datetxt').daterangepicker({
                                autoUpdateInput: false,
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Project.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));
                            });
                           

                        } else {
                            $('#datetxt').daterangepicker({
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Project.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));

                            });
                            $('#datetxt').attr('data', moment().format('YYYY-MM-DD h:mm A'));
                           


                        }



                        autosize(document.querySelectorAll('textarea#Sapo'));
                        R.ScrollAutoSize('#zone-content', function () {
                            return $(window).height() - 10;
                        }, function () {
                            if (CKEDITOR.instances["contentCkeditor"]) {
                                CKEDITOR.instances["contentCkeditor"].destroy();
                            }
                            CKEDITOR.replace('contentCkeditor', { toolbar: 'iweb' });

                            $('#items .item').each(function (i, v) {
                                var ckeditorId = $(v).find('.editor').attr('id');
                                CKEDITOR.replace('' + ckeditorId + '', { toolbar: 'basic' });

                            });
                            return 'auto';
                        }, {});
                    }, 200);

                    $('body').RModuleUnBlock();
                    R.Project.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XSave: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "project",
            ashx: 'modulerequest.ashx',
            action: "xsave",
            success: function (res) {
                if (res.Success) {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.CloseOverlayFull();
                    // R.Project.NewsStatus = 4;
                    R.Project.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
                R.Project.RegisterEvents();
            }
        });
    },

}
$(function () {
    R.Project.Init();
});
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.Account;
using Mi.BO.Base.Security;
using Mi.Common;
using Mi.Entity.Base.Security;
using Golden.Core.Helper;

namespace Golden.CMS.Modules.Authenticate.Actions
{
    public class AuthenticateAction : ActionBase
    {
        #region Implementation of IAction

        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }
        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();

            switch (functionName)
            {
                case "login":
                    responseData = DoLogin();
                    break;
                case "logout":
                    responseData = DoLogout();
                    break;
                case "prelogin":
                    responseData = PreLogin();
                    break;
                case "register":
                    //      responseData = Register();
                    break;
                case "fb_login":
                    responseData = FbLogin();
                    break;
                case "get_capcha":
                    responseData = GetCapcha();
                    break;
                case "":
                    responseData.Success = false;
                    responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                    break;
            }

            return responseData;
        }

        #endregion

        private ResponseData FbLogin()
        {
            ResponseData responseData = new ResponseData();
            var requestTime = DateTime.Now.ToShortDateString();
            var token = GetQueryString.GetPost("token", string.Empty);
            var fid = GetQueryString.GetPost("fid", string.Empty);
            var param = "{\"facebookId\":\"" + fid + "\", \"facebookToken\":\"" + token + "\"}"; ;
            string command = "tempRegister";
            var sign = UIHelper.EncriptMd5("B1368|" + command + "|" + requestTime + "|" + param + "|" + Constants.CLIENT_SECRET);
            var paramSendRequest = "command=" + command + "&requestTime=" + requestTime + "&sign=" + sign + "&params=" + param;
            responseData = UIHelper.SendHttpReqest(paramSendRequest);
            // responseData.Content = ConfigurationManager.AppSettings["XAPIHttpRequest"] + "?" +  paramSendRequest;
            responseData.Content = "facebookId=" + fid + "&facebookToken=" + token;
            return responseData;
        }

        //private ResponseData Register()
        //{
        //    var regexUserName = new Regex("^[a-zA-Z0-9]*$", RegexOptions.IgnoreCase);
        //    ResponseData responseData = new ResponseData();
        //    var name = GetQueryString.GetPost("name", string.Empty);
        //    var pass = GetQueryString.GetPost("pass", string.Empty);
        //    var guid = GetQueryString.GetPost("guid", string.Empty);
        //    var captcha = GetQueryString.GetPost("captcha", string.Empty);
        //   // var image = HttpRuntime.Cache[guid] as CaptchaImage;
        //    var image="";
        //    try
        //    {
        //        // check user
        //        //bool isExist = false;
        //        //XDataBo.CheckXGameExist(name, ref isExist);
        //        //if (isExist)
        //        //{
        //        //    responseData.Message = "Tài khoản này đã tồn tại !";
        //        //    responseData.Success = false;
        //        //    return responseData;
        //        //}

        //        System.Text.RegularExpressions.Match match = regexUserName.Match(name);
        //        if (!match.Success || name.Length < 6 || name.Length > 15)
        //        {
        //            responseData.Message = "Tên đăng nhập từ 6-15 ký tự, chỉ bao gồm chữ và số.";
        //            responseData.Success = false;
        //            return responseData;
        //        }
        //        if (pass.Length < 6 || pass.Length > 30)
        //        {
        //            responseData.Message = "Mật khẩu lớn hơn 6 và nhỏ hơn 30 ký tự !";
        //            responseData.Success = false;
        //            return responseData;
        //        }
        //        if (!string.IsNullOrEmpty(captcha))
        //        {
        //            if (string.Compare(image.Text, captcha, true) == 0)
        //            {
        //                var requestTime = DateTime.Now.ToShortDateString();
        //                var param = "{\"registerName\":\"" + name + "\", \"registerPassword\":\"" + pass + "\"}";
        //                string command = "tempRegister";
        //                var sign = UIHelper.EncriptMd5("B1368|" + command + "|" + requestTime + "|" + param + "|" + Constants.CLIENT_SECRET);
        //                var paramSendRequest = "command=" + command + "&requestTime=" + requestTime + "&sign=" + sign + "&params=" + param;
        //                responseData = UIHelper.SendHttpReqest(paramSendRequest);

        //                string key = "hiephvdepzai123$#@^&^$";
        //                string secret = ShaHash(pass, key);
        //                responseData.Content = "username=" + name + "&password=" + secret;

        //            }
        //            else
        //            {
        //                responseData.Message = "Mã xác thực không đúng!";
        //                responseData.Success = false;
        //                return responseData;
        //            }
        //        }
        //        else
        //        {
        //            responseData.Message = "Nhập mã xác thực.";
        //            responseData.Success = false;
        //            return responseData;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        responseData.Message = ex.Message;
        //        responseData.Success = false;
        //        return responseData;
        //    }

        //    return responseData;
        //}


        private ResponseData DoLogin()
        {
            var responseData = new ResponseData();
            var context = HttpContext.Current;

            var username = GetQueryString.GetPost("email", string.Empty).ToLower();
            var password = GetQueryString.GetPost("pass", string.Empty);
            try
            {
                var res = PolicyBo.ValidAccount(username, password);
                if (res.Success)
                {
                    var enableOTP = false;
                    responseData.Data = !enableOTP ? string.Empty : username;
                    responseData.Message = Constants.MESG_SUCCESS;
                    responseData.Success = res.Success;
                    responseData.Content = "";
                    if (!enableOTP)
                    {
                        FormsAuthentication.Initialize();
                        FormsAuthentication.SetAuthCookie(username, false);
                        var maxRole = EnumPermission.ArticleReporter;
                        if (Role.HasRoleOnPermission(EnumPermission.ArticleAdmin, username))
                        {
                            maxRole = EnumPermission.ArticleAdmin;
                        }
                        else if (Role.HasRoleOnPermission(EnumPermission.ArticleEditor, username))
                        {
                            maxRole = EnumPermission.ArticleEditor;
                        }
                        HttpContext.Current.Session["CurrentRole"] = maxRole;
                    }
                }
                else
                {

                    var obj = UserBo.GetUserByUsername(username);
                    if (obj != null)
                    {
                        if (obj.Status == (int)UserStatus.UnActive)
                        {
                            responseData.Message = Constants.MESG_USER_UNACTIVE;
                        }
                        else
                        {
                            responseData.Message = Constants.MESG_USER_NOT_VAIL;
                        }
                    }
                    else
                    {

                        responseData.Message = Constants.MESG_USER_NOT_EXIST;
                    }
                    responseData.Success = res.Success;


                }
            }
            catch (Exception ex)
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_LOGIN_FAIL;
                Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
            }

            context.Session.Remove(Constants.SURFIX_LOGIN);

            return responseData;
        }

        private static ResponseData DoLogout()
        {
            var responseData = new ResponseData();
            HttpContext.Current.Session.Clear();
            FormsAuthentication.SignOut();
            responseData.Success = true;
            responseData.Message = Constants.MESG_SUCCESS;
            return responseData;
        }
        public static bool HasPermission(EnumPermission permission, string username)
        {
            return Role.HasRoleOnPermission(permission, username) || PolicyProviderManager.Provider.IsFullPermission();
        }
        private static ResponseData PreLogin()
        {
            var responseData = ConvertResponseData.CreateResponseData(new WcfResponseData(),
                                                                      "\\Modules\\Authenticate\\Templates\\FormLogin.aspx");
            return responseData;
        }
        private static ResponseData GetCapcha()
        {
            var responseData = ConvertResponseData.CreateResponseData("", 1, "\\Pages\\Controls\\Capcha.aspx");
            responseData.Success = true;
            return responseData;
        }
        protected static string GetIPAddress()
        {
            HttpContext context = HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        private static string ByteToString(IEnumerable<byte> data)
        {
            return string.Concat(data.Select(b => b.ToString("x2")));
        }
        private static string ShaHash(string value, string key)
        {
            using (var hmac = new HMACSHA1(Encoding.ASCII.GetBytes(key)))
            {
                return ByteToString(hmac.ComputeHash(Encoding.ASCII.GetBytes(value)));
            }
        }
        private byte[] AESEncryptBytes(byte[] clearBytes, byte[] passBytes, byte[] saltBytes)
        {
            byte[] encryptedBytes = null;

            // create a key from the password and salt, use 32K iterations – see note
            var key = new Rfc2898DeriveBytes(passBytes, saltBytes, 32768);

            // create an AES object
            using (Aes aes = new AesManaged())
            {
                // set the key size to 256
                aes.KeySize = 256;
                aes.Key = key.GetBytes(aes.KeySize / 8);
                aes.IV = key.GetBytes(aes.BlockSize / 8);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }
            return encryptedBytes;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="RBLand.Pages.FAQ" %>

<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Register Src="~/Pages/Controls/Sub/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>
<%@ Register Src="~/Pages/Controls/Sub/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="fix-bg-header bg-project">
        <h1 class="page-header align-self-center m-auto">Hỏi đáp</h1>
        <style>
            .fix-bg-header { background: url(/Uploads/<%=!string.IsNullOrEmpty(Zone.Avatar)?Zone.Avatar:"hoidap.jpg" %>) no-repeat center center; background-size: cover; }
        </style>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="hoi-dap.htm">Hỏi đáp</a></li>
            </div>
        </ol>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-7 col-12">
                    <div class="list-job mb-4">
                        <asp:Repeater runat="server" ID="Rpt">
                            <ItemTemplate>
                                <div class="card mb-2">
                                    <div class="card-header d-flex" id="headingOne-<%# Eval("Id") %>" data="<%# Eval("Id") %>">
                                        <span class="stt mr-3">
                                            <%#  Utils.ToInt32Return0(Request.QueryString["page"])==0 ? Container.ItemIndex+1: Container.ItemIndex+1+(10*  Utils.ToInt32Return0(Request.QueryString["page"])) -10 %>
                                        </span>
                                        <p class="title mb-0 align-self-center _item" >  <span><%# Eval("Title") %></span>
                                            <%--<a rel="nofollow" href="#collapseOne-<%# Eval("Id") %>">
                                              
                                            </a>--%>
                                        </p>
                                        <span class="align-self-center ml-auto mr-4"><i class="fas fa-plus fa-minus"></i></span>
                                    </div>

                                    <div id="collapseOne-<%# Eval("Id") %>" class="collapse  ">
                                        <div class="card-block"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row">

                        <uc1:UIPager runat="server" ID="UIPager" PageShow="6" PageSize="10" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <uc1:SideBarRight runat="server" ID="SideBarRight" />

                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="/Core/InitClientConstants/Constants.ashx"></script>
    <script>
        var R = {};
    </script>
    <script src="/Libs/core.js"></script>
    <script>
        scrollBlock();
        $(function () {
            $('.card-header').click(function () {
                var $this = $(this);
                $this.find('i').toggleClass('fa-plus');
                $this.next('.collapse').toggleClass('show');
                //return false;

                var contents = $this.closest('.card').find('.card-block');
                if (contents.html().length > 0) {
                    return false;
                }
                var id = $this.attr('data');
              //  $this.RLoadingModule();
                R.Post({
                    params: { id:id },
                    module: "ui-action",
                    ashx: 'modulerequest.ashx',
                    action: "faq-detail",
                    success: function (res) {
                        if (res.Success) {
                            contents.html(res.Content);

                        }
                       // $('.card-header').RLoadingModuleComplete();
                    }
                });
            });
        });
    </script>
</asp:Content>

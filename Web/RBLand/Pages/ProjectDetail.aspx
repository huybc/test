﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="ProjectDetail.aspx.cs" Inherits="RBLand.Pages.ProjectDetail" %>

<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Register Src="~/Pages/Controls/Sub/BreadCrumbs.ascx" TagPrefix="uc1" TagName="BreadCrumbs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
        <meta property="og:title" content="<%= obj.Title %>" />
        <meta property="og:description" content="<%= obj.Sapo%>" />
        <meta property="og:image" content="https://rbland.vn/Uploads/<%= obj.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        var content = ProjectDetailBo.GetByArticleId(obj != null ? obj.Id : 0).ToArray();
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                    "headline":"<%=!string.IsNullOrEmpty(obj.TitleSeo)?obj.TitleSeo:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.DistributionDate %>",
                                    "dateModified":"<%=obj.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"rbland"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"rbland.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=obj.Sapo %>"
                            }
    </script>
    <style>
        @media (max-width: 768px)
        {.bg-project.bg-pd.s {
    min-height: 50vh !important;
}
        .bg-project { background: url(/Uploads/<%=!string.IsNullOrEmpty(obj.Avatar)?obj.Avatar:"bg-list-project.jpg" %>) no-repeat center center; background-size: cover; }
    </style>
    <section class="bg-project bg-pd s">
        <div class="project" >
            <h1><%=obj.Title%></h1>
            <p><%=obj.Sapo %></p>
        </div>
        <%--   <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="/du-an">Dự án</a></li>
                <li class="breadcrumb-item "><a href="#"><%=obj.Title%></a></li>
            </div>
        </ol>--%>
        <uc1:BreadCrumbs runat="server" ID="BreadCrumbs" />
    </section>
    <div id="projectDetail">

        <section class="w-ct-prj-r-4 py-5">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="heading">
                        <h3 class="mb-4 ">Thông tin dự án</h3>
                        <%-- <p class="w-75 m-auto">
                        <%=obj.Sapo %>
                    </p>--%>
                    </div>
                    <div class="col-md-auto col-lg-10 col-sm-12 col-sm-12 col-12 align-self-center">
                        <%=obj.Body %>
                    </div>
                    <%-- <div class="col-lg-6 col-sm-6 col-sm-6 col-12 bd align-self-center">
                    <div class="content ">
                        <p><strong>CHÍNH SÁCH HỖ TRỢ VAY VỐN MUA</strong></p>

                        <p>Chủ đầu tư Condotel Eastin đã liên kết chặt chẽ với ngân hàng để đưa ra những chính sách tốt nhất. </p>

                        <p>Khách Vay Ngân Hàng</p>
                        <ul class="mb-4">
                            <li>Thời gian vay: Tối đa 20 năm</li>
                            <li>Tỷ lệ vay: 50% giá trị Căn Hộ</li>
                            <li>Hỗ trợ lãi suất 0% tới khi bàn giao nhà.</li>
                        </ul>

                        <ul class="mb-4">
                            <li>Chiết khấu ngay 35% đối với khách nhận lợi nhuận gộp 5 năm</li>
                            <li>Chiết Khấu ngay 15% đối với khách hàng không nhận cam kết tối thiểu</li>
                            <li>Chiết khấu ngay 9,5% đối với khách hàng thanh toán sớm bằng vốn tự có</li>
                            <li>Chiết khấu lên đến 3% đối với khách hàng mua số lượng lớn.</li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-6 col-sm-6 col-sm-6 col-12 align-self-center">
                    <div class="content ">
                        <p><strong>CAM KẾT LỢI NHUẬN</strong></p>

                        <p>Condotel Eastin Phát Linh Hạ Long cam kết được quản lý, vận hành bởi Tập đoàn Absolute Hotels Services (AHS) có kinh nghiệm nhằm gia tăng lợi nhuận cho khách hàng.</p>

                        <p>
                            <strong>Cam kết lợi nhuận:</strong> Khách hàng được chia sẻ 85% lợi nhuận cho thuê phòng ( điều kiện và điều khoản đi kèm) đảm bảo 10% trong 5 năm đầu tiên (giá trị căn hộ không bao gồm các loại thuế phí, chiết khấu).
                        </p>

                        <p><strong>Cam kết kì nghỉ:</strong> Sử dụng 15 đêm miễn phí tại dự án ( Có điều khoản và điều kiện theo sự sắp xếp của bên đơn vị vận hành).</p>

                        <p><strong>Kỳ hạn thanh toán:</strong> 1 năm 1 lần</p>
                    </div>
                </div>--%>
                </div>

            </div>
        </section>
        <section class="w-ct-prj-r-5">
            <%
                var counts = content.Count();
                if (counts == 0)
                {
            %>
            <section class="bg-number py-5" id="sec">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.Invest%><%=string.IsNullOrEmpty(obj.Invest)? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>Giá dự kiến</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.HoursOfWork %><%=obj.HoursOfWork==0? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>DIỆN TÍCH XÂY DỰNG (m2)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.SurfaceArea%><%=obj.SurfaceArea==0? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>SỐ LƯỢNG SẢN PHẨM</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.Budget %></span>
                                </div>
                                <div class="text">
                                    <h4>THỜI ĐIỂM HOÀN THÀNH</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <%} %>
            <%
                for (int i = 0; i < counts; i++)
                {


            %>
            <% int index;
                if (counts == 1)
                {
                    index = 0;
                }
                else
                {
                    index = 1;
                }
                if (i == index)
                { %>

            <section class="bg-number py-5" id="sec">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.Invest%><%=string.IsNullOrEmpty(obj.Invest)? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>Giá dự kiến</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.HoursOfWork %><%=obj.HoursOfWork==0? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>DIỆN TÍCH XÂY DỰNG (m2)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.SurfaceArea%><%=obj.SurfaceArea==0? "":"<sup>+</sup>" %></span>
                                </div>
                                <div class="text">
                                    <h4>SỐ LƯỢNG SẢN PHẨM</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                            <div class="item">
                                <div class="number">
                                    <span class="numeric"><%=obj.Budget %></span>
                                </div>
                                <div class="text">
                                    <h4>THỜI ĐIỂM HOÀN THÀNH</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <% }
            %>
            <% if (isEven(i))
                {
            %>
            <div class="row mx-0">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12  px-0">
                    <div class="image-project " style="background: url('/uploads/<%=content[i].Image %>') no-repeat center center;">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0 bg-ededed">
                    <div class="content p-5">
                        <h3 class="text-uppercase"><%=content[i].Title %></h3>
                        <p><%=content[i].Content %></p>
                    </div>
                </div>
            </div>
            <% }
                else
                { %>
            <div class="row mx-0">

                <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0 bg-ededed">
                    <div class="content p-5">
                        <h3 class="text-uppercase"><%=content[i].Title %></h3>
                        <p><%=content[i].Content %></p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12  px-0">
                    <div class="image-project" style="background: url('/uploads/<%=content[i].Image %>') no-repeat center center">
                        <%-- <img src="" style="width: 100%" />--%>
                    </div>
                </div>
            </div>
            <% } %>

            <%} %>
            <section class="w-ct-prj-r-contact py-5">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-5 col-sm-10 col-12 ">
                            <h3>Liên hệ chúng tôi</h3>
                            <form>
                                <div class="form-group text-center">
                                    <input type="text" class="form-control mb-3" id="txtFullName" placeholder="Họ tên"/>
                                    <input type="text" class="form-control mb-3" id="txtEmail" placeholder="Hòm thư" />
                                    <input type="text" class="form-control mb-3" id="txtPhone" placeholder="Điện thoại"/>
                                    <button type="button" class="btn btn-send mb-5" id="btnSend">Gửi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="text-right" style="padding-top: 40px">
                    <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                </div>
                <div class="comment-facebook">
                    <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="auto" data-numposts="5"></div>
                </div>
            </div>
        </section>

    </div>
    <div class="container">
        <div class="row" style="padding-top: 30px">
            <div class="text-uppercase color-144a91 mb-5">
                <h4>Dự án liên quan</h4>
                <div class="bd-1">
                    <div class="bd-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="list-project py-5">
        <div class="container">
            <div class="row">
                <asp:Repeater runat="server" ID="Rpt">
                    <ItemTemplate>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image">
                                    <a href="/du-an/<%# Page.RouteData.Values["zoneName"]!=null?Page.RouteData.Values["zoneName"].ToString():"" %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm" class="" title="<%# Eval("Title")%>">
                                        <img src="/uploads/thumb/<%# Eval("Avatar") %>" alt="<%# Eval("Title")%>" class="img-fluid" />
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="/Themes/images/list-project-icon.png" alt="<%# Eval("Title")%>" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <h2 class="align-self-center title-h2">
                                            <a href="/du-an/<%# Page.RouteData.Values["zoneName"]!=null?Page.RouteData.Values["zoneName"].ToString():"" %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm" class="" title="<%# Eval("Title")%>"><%# Eval("Title")%></a>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $(function () {
            
            $('#btnSend').off('click').on('click',
                function () {

                    var mail = $('#txtEmail').val();
                    var note = '<%=HttpContext.Current.Request.Url.AbsoluteUri %>';

                  
                    var name = $("#txtFullName").val();
                    if (name == "") {
                        $("#fullname_err").show();
                        $("#txtFullName").focus();
                        return false;
                    }
                    
                    var phone = $("#txtPhone").val();
                    if (phone == "") {
                        $("#phone_err").show();
                        $("#txtPhone").focus();
                        return false;
                    }
                   

                    var data = {
                        id: 0,
                        phone:phone,
                        name: name,
                        email: mail,
                        type: 'project',
                        note: note
                    }
                    $('body').RLoadingModule();
                    R.Post({
                        params: data,
                        module: "ui-action",
                        ashx: 'modulerequest.api',
                        action: "save",
                        success: function (res) {
                            if (res.Success) {
                                alert("Gửi thành công");
                                location.reload();
                            } else {
                                alert("Gửi thất bại");
                            }
                            $('body').RLoadingModuleComplete();

                        }
                    });
                });

        });
    </script>

</asp:Content>

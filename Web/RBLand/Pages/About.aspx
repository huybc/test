﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="RBLand.Pages.About" %>
<%@ Register Src="~/Pages/Controls/Slider.ascx" TagPrefix="uc1" TagName="Slider" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <link href="/Themes/css/about.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style>
        .bg-about.bg-project{
                 background: url(/uploads/<%=UIHelper.GetConfigByName("BackgroudHeader") %>) no-repeat center center; 
        }
    </style>
    <section class="bg-about bg-project">
        <div class="bg-text text-center align-self-center m-auto">
            <h2 style="position: relative">RB LAND</h2>
            <h3>MỘT HÀNH TRÌNH PHÁT TRIỂN</h3>
        </div>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="gioi-thieu.htm">Về RBLAND</a></li>
            </div>
        </ol>
    </section>
    <section class="thu-ngo py-5">
        <div class="container">
            <div class="row">
                <%--                <div class="col-lg-5 col-md-5 col-sm-5 col-12">
                    <div class="text-center">
                        <img src="/Themes/images/thu-ngo.png" class="img-fluid" />
                    </div>
                </div>--%>
                <div class="col-lg-12 col-md-7 col-sm-7 col-12">
                    <div class="text">
                        <p>
                            <%=UIHelper.GetConfigByName("AboutUs") %>
                            
                        </p>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="bg-number py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="number">
                            <span> <%=UIHelper.GetConfigByName("Project") %></span><sup>+</sup>
                        </div>
                        <div class="text">
                            <h4>DỰ ÁN PHÂN PHỐI</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="number">
                            <span> <%=UIHelper.GetConfigByName("INVESTOR") %></span><sup>+</sup>
                        </div>
                        <div class="text">
                            <h4>CHỦ ĐẦU TƯ</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="number">
                            <span> <%=UIHelper.GetConfigByName("STAFF") %></span><sup>+</sup>
                        </div>
                        <div class="text">
                            <h4>CÁN BỘ & NHÂN VIÊN</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="number">
                            <span> <%=UIHelper.GetConfigByName("Successfull") %></span><sup>+</sup>
                        </div>
                        <div class="text">
                            <h4>GIAO DỊCH THÀNH CÔNG</h4>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="about-us py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="heading">
                        <h3 class="text-center mb-3">VỀ CHÚNG TÔI
                        </h3>
                        <p class="text-center ml-auto mr-auto mb-3">
                            <%=UIHelper.GetConfigByName("HappyClient") %>
                        </p>
                        <div class="d-flex justify-content-center mb-5">
                            <div class="tab">
                                <button class="tablinks tab-about active" onclick="openCity(event, 'TNVSM')" id="defaultOpen">Tầm nhìn và sứ mệnh</button>
                                <button class="tablinks tab-about" onclick="openCity(event, 'Diagram')">Sơ đồ tổ chức</button>
                            </div>
                        </div>


                    </div>
                </div>
                <div id="TNVSM" class=" tabcontent">
                    <div class="row">
                        <div class="col-lg-12 col-md-6 col-sm-5 col-12">
                            <p>
                                <%=UIHelper.GetConfigByName("OurMission") %>
                            </p>

                        </div>
                     
                    </div>
                </div>


                <div id="Diagram" class="tabcontent">
                    <img src="/uploads/<%=UIHelper.GetConfigByName("Diagram") %>" class="img-fluid" />
                    

                </div>

            </div>
        </div>
    </section>
    <%--<section class="why about py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center mb-3">Tại sao lựa chọn chúng tôi
                    </h3>
                    <p class="w-50 ml-auto mr-auto mb-5">
                        Chúng tôi luôn cố gắng không ngừng nghỉ để nâng cao kiến thức, mang đến cho khách hàng dịch vụ
                        tư vấn chuyên sâu, tận tâm
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <img src="/Themes/images/why1.png" class="img-fluid" />
                        </div>
                        <div class="text">
                            <h4>Dịch vụ chuyên nghiệp</h4>
                            <p>Chúng tôi luôn cố gắng không ngừng nghỉ để nâng cao kiến thức</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <img src="/Themes/images/why2.png" class="img-fluid" />
                        </div>
                        <div class="text">
                            <h4>Dịch vụ chuyên nghiệp</h4>
                            <p>Chúng tôi luôn cố gắng không ngừng nghỉ để nâng cao kiến thức</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <img src="/Themes/images/why3.png" class="img-fluid" />
                        </div>
                        <div class="text">
                            <h4>Dịch vụ chuyên nghiệp</h4>
                            <p>Chúng tôi luôn cố gắng không ngừng nghỉ để nâng cao kiến thức</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <img src="/Themes/images/why4.png" class="img-fluid" />
                        </div>
                        <div class="text">
                            <h4>Dịch vụ chuyên nghiệp</h4>
                            <p>Chúng tôi luôn cố gắng không ngừng nghỉ để nâng cao kiến thức</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>--%>
<section class="why about py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 class="text-center mb-5">Tại sao lựa chọn chúng tôi
                </h3>
            </div>
            <%
                var choose = ConfigBo.AdvGetByType((int)EnumSlide.ChooseUs, true).ToList();
                foreach (var obj in choose)
                {
            %>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a title="<%=obj.Name %>" href="<%=obj.Url %>">
                                <img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-fluid " alt="<%=obj.Name %>" /></a>
                        </div>
                        <div class="text">
                            <h4><a title="<%=obj.Name %>" href="<%=obj.Url %>"><%=obj.Name %></a></h4>
                            <p><%=obj.Content %></p>
                        </div>
                    </div>
                </div>
            <% } %>
        </div>
    </div>
</section>
    <uc1:Slider runat="server" ID="Slider" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        function openCity(evt, name) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(name).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>
</asp:Content>

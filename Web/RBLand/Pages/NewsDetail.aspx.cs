﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Tag;

namespace RBLand.Pages
{
    public partial class NewsDetail : System.Web.UI.Page
    {
        public NewsEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailById(newsId);
                    Page.Title = !string.IsNullOrEmpty(obj.TitleSeo)? obj.TitleSeo:obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsEntity();

                }
            }
        }
        protected string GetTags(long newsId)
        {
            var tags = CacheObjectBase.GetInstance<TagCached>().GetTagNewsByNewsId(newsId);
            string _strTag = "";

            foreach (var tag in tags)
            {
                if (tag.TagMode == (int)EnumTagType.ForNews)
                {
                    _strTag += "<a title=\"" + tag.Name + "\" href=\"/tag/" + tag.Url + ".htm\"><span class=\"arrow\"></span>" +
                               tag.Name + "</a>";

                }
            }
            return _strTag;
        }
    }
}
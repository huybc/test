﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Partner.ascx.cs" Inherits="RBLand.Pages.Controls.Partner" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<div class="client py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 class="text-center mb-5">Đối tác của chúng tôi</h3>
            </div>
        </div>
        <div class="swiper-container client">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <%
                    var objs = ConfigBo.AdvGetByType((int)EnumSlide.Partner,true).ToList();
                    foreach (var obj in objs)
                    {
                %>
                    <div class="swiper-slide">
                        <a title="<%=obj.Name %>" href="<%=obj.Url %>"><img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-fluid " alt="<%=obj.Name %>" /></a>
                    </div>
                <% } %>
            </div>

        </div>
    </div>
</div>

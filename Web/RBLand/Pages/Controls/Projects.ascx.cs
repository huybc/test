﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using Mi.Entity.Base.News;
using RBLand.Pages.Controls.Sub;

namespace RBLand.Pages.Controls
{
    public partial class Projects : System.Web.UI.UserControl
    {
        public int totalRow = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var alias = Page.RouteData.Values["zoneName"];
                var id = Page.RouteData.Values["id"];
                var productType = Page.RouteData.Values["productType"];
                var page = Utils.ToInt32Return0(Request.QueryString["page"]);
                if (alias != null)
                {
                    var objs= NewsBo.SearchByShortUrl(alias.ToString(), page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
                    if (id != null)
                    {
                        objs = objs.Where(it => it.Id != id.ToInt32Return0());
                    }
                    Rpt.DataSource = objs;
                    Rpt.DataBind();
                }
                else
                {
                  
                    if (productType != null)
                    {
                        var objs = NewsBo.uspSectNewsByProductType(productType.ToString(), page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
                        if (id != null)
                        {
                            objs = objs.Where(it => it.Id != id.ToInt32Return0());
                        }
                        Rpt.DataSource = objs;
                        Rpt.DataBind();
                    }
                }

                
                UIPager.TotalItems = totalRow;

            }
        }
    }
}
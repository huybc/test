﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using Mi.Common;
using RBLand.Core.Helper;

namespace RBLand.Pages.Controls
{
    public partial class News : System.Web.UI.UserControl
    {

        public int totalRow = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var alias = Page.RouteData.Values["zoneName"];
                var page = Utils.ToInt32Return0(Request.QueryString["page"]);
                Rpt.DataSource = NewsBo.uspSectNewsInTreeWithZone(alias.ToString(), page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
                Rpt.DataBind();
                UIPager.TotalItems = totalRow;

            }
        }
    }
}
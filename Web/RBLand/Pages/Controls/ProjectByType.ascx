﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectByType.ascx.cs" Inherits="RBLand.Pages.Controls.ProjectByType" %>
<%@ Register TagPrefix="uc1" TagName="UIPager" Src="~/Pages/Controls/Sub/UIPager.ascx" %>
<section class="list-project py-5">
    <div class="container">
        <div class="row">
            <asp:Repeater runat="server" ID="Rpt">
                <ItemTemplate>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item">
                            <div class="image">
                                <a href="/du-an/<%# Eval("ShortURL") %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm" class="" title="<%# Eval("Title")%>">
                                <img src="/uploads/thumb/<%# Eval("Avatar") %>" alt="<%# Eval("Title")%>" class="img-fluid" />
                            </div>
                            <div class="text ml-5 pr-4 d-flex">
                                <img src="/Themes/images/list-project-icon.png" alt="<%# Eval("Title")%>" class="img-fluid mr-4" />
                                <div class="d-flex">
                                    <h2 class="align-self-center title-h2">
                                        <a href="/du-an/<%# Eval("ShortURL") %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm" class="" title="<%# Eval("Title")%>"><%# Eval("Title")%>222</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>
            <div class="col-lg-12 col-12">
                <div class="row float-lg-right">
                    <uc1:UIPager runat="server" ID="UIPager" PageSize="15" PageShow="5" />
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            </div>
        </div>
    </div>
</section>

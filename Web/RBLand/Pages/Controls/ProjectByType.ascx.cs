﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using RBLand.Pages.Controls.Sub;

namespace RBLand.Pages.Controls
{
    public partial class ProjectByType : System.Web.UI.UserControl
    {
        public int totalRow = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var productType = Page.RouteData.Values["productType"];
                var page = Utils.ToInt32Return0(Request.QueryString["page"]);


                if (productType != null)
                {
                    Rpt.DataSource = NewsBo.uspSectNewsByProductType(productType.ToString(), page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
                    Rpt.DataBind();
                }


                UIPager.TotalItems = totalRow;

            }
        }
    }
}
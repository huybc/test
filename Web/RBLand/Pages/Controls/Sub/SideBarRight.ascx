﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SideBarRight.ascx.cs" Inherits="RBLand.Pages.Controls.Sub.SideBarRight" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<div class="menu-left  mb-4">
    <ul class="list-group ">
        <li class="list-group-item heading-menu-news">Danh mục</li>
        <li class="list-group-item bg-0c3f67"><a href="/tin-tuc.htm">Tin tức</a><i class="fas fa-chevron-right ml-auto"></i></li>
        <%--  <li class="list-group-item bg-0c3f67"><a href="/co-dong.htm">Cổ đông</a><i class="fas fa-chevron-right ml-auto"></i></li>
        <li class="list-group-item bg-0c3f67"><a href="/gioi-thieu.htm">RBland</a><i class="fas fa-chevron-right ml-auto"></i></li>--%>
        <li class="list-group-item bg-0c3f67"><a href="/du-an-hot">Dự án</a><i class="fas fa-chevron-right ml-auto"></i></li>
        <li class="list-group-item bg-0c3f67"><a href="/tu-van">Tư vấn</a><i class="fas fa-chevron-right ml-auto"></i></li>
        <li class="list-group-item bg-0c3f67"><a href="/phan-tich-nhan-dinh">Phân tích thị trường</a><i class="fas fa-chevron-right ml-auto"></i></li>
        <li class="list-group-item bg-0c3f67"><a href="/tuyen-dung.htm">Tuyển dụng</a><i class="fas fa-chevron-right ml-auto"></i></li>
    </ul>
</div>
<%
    var objs = ConfigBo.AdvGetByType((int)EnumSlide.SideBarTop, true).ToList();
    foreach (var obj in objs)
    {
%>
<div class="py-3 qc-1 mb-4">
    <div class="heading-menu-news"><%=obj.Name %></div>
    <div class="px-2 bg-0c3f67 pb-2">
        <a href="<%=obj.Url %>">
            <img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-thumbnail my-3"></a>
        <p><%=obj.Content %></p>
    </div>
</div>
<% } %>

<%
    var objs2 = ConfigBo.AdvGetByType((int)EnumSlide.SideBottom, true).ToList();
    foreach (var obj in objs2)
    {
%>
<div class="qc-2">
    <a href="<%=obj.Url %>">
        <img alt="<%=obj.Name %>" src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-thumbnail my-3"></a>
</div>
<% } %>


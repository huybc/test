﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumbs.ascx.cs" Inherits="RBLand.Pages.Controls.Sub.BreadCrumbs" %>
<ol class="breadcrumb ">
    <div class="container p-0">
        <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
        <%=BreadCrumb %>
    </div>
</ol>
<script type="application/ld+json">
        {  
           "@context":"http://schema.org",
           "@type":"BreadcrumbList",
           "itemListElement":[  
              <%=BreadCrumbItem %>
           ]
        }
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UIPager.ascx.cs" Inherits="RBLand.Pages.Controls.Sub.UIPager" %>
<% if (EndPage > 1)
    { %>
<ul class="pagination r_page">
    <%if (CurrentPage > 1)
        { %>
    <li>
        <a class="first" title="Đầu" href="<%=path %><%=QueryKey %>1"><i class="fa fa-angle-double-left"></i></a>
    </li>
    <li>
        <a class="prev" title="Trước" href="<%=path %><%=QueryKey %><%=CurrentPage-1  %>"><i class="fa fa-angle-left"></i></a>
    </li>
    <% } %>

    <% for (int i = StartPage; i <= EndPage; i++)
        {
            if (CurrentPage == i)
            {
            %>
                <li class="<%=CurrentPage==i?"active":"" %>">
                    <a href="javascript:void(0)"><%=i %></a>
                </li>
            <% }
             else
            { %>
                <li>
                    <a href="<%=path %><%=QueryKey %><%=i%>"><%=i %></a>
                </li>
            <% } %>

      <%} %>


    <%if (CurrentPage < TotalPages)
        { %>
    <li>
        <a title="Sau" class="next" href="<%=path %><%=QueryKey %><%=CurrentPage+1  %>"><i class="fa fa-angle-right"></i></a>
    </li>
    <li>
        <a class="last" title="Cuối" href="<%=path %><%=QueryKey %><%=TotalPages %>"><i class="fa fa-angle-double-right"></i></a>
    </li>
    <% } %>
</ul>
<% } %>
      
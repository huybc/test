﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;

namespace RBLand.Pages.Controls.Sub
{
    public partial class BreadCrumbs : System.Web.UI.UserControl
    {
        public string BreadCrumb = string.Empty;
        public string BreadCrumbItem = string.Empty;

        [Description("")]
        [Browsable(true)]
        [DefaultSettingValue("")]
        public int ZoneId
        {
            get;
            set;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var currentDomain = HttpContext.Current.Request.Url.Authority;
            if (Page.RouteData.Values["zoneName"] != null)
            {
                var objZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneByAlias(Page.RouteData.Values["zoneName"].ToString());
                if (objZone != null)
                {
                    ZoneId = objZone.Id;
                    var objs = CacheObjectBase.GetInstance<ZoneCached>().GetBreadCrumbByCateoryId(ZoneId);
                    int i = 0;
                    foreach (var entity in objs)
                    {
                        BreadCrumb += "<li class=\"breadcrumb-item\"><a href='/" + entity.ShortUrl + "'>" + entity.Name + "</a></li>";
                        i++;
                        if (string.IsNullOrEmpty(BreadCrumbItem))
                        {
                            BreadCrumbItem = "{\"@type\":\"ListItem\",\"position\":" + i + ",\"item\":{\"@id\":\"" +
                                              currentDomain + "/danh-muc/" + entity.ShortUrl +
                                              "\",\"name\":\"" + entity.Name + "\"}}";
                        }
                        else
                        {
                            BreadCrumbItem += ",{\"@type\":\"ListItem\",\"position\":" + i + ",\"item\":{\"@id\":\"" +
                                               currentDomain + "/danh-muc/" + entity.ShortUrl +
                                              "\",\"name\":\"" + entity.Name + "\"}}";
                        }
                    }
                }

            }
            else
            {
                string newsType = Page.RouteData.Values["newsType"] != null ? Page.RouteData.Values["newsType"].ToString() : "";
                string newTypeName = "";
                switch (newsType)
                {
                    case "danh-muc":
                        newTypeName = "Danh mục";
                        break;
                    case "giai-phap":
                        newTypeName = "Giải pháp";
                        break;
                    case "cau-hoi":
                        newTypeName = "Câu hỏi";
                        break;
                    case "su-co-thuong-gap":
                        newTypeName = "Sự cố thường gặp";
                        break;
                    case "quan-tam-nhieu":
                        newTypeName = "Quan tâm nhiều";
                        break;
                    default:
                        break;

                }
                BreadCrumb += "<i class=\"fa fa-angle-right\"></i><a href=\"/" + newsType + "\">" + newTypeName + "</a>";
                BreadCrumbItem = "{\"@type\":\"ListItem\",\"position\":1,\"item\":{\"@id\":\"" +
                                             currentDomain + "/danh-muc/" + newsType +
                                             "\",\"name\":\"" + newTypeName + "\"}}";
            }
        }
    }
}
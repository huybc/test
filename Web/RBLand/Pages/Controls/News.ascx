﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="RBLand.Pages.Controls.News" %>
<%@ Register TagPrefix="uc1" TagName="UIPager" Src="~/Pages/Controls/Sub/UIPager.ascx" %>
<%@ Register Src="~/Pages/Controls/Sub/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>

<%@ Import Namespace="Mi.Common" %>

<%--<section class="bg-project bg-new">
        <div class="">
            <h2>Tin tức</h2>
        </div>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/default.aspx">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="/tin-tuc.htm">Tin tức</a></li>
            </div>
        </ol>
    </section>--%>

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                <ul class="list-news">
                    <asp:Repeater runat="server" ID="Rpt">
                        <ItemTemplate>

                            <li class="item py-4 row">
                                <div class="image align-self-center col-lg-4 col-md-5 col-sm-12">
                                    <a title="<%# Eval("Title")%>" href="<%#Eval("Url") %>-<%#Eval("Id")%>.htm">
                                        <img width="256" alt="<%# Eval("Title")%>" src="/uploads/thumb/<%# Eval("Avatar") %>" class="img-fluid" />
                                    </a>
                                </div>
                                <div class="content col-lg-8 col-md-7 col-sm-12">
                                    <h3 class="title"><a title="<%# Eval("Title")%>" href="<%#Eval("Url") %>-<%#Eval("Id")%>.htm"><%# Eval("Title")%></a></h3>
                                    <div class="time"><i class="far fa-calendar-alt mr-2"></i><%# Eval("DistributionDate") %></div>
                                    <div>
                                        <%# Utility.SubWordInString(Eval("Sapo").ToString(),40) %>
                                    </div>
                                </div>
                            </li>

                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="col-lg-12 col-12">
                    <div class="row float-lg-right">
                        <uc1:UIPager runat="server" ID="UIPager" PageSize="20" PageShow="5" />
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <uc1:SideBarRight runat="server" id="SideBarRight" />
            </div>

        </div>
    </div>

</section>

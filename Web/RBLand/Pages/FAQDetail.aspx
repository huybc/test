﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQDetail.aspx.cs" Inherits="RBLand.Pages.FAQDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%
    var newsId = GetQueryString.GetPost("id", 0L);
    var obj = NewsBo.GetNewsDetailById(newsId);
%>
<%=obj.Body %>
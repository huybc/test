﻿using Mi.BO.Base.News;
using Mi.Common;
using RBLand.Pages.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.Zone;
using RBLand.Core.Helper;

namespace RBLand.Pages
{
    public partial class Recruitment : System.Web.UI.Page
    {

        public int totalRow = 0;
        public ZoneEntity Zone;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string alias = "tuyen-dung";
                 Zone = ZoneBo.GetZoneByAlias(alias);
                if (Zone != null)
                {
                    Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
                    Page.MetaDescription = Zone.MetaDescription;
                    Page.MetaKeywords = Zone.MetaKeyword;
                }

                var page = Utils.ToInt32Return0(Request.QueryString["page"]);
                Rpt.DataSource = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPage.PageSize, ref totalRow);
                Rpt.DataBind();
                UIPage.TotalItems = totalRow;
            }


        }
    }
}
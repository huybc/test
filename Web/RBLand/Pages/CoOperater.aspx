﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="CoOperater.aspx.cs" Inherits="RBLand.Pages.CoOperater" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <link href="/Themes/css/hop-tac.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
       <section class="coop">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-10 offset-md-1 offset-lg-1 col-sm-12 col-12">
                    <div class="content my-5 ">
                        <div class="w-100 mr-auto">
                            <h3 class="wow fadeIn slow">Đối tác chiến lược</h3>
                            <p class="w-75 mb-4">
                                <%=UIHelper.GetConfigByName("Strategic") %>
                            </p>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-12 col-12">
                                    <div class="item wow bounceInUp fast ">
                                        <h4>Đối tác</h4> 
                                        <h4 class="mb-3">Chủ đầu tư</h4>
                                        <%=UIHelper.GetConfigByName("InvestorPartner") %> 
                                       <%-- <p><%=UIHelper.GetConfigByName("HotLine1") %> </p>--%>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-12">
                                    <div class="item wow bounceInUp">
                                        <h4>Đối tác</h4>
                                        <h4 class="mb-3">F2</h4>
                                        <%=UIHelper.GetConfigByName("F2") %>
                                        <%--<p><%=UIHelper.GetConfigByName("HotLine2") %></p>--%>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-12">
                                    <div class="item wow bounceInUp slow">
                                        <h4>Đối tác</h4>
                                        <h4 class="mb-3">Cá nhân</h4>
                                        <%=UIHelper.GetConfigByName("personal") %>
                                       <%-- <p><%=UIHelper.GetConfigByName("HotLine3") %></p>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12 col-12">
                       
                    </div>
                </div>

                <div class="col-md-10 col-lg-10 offset-md-1 offset-lg-1 col-sm-12 col-12">
                        <form class="row" id="myForm">
                                <div class="form-group col-md-6 col-lg-6 col-sm-12 col-12">
                                    <input type="text" class="form-control" placeholder="Họ tên" id="txtFullName">
                                    <span id="fullname_err" class="error">Họ và tên không được để trống</span>
                                    <input type="text" class="form-control" placeholder="Chức vụ" id="txtChucVu">
                                    <span id="chucvu_err" class="error">Chức vụ không đươc để trống</span>
                                    <input type="text" class="form-control" placeholder=" Số điện thoại" id="txtPhone">
                                    <span id="phone_err" class="error">Số điện thoại không được để trống</span>
                                    
                                </div>
    
                                <div class="form-group col-md-6 col-lg-6 col-sm-12 col-12">
                                    <textarea class="form-control" placeholder="Nội dung tin nhắn" id="exampleTextarea"
                                        rows="8"></textarea>
                                </div>
                                <div class="form-group col-md-6 col-lg-6 col-sm-12 col-12">
                                        <a class="btn  btn-send btn-lg wow pulse infinite animated" href="#" role="button"
                                    >Xác nhận</a>
                                     <span id="area_err" class="error"></span>
                                    </div>
                            </form>
                </div>
            </div>
            
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
     <script src="/Themes/js/1.js"></script>
   
    <script>
        $(function () {
            $('.error').hide();

            $('#myForm').off('click').on('click',
                function () {

                    var name = $('#txtFullName').val();
                    var chucvu = $('#txtChucVu').val();
                    var phone = $('#txtPhone').val();
                    // var address = $('#addresstxt').val();
                    // var company = $('#companytxt').val();
                    // var type = $('#typetxt').val();
                    //  var service = $('#servicedll').val();
                    // var status = $('#statusddl').val();
                    var note = $('#exampleTextarea').val();

                    $('.error').hide();
                    var name = $("#txtFullName").val();
                    if (name == "") {
                        $("#fullname_err").show();
                        $("#txtFullName").focus();
                        return false;
                    }
                    var chucvu = $("#txtChucVu").val();
                    if (chucvu == "") {
                        $("#chucvu_err").show();
                        $("#txtChucVu").focus();
                        return false;
                    }
                    var phone = $("#txtPhone").val();
                    if (phone == "") {
                        $("#phone_err").show();
                        $("#txtPhone").focus();
                        return false;
                    }
                        var note = $("#exampleTextarea").val();
                    if (note == "") {
                        $("#area_err").show();
                        $("#exampleTextarea").focus();
                        return false;
                    }

                    var data = {
                        id: 0,
                        name: name,
                        //email: mail,
                        phone: phone,
                        //  address: address,
                        // company: company,
                         type: 'cooperater',
                        //status:status,
                        // service: service,
                        //  status: status,
                        contact: chucvu,
                        note: note
                    }
                    $('body').RLoadingModule();
                    R.Post({
                        params: data,
                        module: "ui-action",
                        ashx: 'modulerequest.api',
                        action: "save",
                        success: function (res) {
                            if (res.Success) {
                                alert("Gửi thành công");
                                location.href = '/doi-tac.htm';
                            } else {
                                alert("Gửi thất bại");
                            }
                            $('body').RLoadingModuleComplete();

                        }
                    });
                });

        });
    </script>
</asp:Content>

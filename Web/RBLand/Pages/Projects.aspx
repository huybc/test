﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="Projects.aspx.cs" Inherits="RBLand.Pages.Projects" %>

<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Register Src="~/Pages/Controls/Partner.ascx" TagPrefix="uc1" TagName="Partner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        var content = ProjectDetailBo.GetByArticleId(obj != null ? obj.Id : 0).ToArray();
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                    "headline":"<%=!string.IsNullOrEmpty(obj.TitleSeo)?obj.TitleSeo:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.DistributionDate %>",
                                    "dateModified":"<%=obj.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"repair"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"Repair.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=obj.Sapo %>"
                            }
    </script>
    <section class="bg-project">
        <div class="">
            <h2>DỰ ÁN</h2>
        </div>
        <ol class="breadcrumb ">
            <div class="container p-0">
                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li class="breadcrumb-item "><a href="#">Dự án</a></li>
            </div>
        </ol>
    </section>
    
    <section class="list-project py-5">
        <div class="container">
            <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project1.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project2.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project3.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project4.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project1.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project2.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project3.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project4.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="item">
                                <div class="image" >
                                        <a href=""> <img src="imgs/project4.jpg" class="img-fluid " /></a>
                                </div>
                                <div class="text ml-5 pr-4 d-flex">
                                    <img src="imgs/list-project-icon.png" class="img-fluid mr-4" />
                                    <div class="d-flex">
                                        <a href="" class="align-self-center"> Scenia Bay Nha Trang</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            
                    </div>
            </div>
        </div>
    </section>

    <uc1:Partner runat="server" ID="Partner" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

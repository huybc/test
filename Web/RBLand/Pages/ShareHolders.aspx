﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="ShareHolders.aspx.cs" Inherits="RBLand.Pages.ShareHolders" %>

<%@ Register Src="~/Pages/Controls/Sub/UIPager.ascx" TagPrefix="uc1" TagName="UIPage" %>
<%@ Register Src="~/Pages/Controls/Sub/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>


<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="RBLand.Core.Helper" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%    
        var page = Utils.ToInt32Return0(Request.QueryString["page"]);
        var objs = ZoneBo.SelectHeader("co-dong", page == 0 ? 1 : page, UIPage.PageSize, ref totalRow);
    %>
    <section class="fix-bg-header bg-project">
        <h1 class="page-header align-self-center m-auto">Cổ đông</h1>
        <style>
            .fix-bg-header { background: url(/Uploads/<%=!string.IsNullOrEmpty(Zone.Avatar)?Zone.Avatar:"hoidap.jpg" %>) no-repeat center center; background-size: cover; }
        </style>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="co-dong.htm">Cổ đông</a></li>
            </div>
        </ol>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">

                    <uc1:SideBarRight runat="server" ID="SideBarRight" />
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="cd-r-item mb-5">
                        <%                        
                            foreach (var zone in objs)
                            {
                        %>
                        <div class="d-flex">
                            <h3><%=zone.Name %></h3>
                            <a href="" class="align-self-center ml-auto more">Xem thêm >></a>
                        </div>
                        <div class="bd-1 mb-4">
                            <div class="bd-2">
                            </div>
                        </div>

                        <table class="table table-bordered table-striped table-responsive table-cd tableFix">
                            <tbody>
                                <%       
                                    foreach (var news in NewsBo.SearchByShortUrl(zone.ShortUrl, page == 0 ? 1 : page, UIPage.PageSize, ref totalRow))
                                    {
                                %>
                                <tr>
                                    <td><%=news.Title %></td>
                                    <td><%=news.DistributionDate %></td>
                                    <td><a href="<%=news.Url %>" class="down"><i class="fas fa-download mr-2"></i>Tải về</a></td>
                                </tr>

                            </tbody>
                            <% }%>
                        </table>
                        <%} %>
                    </div>

                    <div class="row">
                        <uc1:UIPage runat="server" ID="UIPage" PageShow="6" PageSize="5" />
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

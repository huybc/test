﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="NewsByTag.aspx.cs" Inherits="RBLand.Pages.NewsByTag" %>

<%@ Register TagPrefix="uc1" TagName="UIPager" Src="~/Pages/Controls/Sub/UIPager.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SideBarRight" Src="~/Pages/Controls/Sub/SideBarRight.ascx" %>
<%@ Import Namespace="Mi.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="bg-project">
        <style>
            .bg-project { background: url("/Uploads/bg-list-project.jpg" ) no-repeat center center; background-size: cover; }
        </style>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">

                    <ul class="list-news">
                        <asp:Repeater runat="server" ID="Rpt">
                            <ItemTemplate>

                                <li class="item py-4">
                                    <div class="image align-self-center">
                                        <a href="/<%# Eval("ShortURL") %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm">
                                            <img alt="<%# Eval("Title")%>" width="256" src="/uploads/thumb/<%# Eval("Avatar") %>" class="img-fluid" />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h3><a title="<%# Eval("Title")%>" href="/<%# Eval("ShortURL") %>/<%#Eval("Url") %>.<%#Eval("Id")%>.htm"><%# Eval("Title")%></a></h3>
                                        <div class="time"><i class="far fa-calendar-alt mr-2"></i><%# Eval("DistributionDate") %></div>
                                        <div>
                                            <%# Utility.SubWordInString(Eval("Sapo").ToString(),40) %>
                                        </div>
                                    </div>
                                </li>

                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="col-lg-12 col-12">
                        <div class="row float-lg-right">
                            <uc1:UIPager runat="server" ID="UIPager" PageSize="20" PageShow="5" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <uc1:SideBarRight runat="server" ID="SideBarRight" />
                </div>

            </div>
        </div>

    </section>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="Recruitment.aspx.cs" Inherits="RBLand.Pages.Recruitment" %>

<%@ Register TagPrefix="uc1" TagName="UIPage" Src="~/Pages/Controls/Sub/UIPager.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <style>
        form span { color: red; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="fix-bg-header bg-project">
        <h1 class="page-header align-self-center m-auto">Tuyển dụng</h1>
        <style>
            .fix-bg-header { background: url(/Uploads/<%=!string.IsNullOrEmpty(Zone.Avatar)?Zone.Avatar:"hoidap.jpg" %>) no-repeat center center; background-size: cover; }
        </style>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="tuyen-dung.htm">Tuyển dụng</a></li>
            </div>
        </ol>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-7 col-12">
                    <div class="list-job mb-4">
                        <asp:Repeater runat="server" ID="Rpt">
                            <ItemTemplate>
                                <div class="card mb-2">
                                    <div class="card-header d-flex" id="headingOne-<%# Eval("Id") %>" data="<%# Eval("Id") %>">
                                        <span class="stt mr-3">1</span>
                                        <p class="title mb-0 align-self-center">
                                            <span><%# Eval("Title") %></span>
                                        </p>
                                        <span class="align-self-center ml-auto mr-4"><i class="fas fa-plus fa-minus"></i></span>
                                    </div>
                                    <div id="collapseOne-<%# Eval("Id") %>" class="collapse">
                                        <div class="card-block"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row">
                        <uc1:UIPage runat="server" ID="UIPage" PageShow="6" PageSize="10" />

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-12">
                    <form class="form-re row myForm">
                        <div class="form-group mb-0 col-md-12 col-lg-12 col-sm-12 col-12">
                            <h3 class="text-center text-uppercase mb-3">Form Tuyển dụng HLP</h3>
                            <p class="text-center mb-0">Các trường đánh dấu <span class="red">*</span> là bắt buộc</p>

                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">

                            <label>Họ và tên <span class="red">*</span></label>
                            <input type="text" class="form-control" id="txtFullName">
                            <span id="fullname_err" class="error">Họ và tên không được để trống</span>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">
                            <label>Email</label>
                            <input type="text" class="form-control" id="txtEmail">
                            <span id="email_err" class="error">Email không được để trống</span>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">
                            <label>Số điện thoại <span class="red">*</span></label>
                            <input type="text" class="form-control" id="txtPhone">
                            <span id="phone_err" class="error">Số điện thoại không được để trống</span>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">
                            <label>Vị trí ứng tuyển <span class="red">*</span></label>
                            <input type="text" class="form-control" id="txtViTri">
                            <span id="vitri_err" class="error">Vị trí ứng tuyển không được để trống</span>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">
                            <label>Trình độ chuyên môn</label>
                            <textarea class="form-control" rows="8" id="txtExp"></textarea>
                            <span id="exp_err" class="error">Trình độ chuyên môn không được để trống</span>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12">
                            <div class="g-recaptcha" data-sitekey="6LfGwKoUAAAAACVZvLvIPWVAluYdqreJgJWmgbl7" id="recaptcha"></div>
                            <span class="msg-error error"></span>

                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12 col-12 text-center">
                            <input type="submit" class="btn  btn-primary " id="btnSubmit" value="Gửi ngay nào!">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        scrollBlock();
        $(function () {
            $('.card-header').click(function () {
                var $this = $(this);
                $this.find('i').toggleClass('fa-plus');
                $this.next('.collapse').toggleClass('show');
                //return false;

                var contents = $this.closest('.card').find('.card-block');
                if (contents.html().length > 0) {
                    return false;
                }
                var id = $this.attr('data');
                //  $this.RLoadingModule();
                R.Post({
                    params: { id: id },
                    module: "ui-action",
                    ashx: 'modulerequest.api',
                    action: "faq-detail",
                    success: function (res) {
                        if (res.Success) {
                            contents.html(res.Content);

                        }
                        // $('.card-header').RLoadingModuleComplete();
                    }
                });
            });
        });
    </script>
    <script>
        scrollBlock();
        $(function () {
            $('.error').hide();

            $('.myForm').off('click').on('click',
                function () {


                    $('.error').hide();
                    var name = $("#txtFullName").val();
                    if (name == "") {
                        $("#fullname_err").show();
                        $("#txtFullName").focus();
                        return false;
                    }
                    var mail = $("#txtEmail").val();
                    if (mail == "") {
                        $("#email_err").show();
                        $("#txtEmail").focus();
                        return false;
                    }
                    var phone = $("#txtPhone").val();
                    if (phone == "") {
                        $("#phone_err").show();
                        $("#txtPhone").focus();
                        return false;
                    }
                    var contact = $("#txtViTri").val();
                    if (contact == "") {
                        $("#vitri_err").show();
                        $("#txtViTri").focus();
                        return false;
                    }
                    var note = $("#txtExp").val();
                    if (note == "") {
                        $("#exp_err").show();
                        $("#txtExp").focus();
                        return false;
                    }
                    var $captcha = $('#recaptcha'),
                        response = grecaptcha.getResponse();

                    if (response.length === 0) {
                        $('.msg-error').text("Bạn chưa check capcha kìa").show();
                        if (!$captcha.hasClass("error")) {
                            $captcha.addClass("error");
                        }
                    } else {
                        $('.msg-error').text('');
                        $captcha.removeClass("error");
                    }


                    var data = {
                        id: 0,
                        name: name,
                        email: mail,
                        phone: phone,
                        type: 'recruitment',
                        contact: contact,
                        note: note
                    }
                    $('body').RLoadingModule();
                    R.Post({
                        params: data,
                        module: "ui-action",
                        ashx: 'modulerequest.api',
                        action: "save",
                        success: function (res) {
                            if (res.Success) {
                                alert("Gửi thành công");
                                location.href = '/tuyen-dung.htm';
                            } else {
                                alert("Gửi thất bại");
                            }
                            $('body').RLoadingModuleComplete();

                        }
                    });
                });

        });
    </script>



</asp:Content>

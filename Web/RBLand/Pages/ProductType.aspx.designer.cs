﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace RBLand.Pages {
    
    
    public partial class ProductType {
        
        /// <summary>
        /// BreadCrumbs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::RBLand.Pages.Controls.Sub.BreadCrumbs BreadCrumbs;
        
        /// <summary>
        /// ProjectByType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::RBLand.Pages.Controls.ProjectByType ProjectByType;
    }
}

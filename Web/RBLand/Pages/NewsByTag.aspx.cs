﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using Mi.BO.Base.Tag;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Tag;
using RBLand.Core.Helper;

namespace RBLand.Pages
{
    public partial class NewsByTag : System.Web.UI.Page
    {
        public int totalRow = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TagEntity tag;
                if (Page.RouteData.Values["tagName"] != null)
                {
                    tag = TagBo.GetByAlias(Page.RouteData.Values["tagName"].ToString());
                    int totalRows = 0;
                    var objs = NewsBo.GetNewsByTagIdWithPaging(tag != null ? tag.Id : 0, DateTime.MinValue, DateTime.MinValue,
                        NewsFilterFieldForUsername.CreatedBy, NewsSortExpression.CreatedDateDesc, NewsStatus.Published, NewsType.All, EnumNewsIsAnswered.All, 1, 20, ref totalRows);
                    Rpt.DataSource = objs;
                    Rpt.DataBind();
                    UIPager.TotalItems = totalRow;
                }
                else
                {
                    tag = new TagEntity();
                }
               
              

            }
        }
    }
}
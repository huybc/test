﻿using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RBLand.Pages
{
    public partial class Projects : System.Web.UI.Page
    {
        public NewsEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailById(newsId);
                    Page.Title = !string.IsNullOrEmpty(obj.TitleSeo) ? obj.TitleSeo : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsEntity();

                }
            }
        }
        public bool isEven(int row)
        {
            var x = row % 2;
            if (x == 0)
                return true;
            return false;

        }
    }
}
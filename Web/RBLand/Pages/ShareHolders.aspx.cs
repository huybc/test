﻿using System;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using RBLand.Core.Helper;

namespace RBLand.Pages
{
    public partial class ShareHolders : System.Web.UI.Page
    {
        public int totalRow = 0;
        public ZoneEntity Zone;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string alias = "co-dong";
                 Zone = ZoneBo.GetZoneByAlias(alias);
                if (Zone != null)
                {
                    Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
                    Page.MetaDescription = Zone.MetaDescription;
                    Page.MetaKeywords = Zone.MetaKeyword;
                }
                UIPage.TotalItems = totalRow;
            }

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="ProductType.aspx.cs" Inherits="RBLand.Pages.ProductType" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Register Src="~/Pages/Controls/Projects.ascx" TagPrefix="uc1" TagName="Projects" %>
<%@ Register Src="~/Pages/Controls/Sub/BreadCrumbs.ascx" TagPrefix="uc1" TagName="BreadCrumbs" %>
<%@ Register Src="~/Pages/Controls/ProjectByType.ascx" TagPrefix="uc1" TagName="ProjectByType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        if (Zone != null)
        {%>
        <section class="bg-project bg-new">
            <style>
                .bg-project.bg-new { background: url(/Uploads/<%=!string.IsNullOrEmpty(Zone.Avatar)?Zone.Avatar:"bg-list-project.jpg" %>) no-repeat center center; background-size: cover; }
            </style>
            <div class="">
                <h2><%=Zone.Name %></h2>
            </div>
            <uc1:BreadCrumbs runat="server" ID="BreadCrumbs" />

        </section>
    <uc1:ProjectByType runat="server" ID="ProjectByType" />
    <%  }
    %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

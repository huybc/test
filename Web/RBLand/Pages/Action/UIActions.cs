﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.Customer;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using RBLand.CMS.Modules.Service.Action;
using RBLand.Core.Helper;

namespace RBLand.Pages.Action
{
    public class UIActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            switch (functionName)
            {

                case "save":
                    responseData = AddCustomer();
                    break;
                case "faq-detail":
                    responseData = FAQDetail();
                    break;

            }

            return responseData;
        }

        private ResponseData FAQDetail()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData AddCustomer()
        {
            var responseData = new ResponseData();
            var name = GetQueryString.GetPost("name", string.Empty).GetText();
            var email = GetQueryString.GetPost("email", string.Empty).GetText();
            var phoneNumber = GetQueryString.GetPost("phone", string.Empty).GetText();
            var note = GetQueryString.GetPost("note", string.Empty).GetText();
            //var id = GetQueryString.GetPost("id", 0);
            //var name = GetQueryString.GetPost("name", string.Empty).GetText();
            //var email = GetQueryString.GetPost("email", string.Empty).GetText();
            //var phone = GetQueryString.GetPost("phone", string.Empty).GetText();
            //var address = GetQueryString.GetPost("address", string.Empty).GetText();
            ////  var sortOrder = GetQueryString.GetPost("sort", 0);
            //var company = GetQueryString.GetPost("company", string.Empty).GetText();
            //var type = GetQueryString.GetPost("type", string.Empty).GetText();
            //var service = GetQueryString.GetPost("service", string.Empty).GetText();
            //var status = GetQueryString.GetPost("status", "Potential").GetText();
            //var contact = GetQueryString.GetPost("contact", string.Empty).GetText();
            //var note = GetQueryString.GetPost("note", string.Empty).GetText();

            if (!string.IsNullOrEmpty(name))
            {

                var obj = new CustomerEntity
                {
                    //Id = id,
                    FullName = name,
                    Email = email,
                    Mobile = phoneNumber,
                    //Address = address,
                    //Firm = company,
                    //Type = type,
                    //Service = service,
                    //Status = status,
                    //Contactperson = contact,
                    Note = note
                };
                int outId = 0;

                responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                if (outId > 0)
                {
                    responseData.Success = true;
                }
                responseData.Message = "Thêm mới thành công !";

            }
            else
            {
                responseData.Success = false;
                responseData.Message = "Error";
            }
            return responseData;
        }


    }
}
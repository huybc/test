﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="RBLand.Pages.Terms" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="container" style="padding:60px 0">
        <%=UIHelper.GetConfigByName("Terms") %>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

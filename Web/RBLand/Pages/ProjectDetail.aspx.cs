﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using RBLand.Pages.Controls.Sub;

namespace RBLand.Pages
{
    public partial class ProjectDetail : System.Web.UI.Page
    {
        public NewsEntity obj;
        public int totalRow = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var newsId = Utility.ConvertToLong(Page.RouteData.Values["id"]);
                if (newsId > 0)
                {
                    obj = NewsBo.GetNewsDetailById(newsId);
                    Page.Title = !string.IsNullOrEmpty(obj.TitleSeo) ? obj.TitleSeo : obj.Title;
                    Page.MetaKeywords = obj.MetaKeyword;
                    Page.MetaDescription = obj.MetaDescription;
                }
                else
                {
                    obj = new NewsEntity();

                }
           
                var alias = Page.RouteData.Values["zoneName"];
                if (alias != null)
                {
                    var objs = NewsBo.SearchByShortUrl(alias.ToString(), 1, 6, ref totalRow);
                    if (newsId >0)
                    {
                        objs = objs.Where(it => it.Id != newsId.ToInt32Return0());
                    }
                    Rpt.DataSource = objs;
                    Rpt.DataBind();
                }
                



            }
        }
        public bool isEven(int row)
        {
            var x = row % 2;
            if (x == 0)
                return true;
            return false;

        }
    }
}
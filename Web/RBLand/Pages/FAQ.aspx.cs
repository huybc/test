﻿using Mi.BO.Base.News;
using Mi.Common;
using System;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.Zone;
using RBLand.Core.Helper;

namespace RBLand.Pages
{
    public partial class FAQ : System.Web.UI.Page
    {
        public int totalRow = 0;
        public ZoneEntity Zone;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string alias = "hoi-dap";
                 Zone = ZoneBo.GetZoneByAlias(alias);
                if (Zone != null)
                {
                    Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
                    Page.MetaDescription = Zone.MetaDescription;
                    Page.MetaKeywords = Zone.MetaKeyword;
                }

                var page = Utils.ToInt32Return0(Request.QueryString["page"]);
                Rpt.DataSource = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
                Rpt.DataBind();
                UIPager.TotalItems = totalRow;
            }


        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.Zone;

namespace RBLand.Pages
{
    public partial class Articles : System.Web.UI.Page
    {
        public ZoneEntity Zone;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var alias = Page.RouteData.Values["zoneName"];
                if (alias!=null)
                {
                    Zone = ZoneBo.GetZoneByAlias(alias.ToString());
                    Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
                    Page.MetaDescription = Zone.MetaDescription;
                    Page.MetaKeywords = Zone.MetaKeyword;
                }
                else
                {
                    Zone= new ZoneEntity();
                }

            }

        }
    }
}
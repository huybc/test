﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="NewsDetail.aspx.cs" Inherits="RBLand.Pages.NewsDetail" %>

<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Register Src="~/Pages/Controls/Sub/SideBarRight.ascx" TagPrefix="uc1" TagName="SideBarRight" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <%if (obj != null)
      { %>
    <meta property="og:title" content="<%= obj.Title %>" />
    <meta property="og:description" content="<%= obj.Sapo%>" />
    <meta property="og:image" content="https://rbland.vn/Uploads/<%= obj.Avatar %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
    %>
    <script type="application/ld+json">
				            {  
                                    "@context":"http://schema.org",
                                    "@type":"Article",
                                    "mainEntityOfPage":{  
                                        "@type":"WebPage",
                                        "@id":"<%=HttpContext.Current.Request.Url.AbsoluteUri %>"
                                    },
                                   "headline":"<%=!string.IsNullOrEmpty(obj.TitleSeo)?obj.TitleSeo:obj.Title%>",
                                    "image":{  
                                        "@type":"ImageObject",
                                        "url":"<%=domainName+"/uploads/"+obj.Avatar %>",
                                        "height":600,
                                        "width":800
                                    },
                                    "datePublished":"<%=obj.DistributionDate %>",
                                    "dateModified":"<%=obj.LastModifiedDate %>",
                                    "author":{  
                                        "@type":"Person",
                                        "name":"rbland"
                                    },
                                    "publisher":{  
                                        "@type":"Organization",
                                        "name":"rbland.vn",
                                        "logo":{  
                                            "@type":"ImageObject",
                                            "url":"<%=domainName+"/uploads/"+UIHelper.GetConfigByName("LogoTop")%>",
                                            "width":35,
                                            "height":34
                                        }
                                    },
                                    "description":"<%=obj.Sapo %>"
                            }
    </script>
    <style>
        .bg-project.bg-new { background: url(/Uploads/<%=!string.IsNullOrEmpty(obj.Avatar)?obj.Avatar:"bg-news.jpg" %>) no-repeat center center; background-size: cover; }
    </style>
    <section class="bg-project bg-new">
        <div class="">
            <%--<h2>Tin tức</h2>--%>
        </div>
        <ol class="breadcrumb ">
            <div class="container p-0">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item "><a href="/tin-tuc.htm">Tin tức</a></li>
            </div>
        </ol>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="detail-new">
                        <div class="heading">
                            <h1><%=obj.Title%></h1>
                            <div class="time"><i class="far fa-calendar-alt mr-2"></i><time><%=UIHelper.GetLongDate(obj.DistributionDate)%></time></div>

                        </div>
                        <div class="description">
                            <%=obj.Sapo%>
                        </div>
                        <div class="content py-4">
                            <%=obj.Body%>
                        </div>
                        <div class="tags">Tags:<%= GetTags(obj.Id) %> </div>
                    </div>
                    <div class="text-right">
                        <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                    </div>
                    <div class="comment-facebook">
                        <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="auto" data-numposts="5"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <uc1:sidebarright runat="server" id="SideBarRight" />
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 py-5">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="text-uppercase color-144a91 mb-5">
                                <h4>Bài viết liên quan</h4>
                                <div class="bd-1">
                                    <div class="bd-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <%
                            var alias = Page.RouteData.Values["zoneName"];
                            int totalRow = 0;

                            var articles = NewsBo.SearchByShortUrl(alias != null ? alias.ToString() : "tin-tuc", 1, 12, ref totalRow);
                            articles = articles.Where(it => it.Id != obj.Id);
                            if (alias != null)
                            {
                                foreach (var article in articles)
                                {
                        %>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                            <div class="new-lq-item w-100">
                                <div class="image">
                                    <img alt="<%=article.Title %>" src="/Uploads/thumb/<%=article.Avatar %>" class="img-fluid" />
                                </div>
                                <div class="text">
                                    <a title="<%=article.Title %>" href="/<%=article.Url %>"><%=article.Title %></a>
                                </div>
                            </div>
                        </div>
                        <% } %>

                        <%
                            }
                            else
                            {
                                foreach (var article in articles)
                                {
                        %>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-12">
                            <div class="new-lq-item w-100">
                                <div class="image">
                                    <img alt="<%=article.Title %>" src="/Uploads/thumb/<%=article.Avatar %>" class="img-fluid" />
                                </div>
                                <div class="text">
                                    <a title="<%=article.Title %>" href="/<%=article.Url %>-<%=article.Id%>.htm"><%=article.Title %></a>
                                </div>
                            </div>
                        </div>

                        <% }
                            } %>
                        <% if (!articles.ToArray().Any())
                            { %>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <p class="text-center">Đang cập nhật</p>
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <style>
    </style>
</asp:Content>

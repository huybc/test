﻿using System;
using System.Collections.Generic;

namespace RBLand.CMS
{
    public partial class Login : System.Web.UI.Page
    {
        public string CurrentVersion = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string IncludeResources(List<string> urls,ResourceType resourceType)
        {
            string html = "\n<!-- IMS Resources - Version: " + CurrentVersion + "-->";
            foreach (var url in urls)
            {
                html += IncludeResource(url, resourceType);
            }
            return html;
        }
        public string IncludeResource(string url, ResourceType resourceType)
        {
            switch (resourceType)
            {
                case ResourceType.Css:
                    return string.Format("\n<link href=\"{0}?t={1}\" rel=\"stylesheet\" type=\"text/css\" />", url, CurrentVersion);
                    break;
                case ResourceType.Javascript:
                    return string.Format("\n<script src=\"{0}?t={1}\" type=\"text/javascript\"></script>", url,
                                         CurrentVersion);
                    break;
                default:
                    return "";
                    break;
            }
        }
        public enum ResourceType : int
        {
            Css = 1,
            Javascript = 2
        }
    }

}
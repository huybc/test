﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="RBLand.CMS.Modules.Zone.Main" %>

<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>

<link href="/CMS/Modules/Zone/Styles/style.css" rel="stylesheet" />
<div class="panel-heading" id="header-page">
    <h6 class="panel-title">

        <button type="button" data-toggle="modal" id="zoneAddBtn" class="btn btn-default ibtn-xs btn-primary heading-btn _newspost"><i class="icon-plus22 position-left"></i>Thêm mới</button>
        <button type="button" data-toggle="modal" id="btn-new-reload" class="btn btn-default ibtn-xs heading-btn _newsreload" title="Tải lại"><i class="icon-reload-alt position-left"></i></button>
    </h6>
    <div class="heading-elements">
        <div class="input-group">
            <input type="text" class="form-control _search-data" id="_search-data" placeholder="Tìm kiếm bài viết">
            <span class="input-group-btn">
                <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
            </span>
        </div>
    </div>
    <a class="heading-elements-toggle"><i class="icon-menu"></i></a>

</div>
<div class="navbar navbar-default navbar-xs navbar-component" style="margin-right: 0; margin-left: 0">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-filter" style="padding-top: 1px">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <div style="margin: 3px 3px 0 3px">
                    <select data-placeholder="Select your state" class="select" id="zoneTypedll" style="width: 200px" tabindex="4">
                        <option value="-1">Danh mục</option>
                        <% var zones = ZoneBo.GetAllZone((int) ZoneType.News).Where(it=>it.ParentId==0);
                           foreach (var zone in zones)
                           { %>
                            
                            <option value="<%=zone.Id %>"><%=zone.Name %></option>
                       <% } %>
                        <%--<option value="<%=(int)ZoneType.News %>">Tin tức</option>
                        <option value="<%=(int)ZoneType.Recruitment %>">Tuyển dụng</option>
                        <option value="<%=(int)ZoneType.RBLand %>">RBLand</option>
                        <option value="<%=(int)ZoneType.Project %>">Dự án</option>
                        <option value="<%=(int)ZoneType.Shareholders %>">Cổ đông</option>
                        <option value="<%=(int)ZoneType.Advisory %>">Tư vấn</option>
                        <option value="<%=(int)ZoneType.Cooperative %>">Hợp tác</option>
                        <option value="<%=(int)ZoneType.Invest %>">Đầu tư</option>--%>
                    </select>
                </div>
            </li>
            <li class="dropdown">
                <div style="margin: 3px 3px 0 3px">
                    <select data-placeholder="Select your state" class="select" id="zoneStatusdll" style="width: 200px" tabindex="4">
                        <option value="-1">Trạng thái</option>
                        <option value="1">Hiển thị</option>
                        <option value="2">Ẩn</option>
                        <option value="3">Xóa</option>
                    </select>
                </div>
            </li>

            <li class="dropdown">
                <div id="date-filter" class="pull-right date-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%; height: 30px; border: 1px solid #ccc;">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;<span></span><b class="caret"></b>
                </div>
            </li>

        </ul>
        <div class="navbar-right">
            <div id="zone-data-pager">
                <span id="rowInTotals"></span>
                <div class="ipagination-zone iweb">
                    <a href="#" class="first" data-action="first">&laquo;</a>
                    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                    <input type="text" readonly="readonly" />
                    <a href="#" class="next" data-action="next">&rsaquo;</a>
                    <a href="#" class="last" data-action="last">&raquo;</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="module-content">
    <div class="btn-group btn-action" style="display: none">
        <button type="button" style="display: none" class="btn btn-default btn-sm _view" data-popup="tooltip" data-placement="top" data-original-title="Xem"><i class="icon-eye"></i></button>
        <button type="button" class="btn btn-default btn-sm _edit" data-popup="tooltip" data-original-title="Sửa bài"><i class="icon-pencil7"></i></button>
        <button type="button" data-status="<%=(int)ZoneStatus.Delete %>" class="btn btn-default btn-sm _delete status" data-popup="tooltip" data-original-title="Xóa bài"><i class="icon-bin"></i></button>
        <button type="button" data-status="<%=(int)ZoneStatus.Publish %>" class="btn btn-default btn-sm _publish status" style="display: none" data-popup="tooltip" data-original-title="Xuất bản"><i class="icon-file-upload"></i></button>
        <button type="button" data-status="<%=(int)ZoneStatus.UnPublish %>" class="btn btn-default btn-sm _unpublish status" style="display: none" data-popup="tooltip" data-original-title="Hạ bài"><i class="icon-file-download"></i></button>
        <label data-popup="tooltip" data-original-title="Đã chọn">[<label id="rows"></label>]</label>
    </div>
    <div id="zone-wrapper">
        <div class="datatable-scroll _list">
        </div>

    </div>


</div>
<div id="IMSOverlayWrapper">
    <div id="IMSOverlayWrapperIn">
        <div id="IMSOverlayContent">
        </div>
        <span id="IMSOverlayClose"></span>
    </div>
</div>

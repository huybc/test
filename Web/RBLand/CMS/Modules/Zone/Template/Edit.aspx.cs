﻿using System;
using System.Linq;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Action.Core;

namespace RBLand.CMS.Modules.Zone.Template
{
    public partial class Edit : PageBase
    {
        public ZoneEntity obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0);
            var zoneType = GetQueryString.GetPost("zoneType",0);
            if (id > 0)
            {
                obj = ZoneBo.GetZoneById(id);

            }
            else
            {
                obj = new ZoneEntity();
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, zoneType).ToList().Where(it => it.Status == 1);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
        }
    }
}
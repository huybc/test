﻿using System;
using System.Collections.Generic;
using Mi.BO.Base.FileUpload;
using Mi.Common;
using Mi.Entity.Base.FileUpload;
using Mi.Action.Core;

namespace RBLand.CMS.Modules.FileManager
{
    public partial class Main : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var parentid = GetQueryString.GetPost("parentid", 0);
            var keyword = GetQueryString.GetPost("keyword", "");
          //  int totalRows = 0;
            //var objs = FileUploadBo.SearchFileUpload(keyword, parentid, UIHelper.GetCurrentUserName(),
            //    EnumFileUploadType.All, EnumFileUploadStatus.AllStatus, string.Empty, EnumFileSortOrder.UploadedDateAsc,
            //    1, 30, ref totalRows);

        }

        public List<FolderResult> FolderResult()
        {
            var rs = new List<FolderResult>();
            var objs = FileUploadBo.FoldersSearch();
            foreach (var obj in objs)
            {
                rs.Add(new FolderResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name,
                    path = obj.path
                });
            }
            return rs;
        }
    }

   
}
﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web;
using IWeb.UploadHandler;
using Mi.Action.Core;
using Mi.BO.Base.FileUpload;
using Mi.Common;
using Mi.Common.ChannelConfig;
using Mi.Entity.Base.FileUpload;
using Newtonsoft.Json;
using RBLand.Core.Helper;

namespace RBLand.CMS.Modules.FileManager.Action
{

    /// <summary>
    /// This class is an application level implementation of an uploader
    /// 
    /// This uploader subclasses plUploadFileHandler which downloads files
    /// into a temporary folder (~/tempuploads) and then resizes the image,
    /// renames it and copies it a final destination (~/UploadedImages)
    /// 
    /// This handler also deletes old files in both of those folders
    /// just to keep the size of this demo reasonable.
    /// </summary>
    public class ImageUploadHandler : plUploadFileHandler
    {

    string fileStoragePath = CmsChannelConfiguration.GetAppSetting("StorageUrl");

        public static int ImageHeight = 480;

        public ImageUploadHandler()
        {

            if (!PolicyProviderManager.Provider.IsLogin())
            {
                return;
            }

            // Normally you'd set these values from config values
            MaxUploadSize = 10000000;
            AllowedExtensions = ".jpg,.jpeg,.png,.gif,.bmp,.doc,.docx,.xlsx,.xls,.zip,.rar,.txt,.pdf";
        }

        protected override void OnUploadCompleted(string fileName, int folder_id, string name, string ext, long size, HttpPostedFile fileUpload)
        {
            var Server = Context.Server;
            // Physical Path is auto-transformed
            var path = FileUploadPhysicalPath;
            var fullUploadedFileName = Path.Combine(path, fileName);

            // Typically you'd want to ensure that the filename is unique
            // Some ID from the database to correlate - here I use a static img_ prefix
           // string generatedFilename = name + "_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ext;
            string generatedFilename = Utility.UnicodeToKoDauAndGach(name)  + ext;

            string imagePath = Server.MapPath("~/" + fileStoragePath);
            // check duplicate
            if (FileUploadBo.FolderCheckIsExist(0, fileName, folder_id) > 0)
            {
                generatedFilename = name + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ext;
            }
            

            try
            {
                // resize the image and write out in final image folder

                if (SaveFile(fullUploadedFileName, Path.Combine(imagePath, generatedFilename), ext, fileUpload))
                {
                    var objFileUpload = new FileUploadEntity();
                    objFileUpload.Name = generatedFilename;
                    objFileUpload.ParentId = folder_id;
                    objFileUpload.FilePath = fileStoragePath + "/" + generatedFilename;
                    objFileUpload.UploadedDate = DateTime.Now;
                    objFileUpload.UploadedBy = UIHelper.GetCurrentUserName();
                    objFileUpload.Type = 1;
                    objFileUpload.Status = 1;
                    objFileUpload.FileExt = ext;
                    if (size > 0)
                    {
                        objFileUpload.FileSize = (size / 1024f);
                    }

                    ext = ext.ToLower();
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".png" || ext == ".gif" || ext == ".bmp")
                    {
                        var image = Image.FromFile(Server.MapPath("~/" + fileStoragePath + "/" + generatedFilename));
                        objFileUpload.Dimensions = image.PhysicalDimension.Width + "x" + image.PhysicalDimension.Height;
                        // objFileUpload = image.PhysicalDimension.Width + image.PhysicalDimension.Height;
                    }
                    int id = 0;
                    // CHECK DUPLICATE
                    
                    FileUploadBo.InsertFileUpload(objFileUpload, ref id);
                    if (id > 0)
                    {
                        objFileUpload.Id = id;
                     //   objFileUpload.UploadedDate = null;
                        objFileUpload.strDate = UIHelper.GetFullDate(objFileUpload.UploadedDate);
                        string json = JsonConvert.SerializeObject(objFileUpload);
                        WriteUploadCompletedMessage(json);
                    //    WriteSucessResponse(json);
                    }
                }
                // delete the temp file
                // File.Delete(fullUploadedFileName);
            }
            catch (Exception ex)
            {
                WriteErrorResponse("Unable to write out uploaded file: " + ex.Message);
                return;
            }

        }

        protected override bool OnUploadStarted(int chunk, int chunks, string name)
        {
            // time out files after 15 minutes - temporary upload files
            DeleteTimedoutFiles(Path.Combine(FileUploadPhysicalPath, "*.*"), 900);

            // clean out final image folder too
            //  DeleteTimedoutFiles(Path.Combine(Context.Server.MapPath(fileStoragePath), "*.*"), 900);

            return base.OnUploadStarted(chunk, chunks, name);
        }





        #region Sample Helpers
        /// <summary>
        /// Deletes files based on a file spec and a given timeout.
        /// This routine is useful for cleaning up temp files in 
        /// Web applications.
        /// </summary>
        /// <param name="filespec">A filespec that includes path and/or wildcards to select files</param>
        /// <param name="seconds">The timeout - if files are older than this timeout they are deleted</param>
        public static void DeleteTimedoutFiles(string filespec, int seconds)
        {
            string path = Path.GetDirectoryName(filespec);
            string spec = Path.GetFileName(filespec);
            string[] files = Directory.GetFiles(path, spec);

            foreach (string file in files)
            {
                try
                {
                    if (File.GetLastWriteTimeUtc(file) < DateTime.UtcNow.AddSeconds(seconds * -1))
                        File.Delete(file);
                }
                catch { }  // ignore locked files
            }
        }

        /// <summary>
        /// Creates a resized bitmap from an existing image on disk. Resizes the image by 
        /// creating an aspect ratio safe image. Image is sized to the larger size of width
        /// height and then smaller size is adjusted by aspect ratio.
        /// 
        /// Image is returned as Bitmap - call Dispose() on the returned Bitmap object
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns>Bitmap or null</returns>
        public static bool SaveFile(string filename, string outputFilename, string ext, HttpPostedFile fileUpload)
        {
            Bitmap bmpOut = null;

            try
            {
                if (ext == ".jpg" || ext == ".jpeg" || ext == ".png" || ext == ".gif" || ext == ".bmp")
                {
                    Bitmap bmp = new Bitmap(filename);
                    System.Drawing.Imaging.ImageFormat format = bmp.RawFormat;
                    if (outputFilename != filename)
                        bmp.Save(outputFilename);
                    bmp.Dispose();
                }
                else
                {
                    fileUpload.SaveAs(outputFilename);
                }
                return true;

                //decimal ratio;
                //int newWidth = 0;
                //int newHeight = 0;

                ////*** If the image is smaller than a thumbnail just return it
                //if (bmp.Height < height)
                //{
                //    if (outputFilename != filename)
                //        bmp.Save(outputFilename);
                //    bmp.Dispose();
                //    return true;
                //}

                //ratio = (decimal)height / bmp.Height;
                //newHeight = height;
                //newWidth = Convert.ToInt32(bmp.Width * ratio);


                //bmpOut = new Bitmap(newWidth, newHeight);
                //Graphics g = Graphics.FromImage(bmpOut);
                //g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                //g.FillRectangle(Brushes.White, 0, 0, newWidth, newHeight);
                //g.DrawImage(bmp, 0, 0, newWidth, newHeight);

                //bmp.Dispose();

                //bmpOut.Save(outputFilename, format);
                //bmpOut.Dispose();
            }
            catch (Exception ex)
            {
                var msg = ex.GetBaseException();
                return false;
            }

            return true;
        }

        #endregion

    }
}
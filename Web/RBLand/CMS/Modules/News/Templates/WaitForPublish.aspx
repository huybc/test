﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitForPublish.aspx.cs" Inherits="RBland.CMS.Modules.News.Templates.WaitForPublish" %>

<%@ Import Namespace="RBland.Core.Helper" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows %>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
            <td class="w100">
                <div>
                    <label>
                        <input type="checkbox" id="<%# Eval("Id") %>" />
                    </label>
                    <label class="_hot">
                       <i class="icon-file-presentation"></i>
                    </label>
                </div>
            </td>
            <td class="w80 ">
                <a class="avatar">
                    <img class="thumb" src="<%#  UIHelper.Thumb_W(60,Eval("Avatar").ToString()) %>" width="60" />
                    <%# (bool)Eval("IsVideo")?"<i class=\"icon-video\"></i>":"" %>
                </a>
            </td>
            <td>
                <p>
                  <%# Eval("Title") %> - <b class="wysiwyg-color-green _zone" data-id="<%# Eval("ZoneId") %>"></b>
                </p>
                <p>
                    <label>Tạo bởi:</label><span class="bold"><%# Eval("CreatedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>
                </p>
            </td>
            <%--<td><i class="_icon icon-file-minus2" data-popup="tooltip" data-placement="auto" data-original-title="Từ chối"></i></td>--%>
           <%-- <td class="text-center">
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-file-pdf"></i>Export to .pdf</a></li>
                            <li><a href="#"><i class="icon-file-excel"></i>Export to .csv</a></li>
                            <li><a href="#"><i class="icon-file-word"></i>Export to .doc</a></li>
                        </ul>
                    </li>
                </ul>
            </td>--%>
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

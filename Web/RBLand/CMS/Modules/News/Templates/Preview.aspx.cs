﻿using System;
using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;

namespace RBland.CMS.Modules.News.Templates
{
    public partial class Preview : PageBase
    {
        public NewsDetailForEditEntity obj = new NewsDetailForEditEntity();

        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0L);
            obj = NewsBo.GetDetail(id, PolicyProviderManager.Provider.GetAccountName());
            if (id <= 0)
            {
                obj = new NewsDetailForEditEntity
                {
                    NewsInfo = new NewsEntity()
                };
            }
        }

        protected string GetTags(int type)
        {
            string _strTag = "";

            foreach (var tag in obj.TagInNews)
            {

                if (tag.TagMode == type)
                {
                    if (!string.IsNullOrEmpty(_strTag))
                    {
                        _strTag += ", <a title=" + tag.Name + " href=\"/tag/" + tag.Url + "-" + tag.TagId + ".htm\">" + tag.Name + "</a>";
                    }
                    else
                    {
                        _strTag += "<a title=" + tag.Name + " href=\"/tag/" + tag.Url + "-" + tag.TagId + ".htm\">" + tag.Name + "</a>";
                    }

                }
            }

            return _strTag;

        }
    }
}
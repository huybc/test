﻿using System;
using System.Linq;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using RBLand.Core.Helper;

namespace RBLand.CMS.Modules.News.Action
{
    public class NewsActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                    case "search":
                        responseData = GetListNews();
                        break;
                    case "init_main":
                        responseData = InitMain();
                        break;
                    case "news_init_template":
                        responseData = NewsInitTemplate();
                        break;
                    case "publish":
                        responseData = Publish();
                        break;
                    case "save":
                        responseData = News_Save();
                        break;
                    case "preview":
                        responseData = Preview();
                        break;
                    case "unpublish":
                        responseData = Unpublish();
                        break;
                    case "comment":
                        responseData = ChangeStatusComment();
                        break;
                    case "rq_publish":
                        responseData = ChangeStatusToWaitPublish();
                        break;
                    case "reject":
                        responseData = Reject();
                        break;
                    case "hot_main":
                        responseData = HotMain();
                        break;
                    case "remove":
                        responseData = RemoveToTrash();
                        break;
                    case "psearch":
                        responseData = Psearch();
                        break;
                    case "rsearch":
                        responseData = Rsearch();
                        break;
                    case "refer_main":
                        responseData = ReferMain();
                        break;
                    case "refer_main_type":
                        responseData = ReferMainType();
                        break;
                }
            }

            return responseData;
        }

        private ResponseData NewsInitTemplate()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Preview()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Preview.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData InitMain()
        {
            var templatePath = "\\Modules\\NewsList\\Templates\\Main.aspx";
            if (UIHelper.isSmartPhone())
            {
                templatePath = "\\Modules\\NewsList\\Templates\\Tablet\\Main.aspx";
            }
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, templatePath);
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListNews()
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var status = GetQueryString.GetPost("status", 0);
                var templatePath = string.Empty;
                switch (status)
                {
                    case (int)NewsStatus.Published:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\Published.aspx";
                        break;
                    case (int)NewsStatus.Temporary:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\Temporary.aspx";
                        break;
                    case (int)NewsStatus.WaitForPublish:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\WaitForPublish.aspx";

                        break;
                    case (int)NewsStatus.MovedToTrash:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\MovedToTrash.aspx";
                        break;
                    case (int)NewsStatus.Unpublished:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\Unpublished.aspx";
                        break;
                    case (int)NewsStatus.Reject:
                        templatePath = "\\CMS\\Modules\\News\\Templates\\Reject.aspx";
                        break;
                }



                responseData = ConvertResponseData.CreateResponseData("", 1, templatePath);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }
        private ResponseData SendComment()
        {
            var responseData = new ResponseData();

            return responseData;
        }
        public ResponseData News_Save()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                var id = GetQueryString.GetPost("Id", 0L);
                var title = GetQueryString.GetPost("title", string.Empty);
                var sapo = GetQueryString.GetPost("sapo", string.Empty);
                var avatar = GetQueryString.GetPost("avatar", "");
                var zone = GetQueryString.GetPost("zone", 0);
                var source = GetQueryString.GetPost("source", "");
                var hot = GetQueryString.GetPost("hot", false);
                var comment = GetQueryString.GetPost("comment", false);
                var video = GetQueryString.GetPost("video", false);
                var metakeyword = GetQueryString.GetPost("metakeyword", "");
                var titleseo = GetQueryString.GetPost("titleseo", "");
                var metadescription = GetQueryString.GetPost("metadescription", "");
                var date = GetQueryString.GetPost("date", DateTime.MinValue);
                var status = GetQueryString.GetPost("status",0);
                var url = GetQueryString.GetPost("url", string.Empty);
                var body = GetQueryString.GetPost("content", string.Empty);
                var tagIdList = GetQueryString.GetPost("tagIdList", string.Empty);
                body = HttpContext.Current.Server.UrlDecode(body);
                var userName = Policy.GetAccountName();
                if (!string.IsNullOrEmpty(title))
                {
                    var shortUrl = Utility.UnicodeToKoDauAndGach(title);
                    var news = new NewsEntity
                    {
                        Title = title,
                        Sapo = sapo,
                        Body = body,
                        Avatar = avatar,
                        Author = userName,
                        Status = status,
                        Source = source,
                        Type = (int)ZoneType.News,
                        CreatedBy = userName,
                        EditedBy = userName,
                        LastModifiedBy = userName,
                        Url = url,
                        WordCount = Utility.CountWords(body),
                        CreatedDate = DateTime.Now,
                          DistributionDate = (int)NewsStatus.Published== status?DateTime.Now : DateTime.MinValue,
                        Id = id,
                        IsOnMobile = false,
                        SourceURL = "",
                        MetaKeyword = metakeyword,
                        MetaDescription = metadescription,
                        TitleSeo = titleseo,
                        IsAllowComment = comment,
                        IsVideo = video,
                        IsHot = hot
                    };
                    if (status == (int)NewsStatus.Published)
                        news.PublishedBy = userName;

                    if (id > 0)
                    {
                        news.Id = id;
                       
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.UpdateNews(news, zone, tagIdList, userName, true, ref shortUrl));
                        responseData.Data = id;
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(NewsBo.InsertNews(news, zone, tagIdList, userName,ref id, ref shortUrl));
                        responseData.Data = id;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid news's name";
                }
            }
            return responseData;
        }
        private ResponseData HotMain()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\NewsEmbed\\Main.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData ReferMainType()
        {
            ResponseData responseData;

            var type = GetQueryString.GetPost("type", string.Empty);
            if (type.Equals("product"))
            {
                responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\PRefer\\Main.aspx");

            }
            else
            {
                responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\NRefer\\Main.aspx");

            }

            responseData.Success = true;
            return responseData;
        }
        private ResponseData ReferMain()
        {
            ResponseData responseData;
            responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\Refer\\Main.aspx");

            responseData.Success = true;
            return responseData;
        }
       

        private ResponseData List()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Product\\Templates\\List.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Psearch()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\NewsEmbed\\PList.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData Rsearch()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\News\\Templates\\NewsEmbed\\RList.aspx");
            responseData.Success = true;
            return responseData;
        }
        public ResponseData Publish()
        {
            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = Publish(id);
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;
                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }

        public ResponseData Publish(long encryptNewsId)
        {
            var username = Policy.GetAccountName();
            var publishedDate = GetQueryString.GetPost("publishedDate", DateTime.MinValue);
            var data = NewsBo.ChangeStatusToPublished(encryptNewsId, username, publishedDate);
            var responseData = ConvertResponseData.CreateResponseData(data);
            if (responseData.Success) responseData.Data = CryptonForId.EncryptId(encryptNewsId);
            return responseData;
        }
        public ResponseData ChangeStatusComment()
        {
            var listNewsId = GetQueryString.GetPost("id", 0L);
            var username = Policy.GetAccountName();
            var status = GetQueryString.GetPost("status", 0);

            var data = NewsBo.ChangeStatusComment(listNewsId, username, (byte)status);
            var responseData = ConvertResponseData.CreateResponseData(data);
            if (responseData.Success) responseData.Data = CryptonForId.EncryptId(listNewsId);
            return responseData;
        }
        public ResponseData Reject()
        {
            int id = 0;
            var listNewsId = GetQueryString.GetPost("id", 0L);
            var content = GetQueryString.GetPost("content", string.Empty);
            var username = Policy.GetAccountName();

            var data = NewsBo.ChangeStatusToReject(listNewsId, username);
            var responseData = ConvertResponseData.CreateResponseData(data);

            if (responseData.Success)
            {
              
            }
            return responseData;
        }
       


        public ResponseData Unpublish()
        {
            var username = Policy.GetAccountName();
            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";
            Logger.WriteLog(Logger.LogType.Trace, "Unpublish ListNewsid=" + listNewsId);
            try
            {
                var newsIds = listNewsId.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                {
                    responseData = ConvertResponseData.CreateResponseData(NewsBo.Unpublish(id, username));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            Logger.WriteLog(Logger.LogType.Trace, "Unpublished ListNewsId=" + listNewsIdUpdated);
            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public ResponseData RemoveToTrash()
        {
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(NewsBo.MoveToTrash(id, username));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public ResponseData ChangeStatusToWaitPublish()
        {
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(NewsBo.ChangeStatusToWaitPublish(id, username));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }

    }
}
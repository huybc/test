﻿using Mi.Action.Core;
using Mi.BO.Base.Customer;
using Mi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RBLand.CMS.Modules.Customer.Template
{
    public partial class hoptac : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            string name = GetQueryString.GetPost("name", string.Empty);
            string phone = GetQueryString.GetPost("phone", string.Empty);
            string type = GetQueryString.GetPost("type", string.Empty);

            var pageIndex = GetQueryString.GetPost("pageindex", 1);
            var pagesize = GetQueryString.GetPost("pagesize", 10);



            DataRpt.DataSource = CustomerBo.Search(name, phone, "cooperater", pageIndex, pagesize, ref TotalRows);
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }
    }
}
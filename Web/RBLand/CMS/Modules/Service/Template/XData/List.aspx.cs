﻿using System;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Action.Core;

namespace RBLand.CMS.Modules.Service.Template.XData
{
    public partial class List : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
          
            int status = GetQueryString.GetPost("status", 0);
            string title = GetQueryString.GetPost("title", string.Empty);
            var fromDate = GetQueryString.GetPost("from", DateTime.MinValue);
            var todate = GetQueryString.GetPost("to", DateTime.MaxValue);
            var pageIndex = GetQueryString.GetPost("pageindex", 0);
            var pagesize = GetQueryString.GetPost("pagesize", 0);
            var keyWord = GetQueryString.GetPost("keyword", String.Empty);
            var zoneId = GetQueryString.GetPost("zone", 0);
            var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
            var username = GetQueryString.GetPost("account", PolicyProviderManager.Provider.GetAccountName());
            var order = GetQueryString.GetPost("order", (int)ZoneSortExpression.CreatedDateDesc);
            var type = GetQueryString.GetPost("type", (int)ZoneType.All);
            pagesize = pagesize == 0 ? 50 : pagesize;


            if (todate != DateTime.MinValue) todate = new DateTime(todate.Year, todate.Month, todate.Day, 23, 59, 59);

            var sortBy = ZoneSortExpression.CreatedDateDesc;
         //   var status = ZoneStatus.Publish;
            DataRpt.DataSource = ZoneBo.ZoneSearch(keyWord, username, zoneIds, fromDate, DateTime.MaxValue, ZoneFilterFieldForUsername.All, sortBy,status, type, pageIndex, pagesize, ref TotalRows);
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }
    }
}

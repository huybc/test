﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using Mi.Action;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.ErrorCode;
using Mi.Action.Core;

namespace RBLand.CMS.Modules.Slider.Action
{
    public class AdvActions: ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;

                    case "save":
                        responseData = Save();
                        break;
                    case "delete":
                        responseData = Delete();
                        break;

                }
            }

            return responseData;
        }

       
        private ResponseData Delete()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                try
                {
                    var id = GetQueryString.GetPost("id", 0);
                    ConfigBo.AdvDelete(id);
                    responseData.Success = true;
                }
                catch (Exception)
                {

                    responseData.Success = false;
                }

            }
            return responseData;
        }
        private ResponseData Save()
        {
            var responseData = new ResponseData();

            var pjson = GetQueryString.GetPost("json", string.Empty);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var objs = serializer.Deserialize<List<AdvJsonEntity>>(pjson);
            try
            {
                foreach (var item in objs)
                {
                    var obj = new AdvEntity
                    {
                        Id = item.id,
                        Name = item.label,
                        Thumb = item.img + "",//
                        Content = item.des + "",//
                        IsEnable = item.isenable,
                        SortOrder = item.order,
                        Url = item.url,
                        Type = item.type

                    };
                    if (obj.Id > 0)
                    {
                    ConfigBo.AdvUpdate(obj);

                    }
                    else
                    {
                        ConfigBo.AdvInsert(obj);

                    }
                }
                responseData.Success = true;

            }
            catch (Exception)
            {

                responseData.Success = false;
            }




            return responseData;
        }


       
        public class AdvJsonEntity
        {
            public int id { get; set; }
            public int order { get; set; }
            public byte type { get; set; }
            public string label { get; set; }
            public string des { get; set; }
            public string img { get; set; }
            public string url { get; set; }
            public bool isenable { get; set; }
        }
    }
}
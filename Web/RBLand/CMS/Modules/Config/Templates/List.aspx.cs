﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Mi.Action.Core;

namespace RBLand.CMS.Modules.Config.Templates
{
    public partial class List : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static List<posterEntity> GetImage(string json)
        {
            var imgs = new List<posterEntity>();
            if (!string.IsNullOrEmpty(json))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var objs = serializer.Deserialize<List<posterEntity>>(json);
                try
                {
                    foreach (var item in objs)
                    {

                        imgs.Add(new posterEntity
                        {
                            img = item.img,
                            url = item.url
                        });

                    }

                }
                catch (Exception)
                {

                }
            }
            return imgs;
        }


    }
    public class posterEntity
    {
        public string img { get; set; }
        public string url { get; set; }

    }


}
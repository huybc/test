﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuLeft.ascx.cs" Inherits="RBLand.CMS.Controls.MenuLeft" %>
<div class="sidebar-content">

    <!-- User menu -->
   <%-- <div class="sidebar-user">
        <div class="category-content">
            <div class="media">
                <a href="#" class="media-left">
                    <img src="/CMS/Themes/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                <div class="media-body">
                    <span class="media-heading text-semibold">Victoria Baker</span>
                    <div class="text-size-mini text-muted">
                        <i class="icon-pin text-size-small"></i>&nbsp;Santa Ana, CA
								
                    </div>
                </div>

                <div class="media-right media-middle">
                    <ul class="icons-list">
                        <li>
                            <a href="#"><i class="icon-cog3"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>--%>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
        <div class="category-content no-padding">
            <ul class="navigation navigation-main navigation-accordion">

                <!-- Main -->
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                <li class="active"><a href="index.html"><i class="icon-home4"></i><span>Dashboard</span></a></li>
                <li>
                    <a href="#"><i class="icon-stack2"></i><span>Sản phẩm</span></a>
                    <ul>
                        <li><a href="layout_navbar_fixed.html">Chuyên mục</a></li>
                        <li><a href="layout_sidebar_fixed_native.html">Hãng sản xuất</a></li>
                        <li><a href="layout_sidebar_fixed_native.html">Mầu sẩn phẩm</a></li>
                        <li><a href="layout_navbar_sidebar_fixed.html">Danh sách sản phẩm</a></li>
                        <li><a href="layout_navbar_sidebar_fixed.html">Sản phẩm lưu tạm</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-copy"></i><span>Tin bài</span></a>
                    <ul>
                        <li><a href="../../layout_1/LTR/index.html" id="layout1">Chuyên mục</a></li>
                        <li><a href="index.html" id="layout2">Bài lưu tạm</a></li>
                        <li><a href="../../layout_4/LTR/index.html" id="layout4">Bài chờ duyệt</a></li>
                        <li><a href="../../layout_3/LTR/index.html" id="layout3">Bài đã duyệt</a></li>
                        <li><a href="../../layout_3/LTR/index.html" id="layout3">Bài bị xóa</a></li>
                        <li><a href="../../layout_3/LTR/index.html" id="layout3">Giới thiệu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-droplet2"></i><span>Slide</span></a>
                    <ul>
                        <li><a href="colors_primary.html">Primary palette</a></li>
                        <li><a href="colors_danger.html">Danger palette</a></li>
                        <li><a href="colors_success.html">Success palette</a></li>
                        <li><a href="colors_warning.html">Warning palette</a></li>
                        <li><a href="colors_info.html">Info palette</a></li>
                        <li class="navigation-divider"></li>
                        <li><a href="colors_pink.html">Pink palette</a></li>
                        <li><a href="colors_violet.html">Violet palette</a></li>
                        <li><a href="colors_purple.html">Purple palette</a></li>
                        <li><a href="colors_indigo.html">Indigo palette</a></li>
                        <li><a href="colors_blue.html">Blue palette</a></li>
                        <li><a href="colors_teal.html">Teal palette</a></li>
                        <li><a href="colors_green.html">Green palette</a></li>
                        <li><a href="colors_orange.html">Orange palette</a></li>
                        <li><a href="colors_brown.html">Brown palette</a></li>
                        <li><a href="colors_grey.html">Grey palette</a></li>
                        <li><a href="colors_slate.html">Slate palette</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-stack"></i><span>Cấu hình</span></a>
                    <ul>
                        <li><a href="starters/horizontal_nav.html">Horizontal navigation</a></li>
                        <li><a href="starters/1_col.html">1 column</a></li>
                        <li><a href="starters/2_col.html">2 columns</a></li>
                        <li>
                            <a href="#">3 columns</a>
                            <ul>
                                <li><a href="starters/3_col_dual.html">Dual sidebars</a></li>
                                <li><a href="starters/3_col_double.html">Double sidebars</a></li>
                            </ul>
                        </li>
                        <li><a href="starters/4_col.html">4 columns</a></li>
                        <li>
                            <a href="#">Detached layout</a>
                            <ul>
                                <li><a href="starters/detached_left.html">Left sidebar</a></li>
                                <li><a href="starters/detached_right.html">Right sidebar</a></li>
                                <li><a href="starters/detached_sticky.html">Sticky sidebar</a></li>
                            </ul>
                        </li>
                        <li><a href="starters/layout_boxed.html">Boxed layout</a></li>
                        <li class="navigation-divider"></li>
                        <li><a href="starters/layout_navbar_fixed_main.html">Fixed main navbar</a></li>
                        <li><a href="starters/layout_navbar_fixed_secondary.html">Fixed secondary navbar</a></li>
                        <li><a href="starters/layout_navbar_fixed_both.html">Both navbars fixed</a></li>
                        <li><a href="starters/layout_fixed.html">Fixed layout</a></li>
                    </ul>
                </li>
                <li><a href="changelog.html"><i class="icon-puzzle2"></i><span>Quảng cáo</span></a></li>
                <li><a href="changelog.html"><i class="icon-users" title="Forms"></i><span>Quản lý User</span> </a></li>
                <!-- /main -->


                
            </ul>
        </div>
    </div>
    <!-- /main navigation -->

</div>

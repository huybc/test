﻿
define(R.ModuleSetting.ActiveModules.MODULE_ACCOUNT_V2_NAME,
    ["IModule", "Util", "Debug"],
    function (IModule, Util, Debug) {

        function Account(cb) {
            if (typeof (cb) == "undefined") {
                cb = function() {
                    p.Init();
                };
            }
            Account.superclass.constructor.call(this, R.ModuleSetting.ActiveModules.MODULE_ACCOUNT_V2_NAME, { "hasConfig": true, "cb": cb });
        }

        Util.extend(Account, IModule);
        var p = Account.prototype;

        p.PageIndex = 1;
        p.PageSize = 50;
        p.RoleType = {
            Reporter: 1,
            Editor: 2,
            Secretary: 3
        };
        p.CurrentRole = null;
        p.pageIndex = 1;
        p.grouplistCached = null;
        p.KeyWord = '';
        p.Status = '';

        p.provide('Init', function () {
            p.Scroll = null;
            this.InitTemplate();
        });

        p.provide('InitTemplate', function () {
            var self = this;
            R.Post({
                params: {},
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "init_main",
                success: function (res) {
                    if (res.Success) {
                        $('#IMSMainContentWrapper').html(res.Content);

                        $('#IMSUserStatus').fselect();

                        $('#AddNewUser').off('click').on('click', function () {
                            self.AddUpdateAccount();
                            return false;
                        });

                        $('#IMSQuickEditNews').off('click').on('click', function (e) {
                            e.stopPropagation();

                            var _quickupdate = R.ManagerModule.findModule(R.ModuleSetting.ActiveModules.MODULE_QUICK_UPDATE_V2_NAME);
                            if (_quickupdate == null) {
                                _quickupdate = new QuickUpdate();
                                R.ManagerModule.registerModule(_quickupdate);
                                _quickupdate.AddNew();
                            } else {
                                _quickupdate.AddNew();
                            }
                            return false;
                        });

                        $('#IMSCrawlNews').off('click').on('click', function (e) {
                            e.stopPropagation();
                            if (R.QuickCrawler != undefined) {
                                R.QuickCrawler.ShowCrawlPopUp();
                            }
                            return false;
                        });

                        self.GetList();
                    }
                }
            });
        });
        p.Scroll = null;
        p.provide('GetList', function () {
            R.StartLoading();
            var self = this;
            var key = self.KeyWord = $("#UserSearchKeyword").val();
            var status = self.Status = $("#IMSUserStatus").val();
            R.Post({
                params: {
                    "page": p.PageIndex,
                    "num": p.PageSize,
                    "keyword": key,
                    "status": status
                },
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "userlist",
                success: function (res) {
                    if (res.Success) {
                        $('#IMSUserList').html(res.Content);
                        var TotalRow = $('#hf_users_total_row').attr('value');

                        self.WrapPaging(TotalRow, self.PageIndex, self.PageSize, "#IMSUserPager", function (pageIndex, pageSize) {
                            self.PageIndex = pageIndex;
                            self.PageSize = pageSize;
                            self.GetList();
                        });
                        if (!isSmartPhone) {
                            R.ScrollAutoSize('#IMSListItems', function () {
                                return self.CalculateListHeight();
                            });
                        } else {
                            $('#IMSListItems').css({
                                height: self.CalculateListHeight()
                            });
                            if (p.Scroll == null) {
                                p.Scroll = new iScroll('IMSListItems');
                            } else {
                                p.Scroll.refresh();
                            }
                        }
                    }
                    R.StopLoading();

                    $("#UserSearchKeyword").val(p.KeyWord);
                    self.registerEvents();
                }
            });
        });

        p.provide('CalculateListWidth', function () {
            var w = $('#IMSMain').innerWidth() - ($('#IMSRight').innerWidth() + 42);
            if ($(window).width() > 1500) {
                w -= $('#IMSRight').innerWidth();
            }

            if (w < 600) {
                w = $('#IMSMain').innerWidth() - 42;
            }
            return w;
        });

        p.provide('CalculateListHeight', function () {
            return $(window).height() - $('#IMSListContentHeader').offset().top - $('#IMSListContentHeader').height() - 100;
        });

        p.provide('registerEvents', function () {
            var self = this;

            $('.UserItem').find('.Edit').off('click').on('click', function () {
                self.AddUpdateAccount($(this).attr('data'));
            });

            $('.UserItem').find('.Permission').off('click').on('click', function () {
                $('.UserItem').removeClass('UserItemActive');
                $(this).parent().parent().addClass('UserItemActive');

                $('.UserItem').css({ 'background-color': '' });
                $(this).parent().parent().css({ 'background-color': '#e8f4f7' });

                self.Edit($(this).attr('data'), $(this).attr('username'));
            });

            $('.UserItem').find('.Reset').off('click').on('click', function () {
                self.ResetPassword($(this).attr('data'), $(this).attr('username'));
            });

            $('#IMSUserSearchTopTrigger').off('click').on('click', function () {
                self.GetList();
            });
            $('#UserSearchKeyword').off('keyup').on('keyup', function (evt) {
                if (evt.keyCode == 13) {
                    self.GetList();
                }
            });

            $('#IMSUserStatus').off('change').on('change', function () {
                self.GetList();
            });
        });

        p.provide('ResetPassword', function (userid, username) {
            R.Popup({
                title: "Reset Password",
                width: 480,
                height: 200,
                skin: 'ims',
                objContainer: '#VCAccountResetPassword',
                buttons: {
                    "Cập nhật": function () {
                        var popup = $(this);
                        var pwd = $('#VCAccountResetPassword').find("#txt_password_update").val().trim();
                        if (userid != "" && pwd != "") {
                            R.Post({
                                params: {
                                    "userid": userid,
                                    "pwd": pwd
                                },
                                module: "account",
                                ashx: 'modulerequest.ashx',
                                action: "resetpwd",
                                success: function (res) {
                                    if (res.Success) {
                                        R.MessageBox("Thay đổi mật khẩu thành công");
                                        popup.dialog('close');
                                    } else {
                                        R.MessageBox("Có lỗi xảy ra, hãy thử lại");
                                    }
                                }
                            });
                        }
                    },
                    "Hủy": function () {
                        $(this).dialog('close');
                    }
                },
                HtmlBinding: function (obj) {
                    $(obj).html('<div id="AccountUpdatePasswordForm"><span>Nhập mật khẩu: </span> <input type="password" id="txt_password_update"/> </div>');
                }
            });

        });

        p.provide('AddUpdateAccount', function (id) {
            if (id == undefined) {
                id = "";
            };

            var op = {
                module: "account",
                action: "addaccount",
                ashx: 'modulerequest.ashx',
                params: {
                    "userid": id
                },
                success: function (res) {
                    if (res.Success) {
                        var data = res.Data;
                        if (typeof (data) == 'string' && data != '') {
                            data = data.replace(/(new Date\(([0-9\-]+)\))/gi, '"/Date($2)"');
                            data = JSON.parse(data);
                        }
                        R.Popup({
                            title: id == '' ? "Thêm mới tài khoản" : "Cập nhật thông tin tài khoản",
                            width: 550,
                            height: id == '' ? 370 : 320,
                            skin: 'ims',
                            autoScroll: false,
                            objContainer: '#VCAccountAddUpdate',
                            buttons: {
                                "Cập nhật": function () {
                                    var $this = $(this);

                                    var userid = $("#cmsbm_txt_userid").val();
                                    var fullname = $("#txt_account_fullname").val();
                                    var username = $("#txt_account_username").val();
                                    var password = $("#txt_account_password").val();
                                    var address = $("#txt_account_address").val();
                                    var description = $("#txt_account_des").val();
                                    var mobile = $("#txt_account_mobile").val();
                                    var email = $("#txt_account_email").val();
                                    var birthday = getDateTimePicker($("#cms_bm_news_birthday_date").val(), false);
                                    var avatar = $('#UserAvatarDetail img').attr('src');
                                    var status = $("#DDLUserStatus").val();

                                    if (userid == '') {
                                        if (username.trim() == "") {
                                            R.MessageBox("Tên đăng nhập không được để trống.");
                                            return false;
                                        }
                                        if (password.trim() == "") {
                                            R.MessageBox("Mật khẩu không hợp lệ.");
                                            return false;
                                        }
                                    }

                                    //if (email.trim() == "" || !validateEmail(email)) {
                                    //    R.MessageBox("Email không hợp lệ.");
                                    //    return false;
                                    //}

                                    var params = null;
                                    if (userid == '' || id == '') {
                                        params = {
                                            'userid': userid,
                                            'fullname': fullname,
                                            'username': username,
                                            'password': password,
                                            'address': address,
                                            'description': description,
                                            'mobile': mobile,
                                            'email': email,
                                            'birthday': birthday,
                                            'avatar': avatar,
                                            'status': status
                                        };
                                    } else {
                                        params = {
                                            'userid': userid,
                                            'fullname': fullname,
                                            //'username': username,
                                            'address': address,
                                            'description': description,
                                            'mobile': mobile,
                                            'email': email,
                                            'birthday': birthday,
                                            'avatar': avatar,
                                            'status': status
                                        };
                                    }

                                    R.Post({
                                        module: "account",
                                        params: params,
                                        ashx: 'modulerequest.ashx',
                                        action: "add_update_account",
                                        success: function (res1) {
                                            if (res1.Success) {
                                                $this.dialog('close');
                                                p.GetList();
                                            } else {
                                                R.MessageBox(res1.Message);
                                            }
                                        }
                                    });

                                },
                                "Hủy": function () {
                                    $(this).dialog('close');
                                }
                            },
                            HtmlBinding: function (obj) {
                                R.BindData('#VCAccountAddUpdate', res.Content, data);
                                var ddlStatus = $("#DDLUserStatus");
                                ddlStatus.val(ddlStatus.attr('data')).fselect();

                                if (id == '') {
                                    $('#PasswordInfoRow').css('display', '');
                                } else {
                                    $("#trUserNameInfo").css('display', 'none');
                                }

                                $('#cms_bm_news_birthday_date').datetimepicker({
                                    dateFormat: "dd/mm/yy",
                                    showTimepicker: false,
                                    changeYear: true,
                                    changeMonth: true,
                                    maxDate: "+0d",
                                    showOptions: { direction: 'up' },
                                    showOn: "focus"
                                });
                                $('#IMSChangeMyAvatarBtn').off('click').on('click', function () {
                                    IMS.Plugins.PhotoManager.Init({
                                        callback: function (arrImage) {
                                            $.each(arrImage, function (i, item) {
                                                $('#UserAvatarDetail img').attr('src', item.url);
                                            });
                                        }
                                    });
                                });
                                $('#cms_bm_reset_password_trigger').off('click').on('click', function () {
                                    self.ResetPassword();
                                });

                                p.DragDropUploadInitAddAccount();
                            }
                        });
                    }
                }
            };
            R.Post(op);
        });

        p.ScrollOnSetupPermission == null;

        p.provide('Edit', function (id, username) {
            alert(1)
            var self = this;
            R.Post({
                params: {
                    "userid": id
                },
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "edit",
                success: function (res) {
                    if (res.Success) {
                        R.ShowOverlay(res.Content, function () {
                            $('.PBox:even').addClass('PBoxLeft');
                            $('.PBox:odd').addClass('PBoxRight');
                            var scrollHeight = $('#IMSOverlayContent').height() - $('#PermissionUpdateHeader').height() - $('#PermissionUpdate_Body_Header').height() - 50;

                            $("#upadtepermission_account_username").text(username);
                            if (!isSmartPhone) {
                                $('.PBox_Body_Right').css('height', '185px').slimScroll();
                            } else {
                                $('.PBox').css('height', 'auto');
                            }
                            if (!isSmartPhone) {
                                $('#BoxPermission').css({
                                    height: scrollHeight
                                });
                                $('#BoxPermission').slimScroll({ distance: '0px' });
                            } else {
                                $('#Container').css({
                                    height: $('.PBox').outerHeight(true) * ($('.PBox').length / 2)
                                })
                                $('#BoxPermission').css({
                                    height: scrollHeight
                                });
                                p.ScrollOnSetupPermission = new iScroll('BoxPermission');
                            }
                            $('#BoxPermission').find('.CheckBoxAll').off('click').on('click', function () {
                                var me = $(this);
                                var parentRoot = me.parent().parent().parent();
                                var selectedGroup = parentRoot.parent().parent().find('.PBox_Body_Left').find('.Active');
                                var arrayCheckBox = parentRoot.find('input[type="checkbox"]');
                                var checkedState = me.is(':checked');
                                for (var i = 0, len = arrayCheckBox.length; i < len; i++) {
                                    if (checkedState) {
                                        $(arrayCheckBox[i]).addClass('checked');
                                        $(arrayCheckBox[i]).attr('checked', true);
                                    } else {
                                        $(arrayCheckBox[i]).removeClass('checked');
                                        $(arrayCheckBox[i]).attr('checked', false);
                                    }
                                }

                                if (selectedGroup.length > 0) {
                                    //Save zones to zone attribute
                                    self.SaveSelectedZones();
                                } else {
                                    R.MessageBox('Bạn chưa chọn nhóm quyền, vui lòng chọn nhóm quyền bên trái trước khi thực hiện thao tác này!', 'Thông báo', function () { });
                                }
                            });

                            $('.PRole').off('click').on('click', function () {
                                var me = $(this);
                                var cbRoot = me.parent().parent().find('.CheckBoxAll');
                                cbRoot.removeClass('checked');
                                cbRoot.attr('checked', false);

                                $('.PRole').removeClass('Active');
                                me.addClass('Active');
                                var listZones = $(this).attr('zones');
                                listZones != undefined ? listZones : '';
                                self.ShowMyZone(listZones, me);
                                return false;
                            });

                            $('#CbAllPermission').off('click').on('click', function () {
                                var me = $(this);

                                if (me.hasClass('checked')) {
                                    me.attr('checked', false);
                                    me.removeClass('checked');
                                } else {
                                    me.attr('checked', true);
                                    me.addClass('checked');
                                }
                            });

                            $('#CbAllTopicPermission').off('click').on('click', function () {
                                var me = $(this);

                                if (me.hasClass('checked')) {
                                    me.attr('checked', false);
                                    me.removeClass('checked');
                                } else {
                                    me.attr('checked', true);
                                    me.addClass('checked');
                                }
                            });

                            $('#BTNSaveButton').off('click').on('click', function () {
                                self.Save(id);
                                return false;
                            });

                            $('.PBox_Body_Right').find('.IMSCheckbox[isroot="0"]').off('click').on('click', function () {
                                var me = $(this);

                                var checkedState = me.is(':checked');
                                if (checkedState) {
                                    me.addClass('checked');
                                    me.attr('checked', true);
                                } else {
                                    me.removeClass('checked');
                                    me.attr('checked', false);
                                }

                                var demoGroup = self.CheckConditionsToSelectZone(this);
                                if (demoGroup != null) {
                                    R.MessageBox('Bạn chưa chọn nhóm quyền, vui lòng chọn nhóm quyền bên trái trước khi thực hiện thao tác này!', 'Thông báo', function () {
                                        demoGroup.fadeOut("slow");
                                        demoGroup.fadeIn("slow");
                                    });
                                } else {
                                    self.SaveSelectedZones();
                                }
                                //return false;
                            });
                        },
                        function () {
                            $('.UserItem[data="' + id + '"]').removeClass('UserItemActive');
                            $('.UserItem[data="' + id + '"]').css({ 'background-color': '' });
                        });
                    }
                }
            });

        });

        p.provide('ShowMyZone', function (zones, me) {
            var self = this;
            self.CurrentRole = me;
            var pZones = zones.split(';');
            var pCheckboxArray = me.parent().parent().find('input[type="checkbox"]');
            var cb = null;
            var ZoneId = null;
            for (var i = 0, len = pCheckboxArray.length; i < len; i++) {
                cb = $(pCheckboxArray[i]);
                if (cb.attr('isroot') == '0') {
                    ZoneId = cb.attr('data').split('-')[0];
                    if (jQuery.inArray(ZoneId, pZones) != -1) {
                        cb.addClass('checked');
                        cb.attr('checked', true);
                    } else {
                        cb.removeClass('checked');
                        cb.attr('checked', false);
                    }
                }
            }
        });

        p.provide('SaveSelectedZones', function () {
            var self = this;
            if (self.CurrentRole != null && self.CurrentRole != 'undefined') {
                var arrayCheckBox = self.CurrentRole.parent().parent().find('.PBox_Body_Right').find('.IMSCheckbox[isroot="0"]');
                if (arrayCheckBox != undefined && arrayCheckBox.length > 0) {
                    var listZones = '', zoneId = null, cb = null;
                    for (var i = 0, len = arrayCheckBox.length; i < len; i++) {
                        cb = $(arrayCheckBox[i]);
                        if (cb.hasClass('checked') || cb.is(':checked')) {
                            zoneId = cb.attr('data').split('-')[0];
                            listZones += ';' + zoneId;
                        }
                        cb = null;
                    }

                    if (listZones != '') {
                        listZones = listZones.substring(1, listZones.length);
                        self.CurrentRole.find('i').addClass('HasPermission');
                    } else {
                        self.CurrentRole.find('i').removeClass('HasPermission');
                    }

                    self.CurrentRole.attr('zones', listZones);
                }
            }
        });

        p.provide('CheckConditionsToSelectZone', function (me) {
            var self = this;
            var groupBoxId = $(me).parent().parent().parent().attr('group');
            var pGroupEntity = $('#PBox_' + groupBoxId);
            if (pGroupEntity.length > 0) {
                var pBoxBodyLeft = pGroupEntity.find('.PBox_Body').find('.PBox_Body_Left');
                var pBoxRoles = pBoxBodyLeft.find('.PRole');
                var pBoxActiveRoles = pBoxBodyLeft.find('.Active');

                if (pBoxActiveRoles.length <= 0) {
                    self.CurrentRole == null;
                    if (pBoxRoles.length > 0) {
                        var demoGroup = $(pBoxRoles[0]);
                        return demoGroup;
                    }
                    return null;
                }
                return null;
            }
            return null;
        });

        p.provide('SaveAddAccount', function () {
            var elm = "#template_userlist";
            var userid = $("#cmsbm_txt_userid").val();
            if (userid == '' || userid == '0') {
                var username = $("#cmsbm_txt_username").val();
                var password = $("#cmsbm_txt_password").val();
                if (username.trim() == "") {
                    R.MessageBox("Tên đăng nhập không hợp lệ.");
                    return false;
                }
                if (username.trim() == "") {
                    R.MessageBox("Mật khẩu không hợp lệ.");
                    return false;
                }
            }
            var email = $("#cmsbm_txt_email").val();
            if (email.trim() == "" || !validateEmail(email)) {
                R.MessageBox("Email không hợp lệ.");
                return false;
            }
            var dtval = getDateTimePicker($("#cms_bm_news_birthday_date").val(), false);
            $("#hdf_cms_bm_news_birthday_date").val(dtval);
            var postForm = $("#cmsbm_frmupdate_user").serializeArray();
            R.Post({
                params: postForm,
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "add_update_account",
                success: function (res) {
                    if (res.Success) {
                        R.MessageBox("Cập nhật thành công.");
                        p.GetList();
                    } else {
                        R.MessageBox("Có lỗi xảy ra.Vui lòng kiểm tra email hoặc account có trùng không ?");
                    }
                }
            });

        });

        p.provide('Save', function (id) {

            var groupPermission = $('#BoxPermission .PRole');
            var length = groupPermission.length;
            var gPermission = null;
            var listZones = '';
            var zonelist = '';
            var permission = '';
            var prs_permis = "";

            for (var i = 0; i < length; i++) {
                gPermission = $(groupPermission[i]);
                listZones = gPermission.attr('zones');
                permission = gPermission.attr('data');
                if (listZones != undefined && listZones != '' && typeof (listZones) != "undefined") {
                    $.each(listZones.split(';'), function (index, zone) {
                        if (zone != '') {
                            zonelist += ',' + zone;
                        }
                    });

                    zonelist = zonelist.substr(1);
                    prs_permis += ';G' + permission + '(' + zonelist + ')';
                }
                gPermission = null;
                permission = '';
                listZones = '';
                zonelist = '';
            }

            if (prs_permis != '') {
                prs_permis = prs_permis.substr(1);
            }

            var isFullZone = $('input[id="CbAllTopicPermission"]').is(':checked');
            var isFullPermiss = $('input[id="CbAllPermission"]').is(':checked');

            if (isFullZone == false && prs_permis == "") {
                R.MessageBox("Bạn chưa chọn quyền cho tài khoản");
                return false;
            }

            var jsonString = '"userid":"' + id + '", "IsFullPermission":"' + isFullPermiss + '", "IsFullZone":"' + isFullZone + '"';
            jsonString = "{" + jsonString + ', "zonelist":"' + prs_permis + '"}';

            R.Waiting('Đang cập nhật dữ liệu, xin đợi...', '#IMSWaitingSaveUserPermission');
            R.Post({
                params: JSON.parse(jsonString),
                module: "account",
                action: "update",
                ashx: 'modulerequest.ashx',
                success: function (res) {
                    $('.UserItem').removeClass('UserItemActive');
                    $('.UserItem').css({ 'background-color': '' });
                    R.CloseOverlay();
                    if (res.Success) {
                        R.MessageBox("Cập nhật thành công.");
                    } else {
                        R.MessageBox("Có lỗi xảy ra.Vui lòng kiểm tra email hoặc account có trùng không ?");
                    }
                    $('#IMSWaitingSaveUserPermission').dialog('close');
                }
            });
        });

        /*##########################################*/
        /*# Other functions are using for this module
        /*##########################################*/

        function changeClassInputCheck(checkboxId, elmId) {
            if (document.getElementById(checkboxId).checked) {
                $(elmId).removeClass("icon_check").addClass("icon_checked");
            } else {
                $(elmId).removeClass("icon_checked").addClass("icon_check");
            }
        }
        function changeValCheckbox(checkboxId, elmId) {
            var val = document.getElementById(checkboxId).checked;
            if (val == undefined) val = false;
            document.getElementById(checkboxId).checked = !val;
            changeClassInputCheck(checkboxId, elmId);
        }

        p.provide('ChangeAvatar', function () {
            R.Popup({
                title: "Thay đổi ảnh đại diện",
                width: 310,
                height: 350,
                autoScroll: false,
                zIndex: 500,
                skin: 'ims',
                objContainer: '#VCAccountChangeAvatar',
                buttons: {
                    "Cập nhật": function () {
                        var popUp = $(this);
                        var img = $('#CmsBm_Acount_ChangeAvatarWrapperDetail img');
                        if (img.length <= 0) {
                            R.MessageBox('Bạn chưa chọn ảnh!');
                            return false;
                        }
                        var url = img.attr('src');
                        R.Post({
                            module: "account",
                            ashx: 'modulerequest.ashx',
                            params: {
                                "avatar": url
                            },
                            action: "update_avatar",
                            success: function (res) {
                                if (res.Success) {
                                    popUp.dialog('close');
                                    R.MessageBox('Cập nhật avatar thành công!');
                                }
                            }
                        });
                    },
                    "Hủy": function () {
                        $(this).dialog('close');
                    }
                },
                HtmlBinding: function (obj) {
                    var htmlContent = R.GetTemplateContent('/Modules/Account/Templates/ChangeAvatar.aspx', {});
                    $(obj).html(htmlContent);
                    p.DragDropUploadInit();
                }
            });
        });
        p.provide('DragDropUploadInit', function () {
            jQuery.event.props.push('dataTransfer');
            $('#CmsBm_Acount_ChangeAvatarWrapper')
            .unbind('dragenter dragover dragleave drop')
            .bind('dragenter dragover', function () {
                $('#CmsBm_Acount_ChangeAvatarWrapper').css({ 'border': '5px dashed #999' });
            }).bind('dragleave', function () {
                $('#CmsBm_Acount_ChangeAvatarWrapper').css({ 'border': '5px dashed #ddd' });
            })
            .bind('drop', function (e) {
                var _length = e.dataTransfer.files.length;
                if (_length > 0 && _length == 1) {
                    $('#CmsBm_Acount_ChangeAvatarWrapper').hide();
                    $('#CmsBm_Acount_ChangeAvatarWrapperDetail').show();
                    jQuery.each(e.dataTransfer.files, function (index, file) {
                        var fileReader = new FileReader();
                        fileReader.onload = (function (file) {
                            return function (e) {
                                $('#CmsBm_Acount_ChangeAvatarWrapperDetail').html('<img src="' + e.target.result + ' "ondragstart="return false;" style="opacity:0.5"/><span class="upload_info">Đang tải ảnh lên...<span>');
                                var name = 'avatar';
                                //var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);                            
                                name = 'avatar.png';
                                var filename = name;
                                R.Plugins.PhotoManager.MaxSelected = 1;
                                p.UploadAvatarAndReturnUrl(filename, e.target.result, function (imageUrl) {
                                    $('#CmsBm_Acount_ChangeAvatarWrapperDetail img').attr('src', '').attr('src', imageUrl + '?t=' + (new Date()).getTime()).animate({ 'opacity': 1 });
                                    $('#CmsBm_Acount_ChangeAvatarWrapperDetail .upload_info').hide();
                                });
                            };
                        })(file);
                        fileReader.readAsDataURL(file);
                    });
                } else {
                    R.MessageBox('Bạn phải upload 1 ảnh!');
                }
            });
            $('#CmsBm_Acount_ChangeAvatarWrapperDetail').unbind('dragenter dragover dragleave drop')
            .bind('dragenter dragover', function () {
                $('#CmsBm_Acount_ChangeAvatarWrapper').show();
                $('#CmsBm_Acount_ChangeAvatarWrapperDetail').hide();
            });
        });
        p.provide('DragDropUploadInitAddAccount', function () {
            jQuery.event.props.push('dataTransfer');
            $('#UserAvatarUpload')
                .unbind('dragenter dragover dragleave drop')
                .bind('dragenter dragover', function () {
                    $('#UserAvatarUpload').css({ 'border': '5px dashed #999' });
                }).bind('dragleave', function () {
                    $('#UserAvatarUpload').css({ 'border': '5px dashed #ddd' });
                }).bind('drop', function (e) {
                    var _length = e.dataTransfer.files.length;
                    if (_length > 0 && _length == 1) {
                        $('#UserAvatarUpload').hide();
                        $('#UserAvatarDetail').show();
                        jQuery.each(e.dataTransfer.files, function (index, file) {
                            var fileReader = new FileReader();
                            fileReader.onload = (function (file) {
                                return function (e) {
                                    $('#UserAvatarDetail').html('<img src="' + e.target.result + ' "ondragstart="return false;" style="opacity:0.5"/><span class="Progress">Đang tải ảnh lên...<span>');
                                    var name = 'avatar';
                                    //var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);                            
                                    name = 'avatar.png';
                                    var filename = name;
                                    R.Plugins.PhotoManager.MaxSelected = 1;
                                    p.UploadAvatarAndReturnUrl(filename, e.target.result, function (imageUrl) {
                                        $('#UserAvatarDetail img').attr('src', '').attr('src', imageUrl + '?t=' + (new Date()).getTime()).animate({ 'opacity': 1 });
                                        $('#UserAvatarDetail .Progress').hide();
                                    });
                                };
                            })(file);
                            fileReader.readAsDataURL(file);
                        });
                    } else {
                        R.MessageBox('Bạn phải upload 1 ảnh!');
                    }
                });

            $('#UserAvatarDetail').unbind('dragenter dragover dragleave drop')
            .bind('dragenter dragover', function () {
                $('#UserAvatarUpload').show();
                $('#UserAvatarDetail').hide();
            });
        });
        p.provide('ChangePassword', function () {
            R.Popup({
                title: "Đổi mật khẩu",
                width: 360,
                height: 230,
                autoScroll: false,
                skin: 'ims',
                objContainer: '#cms_bm_change_password',
                buttons: {
                    'Lưu': function () {
                        var $dialog = $(this);
                        var oldpass = $('#cms_bm_oldpass').val();
                        var newpass = $('#cms_bm_newpass').val();
                        var cfnewpass = $('#cms_bm_cfnewpass').val();
                        var isOk = true;
                        if (oldpass.length <= 0) {
                            R.MessageBox('Mật khẩu cũ không được để trống');
                            isOk = false;
                        }
                        else if (newpass.length <= 0) {
                            R.MessageBox('Mật khẩu mới không được để trống');
                            isOk = false;
                        }
                        else if (cfnewpass.length <= 0) {
                            R.MessageBox('Xác nhận lại mật khẩu mới không được để trống');
                            isOk = false;
                        } else if (newpass != cfnewpass) {
                            R.MessageBox('Xác nhận mật khẩu không chính xác!');
                            isOk = false;
                        }
                        if (isOk) {
                            var op = {
                                module: "account",
                                ashx: 'modulerequest.ashx',
                                action: "resetmypwd",
                                params: { "oldpass": oldpass, "newpass": newpass, "cfnewpass": cfnewpass },
                                success: function (res) {
                                    if (res.Success) {
                                        R.MessageBox('Đổi mật khẩu thành công!');
                                        $dialog.dialog('close');
                                    } else {
                                        R.MessageBox(res.Message);
                                    }
                                }
                            };
                            R.Post(op);
                        }
                    },
                    'Nhập lại': function () {
                        $('#cms_bm_oldpass').val('');
                        $('#cms_bm_newpass').val('');
                        $('#cms_bm_cfnewpass').val('');
                    },
                    'Hủy': function () {
                        $(this).dialog('close');
                    }
                },
                HtmlBinding: function (obj) {
                    var htmlContent = '<div class="cms_bm_changePassword_wrapper">' +
                '<table>' +
                '<tr><td><span>Mật khẩu cũ: </span></td><td><input type="password" id="cms_bm_oldpass" /></td></tr>' +
                '<tr><td><span>Mật khẩu mới: </span></td><td><input type="password" id="cms_bm_newpass" /></td></tr>' +
                '<tr><td><span>Nhập lại mật khẩu mới: </span></td><td><input type="password" id="cms_bm_cfnewpass" /></td></tr>' +
                '</table></div>';
                    $(obj).html(htmlContent);
                },
                onOpen: function (event, ui) {
                    $('#cms_bm_change_password').parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('PhotoCMS_PhotoHistoryTitleBar');
                    $('#cms_bm_change_password').parents('.ui-dialog').addClass('PhotoCMS_PhotoHistoryWrapperBorder');
                }
            });
        });

        p.provide('EditMyProfile', function () {
            var self = this;
            var op = {
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "editmyprofile",
                success: function (res) {
                    if (res.Success) {
                        var data = res.Data;
                        if (typeof (data) == 'string') {
                            data = data.replace(/(new Date\(([0-9\-]+)\))/gi, '"/Date($2)"');
                            data = JSON.parse(data);
                        }

                        //self.AddUpdateAccount(data.EncryptId);

                        R.Popup({
                            title: "Cập nhật thông tin tài khoản",
                            width: 450,
                            height: 500,
                            autoScroll: false,
                            skin: 'ims',
                            objContainer: '#VCAccountProfile',
                            buttons: {
                                "Cập nhật": function () {
                                    var $this = $(this);
                                    var dtval = getDateTimePicker($("#cms_bm_news_birthday_date").val(), false);
                                    var avatar = $('#CmsBm_Acount_ChangeAvatarWrapperDetail img').attr('src');
                                    $("#hdf_cms_bm_news_birthday_date").val(dtval);
                                    $("#hdf_cms_bm_user_avatar").val(avatar);
                                    var postForm = $("#cmsbm_frmupdate_user").serialize();
                                    R.Post({
                                        module: "account",
                                        params: postForm,
                                        postObject: false,
                                        ashx: 'modulerequest.ashx',
                                        action: "updateprofile",
                                        success: function (res1) {
                                            if (res1.Success) {
                                                $this.dialog('close');
                                                R.MessageBox('Cập nhật thông tin cá nhân thành công!');
                                            }
                                        }
                                    });
                                },
                                "Hủy": function () {
                                    $(this).dialog('close');
                                }
                            },
                            HtmlBinding: function (obj) {
                                var htmlContent = '<div id="CmsBm_Account_Profile_Wrapper"></div>';
                                $(obj).html(htmlContent);
                                R.BindData('#CmsBm_Account_Profile_Wrapper', res.Content, data);
                                $('#cms_bm_news_birthday_date').datetimepicker({
                                    dateFormat: "dd/mm/yy",
                                    showTimepicker: false,
                                    changeYear: true,
                                    changeMonth: true,
                                    maxDate: "+0d",
                                    yearRange: "-60:-15",
                                    showOptions: { direction: 'up' },
                                    showOn: "focus"
                                });
                                p.DragDropUploadInit();
                            }
                        });
                    }
                }
            };
            R.Post(op);
        });

        p.provide('EditMyProfileV2', function () {
            var self = this;
            var op = {
                module: "account",
                ashx: 'modulerequest.ashx',
                action: "editmyprofilev2",
                success: function (res) {
                    if (res.Success) {
                        var data = res.Data;
                        if (typeof (data) == 'string' && data != '') {
                            data = data.replace(/(new Date\(([0-9\-]+)\))/gi, '"/Date($2)"');
                            data = JSON.parse(data);
                        }
                        R.Popup({
                            title: "Cập nhật thông tin tài khoản",
                            width: 550,
                            height: 280,
                            skin: 'ims',
                            autoScroll: false,
                            objContainer: '#VCAccountAddUpdate',
                            buttons: {
                                "Cập nhật": function () {
                                    var $this = $(this);

                                    var userid = $("#cmsbm_txt_userid").val();
                                    var fullname = $("#txt_account_fullname").val();
                                    var username = $("#txt_account_username").val();
                                    var password = $("#txt_account_password").val();
                                    var address = $("#txt_account_address").val();
                                    var description = $("#txt_account_des").val();
                                    var mobile = $("#txt_account_mobile").val();
                                    var email = $("#txt_account_email").val();
                                    var birthday = getDateTimePicker($("#cms_bm_news_birthday_date").val(), false);
                                    var avatar = $('#UserAvatarDetail img').attr('src');
                                    var status = $("#DDLUserStatus").val();

                                    if (userid == '') {
                                        if (username.trim() == "") {
                                            R.MessageBox("Tên đăng nhập không được để trống.");
                                            return false;
                                        }
                                        if (password.trim() == "") {
                                            R.MessageBox("Mật khẩu không hợp lệ.");
                                            return false;
                                        }
                                    }
                                    var params = null;
                                    params = {
                                        'userid': userid,
                                        'fullname': fullname,
                                        //'username': username,
                                        'address': address,
                                        'description': description,
                                        'mobile': mobile,
                                        'email': email,
                                        'birthday': birthday,
                                        'avatar': avatar,
                                        //'status': status
                                    };
                                    R.Post({
                                        module: "account",
                                        params: params,
                                        ashx: 'modulerequest.ashx',
                                        action: "add_update_account",
                                        success: function (res1) {
                                            if (res1.Success) {
                                                $this.dialog('close');
                                            } else {
                                                R.MessageBox(res1.Message);
                                            }
                                        }
                                    });

                                },
                                "Hủy": function () {
                                    $(this).dialog('close');
                                }
                            },
                            HtmlBinding: function (obj) {
                                R.BindData('#VCAccountAddUpdate', res.Content, data);
                                $("#trUserNameInfo").css('display', 'none');
                                $('#cms_bm_news_birthday_date').datetimepicker({
                                    dateFormat: "dd/mm/yy",
                                    showTimepicker: false,
                                    changeYear: true,
                                    changeMonth: true,
                                    maxDate: "+0d",
                                    showOptions: { direction: 'up' },
                                    showOn: "focus"
                                });
                                $('#IMSChangeMyAvatarBtn').off('click').on('click', function () {
                                    IMS.Plugins.PhotoManager.Init({
                                        callback: function (arrImage) {
                                            $.each(arrImage, function (i, item) {
                                                $('#UserAvatarDetail img').attr('src', item.url);
                                            });
                                        }
                                    });
                                });
                                $('#cms_bm_reset_password_trigger').off('click').on('click', function () {
                                    self.ResetPassword();
                                });
                            }
                        });
                    }
                }
            };
            R.Post(op);
        });
        p.provide('UploadAvatarAndReturnUrl', function (filenameWithExtention, fileBase64OrUrl, callback) {
            var PhotoCMS = R.Plugins.PhotoManager;
            var hostUrl = PhotoCMS.PostRequestUrl;
            var url = PhotoCMS.UploadPostRequestUrl;
            var name = filenameWithExtention.substring(0, filenameWithExtention.lastIndexOf('.'));
            var ext = filenameWithExtention.substring(filenameWithExtention.lastIndexOf('.') - 1, filenameWithExtention.length);
            name = filenameWithExtention;
            var filename = name;
            var dataPost = {
                "m": "file",
                "fn": "get_policy_avatar",
                "username": current_username,
                "filename": name
            };

            if ($('.VCCopyPasteUpload').length <= 0) {
                var uploadForm = '<div style="display:none;"><form action="' + url + '" method="POST" enctype="multipart/form-data" class="VCCopyPasteUpload">' +
            '<input type="hidden" name="policy" class="VCPolicyTxt" /><input type="hidden" name="signature" class="VCSignatureTxt" />' +
            '<input type="hidden" name="source" class="VCSourceTxt" />' +
            '</form></div>';
                $('body').append(uploadForm);
            }
            $.ajax({
                url: hostUrl,
                data: dataPost,
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res != null && res.Success) {
                        var data = res.Data;
                        if (typeof (data) == "string") data = JSON.parse(data);
                        policy = data[1];
                        signature = data[2];
                        $('.VCCopyPasteUpload .VCPolicyTxt').val(policy);
                        $('.VCCopyPasteUpload .VCSignatureTxt').val(signature);
                        $('.VCCopyPasteUpload .VCSourceTxt').val(fileBase64OrUrl);
                        $('form.VCCopyPasteUpload').submit(function () {
                            $.ajax({
                                type: "POST",
                                dataType: "",
                                url: $(this).attr('action'),
                                data: $(this).serializeArray(),
                                success: function () {

                                },
                                statusCode: {
                                    200: function () {
                                        callback(cms_bm_host_avatar + data[3] + '/' + filename);
                                    }
                                },
                                done: function () {

                                },
                                error: function (x, t, m) {
                                    callback(cms_bm_host_avatar + data[3] + '/' + filename);
                                }
                            });
                            return false;
                        });
                        $('.VCCopyPasteUpload').submit().unbind('submit');
                    }
                }
            });
        });
        p.provide('GetMyAvatarUrl', function () {
            return cms_bm_host_avatar + '/Avatar/' + current_account + '/' + 'avatar.png';
        });

        p.provide('LoadZoneForGroupPermission', function (groupid, permissionid) {

            $('#group-permission-zone-' + groupid + ' .group-permission .item').removeClass('active');
            $('#group-permission-zone-' + groupid + ' .group-permission .item[permission="' + permissionid + '"]').addClass('active');
            var listZones = $('#group-permission-zone-' + groupid + ' .group-permission .item[permission="' + permissionid + '"]').attr('listzones');
            if (listZones != undefined && listZones != '' && typeof (listZones) != "undefined") {
                listZones = listZones.split(';');
            } else {
                listZones = '';
            }
            $('#group-permission-zone-' + groupid + ' .group-zone .item input').each(function () {
                $(this).attr('checked', false);
            });
            if (listZones.length > 0) {
                $.each(listZones, function (i, item) {
                    $('#group-permission-zone-' + groupid + ' .group-zone .item input[value="' + item + '"]').attr('checked', true);
                });
            }
            p.CheckIsCheckAll();
        });
        p.provide('SaveZoneToPermission', function () {
            var $listZone = $('.group-zone input[type="checkbox"]');
            $listZone.unbind('click').on('click', function () {
                var listZones = '';
                $(this).parents('.group-permission_zone').find('.group-zone .item input').each(function () {
                    var $this = $(this);
                    if ($this.is(':checked')) {
                        listZones += $this.val() + ';';
                    } else {
                        $(this).parents('.group-zone').find('.all_item input').attr('checked', false);
                    }
                });
                $(this).parents('.group-permission_zone').find('.item.active').attr('listzones', listZones);
                p.CheckPermissionChoose();
                p.CheckIsCheckAll();
            });
        });

        p.provide('CheckPermissionChoose', function () {
            $('.group-permission_zone .group-permission .item').each(function () {
                var listZones = $(this).attr('listzones');
                if (listZones != undefined && listZones != '' && typeof (listZones) != "undefined") {
                    $(this).addClass('has_this_permission');
                    var zonesName = '';
                    var listzonesId = $(this).attr('listzones').split(';')
                    $.each(listzonesId, function (i, item) {
                        if (i == 0) {
                            if (item != '') {
                                zonesName += p.GetZoneNameById(item);
                            }
                        } else {
                            if (item != '') {
                                zonesName += ', ' + p.GetZoneNameById(item);
                            }
                        }
                    });
                    $(this).attr('title', 'Có quyền ' + $(this).text().trim() + ' trong chuyên mục: ' + zonesName);
                } else {
                    $(this).removeClass('has_this_permission');
                    $(this).attr('title', '');
                }
            });
        });
        p.provide('GetZoneNameById', function (zoneId) {
            var name = '';
            $('.group-permission_zone:first').find('.group-zone input').each(function (i, item) {
                if ($(this).attr('value') == zoneId) {
                    name = $(this).next().text();
                }
            });
            return name;
        });
        p.provide('CheckAllZones', function (obj) {
            if ($(obj).is(':checked')) {
                $(obj).parents('.group-zone').find('input').attr('checked', true);
            } else {
                $(obj).parents('.group-zone').find('input').attr('checked', false);
            }
        });
        p.provide('CheckIsCheckAll', function () {
            $('.group-zone').each(function () {
                var inputlength = $(this).find('.item input').length;
                var inputcheckedLength = $(this).find('.item input:checked').length;
                if (inputlength == inputcheckedLength) {
                    $(this).find('.all_item input').attr('checked', true);
                }
                else {
                    $(this).find('.all_item input').attr('checked', false);
                }
            });
        });

        /*************************Account Wrapper*********************************/
        //Hàm lấy avatar theo username. tham số truyền vào: username. Nếu không truyền mặc định là account đang đăng nhập
        p.provide('GetUserAvatarByUserName', function (username) {
            if (username == undefined) {
                username = current_username;
            }
            if (IMSUsers == null) {
                return "";
            } else {
                for (var i = 0; i < IMSUsers.length; i++) {
                    if (IMSUsers[i].username.toLowerCase() == username.toLowerCase()) {
                        var avatar = IMSUsers[i].avatar;
                        if (avatar == '') {
                            return cms_bm_host_static + '/Images/user-thumb-small.png';
                        } else {
                            return cms_bm_host_avatar + 'zoom/40_40/' + avatar.replace(cms_bm_host_avatar, '').replace('/thumb/250_250/', '');
                        }
                        break;
                    }
                }
            }
        });
        //Hàm lấy fullname theo username. tham số truyền vào: username. Nếu không truyền mặc định là account đang đăng nhập
        p.provide('GetUserFullNameByUserName', function (username) {
            console.log(username);
            if (typeof (username) == "undefined") {
                username = current_username;
            }
            if (IMSUsers == null) {
                return "";
            } else {
                for (var i = 0; i < IMSUsers.length; i++) {
                    if ($.trim(IMSUsers[i].username.toLowerCase()) == $.trim(username.toLowerCase())) {
                        return IMSUsers[i].name != undefined ? IMSUsers[i].name : username;
                        break;
                    }
                }
            }
        });
        //Hàm lấy userinfo theo username. tham số truyền vào: username. Nếu không truyền mặc định là account đang đăng nhập
        p.provide('GetUserInfoByUserName', function (username) {
            if (username == undefined) {
                username = current_username;
            }
            if (IMSUsers == null) {
                return "";
            } else {
                for (var i = 0; i < IMSUsers.length; i++) {
                    if (IMSUsers[i].username.toLowerCase() == username.toLowerCase()) {
                        return IMSUsers[i];
                        break;
                    }
                }
            }
        });
        //Lấy toàn bộ user ra và cache vào client
        p.provide('GetAllUser', function (callback) {
            callback(IMSUsers);
        });

        p.provide('WrapPaging', function (numRow, PageIndex, PageSize, objContainer, callBack) {
            var btnNext = $(objContainer).find('.btnNext');
            var btnPrev = $(objContainer).find('.btnPrevious');
            var status = $(objContainer).find('.IMSUserPagerStatus');

            if (numRow <= 0) {
                status.html('Không có dữ liệu!');
                btnPrev.addClass("disabled");
                btnNext.addClass("disabled");
                return false;
            }

            var numPage = numRow / PageSize;
            if (PageSize >= numRow) PageIndex = 1;
            if (numPage > Math.floor(numPage))
                numPage = Math.floor(numRow / PageSize) + 1;
            if (PageIndex > numPage) PageIndex = 1;

            var IndexTo = PageIndex * PageSize;
            var IndexForm = IndexTo - PageSize + 1;
            if (IndexTo > numRow) IndexTo = numRow;

            status.html(IndexForm.toString() + ' đến ' + IndexTo.toString() + ' <span>trong tổng số</span> ' + numRow.toString());

            var _prevIndex = PageIndex <= 1 ? _prevIndex = 1 : PageIndex - 1;
            var _nexIndex = PageIndex >= numPage ? _nexIndex = numPage : parseInt(parseInt(PageIndex) + 1);

            if (PageIndex <= 1) {
                btnPrev.addClass("disabled");
                btnPrev.off("click");
            }
            else {
                btnPrev.removeClass("disabled");
                btnPrev.off("click").click(function () { callBack(_prevIndex, PageSize); });
            }

            if (PageIndex >= numPage) {
                btnNext.addClass("disabled");
                btnNext.off("click");
            }
            else {
                btnNext.removeClass("disabled");
                btnNext.off("click").click(function () { callBack(_nexIndex, PageSize); });
            }

        });

        p.release = function () {
            Account.superclass.constructor.prototype.release.call(this);
            //TODO: remove all popups, unnesscary resources 
        };

        return Account;
    });

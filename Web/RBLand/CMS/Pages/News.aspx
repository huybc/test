﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="RBLand.CMS.Pages.News" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/ftag/ftag.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/treegrid/jquery.treegrid.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="news-sldebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Tin tức</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="active n-item" status="3"><a href="javascript:void(0)"><i class="icon-magazine position-left"></i><span>Bài đã duyệt</span></a></li>
                            <li status="2" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Bài chờ duyệt</a></li>
                            <li status="6" class="n-item"><a href="javascript:void(0)"><i class="icon-file-minus2"></i>Bài bị từ chối</a></li>
                            <li status="1" class="n-item"><a href="javascript:void(0)"><i class="icon-floppy-disk"></i>Bài lưu tạm</a></li>
                            <li status="5" class="n-item"><a href="javascript:void(0)"><i class="icon-file-download position-left"></i><span>Bài bị gỡ</span></a></li>
                            <li status="4" class="n-item"><a href="javascript:void(0)"><i class="icon-bin"></i>Bài bị xóa</a></li>
                            <li><a href="javascript:void(0)"><i class="icon-certificate"></i>Nổi bật theo vùng</a></li>
                            <%--<li><a href="javascript:void(0)"><i class="icon-google"></i>Index bài lên google</a></li>--%>
                            <!-- /main -->
                            <!-- Main -->
                            <li class="navigation-header"><span>Magazine</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li status="111"><a href="javascript:void(0)"><i class="icon-newspaper"></i><span>Danh sách</span></a></li>
                            <%--<li><a href="javascript:void(0)"><i class="icon-design"></i>Tạo landing page</a></li>--%>

                            <li class="navigation-header"><span>Danh mục</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="sub-item" data-type="<%=(int)ZoneType.News %>"><a href="javascript:void(0)"><i class="icon-grid"></i><span>Danh sách</span></a></li>
                           <%-- <li class="navigation-header"><span>Games 1368</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="sub-item" data-type="<%=(int)ZoneType.Game %>"><a href="javascript:void(0)"><i class="icon-dice"></i><span>Mini ganme</span></a></li>
                            <li class="sub-item" data-type="<%=(int)ZoneType.News %>"><a href="javascript:void(0)"><i class="icon-grid"></i><span>Danh mục</span></a></li>
                            <li--% class="sub-news-game" data-type="<%=(int)ZoneType.GameNews %>"><a href="javascript:void(0)"><i class="icon-magazine position-left"></i><span>Tin game</span></a></li--%>
                            <%--<li><a href="javascript:void(0)"><i class=" icon-file-presentation"></i>Thêm danh mục</a></li>--%>
                            <!-- /main -->
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">

                <!-- Content area -->
                <div class="page-header" id="news-edit">

                    <div class="page-header-content" id="main-pnl">
                        <div class="panel-heading" id="header-page">
                            <h6 class="panel-title">

                                <button type="button" data-toggle="modal" id="btn-new-post" class="btn btn-default ibtn-xs btn-primary heading-btn _newspost"><i class="icon-quill4 position-left"></i>Viết bài mới</button>
                                <button type="button" data-toggle="modal" id="btn-new-reload" class="btn btn-default ibtn-xs heading-btn _newsreload" title="Tải lại"><i class="icon-reload-alt position-left"></i></button>
                            </h6>
                            <div class="heading-elements">
                                <div class="input-group">
                                    <input type="text" class="form-control _search-data" id="_search-data" placeholder="Tìm kiếm bài viết">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
                                    </span>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="icon-menu"></i></a>

                        </div>
                        <div class="navbar navbar-default navbar-xs navbar-component" style="margin-right: 0; margin-left: 0">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-filter" style="padding-top: 1px">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <div style="margin: 3px 3px 0 3px">
                                            <select data-placeholder="Select your state" class="select" id="zone-filer-ddl" style="width: 200px" tabindex="4">
                                                <option value="">-- Chuyên mục --</option>
                                                <asp:Repeater runat="server" ID="ZoneRpt">
                                                    <ItemTemplate>
                                                        <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="dropdown">
                                        <div id="date-filter" class="pull-right date-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%; height: 30px; border: 1px solid #ccc;">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;<span></span><b class="caret"></b>
                                        </div>
                                    </li>

                                </ul>
                                <div class="navbar-right">
                                    <div id="data-pager">
                                        <span id="rowInTotals"></span>
                                        <div class="ipagination iweb">
                                            <a href="#" class="first" data-action="first">&laquo;</a>
                                            <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                            <input type="text" readonly="readonly" />
                                            <a href="#" class="next" data-action="next">&rsaquo;</a>
                                            <a href="#" class="last" data-action="last">&raquo;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="module-content">
                            <div class="btn-group btn-action" style="display: none">
                                <button type="button" class="btn btn-default btn-sm _view" data-popup="tooltip" data-placement="top" data-original-title="Xem"><i class="icon-eye"></i></button>
                                <button type="button" class="btn btn-default btn-sm _edit" data-popup="tooltip" data-original-title="Sửa bài"><i class="icon-pencil7"></i></button>
                                <button type="button" class="btn btn-default btn-sm _delete" data-popup="tooltip" data-original-title="Xóa bài"><i class="icon-bin"></i></button>
                                <button type="button" class="btn btn-default btn-sm _unpublish" style="display: none" data-popup="tooltip" data-original-title="Hạ bài"><i class="icon-file-download"></i></button>
                                <button type="button" class="btn btn-default btn-sm _publish" style="display: none" data-popup="tooltip" data-original-title="Xuất bản"><i class="icon-file-upload"></i></button>
                                <button type="button" class="btn btn-default btn-sm _request_publish" style="display: none" data-popup="tooltip" data-original-title="Gửi y/c duyệt"><i class="icon-paperplane"></i></button>
                                <label data-popup="tooltip" data-original-title="Đã chọn">[<label id="rows"></label>]</label>
                            </div>
                            <div id="data-list">
                                <div class="datatable-scroll _list">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="page-header-content" id="sub-pnl">
                    </div>
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <!-- Modal -->
    <div class="modal right fade" id="news-form" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" id="news-edit-content">
                </div>

            </div>
        </div>
    </div>
    <div class="modal right fade" id="news-preview" tabindex="1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body" id="news-preview-content">
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
 
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js"></script>
    <script src="/CMS/Themes/js/textcount/textcounter.min.js"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js"></script>
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script src="/CMS/Themes/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/CMS/Themes/js/plugins/pickers/anytime.min.js"></script>
    <script src="/CMS/Themes/js/ftag/ftag.js"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js"></script>
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js"></script>
    <script src="/CMS/Modules/Zone/Scripts/zone.js"></script>
    <%--<script src="/CMS/Modules/News/Scripts/main.js"></script>--%>
    <%--File manager--%>
    <script src="/CMS/Themes/js/jsTree/dist/jstree.min.js"></script>
    <script src="/CMS/Modules/FileManager/fmanagers.v1.js"></script>
   
</asp:Content>

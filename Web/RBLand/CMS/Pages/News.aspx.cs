﻿using System;
using System.Linq;
using Mi.BO.Base.Zone;
using Mi.Entity.Base.Zone;

namespace RBLand.CMS.Pages
{
    public partial class News : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.News).ToList().Where(it => it.Invisibled == false);
                ZoneRpt.DataSource = zones;
                ZoneRpt.DataBind();
            }
        }
    }
}
﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Mi.Common;

namespace RBLand.Core.Helper
{
    public class EmailHelper
    {
        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="fromName">Tên người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="toName">Tên người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="encoding">Định dạng nội dung</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, Encoding encoding)
        {
            try
            {
                var client = new SmtpClient();

                var from = new MailAddress(fromEmail, fromName, encoding);
                var to = new MailAddress(toEmail, toName, encoding);

                var message = new MailMessage(from, to)
                {
                    Subject = subject,
                    SubjectEncoding = encoding,
                    Body = body,
                    BodyEncoding = encoding,
                    IsBodyHtml = true
                };

                client.Send(message);

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "SendMail Error:" + ex);
                return false;
            }
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="fromName">Tên người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="toName">Tên người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="encoding">Định dạng nội dung</param>
        /// <param name="smtpServer">SMTP server</param>
        /// <param name="smtpPort">Cổng SMTP</param>
        /// <param name="smtpUsername">Tên đăng nhập tài khoản SMTP</param>
        /// <param name="smtpPassword">Mật khẩu đăng nhập tài khoản SMTP</param>
        /// <param name="useSsl">Có sử dụng SSL hay không?</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, Encoding encoding, string smtpServer, int smtpPort, string smtpUsername, string smtpPassword, bool useSsl, params string[] attachFiles)
        {
            try
            {
                var client = new SmtpClient();
                if (!string.IsNullOrEmpty(smtpServer))
                    client.Host = smtpServer;
                if (smtpPort > 0)
                    client.Port = smtpPort;
                if (!string.IsNullOrEmpty(smtpUsername) && !string.IsNullOrEmpty(smtpPassword))
                {
                    client.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
                    client.UseDefaultCredentials = false;
                }

                client.EnableSsl = useSsl;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                var from = new MailAddress(fromEmail, fromName, encoding);
                var to = new MailAddress(toEmail, toName, encoding);

                var message = new MailMessage(from, to)
                {
                    Subject = subject,
                    SubjectEncoding = encoding,
                    Body = body,
                    BodyEncoding = encoding,
                    IsBodyHtml = true
                };

                if (attachFiles.Length > 0)
                {
                    var fileCount = attachFiles.Length;
                    for (var i = 0; i < fileCount; i++)
                    {
                        if (!string.IsNullOrEmpty(attachFiles[i]) && File.Exists(attachFiles[i]))
                        {
                            var attachment = new Attachment(attachFiles[i], MediaTypeNames.Application.Octet);
                            message.Attachments.Add(attachment);
                        }
                    }
                }

                client.Send(message);

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "SendMail Error:" + ex);
                return false;
            }
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string toEmail, string subject, string body)
        {
            return SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, body, Encoding.UTF8);
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="encoding">Định dạng nội dung</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string toEmail, string subject, string body, Encoding encoding)
        {
            return SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, body, encoding);
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="useSsl">Có sử dụng SSL hay không?</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string toEmail, string subject, string body, bool useSsl)
        {
            try
            {
                var client = new SmtpClient { EnableSsl = useSsl };

                var from = new MailAddress(fromEmail);
                var to = new MailAddress(toEmail);

                var message = new MailMessage(from, to) { Subject = subject, SubjectEncoding = Encoding.UTF8, Body = body };
                message.SubjectEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;

                client.Send(message);

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "SendMail Error:" + ex);
                return false;
            }
        }

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="encoding">Định dạng nội dung</param>
        /// <param name="useSsl">Có sử dụng SSL hay không?</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        //public static bool SendEmail(string fromEmail, string toEmail, string subject, string body, Encoding encoding, bool useSsl)
        //{
        //    try
        //    {
        //        var client = new SmtpClient { EnableSsl = useSsl };

        //        var from = new MailAddress(fromEmail);
        //        var to = new MailAddress(toEmail);

        //        var message = new MailMessage(from, to) { Subject = subject, SubjectEncoding = encoding, Body = body };
        //        message.SubjectEncoding = encoding;
        //        message.IsBodyHtml = true;

        //        client.Send(message);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, "SendMail Error:" + ex);
        //        return false;
        //    }
        //}

        /// <summary>
        /// Gửi email
        /// </summary>
        /// <param name="fromEmail">Địa chỉ người gửi</param>
        /// <param name="toEmail">Địa chỉ người nhận</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="body">Nội dung email</param>
        /// <param name="smtpServer">SMTP server</param>
        /// <param name="smtpUsername">Tên đăng nhập tài khoản SMTP</param>
        /// <param name="smtpPassword">Mật khẩu đăng nhập tài khoản SMTP</param>
        /// <param name="useSsl">Có sử dụng SSL hay không?</param>
        /// <returns>true nếu thành công, ngược lại trả về false</returns>
        public static bool SendEmail(string fromEmail, string toEmail, string subject, string body, string smtpServer, string smtpUsername, string smtpPassword, bool useSsl)
        {
            return SendEmail(fromEmail, fromEmail, toEmail, toEmail, subject, body, Encoding.UTF8, smtpServer, 25,
                             smtpUsername, smtpPassword, useSsl);
        }

        /// <summary>
        /// Hàm này chuẩn. FOX Update 16/4/2015
        /// </summary>
        /// <param name="FromName"></param>
        /// <param name="FromMail"></param>
        /// <param name="Emails"></param>
        /// <param name="Subject"></param>
        /// <param name="Msg"></param>
        /// <param name="bodyHtml"></param>
        /// <returns></returns>
        public static bool SendEmail(string FromName, string FromMail, string Emails, string Subject, string Msg, bool bodyHtml = true)
        {
            bool result = false;
            try
            {
                MailMessage mailMessage = new MailMessage();


                mailMessage.From = new MailAddress(FromMail, FromName);
                mailMessage.IsBodyHtml = bodyHtml;
                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.HeadersEncoding = Encoding.UTF8;
                mailMessage.SubjectEncoding = Encoding.UTF8;

                var ccList = Emails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                var mail = "";
                foreach (var e in ccList)
                {
                    mail = e.Trim();

                    if (mail == string.Empty)
                    {
                        continue;
                    }
                    if (i == 0)
                    {
                        mailMessage.To.Add(mail);
                    }
                    else
                    {
                        mailMessage.CC.Add(mail);
                    }
                    i++;
                }

                mailMessage.Subject = Subject;
                mailMessage.Body = Msg;

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Timeout = 1000 * 60;
                var section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
                smtpClient.Host = section.Network.Host;
                smtpClient.Port = section.Network.Port;
                smtpClient.EnableSsl = section.Network.EnableSsl;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Credentials = new NetworkCredential(section.Network.UserName, section.Network.Password);

                smtpClient.Send(mailMessage);
                result = true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                result = false;
            }
            return result;
        }


    }
}
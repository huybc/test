﻿namespace RBLand.Core.Helper
{
    public class PageInfo
    {
        public int Rows { get; set; }
        public int Pages { get; set; }
        public int rowcurrent { get; set; }
    }
}
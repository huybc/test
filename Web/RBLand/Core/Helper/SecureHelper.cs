﻿using System.Linq;

namespace RBLand.Core.Helper
{
    public static class SecureHelper
    {
        public static string GetText(this object input)
        {
            if (input == null)
            {
                return "";
            }
            
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(@"<html><body>" + input + "</body></html>");
            return doc.DocumentNode.SelectSingleNode("//body").InnerText;
        }
        public static string GetHtml(this object input)
        {
            if (input == null)
            {
                return "";
            }

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(@"<html><body>" + input + "</body></html>");
            doc.DocumentNode.Descendants()
                .Where(n => n.Name == "script" || n.Name == "style" || n.Name == "input" || n.Name == "form")
                .ToList()
                .ForEach(n => n.Remove());
            return doc.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }
    }
}
﻿using System;
using System.Web;
using System.Web.SessionState;
using Mi.Common;
using Mi.Common.ChannelConfig;

namespace RBLand.Core.InitClientConstants
{
    /// <summary>
    /// Lưu trữ các biến js khởi tạo, có liên quan dữ liệu/ cấu hình
    /// </summary>
    public class Constants : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsContent = "";
            try
            {
                jsContent += string.Format("var StorageUploadUrl = '{0}';\n", CmsChannelConfiguration.GetAppSetting("StorageUploadUrl"));
                jsContent += string.Format("var StorageUrl = '{0}';\n", CmsChannelConfiguration.GetAppSetting("StorageUrl"));
                jsContent += string.Format("var session_request_prefix = '{0}_';\n", context.Session.SessionID);
                jsContent += string.Format("var RHostStatics = '{0}';\n", CmsChannelConfiguration.GetAppSetting("HostStatics"));
                jsContent += string.Format("var RVersion = '{0}';\n", CmsChannelConfiguration.GetAppSetting("Version"));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            context.Response.ContentType = "text/javascript";
            context.Response.Write(jsContent);
            context.Response.Flush();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
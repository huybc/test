﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.SessionState;

using Mi.Action;
using Mi.Action.Core;
using Mi.Common;
using Mi.Common.ChannelConfig;
using RBLand.Core.Helper;

namespace RBLand.Core.RequestBase
{
    public class CmsAsyncRequestBase : IHttpAsyncHandler, IRequiresSessionState
    {
        private static readonly Assembly CurrentAssembly = Assembly.GetExecutingAssembly();

        private static readonly List<CmsChannelConfiguration.ModuleExtension> AllModuleExtensions =
            CmsChannelConfiguration.GetAllModuleExtension();

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.AddHeader("Accept-Encoding", "gzip, deflate");
            var actionName = !string.IsNullOrEmpty(context.Request.Form["m"])
                                 ? context.Request.Form["m"].Trim().ToLower()
                                 : "file";
            if (string.IsNullOrEmpty(actionName))
            {
                context.Response.Status = "404 not found";
                context.Server.Transfer("/404.aspx");
                context.Response.End();
            }
            else
            {
                var action = GetAction(actionName);
                if (action != null)
                {
                    action.Do(context);
                }
                else
                {
                    if (CmsChannelConfiguration.GetAppSettingInInt32(Constants.CMS_SERVER_DEBUG_MODE) == 1)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Action: " + actionName + " chưa được đăng ký!");
                        var msg = TemplateUtils.BuildServerErrorWithFormat("Action: " + actionName, "Chưa được đăng ký!");
                        context.Response.Write(NewtonJson.Serialize(new ResponseData { Content = msg, Success = true }));
                        context.Response.End();
                    }
                }
            }

            var asynch = new CmsAsynchOperation(cb, context, extraData);
            asynch.StartAsyncWork();
            return asynch;
        }

        private class ActionInfo
        {
            public string Assembly { get; set; }
            public string ModuleBasePath { get; set; }
        }
        // Fields
        private static IDictionary<string, ActionInfo> Dicts = new Dictionary<string, ActionInfo>();

        protected static bool RegisterAction(string className, string actionName, Type actionType, bool isModuleAction = false)
        {
            try
            {
                if (actionType.BaseType != typeof(ActionBase)) return false;
                var assembly = actionType.Namespace + "." + actionType.Name;
                var moduleBasePath = "\\Modules\\";
                if (isModuleAction)
                {
                    var moduleExtension =
                        AllModuleExtensions.Find(
                            item =>
                                actionType.Namespace.IndexOf("." + item.Source + ".", StringComparison.CurrentCulture) > 0);

                    if (moduleExtension != null)
                    {
                        //assembly = assembly.Replace(".Modules." + moduleExtension.Source,
                        //    ".ModuleExtensions." + CmsChannelConfiguration.CurrentChannelNamespace + "." + moduleExtension.Target);
                        assembly = assembly.Replace(".Modules.",
                            ".ModuleExtensions." + CmsChannelConfiguration.CurrentChannelNamespace + ".");
                        assembly = assembly.Replace("." + moduleExtension.Source, "." + moduleExtension.Target);
                        moduleBasePath = "\\ModuleExtensions\\" + CmsChannelConfiguration.CurrentChannelNamespace + "\\";
                    }
                }
                var actionInfo = new ActionInfo
                {
                    Assembly = assembly,
                    ModuleBasePath = moduleBasePath
                };
                if (Dicts == null) Dicts = new Dictionary<string, ActionInfo>();
                if (Dicts.ContainsKey(className + "." + actionName))
                {
                    Dicts[className + "." + actionName] = actionInfo;
                }
                else
                {
                    Dicts.Add(className + "." + actionName, actionInfo);
                }
                Dicts[className + "." + actionName] = actionInfo;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("ClassName={0}, ActionName={1} => {2}", className, actionName, ex.ToString()));
                //throw ex;
            }
            return true;
        }

        protected ActionBase GetAction(string actionName)
        {
            ActionBase action = null;
            ActionInfo actionInfo;
            if (Dicts.TryGetValue(this.GetType().Name + "." + actionName, out actionInfo))
            {
                var assembly = actionInfo.Assembly;
                action = (ActionBase) CurrentAssembly.CreateInstance(assembly, false,
                    BindingFlags.CreateInstance, null,
                    null,
                    CultureInfo.CurrentCulture,
                    null);
                if (action == null)
                {
                    action = (ActionBase) UIHelper.CurrentActionAssembly.CreateInstance(assembly, false,
                        BindingFlags
                            .CreateInstance,
                        null,
                        null,
                        CultureInfo
                            .CurrentCulture,
                        null);

                }
                if (action != null) action.ModuleBasePath = actionInfo.ModuleBasePath;
            }
            return action;
        }

        public void EndProcessRequest(IAsyncResult result)
        {
        }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
        }
    }
}
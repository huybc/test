﻿var R = {
    requestTimeout: 60000,
    Post: function (op) {
        var dataPost = {
            "m": op.module,
            "fn": op.action,
            _url: window.location.href.replace('http://' + window.location.host, ''),
            _title: document.title
        };
        var isShowTimeoutMessage = op.isShowTimeoutMessage ? op.isShowTimeoutMessage : false;
        var isPostObject = op.postObject != undefined ? op.postObject : true;
        if (isPostObject) {
            if (op.params != undefined && typeof (op.params) === 'object')
                dataPost = $.extend(dataPost, op.params);
        } else {
            if (op.params != undefined)
                dataPost = $.param(dataPost) + "&" + op.params;
        }
        var url = op.url != undefined && op.url != '' ? op.url : '/modulerequest.api';
        if (!url.startsWith('/')) url = '/' + url;
        if (url.toLowerCase() == '/modulerequest.api') url = session_request_prefix + '/dopost/v1.2/' + (new Date()).getTime() + '/ModuleRequest.api';
        var keyAbort = $.ajax({
            type: 'POST',
            url: '/' + url,
            data: dataPost,
            dataType: "json",
            success: function (res) {
                if (res.ErrorCode == -100) {
                    R.CheckSession(res, function () {
                        R.Post(op);
                    });
                } else {
                    op.success(res);
                }
            },
            error: function (x, t, m) {
                if (op.error != undefined && typeof (op.error) == 'function') {
                    op.error(x);
                } else if (t === "timeout") {
                    if (isShowTimeoutMessage) {
                        R.Confirm("Thời gian xử lý quá lâu, bạn có muốn thử lại không?", function () {
                            R.Post(op);
                        });
                    }
                }
            },
            timeout: op.timeout != undefined ? op.timeout : R.requestTimeout
        });



        return keyAbort;
    },
    CheckSession: function (res, callback) {
        if (res.ErrorCode == -100) {
            R.Post({
                module: "account",
                params: {

                },
                ashx: 'modulerequest.api',
                action: "init_login_popup",
                success: function (res) {
                    if (res.Success) {
                        R.Popup({
                            title: 'Đăng nhập',
                            width: 450,
                            height: 260,
                            skin: 'ims',
                            objContainer: '#LoginOnPopupPopup',
                            buttons: {
                                "Đăng nhập": function () {
                                    var email = $('#LoginOnPopup #EmailTxt').val();
                                    var pass = $('#LoginOnPopup #PwdTxt').val();
                                    R.Post({
                                        module: "account",
                                        params: {
                                            email: email,
                                            pass: pass
                                        },
                                        ashx: 'modulerequest.ashx',
                                        action: "login",
                                        success: function (res1) {
                                            if (res1.Success) {
                                                //R.MessageBox('<div id="RRegisterSuccessDiv">Đăng nhập thành công!</div>');
                                                callback();
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 500);
                                            } else {
                                                R.MessageBox('Thông tin đăng nhập không chính xác!');
                                            }
                                        }
                                    });
                                    $(this).dialog('close');
                                },
                                'Đóng': function () {
                                    $(this).dialog('close');
                                }
                            },
                            HtmlBinding: function (obj) {
                                $(obj).html('<div id="LoginOnPopup" class="RForm">' + res.Content + '</div>');
                            }
                        });
                    }
                }
            });
        }
    },
    Confirm: function (msg, callBack, callBackOnClosing, close, opts) {
        if (typeof (opts) == "undefined") {
            opts = { skin: 'ims' };
        }
        if (typeof (close) == "undefined") var close = true;
        var elementBox = "#IMSConfirmContent";
        if ($(elementBox).length <= 0) {
            $("body").append('<div id="' + elementBox.substr(1) + '" style="display:none;" title="Xác nhận"></div>');
        } else
            $(elementBox).removeAttr("title").attr("title", "Xác nhận");
        $(elementBox).dialog({
            modal: true,
            zIndex: 9999,
            resizable: false,
            stack: false,
            buttons: {
                'Đồng ý': function () {
                    if (close) $(this).dialog('close');
                    if (typeof (callBack) != "undefined") callBack();
                },
                'Bỏ qua': function () {
                    $(this).dialog('close');
                    if (typeof (callBackOnClosing) != "undefined") callBackOnClosing();
                }
            },
            close: function () {
                // if (typeof (callBackOnClosing) != "undefined") callBackOnClosing();
                $(elementBox).empty();
                $(this).dialog("destroy");
            },
            open: function () {
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeader');
                $(elementBox).parents('.ui-dialog').addClass('RPopupBorder');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeaderNoBg');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-buttonpane .ui-button').addClass('RButton RMedium');
            }
        });
        $(elementBox).dialog('open');
        $(elementBox).html(msg);
    },
    Popup: function (op) {
        //objContainer, width, height, title, buttons, autoClose
        if (op.title == undefined) op.title = "";
        if (op.width == undefined) op.width = 500;
        if (op.stack == undefined) op.stack = false;
        if (op.zIndex == undefined) op.zIndex = 9999;
        if (op.height == undefined) op.height = 300;
        if (op.autoClose == undefined) op.autoClose = true;
        if (op.modal == undefined) op.modal = true;
        if (op.draggable == undefined) op.draggable = true;
        if (op.closeOnEscape == undefined) op.closeOnEscape = true;
        if (op.dialogClass == undefined) op.dialogClass = "";
        if (op.onOpen == undefined) op.onOpen = function (e, ui) { };
        if (op.objContainer == undefined || op.objContainer == "") op.objContainer = "#cms_bm_block_messagebox";
        if (op.skin == undefined || op.skin == "") op.skin = "default";
        if ($(op.objContainer).length <= 0) {
            $("body").append('<div id="' + op.objContainer.substr(1) + '" style="display:none; overflow-x:hidden; overflow-y:auto;" title="' + op.title + '"></div>');
        } else {
            $(op.objContainer).css({ 'display': 'none', 'overflow-x': 'hidden', 'overflow-y': 'auto' });
            $(op.objContainer).attr("title", op.title);
        }

        if (op.buttons == undefined) op.buttons = { "Đóng": function () { $(this).dialog("close"); } };
        $(op.objContainer).dialog({
            zIndex: op.zIndex,
            width: op.width,
            height: op.height,
            resizable: false,
            dialogClass: op.dialogClass,
            modal: op.modal,
            closeOnEscape: op.closeOnEscape,
            draggable: op.draggable,
            buttons: op.buttons,
            stack: op.stack,
            beforeclose: function () {
                if (!op.autoClose) {
                    var mesgBox = this;
                    R.Confirm("Bạn có chắc chắn đóng cửa sổ không?", function () {
                        $(mesgBox).dialog("close");
                    });
                    return false;
                } else {
                    return true;
                }
            },
            close: function () {
                if (op.cb != undefined) {
                    op.cb();
                }
                $(this).dialog();
                $(this).empty();
                $(this).dialog("destroy");
            },
            open: function () {
                $(op.objContainer).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeader');
                $(op.objContainer).parents('.ui-dialog').addClass('RPopupBorder');
                $(op.objContainer).parents('.ui-dialog').find('.ui-dialog-buttonpane .ui-button').addClass('RButton RMedium');
                $(op.objContainer).parents('.ui-dialog').find('.ui-dialog-buttonset button').addClass('RButton RMedium');
                op.onOpen();
            }
        });
        $(op.objContainer).dialog('open');
        if (op.noHeader != undefined && op.noHeader)
            $(op.objContainer).parent().find(".ui-dialog-titlebar").remove();
        if (op.noFooter != undefined && op.noFooter)
            $(op.objContainer).parent().find(".ui-dialog-buttonpane").remove();
        if (op.wating != undefined) {
            $(op.objContainer).html('<img src="' + RHostStatics + '/images/waiting.gif" style="vertical-align:middle; border:0" />');
        }
        if (op.HtmlBinding != undefined && typeof (op.HtmlBinding) == 'function') {
            op.HtmlBinding(op.objContainer);
        }
        if (op.autoScroll == undefined) op.autoScroll = true;
        if (op.autoScroll) {
            $(op.objContainer).slimScroll({
                height: $(op.objContainer).height(),
                width: $(op.objContainer).width()
            });
        }
        if (op.closeOutWay != undefined && op.closeOutWay) {
            $(".ui-widget-overlay").on("click", function () {
                $(op.objContainer).dialog("close");
            });
        }
    },
    MessageBox: function (msg, title, cb) {
        if (typeof (title) == "undefined") var title = "Thông báo";
        var elementBox = "#cms_bm_block_messagebox";
        if ($(elementBox).length <= 0) {
            $("body").append('<div id="' + elementBox.substr(1) + '" style="display:none;" title="' + title + '"></div>');
        }
        else {
            $(elementBox).removeAttr("title").attr("title", title);
        }
        $(elementBox).dialog({
            modal: true,
            resizable: false,
            stack: false,
            zIndex: 99999999,
            buttons: {
                'Đóng': function () {
                    $(this).dialog('close');
                    if (cb != undefined && typeof cb == 'function')
                        cb();
                }
            },
            close: function () {
                $(elementBox).empty();
                $(this).dialog("destroy");
                if (cb != undefined && typeof cb == 'function')
                    cb();
            },
            open: function () {
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeader');
                $(elementBox).parents('.ui-dialog').addClass('RPopupBorder');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeaderNoBg');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-buttonpane .ui-button').addClass('RButton RMedium');
            }
        });
        $(elementBox).dialog('open');
        $(elementBox).html(msg);
    },
    MessageTextError: function () {
        var str = '<div class="alert alert-warning no-border">' +
            '<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><span class="text-semibold">Thông báo:</span> Đã xảy ra sự cố. Vui lòng liên hệ với quản trị viên</div>';
        return str;
    },
    MessageBoxMap: function (msg, title, cb) {
        if (typeof (title) == "undefined") var title = "Thông báo";
        var elementBox = "#cms_bm_block_messagebox";
        if ($(elementBox).length <= 0) {
            $("body").append('<div id="' + elementBox.substr(1) + '" style="display:none;" title="' + title + '"></div>');
        }
        else {
            $(elementBox).removeAttr("title").attr("title", title);
        }
        $(elementBox).dialog({
            modal: true,
            resizable: false,
            stack: false,
            zIndex: 99999999,
            buttons: {
                'Tải lại': function () {
                    //$(this).dialog('refresh');
                    document.location.reload(true);
                },
                'Chọn điểm': function () {
                    $(this).dialog('close');
                    //if (cb != undefined && typeof cb == 'function')
                    //    cb();
                }
            },
            close: function () {
                $(elementBox).empty();
                $(this).dialog("destroy");
                if (cb != undefined && typeof cb == 'function')
                    cb();
            },
            open: function () {
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeader');
                $(elementBox).parents('.ui-dialog').addClass('RPopupBorder');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeaderNoBg');
                $(elementBox).parents('.ui-dialog').find('.ui-dialog-buttonpane .ui-button').addClass('RButton RMedium');
            }
        });
        $(elementBox).dialog('open');
        $(elementBox).html(msg);
    },
    BindData: function (elm, content, data, usingJTemplate) {
        if (typeof (elm) == 'string') elm = $(elm);
        if (usingJTemplate == undefined) usingJTemplate = true;
        if (usingJTemplate) {
            elm.setTemplate(content);

            if (typeof (data) == "string")
                data = data != "" ? JSON.parse(data) : null;
            elm.processTemplate(data);
        } else {
            elm.html(content);
        }
        $('.tipsy_handler').tipsy({ live: true, gravity: $.fn.tipsy.autoNS });
        $(".btn_restoreversion").tipsy({ live: true, gravity: "e" });
    },
    SetDialogNotify: function (msg, type) {
        $.confirm({
            title: 'Thông báo !',
            type: type == 'error' ? 'red' : '',
            content: msg,
            animation: 'scaleY',
            closeAnimation: 'scaleY',
            boxWidth: '250px',
            useBootstrap: false,
            buttons: {
                cancel: {
                    text: 'Đóng',
                    btnClass: 're-btn re-btn-default'
                }
            }
        });
    },
    FormatDate: function (date) {
        if (date.length > 0) {
            return date.split("/").reverse().join("-");
        }
        return '';
    },
    FormatDatetime: function (date) {
        if (date.length > 0) {
            return date.substr(0, 10).split('/').reverse().join('-') + " " + date.substr(11, 5);
        }
        return '';
        //dd/mm/yyyy 16:50:05


    },
    CMSMapZone:function (el) {
        //map zone
        var $zoneItem = $(el).find('._zone');
        if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
            for (var j = 0; j < $zoneItem.length; j++) { // lặp các row

                var strHtml = '';
                var arrayId = $zoneItem.eq(j).attr('data-id').split(',');
                for (var k = 0; k < arrayId.length; k++) {
                    $(RAllZones).each(function (i, item) {
                        if (arrayId[k] == item.Id) {
                            if (strHtml.length == 0) {
                                strHtml = item.Name;
                            } else {
                                strHtml += ';' + item.Name;
                            }
                        }
                        return;
                    });
                }
                $zoneItem.eq(j).html(strHtml);
            }
        }
    }
};

R.Waiting = function (msg, objContainerBox, title, isClose) {
    if (typeof (objContainerBox) == "undefined" || objContainerBox == "") var objContainerBox = "#cms_bm_block_messagebox";
    if (typeof (msg) == "undefined" || msg == "") var msg = "Hệ thống đang xử lý dữ liệu, xin đợi chút xíu...";
    if (typeof (title) == "undefined" || title == "") var title = "Thông báo";
    if (typeof (isClose) == "undefined") var isClose = false;
    if ($(objContainerBox).length <= 0) {
        $("body").append('<div id="' + objContainerBox.replace('#', '') + '" style="display:none;" title="' + title + '"></div>');
    }

    var buttons = {};
    if (isClose) {
        buttons = {
            'Đóng': function () {
                $(objContainerBox).dialog('close');
                //if ($('#WaitingSaveSolution').length > 0) {
                //    $('#WaitingSaveSolution').dialog('close');

                //}
                return true;
            }
        };
    }
    $(objContainerBox).dialog({
        modal: true,
        resizable: false,
        buttons: buttons,
        zIndex: 99999999,
        beforeclose: function () {
            return isClose;
        },
        close: function () {
            $(objContainerBox).empty();
            //$(this).dialog("close");            
            $(this).dialog().dialog("destroy");
        }, open: function () {
            $(objContainerBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeader');
            $(objContainerBox).parents('.ui-dialog').find('.ui-dialog-titlebar-close').addClass('RWaitingClose');
            $(objContainerBox).parents('.ui-dialog').addClass('RPopupBorder');
            $(objContainerBox).parents('.ui-dialog').find('.ui-dialog-titlebar').addClass('RPopupHeaderNoBg');
            $(objContainerBox).parents('.ui-dialog').find('.ui-dialog-buttonpane .ui-button').addClass('RButton RMedium');
        }
    });
    $(objContainerBox).dialog('open');
    $(objContainerBox).html('<center><img src="' + RHostStatics + '/images/loading.gif" /><br /><br />' + msg + '</center>');
};

R.TimeAgo = function (elm) {
    $(elm).timeago();
    $(elm).each(function () {
        if ($.trim($(this).text()) == '') {
            $(this).text($(this).attr('title').replace(':nottimeago', ''));
        }
    });
};

R.ValidateEmail = function (email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
R.GetURLParameter = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

R.ScrollAutoSize = function (elm, funcToGetHeight, funcToGetWidth, otherOptions, scrollBy, scrollTo, isCroll) {
    if (elm.length == 0) return;
    if (typeof (funcToGetWidth) == "undefined") {
        funcToGetWidth = function () {

        }
    }
    if (typeof (elm) == 'string') {
        elm = $(elm);
    }
    if (otherOptions == undefined) {
        otherOptions = {};
    }
    var height = funcToGetHeight();
    var width = funcToGetWidth();
    var options = {
        railVisible: false,
        alwaysVisible: false,
        railOpacity: 0.1,
        opacity: 0.2,
        height: height,
        width: width
    };
    $.extend(options, otherOptions);
    var firstOptions = options;
    if (typeof (scrollBy) != "undefined") {
        $.extend(firstOptions, { scrollBy: scrollBy });
    }
    if (typeof (scrollTo) != "undefined") {
        $.extend(firstOptions, { scroll: scrollTo });
    }
    if (typeof (isCroll) != 'undefined') {
        elm.slimScroll(firstOptions)
    }
    // ;
    var ScrollTimeOut;
    $(window).resize(function () {
        //clearTimeout(ScrollTimeOut);
        //ScrollTimeOut = setTimeout(function () {

        //}, 2000);
        if (elm.length == 0) return;
        if (elm.parent('.slimScrollDiv').length > 0) {
            if (elm.length == 0) return;
            $.extend(options, {
                height: funcToGetHeight(),
                width: funcToGetWidth()
            });
            elm.parent().replaceWith(elm);
            elm.removeAttr('style');
            if (typeof (isCroll) != 'undefined') {
                elm.slimScroll(options)
            }
            //   elm.slimScroll(options);
        }
    });
};
R.ShowOverlay = function (htmlContent, cb, closeCb, rightMargin) {

    if (typeof (rightMargin) == "undefined") {
        rightMargin = 0;
    } if (typeof (closeCb) == "undefined") {
        closeCb = function () {

        }
    }

    function ResizeOverlay(isWindowsResize) {
        if (!isWindowsResize) {
            $('#IMSOverlayWrapper').show();
            $('#IMSOverlayContent').css({
                width: 0,
                'margin-right': rightMargin
            });
        } else {
            $('#IMSOverlayContent').css({
                width: 'auto'
            });
            $('#IMSOverlayContent').css({
                'margin-right': 0
            });
        }
        var ww = $(window).width();
        var wh = $(window).height();

        $('#IMSOverlayWrapper').css({
            width: ww,
            height: wh,
            'z-index': 99,
            top: 0,
            left: 0,
            background: 'transparent',
            position: 'fixed'
        });

        $('#IMSOverlayContent').css({ height: wh, 'overflow-y': 'auto', 'overflow-x': 'hidden' });
        $('#IMSOverlayClose').off('click').on('click', function () {
            if (typeof (R) != "undefined" && typeof (R.CloseOverlay) != "undefined") {
                R.CloseOverlay(closeCb);
            } else {
                if (closeCb != 'undefined' && closeCb != null && typeof closeCb == 'function')
                    closeCb();
                $('#IMSOverlayContent').animate({ width: 0 }, 500, function () {
                    $(this).empty();
                    $('#IMSOverlayWrapper').hide().removeAttr('style');
                    $('#IMSOverlayContent').hide().removeAttr('style');
                });
            }
        });
        if (!isWindowsResize) {
            if (typeof (htmlContent) != "undefined") {
                $('#IMSOverlayContent').html(htmlContent);
                var contentWidth = 0;
                $('#IMSOverlayContent').show().children().each(function () {
                    contentWidth = contentWidth + $(this).outerWidth(true);
                });
                $('#IMSOverlayContent').animate({ width: contentWidth }, 500, function () {
                    if (cb != undefined && cb != null && typeof cb == 'function') {
                        cb();
                    }
                });

            } else {
                if (cb != undefined && cb != null && typeof cb == 'function') {
                    cb();
                }
            }
        }
    }
    $(window).off('keyup').on('keyup', function (event) {
        if (event.keyCode == 27) {
            R.CloseOverlay(closeCb);
        }
    });
    ResizeOverlay(false);
    $(window).resize(function () {
        ResizeOverlay(true);
    });

    return $('#IMSOverlayWrapper');
};
R.CloseOverlay = function (cb) {
    if (cb != 'undefined' && cb != null && typeof cb == 'function')
        cb();
    $('#IMSOverlayContent').animate({ width: 0 }, 500, function () {
        $(this).empty();
        $('#IMSOverlayWrapper').hide().removeAttr('style');
        $('#IMSOverlayContent').hide().removeAttr('style');
    });
};

R.ShowOverlayFull = function (uOpts) {
    R.CloseOverlayFull();
    var opts = {
        content: 'Popup chưa có nội dung',
        width: $(window).width(),
        height: function () {
            return $(window).height();
        },
        onOpen: function (obj) { },
        onClose: function () {

        },
        title: ''
    }
    $.extend(true, opts, uOpts);
    var $temp = $('<div id="IMSFullOverlayWrapper" class="IMSFullOverlayWrapper"><div id="IMSFullOverlayInner"><div id="IMSFullOverlayMainContent" class="IMSFullOverlayMainContent"><div id="IMSFullOverlayMainContentHtml"></div></div></div></div>');
    var outerH = ((typeof (opts.height) == 'function') ? opts.height() : opts.height);
    var modalH = outerH //- 20 - 30;// - (padding + header)
    $('#IMSFullOverlayMainContent', $temp).css({
        // width: opts.width,
        height: modalH
    });
    //$('#IMSFullOverlayTitle', $temp).css({
    //    width: opts.width + 20
    //});
    $('#IMSFullOverlayMainContentHtml', $temp).html(opts.content);
    $('#IMSFullOverlayClose', $temp).off('click').on('click', function () {
        $(this).parents('#IMSFullOverlayWrapper').fadeIn('slow', function () {
            opts.onClose();
            $('#IMSFullOverlayWrapper').empty();
            $('#IMSFullOverlayWrapper').remove();
        });
    });

    $('body').append($temp);

    // $('#IMSFullOverlayMainContent').show('');
    $('#IMSFullOverlayInner').css({
        'margin-top': (($(window).height() - outerH) / 2) + 'px'
    });
    $('#IMSFullOverlayWrapper').addClass('fadeIn');
    opts.onOpen('#IMSFullOverlayMainContent');

};

R.CloseOverlayFull = function (cb) {
    $('#IMSFullOverlayWrapper').fadeIn(500, function () {
        $(this).empty();
        $(this).remove();
        if (cb != 'undefined' && cb != null && typeof cb == 'function')
            cb();
    });
};
R.PNotify = function (type, msg) {

    switch (type) {
        case 'warn':
            new PNotify({
                title: 'Thông báo',
                text: msg,
                addclass: 'bg-warning'
            });
            break;
        case 'error':
            new PNotify({
                title: 'Thông báo',
                text: msg,
                addclass: 'bg-error'
            });
            break;

        case 'info':
            new PNotify({
                title: 'Thông báo',
                text: msg,
                addclass: 'bg-info'
            });
            break;

        case 'success':
            new PNotify({
                title: 'Thông báo',
                text: msg,
                addclass: 'bg-success'
            });
            break;
    }
}
R.NotifytPopup = function (type, msg) {
    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

    switch (type) {
        case 'error':
            swalInit({
                title: 'Oops...',
                text: msg,
                type: 'error',
                allowEscapeKey: false,
                allowEnterKey: false
            });
            break;

        case 'info':

            break;

        case 'success':
            swalInit({
                title: 'Thông báo',
                text: msg,
                type: 'success',
                showCloseButton: true
            });

            break;
    }

}

R.NumFormat = function () {
    $('.price-format').each(function (i, v) {
        var point = $(v).attr('data');
        if (typeof (point) !== 'undefined') {
            $(v).attr('title', point.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
            $(v).text(point.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")).removeClass('price-format');
            //Edit core
            $(v).val(point.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")).removeClass('price-format');
            //End edit
        }
    });
},

    R.UnicodeToSeo = function (str) {
        if (str === undefined) {
            return;
        }
        if (str.length === 0) {
            return;
        }
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
        /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
        str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-  
        str = str.replace(/^\-+|\-+$/g, "");

        str = str.replace(/\//g, '-')
        //  str = str.replace(/\--/g, '-')
        //cắt bỏ ký tự - ở đầu và cuối chuỗi 
        return str;
    };



// Loadding 
(function ($) {
    $.fn.RLoading = function () {
        this.append('<div class=\"module-loading\"><div class=\"module-lock\" style=\"z-index: 99;width:100%;height:100%;position:absolute;opacity:0.8;top:0;\"><div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">Đ</div><div id="fountainTextG_2" class="fountainTextG">a</div><div id="fountainTextG_3" class="fountainTextG">n</div><div id="fountainTextG_4" class="fountainTextG">g</div><div id="fountainTextG_5" class="fountainTextG"> </div><div id="fountainTextG_6" class="fountainTextG">t</div><div id="fountainTextG_7" class="fountainTextG">ả</div><div id="fountainTextG_8" class="fountainTextG">i</div><div id="fountainTextG_9" class="fountainTextG"> </div><div id="fountainTextG_10" class="fountainTextG">d</div><div id="fountainTextG_11" class="fountainTextG">ữ</div><div id="fountainTextG_12" class="fountainTextG"> </div><div id="fountainTextG_13" class="fountainTextG">l</div><div id="fountainTextG_14" class="fountainTextG">i</div><div id="fountainTextG_15" class="fountainTextG">ệ</div><div id="fountainTextG_16" class="fountainTextG">u</div><div id="fountainTextG_17" class="fountainTextG">.</div><div id="fountainTextG_18" class="fountainTextG">.</div><div id="fountainTextG_19" class="fountainTextG">.</div></div></div></div></div>');
        return this;
    };
    //$.fn.RLoading = function () {
    //    var h = this.height();
    //    var w = this.width();
    //    this.css('position', 'relative');
    //    this.append('<div class=\"module-loading\"><div class=\"module-lock\" style=\"z-index: 999999;width:' + w + 'px;height:' + h + 'px;position:absolute;opacity:0.4;top:0;\"><div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">Đ</div><div id="fountainTextG_2" class="fountainTextG">a</div><div id="fountainTextG_3" class="fountainTextG">n</div><div id="fountainTextG_4" class="fountainTextG">g</div><div id="fountainTextG_5" class="fountainTextG"> </div><div id="fountainTextG_6" class="fountainTextG">t</div><div id="fountainTextG_7" class="fountainTextG">ả</div><div id="fountainTextG_8" class="fountainTextG">i</div><div id="fountainTextG_9" class="fountainTextG"> </div><div id="fountainTextG_10" class="fountainTextG">d</div><div id="fountainTextG_11" class="fountainTextG">ữ</div><div id="fountainTextG_12" class="fountainTextG"> </div><div id="fountainTextG_13" class="fountainTextG">l</div><div id="fountainTextG_14" class="fountainTextG">i</div><div id="fountainTextG_15" class="fountainTextG">ệ</div><div id="fountainTextG_16" class="fountainTextG">u</div><div id="fountainTextG_17" class="fountainTextG">.</div><div id="fountainTextG_18" class="fountainTextG">.</div><div id="fountainTextG_19" class="fountainTextG">.</div></div></div></div></div>');
    //    return this;
    //};

    $.fn.RLoadingComplete = function () {
        this.find('.module-loading').hide().remove();
    };

}(jQuery));

// Loadding 
(function ($) {

    $.fn.RLoadingModule = function () {

        //var h = $(this.selector).height();
        //var w = $(this.selector).width();
        var h = this.height();
        var w = this.width();

        this.css('position', 'relative');
        this.append('<div class=\"_module-loading\"><div class=\"_module-lock\" style=\"z-index: 999999999;width:' + w + 'px;height:' + h + 'px;background:#fff;position:absolute;opacity:0.2;top:0;\"><img style="top:50%;right:50%;position:absolute" src="data:image/gif;base64,R0lGODlhJgAmAPcAAAAAAAEBAQICAgMDAwQEBAcHBwgICAgICAkJCQoKCgoKCgsLCwsLCwsLCwwMDA0NDQ0NDQ4ODg8PDxAQEBAQEBISEhMTExQUFBYWFhgYGBoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUVFRUZGRkdHR0hISElJSUtLS0xMTE1NTU5OTk9PT1FRUVNTU1RUVFZWVlhYWFlZWVpaWltbW1xcXF1dXV5eXl9fX19fX2BgYGBgYGFhYWFhYWFhYWJiYmNjY2NjY2RkZGVlZWZmZmhoaGlpaWtra21tbW5ubnBwcHFxcXNzc3V1dXd3d3l5eXt7e3x8fHx8fHx8fH19fX19fX19fX19fX19fX19fX19fX5+fn5+fn5+fn5+fn5+fn5+fn5+fn5+fn9/f39/f4CAgIODg4WFhYeHh4mJiYuLi4yMjI2NjY+Pj5CQkJKSkpOTk5WVlZeXl5iYmJmZmZqampubm5ycnJ2dnZ2dnZ2dnZ6enp6enp6enp6enp+fn5+fn6CgoKCgoKGhoaGhoaKioqOjo6Ojo6SkpKampqenp6ioqKqqqqurq62tra+vr7GxsbS0tLW1tba2tre3t7i4uLq6ur29vcDAwMLCwsTExMbGxsjIyMnJycvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NjY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OLi4uTk5OXl5ebm5ujo6Onp6erq6uvr6+3t7e/v7/Dw8PHx8fLy8vX19ff39/n5+fv7+/z8/P39/f39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQJBAD/ACwAAAAAJgAmAAAI/gD/CRxIsKDBgwgTKlzIsOFAdOYcSiwo7g0LFGbATZyIBoVHFFo2Osz24qNHZyINjitHEJxJj9IGjmtFiZnEikaMNDo3EIrJIQOJmWDAQMIjh29y5qw0sNoSj0OUDZRBlKgEYgzFKc3JhSA6ZcrQDexWterRhdqYbFWz0JqFsgwkNZy01RVDKWU3aGt4rhIXM7AadsPLwISulBK1IUuHuLHjxwKNNUqUi11je+d4HjQmprOYWYjtXVu2bK/BSp7FyBErkhxp0poJovYMJ2Lr18tiDwyWelVoaqSzIex1SM8sy6HL2YbMvLnzx9lu6SInER1LidEQCRK0SFxDaLlwZflS5xDU9u23GJbbhas9NoeVzgsCvbBc+/bRHO6Sn39gOF+9eFOQL+3loptC7OjSiCTIECQOKJlkoomAA6mTTTQHpvRLhBH28hxBwXCYiS8fDiROKRFqEk6JJv7yi3csxijjjAEBACH5BAkEAPUALAAAAAAmACYAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQsLCw0NDQ4ODg8PDxERERERERISEhMTExQUFBQUFBUVFRYWFhYWFhcXFxcXFxcXFxgYGBkZGRkZGRoaGhsbGxwcHBwcHB4eHh8fHyAgICIiIiQkJCYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFBQUFFRUVJSUlNTU1RUVFZWVldXV1hYWFlZWVpaWltbW11dXV9fX2BgYGJiYmNjY2VlZWZmZmdnZ2hoaGhoaGlpaWpqamtra2tra2xsbGxsbGxsbG1tbW1tbW5ubm9vb3BwcHFxcXJycnR0dHV1dXd3d3h4eHp6enx8fH19fX9/f4GBgYODg4WFhYeHh4iIiIiIiIiIiImJiYmJiYmJiYmJiYmJiYmJiYmJiYqKioqKioqKioqKioqKioqKioqKioqKiouLi4uLi4uLi4yMjI6OjpCQkJOTk5WVlZeXl5mZmZubm52dnZ6enqCgoKGhoaOjo6SkpKWlpaampqenp6ioqKmpqaqqqqqqqqqqqqurq6urq6urq6urq6ysrKysrKysrK2tra2tra6urq+vr7CwsLCwsLGxsbOzs7S0tLW1tbe3t7i4uLq6ur+/v8PDw8fHx8rKyszMzM7OztDQ0NLS0tTU1NbW1tfX19jY2Nra2tzc3N7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OTk5OXl5eXl5ebm5ubm5ufn5+jo6Ojo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PLy8vPz8/X19ff39/n5+fv7+/z8/P39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/gj+AP8JHEiwoMGDCBMqXMiwocOHCs396dGDjzmIEPnQ2EjDDkaH4YpwpKGD2keD5tIRJDdy47eB5njVwvZQXJ4oUTCpFAhmpJaBzGhgwEAClsObOKOEGhiNy8YrzgZaGTo0RDSG5pLifFMw2lWYVKl2YhgOi9Y8C7mlCIthKUNOWnExPBM2BbiG6UK9kdOrITgwQ3cMO/lwHDfCiBMrHngs0yRgitWtQ8jMjWU3vxB/gwaN3MFRl90YInzumWlokwuCvvyn3snSp9UZRBZaF+JupsMhDBboD6/UhNPJXky8uPHF3oIJO/dwHTqI0Sw5cqSJOUNqwYAhc92w1PTpkBdcnhsGrPw2h5++O8q8EF358l8ZAlMf/5+5YsU8E0RWPthOhusAw4knyRBkTiukkNKKfgLV4000/yF2TIIJFnMcQclQSIoxFw6EziwJwnJRhwKZk0wyI5Ko4oorBgQAIfkECQQA9gAsAAAAACYAJgCHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFhYWGBgYGRkZGhoaHBwcHBwcHR0dHh4eHx8fHx8fICAgISEhISEhIiIiIiIiIiIiIyMjJCQkJCQkJSUlJiYmJycnJycnKSkpKioqKysrLS0tLy8vMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eXl5eX19fYGBgYWFhYmJiZGRkZWVlZmZmZ2dnaWlpa2trbW1tbm5ucHBwcnJyc3NzdHR0dXV1dnZ2d3d3d3d3d3d3eHh4eHh4eXl5eXl5enp6e3t7fHx8fX19fn5+f39/gICAgYGBg4ODhISEhYWFh4eHiIiIioqKi4uLjY2Nj4+PkZGRk5OTk5OTk5OTlJSUlJSUlJSUlJSUlJSUlJSUlJSUlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlpaWlpaWlpaWl5eXl5eXmJiYmJiYm5ubnp6eoKCgoqKipaWlp6enqampq6urra2tr6+vsbGxsrKys7OztbW1tra2tra2t7e3t7e3uLi4uLi4uLi4ubm5ubm5ubm5ubm5urq6urq6u7u7vLy8vLy8vb29vr6+vr6+wcHBxMTEx8fHysrKzc3N0NDQ1NTU2NjY3Nzc4ODg4+Pj5eXl5+fn6enp6urq6+vr7Ozs7e3t7e3t7u7u7u7u7+/v7+/v8PDw8PDw8fHx8fHx8fHx8vLy8vLy8/Pz9PT09PT09fX19vb2+Pj4+fn5+vr6+/v7/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+CP4A/wkcSLCgwYMIEypcyLChw4cK2ZGi4oTROYgQR/3Y+IMQRofpsnD88WTbx4ZFRv64KJAdMl/ZHppT9IVMKnYD9YyEM3DakxEjYhRzqIiLUS63erLZiCbawDNAgcaY1vDLUS58CnbrRpAdi6hAazFMp+ZqooXnfoAd8arhqqtDF/oB68NcQ3a3+BBK1tCcn69WnJ18yO7b4MOIHW6LJWwcwmenTPEdvKuCAQMhlBmMpseO52MnoVm+bOAFuoKxPHsmdfIUadJxB8JSbSdU69eXfxV81tlzbIjQIrwO4bggM1KRjOE8qUq4gQvGEiuEpopVNenYs99tJvihveUOpWuVypTpZsNsy5R1bziLPPn1CdcxU0Y/ZkNX7jMtY7iOPn1qDjHj3iinDZROM80UOJAz/oHH0DKrtAIfOru88souCgr0DTUODuaMhRY2o11BH4Io4ogDrUOMhb6kgyJB6jjjjDov1mjjjQUFBAAh+QQJBAD3ACwAAAAAJgAmAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAiIiIkJCQlJSUmJiYoKCgoKCgpKSkqKiorKysrKyssLCwtLS0tLS0uLi4uLi4uLi4vLy8wMDAwMDAxMTEyMjIzMzMzMzM1NTU2NjY3Nzc5OTk7Ozs9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFxcXFycnJ0dHR2dnZ4eHh6enp8fHx9fX1+fn6AgICBgYGBgYGCgoKDg4ODg4ODg4OEhISEhISEhISFhYWGhoaHh4eIiIiKioqLi4uMjIyNjY2Ojo6Pj4+RkZGSkpKUlJSVlZWXl5eZmZmbm5udnZ2fn5+fn5+fn5+goKCgoKCgoKCgoKCgoKCgoKCgoKChoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGioqKioqKioqKjo6Ojo6OkpKSlpaWmpqampqapqamrq6uurq6xsbGzs7O2tra4uLi7u7u8vLy+vr7AwMDBwcHDw8PDw8PExMTFxcXFxcXGxsbGxsbGxsbGxsbHx8fHx8fHx8fHx8fIyMjIyMjJycnJycnOzs7S0tLW1tba2tre3t7h4eHl5eXp6ens7Ozu7u7w8PDy8vLz8/P19fX29vb39/f39/f4+Pj5+fn5+fn6+vr6+vr6+vr6+vr7+/v7+/v7+/v7+/v8/Pz8/Pz9/f39/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v4I/gD/CRxIsKDBgwgTKlzIsKHDhwrZ0WKjRhY7iBBlLdm4BBPGh244LilT7mPDLCKhFJQGjdvDbp3k1MlF8JFIQwO1iXnxwkcyh53OCD3DLOeejXiwDQTEk6cPlwzlDD0TqaA2bQV9NOXZq+GeqZcYXtn64ldDW0PVTGNYaesaaw6LRaK0liG7ST5wpKlmsq/fv4ARcnP2DBzCa7do1TXpTEWECDmkGbSGaJBlaCa5mXj8OIdhgrssW3Zl0hZnzmZBix6kqvTpx7sKWjMk2lnmzZxVQCVYbdarZ36TiXisAnhghNlw8dp9vLlzhNWuYXQHcdusUqVoNtQ2bRpch8CwXGP/rtAdNWnomSvcJb7U4oTupqGXhrVhNfEWCaarVi2dbPTT3PMQNbnsotRA7CSzyy7F+EfQN9oICJg1Cy7I13MDUVjhhRj+484zCzpzUYcCuWONNSOSqOKKKwYEACH5BAkEAP8ALAAAAAAmACYAhwAAAAAAAAAAAAAAAAAAAAAAAAEBAQEBAQEBAQICAgICAgMDAwMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgwMDA0NDQ8PDxERERMTExUVFRgYGBsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKy0tLS8vLzAwMDExMTMzMzMzMzQ0NDU1NTY2NjY2Njc3Nzg4ODk5OTk5OTo6Ojs7Ozw8PDw8PD09PT09PT4+Pj4+Pj8/P0BAQEBAQEFBQUJCQkNDQ0VFRUZGRkdHR0hISElJSUtLS01NTU9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX9/f4CAgIGBgYODg4SEhIaGhoiIiImJiYuLi4yMjI2NjY6Ojo6Ojo+Pj4+Pj5CQkJCQkJCQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZubm5ycnJ2dnZ+fn6CgoKKioqOjo6Wlpaenp6mpqaurq6urq6urq6ysrKysrKysrKysrKysrKysrKysrK2tra2tra2tra2tra2tra2tra2tra2tra6urq6urq6urq+vr6+vr7CwsLCwsLGxsbOzs7W1tbi4uLu7u729vb+/v8HBwcTExMfHx8jIyMrKyszMzM3Nzc/Pz8/Pz9DQ0NDQ0NHR0dHR0dLS0tLS0tLS0tPT09PT09PT09TU1NTU1NXV1dXV1dXV1dbW1tbW1tfX19fX19jY2Nra2t3d3eDg4OXl5erq6u7u7vLy8vf39/r6+v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHC4f52QMMosVfVjJa6WXx4R+NVu50dAgH5JiC5MCle5jO1yBDxAiCArlpoDk9OXJI8ebQ15yfc54NREcpSxZH5wYayplTCrqGgoDO8VQQ3VOCVZjmvNbQkdRQDMVozdGsITGgdcQx7KR1jzmHxzh9UsswnSYnR+6UG8m3r9+/Cb/RPVhOmLBxfcMh6dChCTmD5ioxmhxu5DkijBk3MVhs8mSOHYllzhytYGfPoC2KHt1BWUFzkjyDG2kux2gkB8n56jWb7zPbHYhUBozQXLJnSYkrtzhu2dWH5ZIzHEdkwAAOFRuiC5YqFrKG1a1tD7iAmCG4VOhzPVb4TLx4Qg2joUe/PuEy99bxNCS3C72w5/+kQw45KwmEDgf4FeOQOcp4I12A1BxzzDMF/vMLBeL98Vc5Ekq410DiEIKHgn+Z0+ExHy43ED3cSHgNPSoWRE855sAY44045ohQQAAh+QQJBAD/ACwAAAAAJgAmAIcAAAABAQECAgIDAwMEBAQFBQUODg4PDw8QEBARERESEhISEhITExMTExMUFBQUFBQUFBQVFRUVFRUVFRUVFRUVFRUWFhYWFhYWFhYWFhYWFhYWFhYXFxcXFxcXFxcYGBgYGBgZGRkZGRkaGhobGxsbGxscHBweHh4fHx8gICAiIiIjIyMlJSUnJycpKSkrKysuLi4xMTEyMjIzMzM0NDQ1NTU2NjY3Nzc5OTk7Ozs8PDw9PT0/Pz8/Pz9AQEBBQUFCQkJCQkJDQ0NERERERERFRUVFRUVFRUVGRkZHR0dHR0dISEhJSUlKSkpKSkpMTExOTk5QUFBSUlJUVFRVVVVWVlZXV1dYWFhZWVlZWVlaWlpbW1tbW1tcXFxcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NlZWVnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3t8fHx9fX1+fn5/f3+AgICBgYGCgoKDg4OEhISFhYWGhoaHh4eIiIiJiYmKioqLi4uMjIyPj4+RkZGTk5OVlZWWlpaXl5eYmJiZmZmampqampqbm5ucnJycnJydnZ2dnZ2enp6fn5+hoaGioqKioqKjo6OkpKSlpaWmpqanp6eoqKipqamrq6usrKytra2vr6+wsLCysrK0tLS1tbW3t7e3t7e4uLi4uLi4uLi4uLi4uLi4uLi4uLi5ubm5ubm5ubm5ubm5ubm5ubm5ubm5ubm6urq6urq6urq7u7u7u7u8vLy8vLy+vr6/v7/CwsLExMTGxsbIyMjLy8vOzs7Q0NDS0tLU1NTW1tbY2NjZ2dnb29vb29vc3Nzc3Nzd3d3d3d3e3t7e3t7f39/f39/g4ODh4eHi4uLk5OTl5eXm5ubn5+fp6enq6urr6+vu7u7x8fH19fX4+Pj7+/v9/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7///8I/gD/CRxIsKDBgwgTKlzIsKHDhwuhPXLkDKLFZWbIaERm8eEjjRoVdXQoCCSZPgXZqYP4blmkTNUICjPJauC7R0aMuCHncJmfn3622TyVZs2pdwNB5cyJxmEkoH5aMUSzNOe3hpagvmJYp6qRaw2nASWUjuGqqozcOdzW6lXZhqzMhHHEbqTdu3jzJky3LiE7Z83O3Q03xYWLMW8JshN16VKmcnajGDY8xiC0xo2TjYw22TAMcAUvY9bckXNnF9kKrvOEeVzkzlYOqmO2DLJda04MW3GtN6G1cL2Dj1wn2CK7dg7XcTFgQMYzh/ea1eo1reFy5gZQJFY4rpb3YysVWYbLgJ05pIbZvHsPnzBceeaMGrYT5n1ZwXvr1t0j+OJ9RfnXgKPWQPd8I4002+wnEDMlYFdIXuwceGBfA40DCSP/4RWhhHUJV9A4BwLnoUHsdDjiiSimiFBAACH5BAkEAP8ALAAAAAAmACYAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhQUFBcXFxkZGRsbGx0dHR8fHyAgICIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSkpKSoqKioqKisrKysrKysrKywsLCwsLCwsLCwsLCwsLC0tLS0tLS0tLS0tLS0tLS0tLS4uLi4uLi4uLi8vLy8vLzAwMDAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzk5OTw8PD8/P0FBQUREREZGRkhISEpKSktLS0xMTE1NTU1NTU5OTk9PT09PT1BQUFBQUFBQUFFRUVJSUlJSUlNTU1RUVFVVVVVVVVdXV1hYWFlZWVtbW11dXV9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2lpaWpqamtra2xsbG1tbW5ubm9vb3BwcHBwcHFxcXJycnJycnNzc3Nzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6enx8fH5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpiYmJmZmZubm52dnZ+fn6CgoKKioqOjo6SkpKWlpaampqenp6ioqKioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6ury8vL29vb+/v8DAwMLCwsPDw8TExMTExMTExMTExMTExMTExMTExMXFxcXFxcXFxcXFxcXFxcXFxcXFxcbGxsjIyMrKys3NzdDQ0NLS0tTU1NbW1tfX19ra2tvb293d3d/f3+Dg4OLi4uPj4+Tk5OXl5eXl5ebm5ufn5+jo6Orq6uvr6+3t7e/v7/Dw8PHx8fHx8fLy8vPz8/X19fj4+Pr6+vz8/P39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHC6992jQNosVmcdho7GXxYSaNGiV1dLgIJBtDBdutg/iuGSdQ2AjqMjlroLtMUaLk+eawmaKfjLwRrOXHz6x3A1nlzBnHIaefP3Mx7LM0J7mGnqAq4rjwUNUoQhleY/SzUjqGtapOcueQW65eZxvmsrNmE7uRePPq3ZtwXbuE7ahNQ5eXHJweQPisTOkqVKhT5fCy6UG5Bx+D1Rw7fjYSG5DKPYpcJZhZM+eO2H6AJhKuILtVjklFHmkGdJ2D66I9mz2SnBfKcEbzRUgu7vDjFt2Z69iObUNzczBQYCLtIbVgyLQ1nEOhO4UcyxlljutF3tnihOVgeO8equE38uTPIzS3vjumhu6akY9mkJ38f0ysZ4E1DrnjjThIEURONtmIQ5A0OXRnQSd7uaMNg9k4J5A5qGBC4F7tcMPgNn8hVxA62mjDm4kEvZMgizDGKCNCAQEAIfkECQQA/wAsAAAAACYAJgCHAAAAAQEBAgICAwMDBAQEBQUFBgYGBgYGBgYGBgYGBwcHBwcHCAgICAgICQkJCQkJCgoKCwsLCwsLDAwMDg4ODw8PEBAQEhISExMTFRUVFxcXGRkZHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKioqLS0tLy8vMTExMzMzNTU1NjY2ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/Pz8/QEBAQEBAQUFBQUFBQUFBQkJCQkJCQkJCQkJCQkJCQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDRERERERERERERUVFRUVFRkZGSEhISkpKS0tLTU1NT09PUFBQUlJSU1NTVFRUVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXV1dXl5eX19fX19fYGBgYWFhYmJiY2NjZWVlZmZmaGhoaWlpbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5enp6e3t7fX19fn5+f39/gICAgYGBgoKCg4ODhISEhISEhYWFhoaGhoaGh4eHh4eHiIiIiYmJioqKi4uLjIyMjY2Njo6OkJCQkpKSk5OTlJSUlZWVlpaWl5eXmJiYmZmZmpqam5ubnJycnZ2dnp6en5+foKCgoaGho6OjpKSkpqamp6enqKioqampq6urra2trq6ur6+vsLCwsbGxsbGxsrKysrKys7OztbW1tra2uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIysrKy8vLzc3Nzs7O0NDQ0dHR0tLS0tLS0tLS0tLS0tLS0tLS0tLS09PT09PT09PT09PT09PT09PT09PT09PT1NTU1NTU1NTU1dXV1tbW2dnZ29vb39/f4uLi6Ojo7Ozs8fHx9PT09vb2+Pj4+fn5+/v7/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+////CP4A/wkcSLCgwYMIEypcyLChw4cLycVyNQ6ixXF78Gj8ZvHhKY0aRXV06AkkHkojC4KLJYscQWQmhxFMdSaMI3MOwVXaqWkdwWGSJP0imMuLUS+JHLrauZPjwkhHjaJrKItppXAMM0VFg5MhOU07RbFjSMzM0VEP130LN7bhMkZ6YrVNSbeu3bsN2Y0Tl46uOUBPokSaWnBdL1iwahHu2AeHYxyRDI5DjNipxXJSHuPI0pfgZMqWIZaLohnLucK5EMtabBGPZkAH14kLdzplOddAAHXFexCdOt7AgydkZ88hu0MsUpCp6HCcsmvlGh4SQV1Ek84LzSnb/u23wnVGqmpTl9Ww3Pbt3gWSS3WK+T92KcSLiJX32XZxBGtlQIDgQqqBZIingkt5lWNOcQKZsx9//TE3ThPUqUBLXbIwyOApA6FjSywE0hWLhfyRItw/54Rg4QbRjYhMCfyVUMyIA52DDDLYwWgjcAEBACH5BAkEAP8ALAAAAAAmACYAhwAAAAEBAQICAgMDAwQEBAUFBQsLCwwMDA0NDQ0NDQ4ODg4ODg8PDw8PDxAQEBAQEBAQEBERERERERERERERERISEhISEhMTExMTExQUFBQUFBUVFRYWFhYWFhcXFxkZGRoaGhsbGx0dHR4eHiAgICIiIiQkJCcnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+PkBAQENDQ0VFRUdHR0lJSUtLS0xMTE5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVVVVVZWVlZWVldXV1dXV1dXV1hYWFhYWFhYWFhYWFhYWFlZWVlZWVlZWVlZWVlZWVlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWFhYWJiYmNjY2RkZGRkZGVlZWZmZmZmZmdnZ2hoaGlpaWtra2xsbG5ubm9vb3BwcHFxcXNzc3R0dHZ2dnh4eHp6en19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaWlpaampqenp6enp6ioqKioqKmpqaqqqqurq6ysrK2tra+vr7KysrS0tLe3t7i4uLq6uru7u729vb6+vr6+vr+/v8DAwMDAwMHBwcLCwsTExMXFxcbGxsfHx8nJycvLy8zMzM3Nzc3Nzc7Ozs/Pz8/Pz9DQ0NHR0dLS0tLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tzc3N3d3d3d3d3d3d3d3d3d3d3d3d7e3t7e3t7e3uHh4eTk5Obm5unp6ezs7PDw8PPz8/b29vj4+Pn5+fv7+/z8/P7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHC8ndukUOosVyiAgRMhTO4sNZGjW+8ugQVkhCqkgWHMfrlzmC104+I1hLz51N5xyOA8VzlT2Cyjp1IkbQ2ZujbzY55MWTZ8eFopC+kZOu4a+moMQxVCV1T06G5lbxlNVQnBykrh7aC6fV4bdMknqpnEu3rl2I9siRWzc33SUuY0KxM2hP2URgfEkuIsKYSCiDEifeenqRS2MicyBLnkyynJTLctoVtHds4q+qJA1d9nSw3ThxiUmaS7QEyiXUd3Pr3u3xp0N2qIjs4PPSYblr34ov/OSiuQs7yhWiu2bNWrjYCbk4bw6s4bnq1q5dYT/Hi1b0Hdtd5Gq4tvo4oCsmTDAhbCCf7TuiK7R3Dh3Bc/HJN19x5YTR3A5yzSWMgALSMhA7yOSin0e/MCjfLLz9k44NDK7wFW/SvCDfC8pkONA5yiiDm4ks6hYQACH5BAkEAP8ALAAAAAAmACYAhwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAgQEBAYGBggICAoKCgsLCw0NDQ4ODhAQEBERERMTExQUFBUVFRYWFhcXFxgYGBkZGRkZGRoaGhsbGxwcHB0dHR0dHR0dHR0dHR4eHh4eHh8fHx8fHyAgICAgICEhISIiIiIiIiMjIyUlJSYmJicnJykpKSoqKiwsLC4uLjAwMDMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUtLS05OTlBQUFJSUlRUVFZWVldXV1lZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGBgYGFhYWFhYWJiYmJiYmJiYmNjY2NjY2NjY2NjY2NjY2RkZGRkZGRkZGRkZGRkZGRkZGVlZWVlZWZmZmdnZ2lpaWpqamtra2tra2xsbG1tbW5ubm9vb3BwcHBwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3l5eXp6enx8fH19fX9/f4CAgIKCgoODg4WFhYaGhoiIiImJiYmJiYqKiouLi42NjY6OjpCQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaOjo6SkpKWlpaampqenp6ioqKmpqaqqqqqqqqurq6ysrKysrK2tra2tra6urq+vr7CwsLGxsbKysrOzs7S0tLa2tri4uLm5ubq6uru7u7y8vL29vb6+vr+/v8HBwcLCwsPDw8TExMbGxsfHx8nJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tLS0tPT09PT09PT09TU1NTU1NXV1dbW1tbW1tfX19fX19jY2NjY2NnZ2dra2tra2tzc3OHh4ebm5uvr6+7u7vLy8vT09Pj4+Pz8/Pz8/P39/f39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHC80VK1YOosVyliBBqjTO4kNhGjXq8uhwV0hItEgWNFcNHDqC4U5+I6isESJZ5xyOm9WqFbCXA8HNijVzIDc9SPXAclitZ8+OC00l1fPH4TensMQxhDU1EdCF6G71LNZQHJ+kvyCOq+gwnKhPx1TKnUu37kVy6+SiW6WnDy6E3YwZg5ZOZacniJ/8LVhOsGCtHs/JSfxkkEFzjo1BtnguDuVAB7EJZvbVYiXKKQ3WIzeusMpzoti4WeXaru3bCKWBw22wmAkCBHCEY5iu1ZQmhtguHDcBOHAcpQ+SykE9B6GcCzk5dz5s4Znq1KVoMYS0HThZgeecOcMukAn4HNUYSkuwfYI5geCAfPhQI65AQ+AxwZ5ChjjnQC0DEbHffjVgV04c1DHhzEPF/JHIcAJxs+CCEwZVzYAkQbPhfsTwJpASGxIBom3joPjBERiamCE3MtYoY0AAIfkECQQA/wAsAAAAACYAJgCHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLDQ0NDw8PERERExMTFRUVFhYWGBgYGRkZGxsbHBwcHh4eHx8fICAgISEhIiIiIyMjJCQkJCQkJSUlJiYmJycnKCgoKCgoKCgoKCgoKSkpKSkpKioqKioqKysrKysrLCwsLS0tLS0tLi4uMDAwMTExMjIyNDQ0NTU1Nzc3OTk5Ozs7Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXl5eYWFhY2NjZWVlZ2dnaWlpampqbGxsbW1tbm5ub29vcHBwcXFxcnJyc3Nzc3NzdHR0dHR0dXV1dXV1dXV1dnZ2dnZ2dnZ2dnZ2dnZ2d3d3d3d3d3d3eHh4eHh4eXl5eXl5enp6enp6e3t7e3t7e3t7fHx8fHx8fX19fX19fn5+fn5+f39/gICAgICAgYGBgoKChYWFh4eHioqKjIyMjo6OkJCQkZGRk5OTlZWVlpaWlpaWl5eXmJiYmpqam5ubnZ2dnp6en5+foKCgoaGhoqKio6OjpKSkpaWlpqamp6enqKioqampqqqqq6urrKysra2trq6ur6+vsLCwsbGxsrKys7OztLS0tbW1tra2t7e3ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwMDAwcHBwsLCwsLCw8PDw8PDxMTExcXFxsbGx8fHyMjIysrKzMzMzs7Oz8/P0dHR0tLS1NTU1dXV1tbW19fX2dnZ2tra3Nzc3d3d3t7e39/f39/f39/f4ODg4ODg4ODg4eHh4uLi4+Pj5eXl5+fn6Ojo6urq7Ozs7+/v8fHx8/Pz9fX19vb29/f3+fn5+/v7/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+////CP4A/wkcSLCgwYMIEypcyLChw4cL2UF7pg6iRXakOHEKhc7iQ2YaNSbz6DBZSE7GSBZ8t00cPILmOoUkR7CaJ0y+XjZ8pwsXrmMFye3aJY4guEWIkvJyuM2nz3QMbyVNCskhOKe5zjHUNRVRJYfwjvl81hCdo6lkH6Zb9xDdrVjVVMqdS7euRXfs8M315QgSM4TjokXDppfkrC+Iv/wt6E6w4I4kESX+MslgY8eQPUpOLOlgOMHXdHqENTkYwnXqCpOEB8tQopR2Y8tWWA7q7ILNcDx4sEUrw2FmxHRy1zCdid27qTCcVaR5EUoNQyFHHm0hHufNtzG0NH13WoHgrF0VBIO9yDWG0ihMN/FO4LgrKlQowTZwE/YvDiEh39BrIJX48SkxkDuNNPdFOA8tI4kl4wxEDoAAijfQOefR5Q0NEKogzW0CdQHhFRwKdI6HKlzRYIgCoZMZiizGFhAAIfkECQQA/wAsAAAAACYAJgCHAAAAAQEBAgICAwMDBAQEBQUFBgYGEREREhISExMTExMTFBQUFBQUFRUVFRUVFRUVFhYWFhYWFhYWFhYWFhYWFhYWFhYWFxcXFxcXFxcXFxcXFxcXFxcXFxcXFxcXGRkZGxsbHR0dHx8fISEhIiIiJCQkJSUlJycnKCgoKioqKysrLCwsLS0tLi4uLy8vMDAwMDAwMTExMjIyMzMzNDQ0NDQ0NDQ0NDQ0NTU1NTU1NjY2NjY2Nzc3Nzc3ODg4OTk5OTk5Ojo6PDw8PT09Pj4+QEBAQUFBQ0NDRUVFR0dHSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ucHBwc3NzdXV1d3d3eXl5e3t7fHx8fn5+f39/gICAgYGBgoKCg4ODhISEhYWFhYWFhoaGhoaGh4eHh4eHiIiIiIiIiIiIiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJioqKioqKioqKioqKioqKi4uLi4uLjIyMjIyMjY2Njo6Oj4+PkJCQkpKSlZWVmJiYmpqanJycnp6eoKCgoqKipKSkpKSkpaWlpqamp6enqKioqampqqqqrKysra2trq6ur6+vsLCwsbGxsrKys7OztLS0tbW1tra2t7e3uLi4ubm5urq6u7u7vLy8vb29v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGxsbGx8fHyMjIyMjIycnJycnJysrKy8vLzMzMzc3Nzs7O0NDQ0tLS1tbW2dnZ29vb3d3d39/f4ODg4uLi5OTk5eXl5ubm5+fn6Ojo6enp6+vr7Ozs7u7u7+/v8PDw8fHx8vLy8/Pz9PT09fX19vb29/f3+Pj4+fn5+vr6+/v7/Pz8/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+////CP4A/wkcSLCgwYMIEypcyLChw4cL21Wrxg6ixXaxTp16lc7iQ2oaNUrz6HBayFPPSBaM9y1cwXKqNKYSRzAbKlHD4jmMx0yYsGkFxTlzRnMgOEyRkgpz6M2nz3UMjSVN+snhN6fJzjFkNjVSKIf4pgkbdq2hOk9TgT5c1+6humG9sqmcS7euXYv44NU1holTNITmsmEDNxdYm8Nt1BLEJxgbto4kNSFuI8ogY8ePVUpG/NVgOcHg8KnsNbkZwnjv6MbbBYhQsruwYy9EV1F2wWpVNoBQU3RhMj53WrlriM7GhuMbzjDkBaU5FE8NZSE/PkKuQkXOm3djyGr68W8Ex1yNKzgnOxSXC7OJmC5kYLg2NmyYQf9vVXY2DlOt35DC2cA38cUHxkDuRNIcG9o8lE0opfSWToABjjdQOfTNFc4REPpgnW10QPiGbQOZQ0cPPcBRIYjttAXiiiwGBAAh+QQJBAD/ACwAAAAAJgAmAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcHBwcICAgJCQkKCgoMDAwNDQ0PDw8RERESEhIUFBQVFRUXFxcZGRkbGxsdHR0fHx8gICAgICAgICAhISEhISEhISEhISEhISEhISEhISEiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIkJCQmJiYoKCgqKiosLCwtLS0vLy8wMDAyMjIzMzM1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs7Ozs8PDw9PT0+Pj4/Pz8/Pz8/Pz8/Pz9AQEBAQEBBQUFBQUFCQkJCQkJDQ0NERERERERFRUVHR0dISEhJSUlLS0tMTExOTk5QUFBSUlJVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3t8fHx9fX1/f3+BgYGFhYWIiIiKioqMjIyOjo6Pj4+QkJCRkZGSkpKTk5OTk5OUlJSUlJSVlZWVlZWVlZWWlpaWlpaWlpaXl5eXl5eXl5eYmJiYmJiYmJiZmZmZmZmampqampqampqampqampqbm5ubm5ubm5ucnJycnJydnZ2dnZ2enp6fn5+goKChoaGioqKjo6OkpKSmpqanp6epqamrq6utra2wsLCzs7O1tbW2tra3t7e4uLi5ubm6urq6urq7u7u8vLy9vb2+vr6/v7/AwMDCwsLDw8PExMTFxcXGxsbHx8fIyMjJycnKysrLy8vMzMzNzc3Ozs7Pz8/Q0NDS0tLT09PU1NTV1dXW1tbX19fY2NjZ2dnZ2dna2trb29vb29ve3t7h4eHj4+Pm5ubp6ens7Ozu7u7x8fHz8/P29vb5+fn7+/v8/Pz8/Pz9/f39/f39/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7///8I/gD/CRxIsKDBgwgTKlzIsKHDhwvRgfuGDqJFdsFo0fpV0aJDbxo1dvPosFtIWtxIFmQ3rlxBc7k01nI5MJytWdUespv27NlIguaoUTNHUJ0pR0ijORzXs2dHhdyQIo3lkFzTaucYepPqSNbDbs+ihXP4CuklbxDRsdPpbVpWlXDjyv1nztcscXMTXmthwMCDWXCjpWL1s6CMvn0ffCMZDY/jPYsJKkOMWBPJVI4de5VMuS8jkqgy49lM8AXlBSk9NhOdmiAzD30drIKrLFInaQjFzYKFNq/v38ANhjvTIoYfogy5MSrky6G6KSaim/DDUJkZLthhNRQmPbqNcQs7Z2HHboamwl3do5MjeA75wEHjsbtPOC5GdysDzf1BEgQPeIG4xPeHQ8LYZ4IPvf0TSBAM9jeQOq5g98d6DoFDyy7uqeNEgwzOh858KplzBYdS/Bfcgg0OGJxA5QTihBMTrijjjDQSFBAAIfkECQQA/wAsAAAAACYAJgCHAAAAAQEBBAQEBgYGBwcHCQkJCgoKCwsLDAwMDAwMDQ0NDg4ODw8PDw8PEBAQEBAQEBAQEREREREREhISExMTFBQUFRUVFhYWGBgYGRkZGxsbHBwcHh4eHx8fISEhIyMjJSUlJycnKSkpKysrLCwsLCwsLCwsLS0tLS0tLS0tLS0tLS0tLS0tLS0tLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uMDAwMjIyNDQ0NjY2ODg4OTk5Ozs7PDw8Pj4+Pz8/QUFBQkJCQ0NDRERERUVFRkZGR0dHR0dHSEhISUlJSkpKS0tLS0tLS0tLS0tLTExMTExMTU1NTU1NTk5OTk5OT09PUFBQUFBQUVFRU1NTVFRUVVVVV1dXWFhYWlpaXFxcXl5eYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5enp6e3t7fHx8fX19fn5+f39/gICAgYGBgoKCg4ODhISEhYWFhoaGh4eHiIiIiYmJi4uLjY2Nj4+PkZGRk5OTlZWVl5eXmZmZm5ubnJycnZ2dnp6en5+foKCgoKCgoaGhoaGhoqKioqKio6Ojo6OjpKSkpKSkpaWlpqampqamp6enqKioqKioqampqampqampqqqqqqqqqqqqqqqqqqqqqqqqq6urq6urq6urrKysrKysra2tra2trq6ur6+vsLCwsbGxsrKys7OztLS0tra2t7e3ubm5u7u7vb29v7+/wsLCxcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Ozs7Oz8/P0NDQ0dHR0tLS09PT1NTU19fX2dnZ2tra3Nzc3d3d3t7e39/f4ODg4eHh4uLi5eXl6enp7Ozs7+/v8fHx8/Pz9fX19vb29/f3+Pj4+fn5+vr6+/v7/f39/f39/f39/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+////CP4A/wkcSLCgwYMIEypcyLChw4cL8YkThw+iRXzQhAlr9s7iw3AaNYLz6BBkyG8kDZpTV5BdMY3E1hEsV2wYyoffsGELV3CdN2/pCtLSRPQmQ3M6sWVjxxAcUaLBHJrLprObzIXinmoa9jFbtnEOgxE1JQ4iu45d26Vcy7atwHTTlplzmxCbDwkSRBxb2+3WLp4GeeDFC6KsR2yEEhsaSbDZ4MGtSN5KnBhYQcePJYQiaYsyIV+BH29gbNEaZUNGB2LDgdeDsLXWTJnahjDdMF+G6erezbtguUE+mmBSyzAcKU/NHKLDI6O5jEjF3ZyZDpphNOfNqzBV2Gr69DxzF14+wy4DR0F16Api8j793FEf2NMMRKdIipRE5AY2Y//I4TH4MmxBGiRNFNhEIgT5It0j+UlFzDPECeSFgU1kUdA77rnFBoVjBNXbP5xQCMmHArXDCRhgWJIeiSy22GJAACH5BAkEAP8ALAAAAAAmACYAhwAAAAAAAAEBAQICAgMDAwQEBAYGBgcHBwgICAkJCQoKCgwMDA4ODg8PDxERERMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhoaGhsbGxsbGxwcHBwcHBwcHB0dHR4eHh4eHh8fHyAgICEhISMjIyQkJCYmJigoKCkpKSsrKywsLC4uLjAwMDIyMjQ0NDY2Njc3Nzc3Nzc3Nzg4ODg4ODg4ODg4ODg4ODg4ODg4ODk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTs7Oz09PT8/P0FBQUNDQ0REREZGRkdHR0lJSUpKSkxMTE1NTU5OTk9PT1BQUFFRUVJSUlJSUlNTU1RUVFVVVVZWVlZWVlZWVlZWVldXV1dXV1hYWFhYWFlZWVlZWVpaWltbW1tbW1xcXF5eXl9fX2BgYGJiYmNjY2VlZWdnZ2lpaWxsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZiYmJycnJ+fn6GhoaOjo6WlpaampqioqKioqKmpqaqqqqqqqqurq6urq6ysrKysrKysrKysrK2tra2tra2tra2tra6urq6urq6urq6urq+vr6+vr7CwsLCwsLCwsLCwsLGxsbGxsbGxsbKysrKysrOzs7Ozs7S0tLW1tba2tre3t7i4uLm5ubq6ury8vL6+vr+/v8HBwcPDw8XFxcjIyMvLy8zMzM3Nzc7Ozs/Pz9HR0dLS0tPT09TU1NbW1tjY2NnZ2dvb29zc3N7e3t/f3+Dg4OLi4uPj4+Tk5OXl5ebm5ufn5+jo6Orq6uvr6+3t7e7u7vDw8PHx8fLy8vT09Pb29vj4+Pr6+v39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHDNWpuwex4j9rypRRo2ixIbqMGc91LHht0qVwBM2BVFZu5EBLBQIEcEBsoDtoGZ+5I9gum7R0Dq/FlBlgg7qB7cCBa0fwXjJTplyJaziJKFFlC8tBheqsoSWrMo1lvbWVWsNwDqyuYLqQGVRf7BwCgyBzRTWH6cRxdKhO2TO2LgML/qft7uCE46R06NADmstvxYy1NPhl8WIXRy2Gi/ToUaXJA6+BsLx4WEdhnTsXI0l68a+OwVI/Wl1wCWkVmStySx1pakFwSBarQOZyW69fKA+yo9Ys9+Hn0KMTVKeoiphNOxmOcxVK2sNDPcJw98CkPc+c88kaYhMf/gtDWufPK8qu8Br7HkUYdoo/505Dd1Kw559A7VxyBhmXxCWQNPx14lA1AfaQBmijYGEhFpsQZIwfd3SiYEPrTHNNQXBciIUa0g3kh4l1pCiQLiaO4uI/7uhyni70zajjjtEFBAAh+QQJBAD/ACwAAAAAJgAmAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsLCwsMDAwNDQ0ODg4PDw8RERESEhITExMUFBQVFRUWFhYYGBgaGhobGxsdHR0eHh4gICAhISEiIiIjIyMjIyMkJCQlJSUmJiYmJiYnJycnJycnJycoKCgoKCgpKSkqKiorKyssLCwtLS0vLy8wMDAyMjIzMzM1NTU2NjY4ODg6Ojo8PDw+Pj5AQEBCQkJDQ0NDQ0NDQ0NERERERERERERERERERERERERERERFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVHR0dJSUlLS0tNTU1PT09QUFBSUlJTU1NVVVVWVlZYWFhZWVlaWlpbW1tcXFxdXV1eXl5eXl5fX19gYGBhYWFiYmJiYmJiYmJiYmJjY2NjY2NkZGRkZGRlZWVlZWVmZmZnZ2dnZ2doaGhqampra2tsbGxubm5vb29xcXFzc3N1dXV4eHh5eXl6enp7e3t8fHx9fX1+fn5/f3+AgICBgYGCgoKDg4OEhISFhYWGhoaHh4eIiIiJiYmKioqLi4uMjIyNjY2Ojo6Pj4+QkJCRkZGSkpKTk5OUlJSVlZWWlpaXl5eYmJiZmZmampqbm5ucnJydnZ2enp6fn5+goKCioqKkpKSmpqaoqKirq6uurq6wsLCxsbGzs7O0tLS1tbW2tra2tra3t7e3t7e4uLi4uLi5ubm5ubm6urq6urq6urq7u7u7u7u8vLy8vLy9vb29vb2+vr6+vr6/v7+/v7+/v7+/v7+/v7+/v7/AwMDAwMDAwMDBwcHBwcHCwsLCwsLDw8PExMTFxcXGxsbHx8fIyMjJycnLy8vNzc3R0dHU1NTY2Njb29vf39/i4uLk5OTl5eXm5ubn5+fo6Ojp6enr6+vs7Ozt7e3v7+/w8PDy8vLz8/P09PT19fX19fX29vb39/f5+fn6+vr8/Pz+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7///8I/gD/CRxIsKDBgwgTKlzIsKHDhwzbtYNIUSA3a9a2VXzYDiNGdRsLXqNEShzBdR6tmQs5kBMEBgw4TBt4DxtGbPcI3uuGbWLDay9hMlhBEB45cvAKSosVqxa7hpSECrW2EB1TptgaepIKs9rCcryuZmsojoNUJw2tMWX2zuG0FTCdfHPIbhzFd9U0stzLd6C5cH0VhnuTIoWUsSHFRZtG7mCcwoWftK0ojtOmTaAaE/w2A3LhmRWdXb4MeiA4Gp5TONvYbPQmaQYfQ+bhkyK40Zw0E/TmpTAPqiHBNXNmEqG2rIGTK19+8F2oN3NmOTQn7BXihpiSaE9SiqE5RH7CZQNf+C3K9iRzGAoLH15Sw3Dnk3xh+Iq9H0MO45yHRDDWnTqmJCVQNvZJ1xA3b2jHxzoDCUPGg2TEQpA1khgyi4AOZTMXQYRASEYfzA0EiYeIhCiQMR7WYqJAzxhiSDMrxijjigEBACH5BAkEAP8ALAAAAAAmACYAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxcXFxgYGBkZGRoaGhsbGx0dHR4eHh8fHyAgICEhISMjIyUlJSYmJigoKCoqKisrKywsLC0tLS4uLi8vLzAwMDExMTExMTIyMjIyMjMzMzMzMzMzMzQ0NDU1NTU1NTY2Njc3Nzg4ODo6Ojs7Oz09PT8/P0BAQEJCQkNDQ0VFRUdHR0lJSUtLS01NTU5OTk5OTk5OTk9PT09PT09PT09PT09PT09PT09PT1BQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFJSUlRUVFZWVlhYWFpaWltbW11dXV5eXmBgYGFhYWNjY2RkZGVlZWZmZmdnZ2hoaGlpaWlpaWpqamtra2xsbG1tbW1tbW1tbW1tbW5ubm5ubm9vb29vb3BwcHBwcHFxcXJycnJycnNzc3V1dXZ2dnd3d3l5eXp6enx8fH5+foCAgIODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq62tra+vr7GxsbOzs7W1tbe3t7m5ubu7u729vb6+vr+/v8DAwMHBwcLCwsLCwsPDw8PDw8TExMTExMXFxcXFxcbGxsbGxsfHx8jIyMjIyMnJycrKysrKysvLy8vLy8vLy8zMzMzMzMzMzMzMzMzMzMzMzM3Nzc3Nzc3Nzc7Ozs7Ozs/Pz9HR0dTU1NfX19nZ2dvb297e3uLi4ubm5ujo6Ovr6+3t7e/v7/Hx8fLy8vT09PX19fb29vb29vf39/j4+Pj4+Pn5+fn5+fr6+vv7+/v7+/39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiwocOHDPPlg0hRYDlv3shVfBjvG0Zv7jYW/NYqljmC8j56iydyYCsRGDDQyEYwHMZwBseBk+fwG8yYGIQUPHduIsFtvHgVY8mQFVCg4ha685WU17eGTp966LbwnLKqVxmSY/F0S0NvwHhh49kQ24+YW8A5fJeuIjiNLfPqHeju3F6F5wThoFHGW0tz2bSxO2iIhmMaYESaW2XKVCu/BM0VeexY20ZslSvTzEyE88zPoU2NJiiIs9mN5CibWnVyKB/HXLiKJHcNW+2D5aL+HU68+EF5rgQR4uWQ3TNhch16okKdCiyG7DgpUsRI90JyXKpdUwnE8Nn27aAakhNPhQ1DX+cVaXK4R/wngrgQGaJFEByj87b0dAd1hKgzkDBvJPgGfwNxA4omtrDlkDh4DdSIgm8YYtxAmGAYyYYC+YJhgCD+cw0mm0xT4ooslhgQACH5BAkEAP8ALAAAAAAmACYAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiIiIiMjIyQkJCUlJSYmJigoKCkpKSkpKSoqKiwsLC0tLS8vLzExMTIyMjQ0NDU1NTc3Nzg4ODk5OTo6Ojo6Ojs7Ozw8PD09PT09PT4+Pj4+Pj4+Pj8/Pz8/P0BAQEFBQUJCQkNDQ0REREVFRUdHR0lJSUpKSkxMTE1NTU9PT1FRUVNTU1VVVVdXV1lZWVpaWlpaWlpaWltbW1tbW1tbW1tbW1tbW1tbW1tbW1xcXFxcXFxcXFxcXFxcXFxcXFxcXFxcXF5eXmBgYGJiYmRkZGVlZWdnZ2lpaWpqamtra21tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXV1dXZ2dnd3d3h4eHh4eHh4eHl5eXl5eXl5eXp6enp6ent7e3x8fHx8fH19fX5+fn9/f4CAgIGBgYODg4SEhIaGhoiIiIqKioyMjI+Pj5CQkJGRkZKSkpOTk5SUlJaWlpiYmJqampycnJ2dnZ+fn6CgoKGhoaKioqOjo6SkpKWlpaWlpaampqampqenp6enp6ioqKioqKioqKmpqampqampqampqaqqqqqqqqurq6ysrKysrK2tra6urq+vr6+vr7CwsLGxsbKysrOzs7W1tbi4uLu7u7+/v8LCwsPDw8XFxcbGxsfHx8jIyMnJycnJycrKyszMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tTU1NXV1dbW1tfX19jY2Nra2tvb293d3d/f3+Dg4OLi4uPj4+Pj4+Tk5OXl5ebm5ujo6Onp6erq6uvr6+zs7O3t7e/v7/Hx8fPz8/X19ff39/j4+Pn5+fr6+vr6+vv7+/v7+/v7+/z8/Pz8/P39/f7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v///wj+AP8JHEiwoMGDCBMqXMiw4cBqzhxKLCgthgEDJo5NlIhOxcWLIchtbKjr48dVIwue6wUs3cBbJi+iTCnw2IwRI5BoEygugkkH1wieGyfP4bmbOEeUeenTgINOBLEpU9asKMNeSZOeG3itUqWgA+Utm6pMXEOsWWWEW8hOGtlxDdMhyWqnYbhmyqrlc6itzIgTdrY2lKdu5DmXNBMrXpxSHSQlRhCVS5zOGjZ3By8B2QxED810u3Dh6sXOYBnOQIjsHDlNtGhrplELgc3aNS7aBCehVkMTXWhcvAoXRMdoCBA73hKfo1ZN+EF16BhLn0797CNK0CR2ixZPYi0w4MtrAGsYyysmbg3ZvQEPHhLDbl692moYjz14QAyjxa80k6Ej+7kQxMwllRhDUDyYxKeMQ+UwAh4n3QnEjB4U6jEMQdzYssqCE51TGkGaVKhHJdURVIqIm5Q4EDUiPqPiQNi8sko1L9Zo443/BAQAIfkECQQA/wAsAAAAACYAJgCHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLi4uLy8vMDAwMTExMjIyNDQ0NTU1NjY2Nzc3ODg4Ojo6PDw8PT09Pz8/QEBAQkJCQ0NDRERERUVFRkZGR0dHSEhISEhISUlJSUlJSkpKSkpKSkpKS0tLTExMTExMTU1NTk5OT09PUVFRUlJSVFRUVlZWV1dXWVlZWlpaXFxcXl5eYGBgYmJiZGRkZWVlZWVlZWVlZmZmZmZmZmZmZmZmZmZmZmZmZmZmZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnaWlpa2trbW1tb29vcXFxcnJydHR0dXV1d3d3eHh4enp6e3t7fHx8fX19fn5+f39/gICAgICAgYGBgoKCg4ODhISEhISEhISEhISEhYWFhYWFhoaGhoaGh4eHh4eHiIiIiYmJiYmJioqKjIyMjY2Njo6OkJCQkZGRk5OTlZWVl5eXmpqam5ubnJycnZ2dnp6en5+foKCgoaGhoqKio6OjpKSkpqamqKioqqqqrKysra2tr6+vsLCwsbGxsrKys7OztLS0tbW1tbW1tra2tra2t7e3t7e3uLi4uLi4uLi4ubm5ubm5ubm5ubm5urq6urq6u7u7vb29wMDAw8PDxcXFx8fHycnJysrKy8vLzMzMzs7Oz8/P0dHR1NTU1tbW2dnZ2tra29vb3Nzc3d3d3t7e39/f4ODg4eHh4uLi5eXl5+fn6Ojo6urq7Ozs7e3t7u7u7u7u7+/v7+/v7+/v8PDw8PDw8fHx8vLy8/Pz9fX19vb2+Pj4+fn5+vr6+vr6+/v7/Pz8/Pz8/f39/f39/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+////CP4A/wkcSLCgwYMIEypcyLDhwGnNHEosOI1HhAg0nk2UqM7iRYzqNjYc9vHjLpEF0S1zRvBYyYuxUA5MFuTFCyzcBJIjURJEzoHr0OVziK6mzRd1Bg4DcREEMILcnj2rNpThsqM2aRDExorVNoL5okl9Zq5hMqwvmqxbKO/a2HMNzdXBeqlhuWjQsknkJoeGjkrwHOZrJ7Ow4cOIJ8IzxcUKpnKF4WnbVrVgqiWYlwCS6Y4YMGDJKg+0k3lJFGwosX3+rLdgndJPrqG8thpYa4KlSuuR2W7YZ2OBC67jRCXKom+F3V3DFjyx8+fQRS4bZUq2w3DYRC/0taY7HOsLgWiFCoUqnMNA3buDYhhu/PhgDuekX1N34TX3oX453DQf/sBnp5gS0UD5oOIeNQ6dowkccaBCUDWGRGjIgAKFE8wvCE6kjjwFqSKhIaZEN9AsH64iokDaLBLhItWcKFA2s8wCnos01hhdQAAh+QQJBAD/ACwAAAAAJgAmAIcAAAAAAAAAAAABAQECAgICAgIDAwMEBAQFBQUFBQUHBwcICAgJCQkLCwsNDQ0PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk5OTk6Ojo7Ozs8PDw9PT0/Pz9AQEBBQUFCQkJDQ0NERERGRkZISEhJSUlLS0tMTExOTk5PT09QUFBRUVFRUVFSUlJTU1NUVFRUVFRVVVVVVVVVVVVWVlZWVlZXV1dYWFhZWVlaWlpbW1tdXV1eXl5gYGBhYWFjY2NkZGRmZmZoaGhqampsbGxubm5wcHBxcXFxcXFxcXFycnJycnJycnJycnJycnJycnJycnJzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N0dHR2dnZ4eHh6enp8fHx+fn6AgICCgoKDg4OFhYWGhoaHh4eIiIiIiIiJiYmKioqLi4uMjIyOjo6Pj4+Pj4+QkJCRkZGRkZGRkZGRkZGSkpKSkpKTk5OTk5OUlJSUlJSVlZWWlpaWlpaXl5eZmZmampqbm5udnZ2enp6goKCioqKkpKSnp6eoqKipqamqqqqrq6usrKytra2urq6vr6+wsLCxsbGysrKzs7O1tbW3t7e5ubm7u7u8vLy+vr6/v7/AwMDCwsLDw8PDw8PExMTFxcXFxcXGxsbGxsbHx8fHx8fIyMjIyMjIyMjJycnKysrLy8vMzMzOzs7Pz8/Q0NDS0tLT09PU1NTV1dXW1tbX19fZ2dnb29vd3d3g4ODj4+Pl5eXp6ens7Ozt7e3u7u7w8PDx8fHx8fHy8vLz8/P09PT29vb39/f4+Pj5+fn6+vr7+/v8/Pz8/Pz8/Pz8/Pz9/f39/f39/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7///8I/gD/CRxIsKDBgwgTKlzIsOFAb94cSizITUuHDkqyTZz45OJFI+s2NpTm0WMykQaxcSM4reTFZgO7mXoUTSK2LDp07AEncJ2OkjXKCSxGIUCAA4Yc4sypA9BAaDUuulA2EIRRoweWMcTGNGcRguVw4RIqMNrVq30YXuuqI8vCZmeNDmKoTlDXTwxxnHWgkSE4P0WSdHKnFoZRCrNQOlznrBg5xZAjSxZoi88dU+ogxwMnLt/BXVdCX9mk2B20ZMmqeS5YSPQVMTxFckON+ptBQq7DdEM5m3ZsgrdcQyp9Opm01QVlnQkj6hxkd92+IZ9Mvbp1yNZUudom0dxKic3ubYjnw50htFasbqVzaEm8+FQMv81iRd+aQ0Lu75BiyI0+fa0NwZIfVQNxUwst2hAUDy/+fccQOqf48YcuBHEzySOPTNKXQOlYs4xtkuWCIYa0XEeQLiM+YouJBW6C4SQOssiNLrnEyOKNOOb4T0AAOwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"></div></div>');
        return this;
    };

    $.fn.RLoadingModuleComplete = function () {
        this.find('._module-loading').hide().remove();
    };

}(jQuery));

$.fn.clickOff = function (callback, selfDestroy) {
    var clicked = false;
    var parent = this;
    var destroy = selfDestroy || true;

    parent.click(function () {
        clicked = true;
    });

    $(document).click(function (event) {
        if (!clicked) {
            callback(parent, event);
        }
        if (destroy) {
            //parent.clickOff = function() {};
            //parent.off("click");
            //$(document).off("click");
            //parent.off("clickOff");
        };
        clicked = false;
    });
};

R.DoLogout = function () {
    R.Post({
        module: "auth",
        ashx: 'modulerequest.api',
        action: "logout",
        success: function (res) {
            if (res.Success) {
                location.reload();
            } else {
                R.MessageBox(res.Message);
                location.reload();
            }
        }
    });
};
(function ($) {

    $.fn.RModuleBlock = function () {
        $(this).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
                'z-index': 998
                //'box-shadow': '0 0 0 1px #ddd'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

    };

    $.fn.RModuleUnBlock = function () {
        $(this).unblock();
    };

}(jQuery));

(function ($) {
    $.fn.tshift = function () {
        var start = 0;
        var checkboxes = $("#" + this.attr('id') + " :checkbox");
        checkboxes.on("click", function (event) {
            if (this.checked) {
                if (event.shiftKey) {
                    end = checkboxes.index(this);
                    if (end < start) {
                        end = start;
                        start = checkboxes.index(this);
                    }
                    checkboxes.each(function (index) {
                        if (index >= start && index < end) {
                            this.checked = true;
                        }
                    });
                }
                start = checkboxes.index(this);
            }
        });
        return this;
    };
})(jQuery);
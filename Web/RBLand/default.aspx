﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/RBLand.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="RBLand._default" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="RBLand.Core.Helper" %>
<%@ Register Src="~/Pages/Controls/Partner.ascx" TagPrefix="uc1" TagName="Partner" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadCph">
    <meta property="fb:app_id" content="1426300910760607" />
    <meta property="og:url" content="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<%=UIHelper.GetConfigByName("MetaTitle") %>" />
    <meta property="og:description" content="<%=UIHelper.GetConfigByName("MetaDescription") %>" />
    <meta property="og:image" content="https://rbland.vn/Uploads/<%=UIHelper.GetConfigByName("PageImage") %>" />
    <link href="/Themes/js/breaking-news-ticker/breaking-news-ticker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="Main">
    <div style="position: relative">
        <h1 style="position: absolute; top: -999px"><%=UIHelper.GetConfigByName("H1HomePage") %></h1>
    </div>
    <section class="slide">
        <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <%
                    var objs = ConfigBo.AdvGetByType((int)EnumSlide.SliderHomePage, true).ToList();
                    foreach (var obj in objs)
                    {
                %>
                <div class="swiper-slide">
                    <a title="<%=obj.Name %>" href="<%=obj.Url %>">
                        <img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-fluid " alt="<%=obj.Name %>" /></a>
                </div>
                <% } %>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>

        </div>
    </section>

    <section class="block-6 my-2 products">
        <div class="container">
            <div class="row">
                <%
                    var productZones = ZoneBo.GetAllZone((int)ZoneType.Product).Where(it => it.Status == (int)ZoneStatus.Publish).OrderBy(it => it.SortOrder);
                    foreach (var zone in productZones)
                    {

                %>
                <div class="col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item wow bounceIn">
                        <i class='<%=zone.Banner %>'></i>
                        <p>
                            <h2><a href="/sp/<%=zone.ShortUrl %>.htm"><%=zone.Name %></a></h2>
                        </p>
                    </div>
                </div>
                <%    
                    } %>
            </div>
        </div>
    </section>

    <section class="news mb-4">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <%--<div class="border-new">
                        <a href="" class="color-f7662c">Tin mới</a>
                        <span class="color-f7662c">23-04-2019</span>
                        <a href="">Dự án Grand World chính thức ra mắt nhà đầu tư ...</a>
                    </div>--%>
                    <div class="breaking-news-ticker border-new" id="newsTicker5">
                        <div class="bn-label  color-f7662c">Tin mới </div>
                        <div class="bn-news">
                            <ul>
                                <%
                                    int totalRow = 0;
                                    var newsFeeds = NewsBo.SearchByShortUrl("tin-tuc", 1, 5, ref totalRow);
                                    foreach (var item in newsFeeds)
                                    {
                                %>
                                <li><a href="<%=item.Url %>-<%=item.Id %>.htm"><%=item.Title %></a></li>
                                <%    
                                    } %>
                            </ul>
                        </div>
                        <div class="bn-controls">
                            <button><span class="bn-arrow bn-prev"></span></button>
                            <button><span class="bn-action"></span></button>
                            <button><span class="bn-arrow bn-next"></span></button>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="border-new">
                        <a href="/tin-tuc.htm" class="color-f7662c mr-auto">Tin tức</a>
                        <a href="/tin-tuc.htm" style="color: #fff" class="btn  btn-more " role="button">Đọc thêm<i class="fas fa-chevron-right ml-2"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="block-5 py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center mb-4">Hãy để RB Land hỗ trợ bạn ngay hôm nay !</h3>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="support wow fadeIn  slow">
                        <%
                            var supports = ConfigBo.AdvGetByType((int)EnumSlide.Support, true).ToList();
                            foreach (var obj in supports)
                            {
                        %>
                        <div class="item">
                            <div class="bg-345583">
                                <div class="image mb-3">
                                    <a title="<%=obj.Name %>" href="<%=obj.Url %>">
                                        <img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-fluid" alt="<%=obj.Name %>" /></a>
                                </div>
                                <a title="<%=obj.Name %>" href="<%=obj.Url %>"><%=obj.Name %></a>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="review my-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center mb-5">Review và phân tích dự án chuyên sâu</h3>
                </div>
                <%
                    var hotzones = ZoneBo.GetAllZone((int)ZoneType.News).Where(it => it.Status == (int)ZoneStatus.Publish && it.IsShowHomePage).OrderBy(it => it.SortOrder).Take(3);
                    foreach (var zone in hotzones)
                    {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="item wow bounceInRight ">
                        <div class="heading px-2 text-center">
                            <h4><a title="<%=zone.Name %>" href="/<%=zone.ShortUrl %>"><%=zone.Name %></a></h4>
                            <hr />
                        </div>
                        <div class="detail">
                            <ul>
                                <% int totalRows = 0;
                                    var news = NewsBo.SearchByShortUrl(zone.ShortUrl, 1, 5, ref totalRows);
                                    foreach (var n in news)
                                    {
                                %>
                                <li>
                                    <a title="<%=n.Title %>" href="/<%=zone.ShortUrl %>/<%=n.Url %>.<%=n.Id %>.htm"><%=n.Title %></a>
                                </li>
                                <%} %>
                                <%--<li>Đánh giá chuyên sâu về tiềm năng của bất động sản liền kề Casio tại các thị trường Singapore, Macau và Lasvegas</li>--%>
                            </ul>
                        </div>
                        <div class="text-center">
                            <a class="btn  btn-more" href="/<%=zone.ShortUrl %>">Xem thêm<i class="fas fa-chevron-right ml-2"></i></a>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </section>

    <section class="why py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="text-center mb-5">Tại sao lựa chọn chúng tôi
                    </h3>
                </div>
                <%
                    var choose = ConfigBo.AdvGetByType((int)EnumSlide.ChooseUs, true).ToList();
                    foreach (var obj in choose)
                    {
                %>
                <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a title="<%=obj.Name %>" href="<%=obj.Url %>">
                                <img src="/uploads/<%=obj.Thumb %>?v=1.0" class="img-fluid " alt="<%=obj.Name %>" /></a>
                        </div>
                        <div class="text">
                            <h4><a title="<%=obj.Name %>" href="<%=obj.Url %>"><%=obj.Name %></a></h4>
                            <p><%=obj.Content %></p>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </section>
    <uc1:partner runat="server" id="Partner" />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="Footer">
    <script src="/Themes/js/breaking-news-ticker/breaking-news-ticker.min.js"></script>
    <script>
        $(function () {
            $('#newsTicker5').breakingNews({
                effect: 'typography'
            });

        })
    </script>

</asp:Content>


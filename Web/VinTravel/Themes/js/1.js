function sliderGalleryThumb() {
    var sliderGalleryThumb = new Swiper('.slide-tour-thumb .thumb', {
        slidesPerView: 4,
        spaceBetween: 15,
        autoPlay: true,
        loop: true,
        centeredSlides: true,
        slideToClickedSlide: true,
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            567: {
                slidesPerView: 3,
            },
            767: {
                slidesPerView: 4,
                spaceBetween: 15,
            },
            991: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
        }


    });
    var sliderGallery = new Swiper('.slide-tour .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        autoPlay: true,
        loop: true,
        thumbs: {
            swiper: sliderGalleryThumb
        }
    });
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="VinTravel._default" %>

<%@ Register Src="~/Pages/Controls/Banner.ascx" TagPrefix="uc1" TagName="Banner" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <uc1:Banner runat="server" ID="Banner" />

    <section class="list-summer">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name ">
                        <div class="left">
                            <h3 class="tit">Điểm đến yêu thích trong nước

                            </h3>
                            <div>Lên rừng xuống biển. Trọn vẹn Việt Nam</div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item-home mb-3">
                        <img src="/Themes/imgs/home-list-1-1.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Nghỉ hè</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item-home mb-3">
                        <img src="/Themes/imgs/home-list-1-2.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Nghỉ hè</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item-home mb-3">
                        <img src="/Themes/imgs/home-list-1-3.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Nghỉ hè</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item-home mb-3">
                        <img src="/Themes/imgs/home-list-1-4.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Nghỉ hè</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <section class="list-location-suggest">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">Du lịch ngay! Vì bạn xứng đáng

                            </h3>
                            <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row no-gutters mb-3">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12 align-self-center">
                    <div class="px-2">
                        <div class="item-home mb-3 ">
                            <img src="/Themes/imgs/home-list-2-1.jpg" alt="" class="w-100" />
                            <div class="ovelay">
                            </div>
                            <div class="category">
                                <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                <div class="bot">Du lịch đi, chần chừ chi!</div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-2.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-3.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 align-self-center  px-2">
                    <div class="item-home ">
                        <img src="/Themes/imgs/home-list-2-4.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-gutters flex-row-reverse">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12 align-self-center">
                    <div class="px-2">
                        <div class="item-home mb-3 ">
                            <img src="/Themes/imgs/home-list-2-1.jpg" alt="" class="w-100" />
                            <div class="ovelay">
                            </div>
                            <div class="category">
                                <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                <div class="bot">Du lịch đi, chần chừ chi!</div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-2.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-3.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 align-self-center  px-2">
                    <div class="item-home ">
                        <img src="/Themes/imgs/home-list-2-5.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="list-uu-dai">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">Ưu đãi tốt nhất hôm nay

                            </h3>
                            <div>Nhanh tay đặt ngay. Để mai sẽ lỡ !</div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="item-1">
                        <a href="" title="" class="">
                            <img class="w-100" src="/Themes/imgs/home-list-3-1.jpg" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="item-1">
                        <a href="" title="" class="">
                            <img class="w-100" src="/Themes/imgs/home-list-3-2.jpg" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="item-1">
                        <a href="" title="" class="">
                            <img class="w-100" src="/Themes/imgs/home-list-3-3.jpg" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="item-1">
                        <a href="" title="" class="">
                            <img class="w-100" src="/Themes/imgs/home-list-3-4.jpg" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="popular-option">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name ">
                        <div class="left">
                            <h3 class="tit">Lựa chọn phổ biến

                            </h3>
                            <div>Thêm trải nghiệm, thêm nhiều niềm vui cùng các chủ nhà của chúng tôi</div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular1.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular2.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular3.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular4.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular3.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular2.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular1.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Themes/imgs/popular3.jpg" class="w-100" alt="" /></a>
                        </div>
                        <div class="area">
                            Phúc Yên, Vĩnh Phúc
                        </div>
                        <div class="name">
                            <a href="" title="">HomeLead_Flamingo Đại Lải Tự DoanhVilla 02 Phòng Ngủ</a>
                        </div>
                        <div class="info-basic">
                            <div class="living-room">
                                1 Khách
                            </div>
                            <div class="bed-room">
                                2 Phòng ngủ
                            </div>
                            <div class="bath-room">
                                2 Phòng ngủ
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="list-location-suggest mb-5">
        <div class="container">
            <div class="row ">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <section class="ss-name px-2">
                        <div class="left">
                            <h3 class="tit">Điểm đến yêu thích nước ngoài

                            </h3>
                            <div>Bao la thế giới. Bốn bể là nhà</div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row no-gutters mb-3">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12 align-self-center">
                    <div class="px-2">
                        <div class="item-home mb-3 ">
                            <img src="/Themes/imgs/home-list-2-1.jpg" alt="" class="w-100" />
                            <div class="ovelay">
                            </div>
                            <div class="category">
                                <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                <div class="bot">Du lịch đi, chần chừ chi!</div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-2.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12  px-2">
                            <div class="item-home">
                                <img src="/Themes/imgs/home-list-2-3.jpg" alt="" class="w-100" />
                                <div class="ovelay">
                                </div>
                                <div class="category">
                                    <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                                    <div class="bot">Du lịch đi, chần chừ chi!</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 align-self-center  px-2">
                    <div class="item-home mb-3">
                        <img src="/Themes/imgs/home-list-2-3.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>
                        </div>
                    </div>
                    <div class="item-home ">
                        <img src="/Themes/imgs/home-list-2-3.jpg" alt="" class="w-100" />
                        <div class="ovelay">
                        </div>
                        <div class="category">
                            <h3 class="top"><a href="" title="">Phú Quốc</a></h3>
                            <div class="bot">Du lịch đi, chần chừ chi!</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="res">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-3 col-sm-4 col-12 text-center align-self-center">
                    <img src="/Themes/imgs/mail-res.png" class="img-fluid" />
                </div>
                <div class="col-xl-8 col-lg-8 col-md-9 col-sm-8 col-12 align-self-center">
                    <div class="form-group form-res">
                        <h3 class="tit">Đăng ký bản tin để thấy được các Ưu Đãi Bí Mật</h3>
                        <div class="tip">Giá giảm hẳn ngay khi bạn đăng nhập!</div>
                        <div class="row form-res">
                            <div class="col-8">
                                <input type="text" class="form-control" placeholder="Nhập địa chỉ email">
                            </div>
                            <div class="col-4 pl-0">
                                <button type="button" class="btn btn-res">Tôi đăng ký</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="suggest py-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="heading mb-4">
                        <div class="top">Các điểm đến được chúng tôi ưa thích</div>
                        <div class="bot">Bao la thế giới. Bốn bể là nhà</div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6">
                    <div class="item">
                        <div class="place">
                            Vịnh Hạ Long
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Hồ Chí Minh
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Đà Lạt
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                    <div class="item">
                        <div class="place">
                            Bình Dương
                        </div>
                        <div class="slot">
                            1,557 chỗ nghỉ
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

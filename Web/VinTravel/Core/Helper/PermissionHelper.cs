﻿using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.Entity.Base.Security;

namespace VinTravel.Core.Helper
{
    public static class PermissionHelper
    {
        public static bool HasEditorPermission()
        {
            string userName = UIHelper.GetCurrentUserName();
            if (PolicyProviderManager.Provider.IsLogin())
            {
                return HasPermission(EnumPermission.ArticleAdmin, userName) || HasPermission(EnumPermission.ArticleEditor, userName) || PolicyProviderManager.Provider.IsFullPermission();
            }
            return false;
        }
        public static bool HasAdminPermission()
        {
            string userName = UIHelper.GetCurrentUserName();
            if (PolicyProviderManager.Provider.IsLogin())
            {
                return HasPermission(EnumPermission.ArticleAdmin, userName) || PolicyProviderManager.Provider.IsFullPermission();
            }
            return false;
        }

        public static bool HasPermission()
        {
            return HasEditorPermission();
        }
        public static bool HasPermission(EnumPermission permission, string userName)
        {
            return Role.HasRoleOnPermission(permission, userName) || PolicyProviderManager.Provider.IsFullPermission();
        }

        /// <summary>
        /// Chế độ hiện tại của user là quản trị hay user thường
        /// </summary>
        /// <returns></returns>
        public static ViewMode CurrentMode()
        {
            return (HttpContext.Current.Session[SessionCurrentMode] == null ? ViewMode.Normal : ViewMode.Admin);
        }

        public static string CurrentModeText()
        {
            return (HttpContext.Current.Session[SessionCurrentMode] == null ? "<i class='fa fa-star-o'></i>Thành viên thường" : "<i class='fa fa-star'></i>Quản trị viên");
        }

        public enum ViewMode : int
        {
            Normal = 1,
            Admin = 2
        }

        public const string SessionCurrentMode = "CurrentMode";
    }
}
﻿using System.Configuration;
using Mi.Common.ChannelConfig;

namespace Mi.VinTravel.Core.Helper
{
    public class Constants
    {
        public enum ErrorCode
        {
            Error = 1,
            Fatal = 2,
            Invalid = 3,
            Ok = 0
        }

        // ConnectionString
        public const string CMS_CONNECTION_STRING = "BookmarkConnectionStringName";
        public const string CMS_CONNECTION_DECRYPT_KEY = "DecryptKeyBookmarkConnectionString";

        // API Media 
        public const string CMS_MEDIA_SERVICE_URL = "CmsMediaWCFServiceUrl";


        public const string DOWNLOAD_DIR = "DownloadDir";

        public static string DownloadDir
        {
            get { return ConfigurationSettings.AppSettings["DownloadDir"]; }
        }

        #region SESSION

        public const string ASP_NET_SESSION_ID = "ASP.NET_SessionId";
        public const string SESSION_USERNAME = "SESSION_USERNAME_LOGIN";
        public const string AUTH_COOKIE_NAME = "_aspx_auth_";
        public const string AUTH_COOKIE_EXPIRE = "AUTH_COOKIE_EXPIRE";
        public const string AUTH_DECRYPT_ACCOUNT = "AUTH_DECRYPT_ACCOUNT";

        #endregion


        /*##############################################*/
        /*# Hostings */
        /*##############################################*/
        public const string MASK_ONLINE_HOST = "HOST_MASK_ONLINE";
        public const string HOST_BOOKMARK = "HOST_BOOKMARK";
        public const string HOST_STATIC = "HOST_STATIC";
        public const string HOST_STATIC_NEWS_OS = "HOST_STATIC_NEWS_OS";
        public const string HOST_CMS_AVATAR = "HOST_CMS_AVATAR";
        public const string HOST_CMS_THUMB = "FILE_MANAGER_HTTPDOWNLOAD";
        public const string HOST_CMS_THUMB_FORMAT = "FILE_MANAGER_HTTPDOWNLOAD_THUMB";

        public const string TIME_TO_CHANGE_CMS = "TIME_TO_CHANGE_CMS";
        public const string HOST_CMS_OLD_THUMB = "FILE_MANAGER_OLD_HTTPDOWNLOAD";
        public const string HOST_CMS_OLD_THUMB_FORMAT = "FILE_MANAGER_OLD_HTTPDOWNLOAD_THUMB";

        public const string FRONT_END_URL = "FRONT_END_URL";
        public const string FRONT_END_LOCAL_URL = "FRONT_END_LOCAL_URL";
        public const string FRONT_END_ZONE_REGEX = "FRONT_END_ZONE_REGEX";
        public const string FRONT_END_DETAIL_REGEX = "FRONT_END_DETAIL_REGEX";
        public const string FRONT_END_REGEX_GROUP_INDEX_FOR_GET_NEWSID = "FRONT_END_REGEX_GROUP_INDEX_FOR_GET_NEWSID";
        public const string FRONT_END_THREAD_REGEX = "FRONT_END_THREAD_REGEX";
        public const string FRONT_END_REGEX_GROUP_INDEX_FOR_GET_THREADID = "FRONT_END_REGEX_GROUP_INDEX_FOR_GET_THREADID";
        public const string LEVEL1_MIN_VIEWCOUNT = "LEVEL1_MIN_VIEWCOUNT";
        public const string LEVEL2_MIN_VIEWCOUNT = "LEVEL2_MIN_VIEWCOUNT";
        public const string LEVEL3_MIN_VIEWCOUNT = "LEVEL3_MIN_VIEWCOUNT";

        /*##############################################*/
        /*# Settings */
        /*##############################################*/
        public const string EnabledOTP = "EnabledOTP";
        public const string AUTH_RIJNDEALKEY = "4022753ee9daa45e8ad5dec533b3e3d70";//32bit
        public const string CONNECTION_RIJNDEALKEY = "ab353651c4d14e5ba9e80180508948be";//32bit

        public const string SYNC_MOBILE_CHANNELID = "SYNC_MOBILE_CHANNELID";

        public const string CACHE_JS_CSS_DURATION = "CACHE_JS_CSS_DURATION";
        public const string CACHE_JS_CSS_EXPIRED = "CACHE_JS_CSS_EXPIRED";
        public const string CSS_WIDGET = "CSS_WIDGET";
        public const string JS_WIDGET = "JS_WIDGET";
        public const string JS_BOOKMARK = "JS_BOOKMARK";
        public const string InUsingTagViaApi = "InUsingTagViaApi";

        public const string CACHE_TEMPLATE_DURATION = "CACHE_TEMPLATE_DURATION";

        public const string CMS_VERSION = "CMS_VERSION";
        public const string CMS_BM_ELEMENT_WIDGET = "CMS_BM_ELEMENT_WIDGET";
        public const string CMS_BM_ELEMENT_AUTH_COOKIE = "_aspx_auth_";

        public const string TAG_AUTO_COMPLETE_ON_CONTENT = "TAG_AUTO_COMPLETE_ON_CONTENT";
        public const string UrlFormatThread = "UrlFormatThread";
        public const string UrlFormatTag = "UrlFormatTag";
        public const string UrlFormatNews = "UrlFormatNews";
        public const string NewsPositionAllowSchedule = "NewsPositionAllowSchedule";

        public const string CMS_CLIENT_DEBUG_MODE = "CMS_CLIENT_DEBUG_MODE";
        public const string CMS_SERVER_DEBUG_MODE = "CMS_SERVER_DEBUG_MODE";
        public const string EditorVersion = "EditorVersion";
        public const string UsingMinifiedEditor = "UsingMinifiedEditor";
        public const string ConfirmOnDoNewsAction = "ConfirmOnDoNewsAction";
        public const string NewsPositionSessionExprireMinutes = "NewsPositionSessionExprireMinutes";

        public const string ALLOW_MULTI_AVATAR = "AllowMultiAvatar";
        public const string QUICKNEWS_IS_RE_UPLOAD_PHOTO_ON_RECEIVE = "QUICKNEWS_IS_RE_UPLOAD_PHOTO_ON_RECEIVE";

        // Actions params

        public const string SECRET_KEY = "SECRET_KEY";
        public const string SURFIX_LOGIN = "SURFIX_LOGIN";


        public const string EXTERNAL_REQUEST_SECRET_KEY = "EXTERNAL_REQUEST_SECRET_KEY";

        public const string IMAGE_MAX_WIDTH = "IMAGE_MAX_WIDTH";
        public const string KeyThumbWidth = "keyThumbWidth";

        public const string FRONT_END_CONFIG_GROUP_KEY = "FrontEndConfig";
        public const string FRONT_END_CONFIG_EXT_FILEUPLOAD_KEY = "ExtensionFileUpload";
        public const string FRONT_END_CONFIG_HOST_ADDRESS_KEY = "HOST";

        public const string ROOT_DIRECTORY = "ROOT_DIRECTORY";
        public const string CMS_SENDMAIL_FROM_SENDER = "FromMailSender";
        public const string CMS_SENDMAIL_FROM_CONFIG = "FromMailConfig";
        public const string CMS_SENDMAIL_REPLY_MAIL_TITLE_CONFIG = "ReplyMailTitle";
        public const string CMS_SENDMAIL_FORWARD_MAIL_FROM_CONFIG = "ForwardMailTitle";

        public const string QUICKNEWS_SHOW_AUTHOR_INFO_IN_BODY = "QUICKNEWS_SHOW_AUTHOR_INFO_IN_BODY";

        public enum ConfigType
        {
            PLAIN_TEXT = 1,
            HTML_TEXT = 2,
            NUMERIC = 3,
            DATETIME = 4,
            IMAGE = 5,
            VIDEO = 6,
            SELECT_LIST = 7
        }
        //Live preview
        public const string LIVEPREVIEW_TEMPLATE_ACTIVE = "LIVEPREVIEW_TEMPLATE_ACTIVE";
        public const string LIVEPREVIEW_TEMPLATE_NEWS_URL = "LIVEPREVIEW_TEMPLATE_NEWS_URL";
        public const string LIVEPREVIEW_TEMPLATE_NEWS_TYPE_PHOTO_URL = "LIVEPREVIEW_TEMPLATE_NEWS_TYPE_PHOTO_URL";
        // Email 
        public const string EMAIL_SENDER = "EMAIL_SENDER";
        public const string EMAIL_PASSWORD = "EMAIL_PASSWORD";
        public const string EMAIL_PORT = "EMAIL_PORT";
        //device
        public const string Android = "ANDROID";        
        public const string IOS = "IOS";        
        public const string Web = "WEB";        
        public const string Winphone = "WINPHONE";        



        #region Message

        public const string MESG_CAN_NOT_FOUND_ACTION = "Hành động của bạn không hợp lệ.";
        public const string MESG_LOAD_TEMPLATE_FAIL = "Không thể tải được template này.";
        public const string MESG_LOGIN_FAIL = "Đăng nhập không thành công";
        public const string MESG_TIMEOUT_SESSION = "Bạn chưa đăng nhập hoặc đã hết phiên làm việc.";
        public const string MESG_UPDATE_SUCCESS = "Cập nhật thành công.";
        public const string MESG_UPDATE_ERROR = "Có lỗi xảy ra trong quá trình xử lý. Hãy thử lại.";
        public const string MESG_RETPWD_SUCCESS = "Mật khẩu đã được thay đổi.";
        public const string MESG_SUCCESS = "Success.";
        public const string MESG_ERROR = "Error.";

        #endregion


        #region News [QuangNV added]
        public const int NEW_STATUS_TEMPORATY = 1;
        public const int NEW_STATUS_TEMPORATY_WAITFOREDIT = 2;
        public const int NEW_STATUS_WAITFORPUBLISH = 5;
        public const int NEW_STATUS_RECEIVEDTOPUBLISH = 6;
        public const int NEW_STATUS_PUBLISHED = 8;
        public const int NEW_STATUS_RETURN_TO_REPORTER = 4;
        public const int NEW_STATUS_RETURN_TO_EDITOR = 7;
        public const int NEW_STATUS_MOVE_TO_TRASH = 9;
        public const int NEW_STATUS_TAKEN_DOWN = 10;

        public const int NEW_ICON_PHOTO = 3;
        public const int NEW_ICON_VIDEO = 2;
        public const int NEW_ICON_PHOTO_VIDEO = 7;


        #endregion

        #region Huu add for mobile
        public const string ALLOW_FAKE_USER_AGENT = "ALLOW_FAKE_USER_AGENT";
        public const string API_VIDEO_GET_EMBED_URL_BYKEY = "API_VIDEO_GET_EMBED_URL_BYKEY";
        public const string WEB_HOST_STATIC = "WEB_HOST_STATIC";
        public const string MOBILE_HOST = "MOBILE_HOST";
        public static string MOBILE_NO_IMAGE
        {
            get
            {
                return CmsChannelConfiguration.GetAppSetting(MOBILE_HOST).TrimEnd('/') + "/Themes/Images/no-image.png";
            }
        }
        public static string MOBILE_DEFAULT_USER_AVATAR
        {
            get { return CmsChannelConfiguration.GetAppSetting(MOBILE_HOST) + "/Themes/Images/ava-default-100x100.png"; }
        }

        #endregion
        
    }
}

﻿R.Register = {
    Init: function () {
        R.Register.RegisterEvent();
    },
    RegisterEvent: function () {
        $('.register input').off('keypress').on('keypress', function () {
            $(this).removeClass('error')
        });
        $('.register input[type=text]').off('keypress').on('keypress', function (e) {
            if (e.keyCode === 13) {
                R.Register.DoRegister();
            }

        });
        var delay = null;
        //$('#username').change(function () {

        //    var name = $('#username').val();
        //    if (name.trim().length == 0) {
        //        return;
        //    }
        //    if (delay != null) clearTimeout(delay);
        //    delay = setTimeout(function () {
        //        var data = { name: name }
        //        R.Register.CheckXuser(data);
        //    }, 500);
        //});

        $('#passwordconfirm').blur(function () {
            if ($('#password').val() != $('#passwordconfirm').val()) {
                $('#passwordconfirm').addClass('error').text('');
                alert('Mật khẩu xác nhận không đúng !');

            } else {
                $('#passwordconfirm').removeClass('error');
            }
        });
        //$('#btn-facebook').off('click').on('click', function() {
        //    alert('Comming soon :D')
        //});
        $('#btn-dangky').off('click').on('click', function () {

            R.Register.DoRegister();
        });
    },

    DoRegister: function () {

        var user = $('#username').val();
        var password = $('#password').val();
        if (user.length < 6 || user.length > 15) {
            $('#username').addClass('error')
            alert('Tên đăng nhập từ 6-15 ký tự, chỉ bao gồm chữ và số.');
            return;
        }
        if (password.length < 6 || password.length > 15) {
            $('#password').addClass('error')
            alert('Mật khẩu lớn hơn 6 và nhỏ hơn 30 ký tự !');

            return;
        }

        var data = {
            name: user,
            pass: $('#password').val(),
            guid: $('#captcha_image img').attr('src').split('guid=')[1],
            captcha: $('#capcha').val()
        }
        $('#register-main').RLoadingModule();
        R.Post({
            params: data,
            module: "authenticate",
            ashx: 'modulerequest.ashx',
            action: "register",
            success: function (res) {
                if (res.Success) {
                    var json = $.parseJSON(res.Data);
                    if (json.success) {
                       // var pass = CryptoJS.AES.encrypt(data.pass, 'hiephvdepzai123$#@^&^$');
                        //var encryptedPass = CryptoJS.AES.encrypt(data.pass, "hiephvdepzai123$#@^&^$");
                        // An example 128-bit key (16 bytes * 8 bits/byte = 128 bits)
                        var key = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
                        // Convert text to bytes
                        var textBytes = aesjs.utils.utf8.toBytes(data.pass);
                        // The counter is optional, and if omitted will begin at 1
                        var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(10));
                        var encryptedBytes = aesCtr.encrypt(textBytes);

                        // To print or store the binary data, you may convert it to hex
                        var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
                       // var urlEncode = encodeURIComponent('/dang-ky-thanh-cong.htm?username=' + data.name + '&password=' + encryptedHex + '&isRegister=true');
                        window.location.href = '/dang-ky-thanh-cong.htm?username=' + data.name + '&password=' + encryptedHex + '&isRegister=true';
                    } else {
                        R.Register.GetCapcha();
                        alert(json.message);
                       
                    }

                } else {
                     R.Register.GetCapcha();
                }
                $('#register-main').RLoadingModuleComplete();
            }
        });
    },
    CheckXuser: function (data) {
        R.Post({
            params: data,
            module: "authenticate",
            ashx: 'modulerequest.ashx',
            action: "check_xuser",
            success: function (res) {
                if (res.Success) {
                    $('#username').addClass('error');
                    alert(res.Message);
                }
            }
        });
    },
    GetCapcha: function (data) {
        R.Post({
            params: data,
            module: "authenticate",
            ashx: 'modulerequest.ashx',
            action: "get_capcha",
            success: function (res) {
                if (res.Success) {
                    $('#captcha_image').html(res.Content);
                }
            }
        });
    }
};


var fbApiInit = false;
var InitOpenIdLogin = function () {
    // $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '265548353914142',
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });
        fbApiInit = true;
        FB.getLoginStatus(function (x) {
            //FOX: do nothing, just call to fix MAC error: not open login popup in first request
        });
    });


};

var IsGoogleLoginInit = false;

var DoLoginGoogle = function (callback) {
    if (!IsGoogleLoginInit) {
        gapi.auth.signIn({
            'callback': function () {
                gapi.client.load('oauth2', 'v2', function () {
                    gapi.client.oauth2.userinfo.get().execute(function (resp) {
                        // console.log(resp);

                        $('body').RLoadingModule();
                        R.Post({
                            module: "account",
                            params: resp,
                            ashx: 'modulerequest.ashx',
                            action: "gg_checklogin",
                            success: function (res) {
                                if (res.Success) {

                                    var p = R.GetURLParameter('returnUrl');
                                    if (p == '') {
                                        location.href = '/tai-khoan.htm';
                                    } else {

                                        window.location = p;
                                    }
                                } else {
                                    $.alert({
                                        title: 'Thông báo',
                                        content: res.Message
                                    });
                                }
                                $('body').RLoadingModuleComplete();

                            }
                        });
                    });
                });
            },
            'approvalprompt': 'force',
            // 'clientid': globalChannelConfig.channelGoogleClientId,
            'clientid': '91741564988-2dfgg7fda4f06kd0r0af91bjo68ndj2j.apps.googleusercontent.com',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
            'requestvisibleactions': 'http://schema.org/CommentAction http://schema.org/ReviewAction',
            'cookiepolicy': 'single_host_origin'
        });
        IsGoogleLoginInit = true;
    }
};

var DoLoginFacebook = function () {
  
    function reloginFacebook(accessToken) {
        FB.api('/me', function (me) {
            me.accessToken = accessToken;
            $('body').RLoadingModule();
            R.Post({
                module: "authenticate",
                params: {
                    token: me.accessToken,
                    fid: me.id
                },
                ashx: 'modulerequest.ashx',
                action: "fb_login",
                success: function (res) {
                    if (res.Success) {
                        var json = $.parseJSON(res.Data);

                        if (json.success) {
                            if (json.registered) {
                                document.location.href = 'http://game.bai1368.com?' + res.Content;
                            } else {
                                var p = R.GetURLParameter('returnUrl');
                                if (p == '') {
                                    location.href = '/dang-ky-thanh-cong.htm?' + res.Content;

                                } else {

                                    window.location = p;
                                }
                            }
                        } else {
                            alert(json.message)
                        }


                    } else {
                        alert(res.Message);

                    }
                    $('body').RLoadingModuleComplete();

                }
            });


        });
    }

    function fbEnsureInit(callback) {
        if (!fbApiInit) {
            setTimeout(function () {
                fbEnsureInit(callback);
            }, 50);
        } else {
            if (callback) {
                callback();
            }
        }
    }

    fbEnsureInit(function () {
        FB.getLoginStatus(function (r) {
            if (r.status === 'connected') {
                var accessToken = r.authResponse.accessToken;
                reloginFacebook(accessToken);
            } else {
                FB.login(function (response) {
                    if (response.status === 'connected') {
                        var accessToken = response.authResponse.accessToken;
                        reloginFacebook(accessToken);
                        return;
                    }
                });
            }
        });
    });
};


$(function () {
    R.Register.Init();
    InitOpenIdLogin();
});



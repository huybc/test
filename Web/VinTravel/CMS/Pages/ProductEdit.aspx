﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="ProductEdit.aspx.cs" Inherits="VinTravel.CMS.Pages.ProductEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Modules/Product/Styles/main.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">

    <!-- Page header -->
    <div class="page-header" id="product-edit">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Forms</span> - Validation</h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i><span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i><span>Schedule</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i>Home</a></li>
                <li><a href="form_validation.html">Forms</a></li>
                <li class="active">Validation</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i>Support</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Settings
								
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i>Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i>Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i>Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i>All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Form validation -->
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Thông tin sản phẩm
                            </legend>

                            <div class="collapse in" id="demo1">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Giá:</label>
                                            <input type="text" id="priceTxt" class="form-control" placeholder="0.00">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Đơn vị:</label>
                                            <input type="text" id="unitTxt" class="form-control" placeholder="chiếc,cái...">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" checked="checked">
                                            <b>Thuế VAT</b>(<i>Giá bao gồm VAT</i>)
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" checked="checked">
                                                    <b>Sản phẩm giảm giá</b>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>%</label>
                                            <input type="text" id="precentTxt" class="form-control" placeholder="ex: 10%">
                                        </div>
                                        <div class="col-md-8">
                                            <label>Giá giảm:</label>
                                            <input type="text" id="priceDiscountTxt" class="form-control" placeholder="Giá giảm">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label>Mã:</label>
                                    <input type="text" class="form-control" placeholder="SP:21218">
                                </div>
                                <div class="form-group">
                                    <label>Chuyên mục:</label>
                                    <select data-placeholder="Select your state" class="select">
                                        <option></option>
                                        <optgroup label="Alaskan/Hawaiian Time Zone">
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </optgroup>
                                        <optgroup label="Pacific Time Zone">
                                            <option value="CA">California</option>
                                            <option value="OR">Oregon</option>
                                            <option value="WA">Washington</option>
                                        </optgroup>
                                        <optgroup label="Mountain Time Zone">
                                            <option value="AZ">Arizona</option>
                                            <option value="CO">Colorado</option>
                                            <option value="WY">Wyoming</option>
                                        </optgroup>
                                        <optgroup label="Central Time Zone">
                                            <option value="AL">Alabama</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                        </optgroup>
                                        <optgroup label="Eastern Time Zone">
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="WV">West Virginia</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Nhà sản xuất:</label>
                                    <select data-placeholder="Select your state" class="select">
                                        <option></option>
                                        <optgroup label="Alaskan/Hawaiian Time Zone">
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </optgroup>
                                        <optgroup label="Pacific Time Zone">
                                            <option value="CA">California</option>
                                            <option value="OR">Oregon</option>
                                            <option value="WA">Washington</option>
                                        </optgroup>
                                        <optgroup label="Mountain Time Zone">
                                            <option value="AZ">Arizona</option>
                                            <option value="CO">Colorado</option>
                                            <option value="WY">Wyoming</option>
                                        </optgroup>
                                        <optgroup label="Central Time Zone">
                                            <option value="AL">Alabama</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                        </optgroup>
                                        <optgroup label="Eastern Time Zone">
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="WV">West Virginia</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tình trạng:</label>
                                    <select data-placeholder="Select your state" class="select">
                                        <option value="AZ">Hàng mới</option>
                                        <option value="CO">Đã qua sử dụng</option>
                                    </select>
                                </div>
                                <div class="form-group">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <label>Bảo hành:</label>
                                            <input type="text" class="form-control" placeholder="ex:06">
                                        </div>
                                        <div class="col-md-4">
                                            <label style="line-height: 30px; padding-top: 30px">/tháng</label>

                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Khuyến mại:</label>
                                    <textarea rows="5" cols="5" class="form-control" placeholder="Thông tin khuyến mại sản phẩm. "></textarea>
                                </div>
                            </div>
                        </fieldset>

                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="panel panel-flat">

                    <div class="panel-body">
                        <div class="">
                            <fieldset class="content-group">
                                <%-- <legend class="text-bold">
                                 
                                </legend>--%>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Tên sản phẩm <span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="basic" class="form-control" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Url<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="basic" class="form-control" required="required" placeholder="Đường dẫn tối ưu cho sản phẩm">
                                    </div>
                                </div>
                                <!-- Input group -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Ảnh sản phẩm <span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <div class="notice-upload-image">
                                            <p>
                                                <b style="font-size: 14px;">Đăng nhiều ảnh để công cụ tìm kiếm dễ thấy bạn hơn!</b><br />
                                                <i style="font-size: 11px; padding-top: 5px; color: #666;">Kéo ảnh lên vị trí đầu tiên để chọn làm Thumbnail (Đăng tối đa 5 ảnh)</i>
                                            </p>
                                        </div>
                                        <div class="row gallery-upload-file">
                                            <div class="col-md-3 r-queue-item">
                                                <i class="fa fa-picture-o"></i>
                                                <p>Đăng ảnh</p>
                                                <input type="file" multiple="multiple" class="file_upload ui-state-default">
                                            </div>
                                            <div class="col-md-3 r-queue-item">
                                                <i class="fa fa-picture-o"></i>
                                                <p>Đăng ảnh</p>
                                                <input type="file" multiple="multiple" class="file_upload ui-state-default">
                                            </div>
                                            <div class="col-md-3 r-queue-item">
                                                <i class="fa fa-picture-o"></i>
                                                <p>Đăng ảnh</p>
                                                <input type="file" multiple="multiple" class="file_upload ui-state-default">
                                            </div>
                                            <div class="col-md-3 r-queue-item">
                                                <i class="fa fa-picture-o"></i>
                                                <p>Đăng ảnh</p>
                                                <input type="file" multiple="multiple" class="file_upload ui-state-default">
                                            </div>
                                            <div class="col-md-3 r-queue-item">
                                                <i class="fa fa-picture-o"></i>
                                                <p>Đăng ảnh</p>
                                                <input type="file" multiple="multiple" class="file_upload ui-state-default">
                                            </div>
                                            <div class="col-md-3 r-queue-item _library">
                                                <i class="fa fa-folder-open"></i>
                                                <p>[Thư viện ảnh]</p>
                                            </div>

                                        </div>
                                        <p style="padding: 20px 5px 10px 5px;">Đăng nhiều hình thật nhanh bằng cách kéo và thả hình vào khung này hoặc nhấn nút phía trên</p>
                                    </div>
                                </div>
                                <!-- /input group -->



                            </fieldset>

                            <fieldset class="content-group">
                                <legend class="text-bold">Thông tin chi tiết</legend>

                                <!-- Number range -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Mô tả ngắn gọn<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <textarea id="SapoCkeditor" class="editor">oidung</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Chi tiết<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <textarea id="detailCkeditor" class="editor">oidung</textarea>
                                    </div>
                                </div>
                                <!-- /number range -->



                            </fieldset>

                            <fieldset class="content-group">
                                <legend class="text-bold">SEO</legend>

                                <!-- Maximum characters -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Từ khóa<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="maximum_characters" class="form-control" required="required" placeholder="Từ khóa SEO sản phẩm, độ dài tối đa 70 ký tự">
                                    </div>
                                </div>
                                <!-- /maximum characters -->

                                <!-- Maximum characters -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Mô tả từ khóa<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="maximum_characters" class="form-control" required="required" placeholder="Mô tả khi tìm kiếm google tối ưu độ dài khoảng [170-180] ký tự">
                                    </div>
                                </div>
                                <!-- /maximum characters -->


                                <!-- Date validation -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Tags<span class="text-danger">*</span></label>
                                    <div class="col-lg-10">
                                        <input type="text" name="date" class="form-control" required="required" placeholder="April, 2014 or any other date format">
                                    </div>
                                </div>

                            </fieldset>
                            <fieldset class="content-group">
                                <%--<legend class="text-bold">Sản phẩm liên quan</legend>--%>
                                <legend class="text-semibold">
                                    <i class="icon-reading position-left"></i>
                                    Sản phẩm liên quan
												<a class="control-arrow" data-toggle="collapse" data-target="#demo2" aria-expanded="true">
                                                    <i class="icon-circle-down2"></i>
                                                </a>
                                </legend>
                                <!-- Maximum characters -->
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <%--<label>Nhà sản xuất:</label>--%>
                                                    <select data-placeholder="Lọc theo danh mục" class="select">
                                                        <option></option>
                                                        <optgroup label="Alaskan/Hawaiian Time Zone">
                                                            <option value="AK">Alaska</option>
                                                            <option value="HI">Hawaii</option>
                                                        </optgroup>
                                                        <optgroup label="Pacific Time Zone">
                                                            <option value="CA">California</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="WA">Washington</option>
                                                        </optgroup>
                                                        <optgroup label="Mountain Time Zone">
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="WY">Wyoming</option>
                                                        </optgroup>
                                                        <optgroup label="Central Time Zone">
                                                            <option value="AL">Alabama</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                        </optgroup>
                                                        <optgroup label="Eastern Time Zone">
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="WV">West Virginia</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="basic" class="form-control" required="required" placeholder="Tìm theo tên">
                                                </div>
                                            </div>

                                        </div>
                                        <ul class="product-refer">
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="col-md-2" style="display: table-cell; min-height: 100px; text-align: center; padding-top: 10%">
                                        <div class="text-center" style="padding: 5px 0">
                                            <button type="submit" class="btn btn-primary"><i class="icon-arrow-right14"></i></button>
                                        </div>
                                        <div class="text-center" style="padding: 5px 0">
                                            <button type="submit" class="btn btn-primary"><i class=" icon-arrow-left13 "></i></button>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <%--<label>Nhà sản xuất:</label>--%>
                                                    <select data-placeholder="Loại" class="select">
                                                        <option value="AK">Sản phẩm liên quan</option>
                                                        <option value="HI">Phụ kiện </option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <ul class="product-refer">
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>
                                            <li>
                                                <a class="thumb">
                                                    <img src="/CMS/Themes/images/placeholder.jpg" width="50px" />
                                                </a>
                                                <p>Are projecting inquietude</p>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                            </fieldset>
                            <div class="text-right">
                                <button type="reset" class="btn btn-default" id="reset">Lưu & Giữ nguyên <i class="fa  fa-save"></i></button>
                                <button type="submit" class="btn btn-primary">Đăng <i class="fa  fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /form validation -->
        <!-- Footer -->
        <div class="footer text-muted">
            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
        </div>
        <!-- /footer -->

    </div>
    <!-- /content area -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
     <script src="/CMS/Themes/js/ckeditor/ckeditor.js"></script>
    <script src="/CMS/Themes/js/ckeditor/config.js"></script>
    <%--<script src="/Libs/scrollbar/jquery.scrollbar.min.js"></script>--%>
    <!-- Theme JS files -->
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/inputs/touchspin.min.js"></script>
   

    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/core/app.js"></script>
    <script type="text/javascript" src="/CMS/Themes/js/pages/form_validation.js"></script>


    <script src="/CMS/Modules/FileManager/fmanagers.v1.js"></script>
    <script src="/CMS/Modules/Product/Scripts/main.js"></script>

</asp:Content>

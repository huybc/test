﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="VinTravel.CMS.Modules.Service.Template.List" %>
<%@ Import Namespace="Mi.Common" %>
<style>
    #zone-tree{ width: 96%;margin: 20px 0}
    #zone-tree tr td i{ float: right;clear: both;padding-right: 10px;color: #a9a9a9;font-size: 14px;display: none;cursor: pointer}
    #zone-tree tr:hover td i{display: block }
</style>
<script>
    var folders = <%= NewtonJson.Serialize(ZoneResult())%>;
</script>



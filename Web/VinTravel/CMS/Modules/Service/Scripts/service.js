﻿R.Service = {
    Init: function () {
        R.Service.RegisterEvents();
        this.ZoneId = 0;
        this.ZoneType = 11;
        this.ZoneNode = null;
        //----
        this.PageIndex = 1;
        this.PageSize = 30;
        this.NewsStatus = 1;
        this.startDate = null;
        this.endDate = null;
        this.ItemSelected = [];
        this.XNewsId = 0;
        this.minDate = moment("01/01/2014").format('DD/MM/YYYY');
        this.maxDate = moment("12/31/2050").format('DD/MM/YYYY');

        if ($('#mini-game').length > 0) {
            R.Service.XNews();
            $('[data-popup="tooltip"]').tooltip();
        }
    },
    RegisterEvents: function () {
        if ($('#mini-game').length > 0) {

            $('#news-save .btn-primary').off('click').on('click', function (e) {
                var status = $(this).attr('status');
                var oid = R.Service.XNewsId;
                var title = $('#titletxt').val();
                var sapo = $('#Sapo').val();
                var zoneddl = $('#_zoneddl').val();
                var url = $('#urltxt').val();
                var titleSeo = $('#metaTitletxt').val();
                var metakeyword = $('#metakeywordtxt').val();
                var metadescription = $('#metadescriptiontxt').val();
                var date = $('#datetxt').attr('data');
                var content = CKEDITOR.instances['detailCkeditor'].getData();
                var ishot = $('#hotcb').is(':checked');
             
                var data = {
                    id: oid,
                    title: title,
                    sapo: sapo,
                    //  author: author,
                    // source: source,
                    zone: zoneddl,
                    date: date,
                    content: content,
                    url: url,
                    //status:status,
                    metakeyword: metakeyword,
                    status: status,
                    metadescription: metadescription,
                    avatar: $('#attack-thumb img').attr('data-img'),
                    metatitle: titleSeo,
                    ishot: ishot
                }

                R.Service.XSave(data);
            });


            $('#module-content .btn-action button._edit').off('click').on('click', function (e) {
                var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked').attr('id');

                R.Service.XEdit(id);
            });

            $('#btn-new-post').off('click').on('click', function () {
                R.Service.XEdit(0);
            });
            var delay = null;
            $('#news-sldebar .navigation li.sub-news-game').off('click').on('click', function () {

                R.Service.ItemSelected = [];
                $('#main-pnl').show('slow');
                $('#sub-pnl').hide();
                $('#news-sldebar .navigation li').removeClass('active');
                $(this).addClass('active');
                R.Service.startDate = R.Service.minDate;
                R.Service.endDate = R.Service.maxDate;
                R.Service.PageIndex = 1;
                $('#date-filter span').html("Tất cả: " + R.Service.minDate + ' - ' + R.Service.maxDate);
                R.Service.NewsStatus = $(this).attr('status');
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Service.XNews();
                }, 200);

            });
            $('#date-filter').off('apply.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
                R.Service.startDate = picker.startDate.format('YYYY-MM-DD');
                R.Service.endDate = picker.endDate.format('YYYY-MM-DD');
                R.Service.XNews();
            });
            $('#zone-filer-ddl').off('change').on('change', function (e) {
                R.Service.XNews();
            });
            $('#zone-type-ddl').off('change').on('change', function (e) {
                R.Service.XNews();
            });
            $('#btn-new-reload').off('click').on('click', function (e) {
                R.Service.startDate = R.Service.minDate;
                R.Service.endDate = R.Service.maxDate;
                R.Service.PageIndex = 1;
                $('#_search-data').val('');
                $('#date-filter span').html("Tất cả: " + R.Service.minDate + ' - ' + R.Service.maxDate);
                if (delay != null) clearTimeout(delay);
                delay = setTimeout(function () {
                    R.Service.XNews();
                }, 200);
            });
            $('#module-content .btn-action button._view').off('click').on('click', function (e) {
                $('#news-preview').RLoading();
                var id = $('#module-content .datatable-scroll table tbody tr td input[type=checkbox]:checked').attr('id');
                $('#news-preview').modal('show').off('shown.bs.modal').on('shown.bs.modal', function () {
                    R.Post({
                        params: { id: id },
                        module: "news",
                        ashx: 'modulerequest.ashx',
                        action: "preview",
                        success: function (res) {
                            if (res.Success) {
                                $('#news-preview-content').html(res.Content).find('#body .detail img').css({ 'max-width': '80%' }).parent().css({
                                    'text-align': 'center', 'width': '100%', 'display': 'inline-block'
                                });
                                $('#news-preview-content p img,#news-preview-content p iframe').parent().css('text-align', 'center');
                                $('#news-preview .modal-content').css('overflow-y', 'hidden');
                                $('[data-popup="tooltip"]').tooltip();
                                // check menu action in view
                                if (R.Service.NewsStatus === '3' || R.Service.NewsStatus === 3) {
                                    $('.btn-group.btn-action ._unpublish').show("slow");
                                } else {
                                    $('.btn-group.btn-action ._unpublish').hide("slow");
                                }
                                // bài lưu tạm
                                if (R.Service.NewsStatus === '2' || R.Service.NewsStatus === 2) {
                                    // từ chối
                                    $('.btn-group.btn-action ._reject').show("slow");
                                } else {
                                    // từ chối
                                    $('.btn-group.btn-action ._reject').hide("slow");
                                }

                                if (R.Service.NewsStatus === '6' || R.Service.NewsStatus === 6) {
                                    $('.btn-group.btn-action ._request_publish').hide("slow");
                                } else {

                                    $('.btn-group.btn-action ._request_publish').show("slow");
                                }

                                R.ScrollAutoSize('#news-preview .container-news', function () {
                                    return $(window).height() - 10;
                                }, function () {
                                    return 'auto';
                                }, {});

                                var $zoneItem = $('#news-preview').find('._zone');
                                if ($zoneItem.length > 0 && $zoneItem != 'undefined') {
                                    var strHtml = '';
                                    var arrayId = $zoneItem.attr('data-id').split(',');
                                    for (var k = 0; k < arrayId.length; k++) {
                                        $(RAllZones).each(function (i, item) {
                                            if (arrayId[k] == item.Id) {
                                                if (strHtml.length == 0) {
                                                    strHtml = item.Name;
                                                } else {
                                                    strHtml += ';' + item.Name;
                                                }
                                            }
                                            return;
                                        });

                                    }
                                    $zoneItem.text(strHtml);
                                }
                                R.Post({
                                    params: { id: id },
                                    module: "news",
                                    ashx: 'modulerequest.ashx',
                                    action: "reject_log",
                                    success: function (res) {
                                        var str = '';
                                        if (res.Success) {
                                            if (parseInt(res.TotalRow) > 0) {
                                                $.each($.parseJSON(res.Data), function (i, item) {
                                                    str += '<li><p><b>' + item.CreatedBy + '</b> - <time>' + item.Date + '</time></p><div id="log_content">' + item.Content + '</div></li>';
                                                });
                                                $('#reject-text').html('<div id="_log"><ul>' + str + '</ul></div>').show();

                                            }
                                        }
                                    }
                                });

                                R.Service.RegisterEvents();
                                $('#news-preview').RLoadingComplete();

                            } else {
                                alert(res.Message);
                            }
                        }
                    });
                });
            });
            $('#module-content .btn-action button.status').off('click').on('click', function (e) {
                var status = $(this).attr('data-status');
                var data = {
                    id: R.Service.ItemSelected.join(','),
                    status: status
                }
                $.confirm({
                    title: 'Thông báo',
                    content: 'Xác nhận yêu cầu ?',
                    buttons: {
                        confirm: {
                            text: 'Thực hiện',
                            action: function () {
                                R.Service.XUpdateStatus(data);
                            }
                        },
                        cancel: {
                            text: 'Hủy',
                            action: function () {

                            }
                        },
                    }
                });


            });

            $('#data-list .datatable-scroll tr td input[type=checkbox]').off('click').on('click', function () {

                var id = $(this).attr('id');
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('row-selected');
                    R.Service.ItemSelected.push(id);
                } else {

                    var index = R.Service.ItemSelected.indexOf(id);
                    if (index > -1) {
                        R.Service.ItemSelected.splice(index, 1);
                    }
                    $(this).closest('tr').removeClass('row-selected');
                }


                if (R.Service.ItemSelected.length === 0) {
                    $('.btn-group.btn-action').slideUp();
                }
                if (R.Service.ItemSelected.length === 1) {
                    $('.btn-group.btn-action').slideDown("slow");
                    // $('.btn-group.btn-action ._delete').show("slow");
                    //$('.btn-group.btn-action ._view').show("slow");
                    $('.btn-group.btn-action ._edit').show("slow");


                } else

                    if (R.Service.ItemSelected.length > 1) {
                        $('.btn-group.btn-action').slideDown("slow");
                        //  $('.btn-group.btn-action ._delete').show("slow");
                        //$('.btn-group.btn-action ._view').hide();
                        $('.btn-group.btn-action ._edit').hide();
                        // bài đã duyệt
                        //if (R.Service.NewsStatus === '1' || R.Service.NewsStatus === 1) {
                        //    $('.btn-group.btn-action ._unpublish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._unpublish').hide("slow");
                        //}
                        // bài lưu tạm
                        //if (R.Service.NewsStatus === '4' || R.Service.NewsStatus === 4) {
                        //    $('.btn-group.btn-action ._publish').show("slow");
                        //} else {
                        //    $('.btn-group.btn-action ._publish').hide("slow");
                        //}



                    }
                if (R.Service.NewsStatus === '3' || R.Service.NewsStatus === 3) {
                    $('.btn-group.btn-action ._delete').hide("slow");

                } else {
                    $('.btn-group.btn-action ._delete').show("slow");
                }
                if (R.Service.NewsStatus === '4' || R.Service.NewsStatus === 4) {
                    $('.btn-group.btn-action ._publish').show("slow");
                } else {
                    $('.btn-group.btn-action ._publish').hide("slow");
                }
                if (R.Service.NewsStatus === '1' || R.Service.NewsStatus === 1) {
                    $('.btn-group.btn-action ._unpublish').show("slow");
                } else {
                    $('.btn-group.btn-action ._unpublish').hide("slow");
                }
                $('#rows').text(R.Service.ItemSelected.length);
            });
        }
        //1368==========================

        var delay = 0;
        $('#news-sldebar .navigation li.sub-item').off('click').on('click', function () {
          
            R.Service.ZoneType = parseInt($(this).attr('data-type'));
            $('#news-sldebar .navigation li').removeClass('active');
            $(this).addClass('active');
            if (delay != null) clearTimeout(delay);
            delay = setTimeout(function () {
                R.Service.MainInit();
            }, 200);

        });

        $('#btn-add-zone').off('click').on('click', function () {
            R.Service.Edit(0);

        });
        $('#titletxt').off('keypress').on('keypress', function () {
            $('#urltxt').val(R.UnicodeToSeo($('#titletxt').val()));
        });
        $('#zoneSaveBtn').off('click').on('click', function () {
            var data = {
                id: R.Service.ZoneId,
                name: $('#zoneNameTxt').val(),
                parent_id: $('#_zoneddl').val(),
                sort: $('#sortZoneTxt').val(),
                sapo: CKEDITOR.instances['sapoZoneTxt'].getData(),
                metakey: $('#metaKeywordZoneTxt').val(),
                metades: $('#metaDescriptionZoneTxt').val(),
                metatitle: $('#metaTitletxt').val(),
                type: R.Service.ZoneType
            }
            R.Service.Save(data);
        });


    },
    AttackFile: function (el) {
        if (typeof (el) != "undefined") {
            $.each(el, function (i, v) {

                $('#attack-thumb img').attr('src',v).attr('data', v);
            });
        }
    },
    MainInit: function () {
        R.Post({
            params: {

            },
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "main",
            success: function (res) {
                if (res.Success) {
                    $('#sub-pnl').html(res.Content);
                    $('#main-pnl').hide();
                    $('#sub-pnl').show('slow');

                    R.Service.Search();
                    R.Service.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XUpdateStatus: function (data) {
        R.Post({
            params: data,
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "update_status",
            success: function (res) {
                if (res.Success) {
                    $.notify("Cập nhật thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.Service.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }

        });
    },
    XNews: function () {

        var el = '#data-list';
        $(el).RLoading();
        var data = {
            keyword: $('#_search-data').val(),
            zone: $('#zone-filer-ddl').val(),
            pageindex: R.Service.PageIndex,
            pagesize: R.Service.PageSize,
            status: R.Service.NewsStatus,
            from: R.Service.startDate,
            to: R.Service.endDate,
            type: $('#zone-type-ddl').val()
        }
        R.Post({
            params: data,
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "xsearch",
            success: function (res) {
                if (res.Success) {
                    R.Service.ItemSelected = [];
                    $(el).find('.datatable-scroll._list').html(res.Content);
                    $('.btn-group.btn-action').hide();
                    R.CMSMapZone('#data-list');

                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);
                    //pager
                    if ($('#data-list .datatable-scroll._list table').attr('page-info') != 'undefined') {
                        var pageInfo = $('#data-list .datatable-scroll._list table').attr('page-info').split('#');
                        var page = pageInfo[0];
                        var extant = pageInfo[1];
                        var totals = pageInfo[2];
                        if (parseInt(totals) < 0) {
                            $('#data-pager').hide();
                            return;
                        } else {
                            $('#data-pager').show();
                        }
                        var rowFrom = '';
                        if (R.Service.PageIndex === 1) {
                            rowFrom = 'Từ 1 đến ' + extant;
                        } else {
                            rowFrom = 'Từ ' + (parseInt(R.Service.PageIndex) * parseInt(R.Service.PageSize) - parseInt(R.Service.PageSize)) + ' đến ' + extant;
                        }

                        $('#rowInTotals').text(rowFrom + '/' + totals);
                        $('.ipagination').jqPagination({
                            current_page: R.Service.PageIndex,
                            max_page: page,
                            paged: function (page) {
                                R.Service.PageIndex = page;
                                R.Service.XNews();
                            }
                        });
                    }
                    //end pager



                    R.Service.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },
    Search: function () {
        R.Post({
            params: {
                type: R.Service.ZoneType
            },
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "search",
            success: function (res) {
                if (res.Success) {
                    var data = $.parseJSON(res.Data);

                    setTimeout(function () {
                        R.Service.JSTree(data);
                    }, 200);

                    R.ScrollAutoSize('.datatable-scroll', function () {
                        return $(window).height() - 92;
                    }, function () {
                        return 'auto';
                    }, {}, {}, {}, true);

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    Save: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "save",
            success: function (res) {
                if (res.Success) {
                    if (data.id > 0) {
                        $('#data-zones').jstree('rename_node', R.Service.ZoneNode, data.name);
                    } else {
                        R.Service.Search();
                    }

                    R.ScrollAutoSize('#data-zones', function () {
                        return $(window).height() - 10;
                    }, function () {
                        return 'auto';
                    }, {});
                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#zone-edit').RModuleUnBlock();
            }
        });
    },
    Edit: function (id) {
        $('#zone-form-edit').RLoading();
        R.Post({
            params: {
                id: id,
                zoneType: R.Service.ZoneType
            },
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "edit",
            success: function (res) {
                if (res.Success) {
                    //   $('#zone-form-edit').html(res.Content);

                    R.ShowOverlay(res.Content, function () {

                        $("#ZoneSaveButton").off('mouseover').off('mouseout')
                                       .mouseover(function () {
                                           $(this).removeClass('SaveButtonNormalState');
                                           $(this).addClass('SaveButtonOverState');
                                       })
                                       .mouseout(function () {
                                           $(this).removeClass('SaveButtonOverState');
                                           $(this).addClass('SaveButtonNormalState');
                                       });

                        $('#ZoneSaveButton').off('click').on('click', function () {
                            return false;
                        });
                    }, function () {

                    });

                    R.Service.ZoneId = id;
                    if (R.Service.ZoneType === 3) {
                        $('#zone-panel').hide();
                    }
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');
                        }
                        if (CKEDITOR.instances['sapoZoneTxt']) {
                            CKEDITOR.instances['sapoZoneTxt'].destroy();
                        }
                        CKEDITOR.replace('sapoZoneTxt', { toolbar: 'iweb' });
                        R.ScrollAutoSize('#zone-edit .panel-body', function () {
                            return $(window).height() - 10;
                        }, function () {
                            return 'auto';
                        }, {});
                    }, 400);

                    $('#zone-form-edit').RLoadingComplete();
                    R.Service.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XEdit: function (id) {

        $('body').RModuleBlock();
        R.Post({
            params: {
                id: id
                // zoneType: R.Service.ZoneType
            },
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "xedit",
            success: function (res) {

                if (res.Success) {
                    //   $('#zone-form-edit').html(res.Content);

                    R.ShowOverlayFull({ content: res.Content }, function () {


                    }, function () {

                    });

                    R.Service.XNewsId = id;
                    if (R.Service.ZoneType === 3) {
                        $('#zone-panel').hide();
                    }
                    $('#attack-thumb .avatar').off('click').on('click', function () {
                        R.Image.SingleUpload($('#attack-thumb .avatar'),
                            function () {
                                //process
                            }, function (response, status) {
                                $('#attack-thumb .avatar').RModuleUnBlock();
                                //success
                                if (!response.success) {
                                    $.confirm({
                                        title: 'Thông báo !',
                                        type: 'red',
                                        content: response.messages,
                                        animation: 'scaleY',
                                        closeAnimation: 'scaleY',
                                        buttons: {
                                            cancel: {
                                                text: 'Đóng',
                                                btnClass: 're-btn re-btn-default'
                                            },
                                            yes: {
                                                isHidden: true, // hide the button
                                                keys: ['ESC'],
                                                action: function () {

                                                }
                                            }
                                        }

                                    });
                                    return;
                                }

                                if (response.success && response.error == 200) {
                                    var img =response.path;
                                    $('#attack-thumb .avatar img').attr({
                                        'src': img + '?w=150&h=100&mode=crop',
                                        'data-img': response.name
                                    });
                                }

                            });

                    });
                    setTimeout(function () {
                        $('#_zoneddl').fselect({
                            dropDownWidth: 0,
                            autoResize: true
                        });
                        if (id > 0) {
                            $('#_zoneddl').val($('#_zoneddl').attr('data'));
                            $('#_zoneddl').trigger('fselect:updated');
                            $('#datetxt').daterangepicker({
                                autoUpdateInput: false,
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Service.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));
                            });
                        } else {
                            $('#datetxt').daterangepicker({
                                "singleDatePicker": true,
                                "timePicker": true,
                                "timePicker24Hour": true,
                                "timePickerSeconds": true,
                                //"autoApply": true,
                                "showCustomRangeLabel": false,
                                "alwaysShowCalendars": true,
                                "startDate": moment(),
                                "endDate": R.Service.maxDate,
                                "opens": "left",
                                locale: {
                                    format: 'DD/MM/YYYY h:mm A'
                                }
                            }, function (start, end, label) {
                                $('#datetxt').attr('data', start.format('YYYY-MM-DD h:mm A'));
                                $('#datetxt').val(start.format('DD-MM-YYYY h:mm A'));

                            });
                            $('#datetxt').attr('data', moment().format('YYYY-MM-DD h:mm A'));
                        }



                        autosize(document.querySelectorAll('textarea#Sapo'));
                        R.ScrollAutoSize('#zone-content', function () {
                            return $(window).height() - 10;
                        }, function () {
                            if (CKEDITOR.instances["detailCkeditor"]) {
                                CKEDITOR.instances["detailCkeditor"].destroy();
                            }
                            CKEDITOR.replace('detailCkeditor', { toolbar: 'iweb' });
                            return 'auto';
                        }, {});
                    }, 200);

                    $('body').RModuleUnBlock();
                    R.Service.RegisterEvents();

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },
    XSave: function (data) {
        $('#zone-edit').RModuleBlock();
        R.Post({
            params: data,
            module: "service",
            ashx: 'modulerequest.ashx',
            action: "xsave",
            success: function (res) {
                if (res.Success) {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });
                    R.CloseOverlayFull();
                    R.Service.NewsStatus = 1;
                    R.Service.XNews();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                 $('#zone-edit').RModuleUnBlock();
                R.Service.RegisterEvents();
            }
        });
    },
    JSTree: function (data) {

        var xtree = $('#data-zones').jstree({
            "core": {
                "animation": 1,
                "check_callback": function (op, node, par, pos, more) {
                    if ((op === "move_node" || op === "copy_node") && node.type && node.type == "root") {
                        return false;
                    }
                    if ((op === "move_node" || op === "copy_node") && more && more.core && !confirm('Xác nhận  !')) {

                        return false;
                    }

                    if ((op === "delete_node" || op === "delete_node") && more && more.core && !confirm('Xác nhận 1 !')) {
                        return false;
                    }

                    // return true;
                },
                "themes": { "stripes": true },
                'data': data
            },
            "types": {
                "#": {
                    //   "max_children": 9,
                    "max_depth": 4,
                    //  "valid_children": ["root"]
                },
                "root": {
                    //  "icon": "/static/3.3.3/assets/images/tree_icon.png",
                    // "valid_children": []
                },
                "default": {
                    //  "valid_children": []
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    //   "valid_children": []
                }
            }, 'sort': function (a, b) {
                //  return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            "contextmenu": {
                "items": function ($node) {
                    //    var tree = $("#tree").jstree(true);
                    return {
                        "Edit": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Sửa",
                            icon: 'fa fa-edit',
                            "action": function (obj) {
                                R.Service.Edit($node.id);
                                R.Service.ZoneNode = $node;
                            }
                        },
                        "Remove": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Xóa",
                            "action": function (obj) {

                                if ($('#data-zones').jstree(true).get_selected(true)[0].children > 0) {
                                    alert('Xóa danh mục con trước !')
                                    return;
                                }
                                $.confirm({
                                    title: 'Xác nhận',
                                    content: 'Xác nhận xóa !',
                                    buttons: {
                                        confirm: {
                                            text: 'Xóa',
                                            action: function () {
                                                R.Post({
                                                    params: { id: $node.id },
                                                    module: "service",
                                                    ashx: 'modulerequest.ashx',
                                                    action: "remove",
                                                    success: function (res) {
                                                        if (res.Success) {

                                                            $('#data-zones').jstree(true).delete_node([$node]);
                                                        } else {
                                                            $.notify(res.Message, {
                                                                autoHideDelay: 3000, className: "error",
                                                                globalPosition: 'right top'
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        },
                                        cancel: {
                                            text: 'Hủy',
                                            action: function () {

                                            }
                                        },
                                    }
                                });



                            },
                            icon: 'fa fa-trash'
                        }

                    };
                }
            },
            "plugins": [
              "contextmenu", "dnd", "search", "state", "types", "wholerow"
            ]
        }).bind("move_node.jstree", function (e, data, node, par) {
            R.Post({
                params: { id: data.node.id, parent_id: data.node.parent },
                module: "service",
                ashx: 'modulerequest.ashx',
                action: "move",
                success: function (res) {
                    if (res.Success) {

                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });
                    }
                }
            });


        }).on('delete_node.jstree', function (e, data) {

        }).bind("select_node.jstree", function (evt, data) {
            $('#fm-wrapper').attr('node-select', data.node.id);
        });;

        var to = false;
        $('#_search-zone').keyup(function () {
            if (to) { clearTimeout(to); }
            to = setTimeout(function () {
                var v = $('#_search-zone').val();
                $('#data-zones').jstree(true).search(v);
            }, 250);
        });
    }

}
$(function () {
    R.Service.Init();
});
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="VinTravel.CMS.Modules.Service.Main" %>

<link href="/CMS/Modules/Zone/Styles/style.css" rel="stylesheet" />
<div class="panel-heading" id="header-page">
    <div class="panel-title">
        <div class="col-md-6">
            <div class="col-md-6 col-lg-3">
                <button type="button" id="btn-add-zone" class="btn btn-default ibtn-xs btn-primary heading-btn"><i class=" icon-pencil3 position-left"></i>Thêm mới</button>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="display: flex">
                    <input type="text" class="form-control _search-data" id="_search-zone" placeholder="Tìm kiếm chuyên mục">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
                    </span>
                </div>
            </div>
            <div class="col-md-12" style="padding: 20px 0">
                <div id="zone-wrapper">
                    <div id="data-zones"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <%--<div id="zone-form-edit"></div>--%>
        </div>
    </div>

</div>
<div id="IMSOverlayWrapper">
    <div id="IMSOverlayWrapperIn">
        <div id="IMSOverlayContent">
        </div>
        <span id="IMSOverlayClose"></span>
    </div>
</div>

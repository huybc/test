﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="VinTravel.CMS.Modules.Config.Templates.Edit" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<div class="tabbable" id="" style="width: 800px">
    <div class=" _body" id="_body">
        <div id="config-content" data="<%=obj.ConfigValueType %>" data-key="<%=obj.ConfigName %>">
            <div class=" panel-flat">
                <div class="panel-title" style="height: 46px; padding: 10px 0 4px 0">
                    <div class="col-md-12 text-right">
                        <button type="button" id="ConfigSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
                    </div>
                </div>
                <div class="panel-body" style="padding-top: 0px">
                    <div id="icroll">
                        <div class="form-group">
                            <label class="col-lg-2">Tên cấu hình</label>
                            <div class="col-lg-10">
                                <b><%=obj.ConfigLabel %></b> [<%=obj.ConfigName %>]
                            </div>
                        </div>
                        <%--<div class="form-group">
                            <label class="control-label">Loại<span class="text-danger">*</span></label>
                            <div class="">
                                <select id="typeddl">
                                    <option value="">-- Chọn --</option>
                                    <option value="<%= (int)EnumStaticHtmlTemplateType.Images %>">Ảnh</option>
                                    <option value="<%= (int)EnumStaticHtmlTemplateType.Content %>">Văn bản</option>
                                    <option value="<%= (int)EnumStaticHtmlTemplateType.Code %>">Code</option>
                                    <option value="<%= (int)EnumStaticHtmlTemplateType.Text %>">Text</option>
                                </select>
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <label class="control-label">Giá trị</label>
                            <div id="typeContent" style="display: none">
                                <textarea id="detailCkeditor" rows="10" class="editor form-control" tabindex="3"><%=obj.ConfigValue %></textarea>
                            </div>
                            <div id="typeImage" style="display: none">
                                <img  id="attack-thumb" src="/<%=!string.IsNullOrEmpty(obj.ConfigValue)?"uploads/"+ obj.ConfigValue:"/CMS/Themes/images/no-thumbnai.png" %>"/><br/><br/>
                               &nbsp; <a href="javascript:void(0)" id="attackFile"><label for="fileSingleupload">[Đính kèm]</label><input accept="image/*" id="fileSingleupload" multiple type="file" name="files[]" style="display: none" /></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinTravel.Core.Helper;
using Mi.BO.Base.Customer;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Mi.Action;
using Mi.Action.Core;

namespace VinTravel.CMS.Modules.Customer.Action
{
    public class CustomerActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            if (!PolicyProviderManager.Provider.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {
                switch (functionName)
                {
                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;

                    case "xsearch":
                        responseData = Customers();
                        break;

                    case "xsave":
                        responseData = SaveZoneNews();
                        break;
                    case "save":
                        responseData = SaveZoneNews2();
                        break;

                    case "update_status":
                        var status = GetQueryString.GetPost("status", 0);
                        responseData = ChangeStatus(status);
                        break;

                    case "xedit":
                        responseData = XEdit();

                        break;
                }
            }

            return responseData;
        }


        private ResponseData XEdit()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Customer\\Template\\Edit.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData GetListZone()
        {
            ResponseData responseData;
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", -1);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name.Replace("+ ", "")
                });
            }
            responseData = ConvertResponseData.CreateResponseData(rs, rs.Count(), "");




            return responseData;
        }
        //private ResponseData Customers()
        //{
        //    var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\CMS\\Modules\\Customer\\Template\\List.aspx");
        //    responseData.Success = true;
        //    return responseData;
        //}

        private ResponseData Customers()
        {
            ResponseData responseData = new ResponseData();
            try
            {
                var type = GetQueryString.GetPost("type", 0);
                var templatePath = string.Empty;
                switch (type)
                {
                    case (int)ZoneType.Cooperative:
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\hoptac.aspx";
                        break;
                    case 12:
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\Download.aspx";
                        break;
                    case 13:
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\Download.aspx";
                        break;
                    case 14:
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\Project.aspx";
                        break;
                    case (int)ZoneType.Blog:
                        templatePath = "\\CMS\\Modules\\Customer\\Template\\List.aspx";
                        break;

                }



                responseData = ConvertResponseData.CreateResponseData("", 1, templatePath);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return responseData;
        }










        private ResponseData SaveZoneNews()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty);
                var email = GetQueryString.GetPost("email", string.Empty);
                var phone = GetQueryString.GetPost("phone", string.Empty);
                var address = GetQueryString.GetPost("address", string.Empty);
                //  var sortOrder = GetQueryString.GetPost("sort", 0);
                var company = GetQueryString.GetPost("company", string.Empty);
                var type = GetQueryString.GetPost("type", string.Empty);
                var service = GetQueryString.GetPost("service", string.Empty);
                var status = GetQueryString.GetPost("status", string.Empty);
                var contact = GetQueryString.GetPost("contact", string.Empty);
                var note = GetQueryString.GetPost("note", string.Empty);



                if (!string.IsNullOrEmpty(name))
                {

                    var obj = new CustomerEntity
                    {
                        Id = id,
                        FullName = name,
                        Email = email,
                        Mobile = phone,
                        Address = address,
                        Firm = company,
                        Type = type,
                        Service = service,
                        Status = status,
                        Contactperson = contact,
                        Note = note
                    };
                    int outId = 0;
                    if (id > 0)
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Update(obj, ref outId));

                        responseData.Success = true;
                        responseData.Message = "Cập nhật thành công !";
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                        if (outId > 0)
                        {
                            responseData.Success = true;
                        }
                        responseData.Message = "Thêm mới thành công !";
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }

        private ResponseData SaveZoneNews2()
        {
            var responseData = new ResponseData();

            if (!Policy.IsLogin())
            {
                responseData.Success = false;
                responseData.Message = Constants.MESG_TIMEOUT_SESSION;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.TimeOutSession;
            }
            else
            {

                var id = GetQueryString.GetPost("id", 0);
                var name = GetQueryString.GetPost("name", string.Empty).GetText();
                var email = GetQueryString.GetPost("email", string.Empty).GetText();
                var phone = GetQueryString.GetPost("phone", string.Empty).GetText();
                var address = GetQueryString.GetPost("address", string.Empty).GetText();
                //  var sortOrder = GetQueryString.GetPost("sort", 0);
                var company = GetQueryString.GetPost("company", string.Empty).GetText();
                var type = GetQueryString.GetPost("type", string.Empty).GetText();
                var service = GetQueryString.GetPost("service", string.Empty).GetText();
                var status = GetQueryString.GetPost("status", "Potential").GetText();
                var contact = GetQueryString.GetPost("contact", string.Empty).GetText();
                var note = GetQueryString.GetPost("note", string.Empty).GetText();

                if (!string.IsNullOrEmpty(name))
                {

                    var obj = new CustomerEntity
                    {
                        Id = id,
                        FullName = name,
                        Email = email,
                        Mobile = phone,
                        Address = address,
                        Firm = company,
                        Type = type,
                        Service = service,
                        Status = status,
                        Contactperson = contact,
                        Note = note
                    };
                    int outId = 0;
                    if (id > 0)
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Update(obj, ref outId));

                        responseData.Success = true;
                        responseData.Message = "Cập nhật thành công !";
                    }
                    else
                    {
                        responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                        if (outId > 0)
                        {
                            responseData.Success = true;
                        }
                        responseData.Message = "Thêm mới thành công !";
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.Message = "Invalid zone's name";
                }
            }
            return responseData;
        }



        public ResponseData ChangeStatus(int status)
        {
            var username = Policy.GetAccountName();

            var listNewsId = GetQueryString.GetPost("id", "");
            ResponseData responseData = null;
            var listNewsIdUpdated = "";

            var newsIds = listNewsId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
            {
                responseData = ConvertResponseData.CreateResponseData(ZoneBo.ChangeStatus((int)id, username, status));
                if (responseData.Success)
                {
                    listNewsIdUpdated += ";" + id;

                }
                var objsNews = ZoneBo.GetZoneById(id.ToInt32Return0());
                if (objsNews != null)
                {
                    if ((int)ZoneStatus.Publish == status)
                    {
                        XMLDAL.UpdateNode(objsNews.ShortUrl, id.ToInt32Return0());

                    }

                }
            }

            if (!string.IsNullOrEmpty(listNewsIdUpdated))
            {
                responseData = ResponseData.CreateSuccessResponseData(listNewsIdUpdated.Remove(0, 1), 1, "");
            }
            return responseData;
        }
        public class ZoneResult
        {
            public string id { get; set; }
            public string parent { get; set; }
            public string text { get; set; }
            public string path { get; set; }
        }
    }
}
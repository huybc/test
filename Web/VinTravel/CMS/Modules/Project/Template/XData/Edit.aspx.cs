﻿using System;
using System.Linq;

using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Zone;

namespace VinTravel.CMS.Modules.Project.Template.XData
{
    public partial class Edit : PageBase
    {
        public NewsDetailForEditEntity _obj = new NewsDetailForEditEntity();
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0L);
            _obj = NewsBo.GetDetail(id, PolicyProviderManager.Provider.GetAccountName());
            if (id <= 0)
            {
                _obj = new NewsDetailForEditEntity
                {
                    NewsInfo = new NewsEntity()
                };
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Project).ToList().Where(it => it.Invisibled == false);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
            var group = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Product).ToList().Where(it => it.Invisibled == false);
            groupRpt.DataSource = group;
            groupRpt.DataBind();
        }
    }
}
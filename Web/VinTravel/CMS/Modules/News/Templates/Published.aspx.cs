﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Security;
using Mi.Entity.Base.Zone;

namespace VinTravel.CMS.Modules.News.Templates
{
    public partial class Published : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            string title = GetQueryString.GetPost("title", string.Empty);
            var fromDate = GetQueryString.GetPost("from", DateTime.MinValue);
            var todate = GetQueryString.GetPost("to", DateTime.MaxValue);
            var pageIndex = GetQueryString.GetPost("pageindex", 1);
            var pagesize = GetQueryString.GetPost("pagesize", 10);
            var keyWord = GetQueryString.GetPost("keyword", String.Empty);
            var zoneId = GetQueryString.GetPost("zone", "");
          
            var username = GetQueryString.GetPost("account", PolicyProviderManager.Provider.GetAccountName());
            var order = GetQueryString.GetPost("order", (int)NewsSortExpression.DistributionDateDesc);
            var newsType = GetQueryString.GetPost("type", (int)NewsType.All);


          


            DataRpt.DataSource = NewsBo.SearchNews(keyWord, username, zoneId, fromDate, DateTime.MaxValue, NewsFilterFieldForUsername.CreatedBy, NewsSortExpression.CreatedDateDesc,
                                                       (int)NewsStatus.Published, 1, pageIndex, pagesize, ref TotalRows);
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal) ((double) TotalRows/pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }
    }
}

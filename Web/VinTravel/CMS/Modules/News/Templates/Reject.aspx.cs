﻿using System;
using System.Collections.Generic;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.News;
using Mi.Common;
using Mi.Entity.Base.News;
using Mi.Entity.Base.Security;
using Mi.Entity.Base.Zone;

namespace VinTravel.CMS.Modules.News.Templates
{
    public partial class Reject : PageBase
    {
        public string pageInfo = string.Empty;
        public int TotalRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            string title = GetQueryString.GetPost("title", string.Empty);
            var fromDate = GetQueryString.GetPost("from", DateTime.MinValue);
            var todate = GetQueryString.GetPost("to", DateTime.MaxValue);
            var pageIndex = GetQueryString.GetPost("pageindex", 0);
            var pagesize = GetQueryString.GetPost("pagesize", 0);
            var keyWord = GetQueryString.GetPost("keyword", String.Empty);
            var zoneId = GetQueryString.GetPost("zone", 0);
            var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
            var username = GetQueryString.GetPost("account", PolicyProviderManager.Provider.GetAccountName());
            var order = GetQueryString.GetPost("order", (int)NewsSortExpression.DistributionDateDesc);
            var newsType = GetQueryString.GetPost("type", (int)NewsType.All);
            pagesize = pagesize == 0 ? 50 : pagesize;


            if (todate != DateTime.MinValue) todate = new DateTime(todate.Year, todate.Month, todate.Day, 23, 59, 59);

            var sortBy = NewsSortExpression.LastModifiedDateDesc;
            var newsStatus = NewsStatus.Reject;

           
            var filterBy = NewsFilterFieldForUsername.CreatedBy;

            if (PolicyProviderManager.Provider.GetCurrentRole() == EnumPermission.ArticleReporter &&
                Role.HasRoleOnPermission(EnumPermission.ArticleReporter, username))
            {
                if (newsStatus == NewsStatus.Unknow) newsStatus = NewsStatus.Temporary;

                if (newsStatus != NewsStatus.Temporary &&
                    newsStatus != NewsStatus.WaitForPublish &&
                    newsStatus != NewsStatus.Published &&
                    newsStatus != NewsStatus.Unpublished &&
                    newsStatus != NewsStatus.MovedToTrash)
                {
                    newsStatus = NewsStatus.Temporary;
                }
                filterBy = (newsStatus == NewsStatus.MovedToTrash
                                ? NewsFilterFieldForUsername.LastModifiedBy
                                : NewsFilterFieldForUsername.CreatedBy);

            }
            else if (PolicyProviderManager.Provider.GetCurrentRole() == EnumPermission.ArticleEditor &&
                     Role.HasRoleOnPermission(EnumPermission.ArticleEditor, username))
            {

                if (newsStatus == NewsStatus.MovedToTrash)
                {
                    filterBy = NewsFilterFieldForUsername.LastModifiedBy;
                }
                else
                {
                    filterBy = NewsFilterFieldForUsername.EditedBy;
                }
            }
            else if (PolicyProviderManager.Provider.GetCurrentRole() == EnumPermission.ArticleAdmin &&
                     Role.HasRoleOnPermission(EnumPermission.ArticleAdmin, username))
            {

                // Filter by?
                if (newsStatus == NewsStatus.MovedToTrash)
                {
                    filterBy = NewsFilterFieldForUsername.LastModifiedBy;
                }
                else if (newsStatus == NewsStatus.Unpublished)
                {
                    username = "";
                    //filterBy = NewsFilterFieldForUsername.LastModifiedBy;
                }
                else // if (newsStatus == NewsStatus.WaitForPublish)
                {
                    username = ""; // Not filter by any username -> get news in all zone in his permission          
                }
                //if (newsStatus == NewsStatus.MovedToTrash || newsStatus == NewsStatus.ReceivedForPublish)
                //{
                //    zoneIds = "";
                //}
            }

            NewsType type;
            if (!Enum.TryParse(newsType.ToString(), out type))
            {
                type = NewsType.All;
            }

            DataRpt.DataSource = NewsBo.SearchNews(keyWord, username, zoneIds, fromDate, DateTime.MaxValue, filterBy, sortBy,
                                                      (int) newsStatus, 1, pageIndex, pagesize, ref TotalRows);
            DataRpt.DataBind();

            var extant = TotalRows - (pagesize * pageIndex);
            var totalPages = Math.Ceiling((decimal)((double)TotalRows / pagesize));
            var ex = 0;

            if (extant < pagesize && pageIndex == totalPages)
            {
                ex = extant + (pagesize * pageIndex);
            }
            else
            {
                ex = pagesize * pageIndex;

            }

            pageInfo = totalPages + "#" + (TotalRows < pagesize ? TotalRows : ex) + "#" + TotalRows;
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reject.aspx.cs" Inherits="VinTravel.CMS.Modules.News.Templates.Reject" %>

<%@ Import Namespace="VinTravel.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
            <td class="w100">
                <div>
                    <label>
                        <input type="checkbox" id="<%# Eval("Id") %>" />
                    </label>
                    <label class="_hot">
                       <i class="icon-file-minus2 position-static"></i>
                    </label>
                </div>
            </td>
            <td class="w80 ">
                <a class="avatar">
                    <img class="thumb" src="<%#  UIHelper.Thumb_W(60,Eval("Avatar").ToString()) %>" width="60" />
                    <%# (bool)Eval("IsVideo")?"<i class=\"icon-video\"></i>":"" %>
                </a>
            </td>
            <td>
                <p>
                   <%# Eval("Title") %> - <b class="wysiwyg-color-green _zone" data-id="<%# Eval("ZoneId") %>"></b>
                </p>
                <p>
                    <label>Tạo bởi:</label><span class="bold"><%# Eval("CreatedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>,
                                                        <label> Xuất bản bởi:</label><span class="bold"><%# Eval("PublishedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("DistributionDate")) %></time>
                </p>
            </td>

            <td><i o-id="<%# Eval("Id") %>" class="_icon  icon-info22" ></i></td>
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

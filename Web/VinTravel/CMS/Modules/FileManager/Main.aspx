﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="VinTravel.CMS.Modules.FileManager.Main" %>

<%@ Import Namespace="Mi.Common" %>
<link href="/CMS/Modules/FileManager/Styles/filemanager.css" rel="stylesheet" />
<div id="fm-container" class="fm-container">
    <div id="fm-wrapper">
        <div class="fm-header">
            <div class="breadcrumbs">
                <div class="nav-title">Current folder: </div>
                <div>
                    <div class="nav-item root active"></div>
                    <!-- ko if: !bcItem.active -->
                    <!-- /ko -->
                </div>
            </div>
            <div class="buttons-panel">
                <!-- ko if: clipboardModel.enabled() -->
                <div class="button-group clipboards-controls">
                    <button id="clipboard-copy" type="button" title="Copy"><span>&nbsp;</span></button>
                    <button id="clipboard-cut" type="button" title="Cut"><span>&nbsp;</span></button>
                    <button id="clipboard-paste" type="button" title="Paste to the current folder"><span>&nbsp;</span></button>
                    <button id="clipboard-clear" type="button" title="Clear clipboard"><span>&nbsp;</span></button>
                </div>
                <div class="button-group">
                    <button class="separator" type="button">&nbsp;</button>
                </div>
                <!-- /ko -->
                <div class="button-group navigate-controls">
                    <button class="fm-btn-level-up no-label" type="button" title="Go to parent folder">
                        <span>&nbsp;</span>
                    </button>
                    <button class="fm-btn-home no-label" type="button" title="Go to root folder">
                        <span>&nbsp;</span>
                    </button>
                </div>
                <!-- ko if: !browseOnly() -->
                <div class="button-group upload-controls">
                    <input id="mode" value="add" type="hidden">
                    <button class="fm-upload" type="button" id="fm-upload">
                        <span>Upload</span>
                    </button>
                </div>
                <div class="button-group create-controls">
                    <button class="fm-btn-create-folder" type="button">
                        <span>New folder</span>
                    </button>
                </div>
                <!-- /ko -->
                <div class="button-group viewmode-controls">
                    <button class="fm-btn-grid no-label active" type="button" title="Switch to grid view">
                        <span>&nbsp;</span>
                    </button>
                    <button class="fm-btn-list no-label" type="button" title="Switch to list view">
                        <span>&nbsp;</span>
                    </button>
                </div>
            </div>
        </div>
        <div id="fm-content">
            <div id="FSlideBar">
                <div id="FRoot">
                </div>
            </div>
            <div id="Fcontent">
                <div id="fm-list">
                    <div class="ui-selectable _list">
                        <%--<ul class="grid">
                        <li class="ui-draggable ui-draggable-handle file ui-selected">

                            <div class="item-content">
                                <div class="clip">
                                    <img src="/CMS/Modules/FileManager/Libs/Images/flip.jpg" width="64">
                                </div>
                                <p>2560x1440_648706_[www.ArtFile.ru].jpg</p>
                            </div>
                        </li>
                       
                    </ul>--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="fm-footer">
            <div class="left">
                <div class="search-box" >
                    <input type="text">
                    <div class="search-box-reset"></div>
                </div>
            </div>

            <div class="right">
                <button type="button" id="btn-attack">Đính kèm</button>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

</div>
<div id="UploadFileDialog">
    <div id="UploadFile"></div>
</div>
<script src="/CMS/Modules/FileManager/Libs/contextMenu/jquery.contextMenu.min.js"></script>
<script src="/CMS/Modules/FileManager/Libs/jquery.splitter/dist/jquery-splitter.min.js"></script>
<script src="/CMS/Modules/FileManager/Libs/plupload/plupload.full.js"></script>

<script>
    var folders = <%= NewtonJson.Serialize(FolderResult())%>;
</script>


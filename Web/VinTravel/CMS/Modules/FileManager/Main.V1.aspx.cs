﻿using System;
using System.Collections.Generic;
using Mi.BO.Base.FileUpload;
using Mi.Action.Core;

namespace VinTravel.CMS.Modules.FileManager
{
    public partial class Main_V1 : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public List<FolderResult> FolderResult()
        {
            var rs = new List<FolderResult>();
            var objs = FileUploadBo.FoldersSearch();
            rs.Add(new FolderResult
            {
                id = "0",
                parent = "#",
                text = "Documents",
               // path = obj.path
            });
            foreach (var obj in objs)
            {
                rs.Add(new FolderResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name,
                    path = obj.path
                });
            }
            return rs;
        }
    }

    public class FolderResult
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string path { get; set; }
    }
}
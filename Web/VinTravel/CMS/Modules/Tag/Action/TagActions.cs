﻿using System;
using System.Collections.Generic;
using System.Web;
using Mi.BO.Base.Tag;
using Mi.Common;
using Mi.Entity.Base.Tag;
using Mi.Action;
using Mi.Action.Core;
using VinTravel.Core.Helper;

namespace VinTravel.CMS.Modules.Tag.Action
{
    public class TagActions : ActionBase
    {
        #region Implementation of IAction

        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }
        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();

            {
                switch (functionName)
                {
                    case "search":
                        responseData = Search();
                        break;

                    case "suggest":
                        responseData = Suggest();
                        break;
                    case "save":
                        responseData = Save();
                        break;
                    case "select_by_tag_name":
                        responseData = SelectByTagName();
                        break;
                    case "search_tag_name":
                        responseData = SearchByTagName();
                        break;
                    
                    case "delete":
                        break;

                    case "":
                        responseData.Success = false;
                        responseData.Message = Constants.MESG_CAN_NOT_FOUND_ACTION;
                        break;
                }
            }
            return responseData;
        }

        #endregion
        private static ResponseData Search()
        {
            var responseData = new ResponseData();
            var data = TagBo.SearchTagForSuggestion(10, -1, GetQueryString.GetPost("keyword", ""), EnumTagType.All, -1, EnumSearchTagOrder.TagHot, false);
            var tags = new List<TagReturnEntity>();
            foreach (var d in data)
            {
                TagReturnEntity tag = new TagReturnEntity();
                tag.label = d.Name;
                tag.value = d.Id;
                tags.Add(tag);
            }
            responseData = ConvertResponseData.CreateResponseData(tags, -1, "");
            return responseData;
        }
        private static ResponseData Suggest()
        {
            var responseData = new ResponseData();
            var data = TagBo.SearchTagForSuggestion(10, -1, GetQueryString.GetPost("keyword", ""), EnumTagType.All, -1, EnumSearchTagOrder.TagHot, false);
            List<string> tags = new List<string>();
            var tagsuggest = new TagSuggest();
            tagsuggest.suggestions = new List<string>();
            tagsuggest.data = new List<string>();
            tagsuggest.url = new List<string>();
            foreach (var d in data)
            {
                tagsuggest.suggestions.Add(d.Name);
                tagsuggest.data.Add(d.Id + "");
                tagsuggest.url.Add(d.Url);
            }
            responseData = ConvertResponseData.CreateResponseData(tagsuggest, -1, "");
            return responseData;
        }
        private static ResponseData Save()
        {
            var responseData = new ResponseData();
            var zoneId = GetQueryString.GetPost("zone", 0);
            var tag = GetQueryString.GetPost("tag", "").GetText();
            var type = GetQueryString.GetPost("type", 0);

            var obj = new TagEntity();
            obj.CreatedBy = UIHelper.GetCurrentUserName();
            obj.CreatedDate = DateTime.Now;
            obj.Invisibled = false;
            obj.IsHotTag = false;
            obj.Priority = 0;
            obj.Type = type;


            if (!string.IsNullOrEmpty(tag))
            {
                //   tag = tag.Substring(80);
                obj.Name = tag;
                obj.Url = Utility.UnicodeToKoDauAndGach(tag);
                obj.UnsignName = Utility.UnicodeToKoDau(tag);
                responseData = ConvertResponseData.CreateResponseData(TagBo.InsertTag(obj, zoneId, ""));

            }

            return responseData;
        }
        private ResponseData SelectByTagName()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Modules\\Tag\\TooltipContent.aspx");
            responseData.Success = true;
            return responseData;
        }
        private ResponseData SearchByTagName()
        {
            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Modules\\Tag\\Tags.aspx");
            responseData.Success = true;
            return responseData;
        }
    }
    public class TagSuggest
    {
        public List<string> suggestions { get; set; }
        public List<string> data { get; set; }
        public List<string> url { get; set; }

    }

    public class TagReturnEntity
    {
        public string label { get; set; }
        public long value { get; set; }
    }
}
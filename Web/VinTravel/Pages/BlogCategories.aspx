﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="BlogCategories.aspx.cs" Inherits="VinTravel.Pages.BlogCategories" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="VinTravel.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% 
        var path = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
        var alias = path[2];
        int totalRow = 0;
        var page = Utils.ToInt32Return0(Request.QueryString["page"]);
        //var result = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 100, ref totalRow).OrderBy(r => r.IsHot).OrderBy(r => r.CreatedDate).ToList();
    %>
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="blog-ss-1">
                        <div class="slide-blog-cate swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <%
                                    var newsHotTopic = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 10, ref totalRow).Where(h => h.IsHot == true).ToList().Count() == 0
                                        ? NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 10, ref totalRow).OrderBy(h => h.CreatedDate).Take(3).ToList()
                                        : NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 10, ref totalRow).Where(h => h.IsHot == true).ToList().OrderBy(h => h.CreatedDate).Take(3).ToList();

                                    for (int i = 0; i < newsHotTopic.Count(); i++)
                                    {
                                %>
                                <div class="swiper-slide">
                                    <div class="item mb-3">
                                        <a href="/blog/<%=alias %>/<%=newsHotTopic[i].Url %>.<%=newsHotTopic[i].Id %>.htm" title="">
                                            <img style="display: inline-block; width: auto; height: 450px" src="/Uploads/<%=newsHotTopic[i].Avatar %>" alt="" /></a><%--Uploads/<%=newsHotTopic[i].Avatar %--%>
                                        <h2 class="title">
                                            <a href="/blog/<%=alias %>/<%=newsHotTopic[i].Url %>.<%=newsHotTopic[i].Id %>.htm" title=""><%=newsHotTopic[i].Title %></a>

                                        </h2>
                                    </div>
                                </div>

                                <%} %>
                            </div>

                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                            <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                        </div>

                    </div>
                    <div class="list-news">
                        <%
                            var newsNormalTopic = new List<NewsEntity>();
                            if (newsHotTopic.Where(h => h.IsHot == true).Count() > 0)
                            {
                                newsNormalTopic = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 10, ref totalRow).Where(h => h.IsHot == false).OrderBy(h => h.CreatedDate).ToList();
                            }
                            if (newsNormalTopic.Where(h => h.IsHot == true).Count() == 0)
                                newsNormalTopic = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, 10, ref totalRow).OrderBy(h => h.CreatedDate).Skip(3).ToList();
                        %>
                        <%foreach (var item in newsNormalTopic)
                            { %>
                        <div class="item py-4 row">
                            <div class="image align-self-center col-lg-4 col-md-5 col-sm-12">
                                <a title="/blog/<%=alias %>/<%=item.Url %>.<%=item.Id %>.htm" href="<%=item.Url%>">
                                    <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid">
                                </a>
                            </div>
                            <div class="content col-lg-8 col-md-7 col-sm-12">
                                <h3 class="title"><a title="" href="/blog/<%=alias %>/<%=item.Url %>.<%=item.Id %>.htm"><%=item.Title %></a></h3>
                                <div class="time">
                                    <i class="far fa-clock mr-2"></i><%=UIHelper.ConvertToDatePicker(item.CreatedDate) %>
                                </div>
                                <div>
                                    <%= Utility.SubWordInString(item.Sapo,40) %>
                                </div>
                            </div>
                        </div>
                        <%} %>

                        <nav class="my-5">
                            <ul class="pagination justify-content-center">

                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>

                            </ul>
                        </nav>


                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="blog-ss-right ">
                        <div class="heading mt-0">
                            <a href="" title="">Tin khuyến mại</a>
                        </div>
                        <% 
                            int kmRow = 0;
                            var km_alias = "khuyen-mai";
                            var kmNews = NewsBo.SearchByShortUrl(km_alias, page == 0 ? 1 : page, 5, ref kmRow).OrderBy(r => r.IsHot).OrderByDescending(r => r.CreatedDate).ToList();
                            foreach (var item in kmNews)
                            {
                        %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/blog/<%=km_alias %>/<%=item.Url %>.<%=item.Id %>" title="">
                                    <img src="/Uploads/thumb/<%=item.Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/blog/<%=km_alias %>/<%=item.Url %>.<%=item.Id %>" title=""><%=item.Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetTime(item.CreatedDate) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                    </div>
                    <div class="blog-ss-right ">
                        <div class="heading ">
                            <a href="" title="">Tin đọc nhiều</a>
                        </div>
                        <%var topView = NewsBo.GetMostedViewNews();
                            for (int i = 0; i < topView.Count(); i++)
                            {%>

                        <%  
                            var zoneAlias = ZoneBo.GetZoneByNewsId(topView[i].Id).First().ShortUrl;
                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <h2 class="title">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(topView[i].Sapo,40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetTime(topView[i].CreatedDate) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <div class="blog-ss-right ">
                        <div class="heading ">
                            Video
                        </div>
                        <%var mediaList = NewsBo.GetTop4VideoNews();
                            foreach (var item in mediaList)
                            {
                                var mediaAlias = ZoneBo.GetZoneByNewsId(item.Id).First();

                        %>
                        <% string data = item.Body;
                                %>
                                <iframe width="100%" height="100%" class="mb-3" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <%}%>
                        

                       <%-- <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        <iframe width="100%" height="100%" class="mb-3" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--%>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="">
    </section>
    </div>
</asp:Content>

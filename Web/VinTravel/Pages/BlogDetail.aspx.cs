﻿using Mi.BoCached.CacheObjects;
using Mi.BoCached.Common;
using Mi.Entity.Base.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VinTravel.Pages
{
    public partial class BlogDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected List<TagNewsWithTagInfoEntity> GetTags(long newsId)
        {
            var tags = CacheObjectBase.GetInstance<TagCached>().GetTagNewsByNewsId(newsId);
            return tags;
        }
    }
}
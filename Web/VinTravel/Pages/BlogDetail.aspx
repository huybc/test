﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="VinTravel.Pages.BlogDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="VinTravel.Core.Helper" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%
        var path = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
        var alias = path[2];
        var newsId = path[3].Split('.')[1].ToString();
        var zoneName = ZoneBo.GetZoneByAlias(alias);
        var newDetail = NewsBo.GetNewsDetailById(int.Parse(newsId));
    %>
    <div class="blog-detail">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <section class="breadcrumb breadcrumb-blog-detail">
                        <div class="breadcrumb-item parent"><a href="/blog" title="">Blog</a></div>
                        <div class="breadcrumb-item"><a href="/blog/<%=alias %>" title=""><%=zoneName.Name %></a></div>
                    </section>
                    <div class="d-flex mb-4">
                        <div class="time align-self-center">
                            Thứ sáu, 6/12/2018 | 09:05
                        </div>
                        <div class="share d-flex ml-auto">
                            <span class="mr-2 align-self-center">Chia sẻ</span>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/fb-ic.png" /></a>
                            <a class="mr-2" href="">
                                <img src="/Themes/imgs/gg-ic.png" /></a>
                        </div>
                    </div>

                    <section class="content-blog">
                        <h1 class="title"><%=newDetail.Title %></h1>
                        <div class="description">
                            <%=newDetail.Sapo %>
                        </div>
                        <%--<p>
                            Được mệnh danh là thiên đường vui chơi giải trí hàng đầu tại Việt Nam, Vinpearl Land Nha Trang từ khu vui chơi đầu tiên trên đảo Hòn Tre cách đây 9 năm, tới nay đã phát triển thành hệ thống quy mô và danh tiếng với các khu vui chơi ngoài trời và trong
                            nhà.
                        </p>
                        <p>
                            Mất hơn một giờ đi tàu từ cảng Cầu Đá, bạn sẽ đến Hòn Nội – một đảo yến thuộc thành phố Nha Trang. Điểm độc đáo của Hòn Nội là nơi đây có bãi tắm đôi duy nhất ở Việt Nam.
                        </p>
                        <div class="mb-3">
                            <img src="imgs/blog-detail.jpg" class="img-fluid m-auto" />
                        </div>
                        <p>
                            Mất hơn một giờ đi tàu từ cảng Cầu Đá, bạn sẽ đến Hòn Nội – một đảo yến thuộc thành phố Nha Trang. Điểm độc đáo của Hòn Nội là nơi đây có bãi tắm đôi duy nhất ở Việt Nam. Đến đây, đứng ở bãi cát trắng mịn, quay hướng nào bạn cũng nhìn thấy biển xanh ngắt
                            một màu. Chưa dừng ở đó, bãi tắm đôi này còn có một bên nước nóng, một bên nước lạnh do các dòng chảy tạo nên, khiến bao du khách tham quan Nha Trang đã một lần đến thì không thể nào quên.
                        </p>
                        <div class="mb-3">
                            <img src="imgs/blog-detail.jpg" class="img-fluid m-auto" />
                        </div>
                        <p>
                            Mất hơn một giờ đi tàu từ cảng Cầu Đá, bạn sẽ đến Hòn Nội – một đảo yến thuộc thành phố Nha Trang. Điểm độc đáo của Hòn Nội là nơi đây có bãi tắm đôi duy nhất ở Việt Nam. Đến đây, đứng ở bãi cát trắng mịn, quay hướng nào bạn cũng nhìn thấy biển xanh ngắt
                            một màu. Chưa dừng ở đó, bãi tắm đôi này còn có một bên nước nóng, một bên nước lạnh do các dòng chảy tạo nên, khiến bao du khách tham quan Nha Trang đã một lần đến thì không thể nào quên.
                        </p>
                        --%>
                        <%=newDetail.Body %>
                        <p class="text-right">
                            <%=newDetail.Author %>
                        </p>
                    </section>

                    <section class="blog-detail-tag d-inline-block">
                        <div class="heading">
                            Tag
                        </div>
                        <%
                            var tags = GetTags(newDetail.Id);
                            foreach (var item in tags)
                            {
                        %>
                        <div class="item-tag">
                            <%=item.Name %>
                        </div>
                        <%} %>


                        <%--<div class="item-tag">
                            phượt thủ
                        </div>
                        <div class="item-tag">
                            bún chả
                        </div>--%>
                    </section>
                    <%--<section class="slide-blog slide-blog-detail">
                        <div class="heading ">
                            <a href="" title="">Có thể bạn quan tâm</a>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-12">
                                <div class="swiper-container">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <!-- Get post by same tag -->

                                        <div class="swiper-slide">
                                            <div class="item-slide">
                                                <div class="image">
                                                    <a href="" title="">
                                                        <img src="imgs/slide1.png" alt="" class="w-100 h-100" /></a>
                                                </div>
                                                <h3 class="title">
                                                    <a href="" title="">Check-in quán The Ylang Gardenista Coffee view đẹp quên lối về ở Hà Nội
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item-slide">
                                                <div class="image">
                                                    <a href="" title="">
                                                        <img src="imgs/slide2.png" alt="" class="w-100 h-100" /></a>
                                                </div>
                                                <h3 class="title">
                                                    <a href="" title="">Check-in quán The Ylang Gardenista Coffee view đẹp quên lối về ở Hà Nội
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item-slide">
                                                <div class="image">
                                                    <a href="" title="">
                                                        <img src="imgs/slide3.png" alt="" class="w-100 h-100" /></a>
                                                </div>
                                                <h3 class="title">
                                                    <a href="" title="">Check-in quán The Ylang Gardenista Coffee view đẹp quên lối về ở Hà Nội
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- If we need navigation buttons -->
                                    <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                                    <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </section>--%>
                    <section class="list-news">
                        <div class="heading ">
                            <a href="" title="">Cùng chuyên mục</a>
                        </div>
                        <%
                            var sameZone = NewsBo.GetByZoneId(zoneName.Id, 4).OrderByDescending(r => r.CreatedDate).Take(4);
                            foreach (var item in sameZone)
                            {%>
                        <div class="item py-4 row">
                            <div class="image align-self-center col-lg-4 col-md-5 col-sm-12">
                                <a title="" href="/blog/<%=alias %>/<%=item.Url %>.<%=item.Id %>">
                                    <img src="/Uploads/thumb/<%=item.Avatar %>" class="img-fluid">
                                </a>
                            </div>
                            <div class="content col-lg-8 col-md-7 col-sm-12">
                                <h3 class="title"><a title="" href="/blog/<%=alias %>/<%=item.Url %>.<%=item.Id %>"><%=item.Title %></a></h3>
                                <div class="time">
                                    <i class="far fa-clock mr-2"></i><%=UIHelper.GetFullDate(item.CreatedDate) %>
                                </div>
                                <div><%=Utility.SubWordInString(item.Sapo, 40)%>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </section>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">

                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="">Tin đọc nhiều</a>
                        </div>
                        <%var topView = NewsBo.GetMostedViewNews();
                            for (int i = 0; i < topView.Count(); i++)
                            {%>

                        <%  
                            var zoneAlias = ZoneBo.GetZoneByNewsId(topView[i].Id).First().ShortUrl;
                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <h2 class="title">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(topView[i].Sapo,40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetTime(topView[i].CreatedDate) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <section class="blog-ss-right ">
                        <div class="heading ">
                            <a href="" title="">Video</a>
                        </div>
                        <%var mediaList = NewsBo.GetTop4VideoNews();
                            foreach (var item in mediaList)
                            {
                                var mediaAlias = ZoneBo.GetZoneByNewsId(item.Id).First();

                        %>
                        <% string data = item.Body;
                                %>
                                <iframe width="100%" height="100%" class="mb-3" src="<%=data %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <%}%>


                    </section>

                </div>

            </div>
        </div>
    </div>

    </div>
</asp:Content>

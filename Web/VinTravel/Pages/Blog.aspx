﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="VinTravel.Pages.Blog" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="VinTravel.Core.Helper" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var result = ZoneBo.GetAllZone((int)Mi.Entity.Base.Zone.ZoneType.Blog); %>
    <section class="blog-ss-1">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="item mb-3">
                        <a href="" title="">
                            <img src="Themes/imgs/9-diem-den-bi-huy-hoai-nghiem-trong-vi-mang-xa-hoi-ivivu-1.jpg" alt="" /></a>
                        <h2 class="title">
                            <a href="" title="">Note ngay top 6 quán trà chiều Sài Gòn sang chảnh để tụ tập với hội bạn
                                thân</a>

                        </h2>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="item mb-3 item-right">
                        <a href="" title="">
                            <img src="Themes/imgs/4-quan-bar-view-bien-lang-man-nhu-phim-ivivu-10-370x215.jpg" alt="" /></a>
                        <h2 class="title">
                            <a href="" title="">Du lịch Vũng Tàu: Cẩm nang từ A đến Z</a>

                        </h2>
                    </div>
                    <div class="item mb-3 item-right">
                        <a href="" title="">
                            <img src="Themes/imgs/9-diem-den-bi-huy-hoai-nghiem-trong-vi-mang-xa-hoi-ivivu-1.jpg" alt="" /></a>
                        <h2 class="title">
                            <a href="" title="">Du lịch Vũng Tàu: Cẩm nang từ A đến Z</a>
                        </h2>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="media">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading text-uppercase">
                        <a href=" " title="">Media</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <%var mediaList = NewsBo.GetTop4VideoNews();
                    foreach (var item in mediaList)
                    {
                        var mediaAlias = ZoneBo.GetZoneByNewsId(item.Id).First();

                %>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <% string data = item.Body;                            
                            %>
                        <iframe width="100%" height="100%" src="<%=item.Body %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a href="/blog/<%=mediaAlias %>/<%=item.Url %>.<%=item.Id %>" title=""><%=item.Title %></a>
                            </h3>
                            <div class="date">
                                <span class="acc"><%=item.Author %></span>
                                <span class=""><%=UIHelper.GetFullDate(item.CreatedDate) %></span>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>

<%--                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a href="" title="">Du lịch Vũng Tàu: Cẩm nang từ A đến Z</a>
                            </h3>
                            <div class="date">
                                <span class="acc">BienTapVien</span>
                                <span class="">T12 19, 2017</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a href="" title="">Du lịch Vũng Tàu: Cẩm nang từ A đến Z</a>
                            </h3>
                            <div class="date">
                                <span class="acc">BienTapVien</span>
                                <span class="">T12 19, 2017</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <div class="item">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/YlGsuRAUdG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="text">
                            <h3 class="title">
                                <a href="" title="">Du lịch Vũng Tàu: Cẩm nang từ A đến Z</a>
                            </h3>
                            <div class="date">
                                <span class="acc">BienTapVien</span>
                                <span class="">T12 19, 2017</span>
                            </div>
                        </div>
                    </div>
                </div>--%>

            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">

                    <%foreach (var item in result)
                        { %>

                    <div class="blog-ss-2">
                        <%--<div class="heading">
                            <a href="blog-category.html" title="">Điểm đến</a>
                        </div>--%>
                        <%if (item.SortOrder == 1)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, 5).ToList();
                        %>
                        <div class="wp">
                            <div class="heading">
                                <a href="/blog/<%=item.ShortUrl %>" title=""><%=item.Name %></a>
                            </div>
                            <div class="row">
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>
                                        <div class="detail">
                                            <%=Utility.SubWordInString(news[i].Sapo.ToString(),40) %>
                                        </div>

                                    </div>
                                </div>
                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i > 1 && i < news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/thumb/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i == news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/thumb/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                    </div>


                    <div class="blog-ss-2 mb-4">
                        <%if (item.SortOrder == 2)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, 5).ToList();
                        %>
                        <div class="wp mb-3">
                            <div class="heading">
                                <a class="" title="" href="/blog/<%=item.ShortUrl %>"><%=item.Name %></a>
                            </div>
                            <%for (int i = 0; i < news.Count(); i++)
                                { %>
                            <%if (i == 0)
                                {%>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>

                                    </div>
                                </div>

                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>

                                    </div>
                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <div class="slide-blog">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="swiper-container">
                                        <!-- Additional required wrapper -->
                                        <div class="swiper-wrapper">
                                            <!-- Slides -->
                                            <%for (int i = 0; i < news.Skip(2).Count(); i++)
                                                { %>
                                            <div class="swiper-slide">
                                                <div class="item-slide">
                                                    <div class="image">
                                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                            <img src="Uploads/<%=news[i].Avatar %>" alt="" class="w-100 h-100" /></a>
                                                    </div>
                                                    <h3 class="title">
                                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %>
                                                        </a>
                                                    </h3>
                                                </div>
                                            </div>

                                            <%} %>
                                        </div>

                                        <!-- If we need navigation buttons -->
                                        <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                                        <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>
                    <%if (item.SortOrder == 3)
                        {
                            var news = NewsBo.GetByZoneId(item.Id, 5).ToList();
                    %>

                    <div class="row accordion">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">

                            <div class="blog-ss-right mb-3">
                                <%--<p>abc</p>--%>
                                <div class="heading-2">
                                    <a href="/blog/<%=item.ShortUrl %>" title=""><%=item.Name %></a>
                                </div>
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                            <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <% =Utility.SubWordInString(news[i].Sapo,40) %>
                                    </div>

                                </div>
                                <%} %>
                                <%if (i > 0)
                                    { %>
                                <div class="item-right d-flex">
                                    <div class="image">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                            <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                    </div>
                                    <div class="text">
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>
                                        <div class="time">
                                            <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                        </div>
                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                        <%if (item.SortOrder == 4)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, 5).ToList();
                        %>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12">

                            <div class="blog-ss-right mb-3">

                                <%--<p>abc</p>--%>
                                <div class="heading-2">
                                    <a href="/blog/<%=item.ShortUrl %>" title=""><%=item.Name %></a>
                                </div>
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="item-left mb-3">
                                    <div class="image">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                            <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                    </div>
                                    <h2 class="title">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                    </h2>
                                    <div class="detail">
                                        <% =Utility.SubWordInString(news[i].Sapo,40) %>
                                    </div>

                                </div>
                                <%} %>
                                <%if (i > 0)
                                    { %>
                                <div class="item-right d-flex">
                                    <div class="image">
                                        <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                            <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                    </div>
                                    <div class="text">
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>
                                        <div class="time">
                                            <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                        </div>
                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>

                    </div>
                    <%} %>
                    <div class="blog-ss-2">
                        <%--<div class="heading">
                            <a href="blog-category.html" title="">Điểm đến</a>
                        </div>--%>
                        <%if (item.SortOrder >= 5)
                            {
                                var news = NewsBo.GetByZoneId(item.Id, 5).ToList();
                        %>
                        <div class="wp">
                            <div class="heading">
                                <a href="/blog/<%=item.ShortUrl %>" title=""><%=item.Name %></a>
                            </div>
                            <div class="row">
                                <%for (int i = 0; i < news.Count(); i++)
                                    { %>
                                <%if (i == 0)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-left">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                        </h2>
                                        <div class="detail">
                                            <%=Utility.SubWordInString(news[i].Sapo.ToString(),40) %>
                                        </div>

                                    </div>
                                </div>
                                <%} %>
                                <%if (i == 1)
                                    { %>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i > 1 && i < news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                    <%if (i == news.Count() - 1)
                                        { %>
                                    <div class="item-right d-flex">
                                        <div class="image">
                                            <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title="">
                                                <img src="Uploads/<%=news[i].Avatar %>" alt="" /></a>
                                        </div>
                                        <div class="text">
                                            <h2 class="title">
                                                <a href="/blog/<%=item.ShortUrl %>/<%=news[i].Url %>.<%=news[i].Id %>.htm" title=""><%=news[i].Title %></a>
                                            </h2>
                                            <div class="time">
                                                <%=UIHelper.GetTime(news[i].CreatedDate) %>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <%} %>
                                <%} %>
                            </div>

                        </div>
                        <%} %>
                    </div>
                    <%} %>
                </div>


                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="">Tin đọc nhiều</a>
                        </div>
                        <%var topView = NewsBo.GetMostedViewNews();
                            for (int i = 0; i < topView.Count(); i++)
                            {%>

                        <%  
                            var zoneAlias = ZoneBo.GetZoneByNewsId(topView[i].Id).First().ShortUrl;
                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <h2 class="title">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(topView[i].Sapo,40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title="">
                                    <img src="/Uploads/thumb/<%=topView[i].Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/blog/<%=zoneAlias %>/<%=topView[i].Url %>.<%=topView[i].Id %>.htm" title=""><%=topView[i].Title %></a>
                                </h2>
                                <div class="time">
                                    <%=UIHelper.GetTime(topView[i].CreatedDate) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                    <div class="ads">
                        <a href="" title="">
                            <img src="/Themes/imgs/ads.jpg" class="img-fluid mb-3" alt=""></a>
                    </div>
                    <div class="blog-ss-right mb-3">
                        <div class="heading">
                            <a title="" href="">Mới cập nhật</a>
                        </div>
                        <%var newestNews = NewsBo.GetTopNewestNews();
                            for (int i = 0; i < newestNews.Count(); i++)
                            {%>

                        <%  
                            var zoneAlias = ZoneBo.GetZoneByNewsId(newestNews[i].Id).First().ShortUrl;
                            if (i == 0)
                            { %>

                        <div class="item-left mb-3">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=newestNews[i].Url %>.<%=newestNews[i].Id %>.htm" title="">
                                    <img src="/Uploads/<%=newestNews[i].Avatar %>" alt="" /></a>
                            </div>
                            <h2 class="title">
                                <a href="/blog/<%=zoneAlias %>/<%=newestNews[i].Url %>.<%=newestNews[i].Id %>.htm" title=""><%=newestNews[i].Title %></a>
                            </h2>
                            <div class="detail">
                                <%=Utility.SubWordInString(newestNews[i].Sapo,40) %>
                            </div>

                        </div>
                        <%} %>
                        <%if (i > 0)
                            { %>
                        <div class="item-right d-flex">
                            <div class="image">
                                <a href="/blog/<%=zoneAlias %>/<%=newestNews[i].Url %>.<%=newestNews[i].Id %>.htm" title="">
                                    <img src="/Uploads/thumb/<%=newestNews[i].Avatar %>" alt="" /></a>
                            </div>
                            <div class="text">
                                <h2 class="title">
                                    <a href="/blog/<%=zoneAlias %>/<%=newestNews[i].Url %>.<%=newestNews[i].Id %>.htm" title=""><%=newestNews[i].Title %></a>
                                </h2>
                                git 
                                <div class="time">
                                    <%=UIHelper.GetTime(newestNews[i].CreatedDate) %>
                                </div>
                            </div>

                        </div>
                        <%} %>
                        <%} %>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="Tours.aspx.cs" Inherits="VinTravel.Pages.Tours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="banner-vmb mb-4">
        <img src="/Themes/imgs/banner-tours.jpg" class="w-100" />
        <div class="ovelay">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="heading">
                            <div class="top">
                                Đặt tours du lịch!
                            </div>
                            <div class="bot m-2">
                                Hơn 3000 tour Quốc tế và trong nước
                            </div>
                        </div>
                        <div class="search-box mb-2">
                            <div class="row no-gutters">
                                <div class="gr-select">
                                    <div class="row no-gutters ">

                                        <div class="col px-1">
                                            <div class="input-departure dropdown ">
                                                <input class="form-control input-search-box pl-5 dropdown-toggle " data-toggle="dropdown" placeholder="Bạn muốn đi đâu?" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/location-ic.png" class=" mr-2" />

                                                </div>
                                                <div class="suggest-select dropdown-menu ">
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hải Phòng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Nẵng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hồ Chí Minh</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="col px-1">
                                            <div class="input-departure">
                                                <input class="form-control input-search-box pl-100" name="date" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/cal.png" class=" mr-2" />
                                                    <div class="align-self-center">Ngày đi:</div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col px-1 ">
                                            <div class="input-departure dropdown">
                                                <input class="form-control input-search-box dropdown-toggle" data-toggle="dropdown" placeholder="" />
                                                <div class="input-icon">
                                                    <img src="/Themes/imgs/input-departure.png" class=" mr-2" />
                                                    <div class="align-self-center">Khởi hành:</div>
                                                </div>
                                                <div class="suggest-select dropdown-menu dropdown-menu-right w-100">
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="color-66666 small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hải Phòng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Nẵng</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Hồ Chí Minh</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">
                                                        <img src="/Themes/imgs/undefined-60x60.jpg" height="40" width="40" class="align-self-center mr-3" />
                                                        <span class="align-self-center font-weight-bold">Đà Lạt</span>
                                                        <span class="small align-self-center ml-auto">138 KS</span>
                                                    </a>

                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                </div>

                                <div class="gr-btn px-1 ">
                                    <a class="btn btn-warning text-uppercase d-flex w-100 h-100 justify-content-center " style="background: #fe8c00;"><span class="align-self-center">Tìm kiếm</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="font-italic  px-1">
                            <span class="" style="color: #fe8c00;">Các điểm đến đang hot:</span> Đà Nẵng, Phú Quốc, Tam Đảo, Nha Trang, Hạ Long
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="list-combo list-tour container">
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">Tours du lịch HOT

                        </h3>
                        <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                    </div>
                    <div class="right">
                        <a href="" class="font-italic color-0072ff" title="">Xem tất cả</a>
                    </div>
                </section>
                <section class="list row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour wish">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">Tours Giảm Giá

                        </h3>
                        <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                    </div>
                    <div class="right">
                        <a href="" class="font-italic color-0072ff" title="">Xem tất cả</a>
                    </div>
                </section>
                <section class="list row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour wish">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">Ưu đãi HOT hôm nay

                        </h3>
                        <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                    </div>
                    <div class="right">
                        <a href="" class="font-italic color-0072ff" title="">Xem tất cả</a>
                    </div>
                </section>
                <section class="list row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour wish">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
        <div class="row tour-cate">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <section class="ss-name">
                    <div class="left">
                        <h3 class="tit">Tours Giảm Giá

                        </h3>
                        <div>Làm việc cả năm. Đến lúc tưởng thưởng</div>
                    </div>
                    <div class="right">
                        <a href="" class="font-italic color-0072ff" title="">Xem tất cả</a>
                    </div>
                </section>
                <section class="list row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour hot">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour wish">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour normal">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                        <div class="item item-tour sell">
                            <div class="image">
                                <a href="" title="">
                                    <img src="/Themes/imgs/combo.jpg" alt="" /></a>
                                <div class="flag">
                                </div>
                                <div class="text">Vietjet</div>
                                <div class="flag top ">
                                </div>
                                <div class="text top ">Tour HOT</div>

                            </div>
                            <div class="content px-3 ">
                                <div class="tit">
                                    <a href="" title="">Tour Indonesia 4N3D: Thiên Đường Nghỉ Dưỡng Bali</a>
                                </div>

                                <div class="d-flex mb-2">
                                    <div class="time ">
                                        <i class="far fa-clock mr-2"></i>
                                        <span class="">4 Ngày 3 Đêm </span>
                                    </div>
                                    <div class="cost ml-auto">
                                        5.500.000 <sup>VNĐ / người</sup>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

            </div>
        </div>
    </section>

    <section class="suggest py-5">
    </section>
</asp:Content>

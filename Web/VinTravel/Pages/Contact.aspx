﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/VinTravel.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="VinTravel.Pages.Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadCph" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <section class="banner-vmb mb-4">
        <img src="/Themes/imgs/bg-lien-he.jpg" class="w-100" />
        <div class="ovelay">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="heading">
                            <div class="top text-center">
                                Liên hệ
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="box">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-12 left">
                            <h4 class="heading">
                                Liên hệ
                            </h4>
                            <div class="form-group row">
                                <label for="example-text-input" class=" col-md-3 col-sm-12 col-12 col-form-label">Họ và tên:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                  <input class="form-control" type="text"  id="txtName">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="example-search-input" class="col-md-3 col-sm-12 col-12 col-form-label">Số điện thoại:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                  <input class="form-control" type="tel"  id="txtPhoneNumber">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="example-email-input" class="col-md-3 col-sm-12 col-12 col-form-label">Email:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                  <input class="form-control" type="email"  id="txtEmail">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="example-email-input" class="col-md-3 col-sm-12 col-12 col-form-label">Lời nhắn:</label>
                                <div class="col-md-9 col-sm-12 col-12">
                                  <textarea class="form-control" type="email" rows="4" id="txtNote"></textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="col-md-3 col-sm-12 col-12 "></div>
                                <div class="col-md-9 col-sm-12 col-12">
                                  <button class="btn btn-book" id="btnSendContact">Gửi ngay</button>
                                </div>
                              </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-12 d-flex  right">
                            <div class="align-self-center">
                                <div class="text-uppercase mb-3">
                                    Gọi ngay
                                </div>
                                <div class="h3 font-weight-bold mb-4">
                                    0963 580 660
                                </div>
                                <div class="text-uppercase font-weight-bold mb-3">
                                    Công ty Cổ phần Vintravel
                                </div>
                                <ul class="info-company">
                                    <li>Địa chỉ: P31908 CC Imperial Plaza, 360 Giải Phóng, Phương Liệt, Thanh Xuân, Hà Nội
            
                                    </li>
                                    <li>Hotline: 0963580660</li>
                                    <li>Email: res@vintraveljsc.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="w-100 mt-5">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.162497187041!2d105.83827291440664!3d20.98612239460954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac680665939b%3A0xe8385889f0083c91!2zMzYwIEdp4bqjaSBQaMOzbmcsIFBoxrDGoW5nIExp4buHdCwgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1566484926837!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>
</asp:Content>

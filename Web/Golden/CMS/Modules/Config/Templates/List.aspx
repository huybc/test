﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Golden.CMS.Modules.Config.Templates.List" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%
    var groupKey = GetQueryString.GetPost("page", string.Empty);
    // groupKey = !string.IsNullOrEmpty(groupKey) ? groupKey : "InfoConfig";
    
    var objs = ConfigBo.GetListConfigByPage(groupKey, -1).ToList();
   
%>
   
        <thead>
        <tr>
            <th>Tên</th>
            <th>Giá trị</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
    <%
        for (int i = 0; i < objs.Count(); i++)
        {
        if (objs[i].ConfigGroupKey.Equals("InfoConfig") || objs[i].ConfigGroupKey.Equals("SourceConfig"))
        { %>
       
            <tr data="<%= objs[i].ConfigName.ToLower() %>">
                <td>[<%= objs[i].ConfigName %>] &nbsp;<b><%= objs[i].ConfigLabel %></b>
                </td>
                <td>
                    <% if (objs[i].ConfigValueType == (int)EnumStaticHtmlTemplateType.Images)
                       { %>
                        <a class="avatar">
                            <img class="thumb" src="<%= UIHelper.Thumb_W(60, objs[i].ConfigValue) %>" width="60" />
                        </a>
                    <% }
                       else
                       { %>
                        <%= !string.IsNullOrEmpty(objs[i].ConfigValue) ? Utility.SubWordInString(objs[i].ConfigValue.GetText(), 50) : "" %>
                    <% } %>
                </td>
                <td><i class="icon-pencil7 edit" title="Cập nhật"></i></td>
            </tr>
           
    <%
        }
    }%>
        </tbody>
  

﻿R.FileManager = {
    Init: function () {
        this.dialogFileManager = null;
        this.multi = null;
        this.plUploadFile = null;
        this.eljstree = '#FRoot';
        this.folderSelected = 0;

        R.FileManager.RegisterEvents();

    },
    RegisterEvents: function () {
        
        $('#fm-upload').off('click').on('click', function () {
            if (parseInt(R.FileManager.folderSelected) === 0) {
                alert('Chọn folder lưu tệp');
                return;
            }
            R.FileManager.Upload();
        });
        
    },

    Open: function (callback,multi) {
        if ($('#fm-container').length === 0) {
            $('body').append('<div id="fm-container-dialog"></div>');
        }

        R.Post({
            params: {},
            module: "fmanager",
            ashx: 'modulerequest.ashx',
            action: "file_manager_open",
            success: function (res) {
                if (res.Success) {
                    var $elDialog = $('#fm-container-dialog');
                    R.FileManager.dialogFileManager = $elDialog.dialog({
                        autoOpen: true,
                        height: 515,
                        width: 800,
                        show: "fadein",
                        hide: "fadeout",
                        modal: false,
                        zIndex: 500,
                        buttons: {

                        },
                        create: function () {

                        },
                        close: function () {
                            $("body").css({ overflow: 'inherit' }).find('.ui-overlay').remove();
                            R.FileManager.dialogFileManager.dialog("destroy");
                        },
                        open: function (event, ui) {
                            R.FileManager.multi = multi;
                            $("body").css({ overflow: 'hidden' }).append('<div class="ui-overlay"></div>');

                            $elDialog.html(res.Content);
                            setTimeout(function () {
                                $("#fm-content").splitter({
                                    type: "v",
                                    outline: true,
                                    sizeLeft: 180,
                                    minLeft: 180,
                                    minRight: 200,
                                    resizeToWidth: true,
                                    dock: "right",
                                    dockSpeed: 200,
                                    dockKey: 'Z', // Alt-Shift-Z in FF/IE
                                    accessKey: 'I' // Alt-Shift-I in FF/IE
                                });
                                R.FileManager.Search();
                            });
                            
                            
                        }
                    });

                    R.FileManager.JSTree();

                } else {
                    R.MessageTextError();
                    console.log(res.Message);
                }

                // $(el).RModuleUnBlock();
            }
        });
    },
    CreateFolder: function ($node, action) {
        if ($('#create-folder').length === 0) {
            $('#fm-container').append('<div id="create-folder" style="" ></div>');
        }
        var $el = $('#create-folder');
        $el.dialog({
            autoOpen: true,
            height: 170,
            width: 300,
            show: "fadein",
            hide: "fadeout",
            modal: true,
            buttons: {

            },
            close: function () {
                $('#create-folder').html('');
            },
            open: function (event, ui) {
                $el.closest('.ui-dialog').find(".ui-dialog-titlebar").remove();
                var htm = '';
                if (action === 'rename') {
                    htm = '<div class="content"><div data-alertify-msg=""><label>Nhập tên mới</label><input data-alertify-input="" id="folderName" value="' + $node.text + '" id="_newFolder" type="text"></div><nav data-alertify-btn-holder=""><button data-alertify-btn="ok" tabindex="1" class="_save">Cập nhật</button><button class="_cancel" data-alertify-btn="cancel" tabindex="2">Cancel</button></nav></div>';
                } else {
                    htm = '<div class="content"><div data-alertify-msg=""><label>Nhập tên folder</label><input data-alertify-input="" id="folderName" value="New Folder" id="_newFolder" type="text"></div><nav data-alertify-btn-holder=""><button data-alertify-btn="ok" tabindex="1" class="_save">Create folder</button><button class="_cancel" data-alertify-btn="cancel" tabindex="2">Cancel</button></nav></div>';

                }

                $el.html(htm);

                setTimeout(function () {
                    $('button._cancel').click(function () {
                        $el.dialog("close");

                    });
                    $('button._save').click(function () {
                        var name = $('#folderName').val();
                        var rx = /[<>:"\/\\|?*\x00-\x1F]|^(?:aux|con|clock\$|nul|prn|com[1-9]|lpt[1-9])$/i;
                        if (rx.test(name)) {
                            $('#folderName').val('').addClass('ms_error');
                            alert('Tên không chưa ký tự: <>:"\/\\|?*');
                            return;

                        }
                        R.FileManager.FolderSave($node, name, action);
                    });


                });

            }
        });
    },
    JSTree: function () {
        //console.log('js tree')
        var tree = $(R.FileManager.eljstree).jstree({
            "core": {
                "animation": 0,
                "check_callback": function (op, node, par, pos, more) {
                    if ((op === "move_node" || op === "copy_node") && node.type && node.type == "root") {
                        return false;
                    }
                    if ((op === "move_node" || op === "copy_node") && more && more.core && !confirm('Xác nhận !')) {

                        return false;
                    }

                    if ((op === "delete_node" || op === "delete_node") && more && more.core && !confirm('Xác nhận 1 !')) {
                        return false;
                    }

                    // return true;
                },
                "themes": { "stripes": true },
                'data': folders
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 4,
                    "valid_children": ["root"]
                },
                "root": {
                    //  "icon": "/static/3.3.3/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "contextmenu": {
                "items": function ($node) {
                    //    var tree = $("#tree").jstree(true);
                    return {
                        "Create": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Create",
                            icon: 'fa fa-edit',
                            "action": function (obj) {
                                R.FileManager.CreateFolder($node, 'create');
                            }
                        },
                        "Rename": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Rename",
                            icon: 'fa tag',

                            "action": function (obj) {
                                R.FileManager.CreateFolder($node, 'rename');

                            }
                        },
                        "Remove": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Remove",
                            "action": function (obj) {
                                if (confirm('Xác nhận xóa !')) {
                                    R.Post({
                                        params: { node_id: $node.id },
                                        module: "fmanager",
                                        ashx: 'modulerequest.ashx',
                                        action: "folder_remove",
                                        success: function (res) {
                                            if (res.Success) {
                                                var tree = $(R.FileManager.eljstree).jstree(true);
                                                tree.delete_node([$node]);
                                            } else {
                                                alert(res.Message);
                                            }
                                        }
                                    });
                                }


                            },
                            icon: 'fa fa-trash'
                        },
                        "Edit": {
                            "separator_before": false,
                            "separator_after": true,
                            "label": "Edit",
                            "action": false,

                            "submenu": {
                                "Cut": {
                                    "seperator_before": false,
                                    "seperator_after": false,
                                    "label": "Cut",
                                    "icon": "fa cut",
                                    action: function (obj) {
                                        alert("Cut");
                                        // this.create(obj, "last", { "attr": { "rel": "Cut" } });
                                    }
                                },
                                "Copy": {
                                    "seperator_before": false,
                                    "seperator_after": false,
                                    "label": "Copy",
                                    "icon": "fa copy",
                                    action: function (obj) {
                                        alert("Copy");
                                        //   this.create(obj, "last", { "attr": { "rel": "Copy" } });
                                    }
                                },
                                "Pate": {
                                    "seperator_before": false,
                                    "seperator_after": false,
                                    "label": "Pate",
                                    "icon": "fa paste",
                                    action: function (obj) {
                                        alert("Pate");
                                        //  this.create(obj, "last", { "attr": { "rel": "service" } });
                                    }
                                }
                            }
                        }
                    };
                }
            },
            "plugins": [
              "contextmenu", "dnd", "search", "state", "types", "wholerow"
            ]
        }).bind("move_node.jstree", function (e, data, node, par) {

            //var parent = data.node.parent;// new parent
            //var old_parent = data.node.old_parent;// old_parent
            //var id = data.node.id;
            //var name = data.node.text;


            var _data = {
                id: data.node.id,
                parent: data.node.parent,// new parent,
                text: data.node.text
            }
            R.FileManager.FolderSave(_data, '', 'move');

        }).on('delete_node.jstree', function (e, data) {

        }).bind("select_node.jstree", function (evt, data) {
            R.FileManager.folderSelected = data.node.id;
            $('#fm-wrapper').attr('node-select', data.node.id);
            R.FileManager.Search();
        });;
    },
    FolderSave: function (node, name, action) {

        var data = {
            'node_id': node.id,
            'node_name': node.text,
            'parent_id': node.parent,
            'name': name,
            'action': action

        }
        R.Post({
            params: data,
            module: "fmanager",
            ashx: 'modulerequest.ashx',
            action: "folder_save",
            success: function (res) {
                if (res.Success) {
                    if (action === 'create') {
                        $(R.FileManager.eljstree).jstree().create_node(node.id, {
                            "id": res.Data,
                            "text": name
                        }, "last", function () {
                            console.log('add node success !');
                        });
                    }
                    if (action === 'rename') {
                        //rename node
                        $(R.FileManager.eljstree).jstree('rename_node', node.id, name);

                    }
                    if (action === 'move') {

                    }

                    $('#create-folder').dialog('close');
                } else {
                    alert(res.Message);
                }
            }
        });
    },
    Upload: function () {
        $('#UploadFileDialog').dialog({
            autoOpen: true,
            height: 334,
            width: 600,
            show: "fadein",
            hide: "fadeout",
            modal: true,
            buttons: {

            },
            close: function () {
                // $('#UploadFile').html('');

                R.FileManager.plUploadFile.bind("FileUploaded", function (upload, file, response) {
                    upload.removeFile(file);

                });
                $('#UploadFileDialog').dialog("destroy");
                //   R.FileManager.plUploadFile.unbind();
            },
            create: function (event, ui) {

                $('#UploadFile').pluploadQueue({
                    runtimes: 'html5,silverlight,flash,html4',
                    url: '/CMS/Modules/FileManager/Action/ImageUploadHandler.ashx',
                    max_file_size: '2mb',
                    chunk_size: '64kb',
                    unique_names: false,
                    // Resize images on clientside if we can
                    resize: { width: 800, height: 600, quality: 90 },
                    // Specify what files to browse for
                    filters: [
                    { title: "Image files", extensions: "jpg,jpeg,gif,png,bmp" },
                    { title: "Zip files", extensions: "zip,rar" },
                    { title: "Doc files", extensions: "doc, xls, pps, docx, xlsx, ppsx,vsd" },
                    { title: "PDF files", extensions: "pdf" }

                    ],
                    // flash_swf_url: 'scripts/plupload/plupload.flash.swf',
                    // silverlight_xap_url: 'scripts/plupload/plupload.silverlight.xap',
                    multiple_queues: true,
                    multipart_params: {
                        'folder_id': R.FileManager.folderSelected
                    },
                });

                // get uploader instance
                R.FileManager.plUploadFile = $("#UploadFile").pluploadQueue();
                // which is the URL to the image that was sent by OnUploadCompleted
                R.FileManager.plUploadFile.bind("FileUploaded", function (upload, file, response) {
                    // remove the file from the list
                    upload.removeFile(file);
                    // Response.response returns server output from onUploadCompleted
                    // our code returns the url to the image so we can display it
                    var imageUrl = response.response;

                });

                // Error handler displays client side errors and transfer errors
                // when you click on the error icons
                R.FileManager.plUploadFile.bind("Error", function (upload, error) {
                    // showStatus(error.message, 3000, true);
                });

                // only allow 5 files to be uploaded at once
                R.FileManager.plUploadFile.bind("FilesAdded", function (up, filesToBeAdded) {
                    if (up.files.length > 5) {
                        up.files.splice(4, up.files.length - 5);
                        showStatus("Only 5 files max are allowed per upload. Extra files removed.", 3000, true);
                        return false;
                    }
                    return true;
                });



            },
            open: function (event, ui) {

                R.FileManager.plUploadFile.splice();
                R.FileManager.plUploadFile.refresh();;
                $("#btnStopUpload").click(function () {
                    R.FileManager.plUploadFile.stop();
                });
                $("#btnStartUpload").click(function () {
                    R.FileManager.plUploadFile.start();
                });

            }
        });

    },
    Search: function () {
        var data = {
            'keyword': '',
            'parentid': $('#fm-wrapper').attr('node-select'),
            'action': ''
        }
        R.Post({
            params: data,
            module: "fmanager",
            ashx: 'modulerequest.ashx',
            action: "search",
            success: function (res) {

                if (res.Success) {
                    var data = JSON.parse(res.Data);
                    var row = '';
                    $.each(data, function (i, v) {
                        var dateString = v.UploadedDate.substr(6);
                        var currentTime = new Date(parseInt(dateString));
                        var month = currentTime.getMonth() + 1;
                        var day = currentTime.getDate();
                        var year = currentTime.getFullYear();
                        var h = currentTime.getHours();
                        var m = currentTime.getMinutes();
                        var date = day + "/" + month + "/" + year + " " + h + ":" + m;
                        var ext = v.FileExt.replace('.', '');
                        row += '<tr data="' + v.Name + '"  class="item-file"><td  class="name"><span class="fname"><i class="list-icon ' + ext + '" ></i>' + v.Name + '</span></td><td class="type">' + v.FileExt + '</td><td class="size">' + v.FileSize + '' +
                            ' Kb</td><td class="dimensions">' + v.Dimensions + '</td><td class="date">' + date + '</td></tr>';
                    });
                    var htmTable = '<table class="">';
                    htmTable += '<thead>';
                    htmTable += '        <tr  class="rowHeader" >';
                    htmTable += '           <th><span >Name</span></th>';
                    htmTable += '            <th ><span>Type</span></th>';
                    htmTable += '            <th ><span >Size</span></th>';
                    htmTable += '            <th ><span>Dimensions</span></th>';
                    htmTable += '            <th class="sorted sorted-desc"><span >Date</span></th>';
                    htmTable += '        </tr>';
                    htmTable += '</thead>';
                    htmTable += '      <tbody>' + row + '';
                    htmTable += '      </tbody>';
                    htmTable += '</table>';
                    $('#Fcontent ._list').html(htmTable);
                    $("#_list").slimScroll();

                    $.contextMenu({
                        selector: '.item-file',
                        callback: function (key, options) {
                            var imgPath = '/' + StorageUrl + '/' + $(this).closest('tr').attr('data');
                            var htm = '<img src="' + imgPath + '" width="600"/>';
                            switch (key) {
                                case 'view':
                                    $('#fm-container').find('#file-view').remove();
                                    $('#fm-container').append('<div id="file-view"></div>');
                                    $('#file-view').dialog({
                                        autoOpen: true,
                                        height: "auto",
                                        width: 600,
                                        show: "fadein",
                                        hide: "fadeout",
                                        modal: true,
                                        buttons: {

                                        },
                                        close: function () {

                                            $('#file-view').dialog("destroy").html('');
                                        },

                                        open: function (event, ui) {

                                            $('#file-view').html(htm);

                                        }
                                    });


                                    break;
                                case 'insert':
                                    break;
                                case 'delete':
                                    break;
                                case 'edit':
                                    break;
                                default:
                            }

                        },
                        items: {
                            "view": { name: "View", icon: "view" },
                            "insert": { name: "Insert", icon: "insert" },
                            //"cut": { name: "Cut", icon: "cut" },
                            //copy: { name: "Copy", icon: "copy" },
                            "edit": { name: "Edit", icon: "edit" },
                            "delete": { name: "Delete", icon: "delete" }
                            //"sep1": "---------",
                            //"quit": {
                            //    name: "Quit", icon: function () {
                            //        return 'context-menu-icon context-menu-icon-quit';
                            //    }
                            //}
                        }
                    });
                    $('.item-file').on('click', function (e) {

                        // console.log($(this).attr('data'))
                    }
                    );
                    R.FileManager.RegisterEvents();
                } else {
                    alert(res.Message);
                }
            }
        });
    }
}


//function setModalMaxHeight(element) {
//    this.$element = $(element);
//    this.$content = this.$element.find('.modal-content');
//    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
//    var dialogMargin = $(window).width() < 768 ? 20 : 60;
//    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
//    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
//    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
//    var maxHeight = contentHeight - (headerHeight + footerHeight);

//    this.$content.css({
//        'overflow': 'hidden'
//    });

//    this.$element
//      .find('.modal-body').css({
//          'max-height': maxHeight,
//          'overflow-y': 'auto'
//      });
//}

//$('.modal').on('show.bs.modal', function () {
//    $(this).show();
//    setModalMaxHeight(this);
//});

//$(window).resize(function () {
//    if ($('.modal.in').length != 0) {
//        setModalMaxHeight($('.modal.in'));
//    }
//});

function customMenu(node) {
    // The default set of all items
    var items = {
        renameItem: { // The "rename" menu item
            label: "Rename",
            action: function () { }
        },
        deleteItem: { // The "delete" menu item
            label: "Delete",
            action: function () { }
        }
    };

    if ($(node).hasClass("folder")) {
        // Delete the "delete" menu item
        delete items.deleteItem;
    }

    return items;
}
$(function () {
    //$('#demoOne').enhsplitter({ minSize: 50, vertical: false });
    //$('#demoOneB').enhsplitter();
    R.FileManager.Init();

})
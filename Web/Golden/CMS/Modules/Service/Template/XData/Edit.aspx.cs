﻿using System;
using System.Linq;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Zone;
using Mi.Action.Core;

namespace Golden.CMS.Modules.Service.Template.XData
{
    public partial class Edit : PageBase
    {
        public ZoneEntity _obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = GetQueryString.GetPost("id", 0);
            if (id > 0)
            {
                _obj = ZoneBo.GetZoneById(id);

            }
            else
            {
                _obj = new ZoneEntity();
            }
            var zones = ZoneBo.GetAllZoneWithTreeView(false, (int)ZoneType.Service).ToList().Where(it => it.Invisibled == false);
            ZoneRpt.DataSource = zones;
            ZoneRpt.DataBind();
        }
    }
}
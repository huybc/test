﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Golden.CMS.Modules.Service.Template.Edit" %>

<div class="panel-flat" id="zone-edit" style="display: inline-block; width: 780px">
    <div class="panel-title" style="height: 46px; padding: 10px 0 4px 0">
        <div class="col-md-12 text-right">
            <button type="button" id="zoneSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
        </div>
    </div>
    <div class="panel-form-zone panel-body" style="padding-top: 0; margin-bottom: 40px">
        <div class="form-group">
            <label>Tên:</label>
            <input type="text" class="form-control" id="zoneNameTxt" placeholder="Tên danh mục" value="<%= obj.Name %>">
        </div>

        <div class="form-group" id="zone-panel">
            <p style="margin-bottom: 0">
                <label>Chuyên mục cha:</label>

            </p>
            <select data-placeholder="Select your state" class="select" id="_zoneddl" style="width: 100%" tabindex="4" data="<%=obj.ParentId %>">
                <option value="">-- Chuyên mục --</option>
                <asp:repeater runat="server" id="ZoneRpt">
                                                <ItemTemplate>
                                                    <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                </ItemTemplate>
                                            </asp:repeater>
            </select>
        </div>

        <div class="form-group">
            <label>Thứ tự:</label>
            <input type="text" class="form-control" id="sortZoneTxt" placeholder="Thứ tự" value="<%= obj.SortOrder %>">
        </div>
        <div class="form-group">
            <label>Mô tả:</label>
            <textarea rows="2" cols="5" class="form-control" id="sapoZoneTxt" placeholder="Enter your message here"><%= obj.Content %></textarea>
        </div>
        <div class="form-group">
            <label>Meta title<span class="text-danger">*</span></label>
            <input type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=obj.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
        </div>
        <div class="form-group">
            <label>Từ khóa:</label>
            <input class="form-control" id="metaKeywordZoneTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="<%=obj.MetaKeyword %>" />
        </div>
        <div class="form-group">
            <label>Mô tả trang:</label>
            <textarea rows="2" cols="5" class="form-control" id="metaDescriptionZoneTxt" placeholder="MetaDescription.. mô tả trang."><%=obj.MetaDescription %></textarea>
        </div>


    </div>
</div>

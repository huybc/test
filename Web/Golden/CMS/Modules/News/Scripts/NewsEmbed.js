﻿R.Special = {
    Init: function () {
        this.ItemSelected = [];
        this.oId = 0;
        this.PageIndex = 1;
        this.PageSize = 30;

        R.Special.RegisterEvents();
        if ($('#plist').length > 0) {
            var pageInfo = $('#plist ul').attr('page-info').split('#');
            var page = pageInfo[0];
            var extant = pageInfo[1];
            var totals = pageInfo[2];
            if (parseInt(totals) < 0) {
                $('#pdata-pager').hide();
                return;
            } else {
                $('#pdata-pager').show();
            }
            var rowFrom = '';
            if (R.Special.PageIndex === 1) {
                rowFrom = 'Từ 1 đến ' + extant;
            } else {
                rowFrom = 'Từ ' + (parseInt(R.Special.PageIndex) * parseInt(R.Special.PageSize) - parseInt(R.Special.PageSize)) + ' đến ' + extant;
            }

            $('#ptotal-rows').text(rowFrom + '/' + totals);
            $('#pdata-pager .ipagination').jqPagination({
                current_page: R.Special.PageIndex,
                max_page: page,
                paged: function (page) {
                    R.Special.PageIndex = page;
                    R.Special.Search();
                }
            });
            //R.ScrollAutoSize('#plist .row,#rlist .row', function () {
            //    return $(window).height() - 200;
            //}, function () {

            //}, {});
        }

    },
    RegisterEvents: function () {
        $('#plist .row ul li,#rlist .row ul li').off('click').on('click', function () {
            var $el = $(this);
            if ($el.hasClass('active')) {
                $el.removeClass('active');
            } else {
                $el.addClass('active');
            }
        });
        $('#paction .btn-add').off('click').on('click', function () {
            var totalSuccess = 0;
            var totalError = 0;

            $('#plist .row ul li.active').each(function (i, v) {
                var id = $(v).attr('p-id');

                $('#rlist .row ul li').each(function (j, u) {
                    var rid = $(u).attr('p-id');
                    if (parseInt(id) === parseInt(rid)) {
                        totalError++;
                    }
                });

                if (totalError === 0) {
                    $('#rlist .row ul').append(v);
                    totalSuccess++;
                }
                $(v).removeClass('active');

            });

            if (totalError > 0) {
                $.notify('Đã tồn tại ' + totalError + '', {
                    autoHideDelay: 2000, className: "error",
                    globalPosition: 'right top'
                });
            }
            setTimeout(function () {
                $('#total-rproduct ul li span').text($("#rlist .row ul li").length);
                //set index
                $('#rlist .row ul li').each(function (i, v) {
                    var idex = parseInt(i) + 1;
                    $(v).attr('index', idex);
                });
                $("#rlist .row ul").sortable({
                    change: function (event, ui) {

                    },
                    update: function (event, ui) {
                        var index = ui.placeholder.index();
                        $('#rlist .row ul li:eq(' + index + ')').removeClass('active');

                        //set index
                        $('#rlist .row ul li').each(function (i, v) {
                            var idex = parseInt(i) + 1;
                            $(v).attr('index', idex);
                        });


                    }
                });

                R.Special.RegisterEvents();
            }, 200);
        });
        $('#paction .btn-unadd').off('click').on('click', function () {
            $('#rlist .row ul li.active').each(function (i, v) {
                $(v).remove();
            });
            $('#total-rproduct ul li span').text($("#rlist .row ul li").length);
            R.Special.RegisterEvents();
        });
        $('#psaveBtn').off('click').on('click', function () {
            var jsondata = [];
            $('#rlist .row ul li').each(function (i, v) {
                jsondata.push({ oid: $(v).attr('p-id'), index: $(v).attr('index') });

            });
            if (jsondata.length === 0) {
                $.notify('Chưa chọn sản phẩm nào', {
                    autoHideDelay: 2000, className: "error",
                    globalPosition: 'right top'
                });

                return;
            }
            if (parseInt($('#zonefixd').val()) === 0) {
                $.notify('Chọn vùng hiển thị', {
                    autoHideDelay: 2000, className: "error",
                    globalPosition: 'right top'
                });


                return;
            }
            var data = {
                pjson: JSON.stringify(jsondata),
                zone: $('#zonefixd').val()
            }
            R.Special.Save(data);
        });

        $('#psearchbtn').off('click').on('click', function () {
            R.Special.Search();
        });
        $('#ReferSaveBtn').off('click').on('click', function () {
            var jsondata = [];
            $('#rlist .row ul li').each(function (i, v) {
                jsondata.push({ oid: $(v).attr('p-id'), index: $(v).attr('index') });

            });
            if (jsondata.length === 0) {
                $.notify('Chưa chọn sản phẩm nào', {
                    autoHideDelay: 2000, className: "error",
                    globalPosition: 'right top'
                });

                return;
            }
            if (parseInt($('#zonefixd').val()) === 0) {
                $.notify('Chọn vùng hiển thị', {
                    autoHideDelay: 2000, className: "error",
                    globalPosition: 'right top'
                });


                return;
            }
            var data = {
                pjson: JSON.stringify(jsondata),
                zone: R.News.oId,
                type: $(this).attr('data')

            }
            R.Special.ReferSave(data);
        });

        $('#news-tab-more-info ul li').off('click').on('click', function () {
            $('#news-tab-more-info ul li').removeClass('active');
            $(this).addClass('active');
            R.Special.GetMainType($(this).attr('data'));
        });
        $('#special-zoneddl').off('change').on('change', function (e) {
            R.Special.Search();
        });
        $('#zonefixd').off('change').on('change', function (e) {
            R.Special.RSearch();
        });


    },
    HotMain: function () {
        $('#main-pnl').RModuleBlock();
        R.Post({
            params: {

            },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "hot_main",
            success: function (res) {
                if (res.Success) {
                    $('#sub-pnl').html(res.Content);
                    $('#main-pnl').hide();
                    $('#sub-pnl').show({
                        //  easing: 'bounce',
                        duration: 200,
                        complete: function () {

                        },
                        done: function () {
                            $('#special-zoneddl,#zonefixd').fselect({
                                dropDownWidth: 0,
                                autoResize: false
                            });
                            R.Special.Init();
                        }
                    });

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                $('#main-pnl').RModuleUnBlock();
            }
        });
    },
    EmbedMain: function (type) {
        $('#references').RModuleBlock();
        R.Post({
            params: {
            },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "refer_main",
            success: function (res) {
                if (res.Success) {
                    $('#references').html(res.Content);
                    $('#special-zoneddl').fselect({
                        dropDownWidth: 0,
                        autoResize: false
                    });
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Special.Init();
                $('#references').RModuleUnBlock();
            }
        });
    },
    GetMainType: function (type) {
        $('#special.refer').RModuleBlock();
        R.Post({
            params: {
                type: type,
                oid: R.News.oId
            },
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "refer_main_type",
            success: function (res) {
                if (res.Success) {
                    $('#special.refer').html(res.Content);
                    $('#special-zoneddl').fselect({
                        dropDownWidth: 0,
                        autoResize: false
                    });
                    $('#total-rproduct ul li span').text($("#rlist .row ul li").length);
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
                R.Special.Init();
                $('#references').RModuleUnBlock();
            }
        });
    },
    Save: function (data) {
        R.Post({
            params: data,
            module: "product",
            ashx: 'modulerequest.ashx',
            action: "psave",
            success: function (res) {
                if (res.Success) {
                    if (res.Success) {
                        $.notify("Lưu thành công !", {
                            autoHideDelay: 3000, className: "success",
                            globalPosition: 'right top'
                        });

                        setTimeout(function () {
                            R.Special.Search();
                        }, 200);

                    } else {
                        $.notify(res.Message, {
                            autoHideDelay: 3000, className: "error",
                            globalPosition: 'right top'
                        });

                    }

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }
            }
        });
    },
    ReferSave: function (data) {
        R.Post({
            params: data,
            module: "news",
            ashx: 'modulerequest.ashx',
            action: "refer_save",
            success: function (res) {
                if (res.Success) {
                    $.notify("Lưu thành công !", {
                        autoHideDelay: 3000, className: "success",
                        globalPosition: 'right top'
                    });

                    //setTimeout(function () {
                    //    R.Special.Search();
                    //}, 200);

                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });

                }
            }
        });
    },

    Search: function () {
        var el = '#plist';
        $(el).RLoading();
        var data = {
            keyword: $('#pkeysearchtxt').val(),
            zone: $('#special-zoneddl').val(),
            pageindex: R.Special.PageIndex,
            pagesie: R.Special.PageSize
        }
        R.Post({
            params: data,
            module: "product",
            ashx: 'modulerequest.ashx',
            action: "psearch",
            success: function (res) {
                if (res.Success) {
                    $('#plist .row').html(res.Content);
                    var pageInfo = $('#plist ul').attr('page-info').split('#');
                    var page = pageInfo[0];
                    var extant = pageInfo[1];
                    var totals = pageInfo[2];
                    if (parseInt(totals) < 0) {
                        $('#pdata-pager').hide();
                        return;
                    } else {
                        $('#pdata-pager').show();
                    }
                    var rowFrom = '';
                    if (R.Special.PageIndex === 1) {
                        rowFrom = 'Từ 1 đến ' + extant;
                    } else {
                        rowFrom = 'Từ ' + (parseInt(R.Special.PageIndex) * parseInt(R.Special.PageSize) - parseInt(R.Special.PageSize)) + ' đến ' + extant;
                    }

                    $('#rowInTotals').text(rowFrom + '/' + totals);
                    $('#pdata-pager .ipagination').jqPagination({
                        current_page: R.Special.PageIndex,
                        max_page: page,
                        paged: function (page) {
                            R.Special.PageIndex = page;
                            R.Special.Search();
                        }
                    });
                    R.Special.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    },

    RSearch: function () {
        var el = '#rlist';
        $(el).RLoading();
        var data = {
            zone: $('#zonefixd').val()
        }
        R.Post({
            params: data,
            module: "product",
            ashx: 'modulerequest.ashx',
            action: "rsearch",
            success: function (res) {
                if (res.Success) {
                    $('#rlist .row').html(res.Content);
                    $("#rlist .row ul").sortable();
                    $('#total-rproduct ul li span').text($("#rlist .row ul li").length);
                    R.Special.RegisterEvents();
                } else {
                    $.notify(res.Message, {
                        autoHideDelay: 3000, className: "error",
                        globalPosition: 'right top'
                    });
                }

                $(el).RLoadingComplete();
            }
        });
    }

},

$(function () {
    $('[data-popup="tooltip"]').tooltip();


});


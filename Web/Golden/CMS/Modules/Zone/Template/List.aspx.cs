﻿using System;
using System.Collections.Generic;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Action.Core;

namespace Golden.CMS.Modules.Zone.Template
{
    public partial class List : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public List<ZoneResult> ZoneResult()
        {
            var rs = new List<ZoneResult>();
            var key = GetQueryString.GetPost("keyword", string.Empty);
            var status = GetQueryString.GetPost("status", 0);
            var type = GetQueryString.GetPost("type", 0);
            var objs = ZoneBo.ZoneSearch(key, status, (byte)type);
            foreach (var obj in objs)
            {
                rs.Add(new ZoneResult
                {
                    id = obj.Id + "",
                    parent = obj.ParentId > 0 ? obj.ParentId + "" : "#",
                    text = obj.Name,
                });
            }
            return rs;
        }
    }

    public class ZoneResult
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string path { get; set; }
    }
}
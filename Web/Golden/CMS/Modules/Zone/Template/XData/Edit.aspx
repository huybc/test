﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Golden.CMS.Modules.Zone.Template.XData.Edit" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<div class="tabbable" id="news-id">
    <%--  <div class="text-right right navbar-fixed-top header" id="_header" style="clear: both; margin: 3px 0; padding-right: 16px">
        <button type="button" class="btn btn-primary btn-xs" id="news-request"><i class="icon-paperplane position-left"></i>Gửi bài</button>&nbsp;&nbsp;
        <button type="button" data-dismiss="modal" class="btn btn-default btn-xs  heading-btn "><i class="icon-x position-left"></i>Đóng</button>
    </div>--%>
    <div class=" _body" id="_body">
        <div class="col-lg-9" style="padding-left: 0;">
            <div id="zone-content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                Đăng bài viết
                            </legend>
                        </fieldset>
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="col-lg-2">Tiêu đề<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="titletxt" class="form-control" value="<%=_obj.Name %>" tabindex="1" required="required" placeholder="Tối ưu ở [60-100] ký tự.">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Mô tả<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea id="Sapo" class="form-control" rows="3" placeholder="Nội dung ngắn gọn" tabindex="2"><%=_obj.Description %></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Thông số<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Hours Of Work</label>
                                        <input type="text" name="basic" id="hoursOfWorkTxt" class="form-control" value="<%=_obj.HoursOfWork %>" tabindex="1" required="required" placeholder="Hours of work">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Construction date</label>
                                        <input type="text" name="basic" id="constructionDateTxt" class="form-control" value="<%=String.Format("{0:dd/MM/yyyy}", _obj.ConstructionDate) %>" data="<%=String.Format("{0:yyyy/MM/dd}", _obj.ConstructionDate) %>" tabindex="1" required="required" placeholder="Construction date">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Surface area</label>
                                        <input type="text" name="basic" id="surfaceAreaTxt" class="form-control" value="<%=_obj.SurfaceArea %>" tabindex="1" required="required" placeholder="Surface area">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label col-lg-12">Budget</label>
                                        <input type="text" name="basic" id="budgetTxt" class="form-control" value="<%=_obj.Budget %>" tabindex="1" required="required" placeholder="Budget">
                                    </div>

                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">Nội dung</legend>

                            <div class="form-group">

                                <div id="items">
                                    <%
                                        var items = ProjectDetailBo.GetByArticleId(_obj.Id).ToArray();
                                        if (items.Any())
                                        {
                                            for (int i = 0; i < items.Count(); i++)
                                            {
                                    %>
                                    <div class="item">
                                        <div class="control-label col-lg-2 text-center _field">
                                            <div class="avatar" style="width: auto; height: auto">
                                                (1500px*630px)
                                                <input accept="image/*" id="img_attack_step-content<%=items[i].Id %>" type="file" name="files[]" style="display: none" />
                                                <label for="img_attack_step-content<%=items[i].Id %>">
                                                    <img data-img="<%=items[i].Image %>" src="/<%=!string.IsNullOrEmpty(items[i].Image)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+items[i].Image:"CMS/Themes/Images/nothumb.JPG" %>" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 _field">
                                            <input type="text" name="basic"  class="form-control title" value="<%=items[i].Title %>" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">
                                            <div class="content-tiem" style="padding-top: 10px">
                                                <textarea class="editor" id="content_<%=items[i].Id %>"><%= items[i].Content%></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <% }
                                        }
                                        else
                                        { %>
                                    <div class="item">
                                        <div class="control-label col-lg-2 text-center _field">
                                            <div class="avatar" style="width: auto; height: auto">
                                                (1500px*630px)
                                                <input accept="image/*" id="img_step-content1" type="file" name="files[]" style="display: none" />
                                                <label for="img_step-content1">
                                                    <img src="/CMS/Themes/Images/nothumb.JPG" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 _field">
                                            <input type="text" name="basic" class="form-control title" value="" tabindex="1" required="required" placeholder="Tối đa 255 ký tự">
                                            <div class="content-tiem" style="padding-top: 10px">
                                                <textarea class="editor" id="detailCkeditor"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                </div>
                                <label class="control-label col-lg-2">&nbsp;<span class="text-danger"></span></label>
                                <div class="col-lg-10">
                                    <div style="padding-top: 20px">
                                        <button type="button" class="btn btn-blue ibtn-xs" id="addItemBtn"><i class="icon-add position-left"></i>Thêm</button>
                                    </div>

                                </div>


                            </div>
                            <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Tags<span class="text-danger"></span></label>
                            <div class="col-lg-10 tags">
                                <textarea id="Tagstext" data-type="<%=(int) EnumTagType.ForNews %>" preset="<%=GetTags((int)EnumTagType.ForNews) %>" placeholder="Tag: sửa chữa, thay thế, phụ tùng..." class="tag-editor-hidden-src"></textarea>
                            </div>
                        </div>--%>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">SEO <i class="icon-stats-growth"></i></legend>
                            <%--    <div class="form-group">
                            <label class="control-label col-lg-2">Url<span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" name="basic" id="url" class="form-control" required="required" value="<%=_obj.NewsInfo.Url %>" placeholder="Đường dẫn tối ưu cho bài viết">
                            </div>
                        </div>--%>
                            <div class="form-group">
                                <label class="col-lg-2">Url<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="urltxt" class="form-control" required="required" value="<%=_obj.ShortUrl %>" placeholder="Tùy chỉnh đường dẫn tin bài">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Meta title<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=_obj.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa với công cụ tìm kiếm">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2">Meta Keyword<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="maximum_characters" id="metakeywordtxt" class="form-control" value="<%=_obj.MetaKeyword %>" placeholder="Khai báo các từ khóa với bộ máy tìm kiếm.">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2">Meta Description<span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <textarea name="maximum_characters" class="form-control" id="metadescriptiontxt" placeholder="Mô tả bài viết hiển thị trên công cụ tìm kiếm. Độ dài tối ưu độ dài khoảng [140-160] ký tự"><%=_obj.MetaDescription %></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" style="padding-right: 0">
            <div id="sidebar">
                <div id="sidebar-content">
                    <div class="panel panel-flat">
                        <div class="panel-body">

                            <%-- <div class="col-lg-12 col-md-12 col-sm-12">--%>
                            <div class="form-group">
                                <label>Chuyên mục</label>
                                <select data-placeholder="Select your state" class="select" id="_zoneddl" style="width: 100%" tabindex="4" data="<%=_obj.ParentId %>">
                                    <option value="">-- Dự án --</option>
                                    <asp:repeater runat="server" id="ZoneRpt">
                                            <ItemTemplate>
                                                <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                            </ItemTemplate>
                                        </asp:repeater>
                                    <option value='4532' data-parent="0">Has#Tag</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ngày đăng</label>
                                <input type="text" class="form-control" id="datetxt" placeholder="" tabindex="10" value="<%=String.Format("{0:dd/MM/yyyy HH:mm:ss tt}", _obj.DistributionDate) %>" data="<%=String.Format("{0:yyyy/MM/dd HH:mm:ss tt}", _obj.DistributionDate) %>">
                            </div>
                            <%-- <div class="form-group text-left">
                                <label class="checkbox-inline checkbox-left">
                                    <input <%=_obj.NewsInfo.IsHot?"checked":"" %> type="checkbox" tabindex="8" id="hotcb">
                                    Nổi bật
								
                                </label>
                                <label class="checkbox-inline checkbox-left">
                                    <input <%=_obj.NewsInfo.IsVideo?"checked":"" %> type="checkbox" tabindex="8" id="videocb">
                                    Tin video
								
                                </label>
                                <label class="checkbox-inline checkbox-left">
                                    <input type="checkbox" tabindex="9" id="commentcb" <%=_obj.AllowComment?"checked":"" %>>Đóng bình luận
								
                                </label>
                            </div>--%>
                            <%-- <div class="form-group">
                                            <label>Tài liệu đính kèm</label>
                                            <a href="#">[Đính kèm]</a>

                                        </div>--%>
                            <div class="form-group" id="attack-thumb">
                                <div>
                                    <label>Ảnh đại diện</label>
                                </div>
                                <%--<a id="attack-thumb" href="#">
                                    <img data="<%=_obj.Avatar %>" src="<%= UIHelper.Thumb_W(70,_obj.Avatar) %>" width="70" /></a>--%>
                                <div class="avatar">
                                    <input accept="image/*" id="fileSingleupload" type="file" name="files[]" style="display: none" />
                                    <label for="fileSingleupload">
                                        <%if (_obj.Id > 0)
                                            {%>
                                        <img data-img="<%=_obj.Avatar %>" src="/<%=!string.IsNullOrEmpty(_obj.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+_obj.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                                        <%}
                                            else
                                            {%>
                                        <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                                        <% } %>
                                    </label>
                                </div>
                            </div>

                            <div class="text-right" id="news-save">
                                <button type="button" status="<%=(int)ZoneStatus.Temp %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-floppy-disk position-left"></i>Lưu</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="button" status="<%=(int)ZoneStatus.Publish %>" class="btn btn-default ibtn-xs btn-primary"><i class="icon-paperplane position-left"></i>Đăng bài</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-default ibtn-xs" id="IMSFullOverlayClose"><i class="icon-x position-left"></i>Đóng</button>


                            </div>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


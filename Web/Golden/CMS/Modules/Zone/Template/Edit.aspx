﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Golden.CMS.Modules.Zone.Template.Edit" %>

<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Common.ChannelConfig" %>

<div class="panel-flat" id="zone-edit" style="display: inline-block; width: 780px">
    <div class="panel-title" style="height: 46px; padding: 10px 0 4px 0; margin-right: 20px">
        <div class="col-md-12 text-right">
            <button type="button" id="zoneSaveBtn" class="btn ibtn-xs btn-primary "><i class="icon-floppy-disk position-left"></i>Lưu</button>
        </div>
    </div>
    <div class="panel-form-zone panel-body" style="padding-top: 0; margin-bottom: 40px; margin-right: 20px">
        <div class="form-group">
            <label>Tên:</label>
            <input type="text" class="form-control" id="zoneNameTxt" placeholder="Tên danh mục" value="<%= obj.Name %>">
        </div>
        <div class="form-group" id="zone-panel">
            <p style="margin-bottom: 0">
                <label>Danh mục:</label>

            </p>
            <select data-placeholder="Select your state" class="select" id="_zoneddl" style="width: 100%" tabindex="4" data="<%=obj.ParentId %>">
                <option value="">- Chọn -</option>
                <asp:repeater runat="server" id="ZoneRpt">
                                                <ItemTemplate>
                                                    <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                </ItemTemplate>
                                            </asp:repeater>
            </select>
        </div>
        <div class="form-group text-left" style="display: none" id="isShowHomaPageEl">
            <label class="checkbox-inline checkbox-left">
                <input <%=obj.IsShowHomePage?"checked":"" %> type="checkbox" tabindex="8" id="isShowHomaPage">
                Hiển thị trang chủ
								
            </label>

        </div>

        <div class="form-group" id="attack-files">
            <div class="row">
                <div class="col-lg-3" id="attack-thumb">
                    <div>
                        <label>Ảnh đại diện</label>
                    </div>
                    <div class="avatar">
                        <input accept="image/*" id="fileSingleUploadThumb" type="file" name="files[]" style="display: none" />
                        <label for="fileSingleUploadThumb">
                            <%if (obj.Id > 0)
                                {%>
                            <img data-img="<%=obj.Avatar %>" src="/<%=!string.IsNullOrEmpty(obj.Avatar)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+obj.Avatar:"CMS/Themes/Images/no-thumbnai.png" %>" />
                            <%}
                                else
                                {%>
                            <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                            <% } %>
                        </label>
                    </div>
                </div>
                <div class="col-lg-6" id="attack-icon">
                    <div>
                        <label>Icon</label>
                    </div>
                    <div class="avatar">
                        <input accept="image/*" id="fileSingleUploadIcon" type="file" name="files[]" style="display: none" />
                        <label for="fileSingleUploadIcon">
                            <%if (obj.Id > 0)
                                {%>
                            <img style="width: 100%" data-img="<%=obj.Banner %>" src="/<%=!string.IsNullOrEmpty(obj.Banner)? CmsChannelConfiguration.GetAppSetting("StorageUrl")+"/"+obj.Banner:"CMS/Themes/Images/no-thumbnai.png" %>" />
                            <%}
                                else
                                {%>
                            <img src="/CMS/Themes/Images/no-thumbnai.png" width="60" />
                            <% } %>
                        </label>
                    </div>
                </div>
                <div class="col-lg-6" id="class-icon" style="display: none">
                    <div>
                        <label>Icon</label>
                    </div>
                    <div>
                        <p><a target="_blank" href="https://fontawesome.com/">https://fontawesome.com/</a></p>
                        <input type="text" class="form-control" id="clasIconTxt" placeholder="address-book" value="<%= obj.Banner %>">
                    </div>
                </div>
            </div>

        </div>

        <div class="form-group">
            <label>Thứ tự:</label>
            <input type="text" class="form-control" id="sortZoneTxt" placeholder="Thứ tự" value="<%= obj.SortOrder %>">
        </div>
        <div class="form-group">
            <label>Mô tả:</label>
            <textarea rows="2" cols="5" class="form-control" id="sapoZoneTxt" placeholder="Enter your message here"><%= obj.Content %></textarea>
        </div>
        <div class="form-group">
            <label>Meta title<span class="text-danger">*</span></label>
            <input type="text" name="basic" id="metaTitletxt" class="form-control" value="<%=obj.MetaTitle %>" tabindex="1" required="required" placeholder="Khai báo từ khóa vs công cụ tìm kiếm">
        </div>
        <div class="form-group">
            <label>Từ khóa:</label>
            <input class="form-control" id="metaKeywordZoneTxt" placeholder="Metakeyword, từ khóa đểo seo danh mục" value="<%=obj.MetaKeyword %>" />
        </div>
        <div class="form-group">
            <label>Mô tả trang:</label>
            <textarea rows="2" cols="5" class="form-control" id="metaDescriptionZoneTxt" placeholder="MetaDescription.. mô tả trang."><%=obj.MetaDescription %></textarea>
        </div>


    </div>
</div>

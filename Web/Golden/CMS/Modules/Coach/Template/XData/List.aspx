﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Golden.CMS.Modules.Coach.Template.XData.List" %>

<%@ Import Namespace="Golden.Core.Helper" %>
<table class="table datatable-show-all dataTable no-footer" total-rows="<%=TotalRows%>" page-info="<%=pageInfo%>">
    <% if (TotalRows > 0)
        { %>
    <tbody>
        <asp:repeater runat="server" id="DataRpt">
           <ItemTemplate>
                <tr role="row">
            <td class="w100">
                <div>
                    <label>
                        <input type="checkbox" id="<%# Eval("Id") %>" />
                    </label>
                    <label class="_hot">
                       <i class="icon-magazine position-static"></i>
                    </label>
                </div>
            </td>
            <td class="w80 ">
                <a class="avatar">
                    <img class="thumb" src="<%#  UIHelper.Thumb_W(60,Eval("Avatar").ToString()) %>" width="60" />
                    <%# (bool)Eval("IsVideo")?"<i class=\"icon-video\"></i>":"" %>
                </a>
            </td>
            <td>
                <p>
                   <%# Eval("Title") %> - <b class="wysiwyg-color-green _zone" data-id="<%# Eval("ZoneId") %>"></b>
                </p>
                <p>
                    <label>Tạo bởi:</label><span class="bold"><%# Eval("CreatedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("CreatedDate")) %></time>,
                                                        <label> Xuất bản bởi:</label><span class="bold"><%# Eval("PublishedBy") %></span> - <time><%# UIHelper.GetLongDate(Eval("DistributionDate")) %></time>
                </p>
            </td>
            <td>
                <p>
                    <label>xem: <%# Eval("ViewCount") %></label>
                </p>
            </td>
            
            <td><i class="_icon icon-comment allow-comment" data="<%# Eval("IsAllowComment") %>" data-popup="tooltip" data-placement="top" data-original-title="đóng/mở bình luận"></i></td>
            <td><a target="_blank" href="https://www.google.com/webmasters/tools/submit-url?urlnt=<%# HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)+ Eval("Url") %>"><i class="_icon icon-google" data-popup="tooltip" data-placement="top" data-original-title="Index google"></i></a></td>
          
        </tr>
           </ItemTemplate>
       </asp:repeater>


    </tbody>
    <% }
        else
        { %>
    <div class="">
        <div id="NoDataSolution">Không tìm thấy</div>
    </div>
    <% } %>
</table>
<div style="float: left; padding-top: 20px"><%= UIHelper.FormatCurrency("VND", TotalRows,false) %> bản ghi.</div>

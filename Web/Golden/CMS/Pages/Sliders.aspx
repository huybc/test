﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Sliders.aspx.cs" Inherits="Golden.CMS.Pages.Sliders" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <style>
        #adv-wrapper { }
        #adv-wrapper tr td { }
        #adv-wrapper tr td i { cursor: pointer; }
        #adv-wrapper tr td img { cursor: pointer; }
        #data-pager { padding-top: 3px; height: 38px; }
        .iweb.ipagination { display: inline-block; border: 1px solid #CDCDCD; }
        .ipagination.iweb { }
        .ipagination.iweb > li { display: inline; list-style-type: none; }
        .iweb.ipagination a { display: block; float: left; width: 20px; height: 20px; outline: none; border-right: 1px solid #CDCDCD; border-left: 1px solid #CDCDCD; color: #555555; vertical-align: middle; text-align: center; text-decoration: none; font-weight: bold; font-size: 16px; font-family: Times, 'Times New Roman', Georgia, Palatino; /* ATTN: need a better font stack */ background-color: #f3f3f3; }
        .iweb.ipagination a:hover, .ipagination a:focus, .pagination a:active { background-color: #cecece; }
        .ipagination a.disabled, .ipagination a.disabled:hover, .ipagination a.disabled:focus, .ipagination a.disabled:active { background-color: #f3f3f3; color: #A8A8A8; cursor: default; }
        .iweb.ipagination a:first-child { border: none; border-radius: 2px 0 0 2px; }
        .iweb.ipagination a:last-child { border: none; border-radius: 0 2px 2px 0; }
        .iweb.ipagination input { float: left; margin: 0; padding: 0; width: 120px; height: 20px; outline: none; border: none; vertical-align: middle; text-align: center; }
        /* gigantic class for demo purposes */
        .iweb.gigantic.pagination { }
        .iweb.ipagination a { height: 30px; width: 35px; line-height: 28px; }
        .iweb.ipagination input { height: 30px; }
        #data-pager > span { vertical-align: top; display: inline-block; line-height: 36px; padding: 0 16px; }
        #news-edit-content .header { background: #FFF; border-bottom: 1px solid #CDCDCD; padding: 2px 0; }
        #news-edit-content ._body { padding-top: 30px; }
        #news-preview [data-dismiss=modal] { color: #f5f5f5; }
        .slimScrollDiv { overflow: initial !important; }
        .page-content input { border: 1px solid #ddd }
        /*#adv-wrapper {
          
            height: 800px !important;
            overflow: auto !important;
          
        }
        .datatable-scroll-sm table { margin-bottom: 30px}*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <%var type = Page.RouteData.Values["type"] != null ? Page.RouteData.Values["type"].ToInt32Return0() : 1; %>
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="config-sidebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Slides</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li data-type="101" class="<%=type==101?"active":"" %>"><a href="/cpanel/sliders-101.htm"><i class="icon-file-presentation"></i>Slide trang chủ</a></li>
                            <li data-type="102" class="<%=type==102?"active":"" %>"><a href="/cpanel/sliders-102.htm"><i class="icon-file-minus2"></i>4 Stamp trang chủ</a></li>
                            <li data-type="103" class="<%=type==103?"active":"" %>"><a href="/cpanel/sliders-103.htm"><i class="icon-floppy-disk"></i>Hình ảnh trang chủ</a></li>
<%--                            <li data-type="104" class="<%=type==104?"active":"" %>"><a href="/cpanel/sliders-104.htm"><i class="icon-headset"></i>Hình ảnh trang chủ</a></li>--%>
                            <li data-type="105" class="<%=type==105?"active":"" %>"><a href="/cpanel/sliders-105.htm"><i class="icon-diff-ignored"></i>Slide liên kết</a></li>
<%--                            <li data-type="6" class="<%=type==6?"active":"" %>"><a href="/cpanel/sliders-6.htm"><i class="icon-diff-ignored"></i>Poster sidebar top</a></li>
                            <li data-type="7" class="<%=type==7?"active":"" %>"><a href="/cpanel/sliders-7.htm"><i class="icon-diff-ignored"></i>Poster sidebar bottom</a></li>--%>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">
                <!-- Content area -->
                <div class="page-header" id="adv-main">
                    <div class="panel-body " id="rlist">
                        <div class="row">
                            <div id="adv-wrapper">
                                <table class="table datatable-show-all dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Ảnh</th>
                                            <th>Link</th>
                                            <th>Thứ tự</th>
                                            <th>Hiển thi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <%

                                        var objs = ConfigBo.AdvGetByType(type).ToList();
                                        for (int i = 0; i < objs.Count(); i++)
                                        {
                                    %>
                                    <tr class="row-item" data="<%=objs[i].Id %>">
                                        <td>
                                            <input type="text" placeholder="Tiêu đề" class="name form-control" value="<%=objs[i].Name %>" />
                                            <textarea style="margin-top: 10px" class="description form-control" placeholder="Mô tả"><%=objs[i].Content %></textarea>
                                        </td>

                                        <td>
                                            <label for="fileSingleupload<%=objs[i].Id %>">
                                                <input accept="image/*" id="fileSingleupload<%=objs[i].Id %>" multiple type="file" name="files[]" style="display: none" />
                                                <img src="/uploads/<%=objs[i].Thumb %>?w=100" data="<%=objs[i].Thumb %>" /></label>
                                        </td>
                                        <td>
                                            <input type="text" style="width: 100%" class="url form-control" value="<%=objs[i].Url %>" /></td>
                                        <td style="width: 100px">
                                            <input type="text" class="sort form-control" value="<%=objs[i].SortOrder %>" /></td>
                                        <td style="width: 40px">
                                            <input type="checkbox" class="isaenable" <%=objs[i].IsEnable?"checked":"" %> />
                                        </td>
                                        <td>
                                            <i class="icon-x delete position-left"></i>
                                        </td>
                                    </tr>
                                    <%} %>
                                    <tr id="row-add">
                                        <td colspan="6">
                                            <i class="icon-plus2 position-left"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <div class="btn-save text-center">
                                                <button type="button" class="btn btn-default ibtn-xs btn-primary" id="save-temp"><i class="icon-floppy-disk position-left"></i>Lưu</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <div id="IMSOverlayWrapper">
        <div id="IMSOverlayWrapperIn">
            <div id="IMSOverlayContent">
            </div>
            <span id="IMSOverlayClose"></span>
        </div>
    </div>
    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/Slider/Scripts/main.js"></script>
    <%--end--%>
</asp:Content>

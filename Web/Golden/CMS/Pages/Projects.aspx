﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Projects.aspx.cs" Inherits="Golden.CMS.Pages.Projects" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/ftag/ftag.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/style.css?v=<%=UIHelper.Version %>" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css?v=<%=UIHelper.Version %>" rel="stylesheet" />
    <link href="/CMS/Themes/js/treegrid/jquery.treegrid.css?v=<%=UIHelper.Version %>" rel="stylesheet" />
    <style>
        #items {}
        #items .item .avatar{cursor: pointer}
        #items .item .avatar img{max-width: 100px}
        #items {}
        #items .item._add{padding-top: 30px}
        #items .item{ position: relative;display: inline-block;width: 100%}
        #items .item i.icon-bin{ position: absolute;right: -13px;top: 53px;cursor: pointer}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container" id="mini-game">

        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="news-sldebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <li class="navigation-header"><span>Dự án</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="sub-news-game active" status="<%=(int)NewsStatus.Published %>" data-type="<%=(int)ZoneType.Project %>"><a href="javascript:void(0)"><i class="icon-magazine position-left"></i><span>Bài đã duyệt</span></a></li>
                            <li class="sub-news-game" status="<%=(int)NewsStatus.Temporary %>" data-type="<%=(int)ZoneType.Project %>"><a href="javascript:void(0)"><i class="icon-magazine"></i>Bài lư tạm</a></li>
                            <li class="sub-news-game" status="<%=(int)NewsStatus.Unpublished %>" data-type="<%=(int)ZoneType.Project %>"><a href="javascript:void(0)"><i class="icon-file-download position-left"></i><span>Bài bị hạ</span></a></li>
                            <li class="sub-news-game" status="<%=(int)NewsStatus.MovedToTrash %>" data-type="<%=(int)ZoneType.Project %>"><a href="javascript:void(0)"><i class="icon-bin"></i>Bài bị xóa</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">

                <!-- Content area -->
                <div class="page-header" id="news-edit">

                    <div class="page-header-content" id="main-pnl">
                        <div class="panel-heading" id="header-page">
                            <h6 class="panel-title">

                                <button type="button" data-toggle="modal" id="btn-new-post" class="btn btn-default ibtn-xs btn-primary heading-btn _newspost"><i class="icon-quill4 position-left"></i>Viết bài mới</button>
                                <button type="button" data-toggle="modal" id="btn-new-reload" class="btn btn-default ibtn-xs heading-btn _newsreload" title="Tải lại"><i class="icon-reload-alt position-left"></i></button>
                            </h6>
                            <div class="heading-elements">
                                <div class="input-group">
                                    <input type="text" class="form-control _search-data" id="_search-data" placeholder="Tìm kiếm bài viết">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
                                    </span>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="icon-menu"></i></a>

                        </div>
                        <div class="navbar navbar-default navbar-xs navbar-component" style="margin-right: 0; margin-left: 0">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-filter" style="padding-top: 1px">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <div style="margin: 3px 3px 0 3px">
                                            <select data-placeholder="Select your state" class="select" id="zone-filer-ddl" style="width: 200px" tabindex="4">
                                                <option value="">-- Chuyên mục --</option>
                                                <asp:Repeater runat="server" ID="ZoneRpt">
                                                    <ItemTemplate>
                                                        <option value='<%#Eval("Id") %>' data-parent="<%#Eval("ParentId") %>"><%#Eval("Name") %></option>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <option value='4532' data-parent="0">Has#Tag</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="dropdown">
                                        <div style="margin: 3px 3px 0 3px">
                                            <select data-placeholder="Select your state" class="select" id="zone-type-ddl" style="width: 200px" tabindex="4">
                                                <option value="4">Tin bài</option>
                                                <option value='9' data-parent="0">Has#Tag</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="dropdown">
                                        <div id="date-filter" class="pull-right date-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; width: 100%; height: 30px; border: 1px solid #ccc;">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;<span></span><b class="caret"></b>
                                        </div>
                                    </li>

                                </ul>
                                <div class="navbar-right">
                                    <div id="data-pager">
                                        <span id="rowInTotals"></span>
                                        <div class="ipagination iweb">
                                            <a href="#" class="first" data-action="first">&laquo;</a>
                                            <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                            <input type="text" readonly="readonly" />
                                            <a href="#" class="next" data-action="next">&rsaquo;</a>
                                            <a href="#" class="last" data-action="last">&raquo;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="module-content">
                            <div class="btn-group btn-action" style="display: none">
                                <button type="button" style="display: none" class="btn btn-default btn-sm _view" data-popup="tooltip" data-placement="top" data-original-title="Xem"><i class="icon-eye"></i></button>
                                <button type="button" class="btn btn-default btn-sm _edit" data-popup="tooltip" data-original-title="Sửa bài"><i class="icon-pencil7"></i></button>
                                <button type="button" data-status="<%=(int)NewsStatus.MovedToTrash %>" class="btn btn-default btn-sm _delete status" data-popup="tooltip" data-original-title="Xóa bài"><i class="icon-bin"></i></button>
                                <button type="button" data-status="<%=(int)NewsStatus.Published %>" class="btn btn-default btn-sm _publish status" style="display: none" data-popup="tooltip" data-original-title="Xuất bản"><i class="icon-file-upload"></i></button>
                                <button type="button" data-status="<%=(int)NewsStatus.Unpublished %>" class="btn btn-default btn-sm _unpublish status" style="display: none" data-popup="tooltip" data-original-title="Hạ bài"><i class="icon-file-download"></i></button>
                                <label data-popup="tooltip" data-original-title="Đã chọn">[<label id="rows"></label>]</label>
                            </div>
                            <div id="data-list">
                                <div class="datatable-scroll _list">
                                </div>

                            </div>


                        </div>

                    </div>
                    <div class="page-header-content" id="sub-pnl">
                    </div>
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <!-- Modal -->
    <div class="modal right fade" id="news-form" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" id="news-edit-content">
                </div>

            </div>
        </div>
    </div>
    <div class="modal right fade" id="news-preview" tabindex="1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body" id="news-preview-content">
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js?v=<%=UIHelper.Version %>"></script>
    <script type="text/javascript">
        var RAllZones = <%= NewtonJson.Serialize(GetAllZoneWithTreeViewSimpleFields())%>;
    </script>
    <script src="/Libs/autosize.js"></script>
    <script src="/CMS/Themes/js/textcount/textcounter.min.js"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js"></script>
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script src="/CMS/Themes/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/CMS/Themes/js/plugins/pickers/anytime.min.js"></script>
    <script src="/CMS/Themes/js/ftag/ftag.js"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/jsTree/dist/jstree.min.js"></script>
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/Project/Scripts/project.js?v=<%=UIHelper.Version %>"></script>
    <script>
        setTimeout(function () {
            $('#zone-filer-ddl,#zone-type-ddl').fselect({
                dropDownWidth: 0,
                autoResize: false
            });

            function cb(start, end, label) {
                $('#date-filter span').html(label + ": " + start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            }

            moment.locale('vi');
            $('#date-filter').daterangepicker({
                //  startDate: start,
                //endDate: end,
                "opens": "right",
                ranges: {
                    'Tất cả': [new Date('01/01/2014'), new Date('12/31/2050')],
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, function (start, end, label) {
                $('#date-filter span').html(label + ": " + start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            });
            $('#date-filter span').html("Tất cả: " + moment("01/01/2014").format('DD/MM/YYYY') + ' - ' + moment("12/31/2050").format('DD/MM/YYYY'));
        }, 200);

        $(function () {
            //     alert(R.UnicodeToSeo('đường @ *% về thênh thang, nghe / nghé'))
        })
    </script>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS/Master/ICMS.Master" AutoEventWireup="true" CodeBehind="Zone.aspx.cs" Inherits="Golden.CMS.Pages.Zone" %>

<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderCph" runat="server">
    <link href="/CMS/Themes/js/comfirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="/CMS/Themes/js/fselect/styles.css" rel="stylesheet" />
    <link href="/CMS/Modules/Zone/Styles/main.css" rel="stylesheet" />
    <link href="/CMS/Modules/News/Styles/main.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCph" runat="server">
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main" id="cms-sidebar">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <li class="navigation-header"><span>Danh mục</span> <i class="icon-menu" title="Main pages"></i></li>
                            <%--<li class="active n-item" data="1"><a href="javascript:void(0)"><i class="icon-magazine position-left"></i><span>Tin tức</span></a></li>
                            <li data="4" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Dự án</a></li>
                            <li data="13" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Sản phẩm BĐS</a></li>--%>
                            <li data="<%=(int)ZoneType.Service %>" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Dịch vụ</a></li>
                            <li data="<%=(int)ZoneType.Coach %>" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Huấn luyện viên</a></li>
                            <li data="<%=(int)ZoneType.Blog %>" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Tin tức</a></li>
                            <%--<li data="17" class="n-item"><a href="javascript:void(0)"><i class="icon-file-presentation"></i>Dịch vụ</a></li>--%>

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper" style="background: #fff">

                <!-- Content area -->
                <div class="page-header" id="news-edit">
                    <div class="page-header-content" id="sub-pnl">

                        <link href="/CMS/Modules/Zone/Styles/style.css" rel="stylesheet" />
                        <div class="panel-heading" id="header-page">
                            <h6 class="panel-title">

                                <button type="button" data-toggle="modal" id="zoneAddBtn" class="btn btn-default ibtn-xs btn-primary heading-btn _newspost"><i class="icon-plus22 position-left"></i>Thêm mới</button>
                                <button type="button" data-toggle="modal" id="btn-new-reload" class="btn btn-default ibtn-xs heading-btn _newsreload" title="Tải lại"><i class="icon-reload-alt position-left"></i></button>
                            </h6>
                            <div class="heading-elements">
                                <div class="input-group">
                                    <input type="text" class="form-control _search-data" id="_search-data" placeholder="Tìm kiếm bài viết">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-primary ibtn-xs _search" type="button" title="Tìm kiếm bài viết"><i class="icon-search4"></i></button>
                                    </span>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="icon-menu"></i></a>

                        </div>
                        <div class="navbar navbar-default navbar-xs navbar-component" style="margin-right: 0; margin-left: 0">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-filter" style="padding-top: 1px">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <div style="margin: 3px 3px 0 3px">
                                            <select data-placeholder="Select your state" class="select" id="zoneStatusdll" style="width: 200px" tabindex="4">
                                                <option value="-1">Trạng thái</option>
                                                <option value="1">Hiển thị</option>
                                                <option value="2">Ẩn</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                                <div class="navbar-right">
                                    <div id="data-pager">
                                        <span id="rowInTotals"></span>
                                        <div class="ipagination iweb">
                                            <a href="#" class="first" data-action="first">&laquo;</a>
                                            <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                            <input type="text" readonly="readonly" />
                                            <a href="#" class="next" data-action="next">&rsaquo;</a>
                                            <a href="#" class="last" data-action="last">&raquo;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="module-content">
                            <div id="zone-wrapper">
                                <div class="datatable-scroll _list">
                                </div>

                            </div>
                        </div>
                        <div id="IMSOverlayWrapper">
                            <div id="IMSOverlayWrapperIn">
                                <div id="IMSOverlayContent">
                                </div>
                                <span id="IMSOverlayClose"></span>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>

    <!-- Modal -->
    <div class="modal right fade" id="news-form" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" id="news-edit-content">
                </div>

            </div>
        </div>
    </div>
    <div class="modal right fade" id="news-preview" tabindex="1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body" id="news-preview-content">
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterCph" runat="server">
    <script src="/CMS/Themes/js/ckeditor4.2/ckeditor.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ckeditor4.2/config.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/textcount/textcounter.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/jquery.jqpagination.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/ui/moment/moment_locales.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/daterangepicker.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/plugins/pickers/anytime.min.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/ftag/ftag.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/fselect/fselect.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Themes/js/comfirm/jquery-confirm.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/Zone/Scripts/zone.js?v=<%=UIHelper.Version %>"></script>
    <%--File manager--%>
    <script>
        var RAllZones = <%= JsonConvert.SerializeObject(GetAllZoneWithTreeViewSimpleFields())%>;
    </script>
    <script src="/CMS/Modules/FileManager/Scripts/image.upload.js?v=<%=UIHelper.Version %>"></script>
    <script src="/CMS/Modules/FileManager/Scripts/image.js?v=<%=UIHelper.Version %>"></script>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="Golden.Pages.Blog" %>

<%@ Register Src="~/Pages/Action/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>

<%@ Import Namespace=" Mi.BO.Base.News" %>
<%@ Import Namespace="  Mi.BO.Base.Zone" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>
<%@ Import Namespace="  Golden.Core.Helper" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%

        int totalRow = 0;
        int totalRow1 = 0;
        ZoneEntity Zone;
        string alias = "blog";
        Zone = ZoneBo.GetZoneByAlias(alias);
        if (Zone != null)
        {
            Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
            Page.MetaDescription = Zone.MetaDescription;
            Page.MetaKeywords = Zone.MetaKeyword;
        }

        var page = Utils.ToInt32Return0(Request.QueryString["page"]);
        var obj = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow).Where(n => n.IsVideo == false).OrderByDescending(n => n.CreatedDate).Take(6).ToList();
        var obj1 = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager1.PageSize, ref totalRow1).Where(n => n.IsVideo == true).OrderByDescending(n => n.CreatedDate).Take(6).ToList();
        var zoneBlogId = ZoneBo.GetZoneByAlias("blog");
        var obj2 = NewsBo.GetByParentId(zoneBlogId.Id, (int)ZoneType.Blog).Where(n => !n.Name.Equals("Lịch Học")).ToList();

        UIPager.TotalItems = totalRow;
        UIPager1.TotalItems = totalRow1;
    %>
    <section class="list-blog-cate">
        <div class="container">
            <div class="d-md-flex justify-content-md-between">
                <div class="heading">
                    Chuyên mục
                </div>
                <div class="list align-self-center d-md-flex flex-wrap">
                    <%
                        foreach (var n in obj2)
                        {
                    %>
                    <div class="item">
                          <a href="/blog/<%=n.ShortURL %>" title=""><%= n.Name %> </a>
                    </div>
                    <%}
                    %>
                </div>
            </div>
        </div>
    </section>

    <div class="list-blog pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="heading-blog-ss mb-3">
                        BÀI VIẾT HỮU ÍCH NHẤT
                    </div>
                </div>
            </div>
            <div class="row">
                <% 
                    foreach (var n in obj)
                    {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="blog-item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/uploads/thumb/<%= n.Avatar %>" class="imgcover"   style="width:350px;height:233px"/>
                            </a>
                        </div>
                        <h3 class="title">
                            <a title="<%= n.Title%>" href="/blog/<%= n.Url %>/<%=n.Title %>.<%=n.Id %>"><%=n.Title %></a>
                        </h3>
                    </div>
                </div>
                <%}
                %>
                <div class="row float-lg-right">
                    <uc1:UIPager runat="server" ID="UIPager" PageSize="9" PageShow="5" />
                </div>
            </div>
        </div>
    </div>

    <section class="res res-2 py-5">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        LIỆU PHÁP CĂNG CƠ THƯ GIÃN HOẠT
                        ĐỘNG NHƯ THẾ NÀO?
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12 _tName" placeholder="Họ và tên" id="txtFullName">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12 _tPhone" placeholder="Số điện thoại" id="txtPhoneNumber">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control col-sm-9 col-12 _tAddress" placeholder="Nơi sinh sống" id=" txtAddress">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-res-yellow _bReginster" id="btnRegister">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="video">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="heading-blog-ss mb-2">
                        Video
                    </div>
                </div>
            </div>
            <div class="row">
                <% 
                    foreach (var v in obj1)
                    {
                %>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="video-item">
                        <iframe width="100%" height="300" src="<%= v.Sapo.Replace("watch?v=","embed/")%>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <%}
                %>
                <div class="row float-lg-right">
                    <uc1:UIPager runat="server" ID="UIPager1" PageSize="6" PageShow="5" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().find('._tName').val());
            var name = $(this).parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Địa chỉ: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>

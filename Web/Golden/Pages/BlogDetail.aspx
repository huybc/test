﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="Golden.Pages.BlogDetail" %>

<%@ Import Namespace=" Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%
        var configs = ConfigBo.GetListConfigByPage("home", 6);
        var tieu_de_dang_ky = configs.Where(c => c.ConfigLabel.Equals("Tiêu đề đăng ký")).FirstOrDefault();
        var video_dang_ky = configs.Where(c => c.ConfigLabel.Equals("Video đăng ký")).FirstOrDefault();
    %>

    <section class="list-blog-cate">
        <div class="container">
            <div class="d-md-flex justify-content-md-between">
                <div class="heading">
                    Chuyên mục
                </div>
                <div class="list align-self-center d-md-flex flex-wrap">
                    <%
                        var zoneBlogId = ZoneBo.GetZoneByAlias("blog");

                        var obj2 = NewsBo.GetByParentId(zoneBlogId.Id, 103).Where(n => !n.Name.Equals("Lịch Học")).ToList();
                        foreach (var n in obj2)
                        {
                    %>
                    <div class="item">
                        <a href="/blog/<%=n.ShortURL %>" title=""><%= n.Name %> </a>
                    </div>
                    <%}
                    %>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-detail py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav class="breadcrumb px-0">
                        <a class="breadcrumb-item" href="/">Trang chủ</a>
                        <a class="breadcrumb-item active" href="blog">Blog</a>
                    </nav>

                    <h1 class="title"><%=obj.Title %></h1>
                    <div class="text-right">
                        <div class="fb-like" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                    </div>
                    <div class="comment-facebook">
                        <div class="fb-comments" data-href="<%=HttpContext.Current.Request.Url.AbsoluteUri %>" data-width="auto" data-numposts="5"></div>
                    </div>
                    <div class="description mb-3"><%=obj.Sapo %></div>
                    <div class="content">
                        <p>
                            <%=obj.Body %>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="list-blog pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="heading-blog-ss mb-3">
                        BÀI VIẾT LIÊN QUAN
                    </div>
                </div>
            </div>

            <div class="row">

                <%
                    var alias = Page.RouteData.Values["zoneName"];
                    int totalRow = 0;

                    var articles = NewsBo.SearchByShortUrl(alias != null ? alias.ToString() : "blog", 1, 12, ref totalRow);
                    articles = articles.Where(it => it.Id != obj.Id);
                    if (alias != null)
                    {
                        foreach (var article in articles)
                        {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="blog-item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Uploads/thumb/<%=article.Avatar %>" class="imgcover" />
                            </a>
                        </div>
                        <h3 class="title">
                            <a title="<%=article.Title %>" href="/<%=article.Url %>"><%=article.Title %></a>
                        </h3>
                    </div>
                </div>
                <% } %>

                <%
                    }
                    else
                    {
                        foreach (var article in articles)
                        {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="blog-item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/Uploads/thumb/<%=article.Avatar %>" class="imgcover" />
                            </a>
                        </div>
                        <h3 class="title">
                            <a title="<%=article.Title %>" href="/<%= article.Url %>-<%= article.Id%>.htm"><%=article.Title %></a>
                        </h3>
                    </div>
                </div>

                <% }
                    } %>
                <% if (!articles.ToArray().Any())
                    { %>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <p class="text-center">Đang cập nhật</p>
                </div>
                <%}%>
            </div>
        </div>
    </section>
    <section class="res py-5">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 align-self-center">
                    <iframe width="100%" height="300" src="<%=video_dang_ky.ConfigValue.Replace("watch?v=","embed/") %>" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=tieu_de_dang_ky.ConfigValue %>
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12 _tName" placeholder="Họ và tên">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12 _tPhone" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control col-sm-9 col-12 _tAddress" placeholder="Nơi sinh sống">
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn _bReginster">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().find('._tName').val());
            var name = $(this).parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Địa chỉ: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="Coach.aspx.cs" Inherits="Golden.Pages.Coach" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .coach_text{
            height:350px;
        }
        .coach_text .bot{
            word-spacing:2px;
            line-height:16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%var zoneList = ZoneBo.GetAllZone((int)ZoneType.Coach);
        string zListString = "";

        var result = new List<NewsEntity>();
        for (int i = 0; i < zoneList.Count(); i++)
        {
            var r = NewsBo.GetByZoneId(zoneList[i].Id, 100);
            result.AddRange(r);
        }
        var configs = ConfigBo.GetListConfigByPage("coach",6);
    %>
    <section class="res res-2 py-5" style="background-size:cover;background-position: inherit;">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        <%var tieu_de_form = configs.Where(r => r.ConfigName.Equals("HLVTiêu d? form")).SingleOrDefault().ConfigValue; %>
                        <%=tieu_de_form %>
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12 _tName" placeholder="Họ và tên">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12 _tPhone" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control col-sm-9 col-12 _tAddress" placeholder="Nơi sinh sống">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-res-yellow _bReginster">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="huan-luyen-vien mb-5">
        <div class="container">
            <div class="ss-name">
                <%var dong_2 = configs.Where(r => r.ConfigName.Equals("HLVDòng 2")).SingleOrDefault(); %>
                <h3 class="title"><%=dong_2.ConfigLabel %></h3>
                <div class="text"><%=dong_2.ConfigValue %>
                </div>
            </div>
            <div class="row justify-content-left">
                <%foreach (var item in result)
                    {
                        var content = ProjectDetailBo.GetByArticleId(item.Id);
                %>

                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-6 col-12 ">
                    <div class="item">
                        <div class="row no-gutters">
                            <div class="col-xl-5 col-lg-6 col-md-5 col-12 col-12">
                                <div class="image">
                                    <img src="/uploads/thumb/<%=item.Avatar %>" class="w-100 h-100" />
                                    <div class="overlay">
                                    </div>
                                    <a href=" " role="button" data-toggle="modal" data-target=".modal-book" class="btn btn-res">Đăng ký ngay</a>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-7 col-12 col-12 coach_text" style="">
                                <div class="text p-3">
                                    <div class="name">
                                        <div class="top">
                                            <%=item.Title %>
                                        </div>
                                        <div class="bot">
                                            <%=item.Sapo %>
                                        </div>
                                    </div>
                                    <ul class="skill">
                                        <%foreach (var c in content)
                                            { %>
                                        <li><%=c.Content.TrimStart().Replace("<p>","").Replace("</pre>","") %>
</li>
                                        <%} %>

                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>


    <section class="res-bot ">
        <div class="container">
            <div class="ss-name">
                <%var dong_3 = configs.Where(r => r.ConfigName.Contains("HLVDòng 3")).SingleOrDefault(); %>
                <h3 class="title"><%=dong_3.ConfigLabel %></h3>
                <div class="text mb-4 px-3">
                    <%=dong_3.ConfigValue %>
                </div>
                <button class="btn btn-res-blue" data-toggle="modal" data-target=".modal-book">Đăng ký ngay</button>
            </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().find('._tName').val());
            var name = $(this).parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Địa chỉ: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>

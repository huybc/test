﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="Service.aspx.cs" Inherits="Golden.Pages.Service" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var result = ZoneBo.GetAllZone((int)Mi.Entity.Base.Zone.ZoneType.Service).Where(r => r.ParentId != 0 && r.Status!=4).OrderBy(r => r.SortOrder).ToList();
        for (int i = 0; i < result.Count(); i++)
        {
            //Lay ra bai tin duy nhat cua dich vu
            var totalRow = 1;
            var count = 0;
            var newInService = NewsBo.GetByZoneId((int)result[i].Id, totalRow).FirstOrDefault();
    %>
    <%if (i == (0) && newInService != null)
        { %>
    <style>
        .dong1{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat center top ; 
        }
    </style>
    <section class="res service-ss-1 dong1" style="">
        <div class="container">
            <div class="row justify-content-sm-center">

                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=newInService.Title %>
                    </div>
                    <div class="content mb-4 ">
                        <%=Utility.SubWordInString(newInService.Sapo,60) %>
                    </div>
                    <div class="form-group">
                        <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                            <button class="btn btn-res-yellow" data-toggle="modal" data-target=".modal-book">Xem thêm</button></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
            </div>
        </div>
    </section>
    <%} %>
    <%if (i == (1) && newInService != null)
        { %>
    <style>
        .dong2{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat center top ; 
        }
    </style>
    <section class="res-bot dong2" style="">
        <div class="container">
            <div class="ss-name">
                <h3 class="title"><%=newInService.Title %></h3>
                <div class="text mb-4 px-3">
                    <%=Utility.SubWordInString(newInService.Sapo,60) %>
                </div>
                <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                    <button class="btn btn-res-yellow">Xem thêm</button></a>
            </div>
    </section>
    <%} %>
    <%if (i == (2) && newInService != null)
        {%>
    <style>
        .dong3{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat top; 
        }
    </style>
    <section class="res service-ss-3 dong3" style="">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 align-self-center mb-3">
                    <h4 class="text-uppercase mb-3">Đăng ký tập thử miễn phí
                    </h4>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12 _tName" placeholder="Họ và tên">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12 _tPhone" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group mb-3">
                            <input type="text" class="form-control col-sm-9 col-12 _tAddress" placeholder="Nơi sinh sống">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-res-yellow-2 _bReginster" role="button" data-toggle="modal" data-target=".modal-book">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 align-self-center ">
                    <div class="heading">
                        <%=newInService.Title %>
                    </div>
                    <div class="content mb-4 ">
                        <%=Utility.SubWordInString(newInService.Sapo,60) %>
                    </div>
                    <div class="form-group">
                        <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                            <button class="btn btn-res-yellow">Xem thêm</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <%} %>
    <%if (i == (3) && newInService != null)
        { %>
    <style>
        .dong4{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat center center; background-size:cover
        }
    </style>
    <section class="res-bot service-ss-4 d-flex dong4" style="">
        <div class="container align-self-center">
            <div class="ss-name">
                <h3 class="title"><%=newInService.Title %></h3>
                <div class="text mb-4 px-3">
                    <%=Utility.SubWordInString(newInService.Sapo,60) %>
                </div>
                <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                    <button class="btn btn-res-yellow">Xem thêm</button></a>
            </div>
        </div>
    </section>
    <%} %>
    <%if (i == (4) && newInService != null)
        { %>
    <style>
        .dong5{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat top; 
        }
    </style>
    <section class="res service-ss-1 dong5" style="">
        <div class="container">
            <div class="row justify-content-sm-center">

                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=newInService.Title %>
                    </div>
                    <div class="content mb-4 ">
                        <%=Utility.SubWordInString(newInService.Sapo,60) %>
                    </div>
                    <div class="form-group">
                        <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                            <button class="btn btn-res-yellow">Xem thêm</button></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
            </div>
        </div>
    </section>
    <%} %>
    <%if (i == (5) && newInService != null)
        { %>
    <style>
        .dong6{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat top; 
        }
    </style>
    <section class="res-bot  dong6" style="">
        <div class="container align-self-center">
            <div class="ss-name">
                <h3 class="title"><%=newInService.Title %></h3>
                <div class="text mb-4 px-3">
                    <%=Utility.SubWordInString(newInService.Sapo,60) %>
                </div>
                <div class="form-group">
                    <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                        <button class="btn btn-res-yellow">Xem thêm</button></a>
                </div>
            </div>
    </section>
    <%  count++;
        } %>
    <%if (i == (6) && newInService != null)
        { %>
    <style>
        .dong7{
            background: url(/uploads/<%=newInService.Avatar%>) no-repeat top; 
        }
    </style>
    <section class="res service-ss-1 dong7" style="">
        <div class="container">
            <div class="row justify-content-sm-center">

                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=newInService.Title %>
                    </div>
                    <div class="content mb-4 ">
                        <%=Utility.SubWordInString(newInService.Sapo,60) %>
                    </div>
                    <div class="form-group">
                        <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                            <button class="btn btn-res-yellow" data-toggle="modal" data-target=".modal-book">Xem thêm</button></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
            </div>
        </div>
    </section>
    <%  
        } %>
    <%if (i == (7) && newInService != null)
        { %>
    <section class="res service-ss-1 " style="background: url(/uploads/<%=newInService.Avatar%>) no-repeat center top fixed; background-size:100% auto">
        <div class="container">
            <div class="row justify-content-sm-center">

                <div class="col-lg-6 col-md-9 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=newInService.Title %>
                    </div>
                    <div class="content mb-4 ">
                        <%=Utility.SubWordInString(newInService.Sapo,60) %>
                    </div>
                    <div class="form-group">
                        <a href="/services/<%=result[i].ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>">
                            <button class="btn btn-res-yellow" data-toggle="modal" data-target=".modal-book">Xem thêm</button></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
            </div>
        </div>
    </section>
    <%  
        } %>

    <%

        } %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().find('._tName').val());
            var name = $(this).parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Địa chỉ: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>

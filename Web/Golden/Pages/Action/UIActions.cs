﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi.Action;
using Mi.Action.Core;
using Mi.BO.Base.Customer;
using Mi.BO.Base.News;
using Mi.BO.Base.Zone;
using Mi.Common;
using Mi.Entity.Base.Customer;
using Mi.Entity.Base.Zone;
using Mi.Entity.ErrorCode;
using Golden.CMS.Modules.Service.Action;
using Golden.Core.Helper;

namespace Golden.Pages.Action
{
    public class UIActions : ActionBase
    {
        protected override string ResponseContentType
        {
            get { return "text/plain; charset=utf-8"; }
        }
        protected override bool IsResponseDataDirectly
        {
            get { return false; }
        }

        protected override object ProcessAction(string functionName, HttpContext context)
        {
            var responseData = new ResponseData();
            switch (functionName)
            {

                case "save":
                    responseData = AddCustomer();
                    break;
                case "faq-detail":
                    responseData = FAQDetail();
                    break;

            }

            return responseData;
        }

        private ResponseData FAQDetail()
        {

            var responseData = ConvertResponseData.CreateResponseData("{}", 0, "\\Pages\\FAQDetail.aspx");
            responseData.Success = true;
            return responseData;
        }

        private ResponseData AddCustomer()
        {
            var responseData = new ResponseData();
            string name = GetQueryString.GetPost("name", string.Empty);
            string email = GetQueryString.GetPost("email", string.Empty);
            string phoneNumber = GetQueryString.GetPost("phoneNumber", string.Empty);
            string note = GetQueryString.GetPost("note", string.Empty);


            if (!string.IsNullOrEmpty(name))
            {

                var obj = new CustomerEntity
                {
                    FullName = name,
                    Email = email,
                    Mobile = phoneNumber,
                    Note = note,
                    Type = "blog"
                };
                int outId = 0;

                responseData = ConvertResponseData.CreateResponseData(CustomerBo.Create(obj, ref outId));

                if (outId > 0)
                {
                    responseData.Success = true;
                }
                responseData.Message = "Thêm mới thành công !";

            }
            else
            {
                responseData.Success = false;
                responseData.Message = "Error";
            }
            return responseData;
        }


    }
}
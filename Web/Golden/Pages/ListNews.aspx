﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="ListNews.aspx.cs" Inherits="Golden.Pages.ListNews" %>

<%@ Register Src="~/Pages/Action/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>

<%@ Import Namespace=" Mi.BO.Base.News" %>
<%@ Import Namespace="  Mi.BO.Base.Zone" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>
<%@ Import Namespace="  Golden.Core.Helper" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
         active1{
            color: #33bcff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%

        int totalRow = 0;
        ZoneEntity Zone;
        var path = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
        var alias = path[2];
        Zone = ZoneBo.GetZoneByAlias(alias);
        if (Zone != null)
        {
            Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
            Page.MetaDescription = Zone.MetaDescription;
            Page.MetaKeywords = Zone.MetaKeyword;
        }
        var shortUrl = Utils.ToInt32Return0(Request.QueryString["page"]);
        var page = Utils.ToInt32Return0(Request.QueryString["page"]);
        var obj = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow).OrderByDescending(n => n.CreatedDate).Take(9).ToList();
        var obj1 = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow).Where(n => n.IsVideo == true).OrderByDescending(n => n.CreatedDate).Take(6).ToList();
        var zoneBlogId = ZoneBo.GetZoneByAlias("blog");
        var obj2 = NewsBo.GetByParentId(zoneBlogId.Id, 103).Where(n => !n.Name.Equals("Lịch Học")).ToList();

        UIPager.TotalItems = totalRow;
    %>
    <section class="list-blog-cate">
        <div class="container">
            <div class="d-md-flex justify-content-md-between">
                <div class="heading">
                    Chuyên mục
                </div>
                <div class="list align-self-center d-md-flex flex-wrap">
                    <% 
                        foreach (var n in obj2)
                        {
                    %>
                    <div class="item subMenu1">
                        <a href="/blog/<%=n.ShortURL %>" title=""><%= n.Name %> </a>
                    </div>
                    <%}
                    %>
                </div>
            </div>
        </div>
    </section>

    <div class="list-blog pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="heading-blog-ss mb-3">
                        BÀI VIẾT HỮU ÍCH NHẤT
                    </div>
                </div>
            </div>
            <div class="row">
                <%
                    if (alias == "video")
                    {%>
                <% 
                    foreach (var n in obj)
                    {
                %>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="video-item">
                        <iframe width="100%" height="300" src="<%= n.Sapo.Replace("watch?v=","embed/")%>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <%}
                %>
                <%}
                    else
                    {%>
                <% 
                    foreach (var n in obj)
                    {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="blog-item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/uploads/thumb/<%= n.Avatar %>" class="imgcover" style="width:350px;height:233px"/>
                            </a>
                        </div>
                        <h3 class="title">
                            <a title="<%= n.Title%>" href="/blog/<%= n.Url %>/<%=n.Title %>.<%=n.Id %>"><%=n.Title %></a>
                        </h3>
                    </div>
                </div>
                <%}
                %>
                <%}
                %>
                <div class="row float-lg-right">
                    <uc1:UIPager runat="server" ID="UIPager" PageSize="9" PageShow="5" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        var subUrl = window.location.pathname.split('/')[2];
        console.log(subUrl);
        $('.list-blog-cate .subMenu1 a[href="/blog/' + subUrl + '"]').css("color","#33bcff");
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Golden.Pages.Contact" %>
<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <%var configs = ConfigBo.GetListConfigByPage("contact", 6);
        var hotline = configs.Where(r => r.ConfigName.Equals("HotLineGym")).SingleOrDefault().ConfigValue;
        var dia_chi = configs.Where(r => r.ConfigName.Equals("Dia_Chi")).SingleOrDefault().ConfigValue;    
        var ten_gym = configs.Where(r => r.ConfigName.Equals("Ten_Gym")).SingleOrDefault().ConfigValue;    
        %>

    <section class="banner-vmb ">
        <img src="/Themes/imgs/bg-lien-he.jpg" class="w-100" />
        <div class="ovelay">
            <div class="container align-self-center">
                <div class="heading">
                    <div class="top text-center">
                        Liên hệ
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="contact">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="box">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-12 left">

                            <div class="form-group row justify-content-center">

                                <div class="col-md-9 col-sm-12 col-12">
                                    <input class="form-control _tName" type="text" placeholder="Họ và tên">
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">

                                <div class="col-md-9 col-sm-12 col-12">
                                    <input class="form-control _tPhone" type="tel" placeholder="Số điện thoại">
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">

                                <div class="col-md-9 col-sm-12 col-12">
                                    <textarea class="form-control _tAddress"  type="text" rows="4"
                                        placeholder="Tin nhắn"></textarea>
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-9 col-sm-12 col-12 text-right">
                                    <button class="btn btn-book _bReginster">Gửi đi</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-12 d-flex  right">
                            <div class="">
                                <div class="text-uppercase mb-2">
                                    Gọi ngay
                                </div>
                                <div class="h3 font-weight-bold mb-4" style="color: #fff;">
                                    <%=hotline %>
                                </div>
                                <div class="text-uppercase font-weight-bold mb-3 h4">
                                    <%=ten_gym %>

                                </div>
                                <ul class="info-company">
                                    <li><%=dia_chi %>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="w-100 " style="z-index: -1; margin-top: -100px;">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.2025847209547!2d105.78940601440732!3d21.024578593298102!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab5183e6caf7%3A0x4212c4ebde477940!2zNjggRMawxqFuZyDEkMOsbmggTmdo4buHLCBZw6puIEhvw6AsIEPhuqd1IEdp4bqleSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1566827218501!5m2!1svi!2s"
            width="100%" height="500" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().parent().find('._tName').val());
            var name = $(this).parent().parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Tin nhắn: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>

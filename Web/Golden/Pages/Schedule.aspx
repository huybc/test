﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="Golden.Pages.Schedule" %>

<%@ Register Src="~/Pages/Action/UIPager.ascx" TagPrefix="uc1" TagName="UIPager" %>

<%@ Import Namespace=" Mi.BO.Base.News" %>
<%@ Import Namespace="  Mi.BO.Base.Zone" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>
<%@ Import Namespace="  Golden.Core.Helper" %>
<%@ Import Namespace=" Mi.Entity.Base.Zone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <%

        int totalRow = 0;
        ZoneEntity Zone;
        string alias = "lich-hoc";
        Zone = ZoneBo.GetZoneByAlias(alias);
        if (Zone != null)
        {
            Page.Title = !string.IsNullOrEmpty(Zone.MetaTitle) ? Zone.MetaTitle : Zone.Name;
            Page.MetaDescription = Zone.MetaDescription;
            Page.MetaKeywords = Zone.MetaKeyword;
        }

        var page = Utils.ToInt32Return0(Request.QueryString["page"]);
        var obj = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
        var obj1 = NewsBo.SearchByShortUrl(alias, page == 0 ? 1 : page, UIPager.PageSize, ref totalRow);
        var zoneBlogId = ZoneBo.GetZoneByAlias("blog");
        var obj2 = NewsBo.GetByParentId(zoneBlogId.Id, (int)ZoneType.Blog);

        UIPager.TotalItems = totalRow;
    %>
    <div class="list-blog pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="heading-blog-ss mb-3">
                        LỊCH HỌC
                    </div>
                </div>
            </div>
                 <div class="row">
                <% 
                    foreach (var n in obj)
                    {
                %>
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="blog-item">
                        <div class="image">
                            <a href="" title="">
                                <img src="/uploads/thumb/<%= n.Avatar %>" class="imgcover" />
                            </a>
                        </div>
                        <h3 class="title">
                            <a title="<%= n.Title%>" href="/blog/<%= n.Url %>/<%=n.Title %>.<%=n.Id %>"><%=n.Title %></a>
                        </h3>
                    </div>
                </div>
                <%}
                %>
                <div class="row float-lg-right">
                    <uc1:UIPager runat="server" ID="UIPager" PageSize="9" PageShow="5" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

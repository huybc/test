﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="ServiceDetail.aspx.cs" Inherits="Golden.Pages.ServiceDetail" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style>
        .opac-div {
            background-color: darkgray;
            opacity: 0.5;
        }

        .opac-text {
            opacity: 1;
        }
        .image-project { width: 100%; height: 100%; background-size: cover !important; min-height: 400px }
    </style>
    <%
        var path = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
        var alias = path[2];
        var newsId = path[3].Split('.')[1].ToString();
        var zoneName = ZoneBo.GetZoneByAlias(alias);
        var newDetail = NewsBo.GetNewsDetailById(int.Parse(newsId));
        var content = ProjectDetailBo.GetByArticleId(newDetail.Id);
        var isRight = true;

        var configs = ConfigBo.GetListConfigByPage("service", 6);
        var video_dang_ky = configs.Where(r => r.ConfigName.Equals("Service_Video")).SingleOrDefault().ConfigValue;
        var tieu_de_dang_ky = configs.Where(r => r.ConfigName.Equals("Service_title")).SingleOrDefault().ConfigValue;
        
    %>
    <%for (int i = 0; i < content.Count(); i++)
        {
            
    %>

    <%if (i == 0)
        {
            var bgUrl = content[i].Image;
    %>
    <%--<section class="res service-ss-1 " style="background: url(/uploads/<%=bgUrl%>) no-repeat center left;">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-2 col-sm-11 col-12 align-self-center">
                </div>
                <div class="col-lg-6 col-md-9 col-sm-11 col-12 opac-div">
                    <div class="heading opac-text">
                        <%=content[i].Title %>
                    </div>
                    <div class="content mb-4 opac-text">
                        <%=content[i].Content %>
                    </div>

                </div>

            </div>
        </div>
    </section>--%>
    <section class="banner-vmb mb-4">
        <a href="" title="" data-toggle="modal" data-target=".modal-book">
            <img src="/uploads/<%=bgUrl%>" class="w-100" />
        </a>

    </section>
    <%  
        } %>
    <%if (i > 0)
        { %>
    <section>
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="row  mx-0">
                    <%if (isRight == true)
                        { %>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0 bg-ededed">
                        <div class="image-project " style="background: url('/uploads/<%=content[i].Image %>') no-repeat center center;">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0">
                        <div class="content p-5">
                            <h3 class="text-uppercase"><%=content[i].Title %></h3>
                            <p><%=content[i].Content %></p>
                        </div>
                    </div>
                    <%

                        } %>
                    <%if (isRight == false)
                        { %>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0">
                        <div class="content p-5">
                            <h3 class="text-uppercase"><%=content[i].Title %></h3>
                            <p><%=content[i].Content %></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 px-0 bg-ededed">
                        <div class="image-project " style="background: url('/uploads/<%=content[i].Image %>') no-repeat center center;">
                        </div>
                    </div>

                    <%
                        } %>
                </div>
            </div>
        </div>
    </section>
    <%} %>

    <%
            isRight = !isRight;
        } %>

    <section class="res py-5">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 align-self-center">
                    <iframe width="100%" height="300" src="<%=video_dang_ky.Replace("watch?v=","embed/") %>" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=tieu_de_dang_ky %>
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12" placeholder="Họ và tên">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control col-sm-9 col-12" placeholder="Nơi sinh sống">
                        </div>
                        <div class="form-group">
                            <button class="btn">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Golden.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Golden._default" %>

<%@ Import Namespace="Mi.BO.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.Zone" %>
<%@ Import Namespace="Mi.Entity.Base.News" %>
<%@ Import Namespace="Mi.BO.Base.ProjectDetail" %>
<%@ Import Namespace="Mi.Common" %>
<%@ Import Namespace="Golden.Core.Helper" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="//widget.manychat.com/724769421221321.js" async="async"></script>
    <style>
        .coach_text{
            height:350px;
        }
        .coach_text .bot{
            word-spacing:2px;
            line-height:16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <% var configs = ConfigBo.GetListConfigByPage("home", 6);
        var dong_1 = configs.Where(c => c.ConfigName.Equals("Dòng 1")).FirstOrDefault();
        var dong_2 = configs.Where(c => c.ConfigName.Equals("Dòng 2")).FirstOrDefault();
        var dong_3 = configs.Where(c => c.ConfigName.Equals("Dòng 3")).FirstOrDefault();
        var dong_4 = configs.Where(c => c.ConfigName.Equals("Dòng 4")).FirstOrDefault();
        var dong_5 = configs.Where(c => c.ConfigName.Equals("Dòng 5")).FirstOrDefault();
        var tieu_de_dang_ky = configs.Where(c => c.ConfigLabel.Equals("Tiêu đề đăng ký")).FirstOrDefault();
        var video_dang_ky = configs.Where(c => c.ConfigLabel.Equals("Video đăng ký")).FirstOrDefault();
        var slide_banner = ConfigBo.AdvGetByType(101);

    %>

    <%//cau hinh %>
    <section class="banner-vmb mb-4 " style="overflow:hidden">
        <div class="single-item">
            <%foreach (var item in slide_banner)
                { %> 
                <div>
                <a href="" title="" data-toggle="modal" data-target=".modal-book">
                    <img src="/uploads/<%=item.Thumb %>" class="w-100" />
                </a>
            </div>
            <%} %>
        </div>

        

    </section>
    <%//cau hinh %>
    <section class="services">
        <div class="container">

            <div class="ss-name">
                <h3 class="title"><%=dong_1.ConfigLabel %></h3>
                <div class="text">
                    <%=dong_1.ConfigValue %>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <%
                var services = ZoneBo.GetAllZone((int)ZoneType.Service).Where(r => r.ParentId > 0).OrderBy(r => r.SortOrder).Take(6).ToList();
                foreach (var item in services)
                {
                    var newInService = NewsBo.GetByZoneId(item.Id, 1).FirstOrDefault();
            %>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="item">
                    <img src="/uploads/<%=item.Avatar %>" class="w-100" />
                    <div class="overlay">
                    </div>
                    <%if (newInService != null)
                        {%>
                    <div class="title">
                        <a href="/services/<%=item.ShortUrl %>/<%=newInService.Url %>.<%=newInService.Id %>" title=""><%=item.Name %></a>
                    </div>
                    <%}
                        else
                        { %>
                    <div class="title">
                        <a href="" title=""><%=item.Name %></a>
                    </div>

                    <%} %>
                </div>
            </div>
            <%} %>
        </div>
    </section>
<section class="why">
        <div class="container">
            <div class="ss-name">
                <h3 class="title"><%=dong_2.ConfigLabel %></h3>
                <div class="text">
                    <%=dong_2.ConfigInitValue %>
                </div>
            </div>
            <%//1 vung hien thi %>
            <div class="row justify-content-center">
                <%var stamps = ConfigBo.AdvGetByType(102).OrderBy(r => r.SortOrder); %>
                <%foreach (var item in stamps)
                    { %>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-11">
                    <div class="item">
                        <div class="image">
                            <img src="/uploads/thumb/<%=item.Thumb %>" />
                        </div>
                        <div class="text">
                            <%=item.Content %>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </section>

    <section class="huan-luyen-vien">
        <div class="container">
            <%//CAu hinh %>
            <div class="ss-name">
                <h3 class="title"><%=dong_3.ConfigLabel %></h3>
                <div class="text">
                    <%=dong_3.ConfigValue %>
                </div>
            </div>
            <div class="row justify-content-left">
                <%var coachZoneList = ZoneBo.GetAllZone((int)ZoneType.Coach).OrderBy(c => c.CreatedDate).Take(6).ToList();

                    var coachListNews = new List<NewsEntity>();
                    for (int i = 0; i < coachZoneList.Count(); i++)
                    {
                        var r = NewsBo.GetByZoneId(coachZoneList[i].Id, 100);
                        coachListNews.AddRange(r);
                    }
                    coachListNews = coachListNews.OrderByDescending(cln => cln.IsHot).Take(4).ToList();
                    foreach (var item in coachListNews)
                    {
                %>
                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-6 col-12">
                    <div class="item">
                        <div class="row no-gutters">
                            <div class="col-xl-5 col-lg-6 col-md-5 col-12 col-12">
                                <div class="image">
                                    <img src="/uploads/<%=item.Avatar %>" class="w-100 h-100" />
                                    <div class="overlay">
                                    </div>
                                    <a href="" role="button" data-toggle="modal" data-target=".modal-book" data-note="<%=item.Title %>" class="btn btn-res">Đăng ký ngay</a>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-7 col-12 col-12 coach_text">
                                <div class="text p-3">
                                    <div class="name">
                                        <div class="top">
                                            <%=item.Title %>
                                        </div>
                                        <div class="bot">
                                            <%=item.Sapo %>
                                        </div>
                                    </div>
                                    <ul class="skill">
                                        <%var content = ProjectDetailBo.GetByArticleId(item.Id);
                                            foreach (var c in content)
                                            {
                                        %>
                                        <li><%=c.Content.TrimStart().Replace("<p>","").Replace("</pre>","") %>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
</section>

    <section class="gallery">
        <div class="container">
            <%//Cau hinh %>
            <div class="ss-name">
                <h3 class="title"><%=dong_4.ConfigLabel %></h3>
                <div class="text">
                    <%=dong_4.ConfigValue %>
                </div>
            </div>
        </div>
        <%//Vung hien thi %>
        <div class="row no-gutters">
            <% var anh_trang_chu = ConfigBo.AdvGetByType(103).ToList();
                for (int i = 0; i < anh_trang_chu.Count(); i++)
                {%>
            <%if (i <= 1)
                { %>
            <div class="col-xl-6 col-lg-6 col-sm-6 col-12">
                <img src="/uploads/<%=anh_trang_chu[i].Thumb %>" class="imgcover" />
            </div>
            <%} %>
            <%if (i > 1)
                { %>
            <div class="col-xl-4 col-lg-4 col-sm-4 col-12">
                <img src="/uploads/<%=anh_trang_chu[i].Thumb %>" class="imgcover" />
            </div>
            <%} %>
            <%} %>
        </div>
    </section>
    <section class="res py-5">
        <div class="container">
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 align-self-center">
                    <iframe width="100%" height="300" src="<%=video_dang_ky.ConfigValue.Replace("watch?v=","embed/") %>" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-11 col-12 ">
                    <div class="heading">
                        <%=tieu_de_dang_ky.ConfigValue %>
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-9 col-12 _tName" placeholder="Họ và tên">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control col-sm-9 col-12 _tPhone" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control col-sm-9 col-12 _tAddress" placeholder="Nơi sinh sống">
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn _bReginster">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <%//Vung hien thi %>
    <section class="client">
        <div class="container">
            <div class="ss-name">
                <h3 class="title"><%=dong_5.ConfigLabel %></h3>
                <div class="text">
                    <%=dong_5.ConfigValue %>
                </div>
            </div>
            <div class="swiper-container pb-5">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <%var slide_lien_ket = ConfigBo.AdvGetByType(105).OrderBy(r => r.SortOrder).ToList(); %>
                    <%foreach (var item in slide_lien_ket)
                        { %>
                    <div class="swiper-slide">
                        <div class="image text-center p-4 ">
                            <img src="/uploads/<%=item.Thumb %>" alt="" class="img-fluid " />
                        </div>
                    </div>

                    <%} %>
                </div>
</div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
    <script>
        $('._bReginster').off('click').on('click', function () {
            console.log(1);
            console.log($(this).parent().parent().find('._tName').val());
            var name = $(this).parent().parent().find('._tName').val();
            var phone = $(this).parent().parent().find('._tPhone').val();
            var address = $(this).parent().parent().find('._tAddress').val();
            var obj = {
                name: name,
                phoneNumber: phone,
                email: 'Khong co email',
                note: 'Địa chỉ: ' + address
            }
            console.log(obj);
            R.Post({
                params: obj,
                module: "ui-action",
                ashx: 'modulerequest.ashx',
                action: "save",
                success: function (res) {
                    if (res.Success) {
                        console.log(1);
                        alert("Cám ơn bạn đã liên hệ!");
                        $(this).closest().find('._tName').val("");
                        $(this).closest().find('._tPhone').val("");
                        $(this).closest().find('._tAddress').val("");
                    }
                    // $('.card-header').RLoadingModuleComplete();
                }
            });
        });
    </script>
</asp:Content>